package ee.stat.dashboard.service.statdata.dimensions;

import ee.stat.dashboard.service.statdata.dimensions.xml.XmlDimension;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

import static ee.stat.dashboard.AssertUtil.LES01;
import static java.util.stream.Collectors.partitioningBy;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.BooleanUtils.isTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;


@SpringBootTest(webEnvironment = RANDOM_PORT)
class StatDataApiDimensionsServiceTest {

    @Autowired
    private StatDataApiDimensionsService statDimensionsService;

    @Test
    void dimensions_are_returned_for_les01() {
        List<XmlDimension> xmlDimensions = statDimensionsService.getDimensions(LES01);
        assertEquals(5, xmlDimensions.size());
        Map<Boolean, List<XmlDimension>> collect = xmlDimensions.stream().collect(partitioningBy(d -> isTrue(d.getTime())));
        List<XmlDimension> timeDimensions = collect.get(true);
        assertEquals(1, timeDimensions.size());
        List<XmlDimension> dimensions = collect.get(false);
        assertEquals(4, dimensions.size());

        List<String> dimensionNames = dimensions.stream().map(XmlDimension::getNameEt).collect(toList());
        assertEquals("Näitaja", dimensionNames.get(0));
        assertEquals("Sugu", dimensionNames.get(1));
        assertEquals("Väärtus/standardviga", dimensionNames.get(2));
        assertEquals("Vanuserühm", dimensionNames.get(3));
    }

    @Test
    void unknown_cube_returns_an_exception() {
        assertThrows(Exception.class, () -> statDimensionsService.getDimensions("unknown"));
    }
}
