package ee.stat.dashboard.service.statdata.request;

import com.fasterxml.jackson.databind.ObjectMapper;
import ee.stat.dashboard.ApplicationInit;
import ee.stat.dashboard.model.widget.back.Filter;
import ee.stat.dashboard.model.widget.back.FilterValue;
import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.model.widget.front.enums.FilterDisplay;
import ee.stat.dashboard.service.client.StatDataApiClient;
import ee.stat.dashboard.service.client.query.SelectionFilter;
import ee.stat.dashboard.service.statdata.StatDimensionsManager;
import ee.stat.dashboard.service.statdata.StatMetaFetchException;
import ee.stat.dashboard.service.statdata.dimensions.api.StatMetaResponse;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlDimension;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ResourceLoader;

import java.util.List;

import static ee.stat.dashboard.AssertUtil.readResourceAsString;
import static ee.stat.dashboard.service.statdata.request.dto.StatRequestConfig.StatRequestConfigBuilder;
import static ee.stat.dashboard.service.statdata.request.dto.StatRequestConfig.builder;
import static java.lang.String.format;
import static java.util.List.of;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@Slf4j
@SpringBootTest(webEnvironment = RANDOM_PORT)
abstract class StatRequestBuilderInterface_CommonTest {

    @Autowired
    protected StatDataSharedRequestBuilderService statDataSharedRequestBuilderService;
    @Autowired
    protected StatDimensionsManager statDimensionsManager;
    @Autowired
    protected ObjectMapper objectMapper;
    @Autowired
    protected ResourceLoader resourceLoader;
    @MockBean
    protected StatDataApiClient statDataApiClient;
    @MockBean
    protected ApplicationInit applicationInit;

    abstract StatDataType statDataType();

    @BeforeEach
    @SneakyThrows
    void setUp() {
        when(statDataApiClient.getMeta(any(Language.class), anyString()))
                .thenAnswer((request) -> {
                    Language language = (Language) request.getArguments()[0];
                    String cube = (String) request.getArguments()[1];
                    return objectMapper.readValue(readResourceAsString(format("meta/%s-%s.json", cube, language.toLocale())), StatMetaResponse.class);
                });
    }

    protected String expected(String location) {
        return readResourceAsString(location).replaceAll("\\s+", "");
    }

    protected StatRequestConfigBuilder simpleUrlConfig(String cube, TimePeriod timePeriod) {
        try {
            return builder().cube(cube).filters(of()).allDimensions(getDimensions(cube)).timePeriod(timePeriod);
        } catch (Exception e) {
            log.error("dimensions failed", e);
            fail("dimensions failed");
            return null;
        }
    }

    protected List<XmlDimension> getDimensions(String cube) throws StatMetaFetchException {
        return statDimensionsManager.getDimensions(cube, statDataType());
    }

    protected Filter time() {
        Filter filter = new Filter();
        filter.setNameEt("Vaatlusperiood");
        filter.setTime(true);
        return filter;
    }

    protected Filter timeYear() {
        Filter filter = new Filter();
        filter.setNameEt("Aasta");
        filter.setTime(true);
        return filter;
    }

    protected Filter timePart() {
        Filter filter = new Filter();
        filter.setNameEt("TimePartGeneric");
        filter.setType(FilterDisplay.TIME_PART);
        return filter;
    }

    protected Filter ageGroup() {
        return filter("Vanuserühm", of(v("0-17"), v("18-64"), v("65 ja vanemad")));
    }

    protected Filter random() {
        return filter("random", of(v("random1"), v("random2"), v("random3")));
    }

    protected Filter value() {
        return filter("Väärtus/standardviga", of(v("Väärtus")));
    }

    protected Filter indicator() {
        return filter("Näitaja", of(v("Absoluutse vaesuse määr, %")));
    }

    protected Filter sex() {
        return filter("Sugu", of(v("Mehed ja naised"), v("Mehed"), v("Naised")));
    }

    protected Filter filter(String name, List<FilterValue> values) {
        Filter filter = new Filter();
        filter.setNameEt(name);
        filter.setValues(values);
        return filter;
    }

    protected FilterValue v(String val) {
        FilterValue value = new FilterValue();
        value.setValueEt(val);
        return value;
    }

    protected List<String> timeValues(List<SelectionFilter> query) {
        return query.get(0).getSelection().getValues();
    }
}
