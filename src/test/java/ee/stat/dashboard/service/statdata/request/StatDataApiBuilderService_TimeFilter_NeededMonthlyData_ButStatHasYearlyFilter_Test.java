package ee.stat.dashboard.service.statdata.request;

import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.service.client.query.StatApiQuery;
import org.junit.jupiter.api.Test;

import java.util.List;

import static ee.stat.dashboard.AssertUtil.TT065;
import static ee.stat.dashboard.AssertUtil._2014_1_1;
import static ee.stat.dashboard.AssertUtil._2017_1_1;
import static ee.stat.dashboard.AssertUtil._2018_1_1;
import static ee.stat.dashboard.AssertUtil._2023_1_1;
import static ee.stat.dashboard.AssertUtil.list_size;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * tests fail when stat has new data, just modify tests
 */
class StatDataApiBuilderService_TimeFilter_NeededMonthlyData_ButStatHasYearlyFilter_Test extends StatRequestBuilderInterface_CommonTest {

    static final String CUBE = TT065;
    static final TimePeriod TIME_PERIOD = TimePeriod.MONTH;

    @Override
    StatDataType statDataType() {
        return StatDataType.DATA_API;
    }

    @Test
    void on_only_startDate_and_endDate___they_set_period_future() throws Exception {
        StatApiQuery query = statDataSharedRequestBuilderService.buildBody(simpleUrlConfig(CUBE, TIME_PERIOD)
                .filters(List.of(timeYear(), timePart()))
                .startDate(_2017_1_1)
                .endDate(_2023_1_1)
                .build(), 1L);

        assertNotNull(query);
        assertEquals("json-stat2", query.getResponse().getFormat());
        list_size(query.getQuery(), 1);
        list_size(timeValues(query.getQuery()), 6);
        assertEquals(expected("requests/tt065/tt065-year-and-month-current-start-end.json"), objectMapper.writeValueAsString(query.getQuery()));
    }

    @Test
    void on_only_startDate_and_endDate___they_set_period_past() throws Exception {
        StatApiQuery query = statDataSharedRequestBuilderService.buildBody(simpleUrlConfig(CUBE, TIME_PERIOD)
                .filters(List.of(timeYear(), timePart()))
                .startDate(_2017_1_1)
                .endDate(_2018_1_1)
                .build(), 1L);

        assertNotNull(query);
        assertEquals("json-stat2", query.getResponse().getFormat());
        list_size(query.getQuery(), 1);
        list_size(timeValues(query.getQuery()), 2);
        assertEquals(expected("requests/tt065/tt065-year-and-month-past-start-end.json"), objectMapper.writeValueAsString(query.getQuery()));
    }

    @Test
    void on_all_3___endDate_and_period_set_period() throws Exception {
        StatApiQuery query = statDataSharedRequestBuilderService.buildBody(simpleUrlConfig(CUBE, TIME_PERIOD)
                .filters(List.of(timeYear(), timePart()))
                .periods(36)
                .startDate(_2014_1_1)
                .endDate(_2023_1_1)
                .build(), 1L);

        assertNotNull(query);
        assertEquals("json-stat2", query.getResponse().getFormat());
        list_size(query.getQuery(), 1);
        list_size(timeValues(query.getQuery()), 5);
        assertEquals(expected("requests/tt065/tt065-year-and-month-periods-current-36-periods.json"), objectMapper.writeValueAsString(query.getQuery()));
    }

    @Test
    void on_endDate_and_periods___period_is_set_by_them_future() throws Exception {
        StatApiQuery query = statDataSharedRequestBuilderService.buildBody(simpleUrlConfig(CUBE, TIME_PERIOD)
                .filters(List.of(timeYear(), timePart()))
                .periods(36)
                .endDate(_2023_1_1)
                .build(), 1L);

        assertNotNull(query);
        assertEquals("json-stat2", query.getResponse().getFormat());
        list_size(query.getQuery(), 1);
        list_size(timeValues(query.getQuery()), 5);
        assertEquals(expected("requests/tt065/tt065-year-and-month-periods-current-36-periods.json"), objectMapper.writeValueAsString(query.getQuery()));
    }

    @Test
    void on_endDate_and_periods___period_is_set_by_them_past() throws Exception {
        StatApiQuery query = statDataSharedRequestBuilderService.buildBody(simpleUrlConfig(CUBE, TIME_PERIOD)
                .filters(List.of(timeYear(), timePart()))
                .periods(36)
                .endDate(_2018_1_1)
                .build(), 1L);

        assertNotNull(query);
        assertEquals("json-stat2", query.getResponse().getFormat());
        list_size(query.getQuery(), 1);
        list_size(timeValues(query.getQuery()), 5);
        assertEquals(expected("requests/tt065/tt065-year-and-month-periods-past-36-periods.json"), objectMapper.writeValueAsString(query.getQuery()));
    }

    @Test
    void on_only_periods___they_set_period() throws Exception {
        StatApiQuery query = statDataSharedRequestBuilderService.buildBody(simpleUrlConfig(CUBE, TIME_PERIOD)
                .filters(List.of(timeYear(), timePart()))
                .periods(36)
                .build(), 1L);

        assertNotNull(query);
        assertEquals("json-stat2", query.getResponse().getFormat());
        list_size(query.getQuery(), 1);
        list_size(timeValues(query.getQuery()), 5);
        assertEquals(expected("requests/tt065/tt065-year-and-month-periods-current-36-periods.json"), objectMapper.writeValueAsString(query.getQuery()));
    }

    @Test
    void on_periods_and_startdate__periods_set_period() throws Exception {
        StatApiQuery query = statDataSharedRequestBuilderService.buildBody(simpleUrlConfig(CUBE, TIME_PERIOD)
                .filters(List.of(timeYear(), timePart()))
                .periods(36)
                .startDate(_2023_1_1)
                .build(), 1L);

        assertNotNull(query);
        assertEquals("json-stat2", query.getResponse().getFormat());
        list_size(query.getQuery(), 1);
        list_size(timeValues(query.getQuery()), 5);
        assertEquals(expected("requests/tt065/tt065-year-and-month-periods-current-36-periods.json"), objectMapper.writeValueAsString(query.getQuery()));
    }
}
