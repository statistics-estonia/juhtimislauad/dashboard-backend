package ee.stat.dashboard.service.statdata.request;

import ee.stat.dashboard.model.widget.back.Filter;
import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.service.client.query.StatApiQuery;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlDimension;
import org.junit.jupiter.api.Test;

import java.util.List;

import static ee.stat.dashboard.AssertUtil.LES01;
import static ee.stat.dashboard.service.statdata.request.dto.StatRequestConfig.builder;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * tests fail when stat has new data, just modify tests
 */
class StatDataApiBuilderService_RegularFilter_Test extends StatRequestBuilderInterface_CommonTest {

    static final TimePeriod TIME_PERIOD = TimePeriod.YEAR;

    @Override
    StatDataType statDataType() {
        return StatDataType.DATA_API;
    }

    @Test
    void empty_filters_define_empty_query() throws Exception {
        List<Filter> filters = List.of();
        StatApiQuery query = statDataSharedRequestBuilderService.buildBody(builder().timePeriod(TIME_PERIOD).cube(LES01)
                .filters(filters).allDimensions(getDimensions(LES01)).timeDimensions(null).build(), 1L);

        assertNotNull(query);
        assertEquals("json-stat2", query.getResponse().getFormat());
        assertEquals("[]", objectMapper.writeValueAsString(query.getQuery()));
    }

    @Test
    void incorrect_filter_returns_empty() throws Exception {
        List<Filter> filters = List.of(random());
        StatApiQuery query = statDataSharedRequestBuilderService.buildBody(builder().timePeriod(TIME_PERIOD).cube(LES01)
                .filters(filters).allDimensions(getDimensions(LES01)).timeDimensions(null).build(), 1L);

        assertNotNull(query);
        assertEquals("json-stat2", query.getResponse().getFormat());
        assertEquals("[]", objectMapper.writeValueAsString(query.getQuery()));
    }

    @Test
    void age_group_defines_first_filter() throws Exception {
        List<Filter> filters = List.of(ageGroup());
        List<XmlDimension> xmlDimensions = getDimensions(LES01);
        StatApiQuery query = statDataSharedRequestBuilderService.buildBody(builder().timePeriod(TIME_PERIOD).cube(LES01)
                .filters(filters).allDimensions(xmlDimensions).timeDimensions(null).build(), 1L);

        assertNotNull(query);
        assertEquals("json-stat2", query.getResponse().getFormat());
        assertEquals(expected("requests/les01/les01-1-agegroup.json"), objectMapper.writeValueAsString(query.getQuery()));
    }

    @Test
    void indicator_defines_second_filter() throws Exception {
        List<Filter> filters = List.of(indicator());
        StatApiQuery query = statDataSharedRequestBuilderService.buildBody(builder().timePeriod(TIME_PERIOD).cube(LES01)
                .filters(filters).allDimensions(getDimensions(LES01)).timeDimensions(null).build(), 1L);

        assertNotNull(query);
        assertEquals("json-stat2", query.getResponse().getFormat());
        assertEquals(expected("requests/les01/les01-1-indicator.json"), objectMapper.writeValueAsString(query.getQuery()));
    }

    @Test
    void age_group_and_indicator_define_first_two_filters() throws Exception {
        List<Filter> filters = List.of(ageGroup(), indicator());
        StatApiQuery query = statDataSharedRequestBuilderService.buildBody(builder().timePeriod(TIME_PERIOD).cube(LES01)
                .filters(filters).allDimensions(getDimensions(LES01)).timeDimensions(null).build(), 1L);

        assertNotNull(query);
        assertEquals("json-stat2", query.getResponse().getFormat());
        assertEquals(expected("requests/les01/les01-2-agegroup-indicator.json"), objectMapper.writeValueAsString(query.getQuery()));
    }

    @Test
    void les_url_full_filters() throws Exception {
        List<Filter> filters = List.of(ageGroup(), indicator(), sex(), value());
        StatApiQuery query = statDataSharedRequestBuilderService.buildBody(builder().timePeriod(TIME_PERIOD).cube(LES01)
                .filters(filters).allDimensions(getDimensions(LES01)).timeDimensions(null).build(), 1L);

        assertNotNull(query);
        assertEquals("json-stat2", query.getResponse().getFormat());
        assertEquals(expected("requests/les01/les01-full-query.json"), objectMapper.writeValueAsString(query.getQuery()));
    }
}
