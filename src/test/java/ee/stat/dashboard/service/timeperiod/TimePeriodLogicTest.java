package ee.stat.dashboard.service.timeperiod;

import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.service.widget.widget.timeperiod.TimePeriodLogic;
import ee.stat.dashboard.service.widget.widget.timeperiod.TimePeriodService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class TimePeriodLogicTest {

    @Autowired
    protected TimePeriodService timePeriodService;

    @Test
    void only_valid_time_periods_are_allowed(){

        assertNotNull(getTimePeriodLogic(TimePeriod.WEEK));
        assertNotNull(getTimePeriodLogic(TimePeriod.MONTH));
        assertNotNull(getTimePeriodLogic(TimePeriod.QUARTER));
        assertNotNull(getTimePeriodLogic(TimePeriod.YEAR));

        throwsError(null);
    }

    private TimePeriodLogic getTimePeriodLogic(TimePeriod timePeriod) {
        return timePeriodService.getLogic(timePeriod);
    }

    private void throwsError(TimePeriod timePeriod) {
        try {
            getTimePeriodLogic(timePeriod);
            fail("should fail");
        } catch (Exception e) {

        }
    }
}
