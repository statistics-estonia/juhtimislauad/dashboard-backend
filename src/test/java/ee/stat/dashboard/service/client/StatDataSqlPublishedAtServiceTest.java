package ee.stat.dashboard.service.client;

import ee.stat.dashboard.service.statdata.publishedat.StatDataSqlPublishedAtService;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StatDataSqlPublishedAtServiceTest {

    @Test
    void cubeCacheDateParseWinter() {
        LocalDateTime parse = StatDataSqlPublishedAtService.parse("2020-10-26T16:05:36");
        LocalDateTime expected = LocalDateTime.of(2020, 10, 26, 14, 5, 36);
        assertEquals(expected, parse);
    }

    @Test
    void cubeCacheDateParseWinter2() {
        LocalDateTime parse = StatDataSqlPublishedAtService.parse("2021-03-15T15:56:30");
        LocalDateTime expected = LocalDateTime.of(2021, 3, 15, 13, 56, 30);
        assertEquals(expected, parse);
    }

    @Test
    void cubeCacheDateParseSummer() {
        LocalDateTime parse = StatDataSqlPublishedAtService.parse("2020-06-26T16:05:36");
        LocalDateTime expected = LocalDateTime.of(2020, 6, 26, 13, 5, 36);
        assertEquals(expected, parse);
    }
}
