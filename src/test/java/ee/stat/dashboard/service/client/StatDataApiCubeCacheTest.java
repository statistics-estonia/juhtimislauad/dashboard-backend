package ee.stat.dashboard.service.client;

import ee.stat.dashboard.service.statdata.publishedat.StatDataApiCubeCache;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StatDataApiCubeCacheTest {

    @Test
    void cubeCacheDateParseWinter() {
        LocalDateTime parse = StatDataApiCubeCache.parse("26.10.2020 16:05:36");
        LocalDateTime expected = LocalDateTime.of(2020, 10, 26, 14, 5, 36);
        assertEquals(expected, parse);
    }

    @Test
    void cubeCacheDateParseWinter2() {
        LocalDateTime parse = StatDataApiCubeCache.parse("15.03.2021 15:56:30");
        LocalDateTime expected = LocalDateTime.of(2021, 3, 15, 13, 56, 30);
        assertEquals(expected, parse);
    }

    @Test
    void cubeCacheDateParseSummer() {
        LocalDateTime parse = StatDataApiCubeCache.parse("26.06.2020 16:05:36");
        LocalDateTime expected = LocalDateTime.of(2020, 6, 26, 13, 5, 36);
        assertEquals(expected, parse);
    }
}
