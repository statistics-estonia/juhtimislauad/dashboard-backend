package ee.stat.dashboard.service.widget.coordinates.converters;

import ee.stat.dashboard.service.widget.coordinates.exception.TimePartMappingException;
import org.junit.jupiter.api.Test;

import static ee.stat.dashboard.model.dashboard.Quarter.I;
import static ee.stat.dashboard.model.dashboard.Quarter.II;
import static ee.stat.dashboard.model.dashboard.Quarter.III;
import static ee.stat.dashboard.model.dashboard.Quarter.IV;
import static ee.stat.dashboard.service.widget.coordinates.converters.QuarterConverter.convert;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class QuarterConverterTest {

    @Test
    void convertTest() throws TimePartMappingException {
        assertEquals(I, convert("1 lalala"));
        assertEquals(I, convert("I lalala"));

        assertEquals(II, convert("2 lalala"));
        assertEquals(II, convert("II lalala"));

        assertEquals(III, convert("3 lalala"));
        assertEquals(III, convert("III lalala"));

        assertEquals(IV, convert("4 lalala"));
        assertEquals(IV, convert("IV lalala"));

        throwsError("I-IV lalala");
        throwsError("1-4 lalala");
        throwsError("lalala");
    }

    private void throwsError(String string) {
        try {
            convert(string);
            fail("should fail");
        } catch (Exception ignored) {

        }
    }
}
