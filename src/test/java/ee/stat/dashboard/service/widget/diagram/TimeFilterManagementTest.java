package ee.stat.dashboard.service.widget.diagram;

import ee.stat.dashboard.model.widget.back.FilterValue;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;

import static ee.stat.dashboard.AssertUtil._2013_1_1;
import static ee.stat.dashboard.AssertUtil._2014_1_1;
import static ee.stat.dashboard.AssertUtil._2016_1_1;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TimeFilterManagementTest {

    @Test
    void sortDesc() {
        List<FilterValue> filterValues = asList(value(_2014_1_1), value(_2013_1_1), value(_2016_1_1));
        TimeFilterManagement.sortDesc(filterValues);
        assertEquals(_2016_1_1.toString(), filterValues.get(0).getValueEt());
        assertEquals(_2014_1_1.toString(), filterValues.get(1).getValueEt());
        assertEquals(_2013_1_1.toString(), filterValues.get(2).getValueEt());
    }

    private FilterValue value(LocalDate localDate) {
        FilterValue filterValue = new FilterValue();
        filterValue.setValueEt(localDate.toString());
        return filterValue;
    }
}
