package ee.stat.dashboard.service.widget;

import ee.stat.dashboard.model.json.CoordinatedResponse;
import ee.stat.dashboard.model.json.CoordinatedResponses;
import ee.stat.dashboard.model.json.DataPoint;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.excel.Excel;
import ee.stat.dashboard.service.widget.coordinates.ExcelToCoordinates;
import ee.stat.dashboard.service.widget.coordinates.ResponseMerger;
import ee.stat.dashboard.service.widget.coordinates.exception.ExcelImportException;
import ee.stat.dashboard.service.widget.coordinates.exception.RawDataException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static ee.stat.dashboard.AssertUtil.MALES;
import static ee.stat.dashboard.AssertUtil.TOTAL;
import static ee.stat.dashboard.AssertUtil._0to17;
import static ee.stat.dashboard.AssertUtil._2019_1_1;
import static ee.stat.dashboard.AssertUtil._2023_1_1;
import static ee.stat.dashboard.AssertUtil._65p;
import static ee.stat.dashboard.model.widget.back.enums.ExcelType.DATA;
import static ee.stat.dashboard.model.widget.back.enums.Language.ET;
import static ee.stat.dashboard.service.excel.Excel_PovertyValidator.validate;
import static ee.stat.dashboard.service.excel.Excel_PovertyValidator.validateAbsPovertyExpectationsAndReality;
import static ee.stat.dashboard.service.excel.Excel_PovertyValidator.validateExpectations;
import static ee.stat.dashboard.service.excel.Excel_PovertyValidator.validateMergeReality;
import static ee.stat.dashboard.service.excel.Excel_PovertyValidator.validateMergeRealityWithoutSize;
import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
class ResponseMergerTest {

    @Autowired
    private ExcelToCoordinates converter;

    @Test
    void when_there_is_real_data_after_expectations_expectations_are_ignored() throws ExcelImportException, RawDataException {
        Excel excel = new Excel("./excel/data/abs_poverty_expect_and_reality.xlsx", DATA);
        Language lang = ET;
        CoordinatedResponses responses = converter.convert(excel, lang, null);
        validateAbsPovertyExpectationsAndReality(lang, responses);
        CoordinatedResponse singleResponse = ResponseMerger.mergeToSingleResponse(responses.getResponses()).getResponse();

        long count = singleResponse.getDataPoints().stream().filter(d -> d.getSource().isExpectation()).count();
        assertEquals(0, count);
    }

    @Test
    void two_responses_are_merged_reality_dominates_expectations() throws ExcelImportException, RawDataException {
        Excel excel = new Excel("./excel/data/abs_poverty_expect_and_reality_for_merge.xlsx", DATA);
        Language lang = ET;
        CoordinatedResponses responses = converter.convert(excel, lang, null);
        validateExpectations(responses.getResponses().get(0), lang);
        int size = responses.getResponses().get(1).getDataPoints().size();
        validateMergeReality(responses.getResponses().get(1), lang);
        assertEquals(27, size);
        CoordinatedResponse singleResponse = ResponseMerger.mergeToSingleResponse(responses.getResponses()).getResponse();

        long countReal = singleResponse.getDataPoints().stream().filter(d -> d.getSource().isRealData()).count();
        long countExp = singleResponse.getDataPoints().stream().filter(d -> d.getSource().isExpectation()).count();

        List<DataPoint> observations2 = singleResponse.getDataPoints();
        assertEquals(72, observations2.size());
        assertEquals(27, countReal);
        assertEquals(45, countExp);
        validateMergeRealityWithoutSize(singleResponse, lang);
        validate(observations2.get(27), 3.5, _0to17, MALES.name(lang), _2019_1_1);
        validate(observations2.get(71), 0.7, _65p, TOTAL.name(lang), _2023_1_1);
    }
}
