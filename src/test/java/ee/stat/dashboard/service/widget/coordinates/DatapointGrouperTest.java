package ee.stat.dashboard.service.widget.coordinates;

import ee.stat.dashboard.model.json.DataPoint;
import ee.stat.dashboard.model.json.Source;
import ee.stat.dashboard.model.widget.back.enums.AxisOrder;
import ee.stat.dashboard.model.widget.front.FilterSerie;
import ee.stat.dashboard.model.widget.front.Graph;
import ee.stat.dashboard.model.widget.front.Serie;
import ee.stat.dashboard.service.widget.grouper.DatapointGrouper;
import ee.stat.dashboard.service.widget.grouper.GraphGroupingException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.List;

import static ee.stat.dashboard.AssertUtil.AGE_GROUP;
import static ee.stat.dashboard.AssertUtil.FEMALES;
import static ee.stat.dashboard.AssertUtil.MALES;
import static ee.stat.dashboard.AssertUtil.REGION;
import static ee.stat.dashboard.AssertUtil.SEX;
import static ee.stat.dashboard.AssertUtil.TALLINN;
import static ee.stat.dashboard.AssertUtil._0to17;
import static ee.stat.dashboard.AssertUtil._2013_1_1;
import static ee.stat.dashboard.AssertUtil.first;
import static ee.stat.dashboard.AssertUtil.last;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.model.widget.back.enums.Language.EN;
import static java.math.BigDecimal.ONE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;


@SpringBootTest
class DatapointGrouperTest {

    @Autowired
    private DatapointGrouper datapointGrouper;

    @Test
    void nothing_is_grouped_to_empty_graph() throws GraphGroupingException {
        Graph graph = datapointGrouper.createDiagram(null);
        assertNotNull(graph);
        assertNull(graph.getSeries());
        assertNull(graph.getPeriod());
    }

    @Test
    void _1D_point_is_grouped_to_one_serie() throws GraphGroupingException {
        List<DataPoint> dataPoints = List.of(maleSex(ONE));
        validate_1Serie_1Point(datapointGrouper.createDiagram(dataPoints));
    }

    @Test
    void _2D_point_is_grouped_to_one_serie() throws GraphGroupingException {
        List<DataPoint> dataPoints = List.of(maleSexAge0to17(ONE));
        validate_1Serie_1Point(datapointGrouper.createDiagram(dataPoints));
    }

    @Test
    void _3D_point_is_grouped_to_one_serie() throws GraphGroupingException {
        List<DataPoint> dataPoints = List.of(maleSexAge0to17Tallinn(ONE));
        validate_1Serie_1Point(datapointGrouper.createDiagram(dataPoints));
    }

    @Test
    void _2_of_same_1D_points_are_grouped_to_one_serie() throws GraphGroupingException {
        List<DataPoint> dataPoints = List.of(maleSex(ONE), maleSex(ONE));
        validate_1Serie_2Points(datapointGrouper.createDiagram(dataPoints));
    }

    @Test
    void _2_of_the_same_2D_points_are_grouped_to_one_serie() throws GraphGroupingException {
        List<DataPoint> dataPoints = List.of(maleSexAge0to17(ONE), maleSexAge0to17(ONE));
        validate_1Serie_2Points(datapointGrouper.createDiagram(dataPoints));
    }

    @Test
    void _2_of_the_same_3D_points_are_grouped_to_one_serie() throws GraphGroupingException {
        List<DataPoint> dataPoints = List.of(maleSexAge0to17Tallinn(ONE), maleSexAge0to17Tallinn(ONE));
        validate_1Serie_2Points(datapointGrouper.createDiagram(dataPoints));
    }

    @Test
    void _2_of_different_1D_points_are_grouped_to_one_serie() throws GraphGroupingException {
        List<DataPoint> dataPoints = List.of(maleSex(ONE), femaleSex(ONE));
        validate_2Serie_1Point(datapointGrouper.createDiagram(dataPoints));
    }

    @Test
    void _2_of_different_2D_points_are_grouped_to_one_serie() throws GraphGroupingException {
        List<DataPoint> dataPoints = List.of(maleSexAge0to17(ONE), femaleSexAge0to17(ONE));
        validate_2Serie_1Point(datapointGrouper.createDiagram(dataPoints));
    }

    @Test
    void _2_of_different_3D_points_are_grouped_to_one_serie() throws GraphGroupingException {
        List<DataPoint> dataPoints = List.of(maleSexAge0to17Tallinn(ONE), femaleSexAge0to17Tallinn(ONE));
        validate_2Serie_1Point(datapointGrouper.createDiagram(dataPoints));
    }

    @Test
    void group_by_time() throws GraphGroupingException {
        List<DataPoint> dataPoints = List.of(maleSex(ONE), maleSex(ONE));
        validate_1Serie_2Points(datapointGrouper.createDiagram(dataPoints, "Sex", "Time", AxisOrder.VALUE_DESC, null));
    }

    @Test
    void group_by_time_desc_nulls_are_last() throws GraphGroupingException {
        List<DataPoint> dataPoints = List.of(maleSex(ONE), maleSex(null));
        Graph diagram = datapointGrouper.createDiagram(dataPoints, "Sex", "Time", AxisOrder.VALUE_DESC, null);
        validate_1Serie_2Points(diagram);
        FilterSerie series = first(diagram.getSeries());
        assertEquals(ONE, first(series.getSeries()).getValue());
        assertNull(last(series.getSeries()).getValue());
    }

    @Test
    void group_by_time_asc_nulls_are_first() throws GraphGroupingException {
        List<DataPoint> dataPoints = List.of(maleSex(ONE), maleSex(null));
        Graph diagram = datapointGrouper.createDiagram(dataPoints, "Sex", "Time", AxisOrder.VALUE_ASC, null);
        validate_1Serie_2Points(diagram);
        FilterSerie series = first(diagram.getSeries());
        assertNull(first(series.getSeries()).getValue());
        assertEquals(ONE, last(series.getSeries()).getValue());
    }

    private DataPoint maleSex(BigDecimal decimal) {
        return DataPoint.builder()
                .dim1(SEX.name(EN))
                .val1(MALES.name(EN))
                .date(_2013_1_1)
                .source(Source.STAT_DATA_API)
                .value(decimal).build();
    }

    private DataPoint femaleSex(BigDecimal decimal) {
        return DataPoint.builder()
                .dim1(SEX.name(EN))
                .val1(FEMALES.name(EN))
                .date(_2013_1_1)
                .source(Source.STAT_DATA_API)
                .value(decimal).build();
    }

    private DataPoint maleSexAge0to17(BigDecimal decimal) {
        return DataPoint.builder()
                .dim1(SEX.name(EN))
                .val1(MALES.name(EN))
                .dim2(AGE_GROUP.name(EN))
                .val2(_0to17)
                .date(_2013_1_1)
                .source(Source.STAT_DATA_API)
                .value(decimal).build();
    }

    private DataPoint maleSexAge0to17Tallinn(BigDecimal decimal) {
        return DataPoint.builder()
                .dim1(SEX.name(EN))
                .val1(MALES.name(EN))
                .dim2(AGE_GROUP.name(EN))
                .val2(_0to17)
                .date(_2013_1_1)
                .dim3(REGION.name(EN))
                .val3(TALLINN)
                .source(Source.STAT_DATA_API)
                .value(decimal).build();
    }

    private DataPoint femaleSexAge0to17(BigDecimal decimal) {
        return DataPoint.builder()
                .dim1(SEX.name(EN))
                .val1(FEMALES.name(EN))
                .dim2(AGE_GROUP.name(EN))
                .val2(_0to17)
                .date(_2013_1_1)
                .source(Source.STAT_DATA_API)
                .value(decimal).build();
    }

    private DataPoint femaleSexAge0to17Tallinn(BigDecimal decimal) {
        return DataPoint.builder()
                .dim1(SEX.name(EN))
                .val1(FEMALES.name(EN))
                .dim2(AGE_GROUP.name(EN))
                .val2(_0to17)
                .date(_2013_1_1)
                .dim3(REGION.name(EN))
                .val3(TALLINN)
                .source(Source.STAT_DATA_API)
                .value(decimal).build();
    }

    private void validate_1Serie_1Point(Graph graph) {
        validate_xSerie_yPoints(graph, 1, 1);
    }

    private void validate_1Serie_2Points(Graph graph) {
        validate_xSerie_yPoints(graph, 1, 2);
    }

    private void validate_2Serie_1Point(Graph graph) {
        validate_xSerie_yPoints(graph, 2, 1);
    }

    private void validate_xSerie_yPoints(Graph graph, int x, int y) {
        assertNotNull(graph);
        List<FilterSerie> filterSeries = graph.getSeries();
        list_size(filterSeries, x);
        List<Serie> series = filterSeries.get(0).getSeries();
        list_size(series, y);
        assertNull(graph.getPeriod());
    }
}
