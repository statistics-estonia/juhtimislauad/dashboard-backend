package ee.stat.dashboard.service.widget.diagram;

import com.google.common.collect.Lists;
import ee.stat.dashboard.model.widget.front.FilterSerie;
import ee.stat.dashboard.model.widget.front.Period;
import ee.stat.dashboard.model.widget.front.Serie;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static ee.stat.dashboard.AssertUtil._2011_12_31;
import static ee.stat.dashboard.AssertUtil._2012_1_1;
import static ee.stat.dashboard.AssertUtil._2013_1_1;
import static ee.stat.dashboard.AssertUtil._2014_1_1;
import static ee.stat.dashboard.AssertUtil._2015_1_1;
import static ee.stat.dashboard.AssertUtil._2016_1_1;
import static ee.stat.dashboard.AssertUtil._2017_1_1;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.model.json.Source.GENERATED_REAL;
import static ee.stat.dashboard.model.json.Source.GENERATED_TARGET;
import static ee.stat.dashboard.model.json.Source.STAT_DATA_API;
import static ee.stat.dashboard.service.widget.diagram.DiagramFixer.addMissingPoints;
import static ee.stat.dashboard.service.widget.diagram.DiagramFixer.pickSource;
import static ee.stat.dashboard.service.widget.diagram.DiagramFixer.removeOutsidePoints;
import static java.math.BigDecimal.ONE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class DiagramFixerTest {

    @Test
    void points_are_added_as_for_missing_dates() {
        FilterSerie serie = serie(Lists.newArrayList(new Serie(_2013_1_1, ONE, STAT_DATA_API)));
        List<LocalDate> dates = new ArrayList<>();
        dates.add(_2013_1_1);
        dates.add(_2014_1_1);
        addMissingPoints(serie, dates, null);
        List<Serie> series = serie.getSeries();
        list_size(series, 2);
        assertEquals(ONE, series.get(0).getValue());
        assertEquals(_2013_1_1, series.get(0).getDate());
        assertNull(series.get(1).getValue());
        assertEquals(_2014_1_1, series.get(1).getDate());
    }

    @Test
    void outlier_points_are_removed() {
        FilterSerie serie = serie(Lists.newArrayList(
                new Serie(_2013_1_1, ONE, STAT_DATA_API),
                new Serie(_2014_1_1, ONE, STAT_DATA_API),
                new Serie(_2015_1_1, ONE, STAT_DATA_API),
                new Serie(_2016_1_1, ONE, STAT_DATA_API)
        ));
        removeOutsidePoints(serie, new Period(_2014_1_1, _2015_1_1));
        List<Serie> series = serie.getSeries();
        list_size(series, 2);
        assertEquals(_2014_1_1, series.get(0).getDate());
        assertEquals(_2015_1_1, series.get(1).getDate());
    }

    @Test
    void inliner_points_are_not_removed() {
        FilterSerie serie = serie(Lists.newArrayList(
                new Serie(_2013_1_1, ONE, STAT_DATA_API),
                new Serie(_2014_1_1, ONE, STAT_DATA_API),
                new Serie(_2015_1_1, ONE, STAT_DATA_API),
                new Serie(_2016_1_1, ONE, STAT_DATA_API)
        ));
        removeOutsidePoints(serie, new Period(_2012_1_1, _2017_1_1));
        List<Serie> series = serie.getSeries();
        list_size(series, 4);
        assertEquals(_2013_1_1, series.get(0).getDate());
        assertEquals(_2014_1_1, series.get(1).getDate());
        assertEquals(_2015_1_1, series.get(2).getDate());
        assertEquals(_2016_1_1, series.get(3).getDate());
    }

    @Test
    void if_data_date_is_same_as_max_real_it_is_generated_real() {
        assertEquals(GENERATED_REAL, pickSource(_2013_1_1, _2013_1_1));
    }

    @Test
    void if_data_date_is_same_smaller_as_max_real_it_is_generated_real() {
        assertEquals(GENERATED_REAL, pickSource(_2011_12_31, _2013_1_1));
    }

    @Test
    void if_max_real_data_date_is_missing_it_is_generated_real() {
        assertEquals(GENERATED_REAL, pickSource(_2013_1_1, null));
    }

    @Test
    void if_data_date_is_bigger_as_max_real_it_is_generated_exp() {
        assertEquals(GENERATED_TARGET, pickSource(_2014_1_1, _2013_1_1));
    }

    private FilterSerie serie(List<Serie> series) {
        FilterSerie serie = new FilterSerie();
        serie.setSeries(series);
        return serie;
    }
}
