package ee.stat.dashboard.service.widget.coordinates.timepart;

import ee.stat.dashboard.model.widget.back.enums.TimePart;
import ee.stat.dashboard.service.widget.coordinates.exception.TimePartMappingException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class TimePartLogicTest {

    @Autowired
    protected TimePartService timePartService;

    @Test
    void only_valid_time_parts_are_allowed() throws TimePartMappingException {

        assertNotNull(getTimePartLogic(TimePart.WEEK));
        assertNotNull(getTimePartLogic(TimePart.MONTH));
        assertNotNull(getTimePartLogic(TimePart.QUARTER));

        throwsError(null);
    }

    private TimePartLogic getTimePartLogic(TimePart timePart) throws TimePartMappingException {
        return timePartService.timePartLogic(timePart);
    }

    private void throwsError(TimePart timePart) {
        try {
            getTimePartLogic(timePart);
            fail("should fail");
        } catch (Exception e) {

        }
    }
}
