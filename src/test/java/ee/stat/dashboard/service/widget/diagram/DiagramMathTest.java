package ee.stat.dashboard.service.widget.diagram;

import ee.stat.dashboard.model.json.CoordinatedResponse;
import ee.stat.dashboard.model.json.DataPoint;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

import static ee.stat.dashboard.AssertUtil.list_size;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.ZERO;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class DiagramMathTest {

    static final BigDecimal _500 = new BigDecimal("500");
    static final BigDecimal _50 = new BigDecimal("50");
    static final BigDecimal _0_5 = new BigDecimal("0.5");
    static final BigDecimal _3 = new BigDecimal("3");

    @Test
    void round_0_rounds_to_full_numbers() {
        List<DataPoint> points = List.of(point(ONE), point(TEN), point(ZERO), point(_0_5), point(_500), point(null));
        CoordinatedResponse response = new CoordinatedResponse(points);
        DiagramMath.round(response, 0);
        List<DataPoint> dataPoints = response.getDataPoints();
        list_size(dataPoints, 6);
        assertEquals(ONE, dataPoints.get(0).getValue());
        assertEquals(TEN, dataPoints.get(1).getValue());
        assertEquals(ZERO, dataPoints.get(2).getValue());
        assertEquals(ONE, dataPoints.get(3).getValue());
        assertEquals(_500, dataPoints.get(4).getValue());
        assertNull(dataPoints.get(5).getValue());
    }


    @Test
    void round_1_rounds_to_one_number_after_coma() {
        List<DataPoint> points = List.of(point(ONE), point(TEN), point(ZERO), point(_0_5), point(_500), point(null));
        CoordinatedResponse response = new CoordinatedResponse(points);
        DiagramMath.round(response, 1);
        List<DataPoint> dataPoints = response.getDataPoints();
        list_size(dataPoints, 6);
        assertEquals(new BigDecimal("1.0"), dataPoints.get(0).getValue());
        assertEquals(new BigDecimal("10.0"), dataPoints.get(1).getValue());
        assertEquals(new BigDecimal("0.0"), dataPoints.get(2).getValue());
        assertEquals(_0_5, dataPoints.get(3).getValue());
        assertEquals(new BigDecimal("500.0"), dataPoints.get(4).getValue());
        assertNull(dataPoints.get(5).getValue());
    }

    @Test
    void round_n1_rounds_to_one_tens() {
        List<DataPoint> points = List.of(point(ONE), point(TEN), point(ZERO), point(_0_5), point(_500), point(null));
        CoordinatedResponse response = new CoordinatedResponse(points);
        DiagramMath.round(response, -1);
        List<DataPoint> dataPoints = response.getDataPoints();
        list_size(dataPoints, 6);
        assertEquals(ZERO, toBigDecimalWithoutScale(dataPoints.get(0).getValue()));
        assertEquals(TEN, toBigDecimalWithoutScale(dataPoints.get(1).getValue()));
        assertEquals(ZERO, toBigDecimalWithoutScale(dataPoints.get(2).getValue()));
        assertEquals(ZERO, toBigDecimalWithoutScale(dataPoints.get(3).getValue()));
        assertEquals(_500, toBigDecimalWithoutScale(dataPoints.get(4).getValue()));
        assertNull(dataPoints.get(5).getValue());
    }

    @Test
    void divide_values_by_10() {
        List<DataPoint> points = List.of(point(ONE), point(TEN), point(ZERO), point(_0_5), point(_500), point(null));
        CoordinatedResponse response = new CoordinatedResponse(points);
        DiagramMath.divideValues(response, TEN);
        List<DataPoint> dataPoints = response.getDataPoints();
        list_size(dataPoints, 6);
        assertEquals(ZERO, dataPoints.get(0).getValue());
        assertEquals(ONE, dataPoints.get(1).getValue());
        assertEquals(ZERO, dataPoints.get(2).getValue());
        assertEquals(new BigDecimal("0.1"), dataPoints.get(3).getValue());
        assertEquals(_50, dataPoints.get(4).getValue());
        assertNull(dataPoints.get(5).getValue());
    }

    @Test
    void divide_values_by_3() {
        List<DataPoint> points = List.of(point(ONE), point(TEN), point(ZERO), point(_0_5), point(_500), point(null));
        CoordinatedResponse response = new CoordinatedResponse(points);
        DiagramMath.divideValues(response, _3);
        List<DataPoint> dataPoints = response.getDataPoints();
        list_size(dataPoints, 6);
        assertEquals(ZERO, dataPoints.get(0).getValue());
        assertEquals(_3, dataPoints.get(1).getValue());
        assertEquals(ZERO, dataPoints.get(2).getValue());
        assertEquals(new BigDecimal("0.2"), dataPoints.get(3).getValue());
        assertEquals(new BigDecimal("167"), dataPoints.get(4).getValue());
        assertNull(dataPoints.get(5).getValue());
    }

    private BigDecimal toBigDecimalWithoutScale(BigDecimal value) {
        return new BigDecimal(value.toBigInteger());
    }

    private DataPoint point(BigDecimal value) {
        DataPoint dataPoint = new DataPoint();
        dataPoint.setValue(value);
        return dataPoint;
    }
}
