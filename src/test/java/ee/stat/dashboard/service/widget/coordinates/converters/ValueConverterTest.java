package ee.stat.dashboard.service.widget.coordinates.converters;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ValueConverterTest {

    @Test
    void values_are_converted_to_bigdecimal(){
        assertNull(ValueConverter.getValue(null));
        assertEquals(BigDecimal.ONE, ValueConverter.getValue(1));
        assertEquals(new BigDecimal("1.1"), ValueConverter.getValue(1.1));
    }

    @Test
    void unknown_types_cause_illegal_state_exception() {
        assertThrows(IllegalStateException.class, () ->ValueConverter.getValue(1.1f));
    }
}
