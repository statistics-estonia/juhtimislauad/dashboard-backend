package ee.stat.dashboard.service.widget.coordinates.converters;

import ee.stat.dashboard.service.widget.coordinates.exception.TimePartMappingException;
import org.junit.jupiter.api.Test;

import static ee.stat.dashboard.model.dashboard.Week._1;
import static ee.stat.dashboard.model.dashboard.Week._10;
import static ee.stat.dashboard.model.dashboard.Week._11;
import static ee.stat.dashboard.model.dashboard.Week._12;
import static ee.stat.dashboard.model.dashboard.Week._13;
import static ee.stat.dashboard.model.dashboard.Week._14;
import static ee.stat.dashboard.model.dashboard.Week._15;
import static ee.stat.dashboard.model.dashboard.Week._16;
import static ee.stat.dashboard.model.dashboard.Week._17;
import static ee.stat.dashboard.model.dashboard.Week._18;
import static ee.stat.dashboard.model.dashboard.Week._19;
import static ee.stat.dashboard.model.dashboard.Week._2;
import static ee.stat.dashboard.model.dashboard.Week._20;
import static ee.stat.dashboard.model.dashboard.Week._21;
import static ee.stat.dashboard.model.dashboard.Week._22;
import static ee.stat.dashboard.model.dashboard.Week._23;
import static ee.stat.dashboard.model.dashboard.Week._24;
import static ee.stat.dashboard.model.dashboard.Week._25;
import static ee.stat.dashboard.model.dashboard.Week._26;
import static ee.stat.dashboard.model.dashboard.Week._27;
import static ee.stat.dashboard.model.dashboard.Week._28;
import static ee.stat.dashboard.model.dashboard.Week._29;
import static ee.stat.dashboard.model.dashboard.Week._3;
import static ee.stat.dashboard.model.dashboard.Week._30;
import static ee.stat.dashboard.model.dashboard.Week._31;
import static ee.stat.dashboard.model.dashboard.Week._32;
import static ee.stat.dashboard.model.dashboard.Week._33;
import static ee.stat.dashboard.model.dashboard.Week._34;
import static ee.stat.dashboard.model.dashboard.Week._35;
import static ee.stat.dashboard.model.dashboard.Week._36;
import static ee.stat.dashboard.model.dashboard.Week._37;
import static ee.stat.dashboard.model.dashboard.Week._38;
import static ee.stat.dashboard.model.dashboard.Week._39;
import static ee.stat.dashboard.model.dashboard.Week._4;
import static ee.stat.dashboard.model.dashboard.Week._40;
import static ee.stat.dashboard.model.dashboard.Week._41;
import static ee.stat.dashboard.model.dashboard.Week._42;
import static ee.stat.dashboard.model.dashboard.Week._43;
import static ee.stat.dashboard.model.dashboard.Week._44;
import static ee.stat.dashboard.model.dashboard.Week._45;
import static ee.stat.dashboard.model.dashboard.Week._46;
import static ee.stat.dashboard.model.dashboard.Week._47;
import static ee.stat.dashboard.model.dashboard.Week._48;
import static ee.stat.dashboard.model.dashboard.Week._49;
import static ee.stat.dashboard.model.dashboard.Week._5;
import static ee.stat.dashboard.model.dashboard.Week._50;
import static ee.stat.dashboard.model.dashboard.Week._51;
import static ee.stat.dashboard.model.dashboard.Week._52;
import static ee.stat.dashboard.model.dashboard.Week._53;
import static ee.stat.dashboard.model.dashboard.Week._6;
import static ee.stat.dashboard.model.dashboard.Week._7;
import static ee.stat.dashboard.model.dashboard.Week._8;
import static ee.stat.dashboard.model.dashboard.Week._9;
import static ee.stat.dashboard.service.widget.coordinates.converters.WeekConverter.convert;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class WeekConverterTest {

    @Test
    void convertWeeks() throws TimePartMappingException {
        assertEquals(_1, convert("1"));
        assertEquals(_2, convert("2"));
        assertEquals(_3, convert("3"));
        assertEquals(_4, convert("4"));
        assertEquals(_5, convert("5"));
        assertEquals(_6, convert("6"));
        assertEquals(_7, convert("7"));
        assertEquals(_8, convert("8"));
        assertEquals(_9, convert("9"));
        assertEquals(_10, convert("10"));
        assertEquals(_11, convert("11"));
        assertEquals(_12, convert("12"));
        assertEquals(_13, convert("13"));
        assertEquals(_14, convert("14"));
        assertEquals(_15, convert("15"));
        assertEquals(_16, convert("16"));
        assertEquals(_17, convert("17"));
        assertEquals(_18, convert("18"));
        assertEquals(_19, convert("19"));
        assertEquals(_20, convert("20"));
        assertEquals(_21, convert("21"));
        assertEquals(_22, convert("22"));
        assertEquals(_23, convert("23"));
        assertEquals(_24, convert("24"));
        assertEquals(_25, convert("25"));
        assertEquals(_26, convert("26"));
        assertEquals(_27, convert("27"));
        assertEquals(_28, convert("28"));
        assertEquals(_29, convert("29"));
        assertEquals(_30, convert("30"));
        assertEquals(_31, convert("31"));
        assertEquals(_32, convert("32"));
        assertEquals(_33, convert("33"));
        assertEquals(_34, convert("34"));
        assertEquals(_35, convert("35"));
        assertEquals(_36, convert("36"));
        assertEquals(_37, convert("37"));
        assertEquals(_38, convert("38"));
        assertEquals(_39, convert("39"));
        assertEquals(_40, convert("40"));
        assertEquals(_41, convert("41"));
        assertEquals(_42, convert("42"));
        assertEquals(_43, convert("43"));
        assertEquals(_44, convert("44"));
        assertEquals(_45, convert("45"));
        assertEquals(_46, convert("46"));
        assertEquals(_47, convert("47"));
        assertEquals(_48, convert("48"));
        assertEquals(_49, convert("49"));
        assertEquals(_50, convert("50"));
        assertEquals(_51, convert("51"));
        assertEquals(_52, convert("52"));
        assertEquals(_53, convert("53"));

        throwsError("Nädalad kokku");
    }

    private void throwsError(String string) {
        try {
            convert(string);
            fail("should fail");
        } catch (Exception ignored) {

        }
    }
}
