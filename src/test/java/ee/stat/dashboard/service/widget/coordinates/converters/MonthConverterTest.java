package ee.stat.dashboard.service.widget.coordinates.converters;

import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.widget.coordinates.exception.TimePartMappingException;
import org.junit.jupiter.api.Test;

import static ee.stat.dashboard.model.widget.back.enums.Language.EN;
import static ee.stat.dashboard.model.widget.back.enums.Language.ET;
import static ee.stat.dashboard.service.widget.coordinates.converters.MonthConverter.convert;
import static java.time.Month.APRIL;
import static java.time.Month.AUGUST;
import static java.time.Month.DECEMBER;
import static java.time.Month.FEBRUARY;
import static java.time.Month.JANUARY;
import static java.time.Month.JULY;
import static java.time.Month.JUNE;
import static java.time.Month.MARCH;
import static java.time.Month.MAY;
import static java.time.Month.NOVEMBER;
import static java.time.Month.OCTOBER;
import static java.time.Month.SEPTEMBER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class MonthConverterTest {

    @Test
    void convertEstMonths() throws TimePartMappingException {
        assertEquals(JANUARY, convert(ET, "jaanuar"));
        assertEquals(FEBRUARY, convert(ET, "veebruar"));
        assertEquals(MARCH, convert(ET, "märts"));
        assertEquals(APRIL, convert(ET, "aprill"));
        assertEquals(MAY, convert(ET, "mai"));
        assertEquals(JUNE, convert(ET, "juuni"));
        assertEquals(JULY, convert(ET, "juuli"));
        assertEquals(AUGUST, convert(ET, "august"));
        assertEquals(SEPTEMBER, convert(ET, "september"));
        assertEquals(OCTOBER, convert(ET, "oktoober"));
        assertEquals(NOVEMBER, convert(ET, "november"));
        assertEquals(DECEMBER, convert(ET, "detsember"));

        throwsError(ET, "kuud kokku");
        throwsError(ET, "kuude keskmine");
    }

    @Test
    void convertEngMonths() throws TimePartMappingException {
        assertEquals(JANUARY, convert(EN, JANUARY.name()));
        assertEquals(FEBRUARY, convert(EN, FEBRUARY.name()));
        assertEquals(MARCH, convert(EN, MARCH.name()));
        assertEquals(APRIL, convert(EN, APRIL.name()));
        assertEquals(MAY, convert(EN, MAY.name()));
        assertEquals(JUNE, convert(EN, JUNE.name()));
        assertEquals(JULY, convert(EN, JULY.name()));
        assertEquals(AUGUST, convert(EN, AUGUST.name()));
        assertEquals(SEPTEMBER, convert(EN, SEPTEMBER.name()));
        assertEquals(OCTOBER, convert(EN, OCTOBER.name()));
        assertEquals(NOVEMBER, convert(EN, NOVEMBER.name()));
        assertEquals(DECEMBER, convert(EN, DECEMBER.name()));

        throwsError(EN, "total month");
        throwsError(EN, "average month");
    }

    private void throwsError(Language lang, String string) {
        try {
            convert(lang, string);
            fail("should fail");
        } catch (Exception ignored) {

        }
    }
}
