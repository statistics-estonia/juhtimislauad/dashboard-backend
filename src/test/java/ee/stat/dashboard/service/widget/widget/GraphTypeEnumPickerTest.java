package ee.stat.dashboard.service.widget.widget;

import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.service.widget.widget.dto.GraphTypeDto;
import ee.stat.dashboard.util.StatBadRequestException;
import org.junit.jupiter.api.Test;

import java.util.List;

import static ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum.bar;
import static ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum.line;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class GraphTypeEnumPickerTest {

    @Test
    void default_is_selected_on_no_input() {
        GraphTypeEnum type = GraphTypePicker.getGraphType(null, getGraphTypes());
        assertEquals(line, type);
    }

    @Test
    void _400_on_wrong_type() {
        assertThrows(StatBadRequestException.class, () -> GraphTypePicker.getGraphType("wrongType", getGraphTypes()));
    }

    @Test
    void line_is_line_when_graph_has_line() {
        GraphTypeEnum type = GraphTypePicker.getGraphType(line.name(), getGraphTypes());
        assertEquals(line, type);
    }

    @Test
    void _400_on_illegal_type() {
        assertThrows(StatBadRequestException.class, () -> GraphTypePicker.getGraphType(bar.name(), getGraphTypes()));
    }

    private GraphTypeDto getGraphTypes() {
        GraphTypeDto types = new GraphTypeDto();
        types.setOptions(List.of(line));
        types.setDefaultOption(line);
        return types;
    }
}
