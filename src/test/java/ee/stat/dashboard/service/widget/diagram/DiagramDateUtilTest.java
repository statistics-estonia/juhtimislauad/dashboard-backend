package ee.stat.dashboard.service.widget.diagram;

import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.model.widget.front.Period;
import ee.stat.dashboard.service.widget.coordinates.dto.MergeResponse;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import jakarta.annotation.Resource;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import static ee.stat.dashboard.AssertUtil._2016_1_1;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class DiagramDateUtilTest {

    static final LocalDate _1990_1_1 = LocalDate.of(1990, 1, 1);
    static final LocalDate _2000_1_1 = LocalDate.of(2000, 1, 1);
    static final LocalDate _2000_2_1 = LocalDate.of(2000, 2, 1);
    static final LocalDate _2000_3_1 = LocalDate.of(2000, 3, 1);
    static final LocalDate _2000_4_1 = LocalDate.of(2000, 4, 1);
    static final LocalDate _2000_5_1 = LocalDate.of(2000, 5, 1);
    static final LocalDate _2000_6_1 = LocalDate.of(2000, 6, 1);
    @Resource
    private DiagramDateUtil diagramDateUtil;

    @Test
    void on_No_Config___period_is_calculated_from_data_end_date_and_periods() {
        Widget widget = monthWidget(null, null, null);
        MergeResponse mergeResponse = data(_2000_3_1, _2000_5_1);
        Period period = diagramDateUtil.buildPeriod(widget, mergeResponse);
        assertEquals(_2000_3_1, period.getFrom());
        assertEquals(_2000_5_1, period.getTo());
        assertEquals(3, monthsBetween(period));
    }

    @Test
    void on_Periods_and_No_Dates___period_is_calculated_from_data_end_date_and_periods() {
        Widget widget = monthWidget(3, null, null);
        MergeResponse mergeResponse = data(_2000_3_1, _2000_5_1);
        Period period = diagramDateUtil.buildPeriod(widget, mergeResponse);
        assertEquals(_2000_3_1, period.getFrom());
        assertEquals(_2000_5_1, period.getTo());
        assertEquals(3, monthsBetween(period));
    }

    @Test
    void on_Periods_and_No_dates___period_is_given_from_data_end_date_and_periods___thus_creating_empty_data() {
        Widget widget = monthWidget(3, null, null);
        MergeResponse mergeResponse = data(_2000_4_1, _2000_5_1);
        Period period = diagramDateUtil.buildPeriod(widget, mergeResponse);
        assertEquals(_2000_3_1, period.getFrom());
        assertEquals(_2000_5_1, period.getTo());
        assertEquals(3, monthsBetween(period));
    }

    @Test
    void on_Periods_and_Start_date___end_period_is_given_from_data_and_periods___but_start_date_can_cut_it() {
        Widget widget = monthWidget(3, _2000_4_1, null);
        MergeResponse mergeResponse = data(_2000_4_1, _2000_5_1);
        Period period = diagramDateUtil.buildPeriod(widget, mergeResponse);
        assertEquals(_2000_4_1, period.getFrom());
        assertEquals(_2000_5_1, period.getTo());
        assertEquals(2, monthsBetween(period));
    }

    @Test
    void on_Periods_and_Start_date___end_period_is_given_from_data_and_periods___but_start_date_can_cut_it_by_adding_no_data_regions() {
        Widget widget = monthWidget(4, _2000_3_1, null);
        MergeResponse mergeResponse = data(_2000_4_1, _2000_5_1);
        Period period = diagramDateUtil.buildPeriod(widget, mergeResponse);
        assertEquals(_2000_3_1, period.getFrom());
        assertEquals(_2000_5_1, period.getTo());
        assertEquals(3, monthsBetween(period));
    }

    @Test
    void on_Periods_and_Start_date___end_period_is_given_from_data_and_periods___but_start_date_can_cut_only_till_data() {
        Widget widget = monthWidget(3, _2000_6_1, null);
        MergeResponse mergeResponse = data(_2000_4_1, _2000_5_1);
        Period period = diagramDateUtil.buildPeriod(widget, mergeResponse);
        assertEquals(_2000_4_1, period.getFrom());
        assertEquals(_2000_5_1, period.getTo());
        assertEquals(2, monthsBetween(period));
    }

    @Test
    void on_Periods_and_Future_End_date___period_is_determined_by_end_date_periods___thus_creating_more_periods() {
        Widget widget = monthWidget(3, null, _2000_6_1);
        MergeResponse mergeResponse = data(_2000_3_1, _2000_5_1);
        Period period = diagramDateUtil.buildPeriod(widget, mergeResponse);
        assertEquals(_2000_3_1, period.getFrom());
        assertEquals(_2000_6_1, period.getTo());
        assertEquals(4, monthsBetween(period));
    }

    @Test
    void on_Periods_and_Past_End_date___period_is_determined_by_end_date_and_periods() {
        Widget widget = monthWidget(3, null, _2000_4_1);
        MergeResponse mergeResponse = data(_2000_1_1, _2000_5_1);
        Period period = diagramDateUtil.buildPeriod(widget, mergeResponse);
        assertEquals(_2000_2_1, period.getFrom());
        assertEquals(_2000_4_1, period.getTo());
        assertEquals(3, monthsBetween(period));
    }

    @Test
    void on_Periods_and_Past_End_date___period_is_determined_by_end_date_and_periods___it_can_have_start_with_data_missing() {
        Widget widget = monthWidget(3, null, _2000_4_1);
        MergeResponse mergeResponse = data(_2000_3_1, _2000_5_1);
        Period period = diagramDateUtil.buildPeriod(widget, mergeResponse);
        assertEquals(_2000_2_1, period.getFrom());
        assertEquals(_2000_4_1, period.getTo());
        assertEquals(3, monthsBetween(period));
    }

    @Test
    void on_Periods_and_Past_End_date_and_Start_date___period_is_determined_by_end_date_and_periods___it_can_have_start_with_data_missing___use_Start_date_to_cut() {
        Widget widget = monthWidget(3, _2000_3_1, _2000_4_1);
        MergeResponse mergeResponse = data(_2000_3_1, _2000_5_1);
        Period period = diagramDateUtil.buildPeriod(widget, mergeResponse);
        assertEquals(_2000_3_1, period.getFrom());
        assertEquals(_2000_4_1, period.getTo());
        assertEquals(2, monthsBetween(period));
    }

    @Test
    void on_Periods_and_Past_End_date_and_Start_date___period_is_determined_by_end_date_and_periods___it_can_have_start_with_data_missing___use_Start_date_to_cut___not_to_generate_longer_period() {
        Widget widget = monthWidget(3, _1990_1_1, _2000_4_1);
        MergeResponse mergeResponse = data(_2000_3_1, _2000_5_1);
        Period period = diagramDateUtil.buildPeriod(widget, mergeResponse);
        assertEquals(_2000_2_1, period.getFrom());
        assertEquals(_2000_4_1, period.getTo());
        assertEquals(3, monthsBetween(period));
    }

    @Test
    void on_No_Periods_Start_and_End_Date___Period_is_determined_by_start_and_end_date___even_if_there_is_no_data_future_periods() {
        Widget widget = monthWidget(null, _2000_4_1, _2000_6_1);
        MergeResponse mergeResponse = data(_2000_1_1, _2000_3_1);
        Period period = diagramDateUtil.buildPeriod(widget, mergeResponse);
        assertEquals(_2000_4_1, period.getFrom());
        assertEquals(_2000_6_1, period.getTo());
        assertEquals(3, monthsBetween(period));
    }

    @Test
    void on_No_Periods_Start_and_End_Date___Period_is_determined_by_start_and_end_date___even_if_there_is_no_data_past_periods() {
        Widget widget = monthWidget(null, _2000_1_1, _2000_3_1);
        MergeResponse mergeResponse = data(_2000_4_1, _2000_6_1);
        Period period = diagramDateUtil.buildPeriod(widget, mergeResponse);
        assertEquals(_2000_1_1, period.getFrom());
        assertEquals(_2000_3_1, period.getTo());
        assertEquals(3, monthsBetween(period));
    }

    @Test
    void on_No_Periods_and_End_Date___Period_is_determined_by_end_date_and_data_start_date() {
        Widget widget = monthWidget(null, null, _2000_6_1);
        MergeResponse mergeResponse = data(_2000_1_1, _2000_3_1);
        Period period = diagramDateUtil.buildPeriod(widget, mergeResponse);
        assertEquals(_2000_1_1, period.getFrom());
        assertEquals(_2000_6_1, period.getTo());
        assertEquals(6, monthsBetween(period));
    }

    @Test
    void on_No_Periods_and_End_Date___Period_is_determined_by_end_date_and_data_start_date___even_by_creating_null_period() {
        Widget widget = monthWidget(null, null, _2000_3_1);
        MergeResponse mergeResponse = data(_2000_4_1, _2000_6_1);
        Period period = diagramDateUtil.buildPeriod(widget, mergeResponse);
        assertEquals(_2000_3_1, period.getFrom());
        assertEquals(_2000_3_1, period.getTo());
        assertEquals(1, monthsBetween(period));
    }

    @Test
    void on_No_Periods_and_Start_Date___Period_is_determined_by_data() {
        Widget widget = monthWidget(null, _2000_3_1, null);
        MergeResponse mergeResponse = data(_2000_4_1, _2000_6_1);
        Period period = diagramDateUtil.buildPeriod(widget, mergeResponse);
        assertEquals(_2000_4_1, period.getFrom());
        assertEquals(_2000_6_1, period.getTo());
        assertEquals(3, monthsBetween(period));
    }

    @Test
    void one_period_date_is_same_to_date() {
        assertEquals(_2016_1_1, diagramDateUtil.generateStartDateFromPeriod(_2016_1_1, 1, TimePeriod.YEAR));
        assertEquals(_2016_1_1, diagramDateUtil.generateStartDateFromPeriod(_2016_1_1, 1, TimePeriod.MONTH));
        assertEquals(_2016_1_1, diagramDateUtil.generateStartDateFromPeriod(_2016_1_1, 1, TimePeriod.QUARTER));
    }

    private Widget monthWidget(Integer periods, LocalDate start, LocalDate end) {
        Widget widget = new Widget();
        widget.setPeriods(periods);
        widget.setStartDate(start);
        widget.setEndDate(end);
        widget.setTimePeriod(TimePeriod.MONTH);
        return widget;
    }

    private MergeResponse data(LocalDate min, LocalDate max) {
        MergeResponse mergeResponse = new MergeResponse();
        mergeResponse.setMinDate(min);
        mergeResponse.setMaxDate(max);
        return mergeResponse;
    }

    private long monthsBetween(Period period) {
        return ChronoUnit.MONTHS.between(period.getFrom(), period.getTo()) + 1;
    }
}
