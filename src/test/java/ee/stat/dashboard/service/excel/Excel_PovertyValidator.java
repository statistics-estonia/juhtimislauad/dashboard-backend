package ee.stat.dashboard.service.excel;

import ee.stat.dashboard.model.json.CoordinatedResponse;
import ee.stat.dashboard.model.json.CoordinatedResponses;
import ee.stat.dashboard.model.json.DataPoint;
import ee.stat.dashboard.model.json.Source;
import ee.stat.dashboard.model.widget.back.enums.Language;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;

import static ee.stat.dashboard.AssertUtil.MALES;
import static ee.stat.dashboard.AssertUtil.TOTAL;
import static ee.stat.dashboard.AssertUtil._0to17;
import static ee.stat.dashboard.AssertUtil._18to64;
import static ee.stat.dashboard.AssertUtil._2016_1_1;
import static ee.stat.dashboard.AssertUtil._2018_1_1;
import static ee.stat.dashboard.AssertUtil._2019_1_1;
import static ee.stat.dashboard.AssertUtil._2023_1_1;
import static ee.stat.dashboard.AssertUtil._65p;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Excel_PovertyValidator {

    public static void validateAbsPovertyReality(Language lang, CoordinatedResponses convert) {
        List<CoordinatedResponse> converted = convert.getResponses();
        assertEquals(1, converted.size());
        CoordinatedResponse response = converted.get(0);
        validateReality(response, lang);
    }

    public static void validateAbsPovertyExpectations(Language lang, CoordinatedResponses convert) {
        List<CoordinatedResponse> converted = convert.getResponses();
        assertEquals(1, converted.size());
        CoordinatedResponse response = converted.get(0);
        validateExpectations(response, lang);
    }

    public static void validateAbsPovertyExpectationsAndReality(Language lang, CoordinatedResponses convert) {
        List<CoordinatedResponse> converted = convert.getResponses();
        assertEquals(2, converted.size());
        validateExpectations(converted.get(0), lang);
        validateReality(converted.get(1), lang);
    }

    public static void validateExpectations(CoordinatedResponse response, Language lang) {
        assertEquals(Source.EXCEL_TARGET, response.getSource());

        List<DataPoint> observations2 = response.getDataPoints();
        assertEquals(72, observations2.size());
        validate(observations2.get(0), 4.1, _0to17, MALES.name(lang), _2016_1_1);
        validate(observations2.get(27), 4.2, _18to64, MALES.name(lang), _2019_1_1);
        validate(observations2.get(71), 0.7, _65p, TOTAL.name(lang), _2023_1_1);
    }

    public static void validateMergeReality(CoordinatedResponse response, Language lang) {
        assertEquals(Source.EXCEL_REALITY, response.getSource());

        List<DataPoint> observations2 = response.getDataPoints();
        assertEquals(27, observations2.size());
        List<DataPoint> collect2 = observations2.stream().sorted(Comparator.comparing(DataPoint::getDate)).collect(toList());
        validate(collect2.get(0), 1.1, _0to17, MALES.name(lang), _2016_1_1);
        validate(collect2.get(26), 9.3, _65p, TOTAL.name(lang), _2018_1_1);
    }

    public static void validateMergeRealityWithoutSize(CoordinatedResponse response, Language lang) {
        List<DataPoint> observations2 = response.getDataPoints();
        List<DataPoint> collect2 = observations2.stream().sorted(Comparator.comparing(DataPoint::getDate)).collect(toList());
        validate(collect2.get(0), 1.1, _0to17, MALES.name(lang), _2016_1_1);
        validate(collect2.get(26), 9.3, _65p, TOTAL.name(lang), _2018_1_1);
    }

    private static void validateReality(CoordinatedResponse response, Language lang) {
        assertEquals(Source.EXCEL_REALITY, response.getSource());

        List<DataPoint> observations2 = response.getDataPoints();
        assertEquals(71, observations2.size());
        List<DataPoint> collect2 = observations2.stream().sorted(Comparator.comparing(DataPoint::getValue)).collect(toList());
        validate(collect2.get(0), 1.1, _0to17, MALES.name(lang), _2016_1_1);
        validate(collect2.get(70), 9.8, _65p, TOTAL.name(lang), _2023_1_1);
    }

    public static void validate(DataPoint obs, double value, String ageGroup, String sex, LocalDate date) {
        assertEquals(BigDecimal.valueOf(value), obs.getValue());
        assertEquals(ageGroup, obs.getVal1());
        assertEquals(sex, obs.getVal2());
        assertEquals(date, obs.getDate());
    }

}
