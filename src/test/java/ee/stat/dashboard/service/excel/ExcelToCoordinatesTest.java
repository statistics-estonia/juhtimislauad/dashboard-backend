package ee.stat.dashboard.service.excel;

import ee.stat.dashboard.model.widget.back.excel.Excel;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlDimension;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlValue;
import ee.stat.dashboard.service.widget.coordinates.ExcelToCoordinates;
import ee.stat.dashboard.service.widget.coordinates.exception.ExcelImportException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static ee.stat.dashboard.AssertUtil.FEMALES;
import static ee.stat.dashboard.AssertUtil.MALES;
import static ee.stat.dashboard.AssertUtil.StatName;
import static ee.stat.dashboard.AssertUtil.TOTAL;
import static ee.stat.dashboard.AssertUtil._0to17;
import static ee.stat.dashboard.AssertUtil._18to64;
import static ee.stat.dashboard.AssertUtil._65p;
import static ee.stat.dashboard.model.widget.back.enums.ExcelType.DATA;
import static ee.stat.dashboard.model.widget.back.enums.Language.EN;
import static ee.stat.dashboard.model.widget.back.enums.Language.ET;
import static ee.stat.dashboard.service.excel.Excel_PovertyValidator.validateAbsPovertyExpectations;
import static ee.stat.dashboard.service.excel.Excel_PovertyValidator.validateAbsPovertyExpectationsAndReality;
import static ee.stat.dashboard.service.excel.Excel_PovertyValidator.validateAbsPovertyReality;

@SpringBootTest
class ExcelToCoordinatesTest {

    @Autowired
    private ExcelToCoordinates converter;

    @Test
    void convert_from_data_reality_only() throws ExcelImportException {
        Excel excel = new Excel("./excel/data/abs_poverty_reality.xlsx", DATA);
        validateAbsPovertyReality(ET, converter.convert(excel, ET, null));
        validateAbsPovertyReality(EN, converter.convert(excel, EN, null));
    }

    @Test
    void convert_from_data_expectations_only() throws ExcelImportException {
        Excel excel = new Excel("./excel/data/abs_poverty_expect.xlsx", DATA);
        validateAbsPovertyExpectations(ET, converter.convert(excel, ET, null));
        validateAbsPovertyExpectations(EN, converter.convert(excel, EN, null));
    }

    @Test
    void convert_from_data_expectations_only_validating_dimensions_and_values() throws ExcelImportException {
        Excel excel = new Excel("./excel/data/abs_poverty_expect.xlsx", DATA);
        XmlDimension dim1 = dimension("Vanuserühm", "Age group",
                List.of(value(_0to17), value(_18to64), value(_65p)));
        XmlDimension dim2 = dimension("Sugu", "Sex",
                List.of(value(MALES),value(FEMALES), value(TOTAL)));
        validateAbsPovertyExpectations(ET, converter.convert(excel, ET, List.of(dim1, dim2)));
        validateAbsPovertyExpectations(EN, converter.convert(excel, EN, List.of(dim1, dim2)));
    }

    @Test
    void convert_from_data_expectations_and_reality() throws ExcelImportException {
        Excel excel = new Excel("./excel/data/abs_poverty_expect_and_reality.xlsx", DATA);
        validateAbsPovertyExpectationsAndReality(ET, converter.convert(excel, ET, null));
        validateAbsPovertyExpectationsAndReality(EN, converter.convert(excel, EN, null));
    }

    private XmlDimension dimension(String nameEt, String nameEn, List<XmlValue> values) {
        XmlDimension xmlDimension = new XmlDimension();
        xmlDimension.setNameEt(nameEt);
        xmlDimension.setNameEn(nameEn);
        xmlDimension.setValues(values);
        return xmlDimension;
    }

    private XmlValue value(StatName name) {
        return value(name.name(ET), name.name(EN));
    }

    private XmlValue value(String val) {
        return value(val, val);
    }

    private XmlValue value(String valEt, String valEn) {
        XmlValue value = new XmlValue();
        value.setValueEt(valEt);
        value.setValueEn(valEn);
        return value;
    }
}
