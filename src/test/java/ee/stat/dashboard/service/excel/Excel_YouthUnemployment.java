package ee.stat.dashboard.service.excel;

import ee.stat.dashboard.model.json.CoordinatedResponse;
import ee.stat.dashboard.model.json.CoordinatedResponses;
import ee.stat.dashboard.model.json.DataPoint;
import ee.stat.dashboard.model.json.Source;
import ee.stat.dashboard.model.widget.back.enums.Language;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static ee.stat.dashboard.AssertUtil.FEMALES;
import static ee.stat.dashboard.AssertUtil.MALES_AND_FEMALES;
import static ee.stat.dashboard.AssertUtil._2015_1_1;
import static ee.stat.dashboard.AssertUtil._2022_1_1;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Excel_YouthUnemployment {

    public static void validateYouthUnemployment(Language lang, CoordinatedResponses convert) {
        List<CoordinatedResponse> converted = convert.getResponses();
        assertEquals(1, converted.size());
        CoordinatedResponse response = converted.get(0);
        validateExpectations(response, lang);
    }

    private static void validateExpectations(CoordinatedResponse response, Language lang) {
        assertEquals(Source.EXCEL_TARGET, response.getSource());

        List<DataPoint> observations2 = response.getDataPoints();
        assertEquals(24, observations2.size());
        validate(observations2.get(0), 13.0, MALES_AND_FEMALES.name(lang), null, _2015_1_1);
        validate(observations2.get(23), 8.8, FEMALES.name(lang), null, _2022_1_1);
    }

    private static void validate(DataPoint obs, double value, String ageGroup, String sex, LocalDate date) {
        assertEquals(BigDecimal.valueOf(value), obs.getValue());
        assertEquals(ageGroup, obs.getVal1());
        assertEquals(sex, obs.getVal2());
        assertEquals(date, obs.getDate());
    }

}
