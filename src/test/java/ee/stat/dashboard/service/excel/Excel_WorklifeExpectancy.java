package ee.stat.dashboard.service.excel;

import ee.stat.dashboard.model.json.CoordinatedResponse;
import ee.stat.dashboard.model.json.CoordinatedResponses;
import ee.stat.dashboard.model.json.DataPoint;
import ee.stat.dashboard.model.json.Source;
import ee.stat.dashboard.model.widget.back.enums.Language;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static ee.stat.dashboard.AssertUtil.FEMALES;
import static ee.stat.dashboard.AssertUtil.MALES_AND_FEMALES;
import static ee.stat.dashboard.AssertUtil._2013_1_1;
import static ee.stat.dashboard.AssertUtil._2014_1_1;
import static ee.stat.dashboard.AssertUtil._2017_1_1;
import static ee.stat.dashboard.AssertUtil._2023_1_1;
import static ee.stat.dashboard.AssertUtil.first;
import static ee.stat.dashboard.AssertUtil.last;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Excel_WorklifeExpectancy {

    public static void validateWorkLifeExpectancy(Language lang, CoordinatedResponses convert) {
        List<CoordinatedResponse> converted = convert.getResponses();
        assertEquals(2, converted.size());
        CoordinatedResponse response = converted.get(0);
        CoordinatedResponse response2 = converted.get(1);
        validateExpectations(response, lang);
        validateReality(response2, lang);
    }

    private static void validateExpectations(CoordinatedResponse response, Language lang) {
        assertEquals(Source.EXCEL_TARGET, response.getSource());

        List<DataPoint> observations2 = response.getDataPoints();
        assertEquals(30, observations2.size());
        validate(first(observations2), 36.646, MALES_AND_FEMALES.name(lang), null, _2014_1_1);
        validate(last(observations2), 37.6308167270603, FEMALES.name(lang), null, _2023_1_1);
    }

    private static void validateReality(CoordinatedResponse response, Language lang) {
        assertEquals(Source.EXCEL_REALITY, response.getSource());

        List<DataPoint> observations2 = response.getDataPoints();
        assertEquals(15, observations2.size());
        validate(first(observations2), 36.5, MALES_AND_FEMALES.name(lang), null, _2013_1_1);
        validate(last(observations2), 37.5, FEMALES.name(lang), null, _2017_1_1);
    }

    private static void validate(DataPoint obs, double value, String val1, String val2, LocalDate date) {
        assertEquals(BigDecimal.valueOf(value), obs.getValue());
        assertEquals(val1, obs.getVal1());
        assertEquals(val2, obs.getVal2());
        assertEquals(date, obs.getDate());
    }

}

