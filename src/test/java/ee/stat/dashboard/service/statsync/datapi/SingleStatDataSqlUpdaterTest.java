package ee.stat.dashboard.service.statsync.datapi;

import ee.stat.dashboard.model.widget.back.statdataapi.StatDataApiData;
import ee.stat.dashboard.service.client.dataapi.StatDataApiResponse;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SingleStatDataSqlUpdaterTest {

    @Test
    void data_not_changed() {
        StatDataApiData data = new StatDataApiData();
        data.setResponseEn("lala : en");
        data.setResponseEt("lala : et");

        StatDataApiResponse newData = new StatDataApiResponse();
        newData.setResponseEn("lala : en");
        newData.setResponseEt("lala : et");

        assertFalse(SingleStatDataApiUpdater.dataChanged(data, newData));
    }

    @Test
    void data_changed() {
        StatDataApiData data = new StatDataApiData();
        data.setResponseEn("lala : en");
        data.setResponseEt("lala : et");

        StatDataApiResponse newData = new StatDataApiResponse();
        newData.setResponseEn("lala : en");
        newData.setResponseEt("lala : et2");

        assertTrue(SingleStatDataApiUpdater.dataChanged(data, newData));
    }
}
