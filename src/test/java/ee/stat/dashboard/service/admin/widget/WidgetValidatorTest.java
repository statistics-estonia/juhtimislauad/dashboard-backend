package ee.stat.dashboard.service.admin.widget;

import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import jakarta.annotation.Resource;
import java.time.LocalDate;

import static ee.stat.dashboard.model.widget.back.enums.TimePeriod.MONTH;
import static ee.stat.dashboard.model.widget.back.enums.TimePeriod.QUARTER;
import static ee.stat.dashboard.model.widget.back.enums.TimePeriod.WEEK;
import static ee.stat.dashboard.model.widget.back.enums.TimePeriod.YEAR;
import static ee.stat.dashboard.util.StatDateUtilTest._2000_1_1;
import static ee.stat.dashboard.util.StatDateUtilTest._2000_1_2;
import static ee.stat.dashboard.util.StatDateUtilTest._2000_1_WEEK;
import static ee.stat.dashboard.util.StatDateUtilTest._2000_2_1;
import static ee.stat.dashboard.util.StatDateUtilTest._2000_4_1;
import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class WidgetValidatorTest {

    @Resource
    private WidgetValidator widgetValidator;

    @Test
    void null_date() {
        expectSuccess(YEAR, null);
        expectSuccess(QUARTER, null);
        expectSuccess(MONTH, null);
        expectSuccess(WEEK, null);
    }

    @Test
    void first_date_of_the_year() {
        expectSuccess(YEAR, _2000_1_1);
        expectSuccess(QUARTER, _2000_1_1);
        expectSuccess(MONTH, _2000_1_1);
        expectFailure(WEEK, _2000_1_1);
        expectSuccess(WEEK, _2000_1_WEEK);
    }

    @Test
    void second_date_of_the_year() {
        expectFailure(YEAR, _2000_1_2);
        expectFailure(QUARTER, _2000_1_2);
        expectFailure(MONTH, _2000_1_2);
        expectFailure(WEEK, _2000_1_2);
    }

    @Test
    void first_feb() {
        expectFailure(YEAR, _2000_2_1);
        expectFailure(QUARTER, _2000_2_1);
        expectSuccess(MONTH, _2000_2_1);
        expectFailure(WEEK, _2000_2_1);
    }

    @Test
    void first_quarter() {
        expectFailure(YEAR, _2000_4_1);
        expectSuccess(QUARTER, _2000_4_1);
        expectSuccess(MONTH, _2000_4_1);
        expectFailure(WEEK, _2000_4_1);
    }

    @Test
    void random_monday() {
        LocalDate _2000_5_29 = LocalDate.of(2000, 5, 29);
        expectFailure(YEAR, _2000_5_29);
        expectFailure(QUARTER, _2000_5_29);
        expectFailure(MONTH, _2000_5_29);
        expectSuccess(WEEK, _2000_5_29);
    }

    private void expectSuccess(TimePeriod period, LocalDate date) {
        expectSuccess(widget(period, date, null));
        expectSuccess(widget(period, null, date));
        expectSuccess(widget(period, date, date));
    }

    private void expectFailure(TimePeriod period, LocalDate date) {
        expectFailure(widget(period, date, null));
        expectFailure(widget(period, null, date));
        expectFailure(widget(period, date, date));
    }

    private void expectSuccess(WidgetAdminResponse widget) {
        try {
            widgetValidator.validateTimePeriodDates(widget);
        } catch (Exception e) {
            fail(format("expected success, but failed instead %s", e.getMessage()));
        }
    }

    private void expectFailure(WidgetAdminResponse widget) {
        try {
            widgetValidator.validateTimePeriodDates(widget);
            fail("expected failure, but succeeded instead");
        } catch (Exception ignored) {
        }
    }

    private WidgetAdminResponse widget(TimePeriod period, LocalDate startDate, LocalDate endDate) {
        WidgetAdminResponse response = new WidgetAdminResponse();
        response.setTimePeriod(period);
        response.setStartDate(startDate);
        response.setEndDate(endDate);
        return response;
    }
}
