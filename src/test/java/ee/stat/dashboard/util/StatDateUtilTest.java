package ee.stat.dashboard.util;

import ee.stat.dashboard.model.dashboard.Quarter;
import ee.stat.dashboard.model.dashboard.Week;
import ee.stat.dashboard.service.widget.coordinates.exception.TimePartCalculationException;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.Year;
import java.util.List;

import static ee.stat.dashboard.AssertUtil.first;
import static ee.stat.dashboard.AssertUtil.last;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.util.StatDateUtil.firstWeekByYear;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

public class StatDateUtilTest {

    public static final LocalDate _2000_1_1 = LocalDate.of(2000, 1, 1);
    public static final LocalDate _2000_1_2 = LocalDate.of(2000, 1, 2);
    public static final LocalDate _2000_2_1 = LocalDate.of(2000, 2, 1);
    public static final LocalDate _2000_4_1 = LocalDate.of(2000, 4, 1);
    public static final LocalDate _2000_1_WEEK = firstWeekByYear(2000);
    public static final LocalDate _2001_1_WEEK = firstWeekByYear(2001);
    public static final LocalDate _2004_1_1 = LocalDate.of(2004, 1, 1);
    public static final LocalDate _2004_1_WEEK = firstWeekByYear(2004);
    public static final LocalDate _2005_1_WEEK = firstWeekByYear(2005);
    public static final Year YEAR_2000 = Year.of(2000);
    public static final Year YEAR_2004 = Year.of(2004);

    @Test
    void latest_date_is_most_recent_date_localdatetime() {
        LocalDateTime date1 = LocalDateTime.of(2000, 10, 10, 10, 10);
        LocalDateTime date2 = LocalDateTime.of(2000, 10, 10, 10, 11);
        assertEquals(date2, StatDateUtil.getLatest(date1, date2));
        assertEquals(date2, StatDateUtil.getLatest(null, date2));
        assertEquals(date1, StatDateUtil.getLatest(date1, null));
    }

    @Test
    void latest_date_is_most_recent_date_localdate() {
        LocalDate date1 = LocalDate.of(2000, 10, 10);
        LocalDate date2 = LocalDate.of(2000, 10, 11);
        assertEquals(date2, StatDateUtil.getLatest(date1, date2));
        assertEquals(date2, StatDateUtil.getLatest(null, date2));
        assertEquals(date1, StatDateUtil.getLatest(date1, null));
    }

    @Test
    void to_date_is_inclusive() {
        List<LocalDate> localDates = StatDateUtil.generateDatesYear(_2000_1_1, _2000_1_1);
        list_size(localDates, 1);
    }

    @Test
    void _4_years_generate_5_yearly_dates() {
        List<LocalDate> localDates = StatDateUtil.generateDatesYear(_2000_1_1, _2004_1_1);
        list_size(localDates, 4 + 1);
        assertEquals(_2000_1_1, first(localDates));
        assertEquals(_2004_1_1, last(localDates));
    }

    @Test
    void _4_years_generate_209_weeks_on_52_week_year() {
        List<LocalDate> localDates = StatDateUtil.generateDatesWeek(_2000_1_WEEK, _2004_1_WEEK);
        list_size(localDates, 4 * 52 + 1);
        assertEquals(_2000_1_WEEK, first(localDates));
        assertEquals(_2004_1_WEEK, last(localDates));
    }

    @Test
    void _4_years_generate_210_weeks_on_53_week_year() {
        List<LocalDate> localDates = StatDateUtil.generateDatesWeek(_2001_1_WEEK, _2005_1_WEEK);
        list_size(localDates, 3 * 52 + 53 + 1);
        assertEquals(_2001_1_WEEK, first(localDates));
        assertEquals(_2005_1_WEEK, last(localDates));
    }

    @Test
    void _4_years_generate_49_month_dates() {
        List<LocalDate> localDates = StatDateUtil.generateDatesMonth(_2000_1_1, _2004_1_1);
        list_size(localDates, 4 * 12 + 1);
        assertEquals(_2000_1_1, first(localDates));
        assertEquals(_2004_1_1, last(localDates));
    }

    @Test
    void _4_years_generate_17_quarter_dates() {
        List<LocalDate> localDates = StatDateUtil.generateDatesQuarter(_2000_1_1, _2004_1_1);
        list_size(localDates, 4 * 4 + 1);
        assertEquals(_2000_1_1, first(localDates));
        assertEquals(_2004_1_1, last(localDates));
    }

    @Test
    void _4_years_generate_9_semester_dates() {
        List<LocalDate> localDates = StatDateUtil.generateDatesSemester(_2000_1_1, _2004_1_1);
        list_size(localDates, 4 * 2 + 1);
        assertEquals(_2000_1_1, first(localDates));
        assertEquals(_2004_1_1, last(localDates));
    }

    @Test
    void previous_years_are_years() {
        LocalDate localDate = StatDateUtil.beforeDateYear(_2000_1_1, 1);
        assertEquals(LocalDate.of(1999, 1, 1), localDate);
        LocalDate localDate2 = StatDateUtil.beforeDateYear(_2000_1_1, 2);
        assertEquals(LocalDate.of(1998, 1, 1), localDate2);
        LocalDate localDate3 = StatDateUtil.beforeDateYear(_2000_1_1, 3);
        assertEquals(LocalDate.of(1997, 1, 1), localDate3);
    }

    @Test
    void previous_weeks_are_weeks() {
        LocalDate localDate = StatDateUtil.beforeDateWeek(_2000_1_WEEK, 1);
        assertEquals(LocalDate.of(1999, 12, 27), localDate);
        LocalDate localDate2 = StatDateUtil.beforeDateWeek(_2000_1_WEEK, 2);
        assertEquals(LocalDate.of(1999, 12, 20), localDate2);
        LocalDate localDate3 = StatDateUtil.beforeDateWeek(_2000_1_WEEK, 3);
        assertEquals(LocalDate.of(1999, 12, 13), localDate3);
    }

    @Test
    void previous_months_are_months() {
        LocalDate localDate = StatDateUtil.beforeDateMonth(_2000_1_1, 1);
        assertEquals(LocalDate.of(1999, 12, 1), localDate);
        LocalDate localDate2 = StatDateUtil.beforeDateMonth(_2000_1_1, 2);
        assertEquals(LocalDate.of(1999, 11, 1), localDate2);
        LocalDate localDate3 = StatDateUtil.beforeDateMonth(_2000_1_1, 3);
        assertEquals(LocalDate.of(1999, 10, 1), localDate3);
    }

    @Test
    void previous_quarters_are_quarters() {
        LocalDate localDate = StatDateUtil.beforeDateQuarter(_2000_1_1, 1);
        assertEquals(LocalDate.of(1999, 10, 1), localDate);
        LocalDate localDate2 = StatDateUtil.beforeDateQuarter(_2000_1_1, 2);
        assertEquals(LocalDate.of(1999, 7, 1), localDate2);
        LocalDate localDate3 = StatDateUtil.beforeDateQuarter(_2000_1_1, 3);
        assertEquals(LocalDate.of(1999, 4, 1), localDate3);
    }

    @Test
    void previous_semesters_are_semesters() {
        LocalDate localDate = StatDateUtil.beforeDateSemester(_2000_1_1, 1);
        assertEquals(LocalDate.of(1999, 7, 1), localDate);
        LocalDate localDate2 = StatDateUtil.beforeDateSemester(_2000_1_1, 2);
        assertEquals(LocalDate.of(1999, 1, 1), localDate2);
        LocalDate localDate3 = StatDateUtil.beforeDateSemester(_2000_1_1, 3);
        assertEquals(LocalDate.of(1998, 7, 1), localDate3);
    }

    @Test
    void build_quarter_date() {
        assertNull(StatDateUtil.buildQuarterDate(null, null));
        assertNull(StatDateUtil.buildQuarterDate(YEAR_2000, null));
        assertNull(StatDateUtil.buildQuarterDate(null, Quarter.I));
        LocalDate localDate = StatDateUtil.buildQuarterDate(YEAR_2000, Quarter.I);
        assertEquals(LocalDate.of(2000, 1, 1), localDate);
        LocalDate localDate2 = StatDateUtil.buildQuarterDate(YEAR_2000, Quarter.II);
        assertEquals(LocalDate.of(2000, 4, 1), localDate2);
    }

    @Test
    void build_month_date() {
        assertNull(StatDateUtil.buildMonthDate(null, null));
        assertNull(StatDateUtil.buildMonthDate(YEAR_2000, null));
        assertNull(StatDateUtil.buildMonthDate(null, Month.JANUARY));
        LocalDate localDate = StatDateUtil.buildMonthDate(YEAR_2000, Month.JANUARY);
        assertEquals(LocalDate.of(2000, 1, 1), localDate);
        LocalDate localDate2 = StatDateUtil.buildMonthDate(YEAR_2000, Month.FEBRUARY);
        assertEquals(LocalDate.of(2000, 2, 1), localDate2);
    }

    @Test
    void build_week_date() throws TimePartCalculationException {
        assertNull(StatDateUtil.buildWeekDate(null, null));
        assertNull(StatDateUtil.buildWeekDate(YEAR_2000, null));
        assertNull(StatDateUtil.buildWeekDate(null, Week._1));
        assertEquals(LocalDate.of(2000, 1, 3), StatDateUtil.buildWeekDate(YEAR_2000, Week._1));
        assertEquals(LocalDate.of(2000, 1, 10), StatDateUtil.buildWeekDate(YEAR_2000, Week._2));
        assertEquals(LocalDate.of(2000, 12, 25), StatDateUtil.buildWeekDate(YEAR_2000, Week._52));
        assertEquals(LocalDate.of(2004, 12, 20), StatDateUtil.buildWeekDate(YEAR_2004, Week._52));
        try {
            StatDateUtil.buildWeekDate(YEAR_2000, Week._53);
            fail("Should have error");
        } catch (TimePartCalculationException e) {
            assertEquals("Year 2000 does not have week 53", e.getMessage());
        }
        assertEquals(LocalDate.of(2004, 12, 27), StatDateUtil.buildWeekDate(YEAR_2004, Week._53));
    }
}
