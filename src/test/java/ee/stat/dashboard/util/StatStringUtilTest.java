package ee.stat.dashboard.util;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import static ee.stat.dashboard.util.StatStringUtil.CUT_NOTICE;
import static ee.stat.dashboard.util.StatStringUtil.cleanUp;
import static ee.stat.dashboard.util.StatStringUtil.cutTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class StatStringUtilTest {

    @Test
    void cleanUp_removes_left_dots_and_trims_string() {
        assertEquals("H.......aapsalu", cleanUp("H.......aapsalu"));
        assertEquals("Haapsalu", cleanUp("..Haapsalu"));
        assertEquals("Haapsalu", cleanUp("  ..Haapsalu"));
        assertEquals("Haapsalu", cleanUp("Haapsalu"));
        assertEquals("Haapsalu", cleanUp("....Haapsalu"));
        assertEquals("Haapsalu", cleanUp(".......Haapsalu"));
        assertEquals("Haapsalu", cleanUp("  Haapsalu   "));

        assertEquals("1. klass", cleanUp("1. klass"));
        assertEquals("1. klass", cleanUp("..1. klass"));
        assertEquals("1. klass", cleanUp("  ..1. klass"));
        assertEquals("1. klass..", cleanUp("..1. klass.."));
    }

    @Test
    void cut_to_cuts_string() {
        String testString = StringUtils.repeat("A", 2000);
        String resultString = cutTo(testString, 200);
        assertEquals(200, resultString.length());
        assertTrue(resultString.endsWith(CUT_NOTICE));
    }

    @Test
    void cut_doesnt_but_short_string() {
        String testString = StringUtils.repeat("A", 187);
        String resultString = cutTo(testString, 200);
        assertEquals(187, resultString.length());
        assertTrue(resultString.endsWith("A"));
    }
}
