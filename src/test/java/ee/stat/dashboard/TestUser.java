package ee.stat.dashboard;

import lombok.Value;

@Value
public class TestUser {

    String username;
    String token;
    String fullname;
}
