package ee.stat.dashboard;

import ee.stat.dashboard.common.TestFilter;
import ee.stat.dashboard.controller.Response;
import ee.stat.dashboard.controller.user.dto.UserData;
import ee.stat.dashboard.model.json.Source;
import ee.stat.dashboard.model.user.user.AppUser;
import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.front.FilterSerie;
import ee.stat.dashboard.model.widget.front.Serie;
import ee.stat.dashboard.service.admin.dashboard.dto.DashboardAdminResponse;
import ee.stat.dashboard.service.admin.dto.StatPage;
import ee.stat.dashboard.service.admin.process.dto.ProcessLogDto;
import ee.stat.dashboard.service.admin.process.dto.StatLogDto;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import ee.stat.dashboard.service.dashboard.dto.DashboardResponse;
import ee.stat.dashboard.service.dashboard.dto.RoleDashboardResponse;
import ee.stat.dashboard.service.element.dto.ClassifierResponse;
import ee.stat.dashboard.service.element.dto.DomainResponse;
import ee.stat.dashboard.service.element.dto.ElementResponse;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlDimension;
import ee.stat.dashboard.service.widget.widget.dto.FilterDto;
import ee.stat.dashboard.service.widget.widget.dto.FilterValueDto;
import ee.stat.dashboard.service.widget.widget.dto.GraphDto;
import ee.stat.dashboard.service.widget.widget.dto.UserWidgetDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static ee.stat.dashboard.AssertUtil.StatName.name;
import static ee.stat.dashboard.model.json.Source.STAT_DATA_API;
import static ee.stat.dashboard.model.widget.back.enums.MapType.KOV;
import static ee.stat.dashboard.model.widget.back.enums.MapType.MK;
import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.time.LocalDate.of;
import static java.util.stream.Collectors.toList;
import static lombok.AccessLevel.PRIVATE;
import static org.apache.commons.lang3.BooleanUtils.isTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.util.StreamUtils.copyToString;

public class AssertUtil {

    public static final String ACCOMODATION = "accomodation";
    public static final String LES01 = "LES01";
    public static final String RAA0012 = "RAA0012";
    public static final String PA001 = "PA001";
    public static final String PA006 = "PA006";
    public static final String TU122 = "TU122";
    public static final String TT065 = "TT065";
    public static final String TU51 = "TU51";
    public static final String EKS21 = "EKS21";
    public static final String EKS31 = "EKS31";
    public static final String VKT14 = "VKT14";
    public static final long DASHBOARD_1 = 1;
    public static final long DASHBOARD_2 = 2;
    public static final long DASHBOARD_3 = 3;
    public static final long DASHBOARD_4 = 4;
    public static final int NR_OF_EHAK_ELEMENTS = 95;
    public static final int NR_OF_ROLE_CLASSIFIERS = 3;
    public static final TestUser USER_REGULAR = new TestUser("user", "user_token", "User Regular");
    public static final TestUser USER_ADMIN = new TestUser("admin", "admin_token", "Admin Regular");

    public static final StatName ACTIVITY = name("Tegevusala", "Economic activity");
    public static final StatName ADJUSTMENT = name("Sesoonne korrigeerimine", "Adjustment");
    public static final StatName ADJUSTMENT_DONE = name("Sesoonselt ja tööpäevade arvuga korrigeeritud", "Seasonally and working day adjusted");
    public static final StatName ADJUSTMENT_SKIPPED = name("Sesoonselt ja tööpäevade arvuga korrigeerimata", "Seasonally and working day unadjusted");
    public static final StatName AGE = name("Vanuserühm", "Age");
    public static final StatName AGEGROUPS_TOGETHER = name("Vanuserühmad kokku", "Age groups total");
    public static final StatName AGE_GROUP = name("Vanuserühm", "Age group");
    public static final StatName AGRICULTURE = name("Põllumajandus, metsamajandus ja kalapüük", "Agriculture, forestry and fishing");
    public static final StatName ANIJA_MUNICIPALITY = name("Anija vald", "Anija rural municipality");
    public static final StatName AREA_REGION = name("Piirkond/Haldusüksus", "Region/Administrative unit");
    public static final StatName BALANCE = name("Bilanss", "Balance");
    public static final StatName BUSINESS_TRIP = name("Tööreis", "Professional, business");
    public static final StatName CLIENT_GROUP = name("Kliendigrupp", "Customer group");
    public static final StatName CLIENT_HOME = name("Koduklient", "Residential customer");
    public static final StatName COUNTY = name("Maakond", "County");
    public static final StatName EXPORT = name("Eksport", "Export");
    public static final StatName FEMALES = name("Naised", "Females");
    public static final StatName FLOW = name("voog", "flow");
    public static final StatName HARJU_COUNTY = name("HARJU MAAKOND", "HARJU COUNTY");
    public static final StatName INDUSTRIAL_BUILDINGS = name("Tööstushooned", "Industrial buildings");
    public static final StatName MALES = name("Mehed", "Males");
    public static final StatName MALES_AND_FEMALES = name("Mehed ja naised", "Males and females");
    public static final StatName MANUFACTURING = name("Töötlev tööstus", "Manufacturing");
    public static final StatName MINING = name("Mäetööstus", "Mining and quarrying");
    public static final StatName REFERENCE_PERIOD = name("Vaatlusperiood", "Reference period");
    public static final StatName REGION = name("Piirkond", "Region");
    public static final StatName SERVICE = name("teenus (EBOPS)", "service (EBOPS)");
    public static final StatName SEX = name("Sugu", "Sex");
    public static final StatName TARTU_COUNTY = name("TARTU MAAKOND", "TARTU COUNTY");
    public static final StatName TIME = name("Aeg", "Time");
    public static final StatName TOTAL = name("Kokku", "Total");
    public static final StatName TRANSPORT = name("Transport", "Transport");
    public static final StatName TRANSPORT_AIR = name("Õhutransport", "Air transport");
    public static final StatName TRANSPORT_SEA = name("Meretransport", "Sea Transport");
    public static final StatName TRIP_PURPOSE = name("Reisi eesmärk", "Purpose of trip");
    public static final StatName WHOLE = name("Kogu Eesti", "Whole country");
    public static final String TALLINN = "Tallinn";
    public static final String _0to17 = "0-17";
    public static final String _0to17_new_et = "0 kuni 17 aastat";
    public static final String _0to17_new_en = "From 0 to 17 years";
    public static final String _16to24 = "16-24";
    public static final String _18to64 = "18-64";
    public static final String _65p = "65+";

    public static final LocalDate _2011_12_31 = of(2011, 12, 31);
    public static final LocalDate _2012_1_1 = of(2012, 1, 1);
    public static final LocalDate _2013_1_1 = of(2013, 1, 1);
    public static final LocalDate _2014_1_1 = of(2014, 1, 1);
    public static final LocalDate _2015_1_1 = of(2015, 1, 1);
    public static final LocalDate _2016_1_1 = of(2016, 1, 1);
    public static final LocalDate _2017_1_1 = of(2017, 1, 1);
    public static final LocalDate _2018_1_1 = of(2018, 1, 1);
    public static final LocalDate _2018_10_1 = of(2018, 10, 1);
    public static final LocalDate _2019_1_1 = of(2019, 1, 1);
    public static final LocalDate _2019_2_1 = of(2019, 2, 1);
    public static final LocalDate _2022_1_1 = of(2022, 1, 1);
    public static final LocalDate _2023_1_1 = of(2023, 1, 1);
    public static final LocalDate _2031_1_1 = of(2031, 1, 1);

    public static final String EH47U_CODE_API = "year_eh47u_nameen_api";
    public static final String EH47U_DESCRIPTION_EN = "YEAR EH47U DescriptionEn";
    public static final String EH47U_DESCRIPTION_ET = "YEAR EH47U DescriptionEt";
    public static final String EH47U_METADATA_EN = "YEAR EH47U MethodLinkEn";
    public static final String EH47U_METADATA_ET = "YEAR EH47U MethodLinkEt";
    public static final String EH47U_NAME_EN_API = "YEAR EH47U NameEn api";
    public static final String EH47U_NAME_ET = "YEAR EH47U NameEt";
    public static final String EH47U_NOTE_EN = "YEAR EH47U NoteEn";
    public static final String EH47U_NOTE_ET = "YEAR EH47U NoteEt";
    public static final String EH47U_SHORTNAME_EN = "YEAR EH47U ShortnameEn";
    public static final String EH47U_SHORTNAME_ET = "YEAR EH47U ShortnameEt";
    public static final String EH47U_STAT_JOB_EN = "YEAR EH47U StatisticianJobLinkEn";
    public static final String EH47U_STAT_JOB_ET = "YEAR EH47U StatisticianJobLinkEt";

    public static final String EKS21_CODE_API = "week_eks21_nameen_api";
    public static final String EKS21_DESCRIPTION_EN = "WEEK EKS21 DescriptionEn";
    public static final String EKS21_DESCRIPTION_ET = "WEEK EKS21 DescriptionEt";
    public static final String EKS21_METADATA_EN = "WEEK EKS21 MethodLinkEn";
    public static final String EKS21_METADATA_ET = "WEEK EKS21 MethodLinkEt";
    public static final String EKS21_NAME_EN = "WEEK EKS21 NameEn api";
    public static final String EKS21_NAME_ET = "WEEK EKS21 NameEt";
    public static final String EKS21_NOTE_EN = "WEEK EKS21 NoteEn";
    public static final String EKS21_NOTE_ET = "WEEK EKS21 NoteEt";
    public static final String EKS21_SHORTNAME_EN = "WEEK EKS21 ShortnameEn";
    public static final String EKS21_SHORTNAME_ET = "WEEK EKS21 ShortnameEt";
    public static final String EKS21_STAT_JOB_EN = "WEEK EKS21 StatisticianJobLinkEn";
    public static final String EKS21_STAT_JOB_ET = "WEEK EKS21 StatisticianJobLinkEt";

    public static final String EKS31_CODE_API = "week_eks31_nameen_api";
    public static final String EKS31_DESCRIPTION_EN = "WEEK EKS31 DescriptionEn";
    public static final String EKS31_DESCRIPTION_ET = "WEEK EKS31 DescriptionEt";
    public static final String EKS31_METADATA_EN = "WEEK EKS31 MethodLinkEn";
    public static final String EKS31_METADATA_ET = "WEEK EKS31 MethodLinkEt";
    public static final String EKS31_NAME_EN = "WEEK EKS31 NameEn api";
    public static final String EKS31_NAME_ET = "WEEK EKS31 NameEt";
    public static final String EKS31_NOTE_EN = "WEEK EKS31 NoteEn";
    public static final String EKS31_NOTE_ET = "WEEK EKS31 NoteEt";
    public static final String EKS31_SHORTNAME_EN = "WEEK EKS31 ShortnameEn";
    public static final String EKS31_SHORTNAME_ET = "WEEK EKS31 ShortnameEt";
    public static final String EKS31_STAT_JOB_EN = "WEEK EKS31 StatisticianJobLinkEn";
    public static final String EKS31_STAT_JOB_ET = "WEEK EKS31 StatisticianJobLinkEt";

    public static final String LES01_CODE_API = "year_les01_nameen_api";
    public static final String LES01_DESCRIPTION_EN = "YEAR LES01 DescriptionEn";
    public static final String LES01_DESCRIPTION_ET = "YEAR LES01 DescriptionEt";
    public static final String LES01_METADATA_EN = "YEAR LES01 MethodLinkEn";
    public static final String LES01_METADATA_ET = "YEAR LES01 MethodLinkEt";
    public static final String LES01_NAME_EN_API = "YEAR LES01 NameEn api";
    public static final String LES01_NAME_ET = "YEAR LES01 NameEt";
    public static final String LES01_NOTE_EN = "YEAR LES01 NoteEn";
    public static final String LES01_NOTE_ET = "YEAR LES01 NoteEt";
    public static final String LES01_SHORTNAME_EN = "YEAR LES01 ShortnameEn";
    public static final String LES01_SHORTNAME_ET = "YEAR LES01 ShortnameEt";
    public static final String LES01_STAT_JOB_EN = "YEAR LES01 StatisticianJobLinkEn";
    public static final String LES01_STAT_JOB_ET = "YEAR LES01 StatisticianJobLinkEt";

    public static final String PA001_CODE_API = "quarter_pa001_nameen_api";
    public static final String PA001_DESCRIPTION_EN = "QUARTER PA001 DescriptionEn";
    public static final String PA001_DESCRIPTION_ET = "QUARTER PA001 DescriptionEt";
    public static final String PA001_METADATA_EN = "QUARTER PA001 MethodLinkEn";
    public static final String PA001_METADATA_ET = "QUARTER PA001 MethodLinkEt";
    public static final String PA001_NAME_EN_API = "QUARTER PA001 NameEn api";
    public static final String PA001_NAME_ET = "QUARTER PA001 NameEt";
    public static final String PA001_NOTE_EN = "QUARTER PA001 NoteEn";
    public static final String PA001_NOTE_ET = "QUARTER PA001 NoteEt";
    public static final String PA001_SHORTNAME_EN = "QUARTER PA001 ShortnameEn";
    public static final String PA001_SHORTNAME_ET = "QUARTER PA001 ShortnameEt";
    public static final String PA001_STAT_JOB_EN = "QUARTER PA001 StatisticianJobLinkEn";
    public static final String PA001_STAT_JOB_ET = "QUARTER PA001 StatisticianJobLinkEt";

    public static final String PA006_CODE_API = "month_pa006_nameen_api";
    public static final String PA006_DESCRIPTION_EN = "MONTH PA006 DescriptionEn";
    public static final String PA006_DESCRIPTION_ET = "MONTH PA006 DescriptionEt";
    public static final String PA006_METADATA_EN = "MONTH PA006 MethodLinkEn";
    public static final String PA006_METADATA_ET = "MONTH PA006 MethodLinkEt";
    public static final String PA006_NAME_EN_API = "MONTH PA006 NameEn api";
    public static final String PA006_NAME_ET = "MONTH PA006 NameEt";
    public static final String PA006_NOTE_EN = "MONTH PA006 NoteEn";
    public static final String PA006_NOTE_ET = "MONTH PA006 NoteEt";
    public static final String PA006_SHORTNAME_EN = "MONTH PA006 ShortnameEn";
    public static final String PA006_SHORTNAME_ET = "MONTH PA006 ShortnameEt";
    public static final String PA006_STAT_JOB_EN = "MONTH PA006 StatisticianJobLinkEn";
    public static final String PA006_STAT_JOB_ET = "MONTH PA006 StatisticianJobLinkEt";

    public static final String RAA0012_CODE_API = "quarter_raa0012_nameen_api";
    public static final String RAA0012_DESCRIPTION_EN = "QUARTER RAA0012 DescriptionEn";
    public static final String RAA0012_DESCRIPTION_ET = "QUARTER RAA0012 DescriptionEt";
    public static final String RAA0012_METADATA_EN = "QUARTER RAA0012 MethodLinkEn";
    public static final String RAA0012_METADATA_ET = "QUARTER RAA0012 MethodLinkEt";
    public static final String RAA0012_NAME_EN_API = "QUARTER RAA0012 NameEn api";
    public static final String RAA0012_NAME_ET = "QUARTER RAA0012 NameEt";
    public static final String RAA0012_NOTE_EN = "QUARTER RAA0012 NoteEn";
    public static final String RAA0012_NOTE_ET = "QUARTER RAA0012 NoteEt";
    public static final String RAA0012_SHORTNAME_EN = "QUARTER RAA0012 ShortnameEn";
    public static final String RAA0012_SHORTNAME_ET = "QUARTER RAA0012 ShortnameEt";
    public static final String RAA0012_STAT_JOB_EN = "QUARTER RAA0012 StatisticianJobLinkEn";
    public static final String RAA0012_STAT_JOB_ET = "QUARTER RAA0012 StatisticianJobLinkEt";

    public static final String TT065_CODE_API = "month_tt065_nameen_api";
    public static final String TT065_DESCRIPTION_EN = "MONTH TT065 DescriptionEn";
    public static final String TT065_DESCRIPTION_ET = "MONTH TT065 DescriptionEt";
    public static final String TT065_METADATA_EN = "MONTH TT065 MethodLinkEn";
    public static final String TT065_METADATA_ET = "MONTH TT065 MethodLinkEt";
    public static final String TT065_NAME_EN_API = "MONTH TT065 NameEn api";
    public static final String TT065_NAME_ET = "MONTH TT065 NameEt";
    public static final String TT065_NOTE_EN = "MONTH TT065 NoteEn";
    public static final String TT065_NOTE_ET = "MONTH TT065 NoteEt";
    public static final String TT065_SHORTNAME_EN = "MONTH TT065 ShortnameEn";
    public static final String TT065_SHORTNAME_ET = "MONTH TT065 ShortnameEt";
    public static final String TT065_STAT_JOB_EN = "MONTH TT065 StatisticianJobLinkEn";
    public static final String TT065_STAT_JOB_ET = "MONTH TT065 StatisticianJobLinkEt";

    public static final String TU122_CODE_API = "month_tu122_nameen_api";
    public static final String TU122_DESCRIPTION_EN = "MONTH TU122 DescriptionEn";
    public static final String TU122_DESCRIPTION_ET = "MONTH TU122 DescriptionEt";
    public static final String TU122_METADATA_EN = "MONTH TU122 MethodLinkEn";
    public static final String TU122_METADATA_ET = "MONTH TU122 MethodLinkEt";
    public static final String TU122_NAME_EN = "MONTH TU122 NameEn api";
    public static final String TU122_NAME_ET = "MONTH TU122 NameEt";
    public static final String TU122_NOTE_EN = "MONTH TU122 NoteEn";
    public static final String TU122_NOTE_ET = "MONTH TU122 NoteEt";
    public static final String TU122_SHORTNAME_EN = "MONTH TU122 ShortnameEn";
    public static final String TU122_SHORTNAME_ET = "MONTH TU122 ShortnameEt";
    public static final String TU122_STAT_JOB_EN = "MONTH TU122 StatisticianJobLinkEn";
    public static final String TU122_STAT_JOB_ET = "MONTH TU122 StatisticianJobLinkEt";

    public static final String TU51_CODE_API = "year_tu51_nameen_api";
    public static final String TU51_DESCRIPTION_EN = "YEAR TU51 DescriptionEn";
    public static final String TU51_DESCRIPTION_ET = "YEAR TU51 DescriptionEt";
    public static final String TU51_METADATA_EN = "YEAR TU51 MethodLinkEn";
    public static final String TU51_METADATA_ET = "YEAR TU51 MethodLinkEt";
    public static final String TU51_NAME_EN_API = "YEAR TU51 NameEn api";
    public static final String TU51_NAME_ET = "YEAR TU51 NameEt";
    public static final String TU51_NOTE_EN = "YEAR TU51 NoteEn";
    public static final String TU51_NOTE_ET = "YEAR TU51 NoteEt";
    public static final String TU51_SHORTNAME_EN = "YEAR TU51 ShortnameEn";
    public static final String TU51_SHORTNAME_ET = "YEAR TU51 ShortnameEt";
    public static final String TU51_STAT_JOB_EN = "YEAR TU51 StatisticianJobLinkEn";
    public static final String TU51_STAT_JOB_ET = "YEAR TU51 StatisticianJobLinkEt";

    public static final String VKT14_CODE_SQL = "year_vkt14_nameen_sql";
    public static final String VKT14_DESCRIPTION_EN = "YEAR VKT14 DescriptionEn";
    public static final String VKT14_DESCRIPTION_ET = "YEAR VKT14 DescriptionEt";
    public static final String VKT14_METADATA_EN = "YEAR VKT14 MethodLinkEn";
    public static final String VKT14_METADATA_ET = "YEAR VKT14 MethodLinkEt";
    public static final String VKT14_NAME_EN_SQL = "YEAR VKT14 NameEn sql";
    public static final String VKT14_NAME_ET = "YEAR VKT14 NameEt";
    public static final String VKT14_NOTE_EN = "YEAR VKT14 NoteEn";
    public static final String VKT14_NOTE_ET = "YEAR VKT14 NoteEt";
    public static final String VKT14_SHORTNAME_EN = "YEAR VKT14 ShortnameEn";
    public static final String VKT14_SHORTNAME_ET = "YEAR VKT14 ShortnameEt";
    public static final String VKT14_STAT_JOB_EN = "YEAR VKT14 StatisticianJobLinkEn";
    public static final String VKT14_STAT_JOB_ET = "YEAR VKT14 StatisticianJobLinkEt";

    public static final ParameterizedTypeReference<Response<List<ElementResponse>>> LIST_OF_ELEMENTS = new ParameterizedTypeReference<>() {
    };
    public static final ParameterizedTypeReference<Response<List<ClassifierResponse>>> LIST_OF_CLASSIFIERS = new ParameterizedTypeReference<>() {
    };
    public static final ParameterizedTypeReference<Response<DashboardResponse>> DASHBOARD_TYPE = new ParameterizedTypeReference<>() {
    };
    public static final ParameterizedTypeReference<Response<List<RoleDashboardResponse>>> LIST_OF_ROLE_DASHBOARDS = new ParameterizedTypeReference<>() {
    };
    public static final ParameterizedTypeReference<Response<WidgetResponse>> WIDGET_TYPE = new ParameterizedTypeReference<>() {
    };
    public static final ParameterizedTypeReference<Response<UserWidgetDto>> USER_WIDGET_TYPE = new ParameterizedTypeReference<>() {
    };
    public static final ParameterizedTypeReference<Response<List<AppUser>>> LIST_OF_APP_USERS = new ParameterizedTypeReference<>() {
    };
    public static final ParameterizedTypeReference<Response<List<XmlDimension>>> LIST_OF_XML_DIMENSIONS = new ParameterizedTypeReference<>() {
    };
    public static final ParameterizedTypeReference<Response<ProcessLogDto>> STAT_PROCESS_LOG = new ParameterizedTypeReference<>() {
    };
    public static final ParameterizedTypeReference<Response<StatPage<ProcessLogDto>>> STAT_PROCESS_LOGS = new ParameterizedTypeReference<>() {
    };
    public static final ParameterizedTypeReference<Response<StatPage<StatLogDto>>> STAT_LOGS = new ParameterizedTypeReference<>() {
    };
    public static final ParameterizedTypeReference<Response<UserData>> USERDATA_TYPE = new ParameterizedTypeReference<>() {
    };
    public static final ParameterizedTypeReference<Response<DashboardAdminResponse>> ADMIN_DASHBOARD = new ParameterizedTypeReference<>() {
    };
    public static final ParameterizedTypeReference<Response<StatPage<DashboardAdminResponse>>> LIST_OF_ADMIN_DASHBOARDS = new ParameterizedTypeReference<>() {
    };
    public static final ParameterizedTypeReference<Response<WidgetAdminResponse>> ADMIN_WIDGET_TYPE = new ParameterizedTypeReference<>() {
    };
    public static final ParameterizedTypeReference<Response<StatPage<WidgetAdminResponse>>> LIST_OF_ADMIN_WIDGETS = new ParameterizedTypeReference<>() {
    };
    public static final ParameterizedTypeReference<Response<List<GraphTypeEnum>>> LIST_OF_GRAPHTYPES = new ParameterizedTypeReference<>() {
    };

    public static BigDecimal d(String s) {
        return s != null ? new BigDecimal(s) : null;
    }

    public static <T> void list_size(List<T> list, int i) {
        assertNotNull(list, "null list");
        assertEquals(i, list.size(), format("expected %s actual %s", i, list.size()));
    }

    public static <T> void list_size_equals_or_bigger(List<T> list, int i) {
        assertNotNull(list);
        assertTrue(i <= list.size());
    }

    public static <K, V> void map_size(Map<K, V> map, int i) {
        assertNotNull(map);
        assertEquals(i, map.size());
    }

    public static void assertValue(Serie serie, Source source) {
        assertNotNull(serie.getValue());
        assertEquals(source.toFront(), serie.getSource());
    }

    public static void assertValueSource(Serie serie, Source source) {
        assertEquals(source.toFront(), serie.getSource());
    }

    public static <T> T first(List<T> series) {
        return series.get(0);
    }

    public static <T> T second(List<T> series) {
        return series.get(1);
    }

    public static <T> T nth(List<T> series, int index) {
        return series.get(index);
    }

    public static <T> T last(List<T> series) {
        return series.get(series.size() - 1);
    }

    public static void commonWidget(WidgetResponse widget, int expected) {
        assertNotNull(widget);
        assertEquals(expected, (long) widget.getId());
        assertNotNull(widget.getLang());
    }

    public static void commonWidget(WidgetResponse widget) {
        assertNotNull(widget);
        assertNotNull(widget.getLang());
    }

    public static void assertFilters(List<FilterDto> filters, TestFilter... testFilters) {
        assertNotNull(testFilters);
        list_size(filters, testFilters.length);
        for (TestFilter filter : testFilters) {
            FilterDto dto = getFilter(filters, filter.getName());
            if (filter.getStrategy().isPrecise()) {
                list_size(dto.getValues(), filter.getNumberOfValues());
            } else {
                list_size_equals_or_bigger(dto.getValues(), filter.getNumberOfValues());
            }
        }
    }

    public static FilterDto getFilter(List<FilterDto> filters, String filter1) {
        return filters.stream().filter(e -> e.getName().equals(filter1)).findAny().orElseThrow(RuntimeException::new);
    }

    public static List<FilterValueDto> defaultValues(FilterDto dto) {
        return dto.getValues().stream().filter(b -> isTrue(b.getSelected())).collect(toList());
    }

    public static boolean filterHas(FilterSerie s, String object) {
        return s.getFilters().containsValue(object);
    }

    public static boolean filterHas(FilterSerie s, Collection<String> object) {
        return s.getFilters().values().containsAll(object);
    }

    public static Optional<FilterSerie> getFilterSeries(List<FilterSerie> filterSeries, String f1, String f2) {
        return filterSeries.stream()
                .filter(s -> filterHas(s, List.of(f1, f2)))
                .findAny();
    }

    public static WidgetResponse getTourismTU51Widget(DashboardResponse dashboard) {
        DomainResponse domain = getDomain(dashboard.getElements(), ACCOMODATION);
        return getWidget(domain.getWidgets(), TU51_CODE_API);
    }

    public static WidgetResponse getTourismEKS21Widget(DashboardResponse dashboard) {
        DomainResponse domain = getDomain(dashboard.getElements(), ACCOMODATION);
        return getWidget(domain.getWidgets(), EKS21_CODE_API);
    }

    public static WidgetResponse getTourismLES01Widget(DashboardResponse dashboard) {
        DomainResponse domain = getDomain(dashboard.getElements(), ACCOMODATION);
        return getWidget(domain.getWidgets(), LES01_CODE_API);
    }

    public static WidgetResponse getTourismVKT14Widget(DashboardResponse dashboard) {
        DomainResponse domain = getDomain(dashboard.getElements(), ACCOMODATION);
        return getWidget(domain.getWidgets(), VKT14_CODE_SQL);
    }

    public static WidgetResponse getTourismTT065Widget(DashboardResponse dashboard) {
        DomainResponse domain = getDomain(dashboard.getElements(), ACCOMODATION);
        return getWidget(domain.getWidgets(), TT065_CODE_API);
    }

    public static WidgetResponse getTourismRAA0012Widget(DashboardResponse dashboard) {
        DomainResponse domain = getDomain(dashboard.getElements(), ACCOMODATION);
        return getWidget(domain.getWidgets(), RAA0012_CODE_API);
    }

    public static WidgetResponse getTourismPA001Widget(DashboardResponse dashboard) {
        DomainResponse domain = getDomain(dashboard.getElements(), ACCOMODATION);
        return getWidget(domain.getWidgets(), PA001_CODE_API);
    }

    public static WidgetResponse getTourismPA006Widget(DashboardResponse dashboard) {
        DomainResponse domain = getDomain(dashboard.getElements(), ACCOMODATION);
        return getWidget(domain.getWidgets(), PA006_CODE_API);
    }

    public static WidgetResponse getTourismEKS31Widget(DashboardResponse dashboard) {
        DomainResponse domain = getDomain(dashboard.getElements(), ACCOMODATION);
        return getWidget(domain.getWidgets(), EKS31_CODE_API);
    }

    public static WidgetResponse getTourismTU122Widget(DashboardResponse dashboard) {
        DomainResponse domain = getDomain(dashboard.getElements(), ACCOMODATION);
        return getWidget(domain.getWidgets(), TU122_CODE_API);
    }

    public static WidgetResponse getTourismEH47UWidget(DashboardResponse dashboard) {
        DomainResponse domain = getDomain(dashboard.getElements(), ACCOMODATION);
        return getWidget(domain.getWidgets(), EH47U_CODE_API);
    }

    public static void assertMapType(WidgetResponse widget, GraphDto graph, Long ehakId) {
        if (widget.getGraphType().isMap()) {
            if (ehakId == null || ehakId <= 2) {
                assertEquals(MK, graph.getMapType());
            } else {
                assertEquals(KOV, graph.getMapType());
            }
        } else {
            assertNull(graph.getMapType());
        }
    }

    public static Optional<Serie> findSerieByAbscissa(List<Serie> series, String total) {
        return series.stream().filter(e -> e.getAbscissa().equalsIgnoreCase(total)).findAny();
    }

    public static void assertPresent(Optional<Serie> totalOp) {
        assertTrue(totalOp.isPresent());
        Serie total = totalOp.get();
        assertValue(total, STAT_DATA_API);
    }

    private static DomainResponse getDomain(List<DomainResponse> elements, String code) {
        list_size_equals_or_bigger(elements, 1);
        Optional<DomainResponse> domainOp = elements.stream().filter(e -> e.getCode().equals(code)).findAny();
        assertTrue(domainOp.isPresent());
        return domainOp.get();
    }

    private static WidgetResponse getWidget(List<WidgetResponse> widgets, String code) {
        list_size_equals_or_bigger(widgets, 1);
        Optional<WidgetResponse> widgetOp = widgets.stream().filter(e -> e.getCode().equals(code)).findAny();
        assertTrue(widgetOp.isPresent());
        return widgetOp.get();
    }

    @SneakyThrows
    public static String readResourceAsString(@NonNull String path) {
        ClassPathResource resource = new ClassPathResource(path);
        if (!resource.exists()) {
            throw new ResourceNotFoundException(path);
        }
        try (InputStream stream = resource.getInputStream()) {
            return readFromStream(stream);
        }
    }

    private static String readFromStream(InputStream inputStream) throws IOException {
        return copyToString(inputStream, UTF_8);
    }

    public static class ResourceNotFoundException extends RuntimeException {

        private static final String MESSAGE = "Resource not found from path %s.";

        public ResourceNotFoundException(String path) {
            super(format(MESSAGE, path));
        }
    }

    @AllArgsConstructor(access = PRIVATE)
    public static class StatName {
        private String nameEt;
        private String nameEn;

        public String name(Language lang) {
            return lang.isEt() ? nameEt : nameEn;
        }

        public static StatName name(String nameEt, String nameEn) {
            return new StatName(nameEt, nameEn);
        }


        public static StatName name(String name) {
            return new StatName(name, name);
        }
    }
}
