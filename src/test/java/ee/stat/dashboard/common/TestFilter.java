package ee.stat.dashboard.common;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static ee.stat.dashboard.common.TestFilter.FilterValidationStrategy.GROWING;
import static ee.stat.dashboard.common.TestFilter.FilterValidationStrategy.PRECISE;
import static lombok.AccessLevel.PRIVATE;

@Getter
@Setter
@NoArgsConstructor(access = PRIVATE)
public class TestFilter {

    private String name;
    private int numberOfValues;
    private FilterValidationStrategy strategy;

    public static TestFilter preciseFilter(String filterName, int numberOfValues) {
        TestFilter filter = new TestFilter();
        filter.setName(filterName);
        filter.setNumberOfValues(numberOfValues);
        filter.setStrategy(PRECISE);
        return filter;
    }

    public static TestFilter growingFilter(String filterName, int numberOfValues) {
        TestFilter filter = new TestFilter();
        filter.setName(filterName);
        filter.setNumberOfValues(numberOfValues);
        filter.setStrategy(GROWING);
        return filter;
    }

    public enum FilterValidationStrategy {
        PRECISE, GROWING;

        public boolean isPrecise() {
            return this == PRECISE;
        }

        public boolean isGrowing() {
            return this == GROWING;
        }
    }

}

