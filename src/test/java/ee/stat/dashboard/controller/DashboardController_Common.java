package ee.stat.dashboard.controller;

import ee.stat.dashboard.TestUser;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.dashboard.dto.DashboardResponse;
import ee.stat.dashboard.service.dashboard.dto.UserDashboardDto;
import ee.stat.dashboard.service.dashboard.dto.UserWidgetsDto;
import ee.stat.dashboard.service.element.dto.ElementResponse;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static ee.stat.dashboard.AssertUtil.DASHBOARD_1;
import static ee.stat.dashboard.AssertUtil.DASHBOARD_TYPE;
import static ee.stat.dashboard.AssertUtil.USER_REGULAR;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.model.widget.back.enums.Language.ET;
import static java.text.MessageFormat.format;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.PATCH;

public abstract class DashboardController_Common extends CommonRestTest {

    protected DashboardResponse getDashboard(Long id, Language lang) {
        return ok(exchange(GET, id, lang, null, null, null, null));
    }

    protected DashboardResponse getDashboard(Long id, Language lang, String prefix) {
        return ok(exchange(GET, id, lang, null, null, null, prefix));
    }

    protected void saveDashboard(ElementResponse element, String prefix) {
        UserDashboardDto saveDto = new UserDashboardDto(element.getId());
        DashboardResponse savedDashboard = ok(exchange(PATCH, DASHBOARD_1, ET, USER_REGULAR, saveDto, "/region", prefix));
        list_size(savedDashboard.getRegions(), 19);
        assertTrue(savedDashboard.getRegions().get(1).getSelected());
    }

    protected void saveDashboard(WidgetResponse widget, String prefix) {
        UserWidgetsDto saveDto = new UserWidgetsDto(List.of(widget.getId()));
        DashboardResponse savedDashboard = ok(exchange(PATCH, DASHBOARD_1, ET, USER_REGULAR, saveDto, "/widgets", prefix));
        list_size(savedDashboard.getSelectedWidgets(), 1);
        assertEquals(widget.getId(), savedDashboard.getSelectedWidgets().get(0));
    }

    protected <T> ResponseEntity<Response<DashboardResponse>> exchange(HttpMethod method, Long id, Language lang, TestUser testUser, T body, String extra, final String prefix) {
        HttpEntity<T> entity = body != null ? new HttpEntity<>(body) : null;
        String url = format("{0}/dashboard/{1}{2}?{3}", orEmpty(prefix), orEmpty(id), orEmpty(extra), orEmpty(lang));
        return addAuth(testUser).exchange(url, method, entity, DASHBOARD_TYPE);
    }
}
