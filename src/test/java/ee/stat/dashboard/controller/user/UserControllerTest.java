package ee.stat.dashboard.controller.user;

import ee.stat.dashboard.controller.CommonRestTest;
import ee.stat.dashboard.model.user.user.AppUser;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static ee.stat.dashboard.AssertUtil.LIST_OF_APP_USERS;
import static ee.stat.dashboard.AssertUtil.list_size_equals_or_bigger;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpMethod.GET;


@SpringBootTest(webEnvironment = RANDOM_PORT)
class UserControllerTest extends CommonRestTest {

    @Test
    void can_request_app_users() {
        List<AppUser> appUsers = getAppUsers();
        assertNotNull(appUsers);
        list_size_equals_or_bigger(appUsers, 2);
    }

    private List<AppUser> getAppUsers() {
        return ok(restTemplate.exchange("/user", GET, null, LIST_OF_APP_USERS));
    }
}
