package ee.stat.dashboard.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import static ee.stat.dashboard.AssertUtil.USER_ADMIN;
import static ee.stat.dashboard.AssertUtil.USER_REGULAR;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;


@SpringBootTest(webEnvironment = RANDOM_PORT)
class PublicUrlTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void index_is_up() {
        isUp("/", "Dashboard backend api is up!");
    }

    @Test
    void visitor_cannot_access_user() {
        ResponseEntity<String> authQuery = restTemplate
                .getForEntity("/auth", String.class);
        assertEquals(UNAUTHORIZED, authQuery.getStatusCode());
        ResponseEntity<String> adminQuery = restTemplate
                .getForEntity("/admin", String.class);
        assertEquals(UNAUTHORIZED, adminQuery.getStatusCode());
    }

    @Test
    void user_can_access_user() {
        ResponseEntity<String> authQuery = restTemplate
                .withBasicAuth(USER_REGULAR.getUsername(), USER_REGULAR.getToken())
                .getForEntity("/auth", String.class);
        assertEquals(OK, authQuery.getStatusCode());
        ResponseEntity<String> adminQuery = restTemplate
                .withBasicAuth(USER_REGULAR.getUsername(), USER_REGULAR.getToken())
                .getForEntity("/admin", String.class);
        assertEquals(FORBIDDEN, adminQuery.getStatusCode());
    }

    @Test
    void admin_can_access_admin() {
        ResponseEntity<String> authQuery = restTemplate
                .withBasicAuth(USER_ADMIN.getUsername(), USER_ADMIN.getToken())
                .getForEntity("/auth", String.class);
        assertEquals(OK, authQuery.getStatusCode());
        ResponseEntity<String> health = restTemplate
                .withBasicAuth(USER_ADMIN.getUsername(), USER_ADMIN.getToken())
                .getForEntity("/admin", String.class);
        assertEquals(OK, health.getStatusCode());
    }

    @Test
    void actuator_is_up() {
        isUp("/actuator");
//        isUp("/actuator/info");
        isUp("/actuator/health");
    }

    private void isUp(String s) {
        ResponseEntity<String> health = restTemplate.getForEntity(s, String.class);
        assertEquals(OK, health.getStatusCode());
    }

    private void isUp(String s, String ok) {
        ResponseEntity<String> cancel = restTemplate.getForEntity(s, String.class);
        assertEquals(OK, cancel.getStatusCode());
        assertEquals(ok, cancel.getBody());
    }

}
