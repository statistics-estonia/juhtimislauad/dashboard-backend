package ee.stat.dashboard.controller;

import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.service.dashboard.dto.DashboardResponse;
import ee.stat.dashboard.service.dashboard.dto.UserWidgetsDto;
import ee.stat.dashboard.service.widget.widget.dto.FilterDto;
import ee.stat.dashboard.service.widget.widget.dto.UserFilterDto;
import ee.stat.dashboard.service.widget.widget.dto.UserGraphTypeDto;
import ee.stat.dashboard.service.widget.widget.dto.UserWidgetDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import ee.stat.dashboard.validator.strategy.DashboardValidationStrategy;
import ee.stat.dashboard.validator.strategy.WidgetValidationStrategy;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static ee.stat.dashboard.AssertUtil.DASHBOARD_1;
import static ee.stat.dashboard.AssertUtil.USER_REGULAR;
import static ee.stat.dashboard.AssertUtil.first;
import static ee.stat.dashboard.AssertUtil.getTourismTU122Widget;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.model.widget.back.enums.Language.ET;
import static ee.stat.dashboard.validator.DashboardValidator.assertTourismDashboard;
import static ee.stat.dashboard.validator.Widget_TU122Validator.assertTU122Widget;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

class DashboardWidgetController_SavePreferences_Test extends DashboardWidgetController_Common {

    @Test
    void user_preferences_flow() {
        Long accommodationId = accomodationWidgetId(DASHBOARD_1);
        WidgetResponse widget = getWidget(DASHBOARD_1, accommodationId, ET, GraphTypeEnum.map);

        ResponseEntity<Response<UserWidgetDto>> response = updatePreferences(USER_REGULAR, DASHBOARD_1, accommodationId, preferences(widget));
        assertEquals(OK, response.getStatusCode());
        WidgetResponse userWidget = getWidget(DASHBOARD_1, accommodationId, ET, GraphTypeEnum.map, USER_REGULAR);
        UserWidgetDto preferences = userWidget.getPreferences();
        assertNotNull(preferences);
        list_size(preferences.getGraphs(), 1);

        UserGraphTypeDto graphTypeDto = preferences.getGraphs().get(0);
        assertEquals(GraphTypeEnum.map, graphTypeDto.getGraphType());
        list_size(graphTypeDto.getFilters(), 1);
        FilterDto filterDto = userWidget.getDiagram().getFilters().get(0);
        UserFilterDto userFilterDto = graphTypeDto.getFilters().get(0);
        assertEquals(filterDto.getId(), userFilterDto.getFilter());
        assertEquals(filterDto.getValues().get(2).getId(), userFilterDto.getValues().get(0));

        assertTU122Widget(userWidget, WidgetValidationStrategy.WIDGET_DATA_1_FILTER);
    }

    @Test
    void user_pin_flow() {
        Long accommodationId = accomodationWidgetId(DASHBOARD_1);
        WidgetResponse widget = getWidget(DASHBOARD_1, accommodationId, ET, GraphTypeEnum.map);

        //update preferences for widget to have some preferences
        ResponseEntity<Response<UserWidgetDto>> preferencesResponse = updatePreferences(USER_REGULAR, DASHBOARD_1, accommodationId, preferences(widget));
        assertEquals(OK, preferencesResponse.getStatusCode());

        user_pin_unpin_flow(accommodationId);
        user_pin_unpin_flow(accommodationId);
    }

    private void user_pin_unpin_flow(Long accommodationId) {
        WidgetResponse originalWidget = getWidget(DASHBOARD_1, accommodationId, ET, GraphTypeEnum.map, USER_REGULAR);

        //pin widget
        ResponseEntity<Response<UserWidgetDto>> pinned = updatePin(USER_REGULAR, DASHBOARD_1, accommodationId, pin());
        assertEquals(OK, pinned.getStatusCode());

        //creates my dashboard if it didn't exist, dashboard has widgets
        DashboardResponse myDashboard = getMyDashboard(ET, USER_REGULAR);
        list_size(myDashboard.getWidgets(), 1);
        assertEquals(accommodationId, first(myDashboard.getWidgets()).getId());

        //my dashboard widget has same preferences as old widget
        WidgetResponse myDashboardWidget = getWidget(myDashboard.getId(), accommodationId, ET, GraphTypeEnum.map, USER_REGULAR);
        assertNotNull(myDashboardWidget.getPreferences());
        list_size(myDashboardWidget.getPreferences().getGraphs(), 1);
        list_size(myDashboardWidget.getDashboards(), 1);
        DashboardResponse firstDashboard = first(myDashboardWidget.getDashboards());
        assertTourismDashboard(firstDashboard, DashboardValidationStrategy.ROLE_DASHBOARD);
        assertEquals(originalWidget.getPreferences().getGraphs(), myDashboardWidget.getPreferences().getGraphs());

        //old widget has pinned = true property
        WidgetResponse pinnedOriginalWidget = getWidget(DASHBOARD_1, accommodationId, ET, GraphTypeEnum.map, USER_REGULAR);
        assertNotNull(pinnedOriginalWidget.getPreferences());
        assertTrue(pinnedOriginalWidget.getPreferences().getPinned());

        //unpin widget
        ResponseEntity<Response<UserWidgetDto>> unpinned = updatePin(USER_REGULAR, DASHBOARD_1, accommodationId, unpin());
        assertEquals(OK, unpinned.getStatusCode());

        //my dashboard is still there, but it has no widgets
        DashboardResponse myDashboard1 = getMyDashboard(ET, USER_REGULAR);
        list_size(myDashboard1.getWidgets(), 0);

        //my dashboard widget is not there 404
        getWidget_notFound(myDashboard.getId(), accommodationId, ET, GraphTypeEnum.map, USER_REGULAR);

        //old widget has pinned = null property
        WidgetResponse unpinnedOriginalWidget = getWidget(DASHBOARD_1, accommodationId, ET, GraphTypeEnum.map, USER_REGULAR);
        assertNotNull(unpinnedOriginalWidget.getPreferences());
        assertNull(unpinnedOriginalWidget.getPreferences().getPinned());
    }

    @Test
    void user_must_be_logged_in_save_preferences() {
        ResponseEntity<Response<UserWidgetDto>> response = updatePreferences(null, DASHBOARD_1, 1L, new UserWidgetDto());
        assertEquals(UNAUTHORIZED, response.getStatusCode());
    }

    @Test
    void user_must_be_logged_in_to_pin() {
        ResponseEntity<Response<UserWidgetDto>> response = updatePin(null, DASHBOARD_1, 1L, new UserWidgetDto());
        assertEquals(UNAUTHORIZED, response.getStatusCode());
    }

    private UserWidgetDto preferences(WidgetResponse widget) {
        UserWidgetDto dto = new UserWidgetDto();
        dto.setGraphs(List.of(userGraphDto(widget)));
        return dto;
    }

    private UserWidgetDto pin() {
        UserWidgetDto dto = new UserWidgetDto();
        dto.setPinned(true);
        return dto;
    }

    private UserWidgetDto unpin() {
        UserWidgetDto dto = new UserWidgetDto();
        dto.setPinned(false);
        return dto;
    }

    private Long accomodationWidgetId(long dashboardId) {
        DashboardResponse dashboard = getDashboard(dashboardId, ET);
        WidgetResponse widget = getTourismTU122Widget(dashboard);
        DashboardResponse savedDashboard = patchDashboard(dashboardId, ET, USER_REGULAR, saveDto(widget));
        assertEquals(1, savedDashboard.getSelectedWidgets().size());
        assertEquals(widget.getId(), savedDashboard.getSelectedWidgets().get(0));
        return widget.getId();
    }

    private UserWidgetsDto saveDto(WidgetResponse widget) {
        return new UserWidgetsDto(List.of(widget.getId()));
    }

    private UserGraphTypeDto userGraphDto(WidgetResponse widget1) {
        UserGraphTypeDto graphTypeDto = new UserGraphTypeDto();
        graphTypeDto.setGraphType(GraphTypeEnum.map);
        graphTypeDto.setFilters(List.of(userFilterDto(widget1)));
        return graphTypeDto;
    }

    private UserFilterDto userFilterDto(WidgetResponse widget1) {
        UserFilterDto userFilterDto = new UserFilterDto();
        userFilterDto.setFilter(widget1.getDiagram().getFilters().get(0).getId());
        userFilterDto.setValues(List.of(widget1.getDiagram().getFilters().get(0).getValues().get(2).getId()));
        return userFilterDto;
    }
}
