package ee.stat.dashboard.controller;

import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import org.junit.jupiter.api.Test;

class DashboardWidgetController_Type_Area_Test extends DashboardWidgetController_Common {

    static final GraphTypeEnum GRAPH_TYPE = GraphTypeEnum.area;

    @Test
    void application_returns_tourism_accommodation_widget() {
        tourism_TU122_BadRequest(GRAPH_TYPE);
    }

    @Test
    void application_returns_tourism_EKS31_widget() {
        tourism_EKS31_BadRequest(GRAPH_TYPE);
    }

    @Test
    void application_returns_tourism_EKS21_widget() {
        tourism_EKS21_BadRequest(GRAPH_TYPE);
    }

    @Test
    void application_returns_tourism_TT065_widget() {
        tourism_TT065_widget(GRAPH_TYPE, null);
    }

    @Test
    void application_returns_tourism_TT065_widget_whole_country() {
        tourism_TT065_widget(GRAPH_TYPE, 1L);
    }

    @Test
    void application_returns_tourism_TT065_widget_harju_county() {
        tourism_TT065_widget(GRAPH_TYPE, 2L);
    }

    @Test
    void application_returns_tourism_TT065_widget_anja_municipality() {
        tourism_TT065_widget(GRAPH_TYPE, 3L);
    }

    @Test
    void application_returns_tourism_TU51_widget() {
        travelling_BadRequest(GRAPH_TYPE);
    }

    @Test
    void application_returns_tourism_LES01_widget() {
        tourism_LES01_BadRequest(GRAPH_TYPE);
    }

    @Test
    void application_returns_tourism_TT065_widget_with_ehak_whole_country() {
        tourism_TT065_widget(GRAPH_TYPE, 1L);
    }

    @Test
    void application_returns_tourism_TT065_widget_with_ehak_harju_county() {
        tourism_TT065_widget(GRAPH_TYPE, 2L);
    }

    @Test
    void application_returns_tourism_TT065_widget_with_ehak_anja_municipality() {
        tourism_TT065_widget(GRAPH_TYPE, 3L);
    }
}
