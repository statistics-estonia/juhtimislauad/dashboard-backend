package ee.stat.dashboard.controller;

import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.dashboard.dto.DashboardResponse;
import ee.stat.dashboard.service.dashboard.dto.RoleDashboardResponse;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import org.junit.jupiter.api.Test;

import java.util.List;

import static ee.stat.dashboard.AssertUtil.DASHBOARD_1;
import static ee.stat.dashboard.AssertUtil.DASHBOARD_2;
import static ee.stat.dashboard.AssertUtil.DASHBOARD_3;
import static ee.stat.dashboard.AssertUtil.DASHBOARD_4;
import static ee.stat.dashboard.AssertUtil.DASHBOARD_TYPE;
import static ee.stat.dashboard.AssertUtil.LIST_OF_ROLE_DASHBOARDS;
import static ee.stat.dashboard.AssertUtil.getTourismTU122Widget;
import static ee.stat.dashboard.model.widget.back.enums.Language.EN;
import static ee.stat.dashboard.model.widget.back.enums.Language.ET;
import static ee.stat.dashboard.validator.DashboardValidator.assertRegionalStatDashboard;
import static ee.stat.dashboard.validator.DashboardValidator.assertSocialMinistryDashboard;
import static ee.stat.dashboard.validator.DashboardValidator.assertTourismDashboard;
import static ee.stat.dashboard.validator.DashboardValidator.assertWelfareDevelopmentPlanDashboard;
import static ee.stat.dashboard.validator.RoleDashboardValidator.validateRoles;
import static ee.stat.dashboard.validator.strategy.DashboardValidationStrategy.DASHBOARD_META;
import static ee.stat.dashboard.validator.strategy.DashboardValidationStrategy.DASHBOARD_META_AND_DATA;
import static org.springframework.http.HttpMethod.GET;

class DashboardV2ControllerTest extends DashboardController_Common {

    @Test
    void application_returns_a_list_of_dashboards() {
        validateRoles(getRoleDashboards(ET), ET);
        validateRoles(getRoleDashboards(EN), EN);
    }

    @Test
    void application_returns_tourism_dashboard() {
        assertTourismDashboard(getDashboard(DASHBOARD_1, ET, "/v2"), DASHBOARD_META_AND_DATA);
        assertTourismDashboard(getDashboard(DASHBOARD_1, EN, "/v2"), DASHBOARD_META_AND_DATA);
    }

    @Test
    void user_can_ask_for_my_dashboard() {
        unAuthorized(restTemplate.exchange("/v2/dashboard/me", GET, null, DASHBOARD_TYPE));
    }

    @Test
    void user_can_save_widgets_for_dashboard_1() {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, ET, "/v2");
        assertTourismDashboard(dashboard, DASHBOARD_META_AND_DATA);
        saveDashboard(dashboard.getRegions().get(1), "/v2");
    }

    @Test
    void user_can_save_region_for_dashboard_1() {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, ET, "/v2");
        assertTourismDashboard(dashboard, DASHBOARD_META_AND_DATA);
        WidgetResponse widget = getTourismTU122Widget(dashboard);
        saveDashboard(widget, "/v2");
    }

    @Test
    void application_returns_social_min_dashboard() {
        assertWelfareDevelopmentPlanDashboard(getDashboard(DASHBOARD_2, ET, "/v2"), DASHBOARD_META_AND_DATA);
        assertWelfareDevelopmentPlanDashboard(getDashboard(DASHBOARD_2, EN, "/v2"), DASHBOARD_META_AND_DATA);
    }

    @Test
    void application_returns_regional_dashboard() {
        assertRegionalStatDashboard(getDashboard(DASHBOARD_3, ET, "/v2"), DASHBOARD_META_AND_DATA);
        assertRegionalStatDashboard(getDashboard(DASHBOARD_3, EN, "/v2"), DASHBOARD_META_AND_DATA);
    }

    @Test
    void application_returns_social_ministry_main_dashboard() {
        assertSocialMinistryDashboard(getDashboard(DASHBOARD_4, ET, "/v2"), DASHBOARD_META);
        assertSocialMinistryDashboard(getDashboard(DASHBOARD_4, EN, "/v2"), DASHBOARD_META);
    }

    private List<RoleDashboardResponse> getRoleDashboards(Language lang) {
        return ok(restTemplate.exchange("/v2/dashboard?language=" + lang.name(), GET, null, LIST_OF_ROLE_DASHBOARDS));
    }
}
