package ee.stat.dashboard.controller.admin;

import ee.stat.dashboard.TestUser;
import ee.stat.dashboard.controller.Response;
import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.service.admin.dto.StatPage;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static ee.stat.dashboard.AssertUtil.LIST_OF_ADMIN_WIDGETS;
import static ee.stat.dashboard.AssertUtil.LIST_OF_GRAPHTYPES;
import static ee.stat.dashboard.AssertUtil.USER_ADMIN;
import static ee.stat.dashboard.AssertUtil.USER_REGULAR;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.AssertUtil.list_size_equals_or_bigger;
import static org.springframework.http.HttpMethod.GET;


class WidgetAdminController_Search_Test extends WidgetAdminController_Common {

    static final String ALL_SOURCES = "sources=EXCEL,DATA_API,DATA_SQL";

    @Test
    void findAll() {
        StatPage<WidgetAdminResponse> data = ok(search("", USER_ADMIN));
        list_size_equals_or_bigger(data.getContent(), 0);
    }

    @Test
    void findAll_with_sources() {
        StatPage<WidgetAdminResponse> data = ok(search("?" + ALL_SOURCES, USER_ADMIN));
        list_size_equals_or_bigger(data.getContent(), 4);
    }

    @Test
    void findAll_search_must_be_atleast_2_chars_long() {
        badRequest(search("?queryForWidget=1&" + ALL_SOURCES, USER_ADMIN));
        ok(search("?queryForWidget=11&" + ALL_SOURCES, USER_ADMIN));

        badRequest(search("?queryForCube=1&" + ALL_SOURCES, USER_ADMIN));
        ok(search("?queryForCube=11&" + ALL_SOURCES, USER_ADMIN));

        badRequest(search("?queryForDashboard=1&" + ALL_SOURCES, USER_ADMIN));
        ok(search("?queryForDashboard=11&" + ALL_SOURCES, USER_ADMIN));
    }

    @Test
    void findAll_search_returns_TU122_widget() {
        StatPage<WidgetAdminResponse> data = ok(search("?queryForWidget=MONTH&" + ALL_SOURCES, USER_ADMIN));
        list_size_equals_or_bigger(data.getContent(), 1);
        StatPage<WidgetAdminResponse> data2 = ok(search("?queryForCube=TU&" + ALL_SOURCES, USER_ADMIN));
        list_size_equals_or_bigger(data2.getContent(), 1);
    }

    @Test
    void application_has_10_graph_types() {
        List<GraphTypeEnum> data = ok(exchange(USER_ADMIN, GET, null, null, null, LIST_OF_GRAPHTYPES, "/graphTypes"));
        list_size(data, 10);
    }

    @Test
    void user_must_be_logged_in_and_have_admin_role() {
        unAuthorized(search("", null));
        forbidden(search("", USER_REGULAR));
    }

    private ResponseEntity<Response<StatPage<WidgetAdminResponse>>> search(String url, TestUser userAdmin) {
        return exchange(userAdmin, GET, url, LIST_OF_ADMIN_WIDGETS);
    }
}
