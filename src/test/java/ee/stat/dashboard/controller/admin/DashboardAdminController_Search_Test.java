package ee.stat.dashboard.controller.admin;

import ee.stat.dashboard.AssertUtil;
import ee.stat.dashboard.controller.Response;
import ee.stat.dashboard.service.admin.dashboard.dto.DashboardAdminResponse;
import ee.stat.dashboard.service.admin.dto.StatPage;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import static ee.stat.dashboard.AssertUtil.DASHBOARD_1;
import static ee.stat.dashboard.AssertUtil.LIST_OF_ADMIN_DASHBOARDS;
import static ee.stat.dashboard.AssertUtil.USER_ADMIN;
import static ee.stat.dashboard.AssertUtil.USER_REGULAR;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.AssertUtil.list_size_equals_or_bigger;
import static ee.stat.dashboard.validator.DashboardValidator.TOURISM_CODE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpMethod.GET;


@SpringBootTest(webEnvironment = RANDOM_PORT)
class DashboardAdminController_Search_Test extends DashboardAdminController_Common {

    @Test
    void findAll() {
        ResponseEntity<Response<StatPage<DashboardAdminResponse>>> response = exchange(USER_ADMIN, GET, null, null, null, LIST_OF_ADMIN_DASHBOARDS);
        StatPage<DashboardAdminResponse> data = ok(response);
        list_size_equals_or_bigger(data.getContent(), 3);
    }

    @Test
    void findAll_search_must_be_atleast_3_chars_long() {
        ResponseEntity<Response<StatPage<DashboardAdminResponse>>> response2 = exchange(USER_ADMIN, GET, null, "1", null, LIST_OF_ADMIN_DASHBOARDS);
        badRequest(response2);
        ResponseEntity<Response<StatPage<DashboardAdminResponse>>> response3 = exchange(USER_ADMIN, GET, null, "11", null, LIST_OF_ADMIN_DASHBOARDS);
        badRequest(response3);
        ResponseEntity<Response<StatPage<DashboardAdminResponse>>> response4 = exchange(USER_ADMIN, GET, null, "111", null, LIST_OF_ADMIN_DASHBOARDS);
        ok(response4);
    }

    @Test
    void findAll_search_returns_sots_min() {
        ResponseEntity<Response<StatPage<DashboardAdminResponse>>> response = exchange(USER_ADMIN, GET, null, "SOTS", null, LIST_OF_ADMIN_DASHBOARDS);
        StatPage<DashboardAdminResponse> data = ok(response);
        list_size(data.getContent(), 1);
        ResponseEntity<Response<StatPage<DashboardAdminResponse>>> response2 = exchange(USER_ADMIN, GET, null, "heaolu", null, LIST_OF_ADMIN_DASHBOARDS);
        StatPage<DashboardAdminResponse> data2 = ok(response2);
        list_size(data2.getContent(), 1);
    }

    @Test
    void findById() {
        ResponseEntity<Response<DashboardAdminResponse>> response = exchange(USER_ADMIN, GET, (long) DASHBOARD_1, null, null, AssertUtil.ADMIN_DASHBOARD);
        DashboardAdminResponse data = ok(response);
        assertEquals(TOURISM_CODE, data.getCode());
    }

    @Test
    void user_must_be_logged_in_and_have_admin_role() {
        ResponseEntity<Response<StatPage<DashboardAdminResponse>>> response = exchange(null, GET, null, null, null, LIST_OF_ADMIN_DASHBOARDS);
        unAuthorized(response);

        ResponseEntity<Response<StatPage<DashboardAdminResponse>>> response2 = exchange(USER_REGULAR, GET, null, null, null, LIST_OF_ADMIN_DASHBOARDS);
        forbidden(response2);
    }
}
