package ee.stat.dashboard.controller.admin;

import ee.stat.dashboard.controller.CommonRestTest;
import ee.stat.dashboard.model.process.enums.LogStatus;
import ee.stat.dashboard.service.admin.dto.StatPage;
import ee.stat.dashboard.service.admin.process.dto.ProcessLogDto;
import ee.stat.dashboard.service.admin.process.dto.StatLogDto;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static ee.stat.dashboard.AssertUtil.STAT_LOGS;
import static ee.stat.dashboard.AssertUtil.STAT_PROCESS_LOG;
import static ee.stat.dashboard.AssertUtil.STAT_PROCESS_LOGS;
import static ee.stat.dashboard.AssertUtil.USER_ADMIN;
import static ee.stat.dashboard.AssertUtil.USER_REGULAR;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpMethod.GET;


@SpringBootTest(webEnvironment = RANDOM_PORT)
class ProcessLogAdminController_Test extends CommonRestTest {

    @Test
    void get_process_log_without_params() {
        assertNotNull(getProcessLogsAdmin(null, null));
    }

    @Test
    void get_process_log_with_page_number() {
        assertNotNull(getProcessLogsAdmin(5, null));
    }

    @Test
    void get_process_log_with_page_size() {
        assertNotNull(getProcessLogsAdmin(null, 500));
    }

    @Test
    void get_process_log_with_page_number_and_size() {
        assertNotNull(getProcessLogsAdmin(5, 500));
    }

    @Test
    void get_specific_process_log() {
        assertNotNull(getSingleProcessLogAdmin(1L));
    }

    @Test
    void get_all_logs_for_specific_process_log_with_processlog_id() {
        assertNotNull(getAllLogsForOneProcessLogIdAdmin(null, null, 1L, null));
    }

    @Test
    void get_all_logs_for_specific_process_log_with_processlog_id_and_page_number() {
        assertNotNull(getAllLogsForOneProcessLogIdAdmin(5, null, 1L, null));
    }

    @Test
    void get_all_logs_for_specific_process_log_with_processlog_id_and_page_number_and_size() {
        assertNotNull(getAllLogsForOneProcessLogIdAdmin(5, 500, 1L, null));
    }

    @Test
    void get_all_logs_for_specific_process_log_with_processlog_id_and_page_number_and_size_and_status() {
        assertNotNull(getAllLogsForOneProcessLogIdAdmin(5, 500, 1L, LogStatus.INFO));
    }

    @Test
    void regular_user_cant_get_process_log() {
        getProcessLogsRegularUser();
    }

    @Test
    void regular_user_cant_get_specific_process_log() {
        getSingleProcessLogRegularUser(7L);
    }

    @Test
    void regular_user_cant_all_logs_for_specific_process_log_with_processlog_id() {
        getAllLogsForOneProcessLogIdRegularUser(7L);
    }

    private StatPage<ProcessLogDto> getProcessLogsAdmin(Integer page, Integer size) {
        String url = "/admin/processlog?";
        if (page != null) url += "&page=" + page;
        if (size != null) url += "&size=" + size;
        return ok(addAuth(USER_ADMIN).exchange(url, GET, null, STAT_PROCESS_LOGS));
    }

    private ProcessLogDto getSingleProcessLogAdmin(Long processLogId) {
        return ok(addAuth(USER_ADMIN).exchange("/admin/processlog/" + processLogId, GET, null, STAT_PROCESS_LOG));
    }

    private StatPage<StatLogDto> getAllLogsForOneProcessLogIdAdmin(Integer page, Integer size, Long processLogId, LogStatus status) {
        String url = "/admin/processlog/" + processLogId + "/logs?";
        if (page != null) url += "&page=" + page;
        if (size != null) url += "&size=" + size;
        if (status != null) url += "&status=" + status;
        return ok(addAuth(USER_ADMIN).exchange(url, GET, null, STAT_LOGS));
    }

    private void getProcessLogsRegularUser() {
        forbidden(addAuth(USER_REGULAR).exchange("/admin/processlog", GET, null, STAT_PROCESS_LOGS));
    }

    private void getSingleProcessLogRegularUser(Long processLogId) {
        forbidden(addAuth(USER_REGULAR).exchange("/admin/processlog/" + processLogId, GET, null, STAT_PROCESS_LOG));
    }

    private void getAllLogsForOneProcessLogIdRegularUser(Long processLogId) {
        forbidden(addAuth(USER_REGULAR).exchange("/admin/processlog/" + processLogId + "/logs", GET, null, STAT_LOGS));
    }
}
