package ee.stat.dashboard.controller.admin;

import ee.stat.dashboard.TestUser;
import ee.stat.dashboard.controller.CommonRestTest;
import ee.stat.dashboard.service.statdata.StatDimensionsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import static java.text.MessageFormat.format;


public abstract class WidgetAdminController_Common extends CommonRestTest {

    @Autowired
    protected StatDimensionsManager manager;

    protected <T, V> ResponseEntity<V> exchange(TestUser user, HttpMethod method, Long id, String name, T body, ParameterizedTypeReference<V> type, String extra) {
        HttpEntity<T> entity = body != null ? new HttpEntity<>(body) : null;
        String url = format("/admin/widget{0}{1}{2}",
                id != null ? ("/" + id) : "",
                name != null ? "?query=" + name : "",
                extra != null ? extra : ""
        );
        return addAuth(user).exchange(url, method, entity, type);
    }

    protected <T, V> ResponseEntity<V> exchange(TestUser user, HttpMethod method, String additionalUrl, ParameterizedTypeReference<V> type) {
        String url = "/admin/widget" + additionalUrl;
        return addAuth(user).exchange(url, method, null, type);
    }
}
