package ee.stat.dashboard.controller.admin;

import ee.stat.dashboard.TestUser;
import ee.stat.dashboard.controller.CommonRestTest;
import ee.stat.dashboard.service.admin.dashboard.dto.DashboardAdminResponse;
import ee.stat.dashboard.service.admin.domain.dto.DomainAdminResponse;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static ee.stat.dashboard.AssertUtil.first;
import static ee.stat.dashboard.AssertUtil.list_size;
import static java.text.MessageFormat.format;
import static org.junit.jupiter.api.Assertions.assertNull;


public abstract class DashboardAdminController_Common extends CommonRestTest {

    protected void validateElements(DashboardAdminResponse savedDashboard, ValidationStrategy strategy) {
        List<DomainAdminResponse> level0 = savedDashboard.getElements();
        list_size(level0, 2);
        list_size(first(level0).getWidgets(), 1);
        List<DomainAdminResponse> level1 = first(level0).getSubElements();
        list_size(level1, strategy.isSave() ? 2 : 3);
        List<DomainAdminResponse> level2 = first(level1).getSubElements();
        list_size(level2, 2);
        assertNull(first(level2).getSubElements());
    }

    protected <T, V> ResponseEntity<V> exchange(TestUser user, HttpMethod method, Long id, String name, T body, ParameterizedTypeReference<V> type) {
        HttpEntity<T> entity = body != null ? new HttpEntity<>(body) : null;
        String url = format("/admin/dashboard{0}{1}",
                id != null ? ("/" + id) : "",
                name != null ? "?name=" + name : ""
        );
        return addAuth(user).exchange(url, method, entity, type);
    }
}
