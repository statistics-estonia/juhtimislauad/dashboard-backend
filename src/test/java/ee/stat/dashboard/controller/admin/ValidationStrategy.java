package ee.stat.dashboard.controller.admin;

public enum ValidationStrategy {

    SAVE, UPDATE;

    public boolean isSave(){
        return this == SAVE;
    }

    public boolean isUpdate(){
        return this == UPDATE;
    }
}
