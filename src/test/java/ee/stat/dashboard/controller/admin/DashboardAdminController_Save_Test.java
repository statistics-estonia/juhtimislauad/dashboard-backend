package ee.stat.dashboard.controller.admin;

import ee.stat.dashboard.controller.Response;
import ee.stat.dashboard.model.dashboard.DashboardStatus;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.repository.WidgetRepository;
import ee.stat.dashboard.service.admin.dashboard.dto.DashboardAdminResponse;
import ee.stat.dashboard.service.admin.dashboard.dto.ElementAdminResponse;
import ee.stat.dashboard.service.admin.domain.dto.DomainAdminResponse;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import ee.stat.dashboard.util.StatListUtil;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

import static ee.stat.dashboard.AssertUtil.ADMIN_DASHBOARD;
import static ee.stat.dashboard.AssertUtil.USER_ADMIN;
import static ee.stat.dashboard.service.widget.widget.WidgetConstants.MEASURE_ET;
import static ee.stat.dashboard.service.widget.widget.WidgetConstants.PROGRAM_ET;
import static ee.stat.dashboard.service.widget.widget.WidgetConstants.RESULT_DOMAIN_ET;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;


class DashboardAdminController_Save_Test extends DashboardAdminController_Common {

    @Autowired
    WidgetRepository widgetRepository;

    @Test
    void save_update_delete() {
        String unique = UUID.randomUUID().toString();
        DashboardAdminResponse newDashboard = newDashboard("Test dashboard " + unique, "Test juhtimislaud " + unique);

        ResponseEntity<Response<DashboardAdminResponse>> savedResponse = exchange(USER_ADMIN, POST, null, null, newDashboard, ADMIN_DASHBOARD);
        DashboardAdminResponse savedDashboard = validate(savedResponse, "Test dashboard ", ValidationStrategy.SAVE);

        Long dashboardId = savedDashboard.getId();
        updateDashboard(unique, savedDashboard);

        ResponseEntity<Response<DashboardAdminResponse>> updatedResponse = exchange(USER_ADMIN, PUT, dashboardId, null, savedDashboard, ADMIN_DASHBOARD);
        validate(updatedResponse, "Test Test dashboard ", ValidationStrategy.UPDATE);

        ResponseEntity<Response<DashboardAdminResponse>> deletedResponse = exchange(USER_ADMIN, DELETE, dashboardId, null, null, ADMIN_DASHBOARD);
        assertEquals(OK, deletedResponse.getStatusCode());

        ResponseEntity<Response<DashboardAdminResponse>> response = exchange(USER_ADMIN, GET, dashboardId, null, null, ADMIN_DASHBOARD);
        assertEquals(BAD_REQUEST, response.getStatusCode());
    }

    private void updateDashboard(String unique, DashboardAdminResponse savedDashboard) {
        savedDashboard.setNameEn("Test Test dashboard " + unique);
        savedDashboard.getElements().get(0).getSubElements().add(domainLvl1());
    }

    private DashboardAdminResponse validate(ResponseEntity<Response<DashboardAdminResponse>> savedResponse, String s, ValidationStrategy save) {
        assertEquals(OK, savedResponse.getStatusCode());
        assertNotNull(savedResponse.getBody());
        DashboardAdminResponse savedDashboard = savedResponse.getBody().getData();
        assertTrue(savedDashboard.getNameEn().startsWith(s));
        validateElements(savedDashboard, save);
        return savedDashboard;
    }

    private DashboardAdminResponse newDashboard(String nameEn, String nameEt) {
        DashboardAdminResponse body = new DashboardAdminResponse();
        body.setStatus(DashboardStatus.HIDDEN);
        body.setNameEn(nameEn);
        body.setNameEt(nameEt);
        body.setRegions(Lists.newArrayList(element(1L), element(2L)));
        body.setElements(Lists.newArrayList(domainLvl0wWidget(), domainLvl0()));
        return body;
    }

    private DomainAdminResponse domainLvl0() {
        DomainAdminResponse e = domain(RESULT_DOMAIN_ET);
        e.setSubElements(Lists.newArrayList(domainLvl1(), domainLvl1()));
        return e;
    }

    private DomainAdminResponse domainLvl0wWidget() {
        DomainAdminResponse e = domain(RESULT_DOMAIN_ET);
        e.setSubElements(Lists.newArrayList(domainLvl1(), domainLvl1()));
        Widget widget = StatListUtil.firstOrNull(widgetRepository.findAll());
        e.setWidgets(Lists.newArrayList(widget(widget != null ? widget.getId() : null)));
        return e;
    }

    private DomainAdminResponse domainLvl1() {
        DomainAdminResponse e = domain(PROGRAM_ET);
        e.setSubElements(Lists.newArrayList(domainLvl2(), domainLvl2()));
        return e;
    }

    private DomainAdminResponse domainLvl2() {
        return domain(MEASURE_ET);
    }

    private DomainAdminResponse domain(String levelNameEt) {
        DomainAdminResponse e = new DomainAdminResponse();
        e.setNameEn("domainNameEn");
        e.setNameEt("domainNameEt");
        e.setLevelNameEt(levelNameEt);
        return e;
    }

    private ElementAdminResponse element(Long id) {
        ElementAdminResponse e = new ElementAdminResponse();
        e.setId(id);
        return e;
    }

    private WidgetAdminResponse widget(Long id) {
        WidgetAdminResponse adminResponse = new WidgetAdminResponse();
        adminResponse.setId(id);
        return adminResponse;
    }
}
