package ee.stat.dashboard.controller.admin;

import ee.stat.dashboard.controller.Response;
import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.model.widget.back.enums.WidgetStatus;
import ee.stat.dashboard.model.widget.front.enums.FilterDisplay;
import ee.stat.dashboard.service.admin.widget.dto.FilterAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.FilterValueAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.GraphTypeAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.StatDataAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlDimension;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlValue;
import org.apache.commons.lang3.BooleanUtils;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static ee.stat.dashboard.AssertUtil.ADMIN_WIDGET_TYPE;
import static ee.stat.dashboard.AssertUtil.LES01;
import static ee.stat.dashboard.AssertUtil.USER_ADMIN;
import static ee.stat.dashboard.AssertUtil._2013_1_1;
import static ee.stat.dashboard.AssertUtil._2023_1_1;
import static ee.stat.dashboard.AssertUtil.first;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.AssertUtil.nth;
import static ee.stat.dashboard.model.widget.back.StatDataType.DATA_API;
import static java.math.BigDecimal.ONE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;


class WidgetAdminController_Save_Test extends WidgetAdminController_Common {

    @Test
    void save_update_delete() throws Exception {
        List<XmlDimension> statDims = manager.getDimensions(LES01, DATA_API);
        ResponseEntity<Response<WidgetAdminResponse>> savedResponse = exchange(USER_ADMIN, POST, null, null, newWidget(statDims), ADMIN_WIDGET_TYPE, null);
        WidgetAdminResponse savedWidget = validate(savedResponse, statDims);
        Long widgetId = savedWidget.getId();

        savedWidget.setDimensions(statDims);
        ResponseEntity<Response<WidgetAdminResponse>> updatedResponse = exchange(USER_ADMIN, PUT, widgetId, null, savedWidget, ADMIN_WIDGET_TYPE, null);
        validate(updatedResponse, statDims);

        ResponseEntity<Response<WidgetAdminResponse>> deletedResponse = exchange(USER_ADMIN, DELETE, widgetId, null, null, ADMIN_WIDGET_TYPE, null);
        assertEquals(OK, deletedResponse.getStatusCode());

        ResponseEntity<Response<WidgetAdminResponse>> response = exchange(USER_ADMIN, GET, widgetId, null, null, ADMIN_WIDGET_TYPE, null);
        assertEquals(BAD_REQUEST, response.getStatusCode());
    }

    private WidgetAdminResponse validate(ResponseEntity<Response<WidgetAdminResponse>> savedResponse, List<XmlDimension> statDims) {
        assertEquals(OK, savedResponse.getStatusCode());
        assertNotNull(savedResponse.getBody());
        WidgetAdminResponse widget = savedResponse.getBody().getData();
        assertEquals("nameen", widget.getCode());
        assertEquals("NameEn", widget.getNameEn());
        assertEquals("NameEt", widget.getNameEt());
        assertEquals("ShortnameEn", widget.getShortnameEn());
        assertEquals("ShortnameEt", widget.getShortnameEt());
        assertEquals("DescriptionEn", widget.getDescriptionEn());
        assertEquals("DescriptionEt", widget.getDescriptionEt());
        assertEquals("NoteEn", widget.getNoteEn());
        assertEquals("NoteEt", widget.getNoteEt());
        assertEquals("UnitEn", widget.getUnitEn());
        assertEquals("UnitEt", widget.getUnitEt());
        assertEquals("SourceEn", widget.getSourceEn());
        assertEquals("SourceEt", widget.getSourceEt());
        assertEquals(ONE, widget.getDivisor());
        assertEquals(10, (int) widget.getPeriods());
        assertEquals(_2013_1_1, widget.getStartDate());
        assertEquals(_2023_1_1, widget.getEndDate());
        assertEquals("MethodsLinkEn", widget.getMethodsLinkEn());
        assertEquals("MethodsLinkEt", widget.getMethodsLinkEt());
        assertEquals("StatisticianJobLinkEn", widget.getStatisticianJobLinkEn());
        assertEquals("StatisticianJobLinkEt", widget.getStatisticianJobLinkEt());
        assertNotNull(widget.getStatDb());
        assertNotNull(widget.getStatDb().getId());
        assertEquals(LES01, widget.getStatDb().getCube());
        list_size(widget.getGraphTypes(), 2);
        GraphTypeAdminDto line = widget.getGraphTypes().get(0);
        assertEquals(GraphTypeEnum.line, line.getType());
        assertTrue(line.getDefaultType());
        assertFilters(statDims, line);

        GraphTypeAdminDto bar = widget.getGraphTypes().get(1);
        assertEquals(GraphTypeEnum.bar, bar.getType());
        assertNull(bar.getDefaultType());
        assertFilters(statDims, bar);
        return widget;
    }

    private void assertFilters(List<XmlDimension> statDims, GraphTypeAdminDto line) {
        int index = 0;
        validateFilterValue(statDims, line, index);
        index++;
        validateFilterValue(statDims, line, index);
        index++;
        validateFilterValue(statDims, line, index);
        index++;
        validateFilterValue(statDims, line, index);
        index++;
        validateFilterValue(statDims, line, index);
    }

    private void validateFilterValue(List<XmlDimension> statDims, GraphTypeAdminDto line, int index) {
        FilterAdminDto localFilter = nth(line.getFilters(), index);
        XmlDimension statFilter = nth(statDims, index);
        assertEquals(localFilter.getNameEt(), statFilter.getNameEt());
        if (BooleanUtils.isNotTrue(localFilter.getTime())) {
            list_size(localFilter.getValues(), 1);
            assertEquals(first(localFilter.getValues()).getValueEt(), first(statFilter.getValues()).getValueEt());
        }
    }

    private WidgetAdminResponse newWidget(List<XmlDimension> statDims) {
        WidgetAdminResponse body = new WidgetAdminResponse();
        body.setCode("Code");
        body.setStatus(WidgetStatus.VISIBLE);
        body.setTimePeriod(TimePeriod.YEAR);
        body.setPrecisionScale(0);
        body.setNameEn("NameEn");
        body.setNameEt("NameEt");
        body.setShortnameEn("ShortnameEn");
        body.setShortnameEt("ShortnameEt");
        body.setDescriptionEn("DescriptionEn");
        body.setDescriptionEt("DescriptionEt");
        body.setNoteEn("NoteEn");
        body.setNoteEt("NoteEt");
        body.setUnitEn("UnitEn");
        body.setUnitEt("UnitEt");
        body.setSourceEn("SourceEn");
        body.setSourceEt("SourceEt");
        body.setDivisor(ONE);
        body.setPeriods(10);
        body.setStartDate(_2013_1_1);
        body.setEndDate(_2023_1_1);
        body.setMethodsLinkEn("MethodsLinkEn");
        body.setMethodsLinkEt("MethodsLinkEt");
        body.setStatisticianJobLinkEn("StatisticianJobLinkEn");
        body.setStatisticianJobLinkEt("StatisticianJobLinkEt");
        StatDataAdminDto statDb = new StatDataAdminDto();
        statDb.setCube(LES01);
        body.setStatDb(statDb);
        body.setDimensions(statDims);

        GraphTypeAdminDto line = new GraphTypeAdminDto();
        line.setType(GraphTypeEnum.line);
        line.setDefaultType(true);
        FilterAdminDto time = filter(statDims.get(4), FilterDisplay.AXIS, false);
        time.setTime(true);
        line.setFilters(filters(statDims, time));

        GraphTypeAdminDto bar = new GraphTypeAdminDto();
        bar.setType(GraphTypeEnum.bar);
        bar.setFilters(filters(statDims, time));
        body.setGraphTypes(Lists.newArrayList(line, bar));
        return body;
    }

    private ArrayList<FilterAdminDto> filters(List<XmlDimension> statDims, FilterAdminDto time) {
        return Lists.newArrayList(
                filter(statDims.get(0), FilterDisplay.LEGEND),
                filter(statDims.get(1), FilterDisplay.CONSTRAINT),
                filter(statDims.get(2), FilterDisplay.MENU),
                filter(statDims.get(3), FilterDisplay.CONSTRAINT),
                time
        );
    }

    private FilterAdminDto filter(XmlDimension dimension, FilterDisplay type) {
        return filter(dimension, type, true);
    }

    private FilterAdminDto filter(XmlDimension dimension, FilterDisplay type, boolean values) {
        FilterAdminDto filter = new FilterAdminDto();
        filter.setNameEt(dimension.getNameEt());
        filter.setNameEn(dimension.getNameEn());
        filter.setType(type);
        if (type.isMenu()) {
            filter.setNumberOfValues(1);
        }
        if (values) {
            filter.setValues(Lists.newArrayList(filterValue(dimension)));
        }
        return filter;
    }

    private FilterValueAdminDto filterValue(XmlDimension xmlDimension) {
        FilterValueAdminDto filterValue = new FilterValueAdminDto();
        XmlValue xmlValue = xmlDimension.getValues().get(0);
        filterValue.setValueEt(xmlValue.getValueEt());
        filterValue.setValueEn(xmlValue.getValueEn());
        return filterValue;
    }
}
