package ee.stat.dashboard.controller.admin;

import ee.stat.dashboard.controller.CommonRestTest;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlDimension;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

import static ee.stat.dashboard.AssertUtil.EKS31;
import static ee.stat.dashboard.AssertUtil.LES01;
import static ee.stat.dashboard.AssertUtil.LIST_OF_XML_DIMENSIONS;
import static ee.stat.dashboard.AssertUtil.USER_ADMIN;
import static ee.stat.dashboard.AssertUtil.USER_REGULAR;
import static java.util.stream.Collectors.partitioningBy;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.BooleanUtils.isTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpMethod.GET;


@SpringBootTest(webEnvironment = RANDOM_PORT)
class StatMetaAdminController_Test extends CommonRestTest {

    @Test
    void get_xml_dimensions() {
        List<XmlDimension> xmlDimensions = getJsonDimensions(LES01);

        assertEquals(5, xmlDimensions.size());
        Map<Boolean, List<XmlDimension>> collect = xmlDimensions.stream().collect(partitioningBy(d -> isTrue(d.getTime())));
        List<XmlDimension> timeDimensions = collect.get(true);
        assertEquals(1, timeDimensions.size());
        List<XmlDimension> dimensions = collect.get(false);
        assertEquals(4, dimensions.size());

        List<String> dimensionNames = dimensions.stream().map(XmlDimension::getNameEt).collect(toList());
        assertEquals("Näitaja", dimensionNames.get(0));
        assertEquals("Sugu", dimensionNames.get(1));
        assertEquals("Väärtus/standardviga", dimensionNames.get(2));
        assertEquals("Vanuserühm", dimensionNames.get(3));

    }

    @Test
    void regular_user_cant_get_xml_dimensions() {
        getJsonDimensionsRegularUser(EKS31);
    }

    private List<XmlDimension> getJsonDimensions(String code) {
        return ok(addAuth(USER_ADMIN).exchange("/admin/stat/" + code + "/dimensions", GET, null, LIST_OF_XML_DIMENSIONS));
    }

    private void getJsonDimensionsRegularUser(String code) {
        forbidden(addAuth(USER_REGULAR).exchange("/admin/stat/" + code + "/dimensions", GET, null, LIST_OF_XML_DIMENSIONS));
    }
}
