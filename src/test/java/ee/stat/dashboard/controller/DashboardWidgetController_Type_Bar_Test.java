package ee.stat.dashboard.controller;

import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import org.junit.jupiter.api.Test;

class DashboardWidgetController_Type_Bar_Test extends DashboardWidgetController_Common {

    static final GraphTypeEnum GRAPH_TYPE = GraphTypeEnum.bar;

    @Test
    void application_returns_tourism_accommodation_widget() {
        tourism_TU122_BadRequest(GRAPH_TYPE);
    }

    @Test
    void application_returns_tourism_EKS31_widget() {
        tourism_EKS31_BadRequest(GRAPH_TYPE);
    }

    @Test
    void application_returns_tourism_EKS21_widget() {
        tourism_EKS21_BadRequest(GRAPH_TYPE);
    }

    @Test
    void application_returns_tourism_RAA0012_widget() {
        tourism_RAA0012_widget(GRAPH_TYPE);
    }

    @Test
    void application_returns_tourism_PA001_widget() {
        tourism_PA001_widget(GRAPH_TYPE);
    }

    @Test
    void application_returns_tourism_PA006_widget() {
        tourism_PA006_widget(GRAPH_TYPE);
    }

    @Test
    void application_returns_tourism_TU51_widget() {
        tourism_TU51_BadRequest(GRAPH_TYPE);
    }

    @Test
    void application_returns_tourism_LES01_widget() {
        tourism_LES01_BadRequest(GRAPH_TYPE);
    }

    @Test
    void application_returns_tourism_TT065_widget() {
        tourism_TT065_BadRequest(GRAPH_TYPE);
    }
}
