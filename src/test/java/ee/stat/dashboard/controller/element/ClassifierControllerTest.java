package ee.stat.dashboard.controller.element;

import ee.stat.dashboard.controller.CommonRestTest;
import ee.stat.dashboard.model.classifier.ClassifierCode;
import ee.stat.dashboard.service.element.dto.ClassifierResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;

import java.util.List;

import static ee.stat.dashboard.AssertUtil.LIST_OF_CLASSIFIERS;
import static ee.stat.dashboard.AssertUtil.NR_OF_ROLE_CLASSIFIERS;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.AssertUtil.nth;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.http.HttpMethod.GET;

class ClassifierControllerTest extends CommonRestTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void findAll() {
        List<ClassifierResponse> elements = getRoles();
        list_size(elements, NR_OF_ROLE_CLASSIFIERS);
        clfWElements(nth(elements, 0), ClassifierCode.BUSINESS, 9);
        clfNoElements(nth(elements, 1));
        clfWElements(nth(elements, 2), ClassifierCode.GOV, 12);
    }

    private void clfNoElements(ClassifierResponse ehak) {
        assertEquals(ClassifierCode.EHAK, ehak.getCode());
        assertNull(ehak.getElements());
    }

    private void clfWElements(ClassifierResponse clf, ClassifierCode code, int size) {
        assertEquals(code, clf.getCode());
        list_size(clf.getElements(), size);
    }

    private List<ClassifierResponse> getRoles() {
        return ok(restTemplate.exchange("/classifier/roles", GET, null, LIST_OF_CLASSIFIERS));
    }
}
