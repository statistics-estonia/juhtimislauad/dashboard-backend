package ee.stat.dashboard.controller.element;

import ee.stat.dashboard.controller.CommonRestTest;
import ee.stat.dashboard.controller.element.dto.ElementStrategy;
import ee.stat.dashboard.controller.element.dto.ParentStrategy;
import ee.stat.dashboard.model.classifier.ClassifierCode;
import ee.stat.dashboard.service.element.dto.ElementResponse;
import org.junit.jupiter.api.Test;

import java.util.List;

import static ee.stat.dashboard.AssertUtil.LIST_OF_ELEMENTS;
import static ee.stat.dashboard.AssertUtil.NR_OF_EHAK_ELEMENTS;
import static ee.stat.dashboard.AssertUtil.list_size;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.http.HttpMethod.GET;

class ElementControllerTest extends CommonRestTest {

    @Test
    void application_returns_ehak_elements() {
        List<ElementResponse> elements = getElements(ClassifierCode.EHAK);
        list_size(elements, NR_OF_EHAK_ELEMENTS);
    }

    @Test
    void application_returns_ehak_elements_by_level() {
        list_size(getFlatElements(0), 1);
        list_size(getFlatElements(1), 15);
        list_size(getFlatElements(2), 79);
    }

    @Test
    void application_returns_ehak_elements_by_parentId() {
        List<ElementResponse> lvl_0_elements = getFlatElements(0);
        list_size(lvl_0_elements, 1);
        ElementResponse wholeCountry = lvl_0_elements.get(0);
        list_size(getChildElements(wholeCountry.getId(), ParentStrategy.INCLUDE), 1 + 15);
        list_size(getChildElements(wholeCountry.getId(), ParentStrategy.EXCLUDE), 15);
    }

    @Test
    void application_returns_ehak_elements_by_level_in_hierarchy() {
        validateWholeCountry(getElementTree(0));
    }

    @Test
    void application_returns_ehak_elements_by_from_whole() {
        validateWholeCountry(getElementTree("Whole"));
    }

    @Test
    void application_returns_ehak_elements_by_from_harju() {
        validateHarjuCounty(getElementTree("HARJU_county"));
    }

    private void validateWholeCountry(List<ElementResponse> lvl_0_elements) {
        validateNested(lvl_0_elements, 1, 15, 79);
    }

    private void validateNested(List<ElementResponse> lvl_0_elements, int lvl1, int lvl2, int lvl3) {
        assertEquals(lvl1, lvl_0_elements.size());
        List<ElementResponse> lvl_1_elements = lvl_0_elements.get(0).getSubElements();
        assertEquals(lvl2, lvl_1_elements.size());
        int count = 0;
        for (ElementResponse element : lvl_1_elements) {
            List<ElementResponse> subElements = element.getSubElements();
            if (subElements != null) {
                count += subElements.size();
            }
        }
        assertEquals(lvl3, count);
    }

    private void validateHarjuCounty(List<ElementResponse> lvl_0_elements) {
        validateNested(lvl_0_elements, 1, 16, 0);
    }

    private List<ElementResponse> getFlatElements(Integer level) {
        String codeString = "?clfCode=" + ClassifierCode.EHAK.name();
        String levelString = "&level=" + level;
        return ok(restTemplate.exchange("/element" + codeString + levelString, GET, null, LIST_OF_ELEMENTS));
    }

    private List<ElementResponse> getChildElements(Long parentId, ParentStrategy parentStrategy) {
        String codeString = "?clfCode=" + ClassifierCode.EHAK.name();
        String parentString = "&parentId=" + parentId + "&parentStrategy=" + parentStrategy.name();
        return ok(restTemplate.exchange("/element" + codeString + parentString, GET, null, LIST_OF_ELEMENTS));
    }

    private List<ElementResponse> getElementTree(Integer level) {
        String codeString = "?clfCode=" + ClassifierCode.EHAK.name();
        String levelString = "&level=" + level;
        String connectionsString = "&elementStrategy=" + ElementStrategy.TREE.name();
        return ok(restTemplate.exchange("/element" + codeString + levelString + connectionsString,
                GET, null, LIST_OF_ELEMENTS));
    }

    private List<ElementResponse> getElementTree(String elementCode) {
        String codeString = "?clfCode=" + ClassifierCode.EHAK.name();
        String levelString = "&elementCode=" + elementCode;
        String connectionsString = "&elementStrategy=" + ElementStrategy.TREE.name();
        return ok(restTemplate.exchange("/element" + codeString + levelString + connectionsString,
                GET, null, LIST_OF_ELEMENTS));
    }

    private List<ElementResponse> getElements(ClassifierCode code) {
        return ok(restTemplate.exchange("/element" + "?clfCode=" + code.name(),
                GET, null, LIST_OF_ELEMENTS));
    }
}
