package ee.stat.dashboard.controller;

import ee.stat.dashboard.TestUser;
import ee.stat.dashboard.model.widget.back.enums.Language;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

@SpringBootTest(webEnvironment = RANDOM_PORT)
public abstract class CommonRestTest {

    @Autowired
    protected TestRestTemplate restTemplate;

    protected <T> T ok(ResponseEntity<Response<T>> response) {
        assertEquals(OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getData());
        return response.getBody().getData();
    }

    protected <T> void badRequest(ResponseEntity<Response<T>> response) {
        assertEquals(BAD_REQUEST, response.getStatusCode());
    }

    protected <T> void unAuthorized(ResponseEntity<Response<T>> response) {
        assertEquals(UNAUTHORIZED, response.getStatusCode());
    }

    protected <T> void forbidden(ResponseEntity<Response<T>> response) {
        assertEquals(FORBIDDEN, response.getStatusCode());
    }

    protected <T> void notFound(ResponseEntity<Response<T>> response) {
        assertEquals(NOT_FOUND, response.getStatusCode());
    }

    protected TestRestTemplate addAuth(TestUser user) {
        return user != null ? restTemplate.withBasicAuth(user.getUsername(), user.getToken()) : restTemplate;
    }

    protected String orEmpty(Language lang) {
        return lang != null ? "language=" + lang.name() : "";
    }

    protected String orEmpty(Long id) {
        return id != null ? id + "" : "";
    }

    protected String orEmpty(String prefix) {
        return prefix != null ? prefix : "";
    }
}
