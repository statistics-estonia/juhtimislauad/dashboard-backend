package ee.stat.dashboard.controller;


import ee.stat.dashboard.TestUser;
import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.dashboard.dto.DashboardResponse;
import ee.stat.dashboard.service.dashboard.dto.UserWidgetsDto;
import ee.stat.dashboard.service.widget.widget.dto.UserWidgetDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import static ee.stat.dashboard.AssertUtil.DASHBOARD_1;
import static ee.stat.dashboard.AssertUtil.DASHBOARD_TYPE;
import static ee.stat.dashboard.AssertUtil.USER_WIDGET_TYPE;
import static ee.stat.dashboard.AssertUtil.WIDGET_TYPE;
import static ee.stat.dashboard.AssertUtil.getTourismEH47UWidget;
import static ee.stat.dashboard.AssertUtil.getTourismEKS21Widget;
import static ee.stat.dashboard.AssertUtil.getTourismEKS31Widget;
import static ee.stat.dashboard.AssertUtil.getTourismLES01Widget;
import static ee.stat.dashboard.AssertUtil.getTourismPA001Widget;
import static ee.stat.dashboard.AssertUtil.getTourismPA006Widget;
import static ee.stat.dashboard.AssertUtil.getTourismRAA0012Widget;
import static ee.stat.dashboard.AssertUtil.getTourismTT065Widget;
import static ee.stat.dashboard.AssertUtil.getTourismTU122Widget;
import static ee.stat.dashboard.AssertUtil.getTourismTU51Widget;
import static ee.stat.dashboard.AssertUtil.getTourismVKT14Widget;
import static ee.stat.dashboard.model.widget.back.enums.Language.EN;
import static ee.stat.dashboard.model.widget.back.enums.Language.ET;
import static ee.stat.dashboard.validator.Widget_EH47UValidator.assertEH47UWidget;
import static ee.stat.dashboard.validator.Widget_EKS21Validator.assertEKS21Widget;
import static ee.stat.dashboard.validator.Widget_EKS31Validator.assertEKS31Widget;
import static ee.stat.dashboard.validator.Widget_LES01Validator.assertLES01Widget;
import static ee.stat.dashboard.validator.Widget_PA001Validator.assertPA001Widget;
import static ee.stat.dashboard.validator.Widget_PA006Validator.assertPA006Widget;
import static ee.stat.dashboard.validator.Widget_RAA0012Validator.assertRAA0012Widget;
import static ee.stat.dashboard.validator.Widget_TT065Validator.assertTT065Widget;
import static ee.stat.dashboard.validator.Widget_TU122Validator.assertTU122Widget;
import static ee.stat.dashboard.validator.Widget_TU51Validator.assertTU51Widget;
import static ee.stat.dashboard.validator.Widget_VKT14Validator.assertVKT14Widget;
import static ee.stat.dashboard.validator.strategy.WidgetValidationStrategy.WIDGET_DATA_DEFAULT;
import static java.text.MessageFormat.format;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.PATCH;

public abstract class DashboardWidgetController_Common extends CommonRestTest {

    protected void tourism_TU122_widget(GraphTypeEnum graphType) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, ET);
        WidgetResponse widget = getTourismTU122Widget(dashboard);
        assertTU122Widget(getWidget(DASHBOARD_1, widget.getId(), ET, graphType), WIDGET_DATA_DEFAULT);
        assertTU122Widget(getWidget(DASHBOARD_1, widget.getId(), ET, graphType), WIDGET_DATA_DEFAULT);
    }

    protected void tourism_TU51_widget(GraphTypeEnum graphType) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, ET);
        WidgetResponse widget = getTourismTU51Widget(dashboard);
        assertTU51Widget(getWidget(DASHBOARD_1, widget.getId(), ET, graphType), WIDGET_DATA_DEFAULT);
        assertTU51Widget(getWidget(DASHBOARD_1, widget.getId(), EN, graphType), WIDGET_DATA_DEFAULT);
    }

    protected void tourism_EKS31_widget(GraphTypeEnum graphType) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, ET);
        WidgetResponse widget = getTourismEKS31Widget(dashboard);
        assertEKS31Widget(getWidget(DASHBOARD_1, widget.getId(), ET, graphType), WIDGET_DATA_DEFAULT);
        assertEKS31Widget(getWidget(DASHBOARD_1, widget.getId(), EN, graphType), WIDGET_DATA_DEFAULT);
    }

    protected void tourism_EKS21_widget(GraphTypeEnum graphType) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, ET);
        WidgetResponse widget = getTourismEKS21Widget(dashboard);
        assertEKS21Widget(getWidget(DASHBOARD_1, widget.getId(), ET, graphType), WIDGET_DATA_DEFAULT);
        assertEKS21Widget(getWidget(DASHBOARD_1, widget.getId(), EN, graphType), WIDGET_DATA_DEFAULT);
    }

    protected void tourism_LES01_widget(GraphTypeEnum graphType) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, ET);
        WidgetResponse widget = getTourismLES01Widget(dashboard);
        assertLES01Widget(getWidget(DASHBOARD_1, widget.getId(), ET, graphType), WIDGET_DATA_DEFAULT);
        assertLES01Widget(getWidget(DASHBOARD_1, widget.getId(), EN, graphType), WIDGET_DATA_DEFAULT);
    }

    protected void tourism_VKT14_widget(GraphTypeEnum graphType) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, ET);
        WidgetResponse widget = getTourismVKT14Widget(dashboard);
        assertVKT14Widget(getWidget(DASHBOARD_1, widget.getId(), ET, graphType), WIDGET_DATA_DEFAULT);
        assertVKT14Widget(getWidget(DASHBOARD_1, widget.getId(), EN, graphType), WIDGET_DATA_DEFAULT);
    }

    protected void tourism_TT065_widget(GraphTypeEnum graphType, Long ehakId) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, ET);
        WidgetResponse widget = getTourismTT065Widget(dashboard);
        assertTT065Widget(getWidget(DASHBOARD_1, widget.getId(), ET, graphType, ehakId),
                WIDGET_DATA_DEFAULT, ehakId);
        assertTT065Widget(getWidget(DASHBOARD_1, widget.getId(), EN, graphType, ehakId),
                WIDGET_DATA_DEFAULT, ehakId);
    }

    protected void tourism_RAA0012_widget(GraphTypeEnum graphType) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, ET);
        WidgetResponse widget = getTourismRAA0012Widget(dashboard);
        assertRAA0012Widget(getWidget(DASHBOARD_1, widget.getId(), ET, graphType),
                WIDGET_DATA_DEFAULT);
        assertRAA0012Widget(getWidget(DASHBOARD_1, widget.getId(), EN, graphType),
                WIDGET_DATA_DEFAULT);
    }

    protected void tourism_PA001_widget(GraphTypeEnum graphType) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, ET);
        WidgetResponse widget = getTourismPA001Widget(dashboard);
        assertPA001Widget(getWidget(DASHBOARD_1, widget.getId(), ET, graphType),
                WIDGET_DATA_DEFAULT);
        assertPA001Widget(getWidget(DASHBOARD_1, widget.getId(), EN, graphType),
                WIDGET_DATA_DEFAULT);
    }

    protected void tourism_PA006_widget(GraphTypeEnum graphType) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, ET);
        WidgetResponse widget = getTourismPA006Widget(dashboard);
        assertPA006Widget(getWidget(DASHBOARD_1, widget.getId(), ET, graphType),
                WIDGET_DATA_DEFAULT);
        assertPA006Widget(getWidget(DASHBOARD_1, widget.getId(), EN, graphType),
                WIDGET_DATA_DEFAULT);
    }

    protected void tourism_EH47U_widget(GraphTypeEnum graphType, Long ehakId) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, ET);
        WidgetResponse widget = getTourismEH47UWidget(dashboard);
        assertEH47UWidget(getWidget(DASHBOARD_1, widget.getId(), ET, graphType, ehakId), WIDGET_DATA_DEFAULT);
        assertEH47UWidget(getWidget(DASHBOARD_1, widget.getId(), EN, graphType, ehakId), WIDGET_DATA_DEFAULT);
    }

    protected void tourism_TU122_BadRequest(GraphTypeEnum graphType) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, ET);
        WidgetResponse widget = getTourismTU122Widget(dashboard);
        getWidget_badRequest(DASHBOARD_1, widget.getId(), ET, graphType);
    }

    protected void tourism_EKS31_BadRequest(GraphTypeEnum graphType) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, ET);
        WidgetResponse widget = getTourismEKS31Widget(dashboard);
        getWidget_badRequest(DASHBOARD_1, widget.getId(), ET, graphType);
    }

    protected void tourism_EKS21_BadRequest(GraphTypeEnum graphType) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, ET);
        WidgetResponse widget = getTourismEKS31Widget(dashboard);
        getWidget_badRequest(DASHBOARD_1, widget.getId(), ET, graphType);
    }

    protected void tourism_TU51_BadRequest(GraphTypeEnum graphType) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, ET);
        WidgetResponse widget = getTourismTU51Widget(dashboard);
        getWidget_badRequest(DASHBOARD_1, widget.getId(), ET, graphType);
    }

    protected void tourism_TT065_BadRequest(GraphTypeEnum graphType) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, ET);
        WidgetResponse widget = getTourismTT065Widget(dashboard);
        getWidget_badRequest(DASHBOARD_1, widget.getId(), ET, graphType);
    }

    protected void tourism_LES01_BadRequest(GraphTypeEnum graphType) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, ET);
        WidgetResponse widget = getTourismLES01Widget(dashboard);
        getWidget_badRequest(DASHBOARD_1, widget.getId(), ET, graphType);
    }

    protected void travelling_BadRequest(GraphTypeEnum graphType) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, ET);
        WidgetResponse widget = getTourismTU51Widget(dashboard);
        getWidget_badRequest(DASHBOARD_1, widget.getId(), ET, graphType);
    }

    protected WidgetResponse getWidget(Long dashboard, Long id, Language language, String params) {
        return getWidget(dashboard, id, language, GraphTypeEnum.line, null, params, null);
    }

    protected WidgetResponse getWidget(Long dashboard, Long id, Language language, String params, GraphTypeEnum graphType) {
        return getWidget(dashboard, id, language, graphType, null, params, null);
    }

    protected WidgetResponse getWidget(Long dashboard, Long id, Language language, GraphTypeEnum graphType) {
        return getWidget(dashboard, id, language, graphType, null, null, null);
    }

    protected WidgetResponse getWidget(Long dashboard, Long id, Language language, GraphTypeEnum graphType, TestUser testUser) {
        return getWidget(dashboard, id, language, graphType, null, null, testUser);
    }

    protected WidgetResponse getWidget(Long dashboard, Long id, Language language, GraphTypeEnum graphType, Long ehakId) {
        return getWidget(dashboard, id, language, graphType, ehakId, null, null);
    }

    protected WidgetResponse getWidget(Long dashboard, Long id, Language language, GraphTypeEnum type, Long ehakId, String params, TestUser user) {
        return ok(exchangeWidget(GET, dashboard, id, language, ehakId, type, params, user));
    }

    protected void getWidget_badRequest(Long dashboard, Long id, Language language, GraphTypeEnum graphType) {
        badRequest(exchangeWidget(GET, dashboard, id, language, null, graphType, null, null));
    }

    protected void getWidget_notFound(Long dashboard, Long id, Language language, GraphTypeEnum graphType, TestUser testUser) {
        notFound(exchangeWidget(GET, dashboard, id, language, null, graphType, null, testUser));
    }

    protected ResponseEntity<Response<UserWidgetDto>> updatePreferences(TestUser testUser, long dashboard, Long id, UserWidgetDto dto) {
        return addAuth(testUser)
                .exchange(format("/dashboard/{0}/widget/{1}/preferences", dashboard, id),
                        PATCH, new HttpEntity<>(dto), USER_WIDGET_TYPE);
    }

    protected ResponseEntity<Response<UserWidgetDto>> updatePin(TestUser testUser, long dashboard, Long id, UserWidgetDto dto) {
        return addAuth(testUser)
                .exchange(format("/dashboard/{0}/widget/{1}/pin", dashboard, id),
                        PATCH, new HttpEntity<>(dto), USER_WIDGET_TYPE);
    }

    protected DashboardResponse getDashboard(Long id, Language lang) {
        return ok(exchangeDashboard(GET, id, lang, null, null, null));
    }

    protected DashboardResponse getMyDashboard(Language lang, TestUser user) {
        return ok(exchangeDashboard(GET, null, lang, user, null, "/me"));
    }

    protected DashboardResponse patchDashboard(Long id, Language lang, TestUser user, UserWidgetsDto saveDto) {
        return ok(exchangeDashboard(PATCH, id, lang, user, saveDto, "/widgets"));
    }

    private ResponseEntity<Response<WidgetResponse>> exchangeWidget(HttpMethod method, Long dashboard, Long id, Language language, Long ehakId, GraphTypeEnum type, String params, TestUser user) {
        String url = format("/dashboard/{0}/widget/{1}?", dashboard, id);
        if (language != null) url += "&language=" + language.name();
        if (type != null) url += "&graphType=" + type.name();
        if (ehakId != null) url += "&ehakId=" + ehakId;
        if (params != null) url += "&" + params;
        return addAuth(user).exchange(url, method, null, WIDGET_TYPE);
    }

    private <T> ResponseEntity<Response<DashboardResponse>> exchangeDashboard(HttpMethod method, Long id, Language lang, TestUser user, T body, String extra) {
        HttpEntity<T> entity = body != null ? new HttpEntity<>(body) : null;
        String url = format("/v2/dashboard/{0}{1}?{2}", id != null ? id : "", extra != null ? extra : "", lang != null ? "language=" + lang.name() : "");
        return addAuth(user).exchange(url, method, entity, DASHBOARD_TYPE);
    }
}
