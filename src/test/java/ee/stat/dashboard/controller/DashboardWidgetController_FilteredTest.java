package ee.stat.dashboard.controller;

import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.dashboard.dto.DashboardResponse;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import org.junit.jupiter.api.Test;

import static ee.stat.dashboard.AssertUtil.DASHBOARD_1;
import static ee.stat.dashboard.AssertUtil.getTourismLES01Widget;
import static ee.stat.dashboard.AssertUtil.getTourismTT065Widget;
import static ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum.area;
import static ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum.line;
import static ee.stat.dashboard.model.widget.back.enums.Language.EN;
import static ee.stat.dashboard.model.widget.back.enums.Language.ET;
import static ee.stat.dashboard.validator.Graph_FilterBuilder.createPovertyOneFilter;
import static ee.stat.dashboard.validator.Graph_FilterBuilder.createRegionalStatFilter;
import static ee.stat.dashboard.validator.Graph_FilterBuilder.createRegionalStatMultiFilter;
import static ee.stat.dashboard.validator.Widget_LES01Validator.assertLES01Widget;
import static ee.stat.dashboard.validator.Widget_TT065Validator.assertTT065Widget;
import static ee.stat.dashboard.validator.strategy.WidgetValidationStrategy.WIDGET_DATA_1_FILTER;
import static ee.stat.dashboard.validator.strategy.WidgetValidationStrategy.WIDGET_DATA_2_FILTERS;
import static ee.stat.dashboard.validator.strategy.WidgetValidationStrategy.WIDGET_DATA_DEFAULT;

class DashboardWidgetController_FilteredTest extends DashboardWidgetController_Common {

    @Test
    void application_returns_defaults_on_no_or_wrong_filters() {
        poverty_wrong_filter(ET);
        poverty_wrong_filter(EN);
    }

    @Test
    void application_returns_poverty_widget_filtered() {
        poverty_one_filter(ET);
        poverty_one_filter(EN);
    }

    @Test
    void application_returns_poverty_widget_multi_filtered() {
        poverty_two_filters(ET);
        poverty_two_filters(EN);
    }

    @Test
    void application_returns_tourism_TT065_widget_filtered() {
        regional_stat_one_filter(ET);
        regional_stat_one_filter(EN);
    }

    @Test
    void application_returns_tourism_TT065_widget_multi_filtered() {
        regional_stat_two_filters(ET);
        regional_stat_two_filters(EN);
    }

    private void regional_stat_one_filter(Language et) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, et);
        WidgetResponse widgetSummary = getTourismTT065Widget(dashboard);
        WidgetResponse widget = getWidget(DASHBOARD_1, widgetSummary.getId(), et, area);
        String filters = createRegionalStatFilter(widget, et);

        assertTT065Widget(getWidget(DASHBOARD_1, widget.getId(), et, filters, area), WIDGET_DATA_1_FILTER, null);
    }

    private void regional_stat_two_filters(Language et) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, et);
        WidgetResponse widgetSummary = getTourismTT065Widget(dashboard);
        WidgetResponse widget = getWidget(DASHBOARD_1, widgetSummary.getId(), et, area);
        String filters = createRegionalStatMultiFilter(widget, et);

        assertTT065Widget(getWidget(DASHBOARD_1, widget.getId(), et, filters, area), WIDGET_DATA_2_FILTERS, null);
    }

    private void poverty_wrong_filter(Language lang) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, lang);
        WidgetResponse widgetSummary = getTourismLES01Widget(dashboard);

        assertLES01Widget(getWidget(DASHBOARD_1, widgetSummary.getId(), lang, "random"), WIDGET_DATA_DEFAULT);
    }

    private void poverty_one_filter(Language lang) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, lang);
        WidgetResponse widgetSummary = getTourismLES01Widget(dashboard);
        WidgetResponse widget = getWidget(DASHBOARD_1, widgetSummary.getId(), lang, line);
        String filters = createPovertyOneFilter(widget, lang);

        assertLES01Widget(getWidget(DASHBOARD_1, widget.getId(), lang, filters), WIDGET_DATA_1_FILTER);
    }

    private void poverty_two_filters(Language lang) {
        DashboardResponse dashboard = getDashboard(DASHBOARD_1, lang);
        WidgetResponse widgetSummary = getTourismLES01Widget(dashboard);
        WidgetResponse widget = getWidget(DASHBOARD_1, widgetSummary.getId(), lang, line);
        String filters = createPovertyOneFilter(widget, lang);

        assertLES01Widget(getWidget(DASHBOARD_1, widget.getId(), lang, filters), WIDGET_DATA_2_FILTERS);
    }
}
