package ee.stat.dashboard.controller;

import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import org.junit.jupiter.api.Test;

class DashboardWidgetController_Type_Line_Test extends DashboardWidgetController_Common {

    static final GraphTypeEnum GRAPH_TYPE = GraphTypeEnum.line;

    @Test
    void application_returns_tourism_accommodation_widget() {
        tourism_TU122_BadRequest(GRAPH_TYPE);
    }

    @Test
    void application_returns_tourism_EKS31_widget() {
        tourism_EKS31_widget(GRAPH_TYPE);
    }

    @Test
    void application_returns_tourism_EKS21_widget() {
        tourism_EKS21_widget(GRAPH_TYPE);
    }

    @Test
    void application_returns_tourism_LES01_widget() {
        tourism_LES01_widget(GRAPH_TYPE);
    }

    @Test
    void application_returns_tourism_TU51_widget() {
        travelling_BadRequest(GRAPH_TYPE);
    }

    @Test
    void application_returns_tourism_TT065_widget() {
        tourism_TT065_BadRequest(GRAPH_TYPE);
    }

    @Test
    void application_returns_tourism_VKT14_widget() {
        tourism_VKT14_widget(GRAPH_TYPE);
    }
}
