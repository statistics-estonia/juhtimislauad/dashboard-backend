package ee.stat.dashboard.controller;

import ee.stat.dashboard.controller.user.dto.UserData;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import static ee.stat.dashboard.AssertUtil.USERDATA_TYPE;
import static ee.stat.dashboard.AssertUtil.USER_ADMIN;
import static ee.stat.dashboard.AssertUtil.USER_REGULAR;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

class MeControllerTests extends CommonRestTest {

    @Test
    void visitor_can_not_access_me() {
        ResponseEntity<Response<UserData>> authQuery = addAuth(null).exchange("/me", GET, null, USERDATA_TYPE);
        assertEquals(UNAUTHORIZED, authQuery.getStatusCode());
    }

    @Test
    void user_can_access_me() {
        UserData userData = ok(addAuth(USER_REGULAR).exchange("/me", GET, null, USERDATA_TYPE));
        assertEquals(USER_REGULAR.getFullname(), userData.getUsername());
    }

    @Test
    void admin_can_access_me() {
        UserData userData = ok(addAuth(USER_ADMIN).exchange("/me", GET, null, USERDATA_TYPE));
        assertEquals(USER_ADMIN.getFullname(), userData.getUsername());
    }

}
