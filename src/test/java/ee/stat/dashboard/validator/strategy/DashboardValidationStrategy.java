package ee.stat.dashboard.validator.strategy;

public enum DashboardValidationStrategy {

    ROLE_DASHBOARD, DASHBOARD_META, DASHBOARD_META_AND_DATA;

    public boolean roleDashboard() {
        return this == ROLE_DASHBOARD;
    }

    public boolean onlyMeta() {
        return this == DASHBOARD_META;
    }

    public boolean hasData() {
        return this == DASHBOARD_META_AND_DATA;
    }

}
