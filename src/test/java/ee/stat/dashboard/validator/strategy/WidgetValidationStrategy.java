package ee.stat.dashboard.validator.strategy;

public enum WidgetValidationStrategy {

    WIDGET_DASHBOARD,
    WIDGET_DATA_DEFAULT,
    WIDGET_DATA_1_FILTER,
    WIDGET_DATA_2_FILTERS;

    public boolean dashboard() {
        return this == WIDGET_DASHBOARD;
    }

    public boolean defaultData() {
        return this == WIDGET_DATA_DEFAULT;
    }

    public boolean filteredData() {
        return this == WIDGET_DATA_1_FILTER;
    }

    public boolean filteredDataMultiple() {
        return this == WIDGET_DATA_2_FILTERS;
    }

}
