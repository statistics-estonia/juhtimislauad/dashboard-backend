package ee.stat.dashboard.validator;

import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.front.FilterSerie;
import ee.stat.dashboard.model.widget.front.Period;
import ee.stat.dashboard.model.widget.front.Serie;
import ee.stat.dashboard.service.widget.widget.dto.GraphDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;

import java.util.List;
import java.util.Optional;

import static ee.stat.dashboard.AssertUtil.AGE;
import static ee.stat.dashboard.AssertUtil.AGEGROUPS_TOGETHER;
import static ee.stat.dashboard.AssertUtil.MALES_AND_FEMALES;
import static ee.stat.dashboard.AssertUtil.SEX;
import static ee.stat.dashboard.AssertUtil._2011_12_31;
import static ee.stat.dashboard.AssertUtil._2018_10_1;
import static ee.stat.dashboard.AssertUtil._2031_1_1;
import static ee.stat.dashboard.AssertUtil.assertFilters;
import static ee.stat.dashboard.AssertUtil.assertValue;
import static ee.stat.dashboard.AssertUtil.filterHas;
import static ee.stat.dashboard.AssertUtil.first;
import static ee.stat.dashboard.AssertUtil.last;
import static ee.stat.dashboard.AssertUtil.map_size;
import static ee.stat.dashboard.common.TestFilter.preciseFilter;
import static ee.stat.dashboard.model.json.Source.STAT_DATA_API;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Graph_Timed_EKS31Validator {

    public static void assertLineGraph(WidgetResponse widget) {
        Language lang = widget.getLang();
        GraphDto graph = widget.getDiagram();
        assertPeriods(graph);
        assertFilters(graph.getFilters(),
                preciseFilter(SEX.name(lang), 3),
                preciseFilter(AGE.name(lang), 4));
        allAgeGroupsAndGenders(graph, lang);
    }

    private static void allAgeGroupsAndGenders(GraphDto graph, Language lang) {
        Optional<FilterSerie> filterSerieOp = graph.getSeries().stream()
                .filter(f -> filterHas(f, List.of(MALES_AND_FEMALES.name(lang), AGEGROUPS_TOGETHER.name(lang))))
                .findAny();
        assertTrue(filterSerieOp.isPresent());
        FilterSerie filterSerie = filterSerieOp.get();
        map_size(filterSerie.getFilters(), 2);
        List<Serie> series = filterSerie.getSeries();
        assertValue(first(series), STAT_DATA_API);
        assertValue(last(series), STAT_DATA_API);
    }

    private static void assertPeriods(GraphDto graph) {
        Period period = graph.getPeriod();
        assertNotNull(period);
        assertTrue(_2011_12_31.isBefore(period.getFrom()));
        assertTrue(_2018_10_1.isBefore(period.getTo()));
        assertTrue(_2031_1_1.isAfter(period.getTo()));
    }
}
