package ee.stat.dashboard.validator;

import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.front.FilterSerie;
import ee.stat.dashboard.model.widget.front.Serie;
import ee.stat.dashboard.service.widget.widget.dto.GraphDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;

import java.util.List;
import java.util.Optional;

import static ee.stat.dashboard.AssertUtil.ACTIVITY;
import static ee.stat.dashboard.AssertUtil.AGRICULTURE;
import static ee.stat.dashboard.AssertUtil.MANUFACTURING;
import static ee.stat.dashboard.AssertUtil.MINING;
import static ee.stat.dashboard.AssertUtil.assertFilters;
import static ee.stat.dashboard.AssertUtil.assertValue;
import static ee.stat.dashboard.AssertUtil.assertValueSource;
import static ee.stat.dashboard.AssertUtil.filterHas;
import static ee.stat.dashboard.AssertUtil.first;
import static ee.stat.dashboard.AssertUtil.last;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.AssertUtil.map_size;
import static ee.stat.dashboard.AssertUtil.nth;
import static ee.stat.dashboard.common.TestFilter.preciseFilter;
import static ee.stat.dashboard.model.json.Source.STAT_DATA_API;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Graph_Timed_PA001Validator {

    public static void validateGraph(WidgetResponse widget) {
        Language lang = widget.getLang();
        GraphDto graph = widget.getDiagram();
        assertFilters(graph.getFilters(), preciseFilter(ACTIVITY.name(lang), 3));

        list_size(graph.getSeries(), 3);

        Optional<FilterSerie> agriculture = graph.getSeries().stream()
                .filter(f -> filterHas(f, AGRICULTURE.name(lang)))
                .findAny();
        assertTrue(agriculture.isPresent());
        assertSerie(agriculture.get());

        Optional<FilterSerie> mining = graph.getSeries().stream()
                .filter(f -> filterHas(f, MINING.name(lang)))
                .findAny();
        assertTrue(mining.isPresent());
        assertSerie(mining.get());

        Optional<FilterSerie> manufacturing = graph.getSeries().stream()
                .filter(f -> filterHas(f, MANUFACTURING.name(lang)))
                .findAny();
        assertTrue(manufacturing.isPresent());
        assertSerie(manufacturing.get());
    }

    private static void assertSerie(FilterSerie serie) {
        map_size(serie.getFilters(), 1);
        List<Serie> total_017_series = serie.getSeries();
        list_size(total_017_series, 40);
        assertValue(first(total_017_series), STAT_DATA_API);
        assertValue(nth(total_017_series, 3), STAT_DATA_API);
        assertValueSource(last(total_017_series), STAT_DATA_API);
    }
}
