package ee.stat.dashboard.validator;

import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.service.widget.widget.dto.GraphTypeDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import ee.stat.dashboard.validator.strategy.WidgetValidationStrategy;

import java.util.List;

import static ee.stat.dashboard.AssertUtil.VKT14;
import static ee.stat.dashboard.AssertUtil.VKT14_CODE_SQL;
import static ee.stat.dashboard.AssertUtil.VKT14_DESCRIPTION_EN;
import static ee.stat.dashboard.AssertUtil.VKT14_DESCRIPTION_ET;
import static ee.stat.dashboard.AssertUtil.VKT14_METADATA_EN;
import static ee.stat.dashboard.AssertUtil.VKT14_METADATA_ET;
import static ee.stat.dashboard.AssertUtil.VKT14_NAME_EN_SQL;
import static ee.stat.dashboard.AssertUtil.VKT14_NAME_ET;
import static ee.stat.dashboard.AssertUtil.VKT14_NOTE_EN;
import static ee.stat.dashboard.AssertUtil.VKT14_NOTE_ET;
import static ee.stat.dashboard.AssertUtil.VKT14_SHORTNAME_EN;
import static ee.stat.dashboard.AssertUtil.VKT14_SHORTNAME_ET;
import static ee.stat.dashboard.AssertUtil.VKT14_STAT_JOB_EN;
import static ee.stat.dashboard.AssertUtil.VKT14_STAT_JOB_ET;
import static ee.stat.dashboard.AssertUtil.commonWidget;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.validator.Graph_Timed_VKT14Validator.validateGraph;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class Widget_VKT14Validator {

    public static void assertVKT14Widget(WidgetResponse widget, WidgetValidationStrategy strategy) {
        commonWidget(widget);

        assertEquals(VKT14_CODE_SQL, widget.getCode());
        if (!(strategy.dashboard())) {
            assertCommonData(widget);
        }
        if (strategy.dashboard()) {
            assertNull(widget.getDiagram());
            return;
        }

        assertStatData(widget);
        assertGraphTypes(widget);
        validateGraph(widget);
    }

    private static void assertCommonData(WidgetResponse widget) {
        assertEquals(TimePeriod.YEAR, widget.getTimePeriod());
        if (widget.getLang().isEt()) {
            assertEquals(VKT14_SHORTNAME_ET, widget.getShortname());
            assertEquals(VKT14_NAME_ET, widget.getName());
            assertEquals(VKT14_DESCRIPTION_ET, widget.getDescription());
            assertEquals(VKT14_NOTE_ET, widget.getNote());
        } else {
            assertEquals(VKT14_SHORTNAME_EN, widget.getShortname());
            assertEquals(VKT14_NAME_EN_SQL, widget.getName());
            assertEquals(VKT14_DESCRIPTION_EN, widget.getDescription());
            assertEquals(VKT14_NOTE_EN, widget.getNote());
        }
    }

    private static void assertGraphTypes(WidgetResponse widget) {
        GraphTypeDto graphTypes = widget.getGraphTypes();
        assertNotNull(graphTypes);
        assertEquals(List.of(GraphTypeEnum.line), graphTypes.getOptions());
        assertEquals(GraphTypeEnum.line, graphTypes.getDefaultOption());
    }

    private static void assertStatData(WidgetResponse widget) {
        list_size(widget.getStatCubes(), 1);
        assertEquals(VKT14, widget.getStatCubes().get(0).getCube());
        if (widget.getLang().isEt()) {
            assertEquals(VKT14_METADATA_ET, widget.getMethodsLinks().get(0));
            assertEquals(VKT14_STAT_JOB_ET, widget.getStatisticianJobLinks().get(0));
        } else {
            assertEquals(VKT14_METADATA_EN, widget.getMethodsLinks().get(0));
            assertEquals(VKT14_STAT_JOB_EN, widget.getStatisticianJobLinks().get(0));
        }
    }
}
