package ee.stat.dashboard.validator;

import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.front.FilterSerie;
import ee.stat.dashboard.model.widget.front.Serie;
import ee.stat.dashboard.service.widget.widget.dto.GraphDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;

import java.util.List;
import java.util.Optional;

import static ee.stat.dashboard.AssertUtil.INDUSTRIAL_BUILDINGS;
import static ee.stat.dashboard.AssertUtil.REFERENCE_PERIOD;
import static ee.stat.dashboard.AssertUtil.REGION;
import static ee.stat.dashboard.AssertUtil.TOTAL;
import static ee.stat.dashboard.AssertUtil.assertFilters;
import static ee.stat.dashboard.AssertUtil.assertMapType;
import static ee.stat.dashboard.AssertUtil.assertPresent;
import static ee.stat.dashboard.AssertUtil.findSerieByAbscissa;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.common.TestFilter.growingFilter;
import static ee.stat.dashboard.common.TestFilter.preciseFilter;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;

public class Graph_Axis_EH47UValidator {

    public static void assertTreeMapGraph(WidgetResponse widget) {
        Language lang = widget.getLang();
        GraphDto graph = widget.getDiagram();
        assertNull(graph.getPeriod());
        assertMapType(widget, graph, null);
        assertFilters(graph.getFilters(),
                preciseFilter(REGION.name(lang), 1),
                growingFilter(REFERENCE_PERIOD.name(lang), 6));

        List<FilterSerie> filterSeries = graph.getSeries();
        list_size(filterSeries, 1);

        List<Serie> series = filterSeries.get(0).getSeries();
        list_size(series, 3);

        Optional<Serie> totalOp = findSerieByAbscissa(series, TOTAL.name(lang));
        assertFalse(totalOp.isPresent());
        assertPresent(findSerieByAbscissa(series, INDUSTRIAL_BUILDINGS.name(lang)));
    }
}
