package ee.stat.dashboard.validator;

import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.front.FilterSerie;
import ee.stat.dashboard.model.widget.front.Serie;
import ee.stat.dashboard.service.widget.widget.dto.GraphDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;

import java.util.List;
import java.util.Optional;

import static ee.stat.dashboard.AssertUtil.AGE_GROUP;
import static ee.stat.dashboard.AssertUtil.FEMALES;
import static ee.stat.dashboard.AssertUtil.MALES;
import static ee.stat.dashboard.AssertUtil.SEX;
import static ee.stat.dashboard.AssertUtil.TOTAL;
import static ee.stat.dashboard.AssertUtil._0to17_new_en;
import static ee.stat.dashboard.AssertUtil._0to17_new_et;
import static ee.stat.dashboard.AssertUtil.assertFilters;
import static ee.stat.dashboard.AssertUtil.assertValue;
import static ee.stat.dashboard.AssertUtil.assertValueSource;
import static ee.stat.dashboard.AssertUtil.first;
import static ee.stat.dashboard.AssertUtil.getFilterSeries;
import static ee.stat.dashboard.AssertUtil.last;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.AssertUtil.map_size;
import static ee.stat.dashboard.AssertUtil.nth;
import static ee.stat.dashboard.common.TestFilter.preciseFilter;
import static ee.stat.dashboard.model.json.Source.STAT_DATA_API;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Graph_Timed_LES01Validator {

    public static void validateGraph(WidgetResponse widget) {
        Language lang = widget.getLang();
        GraphDto graph = widget.getDiagram();
        assertFilters(graph.getFilters(),
                preciseFilter(AGE_GROUP.name(lang), 3),
                preciseFilter(SEX.name(lang), 3));

        String _0to17 = lang.isEt() ? _0to17_new_et : _0to17_new_en;
        Optional<FilterSerie> females_0to17Op = getFilterSeries(graph.getSeries(), FEMALES.name(lang), _0to17);
        Optional<FilterSerie> males_0to17Op = getFilterSeries(graph.getSeries(), MALES.name(lang), _0to17);
        Optional<FilterSerie> total_0to17Op = getFilterSeries(graph.getSeries(), TOTAL.name(lang), _0to17);
        list_size(graph.getSeries(), 3);

        assertTrue(total_0to17Op.isPresent());
        assertTotal0to17(total_0to17Op.get());

        assertFalse(males_0to17Op.isPresent());

        assertFalse(females_0to17Op.isPresent());
    }

    private static void assertTotal0to17(FilterSerie total_0to17) {
        map_size(total_0to17.getFilters(), 2);
        List<Serie> total_017_series = total_0to17.getSeries();
        list_size(total_017_series, 10);
        assertValue(first(total_017_series), STAT_DATA_API);
        assertValue(nth(total_017_series, 3), STAT_DATA_API);
        assertValueSource(last(total_017_series), STAT_DATA_API);
    }
}
