package ee.stat.dashboard.validator;

import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.front.FilterSerie;
import ee.stat.dashboard.model.widget.front.Serie;
import ee.stat.dashboard.service.widget.widget.dto.GraphDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;

import java.util.List;
import java.util.Optional;

import static ee.stat.dashboard.AssertUtil.BALANCE;
import static ee.stat.dashboard.AssertUtil.EXPORT;
import static ee.stat.dashboard.AssertUtil.FLOW;
import static ee.stat.dashboard.AssertUtil.SERVICE;
import static ee.stat.dashboard.AssertUtil.TOTAL;
import static ee.stat.dashboard.AssertUtil.TRANSPORT;
import static ee.stat.dashboard.AssertUtil.TRANSPORT_AIR;
import static ee.stat.dashboard.AssertUtil.TRANSPORT_SEA;
import static ee.stat.dashboard.AssertUtil.assertFilters;
import static ee.stat.dashboard.AssertUtil.assertValue;
import static ee.stat.dashboard.AssertUtil.first;
import static ee.stat.dashboard.AssertUtil.getFilterSeries;
import static ee.stat.dashboard.AssertUtil.last;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.AssertUtil.list_size_equals_or_bigger;
import static ee.stat.dashboard.AssertUtil.map_size;
import static ee.stat.dashboard.AssertUtil.nth;
import static ee.stat.dashboard.common.TestFilter.preciseFilter;
import static ee.stat.dashboard.model.json.Source.STAT_DATA_SQL;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Graph_Timed_VKT14Validator {

    public static void validateGraph(WidgetResponse widget) {
        Language lang = widget.getLang();
        GraphDto graph = widget.getDiagram();
        assertFilters(graph.getFilters(),
                preciseFilter(FLOW.name(lang), 3),
                preciseFilter(SERVICE.name(lang), 4));

        list_size(graph.getSeries(), 4);

        Optional<FilterSerie> totalOp = getFilterSeries(graph.getSeries(), BALANCE.name(lang), TOTAL.name(lang));
        Optional<FilterSerie> transportOp = getFilterSeries(graph.getSeries(), BALANCE.name(lang), TRANSPORT.name(lang));
        Optional<FilterSerie> transportAirOp = getFilterSeries(graph.getSeries(), BALANCE.name(lang), TRANSPORT_AIR.name(lang));
        Optional<FilterSerie> transportSeaOp = getFilterSeries(graph.getSeries(), BALANCE.name(lang), TRANSPORT_SEA.name(lang));

        assertTrue(totalOp.isPresent());
        assertBalanceTotal(totalOp.get());

        assertTrue(transportOp.isPresent());
        assertTrue(transportAirOp.isPresent());
        assertTrue(transportSeaOp.isPresent());

        Optional<FilterSerie> exportOp = getFilterSeries(graph.getSeries(), EXPORT.name(lang), TOTAL.name(lang));
        assertFalse(exportOp.isPresent());
    }

    private static void assertBalanceTotal(FilterSerie serie) {
        map_size(serie.getFilters(), 2);
        List<Serie> series = serie.getSeries();
        list_size_equals_or_bigger(series, 6);
        assertValue(first(series), STAT_DATA_SQL);
        assertValue(nth(series, 3), STAT_DATA_SQL);
        assertValue(last(series), STAT_DATA_SQL);
    }
}
