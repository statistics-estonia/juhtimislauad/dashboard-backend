package ee.stat.dashboard.validator;

import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.service.widget.widget.dto.GraphTypeDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import ee.stat.dashboard.validator.strategy.WidgetValidationStrategy;

import java.util.List;

import static ee.stat.dashboard.AssertUtil.TT065;
import static ee.stat.dashboard.AssertUtil.TT065_CODE_API;
import static ee.stat.dashboard.AssertUtil.TT065_DESCRIPTION_EN;
import static ee.stat.dashboard.AssertUtil.TT065_DESCRIPTION_ET;
import static ee.stat.dashboard.AssertUtil.TT065_METADATA_EN;
import static ee.stat.dashboard.AssertUtil.TT065_METADATA_ET;
import static ee.stat.dashboard.AssertUtil.TT065_NAME_EN_API;
import static ee.stat.dashboard.AssertUtil.TT065_NAME_ET;
import static ee.stat.dashboard.AssertUtil.TT065_NOTE_EN;
import static ee.stat.dashboard.AssertUtil.TT065_NOTE_ET;
import static ee.stat.dashboard.AssertUtil.TT065_SHORTNAME_EN;
import static ee.stat.dashboard.AssertUtil.TT065_SHORTNAME_ET;
import static ee.stat.dashboard.AssertUtil.TT065_STAT_JOB_EN;
import static ee.stat.dashboard.AssertUtil.TT065_STAT_JOB_ET;
import static ee.stat.dashboard.AssertUtil.commonWidget;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum.area;
import static ee.stat.dashboard.model.widget.back.enums.Language.ET;
import static ee.stat.dashboard.validator.Graph_Axis_TT065Validator.assertMapGraph;
import static ee.stat.dashboard.validator.Graph_Timed_RegionalStatValidator.assertLineGraph;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

public class Widget_TT065Validator {

    public static void assertTT065Widget(WidgetResponse widget, WidgetValidationStrategy strategy, Long ehakId) {
        commonWidget(widget);

        assertEquals(TT065_CODE_API, widget.getCode());
        if (!strategy.dashboard()) {
            assertCommonRegionalStatData(widget);
        }
        if (strategy.dashboard()) {
            assertNull(widget.getDiagram());
            return;
        }

        assertGraphTypes(widget);
        assertStatDbData(widget);
        GraphTypeEnum type = widget.getGraphType();
        if (type.isLineOrBarOrStackedOrArea()) {
            assertLineGraph(widget, strategy, ehakId);
        } else if (type.isMapOrVerticalOrPieOrTreemap()) {
            assertMapGraph(widget, ehakId);
        } else {
            fail("graph type validation is missing");
        }
    }

    private static void assertCommonRegionalStatData(WidgetResponse widget) {

        assertEquals(TimePeriod.MONTH, widget.getTimePeriod());
        if (widget.getLang() == ET) {
            assertEquals(TT065_SHORTNAME_ET, widget.getShortname());
            assertEquals(TT065_NAME_ET, widget.getName());
            assertEquals(TT065_DESCRIPTION_ET, widget.getDescription());
            assertEquals(TT065_NOTE_ET, widget.getNote());
        } else {
            assertEquals(TT065_SHORTNAME_EN, widget.getShortname());
            assertEquals(TT065_NAME_EN_API, widget.getName());
            assertEquals(TT065_DESCRIPTION_EN, widget.getDescription());
            assertEquals(TT065_NOTE_EN, widget.getNote());
        }
    }

    private static void assertStatDbData(WidgetResponse widget) {
        list_size(widget.getStatCubes(), 1);
        assertEquals(TT065, widget.getStatCubes().get(0).getCube());
        list_size(widget.getMethodsLinks(), 1);
        list_size(widget.getStatisticianJobLinks(), 1);
        if (widget.getLang().isEt()) {
            assertEquals(TT065_METADATA_ET, widget.getMethodsLinks().get(0));
            assertEquals(TT065_STAT_JOB_ET, widget.getStatisticianJobLinks().get(0));
        } else {
            assertEquals(TT065_METADATA_EN, widget.getMethodsLinks().get(0));
            assertEquals(TT065_STAT_JOB_EN, widget.getStatisticianJobLinks().get(0));
        }
    }

    private static void assertGraphTypes(WidgetResponse widget) {
        GraphTypeDto graphTypes = widget.getGraphTypes();
        assertNotNull(graphTypes);
        assertEquals(List.of(area), graphTypes.getOptions());
        assertEquals(area, graphTypes.getDefaultOption());
    }

}
