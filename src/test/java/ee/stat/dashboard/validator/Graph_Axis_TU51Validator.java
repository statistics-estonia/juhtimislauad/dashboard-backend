package ee.stat.dashboard.validator;

import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.front.FilterSerie;
import ee.stat.dashboard.model.widget.front.Serie;
import ee.stat.dashboard.service.widget.widget.dto.GraphDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;

import java.util.List;
import java.util.Optional;

import static ee.stat.dashboard.AssertUtil.AREA_REGION;
import static ee.stat.dashboard.AssertUtil.BUSINESS_TRIP;
import static ee.stat.dashboard.AssertUtil.REFERENCE_PERIOD;
import static ee.stat.dashboard.AssertUtil.TOTAL;
import static ee.stat.dashboard.AssertUtil.assertFilters;
import static ee.stat.dashboard.AssertUtil.assertMapType;
import static ee.stat.dashboard.AssertUtil.assertPresent;
import static ee.stat.dashboard.AssertUtil.findSerieByAbscissa;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.common.TestFilter.growingFilter;
import static ee.stat.dashboard.common.TestFilter.preciseFilter;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

public class Graph_Axis_TU51Validator {

    public static void assertMapGraph(WidgetResponse widget) {
        Language lang = widget.getLang();
        GraphDto graph = widget.getDiagram();
        assertNull(graph.getPeriod());
        assertMapType(widget, graph, null);
        if (widget.getGraphType().isPieOrVertical())
            assertFilters(graph.getFilters(), growingFilter(REFERENCE_PERIOD.name(lang), 10));
        else {
            assertFilters(graph.getFilters(), preciseFilter(AREA_REGION.name(lang), 95));
        }

        List<FilterSerie> filterSeries = graph.getSeries();
        list_size(filterSeries, 1);

        List<Serie> series = filterSeries.get(0).getSeries();
        list_size(series, widget.getGraphType().isRepresentedWithoutTotal() ? 4 : widget.getGraphType().isTreemap() ? 19 : 5);

        Optional<Serie> totalOp = findSerieByAbscissa(series, TOTAL.name(lang));
        if (widget.getGraphType().isVertical()) {
            assertPresent(totalOp);
        } else if (widget.getGraphType().isMapOrPieOrTreemap()) {
            assertFalse(totalOp.isPresent());
        } else {
            fail("unknown test condition");
        }
        assertPresent(findSerieByAbscissa(series, BUSINESS_TRIP.name(lang)));
    }
}
