package ee.stat.dashboard.validator;

import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.front.FilterSerie;
import ee.stat.dashboard.model.widget.front.Period;
import ee.stat.dashboard.model.widget.front.Serie;
import ee.stat.dashboard.service.widget.widget.dto.FilterDto;
import ee.stat.dashboard.service.widget.widget.dto.FilterValueDto;
import ee.stat.dashboard.service.widget.widget.dto.GraphDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import ee.stat.dashboard.validator.strategy.WidgetValidationStrategy;

import java.util.List;
import java.util.Optional;

import static ee.stat.dashboard.AssertUtil.AGE_GROUP;
import static ee.stat.dashboard.AssertUtil.ANIJA_MUNICIPALITY;
import static ee.stat.dashboard.AssertUtil.FEMALES;
import static ee.stat.dashboard.AssertUtil.HARJU_COUNTY;
import static ee.stat.dashboard.AssertUtil.MALES_AND_FEMALES;
import static ee.stat.dashboard.AssertUtil.NR_OF_EHAK_ELEMENTS;
import static ee.stat.dashboard.AssertUtil.REGION;
import static ee.stat.dashboard.AssertUtil.SEX;
import static ee.stat.dashboard.AssertUtil.TALLINN;
import static ee.stat.dashboard.AssertUtil.WHOLE;
import static ee.stat.dashboard.AssertUtil._16to24;
import static ee.stat.dashboard.AssertUtil._2018_10_1;
import static ee.stat.dashboard.AssertUtil._2019_2_1;
import static ee.stat.dashboard.AssertUtil.assertFilters;
import static ee.stat.dashboard.AssertUtil.assertValue;
import static ee.stat.dashboard.AssertUtil.assertValueSource;
import static ee.stat.dashboard.AssertUtil.defaultValues;
import static ee.stat.dashboard.AssertUtil.filterHas;
import static ee.stat.dashboard.AssertUtil.first;
import static ee.stat.dashboard.AssertUtil.getFilter;
import static ee.stat.dashboard.AssertUtil.last;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.AssertUtil.map_size;
import static ee.stat.dashboard.common.TestFilter.preciseFilter;
import static ee.stat.dashboard.model.json.Source.STAT_DATA_API;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class Graph_Timed_RegionalStatValidator {

    public static void assertLineGraph(WidgetResponse widget, WidgetValidationStrategy strategy, Long ehakId) {
        Language lang = widget.getLang();
        GraphDto graph = widget.getDiagram();
        assertNotNull(graph);
        assertPeriods(graph);
        int ageGroupVals = widget.getGraphType().isArea() ? 3 : 4;
        assertFilters(graph.getFilters(),
                preciseFilter(AGE_GROUP.name(lang), ageGroupVals),
                preciseFilter(REGION.name(lang), NR_OF_EHAK_ELEMENTS),
                preciseFilter(SEX.name(lang), 3));
        assertDefaultValues(ehakId, lang, graph);

        List<FilterSerie> filterSeries = graph.getSeries();
        if (strategy.filteredData()) {
            list_size(filterSeries, ageGroupVals);
            tallinnFemales16to24(lang, filterSeries);
        } else if (strategy.filteredDataMultiple()) {
            list_size(filterSeries, 4 * ageGroupVals);
            tallinnFemales16to24(lang, filterSeries);
        } else if (strategy.defaultData()) {
            if (ehakId == null || ehakId.equals(1L)) {
                list_size(filterSeries, ageGroupVals);
                wholeRegion16to24(lang, filterSeries, WHOLE.name(lang));
            } else if (ehakId.equals(2L)) {
                list_size(filterSeries, ageGroupVals);
                wholeRegion16to24(lang, filterSeries, HARJU_COUNTY.name(lang));
            } else if (ehakId.equals(3L)) {
                list_size(filterSeries, ageGroupVals);
                wholeRegion16to24(lang, filterSeries, ANIJA_MUNICIPALITY.name(lang));
            } else {
                fail();
            }
        } else {
            list_size(filterSeries, NR_OF_EHAK_ELEMENTS * 3 * 4);
            tallinnFemales16to24(lang, filterSeries);
        }
    }

    private static void assertDefaultValues(Long ehakId, Language lang, GraphDto graph) {
        FilterDto dto = getFilter(graph.getFilters(), REGION.name(lang));
        List<FilterValueDto> defaultValues = defaultValues(dto);
        list_size(defaultValues, 1);
        FilterValueDto defaultValue = defaultValues.get(0);
        if (ehakId == null || ehakId.equals(1L)) {
            assertEquals(WHOLE.name(lang), defaultValue.getOption());
        } else if (ehakId.equals(2L)) {
            assertEquals(HARJU_COUNTY.name(lang), defaultValue.getOption());
        } else if (ehakId.equals(3L)) {
            assertEquals(ANIJA_MUNICIPALITY.name(lang), defaultValue.getOption());
        } else if (ehakId.equals(17L)) {
            assertEquals(TALLINN, defaultValue.getOption());
        } else {
            fail();
        }
    }

    private static void tallinnFemales16to24(Language lang, List<FilterSerie> filterSeries) {
        Optional<FilterSerie> tallinn = filterSeries.stream()
                .filter(s -> filterHas(s, List.of(TALLINN, FEMALES.name(lang), _16to24)))
                .findAny();
        assertTrue(tallinn.isPresent());
        FilterSerie tallinnSerie = tallinn.get();
        map_size(tallinnSerie.getFilters(), 3);
        List<Serie> series = tallinnSerie.getSeries();
        assertValue(first(series), STAT_DATA_API);
        assertValue(last(series), STAT_DATA_API);
    }

    private static void wholeRegion16to24(Language lang, List<FilterSerie> filterSeries, String region) {
        Optional<FilterSerie> filterSerieOp = filterSeries.stream()
                .filter(s -> filterHas(s, List.of(region, MALES_AND_FEMALES.name(lang), _16to24)))
                .findAny();
        assertTrue(filterSerieOp.isPresent());
        FilterSerie filterSerie = filterSerieOp.get();
        map_size(filterSerie.getFilters(), 3);
        List<Serie> series = filterSerie.getSeries();
        assertValue(first(series), STAT_DATA_API);
        assertValueSource(last(series), STAT_DATA_API);
    }

    private static void assertPeriods(GraphDto graph) {
        Period period = graph.getPeriod();
        assertNotNull(period);
        assertTrue(_2019_2_1.isAfter(period.getFrom()));
        assertTrue(_2018_10_1.isBefore(period.getTo()));
    }
}
