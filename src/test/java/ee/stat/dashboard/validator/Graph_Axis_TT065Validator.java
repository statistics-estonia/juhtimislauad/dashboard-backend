package ee.stat.dashboard.validator;

import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.front.FilterSerie;
import ee.stat.dashboard.model.widget.front.Serie;
import ee.stat.dashboard.service.widget.widget.dto.GraphDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import org.apache.commons.lang3.BooleanUtils;

import java.util.List;
import java.util.Optional;

import static ee.stat.dashboard.AssertUtil.AGE_GROUP;
import static ee.stat.dashboard.AssertUtil.ANIJA_MUNICIPALITY;
import static ee.stat.dashboard.AssertUtil.HARJU_COUNTY;
import static ee.stat.dashboard.AssertUtil.SEX;
import static ee.stat.dashboard.AssertUtil.TALLINN;
import static ee.stat.dashboard.AssertUtil.TARTU_COUNTY;
import static ee.stat.dashboard.AssertUtil.TIME;
import static ee.stat.dashboard.AssertUtil.WHOLE;
import static ee.stat.dashboard.AssertUtil.assertFilters;
import static ee.stat.dashboard.AssertUtil.assertMapType;
import static ee.stat.dashboard.AssertUtil.assertPresent;
import static ee.stat.dashboard.AssertUtil.findSerieByAbscissa;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.common.TestFilter.growingFilter;
import static ee.stat.dashboard.common.TestFilter.preciseFilter;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class Graph_Axis_TT065Validator {

    public static void assertMapGraph(WidgetResponse widget, Long ehakId) {
        Language lang = widget.getLang();
        GraphDto graph = widget.getDiagram();
        assertNotNull(graph);
        assertNull(graph.getPeriod());
        assertMapType(widget, graph, ehakId);

        assertFilters(graph.getFilters(),
                preciseFilter(AGE_GROUP.name(lang), 4),
                preciseFilter(SEX.name(lang), 3),
                growingFilter(TIME.name(lang), 13));

        List<FilterSerie> filterSeries = graph.getSeries();
        list_size(filterSeries, 1);
        List<Serie> series = filterSeries.get(0).getSeries();
        assertSeries(widget, ehakId, series, lang);
    }

    private static void assertSeries(WidgetResponse widget, Long ehakId, List<Serie> series, Language lang) {
        if (ehakId == null || ehakId.equals(1L)) {
            list_size(series, widget.getGraphType().isMap() ? 15 : 16);
            if (widget.getGraphType().isVertical()) {
                assertSelectedValue(series, WHOLE.name(lang));
            }
        } else if (ehakId.equals(2L)) {
            list_size(series, 15);
            assertSelectedValue(series, HARJU_COUNTY.name(lang));
            assertRegularValue(series, TARTU_COUNTY.name(lang));
        } else if (ehakId.equals(3L)) {
            list_size(series, 79);
            assertSelectedValue(series, ANIJA_MUNICIPALITY.name(lang));
        } else if (ehakId.equals(17L)) {
            list_size(series, 79);
            assertSelectedValue(series, TALLINN);
        } else {
            fail();
        }
    }

    private static void assertSelectedValue(List<Serie> series, String abscissa) {
        Optional<Serie> serie = findSerieByAbscissa(series, abscissa);
        assertPresent(serie);
        assertTrue(BooleanUtils.isTrue(serie.get().getSelected()));
    }

    private static void assertRegularValue(List<Serie> series, String abscissa) {
        Optional<Serie> serie = findSerieByAbscissa(series, abscissa);
        assertPresent(serie);
        assertFalse(BooleanUtils.isTrue(serie.get().getSelected()));
    }
}
