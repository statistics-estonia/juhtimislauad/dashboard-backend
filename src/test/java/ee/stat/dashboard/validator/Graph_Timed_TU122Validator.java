package ee.stat.dashboard.validator;

import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.front.FilterSerie;
import ee.stat.dashboard.model.widget.front.Period;
import ee.stat.dashboard.model.widget.front.Serie;
import ee.stat.dashboard.service.widget.widget.dto.GraphDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import ee.stat.dashboard.validator.strategy.WidgetValidationStrategy;

import java.util.List;
import java.util.Optional;

import static ee.stat.dashboard.AssertUtil.COUNTY;
import static ee.stat.dashboard.AssertUtil.TALLINN;
import static ee.stat.dashboard.AssertUtil.WHOLE;
import static ee.stat.dashboard.AssertUtil._2011_12_31;
import static ee.stat.dashboard.AssertUtil._2018_10_1;
import static ee.stat.dashboard.AssertUtil.assertFilters;
import static ee.stat.dashboard.AssertUtil.assertValue;
import static ee.stat.dashboard.AssertUtil.filterHas;
import static ee.stat.dashboard.AssertUtil.first;
import static ee.stat.dashboard.AssertUtil.last;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.AssertUtil.map_size;
import static ee.stat.dashboard.common.TestFilter.preciseFilter;
import static ee.stat.dashboard.model.json.Source.STAT_DATA_API;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Graph_Timed_TU122Validator {

    public static void assertLineGraph(WidgetResponse widget, WidgetValidationStrategy strategy) {
        Language lang = widget.getLang();
        GraphDto graph = widget.getDiagram();
        assertPeriods(graph);
        assertFilters(graph.getFilters(), preciseFilter(COUNTY.name(lang), 19));
        assertEquals(TALLINN, graph.getFilters().get(0).getValues().get(2).getOption());

        if (strategy.defaultData()) {
            wholeCountryValues(lang, graph);
        } else {
            tallinnValues(graph);
        }
    }

    private static void tallinnValues(GraphDto graph) {
        List<FilterSerie> filterSeries = graph.getSeries();
        list_size(filterSeries, 1);
        Optional<FilterSerie> tallinnSerieOp = filterSeries.stream().filter(s -> filterHas(s, TALLINN)).findAny();
        assertTrue(tallinnSerieOp.isPresent());
        FilterSerie tallinnSerie = tallinnSerieOp.get();
        map_size(tallinnSerie.getFilters(), 1);
    }

    private static void wholeCountryValues(Language lang, GraphDto graph) {
        List<FilterSerie> filterSeries = graph.getSeries();
        list_size(filterSeries, 1);
        Optional<FilterSerie> wholeSerieOp = filterSeries.stream().filter(s -> filterHas(s, WHOLE.name(lang))).findAny();
        assertTrue(wholeSerieOp.isPresent());
        FilterSerie wholeSerie = wholeSerieOp.get();
        map_size(wholeSerie.getFilters(), 1);
        List<Serie> tallinnSeries = wholeSerie.getSeries();
        list_size(tallinnSeries, 60);
        assertValue(first(tallinnSeries), STAT_DATA_API);
        assertValue(last(tallinnSeries), STAT_DATA_API);
    }

    private static void assertPeriods(GraphDto graph) {
        Period period = graph.getPeriod();
        assertNotNull(period);
        assertTrue(_2011_12_31.isBefore(period.getFrom()));
        assertTrue(_2018_10_1.isBefore(period.getTo()));
    }

}
