package ee.stat.dashboard.validator;

import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.front.FilterSerie;
import ee.stat.dashboard.model.widget.front.Period;
import ee.stat.dashboard.service.widget.widget.dto.GraphDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;

import java.util.List;
import java.util.Optional;

import static ee.stat.dashboard.AssertUtil.BUSINESS_TRIP;
import static ee.stat.dashboard.AssertUtil.TRIP_PURPOSE;
import static ee.stat.dashboard.AssertUtil._2011_12_31;
import static ee.stat.dashboard.AssertUtil._2018_10_1;
import static ee.stat.dashboard.AssertUtil.assertFilters;
import static ee.stat.dashboard.AssertUtil.assertValue;
import static ee.stat.dashboard.AssertUtil.filterHas;
import static ee.stat.dashboard.AssertUtil.first;
import static ee.stat.dashboard.AssertUtil.last;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.AssertUtil.map_size;
import static ee.stat.dashboard.common.TestFilter.preciseFilter;
import static ee.stat.dashboard.model.json.Source.STAT_DATA_API;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Graph_Timed_TU51Validator {

    public static final int EXP = 5;

    public static void assertLineGraph(WidgetResponse widget) {
        Language lang = widget.getLang();
        GraphDto graph = widget.getDiagram();
        assertPeriods(graph);
        int filterOpt1 = widget.getGraphType().isRepresentedWithoutTotal() ? EXP - 1 : EXP;
        assertFilters(graph.getFilters(), preciseFilter(TRIP_PURPOSE.name(lang), filterOpt1));
        businessTripValues(graph, widget);
    }

    private static void businessTripValues(GraphDto graph, WidgetResponse widget) {
        List<FilterSerie> filterSeries = graph.getSeries();
        list_size(filterSeries, widget.getGraphType().isRepresentedWithoutTotal() ? EXP - 1 : EXP);
        Optional<FilterSerie> tallinnSerieOp = filterSeries.stream().filter(s -> filterHas(s, BUSINESS_TRIP.name(widget.getLang()))).findAny();
        assertTrue(tallinnSerieOp.isPresent());
        FilterSerie businessTripSerie = tallinnSerieOp.get();
        map_size(businessTripSerie.getFilters(), 1);
        assertValue(first(businessTripSerie.getSeries()), STAT_DATA_API);
        assertValue(last(businessTripSerie.getSeries()), STAT_DATA_API);
    }

    private static void assertPeriods(GraphDto graph) {
        Period period = graph.getPeriod();
        assertNotNull(period);
        assertTrue(_2011_12_31.isBefore(period.getFrom()));
        assertTrue(_2018_10_1.isBefore(period.getTo()));
        assertTrue(period.getFrom().isBefore(period.getTo()));
    }
}
