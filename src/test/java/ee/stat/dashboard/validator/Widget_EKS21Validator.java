package ee.stat.dashboard.validator;

import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.service.widget.widget.dto.GraphTypeDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import ee.stat.dashboard.validator.strategy.WidgetValidationStrategy;

import java.util.List;

import static ee.stat.dashboard.AssertUtil.EKS21;
import static ee.stat.dashboard.AssertUtil.EKS21_CODE_API;
import static ee.stat.dashboard.AssertUtil.EKS21_DESCRIPTION_EN;
import static ee.stat.dashboard.AssertUtil.EKS21_DESCRIPTION_ET;
import static ee.stat.dashboard.AssertUtil.EKS21_METADATA_EN;
import static ee.stat.dashboard.AssertUtil.EKS21_METADATA_ET;
import static ee.stat.dashboard.AssertUtil.EKS21_NAME_EN;
import static ee.stat.dashboard.AssertUtil.EKS21_NAME_ET;
import static ee.stat.dashboard.AssertUtil.EKS21_NOTE_EN;
import static ee.stat.dashboard.AssertUtil.EKS21_NOTE_ET;
import static ee.stat.dashboard.AssertUtil.EKS21_SHORTNAME_EN;
import static ee.stat.dashboard.AssertUtil.EKS21_SHORTNAME_ET;
import static ee.stat.dashboard.AssertUtil.EKS21_STAT_JOB_EN;
import static ee.stat.dashboard.AssertUtil.EKS21_STAT_JOB_ET;
import static ee.stat.dashboard.AssertUtil.commonWidget;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.validator.Graph_Timed_EKS21Validator.assertLineGraph;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class Widget_EKS21Validator {

    public static void assertEKS21Widget(WidgetResponse widget, WidgetValidationStrategy strategy) {
        commonWidget(widget);
        assertEquals(EKS21_CODE_API, widget.getCode());
        if (!(strategy.dashboard())) {
            assertCommonElectricityData(widget);
        }
        if (strategy.dashboard()) {
            assertNull(widget.getDiagram());
            return;
        }

        assertStatData(widget);
        assertGraphTypes(widget);
        assertLineGraph(widget, widget.getLang());
    }

    private static void assertCommonElectricityData(WidgetResponse widget) {
        assertEquals(TimePeriod.WEEK, widget.getTimePeriod());
        if (widget.getLang().isEt()) {
            assertEquals(EKS21_SHORTNAME_ET, widget.getShortname());
            assertEquals(EKS21_NAME_ET, widget.getName());
            assertEquals(EKS21_DESCRIPTION_ET, widget.getDescription());
            assertEquals(EKS21_NOTE_ET, widget.getNote());
        } else {
            assertEquals(EKS21_SHORTNAME_EN, widget.getShortname());
            assertEquals(EKS21_NAME_EN, widget.getName());
            assertEquals(EKS21_DESCRIPTION_EN, widget.getDescription());
            assertEquals(EKS21_NOTE_EN, widget.getNote());
        }
    }

    private static void assertGraphTypes(WidgetResponse widget) {
        GraphTypeDto graphTypes = widget.getGraphTypes();
        assertNotNull(graphTypes);
        assertEquals(List.of(GraphTypeEnum.line), graphTypes.getOptions());
        assertEquals(GraphTypeEnum.line, graphTypes.getDefaultOption());
    }

    private static void assertStatData(WidgetResponse widget) {
        list_size(widget.getStatCubes(), 1);
        assertEquals(EKS21, widget.getStatCubes().get(0).getCube());
        if (widget.getLang().isEt()) {
            assertEquals(EKS21_METADATA_ET, widget.getMethodsLinks().get(0));
            assertEquals(EKS21_STAT_JOB_ET, widget.getStatisticianJobLinks().get(0));
        } else {
            assertEquals(EKS21_METADATA_EN, widget.getMethodsLinks().get(0));
            assertEquals(EKS21_STAT_JOB_EN, widget.getStatisticianJobLinks().get(0));
        }
    }
}
