package ee.stat.dashboard.validator;

import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.widget.widget.dto.FilterDto;
import ee.stat.dashboard.service.widget.widget.dto.FilterValueDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;

import java.text.MessageFormat;
import java.util.Optional;

import static ee.stat.dashboard.AssertUtil.FEMALES;
import static ee.stat.dashboard.AssertUtil.MALES_AND_FEMALES;
import static ee.stat.dashboard.AssertUtil.REGION;
import static ee.stat.dashboard.AssertUtil.SEX;
import static ee.stat.dashboard.AssertUtil.TALLINN;
import static ee.stat.dashboard.AssertUtil.TARTU_COUNTY;
import static ee.stat.dashboard.AssertUtil.TOTAL;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Graph_FilterBuilder {

    public static String createPovertyOneFilter(WidgetResponse widget, Language lang) {
        Optional<FilterDto> filterOp = getFilter(widget, SEX.name(lang));
        assertTrue(filterOp.isPresent());
        FilterDto filter = filterOp.get();
        Optional<FilterValueDto> valueOp = getValueObj(filter, TOTAL.name(lang));
        assertTrue(valueOp.isPresent());
        return MessageFormat.format("{0,number,#}={1,number,#}", filter.getId(), valueOp.get().getId());
    }

    public static String createRegionalStatFilter(WidgetResponse widget, Language lang) {
        Optional<FilterDto> f1 = getFilter(widget, SEX.name(lang));
        assertTrue(f1.isPresent());
        Optional<FilterValueDto> f1v1 = getValueObj(f1.get(), FEMALES.name(lang));
        assertTrue(f1v1.isPresent());
        Optional<FilterDto> f2 = getFilter(widget, REGION.name(lang));
        assertTrue(f2.isPresent());
        Optional<FilterValueDto> f2v1 = getValueObj(f2.get(), TALLINN);
        assertTrue(f2v1.isPresent());
        return MessageFormat.format("{0,number,#}={1,number,#}&{2,number,#}={3,number,#}",
                f1.get().getId(), f1v1.get().getId(),
                f2.get().getId(), f2v1.get().getId());
    }

    public static String createRegionalStatMultiFilter(WidgetResponse widget, Language lang) {
        Optional<FilterDto> f1 = getFilter(widget, SEX.name(lang));
        assertTrue(f1.isPresent());
        Optional<FilterValueDto> f1v1 = getValueObj(f1.get(), FEMALES.name(lang));
        assertTrue(f1v1.isPresent());
        Optional<FilterValueDto> f1v2 = getValueObj(f1.get(), MALES_AND_FEMALES.name(lang));
        assertTrue(f1v2.isPresent());
        Optional<FilterDto> f2 = getFilter(widget, REGION.name(lang));
        assertTrue(f2.isPresent());
        Optional<FilterValueDto> f2v1 = getValueObj(f2.get(), TALLINN);
        assertTrue(f2v1.isPresent());
        Optional<FilterValueDto> f2v2 = getValueObj(f2.get(), TARTU_COUNTY.name(lang));
        assertTrue(f2v2.isPresent());
        return MessageFormat.format("{0,number,#}={1,number,#},{2,number,#}&{3,number,#}={4,number,#},{5,number,#}",
                f1.get().getId(), f1v1.get().getId(), f1v2.get().getId(),
                f2.get().getId(), f2v1.get().getId(), f2v2.get().getId());
    }

    private static Optional<FilterValueDto> getValueObj(FilterDto filterDto, String value) {
        return filterDto.getValues().stream().filter(e -> e.getOption().equals(value)).findAny();
    }

    private static Optional<FilterDto> getFilter(WidgetResponse widget, String name) {
        return widget.getDiagram().getFilters().stream().filter(f -> f.getType().isMenu() && f.getName().equals(name)).findAny();
    }
}
