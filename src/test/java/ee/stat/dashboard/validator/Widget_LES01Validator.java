package ee.stat.dashboard.validator;

import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.service.widget.widget.dto.GraphTypeDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import ee.stat.dashboard.validator.strategy.WidgetValidationStrategy;

import java.util.List;

import static ee.stat.dashboard.AssertUtil.LES01;
import static ee.stat.dashboard.AssertUtil.LES01_CODE_API;
import static ee.stat.dashboard.AssertUtil.LES01_DESCRIPTION_EN;
import static ee.stat.dashboard.AssertUtil.LES01_DESCRIPTION_ET;
import static ee.stat.dashboard.AssertUtil.LES01_METADATA_EN;
import static ee.stat.dashboard.AssertUtil.LES01_METADATA_ET;
import static ee.stat.dashboard.AssertUtil.LES01_NAME_EN_API;
import static ee.stat.dashboard.AssertUtil.LES01_NAME_ET;
import static ee.stat.dashboard.AssertUtil.LES01_NOTE_EN;
import static ee.stat.dashboard.AssertUtil.LES01_NOTE_ET;
import static ee.stat.dashboard.AssertUtil.LES01_SHORTNAME_EN;
import static ee.stat.dashboard.AssertUtil.LES01_SHORTNAME_ET;
import static ee.stat.dashboard.AssertUtil.LES01_STAT_JOB_EN;
import static ee.stat.dashboard.AssertUtil.LES01_STAT_JOB_ET;
import static ee.stat.dashboard.AssertUtil.commonWidget;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.validator.Graph_Timed_LES01Validator.validateGraph;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class Widget_LES01Validator {

    public static void assertLES01Widget(WidgetResponse widget, WidgetValidationStrategy strategy) {
        commonWidget(widget);

        assertEquals(LES01_CODE_API, widget.getCode());
        if (!(strategy.dashboard())) {
            assertCommonData(widget);
        }
        if (strategy.dashboard()) {
            assertNull(widget.getDiagram());
            return;
        }

        assertStatData(widget);
        assertGraphTypes(widget);
        validateGraph(widget);
    }

    private static void assertCommonData(WidgetResponse widget) {
        assertEquals(TimePeriod.YEAR, widget.getTimePeriod());
        if (widget.getLang().isEt()) {
            assertEquals(LES01_SHORTNAME_ET, widget.getShortname());
            assertEquals(LES01_NAME_ET, widget.getName());
            assertEquals(LES01_DESCRIPTION_ET, widget.getDescription());
            assertEquals(LES01_NOTE_ET, widget.getNote());
        } else {
            assertEquals(LES01_SHORTNAME_EN, widget.getShortname());
            assertEquals(LES01_NAME_EN_API, widget.getName());
            assertEquals(LES01_DESCRIPTION_EN, widget.getDescription());
            assertEquals(LES01_NOTE_EN, widget.getNote());
        }
    }

    private static void assertGraphTypes(WidgetResponse widget) {
        GraphTypeDto graphTypes = widget.getGraphTypes();
        assertNotNull(graphTypes);
        assertEquals(List.of(GraphTypeEnum.line), graphTypes.getOptions());
        assertEquals(GraphTypeEnum.line, graphTypes.getDefaultOption());
    }

    private static void assertStatData(WidgetResponse widget) {
        list_size(widget.getStatCubes(), 1);
        assertEquals(LES01, widget.getStatCubes().get(0).getCube());
        if (widget.getLang().isEt()) {
            assertEquals(LES01_METADATA_ET, widget.getMethodsLinks().get(0));
            assertEquals(LES01_STAT_JOB_ET, widget.getStatisticianJobLinks().get(0));
        } else {
            assertEquals(LES01_METADATA_EN, widget.getMethodsLinks().get(0));
            assertEquals(LES01_STAT_JOB_EN, widget.getStatisticianJobLinks().get(0));
        }
    }
}
