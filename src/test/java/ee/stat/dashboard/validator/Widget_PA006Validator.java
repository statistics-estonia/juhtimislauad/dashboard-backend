package ee.stat.dashboard.validator;

import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.service.widget.widget.dto.GraphTypeDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import ee.stat.dashboard.validator.strategy.WidgetValidationStrategy;

import java.util.List;

import static ee.stat.dashboard.AssertUtil.PA006;
import static ee.stat.dashboard.AssertUtil.PA006_CODE_API;
import static ee.stat.dashboard.AssertUtil.PA006_DESCRIPTION_EN;
import static ee.stat.dashboard.AssertUtil.PA006_DESCRIPTION_ET;
import static ee.stat.dashboard.AssertUtil.PA006_METADATA_EN;
import static ee.stat.dashboard.AssertUtil.PA006_METADATA_ET;
import static ee.stat.dashboard.AssertUtil.PA006_NAME_EN_API;
import static ee.stat.dashboard.AssertUtil.PA006_NAME_ET;
import static ee.stat.dashboard.AssertUtil.PA006_NOTE_EN;
import static ee.stat.dashboard.AssertUtil.PA006_NOTE_ET;
import static ee.stat.dashboard.AssertUtil.PA006_SHORTNAME_EN;
import static ee.stat.dashboard.AssertUtil.PA006_SHORTNAME_ET;
import static ee.stat.dashboard.AssertUtil.PA006_STAT_JOB_EN;
import static ee.stat.dashboard.AssertUtil.PA006_STAT_JOB_ET;
import static ee.stat.dashboard.AssertUtil.commonWidget;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum.bar;
import static ee.stat.dashboard.model.widget.back.enums.Language.ET;
import static ee.stat.dashboard.validator.Graph_Timed_PA006Validator.validateGraph;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class Widget_PA006Validator {

    public static void assertPA006Widget(WidgetResponse widget, WidgetValidationStrategy strategy) {
        commonWidget(widget);

        assertEquals(PA006_CODE_API, widget.getCode());
        if (!strategy.dashboard()) {
            assertCommonRegionalStatData(widget);
        }
        if (strategy.dashboard()) {
            assertNull(widget.getDiagram());
            return;
        }

        assertGraphTypes(widget);
        assertStatDbData(widget);
        validateGraph(widget);
    }

    private static void assertCommonRegionalStatData(WidgetResponse widget) {
        assertEquals(TimePeriod.MONTH, widget.getTimePeriod());
        if (widget.getLang() == ET) {
            assertEquals(PA006_SHORTNAME_ET, widget.getShortname());
            assertEquals(PA006_NAME_ET, widget.getName());
            assertEquals(PA006_DESCRIPTION_ET, widget.getDescription());
            assertEquals(PA006_NOTE_ET, widget.getNote());
        } else {
            assertEquals(PA006_SHORTNAME_EN, widget.getShortname());
            assertEquals(PA006_NAME_EN_API, widget.getName());
            assertEquals(PA006_DESCRIPTION_EN, widget.getDescription());
            assertEquals(PA006_NOTE_EN, widget.getNote());
        }
    }

    private static void assertStatDbData(WidgetResponse widget) {
        list_size(widget.getStatCubes(), 1);
        assertEquals(PA006, widget.getStatCubes().get(0).getCube());
        list_size(widget.getMethodsLinks(), 1);
        list_size(widget.getStatisticianJobLinks(), 1);
        if (widget.getLang().isEt()) {
            assertEquals(PA006_METADATA_ET, widget.getMethodsLinks().get(0));
            assertEquals(PA006_STAT_JOB_ET, widget.getStatisticianJobLinks().get(0));
        } else {
            assertEquals(PA006_METADATA_EN, widget.getMethodsLinks().get(0));
            assertEquals(PA006_STAT_JOB_EN, widget.getStatisticianJobLinks().get(0));
        }
    }

    private static void assertGraphTypes(WidgetResponse widget) {
        GraphTypeDto graphTypes = widget.getGraphTypes();
        assertNotNull(graphTypes);
        assertEquals(List.of(bar), graphTypes.getOptions());
        assertEquals(bar, graphTypes.getDefaultOption());
    }

}
