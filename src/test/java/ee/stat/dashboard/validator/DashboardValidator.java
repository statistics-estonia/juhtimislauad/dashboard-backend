package ee.stat.dashboard.validator;

import ee.stat.dashboard.service.dashboard.dto.DashboardResponse;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import ee.stat.dashboard.validator.strategy.DashboardValidationStrategy;
import ee.stat.dashboard.validator.strategy.WidgetValidationStrategy;

import static ee.stat.dashboard.AssertUtil.getTourismTU51Widget;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.model.dashboard.DashboardType.GLOBAL;
import static ee.stat.dashboard.model.dashboard.DashboardType.REGIONAL;
import static ee.stat.dashboard.validator.Widget_TU51Validator.assertTU51Widget;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class DashboardValidator {

    public static final String TOURISM_CODE = "Tourism dashboard";
    public static final String TURISM_NAME_ET = "Turism";
    public static final String TOURISM_NAME_EN = "Tourism";
    public static final String WELFARE_SOCIAL_MIN_CODE = "Heaolu arengukava 2016–2023";
    public static final String WELFARE_SOCIAL_MIN_NAME_ET = "Heaolu arengukava 2016–2023";
    public static final String WELFARE_SOCIAL_MIN_NAME_EN = "Welfare Development Plan 2016–2023";
    public static final String SOCIAL_MIN_CODE = "sotsiaalministeerium";
    public static final String SOCIAL_MIN_NAME_ET = "Sotsiaalministeerium";
    public static final String SOCIAL_MIN_NAME_EN = "Ministry of Social Affairs";
    public static final String REGIONAL_STAT_CODE = "Piirkondlik statistika";
    public static final String REGIONAL_STAT_NAME_ET = "Piirkondlik statistika";
    public static final String REGIONAL_STAT_NAME_EN = "Regional statistics";
    public static final WidgetValidationStrategy VALIDATION_STRATEGY = WidgetValidationStrategy.WIDGET_DASHBOARD;

    public static void assertTourismDashboard(DashboardResponse dashboard, DashboardValidationStrategy validationStrategy) {
        assertCommon(dashboard);
        assertEquals(REGIONAL, dashboard.getType());
        assertEquals(TOURISM_CODE, dashboard.getCode());
        assertEquals(dashboard.getLang().isEt() ? TURISM_NAME_ET : TOURISM_NAME_EN, dashboard.getName());

        if (validationStrategy.roleDashboard()) {
            return;
        }
        list_size(dashboard.getRegions(), 19);

        WidgetResponse widget = getTourismTU51Widget(dashboard);
        assertTU51Widget(widget, VALIDATION_STRATEGY);
    }

    public static void assertSocialMinistryDashboard(DashboardResponse dashboard, DashboardValidationStrategy validationStrategy) {
        assertCommon(dashboard);
        assertEquals(GLOBAL, dashboard.getType());
        assertNull(dashboard.getRegions());
        assertEquals(SOCIAL_MIN_CODE, dashboard.getCode());
        assertEquals(dashboard.getLang().isEt() ? SOCIAL_MIN_NAME_ET : SOCIAL_MIN_NAME_EN, dashboard.getName());

        if (validationStrategy.roleDashboard()) {
            return;
        }
        list_size(dashboard.getElements(), 0);
    }

    public static void assertWelfareDevelopmentPlanDashboard(DashboardResponse dashboard, DashboardValidationStrategy validationStrategy) {
        assertCommon(dashboard);
        assertEquals(GLOBAL, dashboard.getType());
        assertEquals(WELFARE_SOCIAL_MIN_CODE, dashboard.getCode());
        assertEquals(dashboard.getLang().isEt() ? WELFARE_SOCIAL_MIN_NAME_ET : WELFARE_SOCIAL_MIN_NAME_EN, dashboard.getName());
    }

    public static void assertRegionalStatDashboard(DashboardResponse dashboard, DashboardValidationStrategy validationStrategy) {
        assertCommon(dashboard);
        assertEquals(REGIONAL, dashboard.getType());
        assertEquals(REGIONAL_STAT_CODE, dashboard.getCode());
        assertEquals(dashboard.getLang().isEt() ? REGIONAL_STAT_NAME_ET : REGIONAL_STAT_NAME_EN, dashboard.getName());

        if (validationStrategy.roleDashboard()) {
            return;
        }

        list_size(dashboard.getRegions(), 95);
    }

    private static void assertCommon(DashboardResponse dashboard) {
        assertNotNull(dashboard);
        assertNotNull(dashboard.getId());
        assertNotNull(dashboard.getLang());
    }

}
