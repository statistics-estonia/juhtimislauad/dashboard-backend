package ee.stat.dashboard.validator;

import ee.stat.dashboard.model.classifier.ClassifierCode;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.dashboard.dto.DashboardResponse;
import ee.stat.dashboard.service.dashboard.dto.RoleDashboardResponse;
import ee.stat.dashboard.service.element.dto.ElementResponse;

import java.util.List;

import static ee.stat.dashboard.AssertUtil.NR_OF_ROLE_CLASSIFIERS;
import static ee.stat.dashboard.AssertUtil.first;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.AssertUtil.nth;
import static ee.stat.dashboard.AssertUtil.second;
import static ee.stat.dashboard.validator.DashboardValidator.assertRegionalStatDashboard;
import static ee.stat.dashboard.validator.DashboardValidator.assertSocialMinistryDashboard;
import static ee.stat.dashboard.validator.DashboardValidator.assertTourismDashboard;
import static ee.stat.dashboard.validator.DashboardValidator.assertWelfareDevelopmentPlanDashboard;
import static ee.stat.dashboard.validator.strategy.DashboardValidationStrategy.ROLE_DASHBOARD;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class RoleDashboardValidator {

    public static void validateRoles(List<RoleDashboardResponse> roleDashboards, Language lang) {
        list_size(roleDashboards, NR_OF_ROLE_CLASSIFIERS);
        assertBusiness(nth(roleDashboards, 0));
        assertKov(nth(roleDashboards, 1));
        assertGov(nth(roleDashboards, 2), lang);
    }

    private static void assertGov(RoleDashboardResponse role, Language lang) {
        assertEquals(ClassifierCode.GOV, role.getMainRole().getCode());
        list_size(role.getSubRoles(), 12);
        assertNull(role.getDashboard());

        ElementResponse subRole = first(role.getSubRoles());
        list_size(subRole.getDashboards(), 2);

        List<DashboardResponse> dashboards = subRole.getDashboards();
        DashboardResponse first = first(dashboards);
        DashboardResponse second = second(dashboards);

        assertWelfareDevelopmentPlanDashboard(lang.isEt() ? first : second, ROLE_DASHBOARD);
        assertSocialMinistryDashboard(lang.isEt() ? second : first, ROLE_DASHBOARD);

        for (ElementResponse otherRole : role.getSubRoles().subList(1, role.getSubRoles().size())) {
            assertNull(otherRole.getDashboards());
        }
    }

    private static void assertBusiness(RoleDashboardResponse role) {
        assertEquals(ClassifierCode.BUSINESS, role.getMainRole().getCode());
        list_size(role.getSubRoles(), 9);
        assertNull(role.getDashboard());

        ElementResponse subRole = first(role.getSubRoles());
        list_size(subRole.getDashboards(), 1);

        List<DashboardResponse> dashboards = subRole.getDashboards();
        assertTourismDashboard(first(dashboards), ROLE_DASHBOARD);

        for (ElementResponse otherRole : role.getSubRoles().subList(1, role.getSubRoles().size())) {
            assertNull(otherRole.getDashboards());
        }
    }

    private static void assertKov(RoleDashboardResponse role) {
        assertEquals(ClassifierCode.EHAK, role.getMainRole().getCode());
        list_size(role.getSubRoles(), 1 + 15);
        assertNotNull(role.getDashboard());

        DashboardResponse dashboard = role.getDashboard();
        assertRegionalStatDashboard(dashboard, ROLE_DASHBOARD);

        for (ElementResponse otherRole : role.getSubRoles()) {
            assertNull(otherRole.getDashboards());
        }
    }
}
