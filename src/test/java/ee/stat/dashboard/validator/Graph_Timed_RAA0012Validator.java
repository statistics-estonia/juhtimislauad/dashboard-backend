package ee.stat.dashboard.validator;

import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.front.FilterSerie;
import ee.stat.dashboard.model.widget.front.Serie;
import ee.stat.dashboard.service.widget.widget.dto.GraphDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;

import java.util.List;
import java.util.Optional;

import static ee.stat.dashboard.AssertUtil.ADJUSTMENT;
import static ee.stat.dashboard.AssertUtil.ADJUSTMENT_DONE;
import static ee.stat.dashboard.AssertUtil.ADJUSTMENT_SKIPPED;
import static ee.stat.dashboard.AssertUtil.assertFilters;
import static ee.stat.dashboard.AssertUtil.assertValue;
import static ee.stat.dashboard.AssertUtil.assertValueSource;
import static ee.stat.dashboard.AssertUtil.filterHas;
import static ee.stat.dashboard.AssertUtil.first;
import static ee.stat.dashboard.AssertUtil.last;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.AssertUtil.map_size;
import static ee.stat.dashboard.AssertUtil.nth;
import static ee.stat.dashboard.common.TestFilter.preciseFilter;
import static ee.stat.dashboard.model.json.Source.STAT_DATA_API;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Graph_Timed_RAA0012Validator {

    public static void validateGraph(WidgetResponse widget) {
        Language lang = widget.getLang();
        GraphDto graph = widget.getDiagram();
        assertFilters(graph.getFilters(), preciseFilter(ADJUSTMENT.name(lang), 2));

        list_size(graph.getSeries(), 2);

        Optional<FilterSerie> adjustmentDone = graph.getSeries().stream()
                .filter(f -> filterHas(f, ADJUSTMENT_DONE.name(lang)))
                .findAny();
        assertTrue(adjustmentDone.isPresent());
        assertSerie(adjustmentDone.get());

        Optional<FilterSerie> adjustmentSkipped = graph.getSeries().stream()
                .filter(f -> filterHas(f, ADJUSTMENT_SKIPPED.name(lang)))
                .findAny();
        assertTrue(adjustmentSkipped.isPresent());
        assertSerie(adjustmentSkipped.get());
    }

    private static void assertSerie(FilterSerie total_0to17) {
        map_size(total_0to17.getFilters(), 1);
        List<Serie> total_017_series = total_0to17.getSeries();
        list_size(total_017_series, 40);
        assertValue(first(total_017_series), STAT_DATA_API);
        assertValue(nth(total_017_series, 3), STAT_DATA_API);
        assertValueSource(last(total_017_series), STAT_DATA_API);
    }
}
