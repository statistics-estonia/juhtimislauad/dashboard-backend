package ee.stat.dashboard.validator;

import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.front.FilterSerie;
import ee.stat.dashboard.model.widget.front.Serie;
import ee.stat.dashboard.service.widget.widget.dto.GraphDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;

import java.util.List;

import static ee.stat.dashboard.AssertUtil.REFERENCE_PERIOD;
import static ee.stat.dashboard.AssertUtil.TALLINN;
import static ee.stat.dashboard.AssertUtil.assertFilters;
import static ee.stat.dashboard.AssertUtil.assertMapType;
import static ee.stat.dashboard.AssertUtil.assertPresent;
import static ee.stat.dashboard.AssertUtil.findSerieByAbscissa;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.common.TestFilter.preciseFilter;
import static org.junit.jupiter.api.Assertions.assertNull;

public class Graph_Axis_TU122Validator {

    public static void assertMapGraph(WidgetResponse widget) {
        Language lang = widget.getLang();
        GraphDto graph = widget.getDiagram();
        assertNull(graph.getPeriod());
        assertMapType(widget, graph, 3L);
        assertFilters(graph.getFilters(), preciseFilter(REFERENCE_PERIOD.name(lang), 60));

        List<FilterSerie> filterSeries = graph.getSeries();
        list_size(filterSeries, 1);

        List<Serie> series = filterSeries.get(0).getSeries();
        list_size(series, widget.getGraphType().isMap() ? 1 : 3);

        assertPresent(findSerieByAbscissa(series, TALLINN));
    }
}
