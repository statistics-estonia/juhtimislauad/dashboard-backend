package ee.stat.dashboard.validator;

import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.service.widget.widget.dto.GraphTypeDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import ee.stat.dashboard.validator.strategy.WidgetValidationStrategy;

import java.util.List;

import static ee.stat.dashboard.AssertUtil.EH47U_CODE_API;
import static ee.stat.dashboard.AssertUtil.EH47U_DESCRIPTION_EN;
import static ee.stat.dashboard.AssertUtil.EH47U_DESCRIPTION_ET;
import static ee.stat.dashboard.AssertUtil.EH47U_METADATA_EN;
import static ee.stat.dashboard.AssertUtil.EH47U_METADATA_ET;
import static ee.stat.dashboard.AssertUtil.EH47U_NAME_EN_API;
import static ee.stat.dashboard.AssertUtil.EH47U_NAME_ET;
import static ee.stat.dashboard.AssertUtil.EH47U_NOTE_EN;
import static ee.stat.dashboard.AssertUtil.EH47U_NOTE_ET;
import static ee.stat.dashboard.AssertUtil.EH47U_SHORTNAME_EN;
import static ee.stat.dashboard.AssertUtil.EH47U_SHORTNAME_ET;
import static ee.stat.dashboard.AssertUtil.EH47U_STAT_JOB_EN;
import static ee.stat.dashboard.AssertUtil.EH47U_STAT_JOB_ET;
import static ee.stat.dashboard.AssertUtil.commonWidget;
import static ee.stat.dashboard.AssertUtil.list_size;
import static ee.stat.dashboard.validator.Graph_Axis_EH47UValidator.assertTreeMapGraph;
import static ee.stat.dashboard.validator.Graph_Timed_TU51Validator.assertLineGraph;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

public class Widget_EH47UValidator {

    public static void assertEH47UWidget(WidgetResponse widget, WidgetValidationStrategy strategy) {
        commonWidget(widget);
        assertEquals(EH47U_CODE_API, widget.getCode());
        if (!(strategy.dashboard())) {
            assertBuildingConstructionData(widget);
        }
        if (strategy.dashboard()) {
            assertNull(widget.getDiagram());
            return;
        }

        assertStatData(widget);
        assertGraphTypes(widget);

        if (widget.getGraphType().isLineOrBarOrStackedOrArea()) {
            assertLineGraph(widget);
        } else if (widget.getGraphType().isMapOrVerticalOrPieOrTreemap()) {
            assertTreeMapGraph(widget);
        } else {
            fail("graph type validation is missing");
        }
    }

    private static void assertBuildingConstructionData(WidgetResponse widget) {
        assertEquals(TimePeriod.YEAR, widget.getTimePeriod());
        if (widget.getLang().isEt()) {
            assertEquals(EH47U_SHORTNAME_ET, widget.getShortname());
            assertEquals(EH47U_NAME_ET, widget.getName());
            assertEquals(EH47U_NOTE_ET, widget.getNote());
            assertEquals(EH47U_DESCRIPTION_ET, widget.getDescription());
        } else {
            assertEquals(EH47U_SHORTNAME_EN, widget.getShortname());
            assertEquals(EH47U_NAME_EN_API, widget.getName());
            assertEquals(EH47U_NOTE_EN, widget.getNote());
            assertEquals(EH47U_DESCRIPTION_EN, widget.getDescription());
        }
    }

    private static void assertGraphTypes(WidgetResponse widget) {
        GraphTypeDto graphTypes = widget.getGraphTypes();
        assertNotNull(graphTypes);
        assertEquals(List.of(GraphTypeEnum.treemap), graphTypes.getOptions());
        assertEquals(GraphTypeEnum.treemap, graphTypes.getDefaultOption());
    }

    private static void assertStatData(WidgetResponse widget) {
        list_size(widget.getStatCubes(), 1);
        assertEquals("EH47U", widget.getStatCubes().get(0).getCube());
        if (widget.getLang().isEt()) {
            assertEquals(EH47U_METADATA_ET, widget.getMethodsLinks().get(0));
            assertEquals(EH47U_STAT_JOB_ET, widget.getStatisticianJobLinks().get(0));
        } else {
            assertEquals(EH47U_METADATA_EN, widget.getMethodsLinks().get(0));
            assertEquals(EH47U_STAT_JOB_EN, widget.getStatisticianJobLinks().get(0));
        }
    }
}
