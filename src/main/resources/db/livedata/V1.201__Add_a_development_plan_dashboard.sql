SELECT setval('dashboard_id_seq', 2, false);

INSERT INTO dashboard (code, name_et, name_en, status, type, level, created_at, updated_at)
VALUES ('Heaolu arengukava 2016–2023',
        'Heaolu arengukava 2016–2023',
        'Welfare Development Plan 2016–2023',
        'VISIBLE',
        'GLOBAL',
        1,
        timestamp '2018-11-11 12:20:30.000000+00',
        timestamp '2018-11-21 12:20:30.000000+00');
