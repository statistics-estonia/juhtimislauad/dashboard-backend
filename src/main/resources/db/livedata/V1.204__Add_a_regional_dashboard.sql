SELECT setval('dashboard_id_seq', 3, false);

INSERT INTO dashboard (code, name_et, name_en, status, type, level, created_at, updated_at)
VALUES ('Piirkondlik statistika',
        'Piirkondlik statistika',
        'Regional statistics',
        'VISIBLE',
        'REGIONAL',
        0,
        timestamp '2018-11-11 12:20:30.000000+00',
        timestamp '2018-11-21 12:20:30.000000+00');

insert into dashboard_region (dashboard, element)
select 3,
       e.id
from element e
         join classifier c on e.classifier = c.id
where c.code = 'EHAK';