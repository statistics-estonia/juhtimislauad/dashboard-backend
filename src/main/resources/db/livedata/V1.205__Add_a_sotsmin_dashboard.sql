SELECT setval('dashboard_id_seq', 4, false);

INSERT INTO dashboard (code, name_et, name_en, status, type, level, created_at, updated_at)
VALUES ('sotsiaalministeerium',
        'Sotsiaalministeerium',
        'Ministry of Social Affairs',
        'VISIBLE',
        'GLOBAL',
        0,
        timestamp '2018-11-11 12:20:30.000000+00',
        timestamp '2018-11-21 12:20:30.000000+00');

INSERT INTO dashboard_connection (from_id, to_id) VALUES (4, 2);