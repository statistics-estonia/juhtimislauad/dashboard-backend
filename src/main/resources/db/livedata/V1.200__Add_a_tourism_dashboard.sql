SELECT setval('dashboard_id_seq', 1, false);

INSERT INTO dashboard (code, name_et, name_en, status, type, level, created_at, updated_at)
VALUES ('Tourism dashboard',
        'Turism',
        'Tourism',
        'VISIBLE',
        'REGIONAL',
        0,
        timestamp '2018-11-11 12:20:30.000000+00',
        timestamp '2018-11-21 12:20:30.000000+00');

insert into dashboard_region (dashboard, element)
select 1,
       e.id
from element e
         join classifier c on e.classifier = c.id
where c.code = 'EHAK'
  and ( e.level in (0, 1) or e.code in ('Tallinn', 'Tartu_city', 'Parnu'));