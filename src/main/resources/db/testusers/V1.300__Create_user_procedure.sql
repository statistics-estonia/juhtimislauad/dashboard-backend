CREATE OR REPLACE PROCEDURE
    insert_user(u_first_name VARCHAR(400),
                 u_last_name VARCHAR(400))
    LANGUAGE PLPGSQL
AS
$$
DECLARE
    user_id BIGINT;
BEGIN
    SELECT nextval('app_user_id_seq') INTO user_id;

    INSERT into app_user(id, id_code, first_name, last_name)
    values (user_id, lower(u_first_name), u_first_name, u_last_name);

    INSERT into app_user_session(app_user, ip, token, login_with, created_at, session_ends_at)
    values (user_id, 'local', concat(lower(u_first_name), '_token'), 'dev',
            timestamp '2018-11-11 12:20:30.000000+00',
            timestamp '2028-11-11 12:20:30.000000+00');
END;
$$;

CREATE OR REPLACE PROCEDURE
    insert_admin(u_first_name VARCHAR(400),
                 u_last_name VARCHAR(400))
    LANGUAGE PLPGSQL
AS
$$
DECLARE
    user_id BIGINT;
BEGIN
    SELECT nextval('app_user_id_seq') INTO user_id;

    INSERT into app_user(id, id_code, first_name, last_name)
    values (user_id, lower(u_first_name), u_first_name, u_last_name);

    INSERT into app_user_session(app_user, ip, token, login_with, created_at, session_ends_at)
    values (user_id, 'local', concat(lower(u_first_name), '_token'), 'dev',
            timestamp '2018-11-11 12:20:30.000000+00',
            timestamp '2028-11-11 12:20:30.000000+00');

    INSERT into app_user_role(app_user, role, created_at)
    values (user_id, 'ADMIN', timestamp '2018-11-11 12:20:30.000000+00');
END;
$$;