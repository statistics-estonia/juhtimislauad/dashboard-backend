-- diagram_data kus ei ole tegu viimase diagramiga e ei ole esimene ordered aja järgi desc (rnum <> 1)
with a as (
    SELECT diagram.*, row_number() OVER (partition by widget order by created_at desc) as rnum
    FROM diagram
)
delete
from diagram_data
where diagram in (select id from a where rnum <> 1);

-- data kus ei ole tegu viimase diagramiga e ei ole esimene ordered aja järgi desc (rnum <> 1)
with a as (
    SELECT diagram.*, row_number() OVER (partition by widget order by created_at desc) as rnum
    FROM diagram
)
delete
from diagram
where id in (select id from a where rnum <> 1);
