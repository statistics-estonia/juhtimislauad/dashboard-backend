CREATE TABLE user_widget
(
    id            BIGSERIAL PRIMARY KEY,
    widget_domain BIGINT  NOT NULL REFERENCES widget_domain,
    widget        BIGINT  NOT NULL REFERENCES widget,
    dashboard     BIGINT  NOT NULL REFERENCES dashboard,
    app_user      BIGINT  NOT NULL REFERENCES app_user,
    selected      BOOLEAN NOT NULL DEFAULT FALSE,
    order_nr      INTEGER NULL
);

create index user_widget_widget_domain on user_widget (widget_domain);
create index user_widget_dashboard on user_widget (dashboard);
create index user_widget_widget on user_widget (widget);
create index user_widget_app_user on user_widget (app_user);
