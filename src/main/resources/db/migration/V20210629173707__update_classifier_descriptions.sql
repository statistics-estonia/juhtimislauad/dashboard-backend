update classifier
set description_et = 'Rahvastiku, majanduse, keskkonna ja sotsiaalelu andmed maakondade, linnade ja valdade kaupa.',
    description_en = 'Data on population, economy, environment and social life by counties, cities and rural municipalities.'
where code = 'EHAK';

update classifier
set description_et = 'Kuidas Eesti riigil läheb? Kiire ülevaade arengukavade eesmärkide täitmisest, säästvast arengust ja noorteseirest.',
    description_en = 'How is the Estonian government doing? A quick overview of progress towards development targets, sustainable development and youth monitoring.'
where code = 'GOV';

update classifier
set description_et = 'Ettevõtlussektori andmed: majandusnäitajad, palk ja tööjõud, tööstustoodang, hinnad, innovatsioon.',
    description_en = 'Business sector data: financial statistics, wages and salaries and labour force, industrial production, prices, innovation.'
where code = 'BUSINESS';
