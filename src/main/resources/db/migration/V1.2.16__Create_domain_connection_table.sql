CREATE TABLE domain_connection
(
  id      BIGSERIAL PRIMARY KEY,
  from_id BIGINT NOT NULL REFERENCES domain,
  to_id   BIGINT NOT NULL REFERENCES domain
);

create index domain_connection_from_id on domain_connection (from_id);
create index domain_connection_to_id on domain_connection (to_id);