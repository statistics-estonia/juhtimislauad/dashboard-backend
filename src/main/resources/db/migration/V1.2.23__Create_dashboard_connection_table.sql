CREATE TABLE dashboard_connection
(
  id      BIGSERIAL PRIMARY KEY,
  from_id BIGINT NOT NULL REFERENCES dashboard,
  to_id   BIGINT NOT NULL REFERENCES dashboard
);

create index dashboard_connection_from_id on dashboard_connection (from_id);
create index dashboard_connection_to_id on dashboard_connection (to_id);
