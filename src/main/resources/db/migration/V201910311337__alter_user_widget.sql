ALTER TABLE user_widget
    ADD dashboard_widget BIGINT NULL REFERENCES dashboard_widget;

create index user_widget_dashboard_widget on user_widget (dashboard_widget);
