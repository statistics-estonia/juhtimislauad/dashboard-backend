CREATE TABLE app_user_session
(
    id              BIGSERIAL PRIMARY KEY,
    app_user        BIGINT        NOT NULL REFERENCES app_user,
    token           VARCHAR(200)  NOT NULL,
    login_with      VARCHAR(200)  NOT NULL,
    ip              VARCHAR(400) NOT NULL,
    created_at      TIMESTAMP     NOT NULL,
    finished_at     TIMESTAMP     NULL,
    session_ends_at TIMESTAMP     NOT NULL
);

create index app_user_session_app_user on app_user_session (app_user);