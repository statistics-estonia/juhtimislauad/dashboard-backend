drop table dashboard_connection;
alter table dashboard drop level;

update role_dashboard
set dashboard_rule = 'DASHBOARDS'
where dashboard_rule = 'SUB_DASHBOARDS';
