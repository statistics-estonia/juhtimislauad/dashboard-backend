CREATE TABLE user_filter
(
    id              BIGSERIAL PRIMARY KEY,
    user_graph_type BIGINT  NOT NULL REFERENCES user_graph_type,
    filter          BIGINT  NOT NULL REFERENCES filter
);

create index user_filter_user_graph_type on user_filter (user_graph_type);
create index user_filter_filter on user_filter (filter);