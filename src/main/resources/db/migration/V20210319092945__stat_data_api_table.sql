ALTER TABLE stat_data_api
    ADD force_rebuild BOOLEAN DEFAULT FALSE;

UPDATE stat_data_api
SET force_rebuild = false;

ALTER TABLE stat_data_api
    ALTER COLUMN force_rebuild SET NOT NULL;
