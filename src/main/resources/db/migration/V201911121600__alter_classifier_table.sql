ALTER TABLE classifier
    ADD name_et  VARCHAR(400) NULL,
    ADD name_en  VARCHAR(400) NULL,
    ADD order_nr INTEGER      NULL;

UPDATE classifier
SET name_et = name,
    name_en = name;

ALTER TABLE classifier
    DROP COLUMN name,
    DROP COLUMN description;

ALTER TABLE classifier
    ALTER COLUMN name_et SET NOT NULL,
    ALTER COLUMN name_et SET NOT NULL;
