CREATE TABLE user_legend_value
(
    id          BIGSERIAL PRIMARY KEY,
    user_legend BIGINT NOT NULL REFERENCES user_legend,
    filter      BIGINT NOT NULL REFERENCES filter,
    filter_value BIGINT  NOT NULL REFERENCES filter_value
);

create index user_legend_value_filter on user_legend_value (filter);
create index user_legend_value_filter_value on user_legend_value (filter_value);