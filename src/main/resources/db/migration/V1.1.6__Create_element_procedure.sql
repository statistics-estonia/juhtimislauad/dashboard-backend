CREATE OR REPLACE PROCEDURE
  insert_element(clf_code VARCHAR(400),
                 el_code VARCHAR(400),
                 el_name_en VARCHAR(4000),
                 el_name_et VARCHAR(4000),
                 el_level INTEGER DEFAULT NULL)
  LANGUAGE PLPGSQL
AS
$$
DECLARE
  clf_id BIGINT;
BEGIN
  SELECT clf.id INTO clf_id from classifier clf where clf.code = clf_code;
  INSERT INTO Element (classifier, code, name_en, name_et, level)
  values (clf_id, el_code, el_name_en, el_name_et, el_level);
END;
$$;