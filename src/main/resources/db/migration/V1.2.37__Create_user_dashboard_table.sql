CREATE TABLE user_dashboard
(
    id        BIGSERIAL PRIMARY KEY,
    dashboard BIGINT NOT NULL REFERENCES dashboard,
    app_user  BIGINT NOT NULL REFERENCES app_user,
    region    BIGINT NOT NULL REFERENCES element
);

create index user_dashboard_dashboard on user_dashboard (dashboard);
create index user_dashboard_app_user on user_dashboard (app_user);
create index user_dashboard_region on user_dashboard (region);