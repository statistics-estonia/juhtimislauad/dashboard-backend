CREATE OR REPLACE PROCEDURE
  insert_element_connection(clf_code VARCHAR(400),
                            el_from_code VARCHAR(400),
                            el_to_code VARCHAR(400))
  LANGUAGE SQL
AS
$$
call insert_element_connection(clf_code, el_from_code, clf_code, el_to_code);
$$;