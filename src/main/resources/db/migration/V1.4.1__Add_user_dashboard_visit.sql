ALTER TABLE user_dashboard
    ADD COLUMN visited BOOLEAN;

UPDATE user_dashboard
set visited = FALSE;

ALTER TABLE user_dashboard
    ALTER COLUMN visited SET NOT NULL;

ALTER TABLE user_dashboard
    ALTER COLUMN visited SET DEFAULT FALSE;