update process
set name_et = 'Kuvamooduli andmete uuendamine PxWeb APIst'
where code = 'STAT_DATA_API_UPDATE_AND_WIDGET_RECALC';

INSERT INTO process(code, name_en, name_et)
values ('STAT_DATA_SQL_UPDATE_AND_WIDGET_RECALC', 'Stat data sql update and widget recalc',
        'Kuvamooduli andmete uuendamine PxWebSql APIst');
