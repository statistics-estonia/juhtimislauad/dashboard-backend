CREATE TABLE excel_data
(
  id          BIGSERIAL PRIMARY KEY,
  excel       BIGINT NOT NULL REFERENCES excel,
  response_et JSONB  NULL,
  response_en JSONB  NULL
);

create index excel_data_excel on excel_data (excel);