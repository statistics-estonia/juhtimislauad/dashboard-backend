CREATE TABLE user_graph_type
(
  id          BIGSERIAL PRIMARY KEY,
  graph_type  BIGINT       NOT NULL REFERENCES graph_type,
  user_widget BIGINT       NOT NULL REFERENCES user_widget,
  type        VARCHAR(200) NOT NULL,
  selected    BOOLEAN      NOT NULL DEFAULT FALSE
);

create index user_graph_type_graph_type on user_graph_type (graph_type);
create index user_graph_type_user_widget on user_graph_type (user_widget);
