UPDATE Element
SET code = 'Sustainable development',
    name_en = 'Sustainable development',
    name_et = 'Säästev areng'
WHERE classifier = (SELECT id FROM classifier WHERE code = 'GOV')
  AND code = 'Government Office';

UPDATE Element
SET code = 'Youth monitoring',
    name_en = 'Youth monitoring',
    name_et = 'Noorte seire'
WHERE classifier = (SELECT id FROM classifier WHERE code = 'GOV')
  AND code = 'Ministry of Education and Research';

UPDATE Element
SET code = 'Cohesive Estonia',
    name_en = 'Cohesive Estonia',
    name_et = 'Sidus Eesti'
WHERE classifier = (SELECT id FROM classifier WHERE code = 'GOV')
  AND code = 'Ministry of Culture';
