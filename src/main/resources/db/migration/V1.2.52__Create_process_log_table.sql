CREATE TABLE process_log
(
    id         BIGSERIAL PRIMARY KEY,
    process    BIGINT       NOT NULL REFERENCES process,
    status     VARCHAR(200) NOT NULL,
    start_time TIMESTAMP    NOT NULL,
    end_time   TIMESTAMP    NULL,
    analysed   INTEGER      NULL,
    unchanged  INTEGER      NULL,
    changed    INTEGER      NULL,
    failed     INTEGER      NULL
);

create index process_log_process on process_log (process);