CREATE TABLE widget_domain
(
  id        BIGSERIAL PRIMARY KEY,
  widget    BIGINT NOT NULL REFERENCES widget,
  dashboard BIGINT NOT NULL REFERENCES dashboard,
  domain    BIGINT NOT NULL REFERENCES domain
);

create index widget_domain_widget on widget_domain (widget);
create index widget_domain_dashboard on widget_domain (dashboard);
create index widget_domain_domain on widget_domain (domain);