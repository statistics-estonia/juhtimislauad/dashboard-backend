CREATE TABLE role_dashboard
(
    id              BIGSERIAL PRIMARY KEY,
    role_element    BIGINT       NULL REFERENCES element,
    role_classifier BIGINT       NULL REFERENCES classifier,
    dashboard       BIGINT       NOT NULL REFERENCES dashboard,
    dashboard_rule  VARCHAR(200) NOT NULL,
    created_at      TIMESTAMP    NOT NULL
);

create index role_dashboard_role_element on role_dashboard (role_element);
create index role_dashboard_role_classifier on role_dashboard (role_classifier);
create index role_dashboard_dashboard on role_dashboard (dashboard);
