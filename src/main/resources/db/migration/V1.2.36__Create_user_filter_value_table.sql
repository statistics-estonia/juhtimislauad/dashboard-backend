CREATE TABLE user_filter_value
(
    id           BIGSERIAL PRIMARY KEY,
    user_filter  BIGINT  NOT NULL REFERENCES user_filter,
    filter_value BIGINT  NOT NULL REFERENCES filter_value
);

create index user_filter_value_user_filter on user_filter_value (user_filter);
create index user_filter_value_filter_value on user_filter_value (filter_value);