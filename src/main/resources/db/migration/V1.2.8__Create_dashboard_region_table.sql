CREATE TABLE dashboard_region
(
  id        BIGSERIAL PRIMARY KEY,
  dashboard BIGINT NOT NULL REFERENCES dashboard,
  element   BIGINT NOT NULL REFERENCES element
);

create index dashboard_region_dashboard on dashboard_region (dashboard);
create index dashboard_region_element on dashboard_region (element);
