CREATE TABLE classifier
(
  id          BIGSERIAL PRIMARY KEY,
  code        VARCHAR(200)  NOT NULL,
  name        VARCHAR(400)  NOT NULL,
  description VARCHAR(4000) NULL
)