CREATE TABLE app_user
(
  id         BIGSERIAL PRIMARY KEY,
  id_code    VARCHAR(200) NOT NULL,
  first_name VARCHAR(200) NOT NULL,
  last_name  VARCHAR(200) NOT NULL
)