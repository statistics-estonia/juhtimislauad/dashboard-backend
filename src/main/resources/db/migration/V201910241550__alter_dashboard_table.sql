ALTER TABLE dashboard
    ALTER COLUMN code DROP NOT NULL,
    ALTER COLUMN name_en DROP NOT NULL,
    ALTER COLUMN name_et DROP NOT NULL;

ALTER TABLE dashboard
    ADD app_user  BIGINT       NULL REFERENCES app_user,
    ADD user_type VARCHAR(200) NULL;

UPDATE dashboard
SET user_type = 'ADMIN'
WHERE app_user IS NULL;

ALTER TABLE dashboard
    ALTER COLUMN user_type SET NOT NULL;

CREATE INDEX dashboard_app_user on dashboard (app_user);
CREATE INDEX dashboard_user_type on dashboard (user_type);
