CREATE TABLE stat_data_sql
(
    id            BIGSERIAL   PRIMARY KEY,
    widget        BIGINT      NOT NULL REFERENCES widget,
    cube          VARCHAR(20) NOT NULL,
    created_at    TIMESTAMP   NOT NULL,
    updated_at    TIMESTAMP   NULL,
    published_at  TIMESTAMP   NULL,
    force_rebuild BOOLEAN     DEFAULT FALSE
);

create index stat_data_sql_widget on stat_data_sql (widget);


CREATE TABLE stat_data_sql_data
(
    id            BIGSERIAL PRIMARY KEY,
    stat_data_sql BIGINT    NOT NULL REFERENCES stat_data_sql,
    graph_type    BIGINT    NULL REFERENCES graph_type,
    request       VARCHAR   NOT NULL,
    response_et   VARCHAR   NULL,
    response_en   VARCHAR   NULL
);

create index stat_data_sql_data_stat_data_sql on stat_data_sql_data (stat_data_sql);
