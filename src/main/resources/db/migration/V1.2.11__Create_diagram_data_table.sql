CREATE TABLE diagram_data
(
  id         BIGSERIAL PRIMARY KEY,
  widget     BIGINT       NOT NULL REFERENCES widget,
  diagram    BIGINT       NOT NULL REFERENCES diagram,
  graph_type BIGINT       NOT NULL REFERENCES graph_type,
  type       VARCHAR(200) NOT NULL,
  graph_et   JSONB        NULL,
  graph_en   JSONB        NULL
);

create index diagram_graph_widget on diagram_data (widget);
create index diagram_graph_diagram on diagram_data (diagram);
create index diagram_graph_widget_graph on diagram_data (graph_type);
