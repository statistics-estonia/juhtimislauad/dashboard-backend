CREATE TABLE stat_db_data
(
    id          BIGSERIAL PRIMARY KEY,
    stat_db     BIGINT        NOT NULL REFERENCES stat_db,
    graph_type  BIGINT        NULL REFERENCES graph_type,
    url         VARCHAR(2000) NOT NULL,
    response_et JSONB         NULL,
    response_en JSONB         NULL
);

create index stat_db_data_stat_db on stat_db_data (stat_db);