BEGIN;

DELETE FROM role_dashboard
WHERE role_element IN (
    SELECT id
    FROM Element
    WHERE code = 'Ministry of Social Affairs'
);

DELETE FROM dashboard WHERE code = 'sotsiaalministeerium';

DELETE FROM Element WHERE code = 'Ministry of Social Affairs';

COMMIT;
