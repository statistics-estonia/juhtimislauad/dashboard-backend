CREATE TABLE stat_data_api
(
    id           BIGSERIAL PRIMARY KEY,
    widget       BIGINT      NOT NULL REFERENCES widget,
    cube         VARCHAR(20) NOT NULL,
    created_at   TIMESTAMP   NOT NULL,
    updated_at   TIMESTAMP   NULL,
    published_at TIMESTAMP   NULL
);

create index stat_data_api_widget on stat_data_api (widget);


CREATE TABLE stat_data_api_data
(
    id            BIGSERIAL PRIMARY KEY,
    stat_data_api BIGINT  NOT NULL REFERENCES stat_data_api,
    graph_type    BIGINT  NULL REFERENCES graph_type,
    request       VARCHAR NOT NULL,
    response_et   VARCHAR NULL,
    response_en   VARCHAR NULL
);

create index stat_data_api_data_stat_data_api on stat_data_api_data (stat_data_api);
