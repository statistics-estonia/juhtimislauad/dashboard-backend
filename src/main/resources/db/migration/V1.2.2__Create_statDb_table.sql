CREATE TABLE stat_db
(
  id                       BIGSERIAL PRIMARY KEY,
  widget                   BIGINT        NOT NULL REFERENCES widget,
  cube                     VARCHAR(200)  NOT NULL,
  created_at               TIMESTAMP     NOT NULL,
  updated_at               TIMESTAMP     NULL
);

create index stat_db_widget on stat_db (widget);