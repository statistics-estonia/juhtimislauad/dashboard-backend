CREATE TABLE Filter
(
  id               BIGSERIAL PRIMARY KEY,
  widget           BIGINT       NOT NULL REFERENCES widget,
  graph_type       BIGINT       NOT NULL REFERENCES graph_type,
  type             VARCHAR(200) NOT NULL,
  axis_order       VARCHAR(200) NULL,
  name_et          VARCHAR(400) NULL,
  name_en          VARCHAR(400) NULL,
  display_name_et  VARCHAR(400) NULL,
  display_name_en  VARCHAR(400) NULL,
  order_nr         INTEGER      NULL,
  number_of_values INTEGER      NOT NULL,
  time             BOOLEAN      NOT NULL DEFAULT FALSE,
  time_part        VARCHAR(200) NULL,
  region           BOOLEAN      NOT NULL DEFAULT FALSE
);

create index filter_widget on filter (widget);
create index filter_graph_type on filter (graph_type);