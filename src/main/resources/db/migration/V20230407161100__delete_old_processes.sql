delete
from Stat_Log
where process_log in (select id
                      from Process_Log
                      where process in (select id
                                        from Process
                                        where code in ('STAT_DB_WIDGET_MIGRATION', 'STAT_DB_UPDATE_AND_WIDGET_RECALC')));

delete
from Process_Log
where process in (select id
                  from Process
                  where code in ('STAT_DB_WIDGET_MIGRATION', 'STAT_DB_UPDATE_AND_WIDGET_RECALC'));

delete
from Process
where code in ('STAT_DB_WIDGET_MIGRATION', 'STAT_DB_UPDATE_AND_WIDGET_RECALC');
