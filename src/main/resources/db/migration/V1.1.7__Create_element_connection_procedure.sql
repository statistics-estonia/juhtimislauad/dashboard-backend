CREATE OR REPLACE PROCEDURE
  insert_element_connection(clf_from_code VARCHAR(400),
                            el_from_code VARCHAR(400),
                            clf_to_code VARCHAR(400),
                            el_to_code VARCHAR(400))
  LANGUAGE PLPGSQL
AS
$$
DECLARE
  from_id BIGINT;
  to_id   BIGINT;
BEGIN
  SELECT el_from.id INTO from_id
  from element el_from
         join classifier clf on el_from.classifier = clf.id
  where clf.code = clf_from_code
    and el_from.code = el_from_code;

  SELECT el_to.id INTO to_id
  from element el_to
         join classifier clf on el_to.classifier = clf.id
  where clf.code = clf_to_code
    and el_to.code = el_to_code;
  INSERT INTO element_connection (from_id, to_id) VALUES (from_id, to_id);
END;
$$;