CREATE TABLE app_user_role
(
  id         BIGSERIAL PRIMARY KEY,
  app_user   BIGINT       NOT NULL REFERENCES app_user,
  role       VARCHAR(200) NOT NULL,
  created_at TIMESTAMP    NOT NULL
);

create index app_user_role_app_user on app_user_role (app_user);