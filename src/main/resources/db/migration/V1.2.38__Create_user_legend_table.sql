CREATE TABLE user_legend
(
    id              BIGSERIAL PRIMARY KEY,
    user_graph_type BIGINT  NOT NULL REFERENCES user_graph_type,
    target          BOOLEAN NOT NULL DEFAULT FALSE
);

create index user_legend_user_graph_type on user_legend (user_graph_type);