CREATE TABLE filter_value_custom
(
    id                  BIGSERIAL PRIMARY KEY,
    widget              BIGINT       NOT NULL REFERENCES widget,
    display_name_et     VARCHAR(400) NOT NULL,
    display_name_en     VARCHAR(400) NOT NULL,
    dimension_1_name_et VARCHAR(400) NOT NULL,
    dimension_1_name_en VARCHAR(400) NOT NULL,
    dimension_2_name_et VARCHAR(400) NULL,
    dimension_2_name_en VARCHAR(400) NULL,
    selection_value_1   VARCHAR(50)  NOT NULL,
    selection_value_2   VARCHAR(50)  NOT NULL,
    operation VARCHAR(15) NOT NULL
);

CREATE INDEX filter_value_custom_widget ON filter_value_custom (widget);

CREATE TABLE filter_value_custom_for_filter
(
    id                  BIGSERIAL PRIMARY KEY,
    filter_value_custom BIGINT NOT NULL REFERENCES filter_value_custom,
    filter              BIGINT NOT NULL REFERENCES filter
);

CREATE INDEX filter_value_custom_for_filter_filter ON filter_value_custom_for_filter (filter);
