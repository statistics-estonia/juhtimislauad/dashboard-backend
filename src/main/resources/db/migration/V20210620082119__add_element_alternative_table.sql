CREATE TABLE element_alternative
(
    id      BIGSERIAL PRIMARY KEY,
    element BIGINT        NOT NULL REFERENCES element,
    name_et VARCHAR(2000) NOT NULL,
    name_en VARCHAR(2000) NOT NULL
);

create index element_alternative_element on element_alternative (element);

insert into element_alternative(element, name_et, name_en)
select (select id from Element where code = 'Whole')
     , 'Eesti'
     , 'Estonia';

