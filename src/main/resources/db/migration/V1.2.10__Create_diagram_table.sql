CREATE TABLE diagram
(
  id              BIGSERIAL PRIMARY KEY,
  widget          BIGINT       NOT NULL REFERENCES widget,
  status          VARCHAR(200) NOT NULL,
  created_at      TIMESTAMP    NOT NULL,
  updated_at      TIMESTAMP    NULL,
  data_updated_at TIMESTAMP    NULL
);

create index diagram_widget on diagram (widget);