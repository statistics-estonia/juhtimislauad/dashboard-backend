CREATE TABLE domain
(
    id              BIGSERIAL PRIMARY KEY,
    dashboard       BIGINT        NOT NULL REFERENCES dashboard,
    code            VARCHAR(200)  NOT NULL,
    name_et         VARCHAR(2000) NOT NULL,
    name_en         VARCHAR(2000) NOT NULL,
    level           INTEGER       NOT NULL,
    order_nr        INTEGER       NOT NULL,
    level_name_et   VARCHAR(200)  NOT NULL,
    level_name_en   VARCHAR(200)  NOT NULL,
    show_level_name BOOLEAN       NOT NULL
);

create index domain_dashboard on domain (dashboard);
