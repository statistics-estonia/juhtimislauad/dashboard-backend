CREATE TABLE element
(
  id         BIGSERIAL PRIMARY KEY,
  classifier BIGINT        NOT NULL REFERENCES classifier,
  code       VARCHAR(200)  NOT NULL,
  name_et    VARCHAR(2000) NOT NULL,
  name_en    VARCHAR(2000) NOT NULL,
  level      INTEGER       NULL
);

create index element_classifier on element (classifier);