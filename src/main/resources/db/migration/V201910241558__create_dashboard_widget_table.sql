CREATE TABLE dashboard_widget
(
    id         BIGSERIAL PRIMARY KEY,
    dashboard  BIGINT    NOT NULL REFERENCES dashboard,
    widget     BIGINT    NOT NULL REFERENCES widget,
    created_at TIMESTAMP NOT NULL
);

create index dashboard_widget_dashboard on dashboard_widget (dashboard);
create index dashboard_widget_widget on dashboard_widget (widget);
