CREATE TABLE excel
(
  id          BIGSERIAL PRIMARY KEY,
  widget                   BIGINT        NOT NULL REFERENCES widget,
  filename    VARCHAR(400) NOT NULL,
  location    VARCHAR(400) NULL,
  response_et JSONB        NULL,
  response_en JSONB        NULL,
  excel_type  VARCHAR(200) NOT NULL,
  created_at  TIMESTAMP    NOT NULL,
  updated_at  TIMESTAMP    NULL
);

create index excel_widget on excel (widget);