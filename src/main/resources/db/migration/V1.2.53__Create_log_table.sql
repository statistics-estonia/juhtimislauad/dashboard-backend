CREATE TABLE stat_log
(
    id          BIGSERIAL PRIMARY KEY,
    process_log BIGINT        NOT NULL REFERENCES process_log,
    status      VARCHAR(200)  NOT NULL,
    time        TIMESTAMP     NOT NULL,
    message     VARCHAR(4000) NOT NULL,
    error       VARCHAR(4000) NULL,
    widget      BIGINT        NULL,
    cube        VARCHAR(200)  NULL
);

create index stat_log_process_log on stat_log (process_log);