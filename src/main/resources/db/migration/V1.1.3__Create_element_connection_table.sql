CREATE TABLE element_connection
(
  id      BIGSERIAL PRIMARY KEY,
  from_id BIGINT NOT NULL REFERENCES element,
  to_id   BIGINT NOT NULL REFERENCES element
);

create index element_connection_from_id on element_connection (from_id);
create index element_connection_to_id on element_connection (to_id);
