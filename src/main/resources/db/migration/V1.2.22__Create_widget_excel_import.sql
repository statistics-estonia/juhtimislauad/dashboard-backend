CREATE TABLE Widget_Excel_Import
(
  id            BIGSERIAL PRIMARY KEY,
  widget        BIGINT       NULL REFERENCES widget,
  filename      VARCHAR(400) NOT NULL,
  import_status VARCHAR(200) NOT NULL,
  created_at    TIMESTAMP    NOT NULL,
  updated_at    TIMESTAMP    NULL
);

create index widget_excel_import_widget on widget_excel_import (widget);