INSERT INTO classifier (code, name_et, name_en, order_nr)
VALUES ('GOV', 'Keskvalitsus', 'Central government', 2);

INSERT INTO classifier (code, name_et, name_en, order_nr)
VALUES ('BUSINESS', 'Ettevõtlus', 'Business', 1);

UPDATE classifier
SET name_et = 'Kohalik omavalitsus',
    name_en = 'Local Government',
    order_nr = 3
WHERE code = 'EHAK';

call insert_element('GOV', 'Ministry of Education and Research', 'Ministry of Education and Research',
                    'Haridus- ja teadusministeerium', 0);
call insert_element('GOV', 'Ministry of Justice', 'Ministry of Justice', 'Justiitsministeerium', 0);
call insert_element('GOV', 'Ministry of Defence', 'Ministry of Defence', 'Kaitseministeerium', 0);
call insert_element('GOV', 'Ministry of the Environment', 'Ministry of the Environment', 'Keskkonnaministeerium', 0);
call insert_element('GOV', 'Ministry of Culture', 'Ministry of Culture', 'Kultuuriministeerium', 0);
call insert_element('GOV', 'Ministry of Economic Affairs and Communications',
                    'Ministry of Economic Affairs and Communications', 'Majandus- ja kommunikatsiooniministeerium', 0);
call insert_element('GOV', 'Ministry of Rural Affairs', 'Ministry of Rural Affairs', 'Maaeluministeerium', 0);
call insert_element('GOV', 'Ministry of Finance', 'Ministry of Finance', 'Rahandusministeerium', 0);
call insert_element('GOV', 'Ministry of the Interior', 'Ministry of the Interior', 'Siseministeerium', 0);
call insert_element('GOV', 'Ministry of Social Affairs', 'Ministry of Social Affairs', 'Sotsiaalministeerium', 0);
call insert_element('GOV', 'Ministry of Foreign Affairs', 'Ministry of Foreign Affairs', 'Välisministeerium', 0);
call insert_element('GOV', 'Government Office', 'Government Office', 'Riigikantselei', 0);

call insert_element('BUSINESS', 'Tourism industry', 'Tourism industry', 'Turismindus', 0);
call insert_element('BUSINESS', 'Food industry', 'Food industry', 'Toiduainetööstus', 0);
call insert_element('BUSINESS', 'Commerce', 'Commerce', 'Kaubandus', 0);
call insert_element('BUSINESS', 'Forestry and timber industry', 'Forestry and timber industry',
                    'Metsandus ja puidutööstus', 0);
call insert_element('BUSINESS', 'Marine industry', 'Marine industry', 'Merendus', 0);
call insert_element('BUSINESS', 'Construction and real estate', 'Construction and real estate', 'Ehitus ja kinnisvara',
                    0);
call insert_element('BUSINESS', 'Banking and insurance', 'Banking and insurance', 'Pangandus ja kindlustus', 0);
call insert_element('BUSINESS', 'Accountancy', 'Accountancy', 'Raamatupidamine', 0);
call insert_element('BUSINESS', 'Information and communication technology ',
                    'Information and communication technology ', 'Info- ja kommunikatsioonitehnoloogia', 0);
