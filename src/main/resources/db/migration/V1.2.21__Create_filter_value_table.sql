CREATE TABLE Filter_Value
(
    id               BIGSERIAL PRIMARY KEY,
    filter           BIGINT       NOT NULL REFERENCES filter,
    value_et         VARCHAR(400) NULL,
    value_en         VARCHAR(400) NULL,
    display_value_et VARCHAR(400) NULL,
    display_value_en VARCHAR(400) NULL,
    selected         BOOLEAN      NOT NULL DEFAULT FALSE,
    order_nr         INTEGER      NULL
);

create index filter_value_filter on filter_value (filter);