CREATE TABLE dashboard
(
  id         BIGSERIAL PRIMARY KEY,
  code       VARCHAR(400) NOT NULL,
  name_et    VARCHAR(400) NOT NULL,
  name_en    VARCHAR(400) NOT NULL,
  status     VARCHAR(200) NOT NULL,
  type       VARCHAR(200) NOT NULL,
  level      INTEGER      NOT NULL,
  created_at TIMESTAMP    NOT NULL,
  updated_at TIMESTAMP    NULL
)