CREATE TABLE graph_type
(
  id           BIGSERIAL PRIMARY KEY,
  widget       BIGINT       NOT NULL REFERENCES widget,
  type         VARCHAR(200) NOT NULL,
  default_type BOOLEAN      NOT NULL DEFAULT FALSE,
  vertical_rule VARCHAR(200) NULL,
  map_rule     VARCHAR(200) NULL
);

create index graph_type_widget on graph_type (widget);