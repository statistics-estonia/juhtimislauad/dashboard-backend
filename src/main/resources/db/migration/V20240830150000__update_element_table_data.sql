UPDATE Element
SET name_et = 'Noorteseire'
WHERE classifier = (SELECT id FROM classifier WHERE code = 'GOV')
  AND code = 'Youth monitoring';