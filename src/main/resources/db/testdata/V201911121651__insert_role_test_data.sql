insert into role_dashboard(role_element, dashboard, dashboard_rule, created_at)
select e.id, d.id, 'SUB_DASHBOARDS', current_timestamp
from element e,
     dashboard d
where e.code = 'Ministry of Social Affairs'
  and d.code = 'sotsiaalministeerium';

insert into role_dashboard(role_element, dashboard, dashboard_rule, created_at)
select e.id, d.id, 'DASHBOARDS', current_timestamp
from element e,
     dashboard d
where e.code = 'Tourism industry'
  and d.code = 'Tourism dashboard';

insert into role_dashboard(role_classifier, dashboard, dashboard_rule, created_at)
select e.id, d.id, 'ONE_DASHBOARD', current_timestamp
from classifier e,
     dashboard d
where e.code = 'EHAK'
  and d.code = 'Piirkondlik statistika';
