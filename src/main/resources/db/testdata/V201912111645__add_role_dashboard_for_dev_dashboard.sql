insert into role_dashboard(role_element, dashboard, dashboard_rule, created_at)
select e.id, d.id, 'DASHBOARDS', current_timestamp
from element e,
     dashboard d
where e.code = 'Ministry of Social Affairs'
  and d.code = 'Heaolu arengukava 2016–2023';
