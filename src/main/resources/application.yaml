server:
  servlet:
    contextPath: /api
  port: 5080

logging:
  config: classpath:logback-spring.xml

spring:
  jackson:
    default-property-inclusion: non_null
  flyway:
    locations: classpath:db/migration, classpath:db/livedata
    out-of-order: true
  jpa:
    database-platform: org.hibernate.dialect.PostgreSQLDialect
    properties:
      hibernate:
        temp:
          use_jdbc_metadata_defaults: false
        jdbc:
          time_zone: UTC
  datasource:
    url: jdbc:postgresql://localhost:5433/dashboard?currentSchema=dashboard
    username: dashboard
    password: dashboard
  cache:
    cache-names:
      - ClassifierServiceCache_findById
      - ClassifierServiceCache_findByCode
      - ClassifierServiceCache_findByCodeIn
      - DashboardServiceCache_findById
      - DashboardServiceCache_findAllByLevel
      - DashboardServiceCache_findAllFromByTo
      - DashboardServiceCache_findAllToByFrom
      - DiagramServiceCache_findDiagram
      - DiagramServiceCache_findDiagramGraph
      - DomainService_loadDomainsAndWidgets
      - DomainServiceCache_findAllFromByTo
      - DomainServiceCache_findAllByWidget
      - ElementServiceCache_findById
      - ElementServiceCache_findAllByLevel
      - ElementServiceCache_findAllByLevelIn
      - ElementServiceCache_findAllByCode
      - ElementServiceCache_findAllByClfCode
      - ElementServiceCache_findAllLowerNamesByClfCode
      - ElementServiceCache_findAllByClfCodeAndRoleOrderByName
      - ElementServiceCache_findAllToByFrom
      - ElementServiceCache_findAllDashboardRegions
      - ExcelCache_findByWidget
      - FilterService_getDbFilters
      - GraphService_filterData
      - GraphService_fullData
      - GraphTypeServiceCache_findAllByWidget
      - StatDataApiCache_findByWidget
      - StatDataSqlCache_findByWidget
      - WidgetServiceCache_findById
    caffeine:
      spec: expireAfterWrite=5m

management:
  endpoints:
    web:
      exposure:
        include: "info, health"
  info:
    build:
      enabled: false

app:
  session:
    time: 120 #mins
  oauth:
    clientId: stat-dashboard-dev
    simpleScope: openid
    issuer: https://tara-test.ria.ee
    redirectUri: <api-url>/login/tara/callback
    cancelUri: <api-url>/login/tara/cancel
    TARATokenEndpoint: https://tara-test.ria.ee/oidc/token
    TARAAuthorizationEndpoint: https://tara-test.ria.ee/oidc/authorize
    TARAKeyEndpoint: https://tara-test.ria.ee/oidc/jwks
    clientSecret: <client-secret>
    localSuccessUrl: <app-success-url>
    localFailureUrl: <app-failure-url>
    localCancelUrl: <app-cancel-url>
  element:
    wholeCode: Whole
  stat:
    urlLength: 375
    dataApiUrl: https://andmed.stat.ee/api/v1/{0}/stat/{1}
    dataSqlUrl: https://andmed.stat.ee/api/v1/{0}/statsql/{1}
    dataApiCubesUrl: https://andmed.stat.ee/resources/px/databases/stat/menu.xml
    dataSqlPublishedAtUrl: https://andmed.stat.ee/api/v1/{0}/statsql?query={1}
    dataApiPortalUrl: https://andmed.stat.ee/{0}/stat/{1}
    dataSqlPortalUrl: https://andmed.stat.ee/{0}/statsql/{1}
  cron:
    statDataSyncCron: 0 0 * * * *
    timezone: UTC
    statDataApiEnabled: true
    statDataSqlEnabled: true
    statDataUpdateDelayMs: 500
    logCleanup: 0 0 1 * * SAT
    logCleanupDays: 30
    logCleanupEnabled: true
  excel:
    location: ./excel/data/
    widgetLocation: ./widgets/
    type: Andmetüüp
    realLevels: tegelikud tasemed
    separator: \|\|
  http:
    readTimeout: 30000
    connectionTimeout: 30000
    connectionRequestTimeout: 30000
