package ee.stat.dashboard.service.statsync.datapi;

import ee.stat.dashboard.model.process.Process;
import ee.stat.dashboard.model.process.ProcessLog;
import ee.stat.dashboard.model.process.enums.ProcessCode;
import ee.stat.dashboard.repository.ProcessRepository;
import ee.stat.dashboard.service.process.ProcessLogService;
import ee.stat.dashboard.service.process.ProcessLoggingService;
import ee.stat.dashboard.service.statdata.publishedat.StatDataApiCubeCache;
import ee.stat.dashboard.service.statsync.shared.dto.ProcessResult;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static ee.stat.dashboard.model.process.enums.LogStatus.ERROR;
import static ee.stat.dashboard.model.process.enums.ProcessCode.STAT_DATA_API_UPDATE_AND_WIDGET_RECALC;
import static java.lang.String.format;

@Service
@AllArgsConstructor
@Slf4j
@Transactional
public class StatDataApiUpdaterProcess {

    private static final ProcessCode PROCESS_CODE = STAT_DATA_API_UPDATE_AND_WIDGET_RECALC;
    private final StatDataApisUpdater statDataApisUpdater;
    private final ProcessLogService processLogService;
    private final ProcessLoggingService processLoggingService;
    private final ProcessRepository processRepository;
    private final StatDataApiCubeCache statDataApiCubeCache;

    public void process() {
        ProcessLog processLog = processLogService.start(findProcess());
        try {
            statDataApiCubeCache.updateCache();
            ProcessResult result = statDataApisUpdater.checkForStatDbChangesAndBuildNewDiagrams(processLog);
            processLogService.finished(processLog, result);
        } catch (Exception e) {
            processLoggingService.log(processLog, null, ERROR, "Process failed", null, e);
            processLogService.fail(processLog);
        }
    }

    private Process findProcess() {
        Process process = processRepository.findByCode(PROCESS_CODE);
        if (process == null) processNotFound();
        return process;
    }

    private void processNotFound() {
        log.error("Process {} not found", PROCESS_CODE.name());
        throw new RuntimeException(format("Process %s not found", PROCESS_CODE.name()));
    }
}
