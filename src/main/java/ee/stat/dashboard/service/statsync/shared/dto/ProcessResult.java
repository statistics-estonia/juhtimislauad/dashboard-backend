package ee.stat.dashboard.service.statsync.shared.dto;

import lombok.Getter;

@Getter
public class ProcessResult {

    private int analysed;
    private int unchanged;
    private int changed;
    private int failed;

    public void incrementAnalysed() {
        analysed++;
    }

    public void incrementUnchanged() {
        unchanged++;
    }

    public void incrementChanged() {
        changed++;
    }

    public void incrementFailed() {
        failed++;
    }

}
