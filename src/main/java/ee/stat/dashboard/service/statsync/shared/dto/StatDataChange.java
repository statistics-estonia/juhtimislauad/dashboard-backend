package ee.stat.dashboard.service.statsync.shared.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
public class StatDataChange {

    private boolean changed;
    private LocalDateTime publishedAt;
}
