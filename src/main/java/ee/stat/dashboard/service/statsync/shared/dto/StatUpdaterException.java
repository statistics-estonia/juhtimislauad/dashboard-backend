package ee.stat.dashboard.service.statsync.shared.dto;

public class StatUpdaterException extends Exception {

    public StatUpdaterException(String message) {
        super(message);
    }

}
