package ee.stat.dashboard.service.statsync.datapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ee.stat.dashboard.config.props.CronConfig;
import ee.stat.dashboard.model.process.ProcessLog;
import ee.stat.dashboard.model.widget.back.Filter;
import ee.stat.dashboard.model.widget.back.GraphType;
import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.statdataapi.StatDataApi;
import ee.stat.dashboard.model.widget.back.statdataapi.StatDataApiData;
import ee.stat.dashboard.repository.GraphTypeRepository;
import ee.stat.dashboard.repository.StatDataApiDataRepository;
import ee.stat.dashboard.repository.StatDataApiRepository;
import ee.stat.dashboard.service.StatApiType;
import ee.stat.dashboard.service.client.StatDataApiClient;
import ee.stat.dashboard.service.client.dataapi.StatDataApiResponse;
import ee.stat.dashboard.service.client.query.StatApiQuery;
import ee.stat.dashboard.service.process.ProcessLoggingHelper;
import ee.stat.dashboard.service.statdata.StatDimensionsManager;
import ee.stat.dashboard.service.statdata.StatMetaFetchException;
import ee.stat.dashboard.service.statdata.StatPublishedAtManager;
import ee.stat.dashboard.service.statdata.StatRequestBuilderManager;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlDimension;
import ee.stat.dashboard.service.statdata.request.dto.StatRequestConfig;
import ee.stat.dashboard.service.statsync.shared.dto.StatDataChange;
import ee.stat.dashboard.service.statsync.shared.dto.StatUpdaterException;
import ee.stat.dashboard.service.widget.widget.FilterService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static ee.stat.dashboard.model.widget.back.StatDataType.DATA_API;
import static java.time.LocalDateTime.now;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.springframework.transaction.annotation.Propagation.REQUIRES_NEW;

@Component
@AllArgsConstructor
@Transactional(propagation = REQUIRES_NEW)
public class SingleStatDataApiUpdater implements StatApiType {

    private static final String UPDATED_REGEX = "\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}Z";
    private final CronConfig cronConfig;
    private final FilterService filterService;
    private final StatDataApiClient statDataApiClient;
    private final StatDimensionsManager statDimensionsManager;
    private final StatRequestBuilderManager statRequestBuilderManager;
    private final StatPublishedAtManager statPublishedAtManager;
    private final ObjectMapper objectMapper;
    private final ProcessLoggingHelper log;
    private final GraphTypeRepository graphTypeRepository;
    private final StatDataApiRepository statDataApiRepository;
    private final StatDataApiDataRepository statDataApiDataRepository;

    @Override
    public StatDataType statDataType() {
        return DATA_API;
    }

    public StatDataChange updateStatDbIfDataChanged(ProcessLog pLog, StatDataApi statDataApi, Widget widget) throws StatMetaFetchException, StatUpdaterException, JsonProcessingException {
        String cube = statDataApi.getCube();
        log.analysingWidget(pLog, widget, cube, statDataType());

        StatDataChange change = new StatDataChange();
        LocalDateTime publishedAt = statPublishedAtManager.publishedAt(cube, statDataType());
        change.setPublishedAt(publishedAt);

        if (statDataApi.isForceRebuild()) {
            log.forceRebuild(pLog, widget, cube);
        } else if (publishedAt == null) {
            log.publishedAtIsMissing(pLog, widget, cube);
        } else if (statDataApi.getPublishedAt() != null && statDataApi.getPublishedAt().isEqual(publishedAt)) {
            log.publishedAtHasNotChanged(pLog, widget, cube);
            return change;
        } else {
            log.publishedAtHasChanged(pLog, widget, cube);
        }

        for (StatDataApiData statDataApiData : statDataApi.getStatDbData()) {
            analysingGraphType(pLog, widget, cube, statDataApiData.getGraphType());

            StatApiQuery dataQuery = buildQuery(statDataApi, widget, statDataApiData);

            String newRequest = objectMapper.writeValueAsString(dataQuery);
            if (!newRequest.equals(statDataApiData.getRequest())) {
                log.requestChanged(pLog, widget, newRequest, statDataApiData.getRequest(), cube);

                StatDataApiResponse response = statDataApiClient.getData(cube, dataQuery);
                setAndUpdate(statDataApiData, response);
                change.setChanged(true);
            } else {
                StatDataApiResponse response = statDataApiClient.getData(cube, dataQuery);

                if (dataChanged(statDataApiData, response)) {
                    log.responsesAreDifferent(pLog, widget, cube);
                    setAndUpdate(statDataApiData, response);
                    change.setChanged(true);
                } else {
                    log.responsesAreSame(pLog, widget, cube);

                    //normally here would be changed = false, however in a rare case diagram creation failed
                    // we need a mechanism to force diagram creation
                    if (!change.isChanged()) {
                        change.setChanged(statDataApi.isForceRebuild());
                    }
                }
            }
            sleep(cronConfig.getStatDataUpdateDelayMs());
        }

        if (change.isChanged()) {
            log.widgetHasChanged(pLog, widget, cube);
        }
        statDataApi.setPublishedAt(publishedAt);
        statDataApi.setUpdatedAt(now());
        statDataApiRepository.save(statDataApi);
        return change;
    }

    private StatApiQuery buildQuery(StatDataApi statDataApi, Widget widget, StatDataApiData statDataApiData) throws StatUpdaterException, StatMetaFetchException {
        List<Filter> filters = filterService.getDbFiltersNoCache(statDataApiData.getGraphType());
        StatRequestConfig statRequestConfig = StatRequestConfig.requestConfig(widget, statDataApi.getCube(), filters, getDimensions(statDataApi.getCube()));
        return statRequestBuilderManager.buildBody(statRequestConfig, statDataType(), widget.getId());
    }

    static boolean dataChanged(StatDataApiData statDataApiData, StatDataApiResponse newData) {
        boolean changed = false;
        for (Language lang : Language.values()) {
            String newResponse = newData.getResponse(lang).replaceAll(UPDATED_REGEX, "date-replaced");
            String existingResponse = statDataApiData.getResponse(lang).replaceAll(UPDATED_REGEX, "date-replaced");
            if (!newResponse.equals(existingResponse)) {
                changed = true;
                break;
            }
        }
        return changed;
    }

    private List<XmlDimension> getDimensions(String cube) throws StatUpdaterException, StatMetaFetchException {
        List<XmlDimension> dimensions = statDimensionsManager.getDimensions(cube, statDataType());
        if (isEmpty(dimensions)) {
            throw new StatUpdaterException("No dimensions for " + cube);
        }
        return dimensions;
    }

    private void setAndUpdate(StatDataApiData statDbData, StatDataApiResponse response) {
        statDbData.setResponseEt(response.getResponseEt());
        statDbData.setResponseEn(response.getResponseEn());
        statDataApiDataRepository.save(statDbData);
    }

    private void analysingGraphType(ProcessLog processLog, Widget widget, String cube, Long graphTypeId) {
        GraphType graphType = graphTypeRepository.findById(graphTypeId).orElseThrow(RuntimeException::new);
        log.analysingGraphType(processLog, widget, graphType, cube);
    }

    private void sleep(Integer delay) {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
