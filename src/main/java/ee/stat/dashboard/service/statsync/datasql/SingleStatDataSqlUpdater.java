package ee.stat.dashboard.service.statsync.datasql;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ee.stat.dashboard.config.props.CronConfig;
import ee.stat.dashboard.model.process.ProcessLog;
import ee.stat.dashboard.model.widget.back.Filter;
import ee.stat.dashboard.model.widget.back.GraphType;
import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.statdatasql.StatDataSql;
import ee.stat.dashboard.model.widget.back.statdatasql.StatDataSqlData;
import ee.stat.dashboard.repository.GraphTypeRepository;
import ee.stat.dashboard.repository.StatDataSqlDataRepository;
import ee.stat.dashboard.repository.StatDataSqlRepository;
import ee.stat.dashboard.service.StatApiType;
import ee.stat.dashboard.service.client.StatDataSqlClient;
import ee.stat.dashboard.service.client.datasql.StatDataSqlResponse;
import ee.stat.dashboard.service.client.query.StatApiQuery;
import ee.stat.dashboard.service.process.ProcessLoggingHelper;
import ee.stat.dashboard.service.statdata.StatDimensionsManager;
import ee.stat.dashboard.service.statdata.StatMetaFetchException;
import ee.stat.dashboard.service.statdata.StatPublishedAtManager;
import ee.stat.dashboard.service.statdata.StatRequestBuilderManager;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlDimension;
import ee.stat.dashboard.service.statdata.request.dto.StatRequestConfig;
import ee.stat.dashboard.service.statsync.shared.dto.StatDataChange;
import ee.stat.dashboard.service.statsync.shared.dto.StatUpdaterException;
import ee.stat.dashboard.service.widget.widget.FilterService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static ee.stat.dashboard.model.widget.back.StatDataType.DATA_SQL;
import static java.time.LocalDateTime.now;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.springframework.transaction.annotation.Propagation.REQUIRES_NEW;

@Component
@AllArgsConstructor
@Transactional(propagation = REQUIRES_NEW)
public class SingleStatDataSqlUpdater implements StatApiType {

    private static final String UPDATED_REGEX = "\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}Z";
    private final CronConfig cronConfig;
    private final FilterService filterService;
    private final StatDataSqlClient statDataSqlClient;
    private final StatDimensionsManager statDimensionsManager;
    private final StatRequestBuilderManager statRequestBuilderManager;
    private final StatPublishedAtManager statPublishedAtManager;
    private final ObjectMapper objectMapper;
    private final ProcessLoggingHelper log;
    private final GraphTypeRepository graphTypeRepository;
    private final StatDataSqlRepository statDataSqlRepository;
    private final StatDataSqlDataRepository statDataSqlDataRepository;

    @Override
    public StatDataType statDataType() {
        return DATA_SQL;
    }

    public StatDataChange updateStatDbIfDataChanged(ProcessLog pLog, StatDataSql statDataSql, Widget widget) throws StatMetaFetchException, StatUpdaterException, JsonProcessingException {
        String cube = statDataSql.getCube();
        log.analysingWidget(pLog, widget, cube, statDataType());

        StatDataChange change = new StatDataChange();
        LocalDateTime publishedAt = statPublishedAtManager.publishedAt(cube, statDataType());
        change.setPublishedAt(publishedAt);

        if (statDataSql.isForceRebuild()) {
            log.forceRebuild(pLog, widget, cube);
        } else if (publishedAt == null) {
            log.publishedAtIsMissing(pLog, widget, cube);
        } else if (statDataSql.getPublishedAt() != null && statDataSql.getPublishedAt().isEqual(publishedAt)) {
            log.publishedAtHasNotChanged(pLog, widget, cube);
            return change;
        } else {
            log.publishedAtHasChanged(pLog, widget, cube);
        }

        for (StatDataSqlData statDataSqlData : statDataSql.getStatDbData()) {
            analysingGraphType(pLog, widget, cube, statDataSqlData.getGraphType());

            StatApiQuery dataQuery = buildQuery(statDataSql, widget, statDataSqlData);

            String newRequest = objectMapper.writeValueAsString(dataQuery);
            if (!newRequest.equals(statDataSqlData.getRequest())) {
                log.requestChanged(pLog, widget, newRequest, statDataSqlData.getRequest(), cube);

                StatDataSqlResponse response = statDataSqlClient.getData(cube, dataQuery);
                setAndUpdate(statDataSqlData, response);
                change.setChanged(true);
            } else {
                StatDataSqlResponse response = statDataSqlClient.getData(cube, dataQuery);

                if (dataChanged(statDataSqlData, response)) {
                    log.responsesAreDifferent(pLog, widget, cube);
                    setAndUpdate(statDataSqlData, response);
                    change.setChanged(true);
                } else {
                    log.responsesAreSame(pLog, widget, cube);

                    //normally here would be changed = false, however in a rare case diagram creation failed
                    // we need a mechanism to force diagram creation
                    if (!change.isChanged()) {
                        change.setChanged(statDataSql.isForceRebuild());
                    }
                }
            }
            sleep(cronConfig.getStatDataUpdateDelayMs());
        }

        if (change.isChanged()) {
            log.widgetHasChanged(pLog, widget, cube);
        }
        statDataSql.setPublishedAt(publishedAt);
        statDataSql.setUpdatedAt(now());
        statDataSqlRepository.save(statDataSql);
        return change;
    }

    private StatApiQuery buildQuery(StatDataSql statDataSql, Widget widget, StatDataSqlData statDataSqlData) throws StatUpdaterException, StatMetaFetchException {
        List<Filter> filters = filterService.getDbFiltersNoCache(statDataSqlData.getGraphType());
        StatRequestConfig statRequestConfig = StatRequestConfig.requestConfig(widget, statDataSql.getCube(), filters, getDimensions(statDataSql.getCube()));
        return statRequestBuilderManager.buildBody(statRequestConfig, statDataType(), widget.getId());
    }

    static boolean dataChanged(StatDataSqlData statDataSqlData, StatDataSqlResponse newData) {
        boolean changed = false;
        for (Language lang : Language.values()) {
            String newResponse = newData.getResponse(lang).replaceAll(UPDATED_REGEX, "date-replaced");
            String existingResponse = statDataSqlData.getResponse(lang).replaceAll(UPDATED_REGEX, "date-replaced");
            if (!newResponse.equals(existingResponse)) {
                changed = true;
                break;
            }
        }
        return changed;
    }

    private List<XmlDimension> getDimensions(String cube) throws StatUpdaterException, StatMetaFetchException {
        List<XmlDimension> dimensions = statDimensionsManager.getDimensions(cube, statDataType());
        if (isEmpty(dimensions)) {
            throw new StatUpdaterException("No dimensions for " + cube);
        }
        return dimensions;
    }

    private void setAndUpdate(StatDataSqlData statDbData, StatDataSqlResponse response) {
        statDbData.setResponseEt(response.getResponseEt());
        statDbData.setResponseEn(response.getResponseEn());
        statDataSqlDataRepository.save(statDbData);
    }

    private void analysingGraphType(ProcessLog processLog, Widget widget, String cube, Long graphTypeId) {
        GraphType graphType = graphTypeRepository.findById(graphTypeId).orElseThrow(RuntimeException::new);
        log.analysingGraphType(processLog, widget, graphType, cube);
    }

    private void sleep(Integer delay) {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
