package ee.stat.dashboard.service.statsync.datapi;

import ee.stat.dashboard.model.process.ProcessLog;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.back.statdataapi.StatDataApi;
import ee.stat.dashboard.repository.StatDataApiRepository;
import ee.stat.dashboard.repository.WidgetRepository;
import ee.stat.dashboard.service.process.ProcessLoggingService;
import ee.stat.dashboard.service.statdata.StatMetaFetchException;
import ee.stat.dashboard.service.statsync.shared.dto.ProcessResult;
import ee.stat.dashboard.service.statsync.shared.dto.StatDataChange;
import ee.stat.dashboard.service.statsync.shared.dto.StatUpdaterException;
import ee.stat.dashboard.service.widget.diagram.CalculationException;
import ee.stat.dashboard.service.widget.diagram.DiagramBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static ee.stat.dashboard.model.process.enums.LogStatus.ERROR;
import static java.lang.String.format;

@Service
@AllArgsConstructor
@Slf4j
@Transactional
public class StatDataApisUpdater {

    private final StatDataApiRepository statDataApiRepository;
    private final WidgetRepository widgetRepository;
    private final SingleStatDataApiUpdater singleStatDataApiUpdater;
    private final DiagramBuilder diagramBuilder;
    private final ProcessLoggingService processLoggingService;

    public ProcessResult checkForStatDbChangesAndBuildNewDiagrams(ProcessLog processLog) {
        ProcessResult processResult = new ProcessResult();
        for (StatDataApi statDataApi : statDataApiRepository.findAllByOrderByWidgetAsc()) {
            Widget widget = widgetRepository.findById(statDataApi.getWidget()).orElseThrow(RuntimeException::new);
            if (widget.getStatus().isVisible()) {
                processResult.incrementAnalysed();
                try {
                    StatDataChange change = singleStatDataApiUpdater.updateStatDbIfDataChanged(processLog, statDataApi, widget);
                    if (change.isChanged()) {
                        processResult.incrementChanged();
                        diagramBuilder.createNewDiagramForUpdater(widget.getId());
                    } else {
                        processResult.incrementUnchanged();
                    }

                    if (statDataApi.isForceRebuild()) {
                        statDataApi.setForceRebuild(false);
                        statDataApiRepository.save(statDataApi);
                    }
                } catch (Exception e) {
                    processResult.incrementFailed();
                    if (e instanceof StatUpdaterException || e instanceof StatMetaFetchException || e instanceof CalculationException) {
                        processLoggingService.log(processLog, widget, ERROR,
                                e.getMessage(), statDataApi.getCube(), e.getCause());
                    } else {
                        processLoggingService.log(processLog, widget, ERROR,
                                format("Unexpected exception for widget: %s, %s", widget.getId(), e.getMessage()), statDataApi.getCube(), e);
                    }
                    statDataApi.setForceRebuild(true);
                    statDataApiRepository.save(statDataApi);
                }
            }
        }
        return processResult;
    }

}
