package ee.stat.dashboard.service.statsync.datasql;

import ee.stat.dashboard.model.process.ProcessLog;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.back.statdatasql.StatDataSql;
import ee.stat.dashboard.repository.StatDataSqlRepository;
import ee.stat.dashboard.repository.WidgetRepository;
import ee.stat.dashboard.service.process.ProcessLoggingService;
import ee.stat.dashboard.service.statdata.StatMetaFetchException;
import ee.stat.dashboard.service.statsync.shared.dto.ProcessResult;
import ee.stat.dashboard.service.statsync.shared.dto.StatDataChange;
import ee.stat.dashboard.service.statsync.shared.dto.StatUpdaterException;
import ee.stat.dashboard.service.widget.diagram.CalculationException;
import ee.stat.dashboard.service.widget.diagram.DiagramBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static ee.stat.dashboard.model.process.enums.LogStatus.ERROR;
import static java.lang.String.format;

@Service
@AllArgsConstructor
@Slf4j
@Transactional
public class StatDataSqlsUpdater {

    private final StatDataSqlRepository statDataSqlRepository;
    private final WidgetRepository widgetRepository;
    private final SingleStatDataSqlUpdater singleStatDataSqlUpdater;
    private final DiagramBuilder diagramBuilder;
    private final ProcessLoggingService processLoggingService;

    public ProcessResult checkForStatDbChangesAndBuildNewDiagrams(ProcessLog processLog) {
        ProcessResult processResult = new ProcessResult();
        for (StatDataSql statDataSql : statDataSqlRepository.findAllByOrderByWidgetAsc()) {
            Widget widget = widgetRepository.findById(statDataSql.getWidget()).orElseThrow(RuntimeException::new);
            if (widget.getStatus().isVisible()) {
                processResult.incrementAnalysed();
                try {
                    StatDataChange change = singleStatDataSqlUpdater.updateStatDbIfDataChanged(processLog, statDataSql, widget);
                    if (change.isChanged()) {
                        processResult.incrementChanged();
                        diagramBuilder.createNewDiagramForUpdater(widget.getId());
                    } else {
                        processResult.incrementUnchanged();
                    }

                    if (statDataSql.isForceRebuild()) {
                        statDataSql.setForceRebuild(false);
                        statDataSqlRepository.save(statDataSql);
                    }
                } catch (Exception e) {
                    processResult.incrementFailed();
                    if (e instanceof StatUpdaterException || e instanceof StatMetaFetchException || e instanceof CalculationException) {
                        processLoggingService.log(processLog, widget, ERROR,
                                e.getMessage(), statDataSql.getCube(), e.getCause());
                    } else {
                        processLoggingService.log(processLog, widget, ERROR,
                                format("Unexpected exception for widget: %s, %s", widget.getId(), e.getMessage()), statDataSql.getCube(), e);
                    }
                    statDataSql.setForceRebuild(true);
                    statDataSqlRepository.save(statDataSql);
                }
            }
        }
        return processResult;
    }

}
