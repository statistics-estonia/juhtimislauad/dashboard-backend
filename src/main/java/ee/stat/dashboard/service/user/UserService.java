package ee.stat.dashboard.service.user;

import ee.stat.dashboard.controller.user.dto.TaraDataDto;
import ee.stat.dashboard.model.user.user.AppUser;
import ee.stat.dashboard.model.user.user.AppUserSession;
import ee.stat.dashboard.repository.UserRepository;
import ee.stat.dashboard.service.user.dto.UserNewsDto;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;


@Service
@AllArgsConstructor
@Transactional
public class UserService {

    private final UserRepository userRepository;
    private final UserSessionService userSessionService;

    public List<AppUser> findAll() {
        return Stream.of(
                userRepository.findByIdCode("user"),
                userRepository.findByIdCode("user2"),
                userRepository.findByIdCode("admin"),
                userRepository.findByIdCode("admin2")
        ).filter(Objects::nonNull).collect(toList());
    }

    public AppUser login(TaraDataDto dto) {
        AppUser user = getOrSaveNewUser(dto);
        LocalDateTime now = LocalDateTime.now();
        userSessionService.finishOldSessions(user, now);
        AppUserSession session = userSessionService.newSession(user, dto, now);
        user.setSession(session);
        return user;
    }

    public void seenTheNews(UserNewsDto userNewsDto) {
        AppUser user = userRepository.findById(userNewsDto.getId()).orElseThrow(RuntimeException::new);
        if (!user.isSeenTheNews()) {
            user.setSeenTheNews(BooleanUtils.isTrue(userNewsDto.getSeenTheNews()));
            userRepository.save(user);
        }
    }

    private AppUser getOrSaveNewUser(TaraDataDto dto) {
        AppUser existingUser = userRepository.findByIdCode(dto.getIdCode());
        if (existingUser != null) {
            return existingUser;
        }
        return saveUser(dto);
    }

    private AppUser saveUser(TaraDataDto dto) {
        AppUser user = new AppUser();
        user.setIdCode(dto.getIdCode());
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        userRepository.save(user);
        return user;
    }
}
