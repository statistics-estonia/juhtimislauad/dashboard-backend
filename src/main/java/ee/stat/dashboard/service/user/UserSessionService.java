package ee.stat.dashboard.service.user;

import ee.stat.dashboard.config.props.SessionConfig;
import ee.stat.dashboard.config.security.StatUser;
import ee.stat.dashboard.controller.user.dto.TaraDataDto;
import ee.stat.dashboard.model.user.user.AppUser;
import ee.stat.dashboard.model.user.user.AppUserSession;
import ee.stat.dashboard.repository.UserRepository;
import ee.stat.dashboard.repository.UserSessionRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static ee.stat.dashboard.util.StatListUtil.firstOrNull;
import static java.time.LocalDateTime.now;
import static java.util.UUID.randomUUID;

@Service
@AllArgsConstructor
@Transactional
public class UserSessionService {

    private final UserSessionRepository userSessionRepository;
    private final SessionConfig sessionConfig;
    private final UserRepository userRepository;

    public void prolong(StatUser loggedInUser) {
        AppUserSession lastSession = lastSession(loggedInUser, now());
        if (lastSession != null) {
            lastSession.setSessionEndsAt(lastSession.getSessionEndsAt().plusMinutes(sessionConfig.getTime()));
            userSessionRepository.save(lastSession);
        }
    }

    public void logout(StatUser loggedInUser) {
        LocalDateTime now = now();
        AppUserSession lastSession = lastSession(loggedInUser, now);
        if (lastSession != null) {
            lastSession.setFinishedAt(now);
            userSessionRepository.save(lastSession);
        }
    }

    private AppUserSession lastSession(StatUser loggedInUser, LocalDateTime now) {
        AppUser user = userRepository.findByIdCode(loggedInUser.getUsername());
        List<AppUserSession> sessions = userSessionRepository.findByAppUserAndSessionEndsAtAfterAndFinishedAtNullOrderBySessionEndsAtDesc(user.getId(), now);
        return firstOrNull(sessions);
    }

    public void finishOldSessions(AppUser user, LocalDateTime now) {
        for (AppUserSession userLogin : userSessionRepository.findByAppUserAndSessionEndsAtAfterAndFinishedAtNullOrderBySessionEndsAtDesc(user.getId(), now)) {
            userLogin.setFinishedAt(now);
            userSessionRepository.save(userLogin);
        }
    }

    public AppUserSession newSession(AppUser user, TaraDataDto dto, LocalDateTime now) {
        AppUserSession login = new AppUserSession();
        login.setAppUser(user.getId());
        login.setLoginWith(dto.getFrom());
        login.setToken(randomUUID().toString());
        login.setIp(dto.getIp());
        login.setCreatedAt(now);
        login.setSessionEndsAt(now.plusMinutes(sessionConfig.getTime()));
        userSessionRepository.save(login);
        return login;
    }
}
