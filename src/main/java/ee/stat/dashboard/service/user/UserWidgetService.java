package ee.stat.dashboard.service.user;

import ee.stat.dashboard.config.security.StatUser;
import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.user.widget.UserWidget;
import ee.stat.dashboard.repository.UserWidgetRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class UserWidgetService {

    private final UserWidgetRepository userWidgetRepository;

    public List<Long> getSelectedWidgets(Dashboard dashboard, StatUser user) {
        return userWidgetRepository.findByDashboardAndAppUserAndSelectedOrderByOrderNrAsc(dashboard.getId(), user.getId(), true)
                .stream()
                .map(UserWidget::getWidget)
                .collect(toList());
    }
}
