package ee.stat.dashboard.service.user.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserNewsDto {
    private Long id;
    private Boolean seenTheNews;
}
