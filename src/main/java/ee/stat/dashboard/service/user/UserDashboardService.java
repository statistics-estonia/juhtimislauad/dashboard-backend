package ee.stat.dashboard.service.user;

import ee.stat.dashboard.config.security.StatUser;
import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.user.UserDashboard;
import ee.stat.dashboard.repository.UserDashboardRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class UserDashboardService {

    private final UserDashboardRepository userDashboardRepository;

    public boolean getFirstVisit(Dashboard dashboard, StatUser user) {
        List<UserDashboard> userDashboards = userDashboardRepository.findAllByDashboardAndAndAppUser(dashboard.getId(), user.getId());
        return !userDashboards.stream().findFirst().orElse(new UserDashboard()).isVisited();
    }
}
