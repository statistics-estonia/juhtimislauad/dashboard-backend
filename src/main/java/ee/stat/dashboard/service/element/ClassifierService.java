package ee.stat.dashboard.service.element;

import ee.stat.dashboard.model.classifier.Classifier;
import ee.stat.dashboard.model.classifier.ClassifierCode;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.element.dto.ClassifierResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class ClassifierService {

    private final ClassifierServiceCache classifierServiceCache;
    private final ElementService elementService;

    public List<ClassifierResponse> getRoles(Language lang) {
        List<ClassifierResponse> classifiers = classifierServiceCache.findByCodeIn(ClassifierCode.getRoleClassifiers()).stream()
                .map(c -> mapSimple(c, lang))
                .collect(toList());
        classifiers.stream()
                .filter(classifier -> classifier.getCode().isNotEhak())
                .forEach(classifier -> classifier.setElements(elementService.findAll(classifier.getCode(), lang)));
        return classifiers;
    }

    public ClassifierResponse mapSimple(Classifier clf, Language lang) {
        ClassifierResponse dto = new ClassifierResponse();
        dto.setId(clf.getId());
        dto.setCode(clf.getCode());
        dto.setName(clf.getName(lang));
        dto.setDescription(clf.getDescription(lang));
        return dto;
    }
}
