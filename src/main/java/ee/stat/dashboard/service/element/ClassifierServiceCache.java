package ee.stat.dashboard.service.element;

import ee.stat.dashboard.model.classifier.Classifier;
import ee.stat.dashboard.model.classifier.ClassifierCode;
import ee.stat.dashboard.repository.ClassifierRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ClassifierServiceCache {

    private final ClassifierRepository classifierRepository;

    @Cacheable(value = "ClassifierServiceCache_findById", key = "#classifierId")
    public Optional<Classifier> findById(Long classifierId) {
        return classifierRepository.findById(classifierId);
    }

    @Cacheable(value = "ClassifierServiceCache_findByCode", key = "#classifierCode")
    public Classifier findByCode(ClassifierCode classifierCode) {
        return classifierRepository.findByCode(classifierCode);
    }

    @Cacheable(value = "ClassifierServiceCache_findByCodeIn", key = "#classifierCodes")
    public List<Classifier> findByCodeIn(List<ClassifierCode> classifierCodes) {
        return classifierRepository.findByCodeInOrderByOrderNr(classifierCodes);
    }
}
