package ee.stat.dashboard.service.element;

import ee.stat.dashboard.model.classifier.ClassifierCode;
import ee.stat.dashboard.model.classifier.Element;
import ee.stat.dashboard.model.dashboard.DashboardStatus;
import ee.stat.dashboard.model.dashboard.DashboardUserType;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.repository.ElementRepository;
import ee.stat.dashboard.service.element.dto.RoleStrategy;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ElementServiceCache {

    private final ElementRepository elementRepository;

    @Cacheable(value = "ElementServiceCache_findById", key = "#elementId")
    public Optional<Element> findById(Long elementId) {
        return elementRepository.findById(elementId);
    }

    @Cacheable("ElementServiceCache_findAllByLevel")
    public List<Element> findAllByLevel(ClassifierCode code, Integer level) {
        return elementRepository.findAllByClfCodeAndLevel(code, level);
    }

    @Cacheable("ElementServiceCache_findAllByLevelIn")
    public List<Element> findAllByLevelIn(ClassifierCode code, List<Integer> level) {
        return elementRepository.findAllByClfCodeAndLevel(code, level);
    }

    @Cacheable("ElementServiceCache_findAllByCode")
    public List<Element> findAllByCode(ClassifierCode code, String elementCode) {
        return elementRepository.findAllByClfCodeAndElCode(code, elementCode);
    }

    @Cacheable("ElementServiceCache_findAllByClfCode")
    public List<Element> findAllByClfCode(ClassifierCode code) {
        return elementRepository.findAllByClfCode(code);
    }

    @Cacheable("ElementServiceCache_findAllLowerNamesByClfCode")
    public List<String> findAllLowerNamesByClfCode(ClassifierCode code, Language language, List<Integer> levels) {
        return elementRepository.findAllLowerNamesByClfCode(code, language, levels);
    }

    @Cacheable("ElementServiceCache_findAllByClfCodeAndRoleOrderByName")
    public List<Element> findAllByClfCodeAndRoleOrderByName(ClassifierCode code, RoleStrategy role, Language language) {
        return elementRepository.findAllByClfCodeAndRoleOrderByName(code, role, language, DashboardStatus.VISIBLE, DashboardUserType.ADMIN);
    }

    @Cacheable(value = "ElementServiceCache_findAllToByFrom")
    public List<Element> findAllToByFrom(Long id) {
        return elementRepository.findAllToByFrom(id);
    }

    @Cacheable(value = "ElementServiceCache_findAllDashboardRegions", key = "#dashboardId")
    public List<Element> findAllDashboardRegions(Long dashboardId) {
        return elementRepository.findAllDashboardRegions(dashboardId);
    }
}
