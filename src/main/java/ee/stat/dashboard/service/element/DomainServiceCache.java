package ee.stat.dashboard.service.element;

import ee.stat.dashboard.model.classifier.Domain;
import ee.stat.dashboard.repository.DomainRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class DomainServiceCache {

    private final DomainRepository domainRepository;

    @Cacheable(value = "DomainServiceCache_findAllFromByTo", key = "#domainId")
    public List<Domain> findAllFromByTo(Long domainId) {
        return domainRepository.findAllFromByTo(domainId);
    }

    @Cacheable(value = "DomainServiceCache_findAllByWidget", key = "#widgetId")
    public List<Domain> findAllByWidget(Long widgetId) {
        return domainRepository.findAllByWidget(widgetId);
    }
}
