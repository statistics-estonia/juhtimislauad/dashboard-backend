package ee.stat.dashboard.service.element.dto;

import ee.stat.dashboard.service.dashboard.dto.DashboardResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class ElementResponse {

    private Long id;
    private String code;
    private String name;
    private Integer level;
    private Long parentId;
    private Boolean selected;

    private List<ElementResponse> subElements;
    private List<DashboardResponse> dashboards;

}
