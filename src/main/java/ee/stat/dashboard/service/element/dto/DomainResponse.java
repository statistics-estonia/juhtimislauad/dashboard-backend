package ee.stat.dashboard.service.element.dto;

import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DomainResponse {

    private Long id;
    private String code;
    private String name;
    private Integer level;
    private Integer orderNr;
    private String levelName;
    private Boolean showLevelName;
    private Long parentId;
    private DomainResponse parent;

    private List<WidgetResponse> widgets;
    private List<DomainResponse> subElements;
}
