package ee.stat.dashboard.service.element;

import ee.stat.dashboard.model.classifier.Domain;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.repository.DomainRepository;
import ee.stat.dashboard.repository.WidgetRepository;
import ee.stat.dashboard.service.element.dto.DomainResponse;
import ee.stat.dashboard.service.widget.widget.WidgetConverter;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
@AllArgsConstructor
public class DomainService {

    private final DomainRepository domainRepository;
    private final WidgetRepository widgetRepository;
    private final WidgetConverter widgetConverter;
    private final DomainConverter domainConverter;

    @Cacheable(value = "DomainService_loadDomainsAndWidgets", key = "#dashboardId.toString().concat(#lang.name())")
    public List<DomainResponse> loadDomainsAndWidgets(Long dashboardId, Language lang) {
        return domainRepository.findByDashboardAndLevelOrderByOrderNr(dashboardId, 0).stream()
                .map(topDomain -> mapDomainWithChildrenAndWidgets(topDomain, lang))
                .collect(toList());
    }

    private DomainResponse mapDomainWithChildrenAndWidgets(Domain topDomain, Language lang) {
        DomainResponse domain = domainConverter.mapSimple(topDomain, lang);
        setChildrenAndWidgets(domain, lang);
        return domain;
    }

    private void setChildrenAndWidgets(DomainResponse domain, Language lang) {
        List<DomainResponse> allToByFrom = domainRepository.findAllToByFrom(domain.getId()).stream()
                .map(o -> domainConverter.mapSimple(o, domain.getId(), lang))
                .collect(toList());
        if (isNotEmpty(allToByFrom)) {
            for (DomainResponse subElement : allToByFrom) {
                setChildrenAndWidgets(subElement, lang);
            }
            domain.setSubElements(allToByFrom);
        }
        List<Widget> widgets = widgetRepository.findWidgetsByDomain(domain.getId(), lang);
        if (isNotEmpty(widgets)) {
            domain.setWidgets(map(widgets, lang));
        }
    }

    private List<WidgetResponse> map(List<Widget> widgets, Language lang) {
        return widgets.stream()
                .filter(w -> w.getStatus().isVisible())
                .map(w -> widgetConverter.mapMinimal(w, lang))
                .filter(Objects::nonNull)
                .collect(toList());
    }

}
