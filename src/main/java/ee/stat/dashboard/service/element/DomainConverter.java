package ee.stat.dashboard.service.element;

import ee.stat.dashboard.model.classifier.Domain;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.element.dto.DomainResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
@AllArgsConstructor
public class DomainConverter {

    private final DomainServiceCache domainServiceCache;

    public DomainResponse mapSimpleWithParent(Domain element, Language lang) {
        DomainResponse response = mapSimple(element, lang);
        if (response.getLevel() > 1) {
            List<Domain> parents = domainServiceCache.findAllFromByTo(element.getId());
            if (isNotEmpty(parents)) {
                response.setParent(mapSimple(parents.get(0), lang));
            }
        }
        return response;
    }

    public DomainResponse mapSimple(Domain element, Language lang) {
        DomainResponse dto = new DomainResponse();
        dto.setId(element.getId());
        dto.setCode(element.getCode());
        dto.setLevel(element.getLevel());
        dto.setOrderNr(element.getOrderNr());
        dto.setLevelName(element.getLevelName(lang));
        dto.setName(element.getName(lang));
        if (element.isShowLevelName()) {
            dto.setShowLevelName(element.isShowLevelName());
        }
        return dto;
    }

    public DomainResponse mapSimple(Domain element, Long parentId, Language lang) {
        DomainResponse dto = mapSimple(element, lang);
        dto.setParentId(parentId);
        return dto;
    }
}
