package ee.stat.dashboard.service.element.dto;

public enum RoleStrategy {
    HAS_DASHBOARDS;

    public boolean hasDashboard() {
        return this == HAS_DASHBOARDS;
    }
}
