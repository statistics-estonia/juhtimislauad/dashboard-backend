package ee.stat.dashboard.service.element;

import ee.stat.dashboard.config.props.ElementConfig;
import ee.stat.dashboard.controller.element.dto.ElementStrategy;
import ee.stat.dashboard.controller.element.dto.ParentStrategy;
import ee.stat.dashboard.model.classifier.ClassifierCode;
import ee.stat.dashboard.model.classifier.Element;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.element.dto.ElementResponse;
import ee.stat.dashboard.service.element.dto.RoleStrategy;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
@AllArgsConstructor
@Transactional(readOnly = true)
public class ElementService {

    private final ElementServiceCache elementServiceCache;
    private final ElementConfig elementConfig;

    public Optional<Element> findById(Long id) {
        return elementServiceCache.findById(id);
    }

    public List<Element> findAllByClfCodeAndElCode(ClassifierCode code, String elCode) {
        return elementServiceCache.findAllByCode(code, elCode);
    }

    public Element getWholeEstonia() {
        String wholeCode = elementConfig.getWholeCode();
        if (wholeCode == null) {
            throw new IllegalStateException("Whole country code not set");
        }
        List<Element> whole = findAllByClfCodeAndElCode(ClassifierCode.EHAK, wholeCode);
        if (isEmpty(whole)) {
            throw new IllegalStateException("Whole country code missing");
        }
        return whole.get(0);
    }

    public List<ElementResponse> findAll(ClassifierCode code, Language lang) {
        return elementServiceCache.findAllByClfCode(code).stream()
                .map(o -> mapSimple(o, lang))
                .collect(toList());
    }

    public List<ElementResponse> findAll(ClassifierCode code, RoleStrategy role, Language lang) {
        return elementServiceCache.findAllByClfCodeAndRoleOrderByName(code, role, lang).stream()
                .map(o -> mapSimple(o, lang))
                .collect(toList());
    }

    public List<ElementResponse> findAll(ClassifierCode code, Language lang, List<Integer> level, Long parentId, String elementCode, ElementStrategy strategy, ParentStrategy parentStrategy) {
        List<ElementResponse> elements = getDbElements(code, level, parentId, elementCode, parentStrategy).stream()
                .map(o -> mapSimple(o, lang))
                .collect(toList());
        if (strategy.isFlat()) {
            return elements;
        }
        for (ElementResponse element : elements) {
            setChildren(element, lang);
        }
        return elements;
    }

    public List<ElementResponse> findAllDashboardRegions(Long dashboardId, Language lang) {
        return elementServiceCache.findAllDashboardRegions(dashboardId).stream()
                .map(o -> mapSimple(o, lang))
                .collect(toList());
    }

    public ElementResponse mapSimple(Element element, Language lang) {
        ElementResponse dto = new ElementResponse();
        dto.setId(element.getId());
        dto.setCode(element.getCode());
        dto.setLevel(element.getLevel());
        dto.setName(element.getName(lang));
        return dto;
    }

    public ElementResponse mapSimple(Element element, Long parentId, Language lang) {
        ElementResponse dto = mapSimple(element, lang);
        dto.setParentId(parentId);
        return dto;
    }

    private List<Element> getDbElements(ClassifierCode code, List<Integer> level, Long parentId, String elementCode, ParentStrategy parentStrategy) {
        if (parentId != null) {
            List<Element> elements = new ArrayList<>(elementServiceCache.findAllToByFrom(parentId));
            if (parentStrategy.isIncluded()) {
                elementServiceCache.findById(parentId).ifPresent(e -> elements.add(0, e));
            }
            return elements;
        }
        if (elementCode != null) {
            return elementServiceCache.findAllByCode(code, elementCode);
        }
        return elementServiceCache.findAllByLevelIn(code, level);
    }

    private void setChildren(ElementResponse element, Language lang) {
        List<ElementResponse> allToByFrom = elementServiceCache.findAllToByFrom(element.getId()).stream()
                .map(o -> mapSimple(o, element.getId(), lang))
                .collect(toList());
        if (isNotEmpty(allToByFrom)) {
            for (ElementResponse subElement : allToByFrom) {
                setChildren(subElement, lang);
            }
            element.setSubElements(allToByFrom);
        }
    }
}
