package ee.stat.dashboard.service.element.dto;

import ee.stat.dashboard.model.classifier.ClassifierCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class ClassifierResponse {

    private Long id;
    private ClassifierCode code;
    private String name;
    private String description;
    private List<ElementResponse> elements;
}
