package ee.stat.dashboard.service.element;

import ee.stat.dashboard.model.widget.back.GraphType;
import ee.stat.dashboard.model.widget.back.enums.MapType;
import lombok.NoArgsConstructor;

import java.util.List;

import static java.util.Collections.singletonList;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class EhakLevels {

    public static final Integer COUNTRY = 0;
    public static final Integer COUNTY = 1;
    public static final Integer REGION = 2;
    public static final List<Integer> COUNTRY_AND_COUNTY_LIST = List.of(COUNTRY, COUNTY);
    public static final List<Integer> COUNTY_LIST = singletonList(COUNTY);
    public static final List<Integer> REGIONAL_LIST = singletonList(REGION);

    public static List<Integer> ehakLevelToRequestLevels(Integer level, GraphType type) {
        if (type.getType().isMap() && type.getMapRule().isNotDynamic()) {
            return type.getMapRule().isMK() ? COUNTY_LIST : REGIONAL_LIST;
        }
        if (level.equals(REGION)) {
            return REGIONAL_LIST;
        }
        if (level.equals(COUNTY)) {
            return COUNTY_LIST;
        }

        return type.getType().isMap() ? COUNTY_LIST : COUNTRY_AND_COUNTY_LIST;
    }

    public static MapType ehakToMapType(Integer level) {
        return level.equals(REGION) ? MapType.KOV : MapType.MK;
    }
}
