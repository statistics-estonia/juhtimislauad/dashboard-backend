package ee.stat.dashboard.service.logcleanup;

import ee.stat.dashboard.config.props.CronConfig;
import ee.stat.dashboard.model.process.ProcessLog;
import ee.stat.dashboard.repository.ProcessLogRepository;
import ee.stat.dashboard.repository.StatLogRepository;
import ee.stat.dashboard.service.statsync.shared.dto.ProcessResult;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.time.LocalDateTime.now;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service
@AllArgsConstructor
public class LogCleanupService {

    private final ProcessLogRepository processLogRepository;
    private final StatLogRepository statLogRepository;
    private final CronConfig cronConfig;

    public ProcessResult cleanup() {
        ProcessResult processResult = new ProcessResult();
        List<ProcessLog> processLogs = processLogRepository.findAllByStartTimeBefore(now().minusDays(cronConfig.getLogCleanupDays()));
        for (ProcessLog processLog : processLogs) {
            processResult.incrementAnalysed();
            statLogRepository.deleteAllByProcessLog(processLog.getId());
            processResult.incrementChanged();
        }
        List<Long> processIds = processLogs.stream().map(ProcessLog::getId).collect(toList());
        processLogRepository.deleteByIdIn(processIds);
        return processResult;
    }
}
