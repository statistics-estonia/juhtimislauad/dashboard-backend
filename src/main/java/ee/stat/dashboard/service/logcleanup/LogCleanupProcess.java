package ee.stat.dashboard.service.logcleanup;

import ee.stat.dashboard.model.process.Process;
import ee.stat.dashboard.model.process.ProcessLog;
import ee.stat.dashboard.model.process.enums.LogStatus;
import ee.stat.dashboard.model.process.enums.ProcessCode;
import ee.stat.dashboard.repository.ProcessRepository;
import ee.stat.dashboard.service.process.ProcessLogService;
import ee.stat.dashboard.service.process.ProcessLoggingService;
import ee.stat.dashboard.service.statsync.shared.dto.ProcessResult;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static ee.stat.dashboard.model.process.enums.ProcessCode.PROCESS_LOG_CLEANUP;
import static java.lang.String.format;

@Slf4j
@Service
@AllArgsConstructor
@Transactional
public class LogCleanupProcess {

    private static final ProcessCode PROCESS_CODE = PROCESS_LOG_CLEANUP;
    private final ProcessLogService processLogService;
    private final ProcessLoggingService processLoggingService;
    private final ProcessRepository processRepository;
    private final LogCleanupService logCleanupService;

    public void process() {
        ProcessLog processLog = processLogService.start(findProcess());
        try {
            ProcessResult result = logCleanupService.cleanup();
            processLogService.finished(processLog, result);
        } catch (Exception e) {
            processLoggingService.log(processLog, null, LogStatus.ERROR, "Process failed", null, e);
            processLogService.fail(processLog);
        }
    }

    private Process findProcess() {
        Process process = processRepository.findByCode(PROCESS_CODE);
        if (process == null) processNotFound();
        return process;
    }

    private void processNotFound() {
        log.error("Process {} not found", PROCESS_CODE.name());
        throw new RuntimeException(format("Process %s not found", PROCESS_CODE.name()));
    }
}
