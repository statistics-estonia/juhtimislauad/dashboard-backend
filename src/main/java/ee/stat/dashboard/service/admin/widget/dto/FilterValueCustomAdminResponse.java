package ee.stat.dashboard.service.admin.widget.dto;

import ee.stat.dashboard.model.widget.back.enums.OperationType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class FilterValueCustomAdminResponse {

    private String displayNameEt;
    private String displayNameEn;
    private String dimension1NameEt;
    private String dimension1NameEn;
    private String dimension2NameEt;
    private String dimension2NameEn;
    private String selectionValue1;
    private String selectionValue2;
    private OperationType operation;
}
