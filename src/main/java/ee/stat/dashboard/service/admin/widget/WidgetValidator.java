package ee.stat.dashboard.service.admin.widget;

import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import ee.stat.dashboard.service.widget.widget.timeperiod.TimePeriodLogic;
import ee.stat.dashboard.service.widget.widget.timeperiod.TimePeriodService;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import static ee.stat.dashboard.util.ErrorCode.WIDGET_ADMIN_CUBE_CODE_MISSING;
import static ee.stat.dashboard.util.ErrorCode.WIDGET_ADMIN_DIMENSIONS_MISSING;
import static ee.stat.dashboard.util.ErrorCode.WIDGET_ADMIN_END_DATE_INCORRECT;
import static ee.stat.dashboard.util.ErrorCode.WIDGET_ADMIN_EXCEL_NAME_MISSING;
import static ee.stat.dashboard.util.ErrorCode.WIDGET_ADMIN_NAME_EN_MISSING;
import static ee.stat.dashboard.util.ErrorCode.WIDGET_ADMIN_NAME_ET_MISSING;
import static ee.stat.dashboard.util.ErrorCode.WIDGET_ADMIN_PERIODS_MUST_BE_POSITIVE;
import static ee.stat.dashboard.util.ErrorCode.WIDGET_ADMIN_SHORTNAME_EN_MISSING;
import static ee.stat.dashboard.util.ErrorCode.WIDGET_ADMIN_SHORTNAME_ET_MISSING;
import static ee.stat.dashboard.util.ErrorCode.WIDGET_ADMIN_START_DATE_INCORRECT;
import static ee.stat.dashboard.util.ErrorCode.WIDGET_ADMIN_START_IS_AFTER_END;
import static ee.stat.dashboard.util.ErrorCode.WIDGET_ADMIN_STATUS_MISSING;
import static ee.stat.dashboard.util.ErrorCode.WIDGET_ADMIN_TIME_PERIOD_MISSING;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Slf4j
@Service
@AllArgsConstructor
public class WidgetValidator {

    private final TimePeriodService timePeriodService;

    public void validate(WidgetAdminResponse widget) {
        if (widget.getShortnameEt() == null) {
            throw new StatBadRequestException(WIDGET_ADMIN_SHORTNAME_ET_MISSING);
        }
        if (widget.getShortnameEn() == null) {
            throw new StatBadRequestException(WIDGET_ADMIN_SHORTNAME_EN_MISSING);
        }
        if (widget.getNameEt() == null) {
            throw new StatBadRequestException(WIDGET_ADMIN_NAME_ET_MISSING);
        }
        if (widget.getNameEn() == null) {
            throw new StatBadRequestException(WIDGET_ADMIN_NAME_EN_MISSING);
        }
        if (widget.getStatus() == null) {
            throw new StatBadRequestException(WIDGET_ADMIN_STATUS_MISSING);
        }
        if (widget.getTimePeriod() == null) {
            throw new StatBadRequestException(WIDGET_ADMIN_TIME_PERIOD_MISSING);
        }
        if (widget.getTimePeriod() != null) {
            validateTimePeriodDates(widget);
        }
        if (widget.getPeriods() != null && widget.getPeriods() < 1) {
            throw new StatBadRequestException(WIDGET_ADMIN_PERIODS_MUST_BE_POSITIVE);
        }
        if (widget.getStartDate() != null && widget.getEndDate() != null) {
            if (widget.getStartDate().isAfter(widget.getEndDate())) {
                throw new StatBadRequestException(WIDGET_ADMIN_START_IS_AFTER_END);
            }
        }
        if (widget.getStatDb() != null) {
            validateStatDb(widget);
        }
        if (widget.getExcel() != null && widget.getExcel().getFilename() == null) {
            throw new StatBadRequestException(WIDGET_ADMIN_EXCEL_NAME_MISSING);
        }
    }

    void validateTimePeriodDates(WidgetAdminResponse widget) {
        TimePeriodLogic logic = timePeriodService.getLogic(widget.getTimePeriod());
        if (logic.inValid(widget.getStartDate())) {
            throw new StatBadRequestException(WIDGET_ADMIN_START_DATE_INCORRECT);
        }
        if (logic.inValid(widget.getEndDate())) {
            throw new StatBadRequestException(WIDGET_ADMIN_END_DATE_INCORRECT);
        }
    }

    private void validateStatDb(WidgetAdminResponse widget) {
        if (widget.getStatDb().getCube() == null) {
            throw new StatBadRequestException(WIDGET_ADMIN_CUBE_CODE_MISSING);
        }
        if (isNotEmpty(widget.getGraphTypes()) && isEmpty(widget.getDimensions())) {
            throw new StatBadRequestException(WIDGET_ADMIN_DIMENSIONS_MISSING);
        }
    }
}
