package ee.stat.dashboard.service.admin.dashboard;

import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.service.admin.dashboard.dto.DashboardAdminResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class DashboardAdminConverter {

    private final ElementAdminService elementAdminService;

    public DashboardAdminResponse mapWRegions(Dashboard dashboard) {
        DashboardAdminResponse dto = mapSimple(dashboard);
        if (dashboard.getType().isRegional()) {
            dto.setRegions(elementAdminService.findAllDashboardRegions(dashboard.getId()));
        }
        return dto;
    }

    public DashboardAdminResponse mapSimple(Dashboard dashboard) {
        DashboardAdminResponse dto = new DashboardAdminResponse();
        dto.setId(dashboard.getId());
        dto.setCode(dashboard.getCode());
        dto.setNameEn(dashboard.getNameEn());
        dto.setNameEt(dashboard.getNameEt());
        dto.setType(dashboard.getType());
        dto.setStatus(dashboard.getStatus());
        return dto;
    }
}
