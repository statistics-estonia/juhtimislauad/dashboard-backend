package ee.stat.dashboard.service.admin.widget;

import ee.stat.dashboard.config.props.ExcelConfig;
import ee.stat.dashboard.model.widget.back.enums.ExcelType;
import lombok.AllArgsConstructor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Service
@AllArgsConstructor
public class ExcelFileStorageService {

    private final ExcelConfig excelConfig;

    public String storeFile(MultipartFile file) {
        String originalFileName = StringUtils.cleanPath(file.getOriginalFilename());
        if (originalFileName.contains("..")) {
            throw new ResponseStatusException(BAD_REQUEST, "Filename contains invalid path sequence " + originalFileName);
        }

        try {
            String fileName = filename(file);
            Path targetLocation = excelConfig.getLocationPath().resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            return fileName;
        } catch (IOException ex) {
            throw new ResponseStatusException(BAD_REQUEST, "Could not store file " + originalFileName + ". Please try again!", ex);
        }
    }

    private String filename(MultipartFile file) {
        int indexOfDot = file.getOriginalFilename().lastIndexOf('.');
        String name = file.getOriginalFilename().substring(0, indexOfDot);
        String extension = file.getOriginalFilename().substring(indexOfDot);
        return name + "--" + LocalDateTime.now() + extension;
    }

    public Resource loadFileAsResource(String fileName, ExcelType widget) {
        try {
            Path path = widget.isDataOnly() ? excelConfig.getLocationPath() : excelConfig.getWidgetLocationPath();
            Path filePath = path.resolve(fileName).normalize();
            Resource resource = new FileSystemResource(filePath);
            if (resource.exists()) {
                return resource;
            } else {
                throw new ResponseStatusException(BAD_REQUEST, "File not found " + fileName);
            }
        } catch (Exception ex) {
            throw new ResponseStatusException(BAD_REQUEST, "File not found " + fileName, ex);
        }
    }
}
