package ee.stat.dashboard.service.admin.widget.dto;


import ee.stat.dashboard.model.widget.back.enums.ExcelType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExcelAdminDto {

    private Long id;
    private String filename;
    private String location;
    private ExcelType excelType;
}
