package ee.stat.dashboard.service.admin.dto;

public enum DataSource {

    DATA_API,
    DATA_SQL,
    EXCEL,
    /**
     * pseudo-source to describe lack of a source
     */
    NONE
}
