package ee.stat.dashboard.service.admin.widget.dto;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class FilterValueAdminDto {

    private Long id;
    private Integer orderNr;
    private String valueEt;
    private String valueEn;
    private String displayValueEt;
    private String displayValueEn;
    private Boolean selected;
    private Boolean custom;
    private String statId;
}
