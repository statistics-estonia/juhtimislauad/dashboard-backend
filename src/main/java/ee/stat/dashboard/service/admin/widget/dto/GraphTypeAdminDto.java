package ee.stat.dashboard.service.admin.widget.dto;

import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.MapRule;
import ee.stat.dashboard.model.widget.back.enums.VerticalRule;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GraphTypeAdminDto {

    private Long id;
    private GraphTypeEnum type;
    private Boolean defaultType;
    private MapRule mapRule;
    private VerticalRule verticalRule;

    private StatDbDataAdminDto statDbData;
    private StatDataApiDataAdminDto statDataApiData;
    private StatDataApiDataAdminDto statDataSqlData;
    private List<FilterAdminDto> filters;
}
