package ee.stat.dashboard.service.admin.widget;


import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.back.excel.Excel;
import ee.stat.dashboard.model.widget.back.statdataapi.StatDataApi;
import ee.stat.dashboard.model.widget.back.statdatasql.StatDataSql;
import ee.stat.dashboard.repository.DashboardRepository;
import ee.stat.dashboard.repository.ExcelRepository;
import ee.stat.dashboard.repository.StatDataApiRepository;
import ee.stat.dashboard.repository.StatDataSqlRepository;
import ee.stat.dashboard.service.admin.dashboard.DashboardAdminConverter;
import ee.stat.dashboard.service.admin.dashboard.dto.DashboardAdminResponse;
import ee.stat.dashboard.service.admin.dto.DataSource;
import ee.stat.dashboard.service.admin.widget.dto.ExcelAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.StatDataAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static ee.stat.dashboard.service.admin.dto.DataSource.DATA_API;
import static ee.stat.dashboard.service.admin.dto.DataSource.DATA_SQL;
import static ee.stat.dashboard.service.admin.dto.DataSource.EXCEL;
import static ee.stat.dashboard.service.admin.dto.DataSource.NONE;
import static ee.stat.dashboard.util.StatListUtil.firstOrNull;
import static java.util.stream.Collectors.toList;
import static org.hibernate.internal.util.collections.CollectionHelper.isNotEmpty;

@Service
@AllArgsConstructor
public class WidgetAdminConverter {

    private final StatDataApiRepository statDataApiRepository;
    private final StatDataSqlRepository statDataSqlRepository;
    private final DashboardRepository dashboardRepository;
    private final ExcelRepository excelRepository;
    private final DashboardAdminConverter dashboardAdminConverter;

    public WidgetAdminResponse mapWCubes(Widget widget) {
        WidgetAdminResponse dto = mapMainData(widget);
        List<String> cubes = cubes(widget);
        dto.setCube(firstOrNull(cubes));
        return dto;
    }

    public WidgetAdminResponse mapWCubesWDashboards(Widget widget) {
        WidgetAdminResponse dto = mapWCubes(widget);
        dto.setSources(getSources(widget));
        dto.setCube(firstOrNull(cubes(widget)));
        dto.setDashboards(mapDashboards(widget));
        return dto;
    }

    public WidgetAdminResponse mapMainData(Widget widget) {
        WidgetAdminResponse dto = new WidgetAdminResponse();
        dto.setId(widget.getId());
        dto.setCode(widget.getCode());
        dto.setShortnameEt(widget.getShortnameEt());
        dto.setShortnameEn(widget.getShortnameEn());
        dto.setNameEt(widget.getNameEt());
        dto.setNameEn(widget.getNameEn());
        dto.setStatus(widget.getStatus());
        return dto;
    }

    public StatDataAdminDto convertStatDb(StatDataApi statDataApi) {
        StatDataAdminDto statDbDto = new StatDataAdminDto();
        statDbDto.setId(statDataApi.getId());
        statDbDto.setCube(statDataApi.getCube());
        statDbDto.setStatDataType(StatDataType.DATA_API);
        return statDbDto;
    }

    public StatDataAdminDto convertStatDb(StatDataSql statDataSql) {
        StatDataAdminDto statDbDto = new StatDataAdminDto();
        statDbDto.setId(statDataSql.getId());
        statDbDto.setCube(statDataSql.getCube());
        statDbDto.setStatDataType(StatDataType.DATA_SQL);
        return statDbDto;
    }

    public ExcelAdminDto convertExcel(Excel excel) {
        ExcelAdminDto excelDto = new ExcelAdminDto();
        excelDto.setId(excel.getId());
        excelDto.setExcelType(excel.getExcelType());
        excelDto.setFilename(excel.getFilename());
        excelDto.setLocation(excel.getLocation());
        return excelDto;
    }

    private List<String> cubes(Widget widget) {
        List<String> apiCubes = statDataApiRepository.findByWidget(widget.getId()).stream().map(StatDataApi::getCube).collect(toList());
        if (isNotEmpty(apiCubes)) {
            return apiCubes;
        }
        return statDataSqlRepository.findByWidget(widget.getId()).stream().map(StatDataSql::getCube).collect(toList());
    }

    private List<DashboardAdminResponse> mapDashboards(Widget widget) {
        return dashboardRepository.findAllByWidget(widget.getId()).stream().map(dashboardAdminConverter::mapSimple).collect(toList());
    }

    public void mapDetailedData(WidgetAdminResponse dto, Widget widget) {
        dto.setTimePeriod(widget.getTimePeriod());
        dto.setDivisor(widget.getDivisor());
        dto.setPeriods(widget.getPeriods());
        dto.setStartDate(widget.getStartDate());
        dto.setEndDate(widget.getEndDate());
        dto.setPrecisionScale(widget.getPrecisionScale());
        dto.setNoteEt(widget.getNoteEt());
        dto.setNoteEn(widget.getNoteEn());
        dto.setUnitEt(widget.getUnitEt());
        dto.setUnitEn(widget.getUnitEn());
        dto.setSourceEt(widget.getSourceEt());
        dto.setSourceEn(widget.getSourceEn());
        dto.setDescriptionEt(widget.getDescriptionEt());
        dto.setDescriptionEn(widget.getDescriptionEn());
        dto.setMethodsLinkEt(widget.getMethodsLinkEt());
        dto.setMethodsLinkEn(widget.getMethodsLinkEn());
        dto.setStatisticianJobLinkEt(widget.getStatisticianJobLinkEt());
        dto.setStatisticianJobLinkEn(widget.getStatisticianJobLinkEn());
    }

    private List<DataSource> getSources(Widget widget) {
        List<DataSource> sources = new ArrayList<>();
        if (excelRepository.existsByWidget(widget.getId())) {
            sources.add(EXCEL);
        }
        if (statDataApiRepository.existsByWidget(widget.getId())) {
            sources.add(DATA_API);
        } else if (statDataSqlRepository.existsByWidget(widget.getId())) {
            sources.add(DATA_SQL);
        }
        if (sources.isEmpty()) {
            sources.add(NONE);
        }
        return sources;
    }
}
