package ee.stat.dashboard.service.admin.widget.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StatDbDataAdminDto {

    private Long id;
    private String url;

}
