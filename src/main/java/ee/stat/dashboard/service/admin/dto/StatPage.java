package ee.stat.dashboard.service.admin.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class StatPage<T> {

    private List<T> content;
    private Integer page;
    private Integer size;
    private Integer totalPages;
    private long totalElements;
}
