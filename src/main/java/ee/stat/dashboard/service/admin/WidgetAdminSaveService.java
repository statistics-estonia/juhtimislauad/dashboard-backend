package ee.stat.dashboard.service.admin;

import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.repository.WidgetRepository;
import ee.stat.dashboard.service.admin.widget.ExcelAdminService;
import ee.stat.dashboard.service.admin.widget.GraphTypeAdminService;
import ee.stat.dashboard.service.admin.widget.StatDataAdminService;
import ee.stat.dashboard.service.admin.widget.WidgetCacheManager;
import ee.stat.dashboard.service.admin.widget.WidgetDashboardService;
import ee.stat.dashboard.service.admin.widget.WidgetDeleteService;
import ee.stat.dashboard.service.admin.widget.WidgetValidator;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import ee.stat.dashboard.service.widget.diagram.DiagramBuilder;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static ee.stat.dashboard.service.widget.widget.WidgetCodeUtil.toCode;
import static ee.stat.dashboard.util.ErrorCode.WIDGET_IS_NOT_EXISTING;
import static java.lang.String.format;
import static java.time.LocalDateTime.now;
import static java.time.temporal.TemporalAdjusters.firstDayOfMonth;
import static java.time.temporal.TemporalAdjusters.firstDayOfYear;

@Slf4j
@Service
@AllArgsConstructor
@Transactional
public class WidgetAdminSaveService {

    private final WidgetValidator widgetValidator;
    private final WidgetRepository widgetRepository;
    private final WidgetDashboardService widgetDashboardService;
    private final StatDataAdminService statDataAdminService;
    private final ExcelAdminService excelAdminService;
    private final GraphTypeAdminService graphTypeAdminService;
    private final WidgetDeleteService widgetDeleteService;
    private final DiagramBuilder diagramBuilder;
    private final WidgetCacheManager widgetCacheManager;

    public String buildNewDiagram(WidgetAdminResponse widget) {
        try {
            diagramBuilder.createNewDiagramForUpdater(widget.getId());
            return "";
        } catch (Exception e) {
            Throwable error = e.getCause() != null ? e.getCause() : e;
            log.error("error creating diagram for widget {}: {}", widget.getId(), error.getMessage(), error);
            return format("Error creating diagram for widget %s: %s", error.getClass(), error.getMessage());
        }
    }

    public void buildNewDiagramWithId(Long widgetId) {
        try {
            diagramBuilder.createNewDiagramForUpdaterWithoutTrans(widgetId);
        } catch (Exception e) {
            Throwable error = e.getCause() != null ? e.getCause() : e;
            log.error("error creating diagram for widget {}: {}", widgetId, error.getMessage(), error);
        }
    }

    public Long save(WidgetAdminResponse widget) {
        fixupDates(widget);
        widgetValidator.validate(widget);
        widget.setCode(toCode(widget.getNameEn()));
        Widget dbWidget = new Widget();
        updateEntity(dbWidget, widget);
        dbWidget.setCreatedAt(now());
        dbWidget.setUpdatedAt(null);

        return saveOrUpdate(widget, dbWidget).getId();
    }

    public Long update(Long widgetId, WidgetAdminResponse widget) {
        fixupDates(widget);
        widgetValidator.validate(widget);
        widget.setCode(toCode(widget.getNameEn()));
        Widget dbWidget = widgetRepository.findById(widgetId).orElseThrow(() -> new StatBadRequestException(WIDGET_IS_NOT_EXISTING));
        updateEntity(dbWidget, widget);
        dbWidget.setUpdatedAt(now());

        WidgetAdminResponse update = saveOrUpdate(widget, dbWidget);
        widgetCacheManager.update(update);
        return update.getId();
    }

    public void delete(Long widgetId) {
        Widget widget = widgetRepository.findById(widgetId).orElseThrow(() -> new StatBadRequestException(WIDGET_IS_NOT_EXISTING));
        widgetCacheManager.delete(widget);
        widgetDeleteService.delete(widget);
    }

    private void updateEntity(Widget dbWidget, WidgetAdminResponse widget) {
        dbWidget.setCode(widget.getCode());
        dbWidget.setShortnameEn(widget.getShortnameEn());
        dbWidget.setShortnameEt(widget.getShortnameEt());
        dbWidget.setNameEn(widget.getNameEn());
        dbWidget.setNameEt(widget.getNameEt());
        dbWidget.setDescriptionEn(widget.getDescriptionEn());
        dbWidget.setDescriptionEt(widget.getDescriptionEt());
        dbWidget.setNoteEn(widget.getNoteEn());
        dbWidget.setNoteEt(widget.getNoteEt());
        dbWidget.setSourceEn(widget.getSourceEn());
        dbWidget.setSourceEt(widget.getSourceEt());
        dbWidget.setUnitEn(widget.getUnitEn());
        dbWidget.setUnitEt(widget.getUnitEt());
        dbWidget.setDivisor(widget.getDivisor());
        dbWidget.setStatus(widget.getStatus());
        dbWidget.setPrecisionScale(widget.getPrecisionScale());
        dbWidget.setPeriods(widget.getPeriods());
        dbWidget.setMethodsLinkEt(widget.getMethodsLinkEt());
        dbWidget.setMethodsLinkEn(widget.getMethodsLinkEn());
        dbWidget.setStatisticianJobLinkEt(widget.getStatisticianJobLinkEt());
        dbWidget.setStatisticianJobLinkEn(widget.getStatisticianJobLinkEn());
        dbWidget.setTimePeriod(widget.getTimePeriod());
        dbWidget.setStartDate(widget.getStartDate());
        dbWidget.setEndDate(widget.getEndDate());
    }

    private WidgetAdminResponse saveOrUpdate(WidgetAdminResponse widget, Widget dbWidget) {
        Widget save = widgetRepository.save(dbWidget);
        widget.setId(save.getId());
        widgetDashboardService.updateDashboards(widget);
        statDataAdminService.updateStatDbs(widget);
        excelAdminService.updateExcels(widget);
        graphTypeAdminService.updateGraphTypes(widget);
        return widget;
    }

    private void fixupDates(WidgetAdminResponse widget) {
        TimePeriod timePeriod = widget.getTimePeriod();
        if (timePeriod != null) {
            if (timePeriod.isYear()) {
                if (widget.getStartDate() != null) {
                    widget.setStartDate(widget.getStartDate().with(firstDayOfYear()));
                }
                if (widget.getEndDate() != null) {
                    widget.setEndDate(widget.getEndDate().with(firstDayOfYear()));
                }
            } else if (timePeriod.isQuarter() || timePeriod.isMonth()) {
                if (widget.getStartDate() != null) {
                    widget.setStartDate(widget.getStartDate().with(firstDayOfMonth()));
                }
                if (widget.getEndDate() != null) {
                    widget.setEndDate(widget.getEndDate().with(firstDayOfMonth()));
                }
            }
        }
    }
}
