package ee.stat.dashboard.service.admin.dashboard;


import ee.stat.dashboard.model.classifier.Domain;
import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.widget.back.WidgetDomain;
import ee.stat.dashboard.repository.DashboardRegionRepository;
import ee.stat.dashboard.repository.DashboardRepository;
import ee.stat.dashboard.repository.DashboardWidgetRepository;
import ee.stat.dashboard.repository.DomainConnectionRepository;
import ee.stat.dashboard.repository.DomainRepository;
import ee.stat.dashboard.repository.RoleDashboardRepository;
import ee.stat.dashboard.repository.UserDashboardRepository;
import ee.stat.dashboard.repository.WidgetDomainRepository;
import ee.stat.dashboard.service.admin.widget.UserWidgetDeleteService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
@AllArgsConstructor
@Transactional
public class DashboardDeleteService {

    private final DashboardRepository dashboardRepository;
    private final DashboardWidgetRepository dashboardWidgetRepository;
    private final DashboardRegionRepository dashboardRegionRepository;
    private final UserWidgetDeleteService userWidgetDeleteService;
    private final WidgetDomainRepository widgetDomainRepository;
    private final DomainConnectionRepository domainConnectionRepository;
    private final DomainRepository domainRepository;
    private final RoleDashboardRepository roleDashboardRepository;
    private final UserDashboardRepository userDashboardRepository;

    public void delete(Dashboard dashboard) {
        dashboardRegionRepository.deleteByDashboard(dashboard.getId());
        userWidgetDeleteService.deleteUserWidgets(dashboard);
        userDashboardRepository.deleteByDashboard(dashboard.getId());

        widgetDomainRepository.deleteByDashboard(dashboard.getId());
        dashboardWidgetRepository.deleteByDashboard(dashboard.getId());
        roleDashboardRepository.deleteByDashboard(dashboard.getId());

        List<Domain> domains = domainRepository.findByDashboard(dashboard.getId());
        if (isNotEmpty(domains)) {
            domainConnectionRepository.deleteByToIdIn(domainIds(domains));
        }

        domainRepository.deleteByDashboard(dashboard.getId());

        dashboardRepository.deleteById(dashboard.getId());
    }

    public void deleteUserWidgets(List<WidgetDomain> widgetDomains) {
        List<Long> wdIds = widgetDomains.stream().map(WidgetDomain::getId).collect(toList());
        userWidgetDeleteService.deleteUserWidgets(wdIds);
        widgetDomainRepository.deleteByIdIn(wdIds);
    }

    private List<Long> domainIds(List<Domain> domains) {
        return domains.stream().map(Domain::getId).collect(toList());
    }
}
