package ee.stat.dashboard.service.admin.dashboard;

import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.admin.dashboard.dto.DashboardAdminResponse;
import lombok.AllArgsConstructor;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class DashboardCacheManager {

    private final CacheManager cacheManager;

    @Caching(evict = {
            @CacheEvict(value = "DashboardServiceCache_findAllByLevel", allEntries = true),
            @CacheEvict(value = "ElementServiceCache_findAllByClfCodeAndRoleOrderByName", allEntries = true),
    })
    public void save(DashboardAdminResponse dashboard) {
        //method updates the cache
    }

    @Caching(evict = {
            @CacheEvict(value = "DashboardServiceCache_findAllByLevel", allEntries = true),
            @CacheEvict(value = "DashboardServiceCache_findById", key = "#dashboard.id"),
            @CacheEvict(value = "ElementServiceCache_findAllDashboardRegions", key = "#dashboard.id"),
            @CacheEvict(value = "ElementServiceCache_findAllByClfCodeAndRoleOrderByName", allEntries = true)
    })
    public void update(DashboardAdminResponse dashboard) {
        updateDomainServiceCache(dashboard.getId(), cacheManager.getCache("DomainService_loadDomainsAndWidgets"));
    }

    @Caching(evict = {
            @CacheEvict(value = "DashboardServiceCache_findById", key = "#dashboardId"),
            @CacheEvict(value = "DashboardServiceCache_findAllByLevel", allEntries = true),
            @CacheEvict(value = "ElementServiceCache_findAllByClfCodeAndRoleOrderByName", allEntries = true),
    })
    public void delete(Long dashboardId) {
        //method updates the cache
    }

    public void updateDomainServiceCache(List<Long> dashboardIds) {
        Cache domainsAndWidgets = cacheManager.getCache("DomainService_loadDomainsAndWidgets");
        for (Long dashboardId : dashboardIds) {
            updateDomainServiceCache(dashboardId, domainsAndWidgets);
        }
    }

    private void updateDomainServiceCache(Long dashboardId, Cache cache) {
        if (cache != null) {
            for (Language value : Language.values()) {
                cache.evict(dashboardId.toString() + value.name());
            }
        }
    }
}
