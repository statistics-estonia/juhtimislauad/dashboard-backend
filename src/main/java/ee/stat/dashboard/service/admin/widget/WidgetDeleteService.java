package ee.stat.dashboard.service.admin.widget;

import ee.stat.dashboard.model.user.widget.UserGraphType;
import ee.stat.dashboard.model.widget.back.Filter;
import ee.stat.dashboard.model.widget.back.FilterValue;
import ee.stat.dashboard.model.widget.back.GraphType;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.back.excel.Excel;
import ee.stat.dashboard.model.widget.back.statdataapi.StatDataApi;
import ee.stat.dashboard.model.widget.back.statdatasql.StatDataSql;
import ee.stat.dashboard.model.widget.front.Diagram;
import ee.stat.dashboard.repository.DashboardWidgetRepository;
import ee.stat.dashboard.repository.DiagramDataRepository;
import ee.stat.dashboard.repository.DiagramRepository;
import ee.stat.dashboard.repository.ExcelDataRepository;
import ee.stat.dashboard.repository.ExcelRepository;
import ee.stat.dashboard.repository.FilterRepository;
import ee.stat.dashboard.repository.FilterValueCustomForFilterRepository;
import ee.stat.dashboard.repository.FilterValueCustomRepository;
import ee.stat.dashboard.repository.FilterValueRepository;
import ee.stat.dashboard.repository.GraphTypeRepository;
import ee.stat.dashboard.repository.StatDataApiDataRepository;
import ee.stat.dashboard.repository.StatDataApiRepository;
import ee.stat.dashboard.repository.StatDataSqlDataRepository;
import ee.stat.dashboard.repository.StatDataSqlRepository;
import ee.stat.dashboard.repository.UserGraphTypeRepository;
import ee.stat.dashboard.repository.UserLegendRepository;
import ee.stat.dashboard.repository.WidgetDomainRepository;
import ee.stat.dashboard.repository.WidgetRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
@AllArgsConstructor
@Transactional
public class WidgetDeleteService {

    private final FilterValueRepository filterValueRepository;
    private final FilterRepository filterRepository;
    private final GraphTypeRepository graphTypeRepository;
    private final WidgetRepository widgetRepository;
    private final WidgetDomainRepository widgetDomainRepository;
    private final StatDataApiRepository statDataApiRepository;
    private final StatDataApiDataRepository statDataApiDataRepository;
    private final StatDataSqlRepository statDataSqlRepository;
    private final StatDataSqlDataRepository statDataSqlDataRepository;
    private final ExcelRepository excelRepository;
    private final ExcelDataRepository excelDataRepository;
    private final DiagramRepository diagramRepository;
    private final DiagramDataRepository diagramDataRepository;
    private final UserGraphTypeRepository userGraphTypeRepository;
    private final UserLegendRepository userLegendRepository;
    private final DashboardWidgetRepository dashboardWidgetRepository;
    private final FilterValueCustomRepository filterValueCustomRepository;
    private final FilterValueCustomForFilterRepository filterValueCustomForFilterRepository;
    private final UserWidgetDeleteService userWidgetDeleteService;

    public void delete(Widget widget) {
        List<Excel> excels = excelRepository.findByWidget(widget.getId());
        if (isNotEmpty(excels)) {
            List<Long> excelsIds = excels.stream().map(Excel::getId).collect(toList());
            excelDataRepository.deleteByExcelIn(excelsIds);
            excelRepository.deleteByWidget(widget.getId());
        }

        List<StatDataApi> statDataApis = statDataApiRepository.findByWidget(widget.getId());
        if (isNotEmpty(statDataApis)) {
            List<Long> statDataApiIds = statDataApis.stream().map(StatDataApi::getId).collect(toList());
            statDataApiDataRepository.deleteByStatDataApiIn(statDataApiIds);
            statDataApiRepository.deleteByWidget(widget.getId());
        }

        List<StatDataSql> statDataSqls = statDataSqlRepository.findByWidget(widget.getId());
        if (isNotEmpty(statDataSqls)) {
            List<Long> statDataSqlIds = statDataSqls.stream().map(StatDataSql::getId).collect(toList());
            statDataSqlDataRepository.deleteByStatDataSqlIn(statDataSqlIds);
            statDataSqlRepository.deleteByWidget(widget.getId());
        }

        List<Long> graphTypeIds = graphTypeRepository.findAllByWidgetOrderByIdAsc(widget.getId()).stream()
                .map(GraphType::getId)
                .collect(toList());
        List<Filter> filters = graphTypeIds.isEmpty() ? new ArrayList<>() : filterRepository.findAllByGraphTypeInOrderByIdAsc(graphTypeIds);

        if (isNotEmpty(filters)) {
            filterValueCustomForFilterRepository.deleteByFilterIn(filters.stream().map(Filter::getId).collect(toList()));
        }
        filterValueCustomRepository.deleteAllByWidget(widget.getId());
        deleteDiagrams(widget.getId());

        userWidgetDeleteService.deleteUserWidgets(widget);
        dashboardWidgetRepository.deleteByWidget(widget.getId());
        deleteGraphTypes(graphTypeIds, filters);

        widgetDomainRepository.deleteByWidget(widget.getId());
        widgetRepository.deleteById(widget.getId());
    }

    public void deleteDiagrams(Long widgetId) {
        List<Diagram> diagrams = diagramRepository.findByWidget(widgetId);
        if (isNotEmpty(diagrams)) {
            List<Long> diagramIds = diagrams.stream().map(Diagram::getId).collect(toList());
            diagramDataRepository.deleteByDiagramIn(diagramIds);
            diagramRepository.deleteByWidget(widgetId);
        }
    }

    public void deleteGraphTypes(List<Long> graphTypeIds, List<Filter> filters) {
        deleteGraphTypesInner(graphTypeIds, filters);
    }

    public void deleteGraphTypes(List<GraphType> existingGraphTypes) {
        List<Long> graphTypeIds = existingGraphTypes.stream().map(GraphType::getId).collect(toList());
        List<Filter> filters = graphTypeIds.isEmpty() ? new ArrayList<>() : filterRepository.findAllByGraphTypeInOrderByIdAsc(graphTypeIds);
        deleteGraphTypesInner(graphTypeIds, filters);
    }

    public void deleteFilters(List<Filter> filters) {
        List<Long> filterIds = filters.stream().map(Filter::getId).collect(toList());
        List<FilterValue> filterValues = filters.isEmpty() ? new ArrayList<>() : filterValueRepository.findAllByFilterIn(filterIds);

        deleteFilterValues(filterValues);
        userWidgetDeleteService.deleteUserFiltersByFilter(filterIds);

        if (isNotEmpty(filterIds)) {
            filterRepository.deleteByIdIn(filterIds);
        }
    }

    public void deleteFilterValues(List<FilterValue> filterValues) {
        List<Long> filterValuesIds = filterValues.stream().map(FilterValue::getId).collect(toList());
        userWidgetDeleteService.deleteUserFilterValuesByFilterValue(filterValuesIds);

        if (isNotEmpty(filterValuesIds)) {
            filterValueRepository.deleteByIdIn(filterValuesIds);
        }
    }

    private void deleteGraphTypesInner(List<Long> graphTypeIds, List<Filter> filters) {
        deleteFilters(filters);

        if (isNotEmpty(graphTypeIds)) {
            List<UserGraphType> userGraphTypes = userGraphTypeRepository.findAllByGraphTypeIn(graphTypeIds);
            if (isNotEmpty(userGraphTypes)) {
                List<Long> userGraphTypeIds = userGraphTypes.stream().map(UserGraphType::getId).collect(toList());
                userLegendRepository.deleteByUserGraphTypeIn(userGraphTypeIds);
                userGraphTypeRepository.deleteByIdIn(userGraphTypeIds);
            }
            diagramDataRepository.deleteByGraphTypeIn(graphTypeIds);
            statDataApiDataRepository.deleteByGraphTypeIn(graphTypeIds);
            statDataSqlDataRepository.deleteByGraphTypeIn(graphTypeIds);
            graphTypeRepository.deleteByIdIn(graphTypeIds);
        }
    }
}
