package ee.stat.dashboard.service.admin.widget;

import ee.stat.dashboard.model.widget.back.WidgetDomain;
import ee.stat.dashboard.repository.WidgetDomainRepository;
import ee.stat.dashboard.service.admin.dashboard.DashboardCacheManager;
import ee.stat.dashboard.service.admin.dashboard.DashboardDeleteService;
import ee.stat.dashboard.service.admin.dashboard.dto.DashboardAdminResponse;
import ee.stat.dashboard.service.admin.domain.dto.DomainAdminResponse;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ee.stat.dashboard.util.ErrorCode.DASHBOARD_EXISTS_WITH_ANOTHER_CONNECTION;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
@AllArgsConstructor
@Transactional
public class WidgetDashboardService {

    private final WidgetDomainRepository widgetDomainRepository;
    private final DashboardDeleteService dashboardDeleteService;
    private final DashboardCacheManager dashboardCacheManager;

    public void updateDashboards(WidgetAdminResponse widget) {
        List<WidgetDomain> existingWidgetDomains = widgetDomainRepository.findAllByWidget(widget.getId());
        List<WidgetDomain> newWidgetDomains = new ArrayList<>();
        if (isNotEmpty(widget.getElements())) {
            Map<Long, String> uniqueDashboards = new HashMap<>();
            for (DomainAdminResponse element : widget.getElements()) {
                DashboardAdminResponse dashboard = element.getDashboard();

                String domainName = uniqueDashboards.get(dashboard.getId());
                if (domainName != null) {
                    throw dashboardExistsWithAnotherConnection(element, dashboard, domainName);
                }
                uniqueDashboards.put(dashboard.getId(), element.getNameEt());
                WidgetDomain newWidgetDomain = existingWidgetDomains.stream()
                        .filter(wd -> wd.getDomain().equals(element.getId()))
                        .filter(wd -> wd.getWidget().equals(widget.getId()))
                        .findAny()
                        .orElseGet(() -> new WidgetDomain(widget.getId(), element.getId(), dashboard.getId()));

                newWidgetDomains.add(newWidgetDomain);
            }
        }

        widgetDomainRepository.saveAll(newWidgetDomains);

        existingWidgetDomains.removeAll(newWidgetDomains);

        dashboardCacheManager.updateDomainServiceCache(dashboardIdsForCache(existingWidgetDomains, newWidgetDomains));
        dashboardDeleteService.deleteUserWidgets(existingWidgetDomains);
    }

    private List<Long> dashboardIdsForCache(List<WidgetDomain> existingWidgetDomains, List<WidgetDomain> newWidgetDomains) {
        List<Long> oldDashboardIds = existingWidgetDomains.stream().map(WidgetDomain::getDashboard).distinct().collect(toList());
        List<Long> newDashboardIds = newWidgetDomains.stream().map(WidgetDomain::getDashboard).distinct().collect(toList());
        oldDashboardIds.addAll(newDashboardIds);
        return oldDashboardIds;
    }

    private StatBadRequestException dashboardExistsWithAnotherConnection(DomainAdminResponse element, DashboardAdminResponse dashboard, String domainName) {
        return new StatBadRequestException(DASHBOARD_EXISTS_WITH_ANOTHER_CONNECTION, format("dashboard %s already exists with another connection c1: %s c2: %s",
                dashboard.getNameEt(), domainName, element.getNameEt()));
    }
}
