package ee.stat.dashboard.service.admin.process.dto;

import ee.stat.dashboard.model.process.enums.ProcessLogStatus;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;


@Getter
@Setter
public class ProcessLogDto {

    private Long id;
    private String process;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private ProcessLogStatus status;
    private Integer analysed;
    private Integer unchanged;
    private Integer changed;
    private Integer failed;
}
