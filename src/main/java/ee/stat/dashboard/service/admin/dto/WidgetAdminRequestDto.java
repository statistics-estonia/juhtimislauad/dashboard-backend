package ee.stat.dashboard.service.admin.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

import static ee.stat.dashboard.service.admin.dto.DataSource.DATA_API;
import static ee.stat.dashboard.service.admin.dto.DataSource.DATA_SQL;
import static ee.stat.dashboard.service.admin.dto.DataSource.EXCEL;
import static ee.stat.dashboard.service.admin.dto.DataSource.NONE;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class WidgetAdminRequestDto {

    private Integer page = 0;
    private Integer size = 10;
    private String queryForCube;
    private String queryForDashboard;
    private String queryForWidget;
    private List<DataSource> sources;

    public boolean hasQuery() {
        return isNotBlank(queryForCube) || isNotBlank(queryForDashboard) || isNotBlank(queryForWidget);
    }

    public boolean hasQueryForCube() {
        return isNotBlank(queryForCube);
    }

    public boolean hasQueryForDashboard() {
        return isNotBlank(queryForDashboard);
    }

    public boolean hasQueryForWidget() {
        return isNotBlank(queryForWidget);
    }

    public boolean hasExcelSource() {
        return hasSource(EXCEL);
    }

    public boolean hasNoneSource() {
        return hasSource(NONE);
    }

    public boolean hasDataApiSource() {
        return hasSource(DATA_API);
    }

    public boolean hasDataSqlSource() {
        return hasSource(DATA_SQL);
    }

    private boolean hasSource(DataSource source) {
        return isNotEmpty(sources) && sources.contains(source);
    }

    public boolean hasStatSource() {
        return isNotEmpty(sources)
                && (sources.contains(DATA_API) || sources.contains(DATA_SQL));
    }


}
