package ee.stat.dashboard.service.admin.widget;

import ee.stat.dashboard.model.widget.back.Filter;
import ee.stat.dashboard.model.widget.back.FilterValue;
import ee.stat.dashboard.model.widget.back.FilterValueCustom;
import ee.stat.dashboard.model.widget.back.FilterValueCustomForFilter;
import ee.stat.dashboard.model.widget.back.GraphType;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.front.Diagram;
import ee.stat.dashboard.model.widget.front.DiagramData;
import ee.stat.dashboard.model.widget.front.Graph;
import ee.stat.dashboard.model.widget.front.enums.DiagramStatus;
import ee.stat.dashboard.repository.DiagramDataRepository;
import ee.stat.dashboard.repository.DiagramRepository;
import ee.stat.dashboard.repository.FilterRepository;
import ee.stat.dashboard.repository.FilterValueCustomForFilterRepository;
import ee.stat.dashboard.repository.FilterValueRepository;
import ee.stat.dashboard.repository.StatDataApiDataRepository;
import ee.stat.dashboard.repository.StatDataIdAndRequest;
import ee.stat.dashboard.repository.StatDataSqlDataRepository;
import ee.stat.dashboard.service.admin.widget.dto.FilterAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.FilterValueAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.GraphTypeAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.StatDataApiDataAdminDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static ee.stat.dashboard.model.widget.back.enums.Language.values;
import static ee.stat.dashboard.util.StatListUtil.first;
import static ee.stat.dashboard.util.StatListUtil.firstOrNull;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.concat;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.BooleanUtils.isTrue;

@Service
@AllArgsConstructor
public class GraphTypeAdminConverter {

    private final FilterRepository filterRepository;
    private final StatDataApiDataRepository statDataApiDataRepository;
    private final StatDataSqlDataRepository statDataSqlDataRepository;
    private final FilterValueRepository filterValueRepository;
    private final FilterValueCustomForFilterRepository filterValueCustomForFilterRepository;
    private final DiagramDataRepository diagramDataRepository;
    private final DiagramRepository diagramRepository;

    public GraphTypeAdminDto mapGraphTypeData(GraphType graphType, Widget widget) {
        GraphTypeAdminDto graphTypeDto = mapGraphType(graphType);
        List<StatDataIdAndRequest> statDataApiData = statDataApiDataRepository.findRequestByGraphType(graphType.getId());
        if (isNotEmpty(statDataApiData)) {
            StatDataIdAndRequest first = first(statDataApiData);
            graphTypeDto.setStatDataApiData(new StatDataApiDataAdminDto(first.getId(), first.getRequest()));
        }
        List<StatDataIdAndRequest> statDataSqlData = statDataSqlDataRepository.findRequestByGraphType(graphType.getId());
        if (isNotEmpty(statDataSqlData)) {
            StatDataIdAndRequest first = first(statDataSqlData);
            graphTypeDto.setStatDataSqlData(new StatDataApiDataAdminDto(first.getId(), first.getRequest()));
        }
        List<Filter> filters = filterRepository.findAllByGraphTypeOrderByOrderNrAscIdAsc(graphType.getId());
        if (isNotEmpty(filters)) {
            graphTypeDto.setFilters(filters.stream().map(filter -> mapFilterData(filter, widget, graphType)).collect(toList()));
        }
        return graphTypeDto;
    }

    private GraphTypeAdminDto mapGraphType(GraphType graphType) {
        GraphTypeAdminDto graphTypeDto = new GraphTypeAdminDto();
        graphTypeDto.setId(graphType.getId());
        graphTypeDto.setType(graphType.getType());
        if (graphType.isDefaultType()) {
            graphTypeDto.setDefaultType(true);
        }
        graphTypeDto.setMapRule(graphType.getMapRule());
        graphTypeDto.setVerticalRule(graphType.getVerticalRule());
        return graphTypeDto;
    }

    private FilterAdminDto mapFilterData(Filter filter, Widget widget, GraphType graphType) {
        FilterAdminDto filterDto = mapFilter(filter);
        if (filterDto.getType().isAxis() && isTrue(filterDto.getTime())) {
            List<Diagram> diagrams = diagramRepository.findByWidgetAndStatusOrderByCreatedAtDesc(widget.getId(), DiagramStatus.APPROVED);
            Diagram diagram = firstOrNull(diagrams);
            if (diagram != null) {
                Optional<DiagramData> data = diagramDataRepository.findByDiagramAndGraphType(diagram.getId(), graphType.getId());
                if (data.isPresent()) {
                    for (Language lang : values()) {
                        Graph graph = data.get().getGraph(lang);
                        if (graph != null) {
                            filterDto.setPeriod(lang, graph.getPeriod());
                        }
                    }
                }
            }
        } else {
            List<FilterValue> values = filterValueRepository.findAllByFilterOrderByOrderNrAscIdAsc(filterDto.getId());
            List<FilterValueCustom> customValues = filterValueCustomForFilterRepository.findAllByFilterId(filter.getId())
                    .stream().map(FilterValueCustomForFilter::getFilterValueCustom).collect(toList());
            setValuesToDto(values, customValues, filterDto);
        }
        return filterDto;
    }

    private void setValuesToDto(List<FilterValue> values, List<FilterValueCustom> customValues, FilterAdminDto filterDto) {
        if (isNotEmpty(values) || isNotEmpty(customValues)) {
            List<FilterValueAdminDto> combinedValues = concat(
                    values.stream().map(this::mapFilterValue),
                    customValues.stream().map(this::mapFilterValueCustom)
            ).collect(toList());

            filterDto.setValues(combinedValues);
        }
    }

    private FilterAdminDto mapFilter(Filter filter) {
        FilterAdminDto filterDto = new FilterAdminDto();
        filterDto.setId(filter.getId());
        filterDto.setType(filter.getType());
        filterDto.setAxisOrder(filter.getAxisOrder());
        filterDto.setNameEt(filter.getNameEt());
        filterDto.setDisplayNameEt(filter.getDisplayNameEt());
        filterDto.setNameEn(filter.getNameEn());
        filterDto.setDisplayNameEn(filter.getDisplayNameEn());
        filterDto.setOrderNr(filter.getOrderNr());
        filterDto.setNumberOfValues(filter.getNumberOfValues());
        if (filter.isTime()) {
            filterDto.setTime(true);
        }
        if (filter.isRegion()) {
            filterDto.setRegion(true);
        }
        filterDto.setTimePart(filter.getTimePart());
        filterDto.setAxisOrder(filter.getAxisOrder());
        return filterDto;
    }

    private FilterValueAdminDto mapFilterValue(FilterValue filterValue) {
        FilterValueAdminDto dto = new FilterValueAdminDto();
        dto.setId(filterValue.getId());
        dto.setValueEn(filterValue.getValueEn());
        dto.setValueEt(filterValue.getValueEt());
        dto.setDisplayValueEn(filterValue.getDisplayValueEn());
        dto.setDisplayValueEt(filterValue.getDisplayValueEt());
        if (filterValue.isSelected()) {
            dto.setSelected(true);
        }
        dto.setOrderNr(filterValue.getOrderNr());
        dto.setCustom(false);
        return dto;
    }

    private FilterValueAdminDto mapFilterValueCustom(FilterValueCustom filterValueCustom) {
        FilterValueAdminDto dto = new FilterValueAdminDto();
        dto.setId(filterValueCustom.getId());
        dto.setValueEn(filterValueCustom.getDisplayNameEn());
        dto.setValueEt(filterValueCustom.getDisplayNameEt());
        dto.setCustom(true);
        if (filterValueCustom.isSelected()) {
            dto.setSelected(true);
        }
        return dto;
    }

}
