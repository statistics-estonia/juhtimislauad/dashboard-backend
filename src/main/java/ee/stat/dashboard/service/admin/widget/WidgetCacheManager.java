package ee.stat.dashboard.service.admin.widget;

import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.back.WidgetDomain;
import ee.stat.dashboard.repository.WidgetDomainRepository;
import ee.stat.dashboard.service.admin.dashboard.DashboardCacheManager;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Slf4j
@Component
@AllArgsConstructor
public class WidgetCacheManager {

    private CacheManager cacheManager;
    private DashboardCacheManager dashboardCacheManager;
    private WidgetDomainRepository widgetDomainRepository;

    @Caching(evict = {
            @CacheEvict(value = "DiagramServiceCache_findDiagram", key = "#widget.id"),
            @CacheEvict(value = "DomainServiceCache_findAllByWidget", key = "#widget.id"),
            @CacheEvict(value = "ExcelCache_findByWidget", key = "#widget.id"),
            @CacheEvict(value = "GraphTypeServiceCache_findAllByWidget", key = "#widget.id"),
            @CacheEvict(value = "StatDataApiCache_findByWidget", key = "#widget.id"),
            @CacheEvict(value = "StatDataSqlCache_findByWidget", key = "#widget.id"),
            @CacheEvict(value = "WidgetServiceCache_findById", key = "#widget.id"),
    })
    public void update(WidgetAdminResponse widget) {
        //method updates the cache
    }

    public void updateGraphType(List<Long> graphTypeIds) {
        if (CollectionUtils.isNotEmpty(graphTypeIds)) {
            Cache filterCache = cacheManager.getCache("FilterService_getDbFilters");
            if (filterCache != null) {
                graphTypeIds.forEach(filterCache::evict);
            }
            Cache diagramDataCache = cacheManager.getCache("DiagramServiceCache_findDiagramGraph");
            if (diagramDataCache != null) {
                graphTypeIds.forEach(diagramDataCache::evict);
            }
        }
    }

    @Caching(evict = {
            @CacheEvict(value = "DiagramServiceCache_findDiagram", key = "#widget.id"),
            @CacheEvict(value = "DomainServiceCache_findAllByWidget", key = "#widget.id"),
            @CacheEvict(value = "ExcelCache_findByWidget", key = "#widget.id"),
            @CacheEvict(value = "GraphTypeServiceCache_findAllByWidget", key = "#widget.id"),
            @CacheEvict(value = "StatDataApiCache_findByWidget", key = "#widget.id"),
            @CacheEvict(value = "StatDataSqlCache_findByWidget", key = "#widget.id"),
            @CacheEvict(value = "WidgetServiceCache_findById", key = "#widget.id"),
    })
    public void delete(Widget widget) {
        List<WidgetDomain> widgetDomains = widgetDomainRepository.findAllByWidget(widget.getId());
        List<Long> dashboardIds = widgetDomains.stream().map(WidgetDomain::getDashboard).distinct().collect(toList());
        dashboardCacheManager.updateDomainServiceCache(dashboardIds);
    }
}
