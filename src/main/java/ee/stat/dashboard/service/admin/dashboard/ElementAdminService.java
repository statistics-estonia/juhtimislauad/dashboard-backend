package ee.stat.dashboard.service.admin.dashboard;

import ee.stat.dashboard.model.classifier.Classifier;
import ee.stat.dashboard.model.classifier.Element;
import ee.stat.dashboard.repository.ElementRepository;
import ee.stat.dashboard.service.admin.dashboard.dto.ClassifierAdminResponse;
import ee.stat.dashboard.service.admin.dashboard.dto.ElementAdminResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Transactional
public class ElementAdminService {

    private final ElementRepository elementRepository;

    public List<ElementAdminResponse> findAllDashboardRegions(Long dashboardId) {
        return elementRepository.findAllDashboardRegions(dashboardId).stream()
                .map(this::mapMainData)
                .collect(toList());
    }

    public ElementAdminResponse mapMainData(Element element){
        ElementAdminResponse dto = new ElementAdminResponse();
        dto.setId(element.getId());
        dto.setCode(element.getCode());
        dto.setLevel(element.getLevel());
        dto.setNameEn(element.getNameEn());
        dto.setNameEt(element.getNameEt());
        return dto;
    }

    public ClassifierAdminResponse mapMainData(Classifier classifier){
        ClassifierAdminResponse dto = new ClassifierAdminResponse();
        dto.setId(classifier.getId());
        dto.setCode(classifier.getCode());
        dto.setNameEn(classifier.getNameEn());
        dto.setNameEt(classifier.getNameEt());
        dto.setDescriptionEn(classifier.getDescriptionEn());
        dto.setDescriptionEt(classifier.getDescriptionEt());
        dto.setOrderNr(classifier.getOrderNr());
        return dto;
    }
}
