package ee.stat.dashboard.service.admin.dashboard.dto;

import ee.stat.dashboard.model.dashboard.DashboardStatus;
import ee.stat.dashboard.model.dashboard.DashboardType;
import ee.stat.dashboard.service.admin.domain.dto.DomainAdminResponse;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class DashboardAdminResponse {

    private Long id;
    private String code;
    private String nameEt;
    private String nameEn;
    private DashboardStatus status;
    private DashboardType type;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    private List<DashboardAdminResponse> subDashboards;
    private List<ElementAdminResponse> regions;
    private List<DomainAdminResponse> elements;
    private ClassifierAdminResponse mainRole;
    private ElementAdminResponse subRole;
}
