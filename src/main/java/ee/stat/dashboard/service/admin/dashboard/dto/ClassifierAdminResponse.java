package ee.stat.dashboard.service.admin.dashboard.dto;

import ee.stat.dashboard.model.classifier.ClassifierCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClassifierAdminResponse {

    private Long id;
    private ClassifierCode code;
    private String nameEt;
    private String nameEn;
    private String descriptionEt;
    private String descriptionEn;
    private Integer orderNr;

}
