package ee.stat.dashboard.service.admin.widget;

import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.model.widget.back.statdataapi.StatDataApi;
import ee.stat.dashboard.model.widget.back.statdatasql.StatDataSql;
import ee.stat.dashboard.repository.StatDataApiDataRepository;
import ee.stat.dashboard.repository.StatDataApiRepository;
import ee.stat.dashboard.repository.StatDataSqlDataRepository;
import ee.stat.dashboard.repository.StatDataSqlRepository;
import ee.stat.dashboard.service.admin.widget.dto.StatDataAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import ee.stat.dashboard.service.statdata.StatMetaFetchException;
import ee.stat.dashboard.service.statdata.StatPublishedAtManager;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.Objects;

import static ee.stat.dashboard.model.widget.back.StatDataType.DATA_API;
import static ee.stat.dashboard.util.StatListUtil.firstOrNull;
import static java.lang.String.format;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Service
@AllArgsConstructor
@Transactional
public class StatDataAdminService {

    private final StatDataApiRepository statDataApiRepository;
    private final StatDataApiDataRepository statDataApiDataRepository;
    private final StatDataSqlRepository statDataSqlRepository;
    private final StatDataSqlDataRepository statDataSqlDataRepository;
    private final StatPublishedAtManager statPublishedAtManager;

    public void updateStatDbs(WidgetAdminResponse widget) {
        StatDataApi existingStatDataApi = firstOrNull(statDataApiRepository.findByWidget(widget.getId()));
        StatDataSql existingStatDataSql = firstOrNull(statDataSqlRepository.findByWidget(widget.getId()));
        StatDataAdminDto newStatDb = widget.getStatDb();
        if (newStatDb != null && newStatDb.getStatDataType() == null) {
            //todo2 consider removing this force setup, as this should be passed by frontend
            newStatDb.setStatDataType(DATA_API);
        }

        if (noExisting(existingStatDataApi, existingStatDataSql) && newStatDb == null) {  //both null
            //do nothing
        } else if (hasExisting(existingStatDataApi, existingStatDataSql) && newStatDb == null) { //existing not-n, new null
            deleteOld(existingStatDataApi, existingStatDataSql);
            widget.setStatDb(null);
        } else if (newStatDb == null) {
            //impossible case but removes NPE alerts from saveNew
            throw new ResponseStatusException(BAD_REQUEST);
        } else if (noExisting(existingStatDataApi, existingStatDataSql)) { //existing null, new not-n
            widget.getStatDb().setId(saveNew(widget, newStatDb));
            widget.getStatDb().setPublishedAtChanged(true);
        } else if (sameCube(existingStatDataApi, existingStatDataSql, newStatDb)) { //same cube
            sameCubeFlow(widget, existingStatDataApi, existingStatDataSql, newStatDb);
        } else {
            deleteOld(existingStatDataApi, existingStatDataSql);
            widget.getStatDb().setId(saveNew(widget, newStatDb));
            widget.getStatDb().setPublishedAtChanged(true);
        }
    }

    private void sameCubeFlow(WidgetAdminResponse widget, StatDataApi existingStatDataApi, StatDataSql existingStatDataSql, StatDataAdminDto newStatDb) {
        if (existingStatDataApi != null) {
            widget.getStatDb().setId(existingStatDataApi.getId());
            LocalDateTime newPublishedAt = getNewPublishedAt(existingStatDataApi.getCube(), newStatDb.getStatDataType());
            widget.getStatDb().setPublishedAtChanged(!Objects.equals(existingStatDataApi.getPublishedAt(), newPublishedAt));
        } else {
            widget.getStatDb().setId(existingStatDataSql.getId());
            LocalDateTime newPublishedAt = getNewPublishedAt(existingStatDataSql.getCube(), newStatDb.getStatDataType());
            widget.getStatDb().setPublishedAtChanged(!Objects.equals(existingStatDataSql.getPublishedAt(), newPublishedAt));
        }
    }

    private boolean sameCube(StatDataApi existingStatDataApi, StatDataSql existingStatDataSql, StatDataAdminDto newStatDb) {
        if (newStatDb.getStatDataType().isDataApi()) {
            return existingStatDataApi != null && existingStatDataApi.getCube().equals(newStatDb.getCube());
        } else {
            return existingStatDataSql != null && existingStatDataSql.getCube().equals(newStatDb.getCube());
        }
    }

    private boolean hasExisting(StatDataApi existingStatDataApi, StatDataSql existingStatDataSql) {
        return existingStatDataApi != null || existingStatDataSql != null;
    }

    private boolean noExisting(StatDataApi existingStatDataApi, StatDataSql existingStatDataSql) {
        return existingStatDataApi == null && existingStatDataSql == null;
    }

    private Long saveNew(WidgetAdminResponse widget, StatDataAdminDto newStatDb) {
        LocalDateTime newPublishedAt = getNewPublishedAt(newStatDb.getCube(), newStatDb.getStatDataType());
        if (newStatDb.getStatDataType().isDataApi()) {
            StatDataApi statDb = new StatDataApi();
            statDb.setWidget(widget.getId());
            statDb.setCube(newStatDb.getCube().toUpperCase());
            statDb.setCreatedAt(LocalDateTime.now());
            statDb.setPublishedAt(newPublishedAt);
            statDataApiRepository.save(statDb);
            return statDb.getId();
        } else {
            StatDataSql statDb = new StatDataSql();
            statDb.setWidget(widget.getId());
            statDb.setCube(newStatDb.getCube().toUpperCase());
            statDb.setCreatedAt(LocalDateTime.now());
            statDb.setPublishedAt(newPublishedAt);
            statDataSqlRepository.save(statDb);
            return statDb.getId();
        }
    }

    private void deleteOld(StatDataApi existingStatDataApi, StatDataSql existingStatDataSql) {
        if (existingStatDataApi != null) {
            deleteOld(existingStatDataApi);
        }
        if (existingStatDataSql != null) {
            deleteOld(existingStatDataSql);
        }
    }

    private void deleteOld(StatDataApi existingStatDb) {
        statDataApiDataRepository.deleteByStatDataApi(existingStatDb.getId());
        statDataApiRepository.delete(existingStatDb);
    }

    private void deleteOld(StatDataSql existingStatDb) {
        statDataSqlDataRepository.deleteByStatDataSql(existingStatDb.getId());
        statDataSqlRepository.delete(existingStatDb);
    }

    private LocalDateTime getNewPublishedAt(String existingCube, StatDataType type) {
        try {
            return statPublishedAtManager.publishedAt(existingCube, type);
        } catch (StatMetaFetchException e) {
            throw new ResponseStatusException(BAD_REQUEST, format("problem with the cube %s", existingCube));
        }
    }
}
