package ee.stat.dashboard.service.admin.widget.dto;

import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.model.widget.back.enums.WidgetStatus;
import ee.stat.dashboard.service.admin.dashboard.dto.DashboardAdminResponse;
import ee.stat.dashboard.service.admin.domain.dto.DomainAdminResponse;
import ee.stat.dashboard.service.admin.dto.DataSource;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlDimension;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class WidgetAdminResponse {

    private Long id;
    private String code;
    private WidgetStatus status;
    private TimePeriod timePeriod;
    private Integer precisionScale;
    private String shortnameEt;
    private String shortnameEn;
    private String nameEt;
    private String nameEn;
    private String descriptionEt;
    private String descriptionEn;
    private String noteEt;
    private String noteEn;
    private String unitEt;
    private String unitEn;
    private String sourceEt;
    private String sourceEn;
    private BigDecimal divisor;
    private Integer periods;
    private LocalDate startDate;
    private LocalDate endDate;
    private LocalDateTime dataUpdatedAt;
    private String cube;
    private String methodsLinkEt;
    private String methodsLinkEn;
    private String statisticianJobLinkEt;
    private String statisticianJobLinkEn;
    private StatDataAdminDto statDb;
    private ExcelAdminDto excel;
    private Boolean calculationProblem;
    private String calculationProblemMessage;
    private Boolean doAnyCustomFilterValuesExist;
    private Boolean areAnyCustomFilterValuesUsed;

    private List<DashboardAdminResponse> dashboards;
    private List<DomainAdminResponse> elements;
    private List<GraphTypeAdminDto> graphTypes;
    private List<XmlDimension> dimensions;
    private List<DataSource> sources;
}
