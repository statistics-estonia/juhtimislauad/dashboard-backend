package ee.stat.dashboard.service.admin.process;

import ee.stat.dashboard.model.process.StatLog;
import ee.stat.dashboard.model.process.enums.LogStatus;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.repository.StatLogRepository;
import ee.stat.dashboard.service.admin.dto.StatPage;
import ee.stat.dashboard.service.admin.process.dto.StatLogDto;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import ee.stat.dashboard.service.widget.widget.WidgetServiceCache;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static java.util.stream.Collectors.toList;
import static org.springframework.data.domain.Sort.Direction.DESC;

@Service
@AllArgsConstructor
@Transactional(readOnly = true)
public class StatLogQueryService {

    private final StatLogRepository statLogRepository;
    private final WidgetServiceCache widgetServiceCache;
    private final ProcessLogQueryService processLogQueryService;

    public StatPage<StatLogDto> findAll(Integer page, Integer size, Long processLogId, LogStatus logStatus) {
        processLogQueryService.findById(processLogId);
        PageRequest of = PageRequest.of(page, size, Sort.by(DESC, "id"));
        Page<StatLog> all = findAll(processLogId, logStatus, of);
        StatPage<StatLogDto> statPage = new StatPage<>();
        statPage.setPage(page);
        statPage.setSize(size);
        statPage.setTotalPages(all.getTotalPages());
        statPage.setTotalElements(all.getTotalElements());
        statPage.setContent(all.getContent().stream().map(this::convert).collect(toList()));
        return statPage;
    }

    private StatLogDto convert(StatLog statLog) {
        StatLogDto dto = new StatLogDto();
        dto.setId(statLog.getId());
        dto.setProcessLog(statLog.getProcessLog());
        dto.setStatus(statLog.getStatus());
        dto.setTime(statLog.getTime());
        dto.setMessage(statLog.getMessage());
        dto.setError(statLog.getError());
        dto.setCube(statLog.getCube());
        if (statLog.getWidget() != null) {
            widgetServiceCache.findById(statLog.getWidget())
                    .ifPresent(widget -> dto.setWidget(convertName(widget)));
        }
        if (dto.getWidget() == null) {
            dto.setWidget(new WidgetAdminResponse());
        }
        return dto;
    }

    private WidgetAdminResponse convertName(Widget widget) {
        WidgetAdminResponse response = new WidgetAdminResponse();
        response.setId(widget.getId());
        response.setNameEt(widget.getNameEt());
        response.setShortnameEt(widget.getShortnameEt());
        return response;
    }

    private Page<StatLog> findAll(Long processLogId, LogStatus logStatus, PageRequest of) {
        if (logStatus != null) {
            return statLogRepository.findAllByProcessLogAndStatus(processLogId, logStatus, of);
        }
        return statLogRepository.findAllByProcessLog(processLogId, of);
    }
}
