package ee.stat.dashboard.service.admin;

import ee.stat.dashboard.model.classifier.Domain;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.repository.DomainRepository;
import ee.stat.dashboard.repository.WidgetRepository;
import ee.stat.dashboard.service.admin.domain.DomainAdminConverter;
import ee.stat.dashboard.service.admin.domain.dto.DomainAdminResponse;
import ee.stat.dashboard.service.admin.widget.WidgetAdminConverter;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import static ee.stat.dashboard.model.widget.back.enums.Language.ET;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
@AllArgsConstructor
public class DomainAdminService {

    private final DomainRepository domainRepository;
    private final WidgetRepository widgetRepository;
    private final DomainAdminConverter domainAdminConverter;
    private final WidgetAdminConverter widgetAdminConverter;

    public List<DomainAdminResponse> loadDomainsAndWidgets(Long dashboardId) {
        return domainRepository.findByDashboardAndLevelOrderByOrderNr(dashboardId, 0).stream()
                .map(this::mapDomainWithChildrenAndWidgets)
                .collect(toList());
    }

    public List<DomainAdminResponse> loadDomains(Long dashboardId) {
        return domainRepository.findByDashboardOrderByOrderNr(dashboardId).stream()
                .map(domainAdminConverter::mapMainData)
                .collect(toList());
    }

    private DomainAdminResponse mapDomainWithChildrenAndWidgets(Domain topDomain) {
        DomainAdminResponse domain = domainAdminConverter.mapMainData(topDomain);
        setChildrenAndWidgets(domain);
        return domain;
    }

    private void setChildrenAndWidgets(DomainAdminResponse domain) {
        List<DomainAdminResponse> allToByFrom = domainRepository.findAllToByFrom(domain.getId()).stream()
                .map(o -> domainAdminConverter.mapWParentId(o, domain.getId()))
                .collect(toList());
        if (isNotEmpty(allToByFrom)) {
            for (DomainAdminResponse subElement : allToByFrom) {
                setChildrenAndWidgets(subElement);
            }
            domain.setSubElements(allToByFrom);
        }
        List<Widget> dbWidgets = widgetRepository.findWidgetsByDomain(domain.getId(), ET);
        if (isNotEmpty(dbWidgets)) {
            domain.setWidgets(dbWidgets.stream().map(widgetAdminConverter::mapWCubes).collect(toList()));
        }
    }

}
