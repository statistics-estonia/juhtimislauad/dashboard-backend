package ee.stat.dashboard.service.admin.widget;

import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.user.widget.UserFilter;
import ee.stat.dashboard.model.user.widget.UserFilterValue;
import ee.stat.dashboard.model.user.widget.UserGraphType;
import ee.stat.dashboard.model.user.widget.UserLegend;
import ee.stat.dashboard.model.user.widget.UserLegendValue;
import ee.stat.dashboard.model.user.widget.UserWidget;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.repository.UserFilterRepository;
import ee.stat.dashboard.repository.UserFilterValueRepository;
import ee.stat.dashboard.repository.UserGraphTypeRepository;
import ee.stat.dashboard.repository.UserLegendRepository;
import ee.stat.dashboard.repository.UserLegendValueRepository;
import ee.stat.dashboard.repository.UserWidgetRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
@AllArgsConstructor
@Transactional
public class UserWidgetDeleteService {

    private UserWidgetRepository userWidgetRepository;
    private UserGraphTypeRepository userGraphTypeRepository;
    private UserFilterRepository userFilterRepository;
    private UserFilterValueRepository userFilterValueRepository;
    private UserLegendRepository userLegendRepository;
    private UserLegendValueRepository userLegendValueRepository;

    public void deleteUserWidgets(Dashboard dashboard) {
        List<UserWidget> userWidgets = userWidgetRepository.findAllByDashboard(dashboard.getId());
        deleteUserData(userWidgets);
        userWidgetRepository.deleteByDashboard(dashboard.getId());
    }

    public void deleteUserWidgets(Widget widget) {
        List<UserWidget> userWidgets = userWidgetRepository.findAllByWidget(widget.getId());
        deleteUserData(userWidgets);
        userWidgetRepository.deleteByWidget(widget.getId());
    }

    public void deleteUserWidgets(List<Long> wdIds) {
        List<UserWidget> userWidgets = userWidgetRepository.findAllByWidgetDomainIn(wdIds);
        deleteUserData(userWidgets);
        userWidgetRepository.deleteByWidgetDomainIn(wdIds);
    }

    public void deleteUserData(UserWidget userWidget) {
        deleteUserData(List.of(userWidget));
    }

    public void deleteUserFilters(List<Long> userGraphTypeIds) {
        List<UserFilter> userFilters = userGraphTypeIds.isEmpty() ? new ArrayList<>() : userFilterRepository.findAllByUserGraphTypeIn(userGraphTypeIds);
        List<Long> userFilterIds = userFilters.stream().map(UserFilter::getId).collect(toList());

        deleteUserFilterValues(userFilterIds);

        if (isNotEmpty(userFilterIds)) {
            userFilterRepository.deleteByIdIn(userFilterIds);
        }
    }

    public void deleteUserFiltersByFilter(List<Long> filterIds) {
        if (isNotEmpty(filterIds)) {
            userFilterRepository.deleteByFilterIn(filterIds);
            userLegendValueRepository.deleteByFilterIn(filterIds);
        }
    }

    public void deleteUserFilterValuesByFilterValue(List<Long> filterValuesIds) {
        if (isNotEmpty(filterValuesIds)) {
            userFilterValueRepository.deleteByFilterValueIn(filterValuesIds);
            userLegendValueRepository.deleteByFilterValueIn(filterValuesIds);
        }
    }

    private void deleteUserLegends(List<Long> userGraphTypeIds) {
        List<UserLegend> userLegends = userGraphTypeIds.isEmpty() ? new ArrayList<>() : userLegendRepository.findAllByUserGraphTypeIn(userGraphTypeIds);
        List<Long> userLegendIds = userLegends.stream().map(UserLegend::getId).collect(toList());

        deleteUserLegendValues(userLegendIds);

        if (isNotEmpty(userLegendIds)) {
            userLegendRepository.deleteByIdIn(userLegendIds);
        }
    }

    private void deleteUserData(List<UserWidget> userWidgets) {
        List<Long> userWidgetsIds = userWidgets.stream().map(UserWidget::getId).collect(toList());
        List<UserGraphType> allByUserWidget = userWidgets.isEmpty() ? new ArrayList<>() : userGraphTypeRepository.findAllByUserWidgetIn(userWidgetsIds);
        List<Long> userGraphTypeIds = allByUserWidget.stream().map(UserGraphType::getId).collect(toList());

        deleteUserFilters(userGraphTypeIds);
        deleteUserLegends(userGraphTypeIds);

        if (isNotEmpty(userGraphTypeIds)) {
            userGraphTypeRepository.deleteByIdIn(userGraphTypeIds);
        }
    }

    private void deleteUserLegendValues(List<Long> userLegendIds) {
        List<UserLegendValue> legendValues = userLegendIds.isEmpty() ? new ArrayList<>() : userLegendValueRepository.findAllByUserLegendIn(userLegendIds);
        List<Long> userLegendValueIds = legendValues.stream().map(UserLegendValue::getId).collect(toList());

        if (isNotEmpty(userLegendValueIds)) {
            userLegendValueRepository.deleteByIdIn(userLegendValueIds);
        }
    }

    private void deleteUserFilterValues(List<Long> userFilterIds) {
        List<UserFilterValue> userFilterValues = userFilterIds.isEmpty() ? new ArrayList<>() : userFilterValueRepository.findAllByUserFilterIn(userFilterIds);
        List<Long> userFilterValuesIds = userFilterValues.stream().map(UserFilterValue::getId).collect(toList());
        if (isNotEmpty(userFilterValuesIds)) {
            userFilterValueRepository.deleteByIdIn(userFilterValuesIds);
        }
    }
}
