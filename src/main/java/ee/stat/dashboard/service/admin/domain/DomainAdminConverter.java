package ee.stat.dashboard.service.admin.domain;

import ee.stat.dashboard.model.classifier.Domain;
import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.repository.DashboardRepository;
import ee.stat.dashboard.service.admin.dashboard.DashboardAdminConverter;
import ee.stat.dashboard.service.admin.domain.dto.DomainAdminResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class DomainAdminConverter {

    private final DashboardRepository dashboardRepository;
    private final DashboardAdminConverter dashboardAdminConverter;

    public DomainAdminResponse mapMainData(Domain element) {
        DomainAdminResponse dto = new DomainAdminResponse();
        dto.setId(element.getId());
        dto.setCode(element.getCode());
        dto.setLevel(element.getLevel());
        dto.setOrderNr(element.getOrderNr());
        dto.setLevelNameEn(element.getLevelNameEn());
        dto.setLevelNameEt(element.getLevelNameEt());
        dto.setNameEn(element.getNameEn());
        dto.setNameEt(element.getNameEt());
        return dto;
    }

    public DomainAdminResponse mapWParentId(Domain element, Long parentId) {
        DomainAdminResponse dto = mapMainData(element);
        dto.setParentId(parentId);
        return dto;
    }

    public DomainAdminResponse mapWDashboard(Domain domain) {
        Dashboard dashboard = dashboardRepository.findById(domain.getDashboard()).orElseThrow(RuntimeException::new);
        DomainAdminResponse domainAdminResponse = mapMainData(domain);
        domainAdminResponse.setDashboard(dashboardAdminConverter.mapSimple(dashboard));
        return domainAdminResponse;
    }
}
