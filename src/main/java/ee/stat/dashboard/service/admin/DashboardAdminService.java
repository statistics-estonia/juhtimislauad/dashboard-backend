package ee.stat.dashboard.service.admin;

import ee.stat.dashboard.controller.admin.DashboardStrategy;
import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.dashboard.DashboardUserType;
import ee.stat.dashboard.repository.DashboardRepository;
import ee.stat.dashboard.service.admin.dashboard.DashboardAdminConverter;
import ee.stat.dashboard.service.admin.dashboard.dto.DashboardAdminResponse;
import ee.stat.dashboard.service.admin.dto.RoleResponse;
import ee.stat.dashboard.service.admin.dto.StatPage;
import ee.stat.dashboard.util.ErrorCode;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static ee.stat.dashboard.util.ErrorCode.DASHBOARD_IS_NOT_EXISTING;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.springframework.data.domain.Sort.Direction.ASC;

@Service
@AllArgsConstructor
@Transactional(readOnly = true)
public class DashboardAdminService {

    private final DomainAdminService domainAdminService;
    private final RoleDashboardAdminService roleDashboardAdminService;
    private final DashboardAdminConverter dashboardAdminConverter;
    private final DashboardRepository dashboardRepository;

    public StatPage<DashboardAdminResponse> findAll(Integer page, Integer size, String name, DashboardStrategy dashboardStrategy) {
        PageRequest of = PageRequest.of(page, size, Sort.by(ASC, "nameEt"));
        Page<Dashboard> all = findAll(name, of);
        StatPage<DashboardAdminResponse> statPage = new StatPage<>();
        statPage.setPage(page);
        statPage.setSize(size);
        statPage.setTotalPages(all.getTotalPages());
        statPage.setTotalElements(all.getTotalElements());
        statPage.setContent(convertAll(all, dashboardStrategy));
        return statPage;
    }

    private List<DashboardAdminResponse> convertAll(Page<Dashboard> all, DashboardStrategy dashboardStrategy) {
        List<DashboardAdminResponse> results = new ArrayList<>();
        for (Dashboard dashboard : all.getContent()) {
            DashboardAdminResponse dashboardAdminResponse = dashboardAdminConverter.mapSimple(dashboard);
            if (dashboardStrategy.withElements()) {
                addElementsToDashboard(dashboard, dashboardAdminResponse);
            }
            results.add(dashboardAdminResponse);
        }
        return results;
    }

    private void addElementsToDashboard(Dashboard dashboard, DashboardAdminResponse dashboardAdminResponse) {
        dashboardAdminResponse.setElements(domainAdminService.loadDomains(dashboard.getId()));
        List<DashboardAdminResponse> children = dashboardAdminResponse.getSubDashboards();
        if (isNotEmpty(children)) {
            for (DashboardAdminResponse child : children) {
                child.setElements(domainAdminService.loadDomains(child.getId()));
            }
        }
    }

    public DashboardAdminResponse findById(Long id) {
        Dashboard dashboard = dashboardRepository.findById(id).orElseThrow(() -> new StatBadRequestException(DASHBOARD_IS_NOT_EXISTING, "Dashboard id" + id));
        if (dashboard.getUserType().isUser()) {
            throw new StatBadRequestException(ErrorCode.USER_DASHBOARDS_ARE_NOT_VISIBLE_TO_ADMIN);
        }
        DashboardAdminResponse dto = dashboardAdminConverter.mapWRegions(dashboard);
        dto.setElements(domainAdminService.loadDomainsAndWidgets(dashboard.getId()));
        RoleResponse role = roleDashboardAdminService.getRole(dashboard.getId());
        if (role != null) {
            dto.setMainRole(role.getMainRole());
            dto.setSubRole(role.getSubRole());
        }
        return dto;
    }

    private Page<Dashboard> findAll(String name, PageRequest of) {
        if (StringUtils.isNotBlank(name) && name.trim().length() >= 3) {
            return dashboardRepository.findAllByNameEtIsLikeAndLevel("%" + name + "%", 0, of);
        } else {
            return dashboardRepository.findAllByUserType(DashboardUserType.ADMIN, of);
        }
    }
}
