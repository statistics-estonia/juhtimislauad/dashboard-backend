package ee.stat.dashboard.service.admin.dashboard;


import ee.stat.dashboard.model.classifier.Domain;
import ee.stat.dashboard.model.classifier.DomainConnection;
import ee.stat.dashboard.model.widget.back.WidgetDomain;
import ee.stat.dashboard.repository.DomainConnectionRepository;
import ee.stat.dashboard.repository.DomainRepository;
import ee.stat.dashboard.repository.WidgetDomainRepository;
import ee.stat.dashboard.service.admin.dashboard.dto.DashboardAdminResponse;
import ee.stat.dashboard.service.admin.domain.DomainValidator;
import ee.stat.dashboard.service.admin.domain.dto.DomainAdminResponse;
import ee.stat.dashboard.service.admin.dto.ValidationStrategy;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import ee.stat.dashboard.service.widget.widget.WidgetCodeUtil;
import ee.stat.dashboard.util.ErrorCode;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static ee.stat.dashboard.service.widget.widget.WidgetConstants.DOMAIN_EN;
import static ee.stat.dashboard.service.widget.widget.WidgetConstants.DOMAIN_ET;
import static ee.stat.dashboard.service.widget.widget.WidgetConstants.MEASURE_EN;
import static ee.stat.dashboard.service.widget.widget.WidgetConstants.MEASURE_ET;
import static ee.stat.dashboard.service.widget.widget.WidgetConstants.PROGRAM_EN;
import static ee.stat.dashboard.service.widget.widget.WidgetConstants.PROGRAM_ET;
import static ee.stat.dashboard.service.widget.widget.WidgetConstants.RESULT_DOMAIN_EN;
import static ee.stat.dashboard.service.widget.widget.WidgetConstants.RESULT_DOMAIN_ET;
import static java.lang.String.format;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
@AllArgsConstructor
@Transactional
public class DashboardDomainService {

    private final WidgetDomainRepository widgetDomainRepository;
    private final DomainRepository domainRepository;
    private final DomainConnectionRepository domainConnectionRepository;
    private final DashboardDeleteService dashboardDeleteService;
    private final DomainValidator domainValidator;

    public void updateDomains(DashboardAdminResponse dashboard, ValidationStrategy strategy) {
        if (strategy.isUpdating()) {
            //todo2 in the future, if necessary optimize domain connections
            domainConnectionRepository.deleteByDashboardId(dashboard.getId());
        }

        int order = 0;
        List<Domain> domainsToSave = new ArrayList<>();
        List<WidgetDomain> widgetDomainsToSave = new ArrayList<>();
        List<WidgetDomain> existingWidgetDomains = widgetDomainRepository.findAllByDashboard(dashboard.getId());

        if (isNotEmpty(dashboard.getElements())) {
            Map<Long, String> uniqueWidgets = new HashMap<>();
            for (DomainAdminResponse level0 : dashboard.getElements()) {
                order = saveDomain(dashboard, order, domainsToSave, level0);
                saveWidgets(dashboard, widgetDomainsToSave, existingWidgetDomains, level0, uniqueWidgets);
                order = saveChildren(domainsToSave, widgetDomainsToSave, existingWidgetDomains, dashboard, level0, 1, order, uniqueWidgets);
            }
        }

        widgetDomainRepository.saveAll(widgetDomainsToSave);

        if (strategy.isUpdating()) {
            existingWidgetDomains.removeAll(widgetDomainsToSave);
            if (isNotEmpty(existingWidgetDomains)) {
                dashboardDeleteService.deleteUserWidgets(existingWidgetDomains);
            }

            List<Domain> existingDomains = domainRepository.findByDashboard(dashboard.getId());
            existingDomains.removeAll(domainsToSave);
            if (isNotEmpty(existingDomains)) {
                domainRepository.deleteAll(existingDomains);
            }
        }
    }

    private int saveDomain(DashboardAdminResponse dashboard, int order, List<Domain> domainsToSave, DomainAdminResponse level0) {
        Domain domain = toDbDomain(dashboard, order++, level0, 0);
        domainsToSave.add(domain);
        level0.setId(domain.getId());
        return order;
    }

    private int saveChildren(List<Domain> domainsToSave, List<WidgetDomain> widgetDomainsToSave, List<WidgetDomain> existingWidgetDomains, DashboardAdminResponse dashboard, DomainAdminResponse parent, int depth, int order, Map<Long, String> uniqueWidgets) {
        if (isNotEmpty(parent.getSubElements())) {
            for (DomainAdminResponse child : parent.getSubElements()) {
                order = saveChildDomain(domainsToSave, dashboard, parent, depth, order, child);
                saveWidgets(dashboard, widgetDomainsToSave, existingWidgetDomains, child, uniqueWidgets);
                order = saveChildren(domainsToSave, widgetDomainsToSave, existingWidgetDomains, dashboard, child, depth + 1, order, uniqueWidgets);
            }
        }
        return order;
    }

    private int saveChildDomain(List<Domain> domainsToSave, DashboardAdminResponse dashboard, DomainAdminResponse parent, int depth, int order, DomainAdminResponse child) {
        Domain domain = toDbDomain(dashboard, order++, child, depth);
        child.setId(domain.getId());
        domainConnectionRepository.save(new DomainConnection(parent.getId(), domain.getId()));
        domainsToSave.add(domain);
        return order;
    }

    private void saveWidgets(DashboardAdminResponse dashboard, List<WidgetDomain> widgetDomainsToSave,
                             List<WidgetDomain> existingWidgetDomains, DomainAdminResponse domain, Map<Long, String> uniqueWidgets) {
        if (isNotEmpty(domain.getWidgets())) {
            for (WidgetAdminResponse widget : domain.getWidgets()) {
                String domainName = uniqueWidgets.get(widget.getId());
                if (domainName != null) {
                    throw new StatBadRequestException(ErrorCode.WIDGET_ADMIN_WIDGET_EXISTS_WITH_ANOTHER_CONNECTION, format("widget %s already exists with another connection c1 %s c2 %s",
                            widget.getNameEn(), domainName, domain.getNameEn()));
                }
                uniqueWidgets.put(widget.getId(), domain.getNameEt());
                Optional<WidgetDomain> existingWidgetDomain = existingWidgetDomain(existingWidgetDomains, widget, dashboard, domain);
                widgetDomainsToSave.add(existingWidgetDomain
                        .orElseGet(() -> new WidgetDomain(widget.getId(), domain.getId(), dashboard.getId())));
            }
        }
    }

    private Optional<WidgetDomain> existingWidgetDomain(List<WidgetDomain> existingWidgetDomains, WidgetAdminResponse widget, DashboardAdminResponse dashboard, DomainAdminResponse domain) {
        return existingWidgetDomains.stream()
                .filter(wd -> wd.getWidget().equals(widget.getId()))
                .filter(wd -> wd.getDashboard().equals(dashboard.getId()))
                .filter(wd -> wd.getDomain().equals(domain.getId()))
                .findAny();
    }

    private Domain toDbDomain(DashboardAdminResponse dashboard, int order, DomainAdminResponse response, int level) {
        response.setLevel(level);
        domainValidator.validateDomain(response);
        response.setLevelNameEn(levelNameEn(response.getLevelNameEt()));
        Domain domain = new Domain();
        domain.setId(response.getId());
        domain.setLevel(level);
        domain.setOrderNr(order);
        domain.setDashboard(dashboard.getId());
        domain.setCode(WidgetCodeUtil.toCode(response.getNameEn()));
        domain.setShowLevelName(notRegularDomain(response));
        domain.setNameEn(response.getNameEn());
        domain.setNameEt(response.getNameEt());
        domain.setLevelNameEt(response.getLevelNameEt());
        domain.setLevelNameEn(response.getLevelNameEn());
        domainRepository.save(domain);
        return domain;
    }

    private boolean notRegularDomain(DomainAdminResponse response) {
        return !response.getLevelNameEt().equals(DOMAIN_ET);
    }

    private String levelNameEn(String levelNameEt) {
        if (levelNameEt.equals(RESULT_DOMAIN_ET)) {
            return RESULT_DOMAIN_EN;
        } else if (levelNameEt.equals(DOMAIN_ET)) {
            return DOMAIN_EN;
        } else if (levelNameEt.equals(PROGRAM_ET)) {
            return PROGRAM_EN;
        } else if (levelNameEt.equals(MEASURE_ET)) {
            return MEASURE_EN;
        } else {
            throw new IllegalStateException("unknown levelname: " + levelNameEt);
        }
    }
}
