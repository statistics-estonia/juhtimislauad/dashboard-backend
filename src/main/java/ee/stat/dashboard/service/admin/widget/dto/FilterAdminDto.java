package ee.stat.dashboard.service.admin.widget.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ee.stat.dashboard.model.widget.back.enums.AxisOrder;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.enums.TimePart;
import ee.stat.dashboard.model.widget.front.Period;
import ee.stat.dashboard.model.widget.front.enums.FilterDisplay;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
public class FilterAdminDto {

    private Long id;
    private FilterDisplay type;
    private String nameEt;
    private String nameEn;
    private String displayNameEt;
    private String displayNameEn;
    private Integer orderNr;
    private Integer numberOfValues;
    private Boolean time;
    private Boolean region;
    private TimePart timePart;
    private AxisOrder axisOrder;
    private Period periodEt;
    private Period periodEn;

    private List<FilterValueAdminDto> values;

    @JsonIgnore
    public Period getPeriod(Language lang){
        return lang.isEt() ? periodEt : periodEn;
    }

    @JsonIgnore
    public void setPeriod(Language lang, Period period){
        if (lang.isEt()) {
            this.periodEt = period;
        } else {
            this.periodEn = period;
        }
    }
}
