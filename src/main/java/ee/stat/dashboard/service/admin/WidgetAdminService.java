package ee.stat.dashboard.service.admin;

import ee.stat.dashboard.model.classifier.Domain;
import ee.stat.dashboard.model.widget.back.GraphType;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.back.excel.Excel;
import ee.stat.dashboard.model.widget.back.statdataapi.StatDataApi;
import ee.stat.dashboard.model.widget.back.statdatasql.StatDataSql;
import ee.stat.dashboard.repository.DomainRepository;
import ee.stat.dashboard.repository.ExcelRepository;
import ee.stat.dashboard.repository.GraphTypeRepository;
import ee.stat.dashboard.repository.StatDataApiRepository;
import ee.stat.dashboard.repository.StatDataSqlRepository;
import ee.stat.dashboard.repository.WidgetRepository;
import ee.stat.dashboard.service.admin.domain.DomainAdminConverter;
import ee.stat.dashboard.service.admin.dto.StatPage;
import ee.stat.dashboard.service.admin.dto.WidgetAdminRequestDto;
import ee.stat.dashboard.service.admin.widget.FilterValueCustomService;
import ee.stat.dashboard.service.admin.widget.GraphTypeAdminConverter;
import ee.stat.dashboard.service.admin.widget.WidgetAdminConverter;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static ee.stat.dashboard.util.ErrorCode.WIDGET_IS_NOT_EXISTING;
import static ee.stat.dashboard.util.StatDateUtil.getLatest;
import static ee.stat.dashboard.util.StatListUtil.first;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Slf4j
@Service
@AllArgsConstructor
@Transactional(readOnly = true)
public class WidgetAdminService {

    private final FilterValueCustomService filterValueCustomService;
    private final WidgetRepository widgetRepository;
    private final StatDataApiRepository statDataApiRepository;
    private final StatDataSqlRepository statDataSqlRepository;
    private final ExcelRepository excelRepository;
    private final GraphTypeRepository graphTypeRepository;
    private final DomainRepository domainRepository;
    private final WidgetAdminConverter widgetAdminConverter;
    private final DomainAdminConverter domainAdminConverter;
    private final GraphTypeAdminConverter graphTypeAdminConverter;

    public StatPage<WidgetAdminResponse> findAll(WidgetAdminRequestDto requestDto) {
        StatPage<Widget> all = widgetRepository.findAllByNameAndSources(requestDto);
        StatPage<WidgetAdminResponse> statPage = new StatPage<>();
        statPage.setPage(requestDto.getPage());
        statPage.setSize(requestDto.getSize());
        statPage.setTotalPages(all.getTotalPages());
        statPage.setTotalElements(all.getTotalElements());
        statPage.setContent(all.getContent().stream().map(widgetAdminConverter::mapWCubesWDashboards).collect(toList()));
        return statPage;
    }

    public WidgetAdminResponse findById(Long widgetId) {
        Widget widget = widgetRepository.findById(widgetId).orElseThrow(() -> new StatBadRequestException(WIDGET_IS_NOT_EXISTING));
        WidgetAdminResponse adminResponse = widgetAdminConverter.mapMainData(widget);
        widgetAdminConverter.mapDetailedData(adminResponse, widget);
        List<Domain> domains = domainRepository.findAllByWidget(widget.getId());
        if (isNotEmpty(domains)) {
            adminResponse.setElements(domains.stream().map(domainAdminConverter::mapWDashboard).collect(toList()));
        }

        List<StatDataApi> statDataApis = statDataApiRepository.findByWidget(widget.getId());
        if (isNotEmpty(statDataApis)) {
            StatDataApi first = first(statDataApis);
            adminResponse.setStatDb(widgetAdminConverter.convertStatDb(first));
            adminResponse.setDataUpdatedAt(getLatest(adminResponse.getDataUpdatedAt(), first.getLatestDate()));
        } else {
            List<StatDataSql> statDataSqls = statDataSqlRepository.findByWidget(widget.getId());
            if (isNotEmpty(statDataSqls)) {
                StatDataSql first = first(statDataSqls);
                adminResponse.setStatDb(widgetAdminConverter.convertStatDb(first));
                adminResponse.setDataUpdatedAt(getLatest(adminResponse.getDataUpdatedAt(), first.getLatestDate()));
            }
        }

        List<Excel> excels = excelRepository.findByWidget(widget.getId());
        if (isNotEmpty(excels)) {
            Excel first = first(excels);
            adminResponse.setExcel(widgetAdminConverter.convertExcel(first));
            adminResponse.setDataUpdatedAt(getLatest(adminResponse.getDataUpdatedAt(), first.getLatestDate()));
        }
        List<GraphType> graphTypes = graphTypeRepository.findAllByWidgetOrderByIdAsc(widget.getId());
        if (isNotEmpty(graphTypes)) {
            adminResponse.setGraphTypes(graphTypes.stream().map(graphType -> graphTypeAdminConverter.mapGraphTypeData(graphType, widget)).collect(toList()));
        } else {
            adminResponse.setGraphTypes(new ArrayList<>());
        }
        adminResponse.setDoAnyCustomFilterValuesExist(filterValueCustomService.doesWidgetHaveAnyCustomFilterValues(widgetId));
        adminResponse.setAreAnyCustomFilterValuesUsed(filterValueCustomService.doesWidgetUseAnyCustomFilterValues(widgetId));
        return adminResponse;
    }

}
