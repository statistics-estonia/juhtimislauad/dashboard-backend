package ee.stat.dashboard.service.admin.domain.dto;

import ee.stat.dashboard.service.admin.dashboard.dto.DashboardAdminResponse;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DomainAdminResponse {

    private Long id;
    private Long parentId;
    private Integer orderNr;
    private String code;
    private String nameEt;
    private String nameEn;
    private Integer level;
    private String levelNameEt;
    private String levelNameEn;

    private DashboardAdminResponse dashboard;
    private List<WidgetAdminResponse> widgets;
    private List<DomainAdminResponse> subElements;
}
