package ee.stat.dashboard.service.admin.process.dto;

import ee.stat.dashboard.model.process.enums.LogStatus;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import java.time.LocalDateTime;

import static jakarta.persistence.EnumType.STRING;
import static jakarta.persistence.GenerationType.IDENTITY;

@Getter
@Setter
public class StatLogDto {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long processLog;
    @Enumerated(STRING)
    private LogStatus status;
    private LocalDateTime time;
    private String message;
    private String error;
    private WidgetAdminResponse widget;
    private String cube;
}
