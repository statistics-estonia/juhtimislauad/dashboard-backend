package ee.stat.dashboard.service.admin.widget;

import ee.stat.dashboard.service.admin.widget.dto.FilterValueCustomAdminResponse;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.NoArgsConstructor;

import static ee.stat.dashboard.util.ErrorCode.FILTER_VALUE_CUSTOM_ADMIN_DIMENSIONS_EN_ARE_SAME;
import static ee.stat.dashboard.util.ErrorCode.FILTER_VALUE_CUSTOM_ADMIN_DIMENSIONS_ET_ARE_SAME;
import static ee.stat.dashboard.util.ErrorCode.FILTER_VALUE_CUSTOM_ADMIN_DIMENSION_1_NAME_EN_MISSING;
import static ee.stat.dashboard.util.ErrorCode.FILTER_VALUE_CUSTOM_ADMIN_DIMENSION_1_NAME_ET_MISSING;
import static ee.stat.dashboard.util.ErrorCode.FILTER_VALUE_CUSTOM_ADMIN_DIMENSION_2_NAME_EN_MISSING;
import static ee.stat.dashboard.util.ErrorCode.FILTER_VALUE_CUSTOM_ADMIN_DIMENSION_2_NAME_ET_MISSING;
import static ee.stat.dashboard.util.ErrorCode.FILTER_VALUE_CUSTOM_ADMIN_DISPLAY_NAME_EN_MISSING;
import static ee.stat.dashboard.util.ErrorCode.FILTER_VALUE_CUSTOM_ADMIN_DISPLAY_NAME_EN_TOO_LONG;
import static ee.stat.dashboard.util.ErrorCode.FILTER_VALUE_CUSTOM_ADMIN_DISPLAY_NAME_ET_MISSING;
import static ee.stat.dashboard.util.ErrorCode.FILTER_VALUE_CUSTOM_ADMIN_DISPLAY_NAME_ET_TOO_LONG;
import static ee.stat.dashboard.util.ErrorCode.FILTER_VALUE_CUSTOM_ADMIN_OPERATION_MISSING;
import static ee.stat.dashboard.util.ErrorCode.FILTER_VALUE_CUSTOM_ADMIN_SELECTION_VALUES_ARE_SAME;
import static ee.stat.dashboard.util.ErrorCode.FILTER_VALUE_CUSTOM_ADMIN_SELECTION_VALUE_1_MISSING;
import static ee.stat.dashboard.util.ErrorCode.FILTER_VALUE_CUSTOM_ADMIN_SELECTION_VALUE_2_MISSING;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class FilterValueCustomValidator {

    private static final int DISPLAY_NAME_MAX_LENGTH = 400;

    public static void validate(FilterValueCustomAdminResponse adminResponse) {
        if (isNull(adminResponse.getDisplayNameEt())) {
            throw new StatBadRequestException(FILTER_VALUE_CUSTOM_ADMIN_DISPLAY_NAME_ET_MISSING);
        }
        if (isNull(adminResponse.getDisplayNameEn())) {
            throw new StatBadRequestException(FILTER_VALUE_CUSTOM_ADMIN_DISPLAY_NAME_EN_MISSING);
        }
        if (isNull(adminResponse.getDimension1NameEt())) {
            throw new StatBadRequestException(FILTER_VALUE_CUSTOM_ADMIN_DIMENSION_1_NAME_ET_MISSING);
        }
        if (isNull(adminResponse.getDimension1NameEn())) {
            throw new StatBadRequestException(FILTER_VALUE_CUSTOM_ADMIN_DIMENSION_1_NAME_EN_MISSING);
        }
        if (isNull(adminResponse.getSelectionValue1())) {
            throw new StatBadRequestException(FILTER_VALUE_CUSTOM_ADMIN_SELECTION_VALUE_1_MISSING);
        }
        if (isNull(adminResponse.getSelectionValue2())) {
            throw new StatBadRequestException(FILTER_VALUE_CUSTOM_ADMIN_SELECTION_VALUE_2_MISSING);
        }
        if (isNull(adminResponse.getOperation())) {
            throw new StatBadRequestException(FILTER_VALUE_CUSTOM_ADMIN_OPERATION_MISSING);
        }

        if (adminResponse.getDisplayNameEt().length() > DISPLAY_NAME_MAX_LENGTH) {
            throw new StatBadRequestException(FILTER_VALUE_CUSTOM_ADMIN_DISPLAY_NAME_ET_TOO_LONG);
        }
        if (adminResponse.getDisplayNameEn().length() > DISPLAY_NAME_MAX_LENGTH) {
            throw new StatBadRequestException(FILTER_VALUE_CUSTOM_ADMIN_DISPLAY_NAME_EN_TOO_LONG);
        }
        
        if (isNull(adminResponse.getDimension2NameEt()) && isNull(adminResponse.getDimension2NameEn())
                && adminResponse.getSelectionValue1().equals(adminResponse.getSelectionValue2())) {
            throw new StatBadRequestException(FILTER_VALUE_CUSTOM_ADMIN_SELECTION_VALUES_ARE_SAME);
        }

        if (firstNonNullSecondNull(adminResponse.getDimension2NameEt(), adminResponse.getDimension2NameEn())) {
            throw new StatBadRequestException(FILTER_VALUE_CUSTOM_ADMIN_DIMENSION_2_NAME_EN_MISSING);
        }
        if (firstNonNullSecondNull(adminResponse.getDimension2NameEn(), adminResponse.getDimension2NameEt())) {
            throw new StatBadRequestException(FILTER_VALUE_CUSTOM_ADMIN_DIMENSION_2_NAME_ET_MISSING);
        }
    }

    private static boolean firstNonNullSecondNull(String first, String second) {
        return nonNull(first) && isNull(second);
    }
}
