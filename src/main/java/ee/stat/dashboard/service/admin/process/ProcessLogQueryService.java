package ee.stat.dashboard.service.admin.process;

import ee.stat.dashboard.model.process.ProcessLog;
import ee.stat.dashboard.repository.ProcessLogRepository;
import ee.stat.dashboard.service.admin.dto.StatPage;
import ee.stat.dashboard.service.admin.process.dto.ProcessLogDto;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static ee.stat.dashboard.util.ErrorCode.MISSING_PROCESS_LOG;
import static java.util.stream.Collectors.toList;
import static org.springframework.data.domain.Sort.Direction.DESC;

@Service
@Transactional
@AllArgsConstructor
public class ProcessLogQueryService {

    private final ProcessLogRepository processLogRepository;

    public StatPage<ProcessLogDto> findAll(Integer page, Integer size) {
        PageRequest of = PageRequest.of(page, size, Sort.by(DESC, "id"));
        Page<ProcessLog> all = processLogRepository.findAllBy(of);
        StatPage<ProcessLogDto> statPage = new StatPage<>();
        statPage.setPage(page);
        statPage.setSize(size);
        statPage.setTotalPages(all.getTotalPages());
        statPage.setTotalElements(all.getTotalElements());
        statPage.setContent(all.getContent().stream().map(this::convert).collect(toList()));
        return statPage;
    }

    public ProcessLogDto convert(ProcessLog dbLog) {
        ProcessLogDto dto = new ProcessLogDto();
        dto.setId(dbLog.getId());
        dto.setProcess(dbLog.getProcess().getNameEt());
        dto.setStartTime(dbLog.getStartTime());
        dto.setEndTime(dbLog.getEndTime());
        dto.setStatus(dbLog.getStatus());
        dto.setAnalysed(dbLog.getAnalysed());
        dto.setUnchanged(dbLog.getUnchanged());
        dto.setChanged(dbLog.getChanged());
        dto.setFailed(dbLog.getFailed());
        return dto;
    }

    public ProcessLogDto findById(Long id) {
        ProcessLog processLog = processLogRepository.findById(id).orElseThrow(() -> new StatBadRequestException(MISSING_PROCESS_LOG));
        return convert(processLog);
    }
}
