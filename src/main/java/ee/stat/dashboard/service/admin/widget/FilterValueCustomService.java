package ee.stat.dashboard.service.admin.widget;

import ee.stat.dashboard.model.widget.back.FilterValueCustom;
import ee.stat.dashboard.repository.FilterValueCustomForFilterRepository;
import ee.stat.dashboard.repository.FilterValueCustomRepository;
import ee.stat.dashboard.service.admin.widget.dto.FilterValueCustomAdminResponse;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static ee.stat.dashboard.service.admin.widget.FilterValueCustomValidator.validate;
import static ee.stat.dashboard.service.widget.widget.FilterValueCustomConverter.mapToDto;
import static ee.stat.dashboard.service.widget.widget.FilterValueCustomConverter.mapToEntity;
import static ee.stat.dashboard.util.ErrorCode.FILTER_VALUE_CUSTOM_ADMIN_DELETE_FORBIDDEN_ALREADY_REFERENCED_IN_A_FILTER;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
@AllArgsConstructor
@Transactional
public class FilterValueCustomService {

    private final FilterValueCustomRepository filterValueCustomRepository;
    private final FilterValueCustomForFilterRepository filterValueCustomForFilterRepository;

    public FilterValueCustomAdminResponse saveFilterValueCustom(Long widgetId, FilterValueCustomAdminResponse adminResponse) {
        validate(adminResponse);
        FilterValueCustom savedEntity = filterValueCustomRepository.save(mapToEntity(widgetId, adminResponse));
        return mapToDto(savedEntity);
    }

    public void deleteCustomFilterValues(Long widgetId) {
        if (doesWidgetUseAnyCustomFilterValues(widgetId)) {
            throw new StatBadRequestException(FILTER_VALUE_CUSTOM_ADMIN_DELETE_FORBIDDEN_ALREADY_REFERENCED_IN_A_FILTER);
        }
        filterValueCustomRepository.deleteAllByWidget(widgetId);
    }

    public boolean doesWidgetHaveAnyCustomFilterValues(Long widgetId) {
        return isNotEmpty(filterValueCustomRepository.findAllByWidgetOrderByIdAsc(widgetId));
    }

    public boolean doesWidgetUseAnyCustomFilterValues(Long widgetId) {
        return isNotEmpty(filterValueCustomForFilterRepository.findAllByWidgetId(widgetId));
    }
}
