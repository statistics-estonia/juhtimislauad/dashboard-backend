package ee.stat.dashboard.service.admin.widget;

import ee.stat.dashboard.config.props.ExcelConfig;
import ee.stat.dashboard.model.widget.back.enums.ExcelType;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.excel.Excel;
import ee.stat.dashboard.model.widget.back.excel.ExcelData;
import ee.stat.dashboard.repository.ExcelDataRepository;
import ee.stat.dashboard.repository.ExcelRepository;
import ee.stat.dashboard.service.admin.widget.dto.ExcelAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import ee.stat.dashboard.service.widget.coordinates.ExcelToCoordinates;
import ee.stat.dashboard.service.widget.coordinates.exception.ExcelImportException;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Arrays;

import static ee.stat.dashboard.util.StatListUtil.firstOrNull;
import static ee.stat.dashboard.util.StatListUtil.listOrNull;
import static java.lang.String.format;

@Slf4j
@Service
@AllArgsConstructor
@Transactional
public class ExcelAdminService {

    private ExcelRepository excelRepository;
    private ExcelDataRepository excelDataRepository;
    private ExcelToCoordinates excelToCoordinates;
    private ExcelConfig excelConfig;

    public void updateExcels(WidgetAdminResponse widget) {
        Excel existingExcel = firstOrNull(excelRepository.findByWidget(widget.getId()));
        ExcelAdminDto newExcel = widget.getExcel();
        if (existingExcel == null || newExcel == null || !existingExcel.getFilename().equals(newExcel.getFilename())) {
            if (newExcel != null) {
                Excel excel = new Excel();
                excel.setWidget(widget.getId());
                excel.setFilename(newExcel.getFilename());
                excel.setLocation(excelConfig.getLocation() + newExcel.getFilename());
                excel.setExcelType(ExcelType.DATA);
                excel.setCreatedAt(LocalDateTime.now());
                excelRepository.save(excel);
                ExcelData data = new ExcelData();
                data.setExcel(excel.getId());
                Arrays.stream(Language.values())
                        .forEach(lang -> {
                            try {
                                //todo2 xml dimension might not match the frontName
                                data.setResponse(excelToCoordinates.convert(excel, lang, listOrNull(widget.getDimensions())), lang);
                            } catch (ExcelImportException e) {
                                log.error("Excel import exception {}", e.getMessage(), e);
                                throw new StatBadRequestException(e.getCode(), format("excel problem: %s", e.getMessage()));
                            }
                        });
                excelDataRepository.save(data);
                newExcel.setExcelType(excel.getExcelType());
                newExcel.setId(excel.getId());
                widget.setExcel(newExcel);
            }
            if (existingExcel != null) {
                widget.setExcel(null);
                excelDataRepository.deleteByExcel(existingExcel.getId());
                excelRepository.deleteById(existingExcel.getId());
            }
        }
    }
}
