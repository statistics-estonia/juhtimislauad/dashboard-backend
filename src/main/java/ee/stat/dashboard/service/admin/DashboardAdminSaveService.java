package ee.stat.dashboard.service.admin;

import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.dashboard.DashboardType;
import ee.stat.dashboard.model.dashboard.DashboardUserType;
import ee.stat.dashboard.repository.DashboardRepository;
import ee.stat.dashboard.service.admin.dashboard.DashboardCacheManager;
import ee.stat.dashboard.service.admin.dashboard.DashboardDeleteService;
import ee.stat.dashboard.service.admin.dashboard.DashboardDomainService;
import ee.stat.dashboard.service.admin.dashboard.DashboardRegionService;
import ee.stat.dashboard.service.admin.dashboard.DashboardValidator;
import ee.stat.dashboard.service.admin.dashboard.dto.DashboardAdminResponse;
import ee.stat.dashboard.service.admin.dto.ValidationStrategy;
import ee.stat.dashboard.service.widget.widget.WidgetCodeUtil;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

import static ee.stat.dashboard.service.admin.dto.ValidationStrategy.SAVING;
import static ee.stat.dashboard.service.admin.dto.ValidationStrategy.UPDATING;
import static ee.stat.dashboard.util.ErrorCode.DASHBOARD_IS_NOT_EXISTING;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
@AllArgsConstructor
@Transactional
public class DashboardAdminSaveService {

    private final DashboardValidator dashboardValidator;
    private final DashboardDeleteService dashboardDeleteService;
    private final DashboardDomainService dashboardDomainService;
    private final DashboardRegionService dashboardRegionService;
    private final RoleDashboardAdminService roleDashboardAdminService;
    private final DashboardRepository dashboardRepository;
    private final DashboardCacheManager dashboardCacheManager;

    public Long save(DashboardAdminResponse dashboard) {
        dashboard.setType(isNotEmpty(dashboard.getRegions()) ? DashboardType.REGIONAL : DashboardType.GLOBAL);
        dashboardValidator.validate(dashboard, SAVING);
        dashboard.setCode(WidgetCodeUtil.toCode(dashboard.getNameEn()));
        Dashboard dbDashboard = new Dashboard();
        updateEntity(dbDashboard, dashboard);
        dbDashboard.setCreatedAt(LocalDateTime.now());

        Dashboard saved = saveOrUpdate(dashboard, dbDashboard, SAVING);

        dashboardCacheManager.save(dashboard);
        return saved.getId();
    }

    public Long update(Long id, DashboardAdminResponse dashboard) {
        dashboard.setType(isNotEmpty(dashboard.getRegions()) ? DashboardType.REGIONAL : DashboardType.GLOBAL);
        dashboardValidator.validate(dashboard, UPDATING);

        dashboard.setCode(WidgetCodeUtil.toCode(dashboard.getNameEn()));
        Dashboard dbDashboard = getDashboard(id);
        updateEntity(dbDashboard, dashboard);

        Dashboard saved = saveOrUpdate(dashboard, dbDashboard, UPDATING);

        dashboardCacheManager.update(dashboard);
        return saved.getId();
    }

    public void delete(Long id) {
        Dashboard dashboard = getDashboard(id);
        dashboardDeleteService.delete(dashboard);
        dashboardCacheManager.delete(id);
    }

    private Dashboard saveOrUpdate(DashboardAdminResponse dashboard, Dashboard dbDashboard, ValidationStrategy saving) {
        Dashboard saved = dashboardRepository.save(dbDashboard);
        dashboard.setId(saved.getId());
        roleDashboardAdminService.saveRole(dashboard);
        dashboardRegionService.updateRegions(dashboard);
        dashboardDomainService.updateDomains(dashboard, saving);
        return saved;
    }

    private void updateEntity(Dashboard entity, DashboardAdminResponse dto) {
        entity.setCode(dto.getCode());
        entity.setNameEt(dto.getNameEt());
        entity.setNameEn(dto.getNameEn());
        entity.setStatus(dto.getStatus());
        entity.setType(dto.getType());
        entity.setUpdatedAt(LocalDateTime.now());
        entity.setUserType(DashboardUserType.ADMIN);
    }

    private Dashboard getDashboard(Long id) {
        return dashboardRepository.findById(id).orElseThrow(() -> new StatBadRequestException(DASHBOARD_IS_NOT_EXISTING, "Dashboard id" + id));
    }
}
