package ee.stat.dashboard.service.admin.dashboard;

import ee.stat.dashboard.model.classifier.ClassifierCode;
import ee.stat.dashboard.model.classifier.Element;
import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.repository.DashboardRepository;
import ee.stat.dashboard.service.admin.dashboard.dto.DashboardAdminResponse;
import ee.stat.dashboard.service.admin.dashboard.dto.ElementAdminResponse;
import ee.stat.dashboard.service.admin.dto.ValidationStrategy;
import ee.stat.dashboard.service.element.ElementServiceCache;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;

import static ee.stat.dashboard.util.ErrorCode.DASHBOARD_ADMIN_EHAK_ROLE_MUST_HAVE_NO_SUBROLE;
import static ee.stat.dashboard.util.ErrorCode.DASHBOARD_ADMIN_NAME_ALREADY_EXISTS;
import static ee.stat.dashboard.util.ErrorCode.DASHBOARD_ADMIN_NAME_IS_MANDATORY;
import static ee.stat.dashboard.util.ErrorCode.DASHBOARD_ADMIN_REGIONAL_DASHBOARD_INVALID_REGION;
import static ee.stat.dashboard.util.ErrorCode.DASHBOARD_ADMIN_REGIONAL_DASHBOARD_MUST_HAVE_REGION;
import static ee.stat.dashboard.util.ErrorCode.DASHBOARD_ADMIN_ROLE_MUST_HAVE_SUBROLE;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
@AllArgsConstructor
public class DashboardValidator {

    private final DashboardRepository dashboardRepository;
    private final ElementServiceCache elementServiceCache;

    public void validate(DashboardAdminResponse dashboard, ValidationStrategy strategy) {
        validateName(dashboard, strategy, dashboard.getNameEt(), dashboardRepository::findByNameEt);
        validateName(dashboard, strategy, dashboard.getNameEn(), dashboardRepository::findByNameEn);
        if (dashboard.getStatus() == null) {
            throw new StatBadRequestException(DASHBOARD_ADMIN_REGIONAL_DASHBOARD_INVALID_REGION);
        }
        if (dashboard.getType().isRegional()) {
            validateRegions(dashboard);
        }
        if (dashboard.getMainRole() != null) {
            if (dashboard.getMainRole().getCode() == ClassifierCode.EHAK && dashboard.getSubRole() != null) {
                throw new StatBadRequestException(DASHBOARD_ADMIN_EHAK_ROLE_MUST_HAVE_NO_SUBROLE);
            }
            if (dashboard.getMainRole().getCode() != ClassifierCode.EHAK && dashboard.getSubRole() == null) {
                throw new StatBadRequestException(DASHBOARD_ADMIN_ROLE_MUST_HAVE_SUBROLE);
            }
        }
    }

    private void validateRegions(DashboardAdminResponse dashboard) {
        if (isEmpty(dashboard.getRegions())) {
            throw new StatBadRequestException(DASHBOARD_ADMIN_REGIONAL_DASHBOARD_MUST_HAVE_REGION);
        } else {
            List<Element> clfCode = elementServiceCache.findAllByClfCode(ClassifierCode.EHAK);
            List<Long> collect = clfCode.stream().map(Element::getId).collect(toList());
            for (ElementAdminResponse region : dashboard.getRegions()) {
                if (!collect.contains(region.getId())) {
                    throw new StatBadRequestException(DASHBOARD_ADMIN_REGIONAL_DASHBOARD_INVALID_REGION);
                }
            }
        }
    }

    private void validateName(DashboardAdminResponse dashboard, ValidationStrategy strategy, String name, Function<String, List<Dashboard>> findByName) {
        if (name == null) {
            throw new StatBadRequestException(DASHBOARD_ADMIN_NAME_IS_MANDATORY);
        } else {
            List<Dashboard> byName = findByName.apply(name);
            if (strategy.isSaving()) {
                if (isNotEmpty(byName)) {
                    throw new StatBadRequestException(DASHBOARD_ADMIN_NAME_ALREADY_EXISTS);
                }
            } else {
                if (byName.size() > 1) {
                    throw new StatBadRequestException(DASHBOARD_ADMIN_NAME_ALREADY_EXISTS);
                } else if (byName.size() == 1) {
                    Dashboard dashboard1 = byName.get(0);
                    if (!dashboard1.getId().equals(dashboard.getId())) {
                        throw new StatBadRequestException(DASHBOARD_ADMIN_NAME_ALREADY_EXISTS);
                    }
                }
            }
        }
    }
}
