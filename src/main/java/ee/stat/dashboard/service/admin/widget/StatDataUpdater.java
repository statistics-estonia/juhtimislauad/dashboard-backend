package ee.stat.dashboard.service.admin.widget;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ee.stat.dashboard.model.widget.back.GraphType;
import ee.stat.dashboard.model.widget.back.statdataapi.StatDataApiData;
import ee.stat.dashboard.model.widget.back.statdatasql.StatDataSqlData;
import ee.stat.dashboard.repository.StatDataApiDataRepository;
import ee.stat.dashboard.repository.StatDataIdAndRequest;
import ee.stat.dashboard.repository.StatDataSqlDataRepository;
import ee.stat.dashboard.service.admin.widget.dto.StatDataAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import ee.stat.dashboard.service.client.StatDataApiClient;
import ee.stat.dashboard.service.client.StatDataSqlClient;
import ee.stat.dashboard.service.client.dataapi.StatDataApiResponse;
import ee.stat.dashboard.service.client.datasql.StatDataSqlResponse;
import ee.stat.dashboard.service.client.query.StatApiQuery;
import ee.stat.dashboard.service.statdata.StatMetaFetchException;
import ee.stat.dashboard.service.statdata.StatRequestBuilderManager;
import ee.stat.dashboard.service.statdata.request.dto.StatRequestConfig;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import static ee.stat.dashboard.model.widget.back.StatDataType.DATA_API;
import static ee.stat.dashboard.model.widget.back.StatDataType.DATA_SQL;
import static ee.stat.dashboard.service.statdata.request.dto.StatRequestConfig.requestConfig;
import static ee.stat.dashboard.util.ErrorCode.STAT_DATA_API_JSON_PROBLEM;
import static ee.stat.dashboard.util.ErrorCode.STAT_DB_HAD_ERROR;
import static ee.stat.dashboard.util.StatListUtil.firstOrNull;

@Slf4j
@Service
@AllArgsConstructor
public class StatDataUpdater {

    private final StatDataApiClient statDataApiClient;
    private final StatDataSqlClient statDataSqlClient;
    private final StatRequestBuilderManager statRequestBuilderManager;
    private final StatDataApiDataRepository statDataApiDataRepository;
    private final StatDataSqlDataRepository statDataSqlDataRepository;
    private final ObjectMapper objectMapper;

    public void update(WidgetAdminResponse widget, GraphType graphType) {
        try {
            StatDataAdminDto statDataAdminDto = widget.getStatDb();
            StatRequestConfig config = requestConfig(widget, statDataAdminDto.getCube(), graphType.getFilters(), widget.getDimensions());
            if (statDataAdminDto.getStatDataType().isDataApi()) {
                statDataApiFlow(widget, graphType, statDataAdminDto, config);
            } else {
                statDataSqlFlow(widget, graphType, statDataAdminDto, config);
            }
        } catch (StatMetaFetchException e) {
            throw new StatBadRequestException(STAT_DB_HAD_ERROR, e.getMessage());
        } catch (JsonProcessingException e) {
            throw new StatBadRequestException(STAT_DATA_API_JSON_PROBLEM, e.getMessage());
        }
    }

    private void statDataSqlFlow(WidgetAdminResponse widget, GraphType graphType, StatDataAdminDto statDataAdminDto, StatRequestConfig config) throws JsonProcessingException, StatMetaFetchException {
        StatDataIdAndRequest existingStatData = firstOrNull(statDataSqlDataRepository.findRequestByGraphType(graphType.getId()));
        StatApiQuery query = statRequestBuilderManager.buildBody(config, DATA_SQL, widget.getId());
        String request = objectMapper.writeValueAsString(query);
        log.info("Data api data request {} {} {}", statDataAdminDto.getCube(), widget.getId(), request);

        if (shouldUpdateData(widget, existingStatData, request)) {
            StatDataSqlData data = new StatDataSqlData();
            data.setId(existingStatData != null ? existingStatData.getId() : null);
            data.setGraphType(graphType.getId());
            data.setStatDataSql(statDataAdminDto.getId());
            data.setRequest(request);
            StatDataSqlResponse statResponse = statDataSqlClient.getData(statDataAdminDto.getCube(), query);
            setResponsesToStat(data, statResponse);
            statDataSqlDataRepository.save(data);
        }
    }

    private void statDataApiFlow(WidgetAdminResponse widget, GraphType graphType, StatDataAdminDto statDataAdminDto, StatRequestConfig config) throws JsonProcessingException, StatMetaFetchException {
        StatDataIdAndRequest existingStatData = firstOrNull(statDataApiDataRepository.findRequestByGraphType(graphType.getId()));
        StatApiQuery query = statRequestBuilderManager.buildBody(config, DATA_API, widget.getId());
        String request = objectMapper.writeValueAsString(query);
        log.info("Data api data request {} {} {}", statDataAdminDto.getCube(), widget.getId(), request);

        if (shouldUpdateData(widget, existingStatData, request)) {
            StatDataApiData data = new StatDataApiData();
            data.setId(existingStatData != null ? existingStatData.getId() : null);
            data.setGraphType(graphType.getId());
            data.setStatDataApi(statDataAdminDto.getId());
            data.setRequest(request);
            StatDataApiResponse statResponse = statDataApiClient.getData(statDataAdminDto.getCube(), query);
            setResponsesToStat(data, statResponse);
            statDataApiDataRepository.save(data);
        }
    }

    private boolean shouldUpdateData(WidgetAdminResponse widget, StatDataIdAndRequest existingStatData, String request) {
        return existingStatData == null
                || widget.getStatDb().isPublishedAtChanged()
                || !existingStatData.getRequest().equals(request);
    }

    private void setResponsesToStat(StatDataApiData statDbData, StatDataApiResponse response) {
        statDbData.setResponseEt(response.getResponseEt());
        statDbData.setResponseEn(response.getResponseEn());
    }

    private void setResponsesToStat(StatDataSqlData statDbData, StatDataSqlResponse response) {
        statDbData.setResponseEt(response.getResponseEt());
        statDbData.setResponseEn(response.getResponseEn());
    }
}
