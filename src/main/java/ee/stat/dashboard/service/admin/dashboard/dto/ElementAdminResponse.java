package ee.stat.dashboard.service.admin.dashboard.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ElementAdminResponse {

    private Long id;
    private String code;
    private String nameEt;
    private String nameEn;
    private Integer level;

}
