package ee.stat.dashboard.service.admin;

import ee.stat.dashboard.model.classifier.Classifier;
import ee.stat.dashboard.model.classifier.ClassifierCode;
import ee.stat.dashboard.model.classifier.DashboardRule;
import ee.stat.dashboard.model.classifier.Element;
import ee.stat.dashboard.model.classifier.RoleDashboard;
import ee.stat.dashboard.repository.RoleDashboardRepository;
import ee.stat.dashboard.service.admin.dashboard.ElementAdminService;
import ee.stat.dashboard.service.admin.dashboard.dto.DashboardAdminResponse;
import ee.stat.dashboard.service.admin.dto.RoleResponse;
import ee.stat.dashboard.service.element.ClassifierServiceCache;
import ee.stat.dashboard.service.element.ElementServiceCache;
import ee.stat.dashboard.util.StatListUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;

@Service
@AllArgsConstructor
public class RoleDashboardAdminService {

    private final RoleDashboardRepository roleDashboardRepository;
    private final ElementServiceCache elementServiceCache;
    private final ClassifierServiceCache classifierServiceCache;
    private final ElementAdminService elementAdminService;

    public RoleResponse getRole(Long dashboardId) {
        List<RoleDashboard> roleDashboards = roleDashboardRepository.findByDashboard(dashboardId);
        if (isEmpty(roleDashboards)) {
            return null;
        }
        RoleDashboard roleDashboard = StatListUtil.first(roleDashboards);
        if (roleDashboard.getRoleElement() != null) {
            Element element = elementServiceCache.findById(roleDashboard.getRoleElement()).orElseThrow(RuntimeException::new);
            return new RoleResponse(elementAdminService.mapMainData(element.getClassifier()), elementAdminService.mapMainData(element));
        }
        if (roleDashboard.getRoleClassifier() != null) {
            Classifier classifier = classifierServiceCache.findById(roleDashboard.getRoleClassifier()).orElseThrow(RuntimeException::new);
            return new RoleResponse(elementAdminService.mapMainData(classifier));
        }
        throw new UnsupportedOperationException("unknown situation");
    }

    public void saveRole(DashboardAdminResponse dashboard) {
        if (dashboard.getMainRole() == null) {
            roleDashboardRepository.deleteByDashboard(dashboard.getId());
        } else {
            List<RoleDashboard> byDashboard = roleDashboardRepository.findByDashboard(dashboard.getId());
            RoleDashboard existingRoleDashboard = StatListUtil.firstOrNull(byDashboard);
            if (dashboard.getMainRole().getCode() == ClassifierCode.EHAK) {
                updateClassifier(dashboard, existingRoleDashboard);
            } else {
                updateElement(dashboard, existingRoleDashboard);
            }
        }
    }

    private void updateElement(DashboardAdminResponse dashboard, RoleDashboard existingRoleDashboard) {
        RoleDashboard newRoleDashboard = updateCommon(dashboard, existingRoleDashboard);
        Element element = elementServiceCache.findById(dashboard.getSubRole().getId()).orElseThrow(RuntimeException::new);
        newRoleDashboard.setRoleElement(dashboard.getSubRole().getId());
        newRoleDashboard.setDashboardRule(clfToRule(element.getClassifier().getCode()));
        roleDashboardRepository.save(newRoleDashboard);
    }

    private void updateClassifier(DashboardAdminResponse dashboard, RoleDashboard existingRoleDashboard) {
        RoleDashboard newRoleDashboard = updateCommon(dashboard, existingRoleDashboard);
        Classifier classifier = classifierServiceCache.findById(dashboard.getMainRole().getId()).orElseThrow(RuntimeException::new);
        newRoleDashboard.setRoleClassifier(dashboard.getMainRole().getId());
        newRoleDashboard.setDashboardRule(clfToRule(classifier.getCode()));
        roleDashboardRepository.save(newRoleDashboard);
    }

    private RoleDashboard updateCommon(DashboardAdminResponse dashboard, RoleDashboard existingRoleDashboard) {
        RoleDashboard newRoleDashboard = new RoleDashboard();
        newRoleDashboard.setId(existingRoleDashboard != null ? existingRoleDashboard.getId() : null);
        newRoleDashboard.setDashboard(dashboard.getId());
        newRoleDashboard.setCreatedAt(existingRoleDashboard != null ? existingRoleDashboard.getCreatedAt() : LocalDateTime.now());
        return newRoleDashboard;
    }

    private DashboardRule clfToRule(ClassifierCode code) {
        if (code == ClassifierCode.EHAK) {
            return DashboardRule.ONE_DASHBOARD;
        } else if (code == ClassifierCode.BUSINESS || code == ClassifierCode.GOV) {
            return DashboardRule.DASHBOARDS;
        }
        throw new UnsupportedOperationException("unknown clf: " + code.name());
    }
}
