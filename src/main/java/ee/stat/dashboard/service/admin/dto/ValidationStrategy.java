package ee.stat.dashboard.service.admin.dto;

public enum ValidationStrategy {
    SAVING, UPDATING;

    public boolean isSaving() {
        return this == SAVING;
    }

    public boolean isUpdating() {
        return this == UPDATING;
    }
}
