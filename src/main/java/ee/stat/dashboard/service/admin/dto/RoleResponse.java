package ee.stat.dashboard.service.admin.dto;

import ee.stat.dashboard.service.admin.dashboard.dto.ClassifierAdminResponse;
import ee.stat.dashboard.service.admin.dashboard.dto.ElementAdminResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RoleResponse {

    private ClassifierAdminResponse mainRole;
    private ElementAdminResponse subRole;

    public RoleResponse(ClassifierAdminResponse mainRole) {
        this.mainRole = mainRole;
    }
}
