package ee.stat.dashboard.service.admin.widget;

import ee.stat.dashboard.model.widget.back.Filter;
import ee.stat.dashboard.model.widget.back.FilterValue;
import ee.stat.dashboard.model.widget.back.FilterValueCustom;
import ee.stat.dashboard.model.widget.back.FilterValueCustomForFilter;
import ee.stat.dashboard.model.widget.back.GraphType;
import ee.stat.dashboard.repository.FilterRepository;
import ee.stat.dashboard.repository.FilterValueCustomForFilterRepository;
import ee.stat.dashboard.repository.FilterValueCustomRepository;
import ee.stat.dashboard.repository.FilterValueRepository;
import ee.stat.dashboard.repository.GraphTypeRepository;
import ee.stat.dashboard.repository.UserFilterValueRepository;
import ee.stat.dashboard.repository.UserLegendValueRepository;
import ee.stat.dashboard.service.admin.widget.dto.FilterAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.FilterValueAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.GraphTypeAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlDimension;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlValue;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static ee.stat.dashboard.service.widget.widget.WidgetConstants.FILTER_VALUE_CUSTOM_PREFIX;
import static java.lang.Long.parseLong;
import static java.lang.String.valueOf;
import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.BooleanUtils.isTrue;

@Slf4j
@Service
@AllArgsConstructor
public class GraphTypeAdminService {

    private final FilterRepository filterRepository;
    private final GraphTypeRepository graphTypeRepository;
    private final FilterValueRepository filterValueRepository;
    private final FilterValueCustomRepository filterValueCustomRepository;
    private final FilterValueCustomForFilterRepository filterValueCustomForFilterRepository;
    private final WidgetDeleteService widgetDeleteService;
    private final GraphTypeValidator graphTypeValidator;
    private final UserFilterValueRepository userFilterValueRepository;
    private final UserLegendValueRepository userLegendValueRepository;
    private final WidgetCacheManager widgetCacheManager;
    private final StatDataUpdater statDataUpdater;

    public void updateGraphTypes(WidgetAdminResponse widget) {
        List<GraphType> existingGraphTypes = graphTypeRepository.findAllByWidgetOrderByIdAsc(widget.getId());
        List<GraphType> newGraphTypes = new ArrayList<>();
        List<Filter> existingFilters = new ArrayList<>();
        List<Filter> savedFilters = new ArrayList<>();
        List<FilterValue> existingFilterValues = new ArrayList<>();
        List<FilterValue> savedFilterValues = new ArrayList<>();
        List<FilterValueCustomForFilter> existingCustomValuesForFilter = new ArrayList<>();
        List<FilterValueCustomForFilter> savedCustomValuesForFilter = new ArrayList<>();
        if (isNotEmpty(widget.getGraphTypes())) {
            for (GraphTypeAdminDto graphTypeDto : widget.getGraphTypes()) {
                GraphType graphType = graphType(widget, graphTypeDto);
                newGraphTypes.add(graphType);
                existingFilters.addAll(filterRepository.findAllByGraphTypeOrderByOrderNrAscIdAsc(graphTypeDto.getId()));
                if (isNotEmpty(graphTypeDto.getFilters())) {
                    List<Filter> filters = new ArrayList<>();
                    int filterOrder = 0;
                    for (FilterAdminDto filterDto : graphTypeDto.getFilters()) {
                        Filter filter = filter(filterDto, widget, graphType, filterOrder++);
                        filters.add(filter);
                        if (isTrue(filterDto.getTime())) {
                            continue;
                        }
                        existingFilterValues.addAll(filterValueRepository.findAllByFilterOrderByOrderNrAscIdAsc(filterDto.getId()));
                        existingCustomValuesForFilter.addAll(filterValueCustomForFilterRepository.findAllByFilterId(filterDto.getId()));

                        if (isNotEmpty(widget.getDimensions())) {
                            Optional<XmlDimension> xmlDimensionOp = widget.getDimensions().stream().filter(d -> d.getNameEt().equals(filterDto.getNameEt())).findAny();
                            if (xmlDimensionOp.isPresent() && isNotEmpty(filterDto.getValues())) {
                                List<String> collect = xmlDimensionOp.get().getValues().stream().map(XmlValue::getValueEt).collect(toList());
                                filterDto.getValues().sort((v1, v2) -> collect.indexOf(v1.getValueEt()) > collect.indexOf(v2.getValueEt()) ? 1 : 0);
                            }
                        }
                        List<FilterValueAdminDto> values = filterDto.getValues();

                        if (isNotEmpty(values)) {
                            List<FilterValue> filterValues = new ArrayList<>();
                            List<FilterValueCustomForFilter> customFilterValues = new ArrayList<>();
                            int valueOrder = 0;
                            for (FilterValueAdminDto valueDto : values) {
                                if (isTrue(valueDto.getCustom())) {
                                    valueOrder++;
                                    customFilterValues.add(valueCustom(valueDto, filter, graphTypeDto));
                                } else {
                                    filterValues.add(value(valueDto, filter, valueOrder++));
                                }
                            }
                            filter.setValues(filterValues);
                            savedFilterValues.addAll(filterValues);
                            savedCustomValuesForFilter.addAll(customFilterValues);
                        }
                    }
                    graphType.setFilters(filters);
                    savedFilters.addAll(filters);
                }

                if (widget.getStatDb() != null) {
                    statDataUpdater.update(widget, graphType);
                }
            }
        }
        existingFilterValues.removeAll(savedFilterValues);
        if (isNotEmpty(existingFilterValues)) {
            widgetDeleteService.deleteFilterValues(existingFilterValues);
        }

        existingCustomValuesForFilter.removeAll(savedCustomValuesForFilter);
        if (isNotEmpty(existingCustomValuesForFilter)) {
            List<Long> ids = existingCustomValuesForFilter.stream().map(FilterValueCustomForFilter::getId).collect(toList());
            filterValueCustomForFilterRepository.deleteByIdIn(ids);
        }

        // if no graphs are left, all custom filter relations must also be deleted before continuing with filter deletion
        if (isEmpty(existingFilters) && isEmpty(savedFilters)) {
            filterValueCustomForFilterRepository.deleteByWidgetId(widget.getId());
        }

        existingFilters.removeAll(savedFilters);
        if (isNotEmpty(existingFilters)) {
            List<Long> ids = existingFilterValues.stream().map(FilterValue::getId).collect(toList());
            userFilterValueRepository.deleteByFilterValueIn(ids);
            userLegendValueRepository.deleteByFilterValueIn(ids);
        }

        widgetDeleteService.deleteFilters(existingFilters);
        existingGraphTypes.removeAll(newGraphTypes);
        widgetCacheManager.updateGraphType(graphTypeIdsForCache(existingGraphTypes, newGraphTypes));
        widgetDeleteService.deleteGraphTypes(existingGraphTypes);
    }

    private GraphType graphType(WidgetAdminResponse widget, GraphTypeAdminDto graphTypeAdminDto) {
        graphTypeValidator.validateGraphType(graphTypeAdminDto, widget);
        GraphType graphType = new GraphType();
        graphType.setId(graphTypeAdminDto.getId());
        graphType.setType(graphTypeAdminDto.getType());
        graphType.setWidget(widget.getId());
        graphType.setDefaultType(isTrue(graphTypeAdminDto.getDefaultType()));
        graphType.setVerticalRule(graphTypeAdminDto.getType().isVertical() ? graphTypeAdminDto.getVerticalRule() : null);
        graphType.setMapRule(graphTypeAdminDto.getType().isMap() ? graphTypeAdminDto.getMapRule() : null);
        graphTypeRepository.save(graphType);
        return graphType;
    }

    private Filter filter(FilterAdminDto filterDto, WidgetAdminResponse widget, GraphType graphType, int filterOrder) {
        graphTypeValidator.validateFilter(filterDto);
        Filter filter = new Filter();
        filter.setId(filterDto.getId());
        filter.setWidget(widget.getId());
        filter.setGraphType(graphType.getId());
        filter.setType(filterDto.getType());
        filter.setNameEt(filterDto.getNameEt());
        filter.setNameEn(filterDto.getNameEn());
        filter.setDisplayNameEt(filterDto.getDisplayNameEt());
        filter.setDisplayNameEn(filterDto.getDisplayNameEn());
        filter.setOrderNr(filterOrder);
        filter.setNumberOfValues(filterDto.getNumberOfValues() != null ? filterDto.getNumberOfValues() : 1);
        filter.setTime(isTrue(filterDto.getTime()));
        filter.setRegion(isTrue(filterDto.getRegion()));
        filter.setTimePart(filterDto.getTimePart());
        filter.setAxisOrder(filterDto.getAxisOrder());
        filterRepository.save(filter);
        return filter;
    }

    private FilterValue value(FilterValueAdminDto valueDto, Filter filter, int orderNr) {
        graphTypeValidator.validateValue(valueDto);
        FilterValue value = new FilterValue();
        value.setId(valueDto.getId());
        value.setFilter(filter.getId());
        value.setValueEt(valueDto.getValueEt());
        value.setValueEn(valueDto.getValueEn());
        value.setDisplayValueEt(valueDto.getDisplayValueEt());
        value.setDisplayValueEn(valueDto.getDisplayValueEn());
        value.setSelected(isTrue(valueDto.getSelected()));
        value.setOrderNr(orderNr);
        filterValueRepository.save(value);
        return value;
    }

    private FilterValueCustomForFilter valueCustom(FilterValueAdminDto valueDto, Filter filter, GraphTypeAdminDto graphTypeAdminDto) {
        graphTypeValidator.validateValueCustom(valueDto, filter, graphTypeAdminDto);

        String initialId = isNull(valueDto.getId())
                ? valueDto.getStatId()
                : valueOf(valueDto.getId());
        Long filterValueCustomId = parseLong(initialId.replace(FILTER_VALUE_CUSTOM_PREFIX, ""));
        FilterValueCustom filterValueCustom = filterValueCustomRepository.findById(filterValueCustomId)
                .orElseThrow(() -> new EntityNotFoundException("FilterValueCustom not found with id: " + filterValueCustomId));
        filterValueCustom.setSelected(isTrue(valueDto.getSelected()));

        FilterValueCustomForFilter valueForFilter = new FilterValueCustomForFilter();
        valueForFilter.setFilter(filter.getId());
        valueForFilter.setFilterValueCustom(filterValueCustom);
        filterValueCustomForFilterRepository.save(valueForFilter);
        return valueForFilter;
    }

    private List<Long> graphTypeIdsForCache(List<GraphType> existingGraphTypes, List<GraphType> newGraphTypes) {
        List<Long> existingGraphTypeIds = existingGraphTypes.stream().map(GraphType::getId).distinct().collect(toList());
        List<Long> newGraphTypeIds = newGraphTypes.stream().map(GraphType::getId).distinct().collect(toList());
        existingGraphTypeIds.addAll(newGraphTypeIds);
        return existingGraphTypeIds;
    }
}
