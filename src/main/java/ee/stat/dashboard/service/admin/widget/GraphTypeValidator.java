package ee.stat.dashboard.service.admin.widget;

import ee.stat.dashboard.model.widget.back.Filter;
import ee.stat.dashboard.model.widget.front.enums.FilterDisplay;
import ee.stat.dashboard.service.admin.widget.dto.FilterAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.FilterValueAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.GraphTypeAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import ee.stat.dashboard.util.StatBadRequestException;
import org.springframework.stereotype.Service;

import static ee.stat.dashboard.model.widget.front.enums.FilterDisplay.AXIS;
import static ee.stat.dashboard.model.widget.front.enums.FilterDisplay.LEGEND;
import static ee.stat.dashboard.model.widget.front.enums.FilterDisplay.MENU;
import static ee.stat.dashboard.util.ErrorCode.FILTER_ADMIN_AXIS_MUST_HAVE_ORDER;
import static ee.stat.dashboard.util.ErrorCode.FILTER_ADMIN_AXIS_ONLY_1;
import static ee.stat.dashboard.util.ErrorCode.FILTER_ADMIN_FILTER_MUST_HAVE_VALUES;
import static ee.stat.dashboard.util.ErrorCode.FILTER_ADMIN_MAX_1_LEGEND;
import static ee.stat.dashboard.util.ErrorCode.FILTER_ADMIN_MAX_1_REGION;
import static ee.stat.dashboard.util.ErrorCode.FILTER_ADMIN_MAX_1_TIME;
import static ee.stat.dashboard.util.ErrorCode.FILTER_ADMIN_MAX_4_LEGEND_MENU;
import static ee.stat.dashboard.util.ErrorCode.FILTER_ADMIN_NO_NAME_EN;
import static ee.stat.dashboard.util.ErrorCode.FILTER_ADMIN_NO_NAME_ET;
import static ee.stat.dashboard.util.ErrorCode.FILTER_ADMIN_NO_TYPE;
import static ee.stat.dashboard.util.ErrorCode.FILTER_ADMIN_TIME_AXIS_MUST_HAVE_0_VALUES;
import static ee.stat.dashboard.util.ErrorCode.FILTER_ADMIN_TIME_PART_FILTER_MUST_HAVE_TIME_PART;
import static ee.stat.dashboard.util.ErrorCode.FILTER_VALUE_ADMIN_NO_VALUE_EN;
import static ee.stat.dashboard.util.ErrorCode.FILTER_VALUE_ADMIN_NO_VALUE_ET;
import static ee.stat.dashboard.util.ErrorCode.FILTER_VALUE_CUSTOM_FILTER_MUST_NOT_BE_REGIONAL;
import static ee.stat.dashboard.util.ErrorCode.FILTER_VALUE_CUSTOM_FILTER_MUST_NOT_BE_TIME_PART;
import static ee.stat.dashboard.util.ErrorCode.FILTER_VALUE_CUSTOM_FILTER_MUST_NOT_BE_TIME_WITH_GRAPH_TYPE_LINE_OR_BAR_OR_STACKED_OR_AREA;
import static ee.stat.dashboard.util.ErrorCode.GRAPH_TYPE_ADMIN_CONSTRAINT_ONLY_1_VALUE;
import static ee.stat.dashboard.util.ErrorCode.GRAPH_TYPE_ADMIN_EXCEL_ONLY_LINE_AND_BAR;
import static ee.stat.dashboard.util.ErrorCode.GRAPH_TYPE_ADMIN_LINE_BAR_STACKED_AREA_MUST_HAVE_TIME_AXIS;
import static ee.stat.dashboard.util.ErrorCode.GRAPH_TYPE_ADMIN_MAP_MUST_HAVE_MAP_RULE;
import static ee.stat.dashboard.util.ErrorCode.GRAPH_TYPE_ADMIN_MAP_VERTICAL_PIE_TREEMAP_RADAR_PYRAMID_MUST_HAVE_TIME_NOT_AXIS;
import static ee.stat.dashboard.util.ErrorCode.GRAPH_TYPE_ADMIN_NO_TYPE;
import static ee.stat.dashboard.util.ErrorCode.GRAPH_TYPE_ADMIN_VERTICAL_MUST_HAVE_VERTICAL_RULE;
import static ee.stat.dashboard.util.ErrorCode.WIDGET_ADMIN_WEEK_GRAPH_TYPES_ARE_RESTRICTED;
import static java.lang.String.format;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.BooleanUtils.isNotTrue;
import static org.apache.commons.lang3.BooleanUtils.isTrue;
import static org.springframework.util.ObjectUtils.isEmpty;

@Service
public class GraphTypeValidator {

    private static final String GRAPH_TYPE_W_TYPE = "graphType %s";
    private static final String FILTER_W_NAME = "filter %s";

    public void validateGraphType(GraphTypeAdminDto graphTypeDto, WidgetAdminResponse widget) {
        if (graphTypeDto.getType() == null) {
            throw new StatBadRequestException(GRAPH_TYPE_ADMIN_NO_TYPE);
        }
        if (widget.getTimePeriod().isWeek() && !graphTypeDto.getType().isLine()) {
            throw new StatBadRequestException(WIDGET_ADMIN_WEEK_GRAPH_TYPES_ARE_RESTRICTED);
        }
        if (!graphTypeDto.getType().isLineOrBar()) {
            if (widget.getExcel() != null && widget.getStatDb() == null) {
                throw new StatBadRequestException(GRAPH_TYPE_ADMIN_EXCEL_ONLY_LINE_AND_BAR);
            }
        }
        if (isNotEmpty(graphTypeDto.getFilters())) {
            validateFiltersAccordingToGraphType(graphTypeDto);
        }
        if (graphTypeDto.getType().isMap()) {
            if (graphTypeDto.getMapRule() == null) {
                throw new StatBadRequestException(GRAPH_TYPE_ADMIN_MAP_MUST_HAVE_MAP_RULE);
            }
        }
        if (graphTypeDto.getType().isVertical()) {
            if (graphTypeDto.getVerticalRule() == null) {
                throw new StatBadRequestException(GRAPH_TYPE_ADMIN_VERTICAL_MUST_HAVE_VERTICAL_RULE);
            }
        }
    }

    public void validateFilter(FilterAdminDto filterDto) {
        if (filterDto.getType() == null) {
            throw new StatBadRequestException(FILTER_ADMIN_NO_TYPE);
        }
        if (filterDto.getNameEt() == null) {
            throw new StatBadRequestException(FILTER_ADMIN_NO_NAME_ET);
        }
        if (filterDto.getNameEn() == null) {
            throw new StatBadRequestException(FILTER_ADMIN_NO_NAME_EN, filterWName(filterDto));
        }
        if (filterDto.getType().isTimePart() && filterDto.getTimePart() == null) {
            throw new StatBadRequestException(FILTER_ADMIN_TIME_PART_FILTER_MUST_HAVE_TIME_PART, filterWName(filterDto));
        }
        if (isNotTrue(filterDto.getTime())) {
            notTimePartFilterMustHaveValues(filterDto);
            constraintMustHaveExactly1Value(filterDto);
            axisMustHaveOrder(filterDto);
        }
        if (filterDto.getType().isAxis() && isTrue(filterDto.getTime()) && isNotEmpty(filterDto.getValues())) {
            throw new StatBadRequestException(FILTER_ADMIN_TIME_AXIS_MUST_HAVE_0_VALUES, filterWName(filterDto));
        }
    }

    public void validateValue(FilterValueAdminDto valueDto) {
        if (valueDto.getValueEn() == null) {
            throw new StatBadRequestException(FILTER_VALUE_ADMIN_NO_VALUE_EN);
        }
        if (valueDto.getValueEt() == null) {
            throw new StatBadRequestException(FILTER_VALUE_ADMIN_NO_VALUE_ET);
        }
    }

    public void validateValueCustom(FilterValueAdminDto valueDto, Filter filter, GraphTypeAdminDto graphTypeAdminDto) {
        validateValue(valueDto);
        if (filter.isRegion()) {
            throw new StatBadRequestException(FILTER_VALUE_CUSTOM_FILTER_MUST_NOT_BE_REGIONAL);
        }
        if (filter.getType().isTimePart()) {
            throw new StatBadRequestException(FILTER_VALUE_CUSTOM_FILTER_MUST_NOT_BE_TIME_PART);
        }
        if (graphTypeAdminDto.getType().isLineOrBarOrStackedOrArea() && filter.isTime()) {
            throw new StatBadRequestException(FILTER_VALUE_CUSTOM_FILTER_MUST_NOT_BE_TIME_WITH_GRAPH_TYPE_LINE_OR_BAR_OR_STACKED_OR_AREA);
        }
    }

    private void validateFiltersAccordingToGraphType(GraphTypeAdminDto graphTypeDto) {
        exactlyOneAxis(graphTypeDto);
        if (graphTypeDto.getType().isLineOrBarOrStackedOrArea()) {
            if (!axisIsTime(graphTypeDto)) {
                throw new StatBadRequestException(GRAPH_TYPE_ADMIN_LINE_BAR_STACKED_AREA_MUST_HAVE_TIME_AXIS, graphTypeWName(graphTypeDto));
            }
        }
        if (graphTypeDto.getType().isMapOrVerticalOrPieOrTreemapOrRadarOrPyramid()) {
            if (axisIsTime(graphTypeDto)) {
                throw new StatBadRequestException(GRAPH_TYPE_ADMIN_MAP_VERTICAL_PIE_TREEMAP_RADAR_PYRAMID_MUST_HAVE_TIME_NOT_AXIS, graphTypeWName(graphTypeDto));
            }
        }
        if (getCount(graphTypeDto, LEGEND) > 1) {
            throw new StatBadRequestException(FILTER_ADMIN_MAX_1_LEGEND, graphTypeWName(graphTypeDto));
        }
        if (getCount(graphTypeDto, LEGEND, MENU) > 4) {
            throw new StatBadRequestException(FILTER_ADMIN_MAX_4_LEGEND_MENU, graphTypeWName(graphTypeDto));
        }
        if (countOfRegions(graphTypeDto) > 1) {
            throw new StatBadRequestException(FILTER_ADMIN_MAX_1_REGION, graphTypeWName(graphTypeDto));
        }
        if (countOfTime(graphTypeDto) != 1) {
            throw new StatBadRequestException(FILTER_ADMIN_MAX_1_TIME, graphTypeWName(graphTypeDto));
        }
    }

    private void notTimePartFilterMustHaveValues(FilterAdminDto filterDto) {
        if (filterDto.getTimePart() == null && isEmpty(filterDto.getValues())) {
            throw new StatBadRequestException(FILTER_ADMIN_FILTER_MUST_HAVE_VALUES);
        }
    }

    private void constraintMustHaveExactly1Value(FilterAdminDto filterDto) {
        if (filterDto.getType().isConstraint() && filterDto.getValues().size() != 1) {
            throw new StatBadRequestException(GRAPH_TYPE_ADMIN_CONSTRAINT_ONLY_1_VALUE);
        }
    }

    private void axisMustHaveOrder(FilterAdminDto filterDto) {
        if (filterDto.getType().isAxis() && filterDto.getAxisOrder() == null) {
            throw new StatBadRequestException(FILTER_ADMIN_AXIS_MUST_HAVE_ORDER);
        }
    }

    private void exactlyOneAxis(GraphTypeAdminDto graphTypeDto) {
        if (getCount(graphTypeDto, AXIS) != 1) {
            throw new StatBadRequestException(FILTER_ADMIN_AXIS_ONLY_1);
        }
    }

    private long countOfTime(GraphTypeAdminDto graphTypeDto) {
        return graphTypeDto.getFilters().stream().filter(f -> isTrue(f.getTime())).count();
    }

    private long countOfRegions(GraphTypeAdminDto graphTypeDto) {
        return graphTypeDto.getFilters().stream().filter(f -> isTrue(f.getRegion())).count();
    }

    private boolean axisIsTime(GraphTypeAdminDto graphTypeDto) {
        return graphTypeDto.getFilters().stream()
                .filter(f -> f.getType() != null && f.getType().equals(AXIS))
                .anyMatch(f -> isTrue(f.getTime()));
    }

    private long getCount(GraphTypeAdminDto graphTypeDto, FilterDisplay axis) {
        return graphTypeDto.getFilters().stream().filter(f -> f.getType() != null && f.getType().equals(axis)).count();
    }

    private long getCount(GraphTypeAdminDto graphTypeDto, FilterDisplay f1, FilterDisplay f2) {
        return graphTypeDto.getFilters().stream().filter(f -> f.getType() != null &&
                (f.getType().equals(f1) || f.getType().equals(f2))).count();
    }

    private String graphTypeWName(GraphTypeAdminDto graphTypeDto) {
        return format(GRAPH_TYPE_W_TYPE, graphTypeDto.getType().name());
    }

    private String filterWName(FilterAdminDto filterDto) {
        return format(FILTER_W_NAME, filterDto.getNameEt());
    }
}
