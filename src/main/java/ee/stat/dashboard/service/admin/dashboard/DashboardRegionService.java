package ee.stat.dashboard.service.admin.dashboard;

import ee.stat.dashboard.model.dashboard.DashboardRegion;
import ee.stat.dashboard.repository.DashboardRegionRepository;
import ee.stat.dashboard.service.admin.dashboard.dto.DashboardAdminResponse;
import ee.stat.dashboard.service.admin.dashboard.dto.ElementAdminResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
@AllArgsConstructor
@Transactional
public class DashboardRegionService {

    private final DashboardRegionRepository dashboardRegionRepository;

    public void updateRegions(DashboardAdminResponse dashboard) {
        if (dashboard.getType().isGlobal()) {
            dashboardRegionRepository.deleteByDashboard(dashboard.getId());
        } else {
            List<DashboardRegion> existingDbRegions = dashboardRegionRepository.findAllByDashboard(dashboard.getId());

            List<DashboardRegion> regions = new ArrayList<>();
            for (ElementAdminResponse region : dashboard.getRegions()) {
                regions.add(existingDbRegions.stream()
                        .filter(e -> e.getElement().equals(region.getId()))
                        .findAny()
                        .orElseGet(() -> new DashboardRegion(dashboard.getId(), region.getId())));
            }

            existingDbRegions.removeAll(regions);
            dashboardRegionRepository.deleteAll(existingDbRegions);

            List<DashboardRegion> newRegions = regions.stream().filter(r -> r.getId() == null).collect(toList());
            if (isNotEmpty(newRegions)) {
                dashboardRegionRepository.saveAll(newRegions);
            }
        }
    }
}
