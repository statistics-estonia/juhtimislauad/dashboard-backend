package ee.stat.dashboard.service.admin.domain;

import ee.stat.dashboard.service.admin.domain.dto.DomainAdminResponse;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import static ee.stat.dashboard.service.widget.widget.WidgetConstants.MEASURE_ET;
import static ee.stat.dashboard.service.widget.widget.WidgetConstants.PROGRAM_ET;
import static ee.stat.dashboard.service.widget.widget.WidgetConstants.domainsEt;
import static ee.stat.dashboard.util.ErrorCode.DOMAIN_ADMIN_LEVELNAME_ET_INVALID;
import static ee.stat.dashboard.util.ErrorCode.DOMAIN_ADMIN_LEVELNAME_ET_MISSING;
import static ee.stat.dashboard.util.ErrorCode.DOMAIN_ADMIN_SHORTNAME_EN_MISSING;
import static ee.stat.dashboard.util.ErrorCode.DOMAIN_ADMIN_SHORTNAME_ET_MISSING;
import static java.lang.String.format;

@Service
@AllArgsConstructor
public class DomainValidator {

    public void validateDomain(DomainAdminResponse response) {
        if (response.getNameEt() == null) {
            throw new StatBadRequestException(DOMAIN_ADMIN_SHORTNAME_ET_MISSING);
        }
        if (response.getNameEn() == null) {
            throw new StatBadRequestException(DOMAIN_ADMIN_SHORTNAME_EN_MISSING);
        }
        if (response.getLevelNameEt() == null) {
            throw new StatBadRequestException(DOMAIN_ADMIN_LEVELNAME_ET_MISSING);
        }
        if (response.getLevel() == 0) {
            validateLevel(response, domainsEt);
        }
        if (response.getLevel() == 1) {
            validateLevel(response, PROGRAM_ET);
        }
        if (response.getLevel() == 2) {
            validateLevel(response, MEASURE_ET);
        }
    }

    private void validateLevel(DomainAdminResponse response, List<String> levelNamesEt) {
        if (!levelNamesEt.contains(response.getLevelNameEt())) {
            throw new StatBadRequestException(DOMAIN_ADMIN_LEVELNAME_ET_INVALID);
        }
    }

    private void validateLevel(DomainAdminResponse response, String programEt) {
        if (!programEt.equals(response.getLevelNameEt())) {
            throw new StatBadRequestException(DOMAIN_ADMIN_LEVELNAME_ET_INVALID, format("incorrect et levelName %s %s", response.getLevel(), response.getLevelNameEt()));
        }
    }
}
