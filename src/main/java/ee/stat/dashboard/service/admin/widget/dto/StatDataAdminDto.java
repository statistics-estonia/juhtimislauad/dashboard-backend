package ee.stat.dashboard.service.admin.widget.dto;


import ee.stat.dashboard.model.widget.back.StatDataType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatDataAdminDto {

    private Long id;
    private String cube;
    private StatDataType statDataType;
    private boolean publishedAtChanged;
}
