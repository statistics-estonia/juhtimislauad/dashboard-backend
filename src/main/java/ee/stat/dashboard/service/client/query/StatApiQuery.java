package ee.stat.dashboard.service.client.query;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class StatApiQuery {

    private List<SelectionFilter> query;
    private ResponseType response;
}
