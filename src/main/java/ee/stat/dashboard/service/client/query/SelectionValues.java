package ee.stat.dashboard.service.client.query;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SelectionValues {

    private String filter;
    private List<String> values;

    @Getter
    @AllArgsConstructor
    public enum FilterType {
        /**
         * specify exact values
         */
        ITEM("item"),
        /**
         * wildcard value matching
         */
        ALL("all"),
        /**
         * top nr of values
         */
        TOP("top");

        private final String value;
    }
}
