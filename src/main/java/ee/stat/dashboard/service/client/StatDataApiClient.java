package ee.stat.dashboard.service.client;

import ee.stat.dashboard.config.props.StatConfig;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.client.dataapi.StatDataApiResponse;
import ee.stat.dashboard.service.client.query.StatApiQuery;
import ee.stat.dashboard.service.statdata.dimensions.api.StatMetaResponse;
import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


import java.text.MessageFormat;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@Service
@AllArgsConstructor
public class StatDataApiClient {

    private final RestTemplate restTemplate;
    private final StatConfig statConfig;

    @PostConstruct
    private void checkUrls(){
        checkUrl(statConfig.getDataApiUrl());
        checkUrl(statConfig.getDataApiCubesUrl());
    }

    public StatDataApiResponse getData(String cube, StatApiQuery request){
        StatDataApiResponse response = new StatDataApiResponse();
        for (Language lang : Language.values()) {
            response.setResponse(getData(lang, cube, request), lang);
        }
        return response;
    }

    public StatMetaResponse getMeta(Language lang, String cube) {
        ResponseEntity<StatMetaResponse> response = restTemplate.getForEntity(toUrl(lang, cube), StatMetaResponse.class);
        if (!response.getStatusCode().is2xxSuccessful()) {
            throw new IllegalStateException("response is not 200, code: " + cube);

        } else if (response.getBody() == null) {
            throw new IllegalStateException("response is null, code: " + cube);
        }
        return response.getBody();
    }

    public String getCubesData(){
        ResponseEntity<String> response = restTemplate.getForEntity(statConfig.getDataApiCubesUrl(), String.class);
        if (!response.getStatusCode().is2xxSuccessful()) {
            throw new IllegalStateException("response is not 200");

        } else if (response.getBody() == null) {
            throw new IllegalStateException("response is null");
        }
        return response.getBody();
    }

    private String getData(Language lang, String cube, StatApiQuery request) {
        ResponseEntity<String> response = restTemplate.exchange(toUrl(lang, cube), POST, entity(request), String.class);
        if (!response.getStatusCode().is2xxSuccessful()) {
            throw new IllegalStateException("response is not 200, code: " + cube);
        } else if (response.getBody() == null) {
            throw new IllegalStateException("response is null, code: " + cube);
        }
        return response.getBody();
    }

    private String toUrl(Language lang, String cube) {
        return MessageFormat.format(statConfig.getDataApiUrl(), lang.toLocale(), cube);
    }

    private <T> HttpEntity<T> entity(T query) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(APPLICATION_JSON);
        return new HttpEntity<>(query, headers);
    }

    private void checkUrl(String metaUrl) {
        if (isBlank(metaUrl)) {
            throw new IllegalStateException("Link meta url is not configured");
        }
    }
}
