package ee.stat.dashboard.service.client.datasql;

import ee.stat.dashboard.model.widget.back.enums.Language;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatDataSqlResponse {

    private String responseEt;
    private String responseEn;

    public void setResponse(String response, Language lang) {
        if (lang.isEt()) {
            this.setResponseEt(response);
        } else {
            this.setResponseEn(response);
        }
    }

    public String getResponse(Language lang) {
        return lang.isEt() ? responseEt : responseEn;
    }

}
