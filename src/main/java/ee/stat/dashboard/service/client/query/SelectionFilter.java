package ee.stat.dashboard.service.client.query;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SelectionFilter {

    private String code;
    private SelectionValues selection;

}
