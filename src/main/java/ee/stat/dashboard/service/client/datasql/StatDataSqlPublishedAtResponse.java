package ee.stat.dashboard.service.client.datasql;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class StatDataSqlPublishedAtResponse {

    private String id;
    private String path;
    private String title;
    private BigDecimal score;
    private String published;

}
