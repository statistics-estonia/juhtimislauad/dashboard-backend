package ee.stat.dashboard.service.client;

import ee.stat.dashboard.config.props.StatConfig;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.client.datasql.StatDataSqlResponse;
import ee.stat.dashboard.service.client.query.StatApiQuery;
import ee.stat.dashboard.service.statdata.dimensions.api.StatMetaResponse;
import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.MessageFormat;

import static ee.stat.dashboard.model.widget.back.enums.Language.ET;
import static ee.stat.dashboard.model.widget.back.enums.Language.values;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@Service
@AllArgsConstructor
public class StatDataSqlClient {

    private final RestTemplate restTemplate;
    private final StatConfig statConfig;

    @PostConstruct
    private void checkUrls(){
        checkUrl(statConfig.getDataSqlUrl());
        checkUrl(statConfig.getDataSqlPublishedAtUrl());
    }

    public StatDataSqlResponse getData(String cube, StatApiQuery request){
        StatDataSqlResponse response = new StatDataSqlResponse();
        for (Language lang : values()) {
            response.setResponse(getData(lang, cube, request), lang);
        }
        return response;
    }

    public StatMetaResponse getMeta(Language lang, String cube) {
        ResponseEntity<StatMetaResponse> response = restTemplate.getForEntity(toUrl(lang, cube), StatMetaResponse.class);
        if (!response.getStatusCode().is2xxSuccessful()) {
            throw new IllegalStateException("response is not 200, code: " + cube);

        } else if (response.getBody() == null) {
            throw new IllegalStateException("response is null, code: " + cube);
        }
        return response.getBody();
    }

    public String getPublishedAtInformation(String cube){
        ResponseEntity<String> response = restTemplate.getForEntity(toPublishedUrl(cube), String.class);
        if (!response.getStatusCode().is2xxSuccessful()) {
            throw new IllegalStateException("response is not 200");

        } else if (response.getBody() == null) {
            throw new IllegalStateException("response is null");
        }
        return response.getBody();
    }

    private String toPublishedUrl(String cube) {
        return MessageFormat.format(statConfig.getDataSqlPublishedAtUrl(), ET.toLocale(), cube);
    }

    private String getData(Language lang, String cube, StatApiQuery request) {
        ResponseEntity<String> response = restTemplate.exchange(toUrl(lang, cube), POST, entity(request), String.class);
        if (!response.getStatusCode().is2xxSuccessful()) {
            throw new IllegalStateException("response is not 200, code: " + cube);
        } else if (response.getBody() == null) {
            throw new IllegalStateException("response is null, code: " + cube);
        }
        return response.getBody();
    }

    private String toUrl(Language lang, String cube) {
        return MessageFormat.format(statConfig.getDataSqlUrl(), lang.toLocale(), cube);
    }

    private <T> HttpEntity<T> entity(T query) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(APPLICATION_JSON);
        return new HttpEntity<>(query, headers);
    }

    private void checkUrl(String metaUrl) {
        if (isBlank(metaUrl)) {
            throw new IllegalStateException("Link meta url is not configured");
        }
    }
}
