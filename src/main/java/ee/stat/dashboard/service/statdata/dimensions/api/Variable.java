package ee.stat.dashboard.service.statdata.dimensions.api;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class Variable {

    private String code;
    private String text;
    private List<String> values;
    private List<String> valueTexts;
    private Boolean time;
    private Boolean elimination;

}
