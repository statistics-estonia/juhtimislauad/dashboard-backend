package ee.stat.dashboard.service.statdata.request;

import ee.stat.dashboard.service.StatApiType;
import ee.stat.dashboard.service.client.query.StatApiQuery;
import ee.stat.dashboard.service.statdata.request.dto.StatRequestConfig;

public interface StatRequestBuilderInterface extends StatApiType {

    StatApiQuery buildBody(StatRequestConfig config, Long widgetId);
}
