package ee.stat.dashboard.service.statdata;

public class StatMetaFetchException extends Exception {

    public StatMetaFetchException(String message) {
        super(message);
    }

    public StatMetaFetchException(String message, Throwable cause) {
        super(message, cause);
    }
}
