package ee.stat.dashboard.service.statdata.publishedat;

import ee.stat.dashboard.service.StatApiType;

import java.time.LocalDateTime;

public interface StatPublishedAtInterface extends StatApiType {

    LocalDateTime publishedAt(String cube);
}
