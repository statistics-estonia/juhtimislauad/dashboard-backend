package ee.stat.dashboard.service.statdata.publishedat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.service.client.StatDataSqlClient;
import ee.stat.dashboard.service.client.datasql.StatDataSqlPublishedAtResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import static ee.stat.dashboard.model.widget.back.StatDataType.DATA_SQL;
import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;

@Slf4j
@Service
@AllArgsConstructor
public class StatDataSqlPublishedAtService implements StatPublishedAtInterface {

    private static final DateTimeFormatter STAT_SQL_PUBLISHED_PATTERN = ISO_DATE_TIME;
    private static final ZoneId EUROPE_TALLINN = ZoneId.of("Europe/Tallinn");
    private static final ZoneId UTC = ZoneId.of("UTC");

    private final ObjectMapper objectMapper;
    private final StatDataSqlClient statDataSqlClient;

    @Override
    public StatDataType statDataType() {
        return DATA_SQL;
    }

    @Override
    public LocalDateTime publishedAt(String cube) {
        Optional<StatDataSqlPublishedAtResponse> responseOp = getPublishedAtResponse(cube);
        if (responseOp.isEmpty()) {
            log.info("Cube published at response not found for {}", cube);
            return null;
        }
        LocalDateTime published = parse(responseOp.get().getPublished());
        if (published == null) {
            log.info("Cube published at response found, but date not found for {}", cube);
        }
        return published;
    }

    private Optional<StatDataSqlPublishedAtResponse> getPublishedAtResponse(String cube) {
        return mapToResponse(statDataSqlClient.getPublishedAtInformation(cube)).stream()
                .filter(response -> response.getId().equals(cube))
                .findFirst();
    }

    private List<StatDataSqlPublishedAtResponse> mapToResponse(String response) {
        try {
            return objectMapper.readValue(response, new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            log.error("Json error ", e);
            throw new RuntimeException("Dataset reading failed");
        }
    }

    public static LocalDateTime parse(String value) {
        try {
            return LocalDateTime.parse(value, STAT_SQL_PUBLISHED_PATTERN)
                    .atZone(EUROPE_TALLINN)
                    .withZoneSameInstant(UTC)
                    .toLocalDateTime();
        } catch (Exception e) {
            log.error("Unexpected error while parsing date {}", value, e);
            return null;
        }
    }
}
