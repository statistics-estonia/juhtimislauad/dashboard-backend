package ee.stat.dashboard.service.statdata.request;

import ee.stat.dashboard.model.widget.back.Filter;
import ee.stat.dashboard.model.widget.back.FilterValue;
import ee.stat.dashboard.model.widget.back.FilterValueCustom;
import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.model.widget.back.enums.FilterValueCustomType;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.repository.FilterValueCustomRepository;
import ee.stat.dashboard.service.client.query.ResponseType;
import ee.stat.dashboard.service.client.query.SelectionFilter;
import ee.stat.dashboard.service.client.query.SelectionValues;
import ee.stat.dashboard.service.client.query.StatApiQuery;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlDimension;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlValue;
import ee.stat.dashboard.service.statdata.request.dto.StatRequestConfig;
import ee.stat.dashboard.service.widget.coordinates.time.TimeService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static ee.stat.dashboard.model.widget.back.StatDataType.DATA_SHARED;
import static ee.stat.dashboard.model.widget.back.enums.FilterValueCustomType.ONE_DIMENSIONAL;
import static ee.stat.dashboard.model.widget.back.enums.FilterValueCustomType.OTHER;
import static ee.stat.dashboard.model.widget.back.enums.FilterValueCustomType.TWO_DIMENSIONAL_FIRST_MATCH;
import static ee.stat.dashboard.model.widget.back.enums.FilterValueCustomType.TWO_DIMENSIONAL_SAME_TYPE;
import static ee.stat.dashboard.model.widget.back.enums.FilterValueCustomType.TWO_DIMENSIONAL_SECOND_MATCH;
import static ee.stat.dashboard.model.widget.back.enums.Language.ET;
import static ee.stat.dashboard.model.widget.back.enums.TimePeriod.YEAR;
import static ee.stat.dashboard.service.client.query.SelectionValues.FilterType.ALL;
import static ee.stat.dashboard.service.client.query.SelectionValues.FilterType.ITEM;
import static ee.stat.dashboard.service.widget.widget.WidgetConstants.FILTER_VALUE_CUSTOM_PREFIX;
import static ee.stat.dashboard.util.StatDateUtil.afterOrEquals;
import static ee.stat.dashboard.util.StatDateUtil.beforeOrEquals;
import static java.util.Collections.singletonList;
import static java.util.Comparator.comparing;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.concat;

@Slf4j
@Service
@AllArgsConstructor
class StatDataSharedRequestBuilderService implements StatRequestBuilderInterface {

    private static final String JSON_STAT_2 = "json-stat2";
    private static final List<String> ALL_VALUES = singletonList("*");
    private final TimeService timeService;
    private final FilterValueCustomRepository filterValueCustomRepository;

    @Override
    public StatDataType statDataType() {
        return DATA_SHARED;
    }

    @Override
    public StatApiQuery buildBody(StatRequestConfig config, Long widgetId) {
        StatApiQuery statApiQuery = new StatApiQuery();
        statApiQuery.setQuery(buildFilters(config, widgetId));
        statApiQuery.setResponse(response());
        return statApiQuery;
    }

    private List<SelectionFilter> buildFilters(StatRequestConfig config, Long widgetId) {
        List<SelectionFilter> selectionFilters = new ArrayList<>();
        List<FilterValueCustom> customFilterValues = filterValueCustomRepository.findAllByWidgetOrderByIdAsc(widgetId);
        for (XmlDimension xmlDimension : config.getAllDimensions()) {
            Map<FilterValueCustomType, List<FilterValueCustom>> dimensionCustomFilterValues = customFilterValues.stream()
                .collect(groupingBy(cfv -> {
                    String firstDimensionName = cfv.getFirstDimensionName(ET);
                    String secondDimensionName = cfv.getSecondDimensionName(ET);
                    String dimensionNameEt = xmlDimension.getNameEt();

                    if (firstDimensionName != null && firstDimensionName.equals(dimensionNameEt)
                        && secondDimensionName != null && secondDimensionName.equals(dimensionNameEt)) {
                        return TWO_DIMENSIONAL_SAME_TYPE;
                    } else if (firstDimensionName != null && firstDimensionName.equals(dimensionNameEt) && secondDimensionName == null) {
                        return ONE_DIMENSIONAL;
                    } else if (firstDimensionName != null && firstDimensionName.equals(dimensionNameEt)) {
                        return TWO_DIMENSIONAL_FIRST_MATCH;
                    } else if (secondDimensionName != null && secondDimensionName.equals(dimensionNameEt)) {
                        return TWO_DIMENSIONAL_SECOND_MATCH;
                    } else {
                        return OTHER;
                    }
                }));

            config.getFilters().stream()
                    .filter(f -> f.getNameEt().equals(xmlDimension.getNameEt()))
                    .findAny()
                    .ifPresent(filter -> selectionFilters.add(buildFilter(config, xmlDimension, filter, dimensionCustomFilterValues)));
        }
        return selectionFilters;
    }

    private SelectionFilter buildFilter(StatRequestConfig config, XmlDimension xmlDimension, Filter filter, Map<FilterValueCustomType, List<FilterValueCustom>> dimensionCustomFilterValues) {
        if (filter.isTime()) {
            return timeFilter(config, xmlDimension);
        } else if (filter.getTimePart() != null) {
            return allValues(xmlDimension);
        } else if (filter.getValues().size() == xmlDimension.getValues().size()) {
            return allValues(xmlDimension);
        } else {
            return buildValues(xmlDimension, filter, dimensionCustomFilterValues);
        }
    }

    private SelectionFilter timeFilter(StatRequestConfig config, XmlDimension xmlDimension) {
        TimePeriod timePeriod = decideApiTimePeriod(config);
        setDatesToValues(xmlDimension, timePeriod);
        if (config.getEndDate() != null && config.getStartDate() != null && config.getPeriods() == null) { // start & end date dominate
            return startEndDefined(config, xmlDimension);
        } else if (config.getEndDate() != null && config.getPeriods() != null) { // end date dominates
            return endOnlyDefined(config, xmlDimension, timePeriod);
        } else if (config.getEndDate() == null && config.getStartDate() != null && config.getPeriods() != null) {
            return startOnlyDefined(config, xmlDimension, timePeriod);
        } else if (config.getStartDate() != null && config.getPeriods() == null) {
            return startOnlyWithoutPeriodLimit(config, xmlDimension);
        } else if (config.getPeriods() != null) { // no dates
            return noDatesDefined(config, xmlDimension, timePeriod);
        } else {
            return allValues(xmlDimension);
        }
    }

    private SelectionFilter noDatesDefined(StatRequestConfig config, XmlDimension xmlDimension, TimePeriod timePeriod) {
        XmlDimension dimension = XmlDimension.builder()
                .statId(xmlDimension.getStatId())
                .nameEn(xmlDimension.getNameEn())
                .nameEt(xmlDimension.getNameEt())
                .values(xmlDimension.getValues().stream()
                        .filter(value -> nonNull(value.getDate()))
                        .sorted(comparing(XmlValue::getDate).reversed())
                        .limit(periodLength(config, timePeriod))
                        .collect(toList()))
                .build();
        return itemValues(dimension);
    }

    /**
     * Periods are inclusive 2020 and 1 period should contain 2020 and 2019 (2 values).
     */
    private Integer periodLength(StatRequestConfig config, TimePeriod timePeriod) {
      return timePeriod.isYear() ? config.getYearlyPeriod() : config.getPeriods();
    }

    private SelectionFilter endOnlyDefined(StatRequestConfig config, XmlDimension xmlDimension, TimePeriod timePeriod) {
        XmlDimension dimension = XmlDimension.builder()
                .statId(xmlDimension.getStatId())
                .nameEn(xmlDimension.getNameEn())
                .nameEt(xmlDimension.getNameEt())
                .values(xmlDimension.getValues().stream()
                        .filter(value -> nonNull(value.getDate()))
                        .filter(value -> beforeOrEquals(value.getDate(), config.getEndDate()))
                        .sorted(comparing(XmlValue::getDate).reversed())
                        .limit(periodLength(config, timePeriod))
                        .collect(toList()))
                .build();
        return itemValues(dimension);
    }

    private SelectionFilter startEndDefined(StatRequestConfig config, XmlDimension xmlDimension) {
        XmlDimension dimension = XmlDimension.builder()
                .statId(xmlDimension.getStatId())
                .nameEn(xmlDimension.getNameEn())
                .nameEt(xmlDimension.getNameEt())
                .values(xmlDimension.getValues().stream()
                        .filter(value -> nonNull(value.getDate()))
                        .filter(value -> beforeOrEquals(value.getDate(), config.getEndDate()))
                        .filter(value -> afterOrEquals(value.getDate(), config.getStartDate()))
                        .sorted(comparing(XmlValue::getDate).reversed())
                        .collect(toList()))
                .build();
        return itemValues(dimension);
    }

    private SelectionFilter startOnlyDefined(StatRequestConfig config, XmlDimension xmlDimension, TimePeriod timePeriod) {
        XmlDimension dimension = XmlDimension.builder()
            .statId(xmlDimension.getStatId())
            .nameEn(xmlDimension.getNameEn())
            .nameEt(xmlDimension.getNameEt())
            .values(xmlDimension.getValues().stream()
                .filter(value -> nonNull(value.getDate()))
                .filter(value -> afterOrEquals(value.getDate(), config.getStartDate()))
                .sorted(comparing(XmlValue::getDate))
                .limit(periodLength(config, timePeriod))
                .collect(toList()))
            .build();
        return itemValues(dimension);
    }

    private SelectionFilter startOnlyWithoutPeriodLimit(StatRequestConfig config, XmlDimension xmlDimension) {
        XmlDimension dimension = XmlDimension.builder()
            .statId(xmlDimension.getStatId())
            .nameEn(xmlDimension.getNameEn())
            .nameEt(xmlDimension.getNameEt())
            .values(xmlDimension.getValues().stream()
                .filter(value -> nonNull(value.getDate()))
                .filter(value -> afterOrEquals(value.getDate(), config.getStartDate()))
                .sorted(comparing(XmlValue::getDate))
                .collect(toList()))
            .build();
        return itemValues(dimension);
    }

    private void setDatesToValues(XmlDimension xmlDimension, TimePeriod timePeriod) {
        List<XmlValue> values = xmlDimension.getValues();
        values.forEach(xmlValue -> xmlValue.setDate(timeService.safeGetDate(timePeriod, xmlValue.getStatId())));
        xmlDimension.setValues(values);
    }

    /**
     * week is only supported as time part, existence of time part means year must be used
     */
    private TimePeriod decideApiTimePeriod(StatRequestConfig config) {
        TimePeriod timePeriod = config.getTimePeriod();
        if (timePeriod.isWeek() || config.getTimePartFilter() != null) {
            return YEAR;
        }
        return timePeriod;
    }

    private SelectionFilter itemValues(XmlDimension dimension) {
        List<String> values = dimension.getValues().stream().map(XmlValue::getStatId).collect(toList());
        SelectionValues selectionValues = new SelectionValues();
        selectionValues.setFilter(ITEM.getValue());
        selectionValues.setValues(values);
        SelectionFilter selection = new SelectionFilter();
        selection.setCode(dimension.getStatId());
        selection.setSelection(selectionValues);
        return selection;
    }

    private SelectionFilter allValues(XmlDimension dimension) {
        SelectionValues selectionValues = new SelectionValues();
        selectionValues.setFilter(ALL.getValue());
        selectionValues.setValues(ALL_VALUES);
        SelectionFilter selection = new SelectionFilter();
        selection.setCode(dimension.getStatId());
        selection.setSelection(selectionValues);
        return selection;
    }

    private SelectionFilter buildValues(XmlDimension xmlDimension, Filter filter, Map<FilterValueCustomType, List<FilterValueCustom>> dimensionCustomFilterValues) {
        SelectionFilter selection = new SelectionFilter();
        selection.setCode(xmlDimension.getStatId());
        selection.setSelection(mapValues(xmlDimension, filter, dimensionCustomFilterValues));
        return selection;
    }

    private SelectionValues mapValues(XmlDimension xmlDimension, Filter filter, Map<FilterValueCustomType, List<FilterValueCustom>> dimensionCustomFilterValues) {
        List<String> selectedValues = filter.getValues().stream().map(FilterValue::getValueEt).collect(toList());
        List<String> values = concat(
                xmlDimension.getValues().stream()
                        .filter(xmlValue -> selectedValues.contains(xmlValue.getValueEt()))
                        .map(XmlValue::getStatId),
                getUniqueCustomFilterValueSources(dimensionCustomFilterValues).stream()
        ).distinct().collect(toList());
        SelectionValues selectionValues = new SelectionValues();
        selectionValues.setFilter(ITEM.getValue());
        selectionValues.setValues(values);
        return selectionValues;
    }

    private List<String> getUniqueCustomFilterValueSources(Map<FilterValueCustomType, List<FilterValueCustom>> dimensionCustomFilterValues) {
        Set<String> uniqueValues = new HashSet<>();
        for (Map.Entry<FilterValueCustomType, List<FilterValueCustom>> keyValue : dimensionCustomFilterValues.entrySet()) {
            keyValue.getValue().forEach(fvc -> getValuesForSingleFilterValueCustom(keyValue, fvc, uniqueValues));
        }
        return new ArrayList<>(uniqueValues);
    }

    private void getValuesForSingleFilterValueCustom(Map.Entry<FilterValueCustomType, List<FilterValueCustom>> keyValue, FilterValueCustom filterValueCustom, Set<String> uniqueValues) {
        switch (keyValue.getKey()) {
            case ONE_DIMENSIONAL, TWO_DIMENSIONAL_SAME_TYPE:
                if (selectionValueIsNotCustom(filterValueCustom, 1))
                    uniqueValues.add(filterValueCustom.getSelectionValue1());
                if (selectionValueIsNotCustom(filterValueCustom, 2))
                    uniqueValues.add(filterValueCustom.getSelectionValue2());
                break;
            case TWO_DIMENSIONAL_FIRST_MATCH:
                if (selectionValueIsNotCustom(filterValueCustom, 1))
                    uniqueValues.add(filterValueCustom.getSelectionValue1());
                break;
            case TWO_DIMENSIONAL_SECOND_MATCH:
                if (selectionValueIsNotCustom(filterValueCustom, 2))
                    uniqueValues.add(filterValueCustom.getSelectionValue2());
                break;
          default:
                break;
        }
    }

    private static boolean selectionValueIsNotCustom(FilterValueCustom filterValueCustom, int valueNumber) {
        String selectionValue = valueNumber == 1
                ? filterValueCustom.getSelectionValue1()
                : filterValueCustom.getSelectionValue2();
        return !selectionValue.contains(FILTER_VALUE_CUSTOM_PREFIX);
    }

    private ResponseType response() {
        ResponseType response = new ResponseType();
        response.setFormat(JSON_STAT_2);
        return response;
    }
}
