package ee.stat.dashboard.service.statdata;

import ee.stat.dashboard.model.widget.back.FilterValueCustom;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlDimension;
import java.util.List;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static ee.stat.dashboard.service.widget.widget.WidgetConstants.FILTER_VALUE_CUSTOM_PREFIX;
import static java.lang.Long.parseLong;

@UtilityClass
public class StatDimensionsExplained {

  public static String getCustomExplanation(List<XmlDimension> dimensions, List<FilterValueCustom> widgetCustomFilterValues, FilterValueCustom cvf) {
    String name = cvf.getDisplayNameEt();

    name = getCustomFilterName(dimensions, widgetCustomFilterValues, cvf, name);

    return name;
  }

  @NotNull
  private static String getCustomFilterName(List<XmlDimension> dimensions, List<FilterValueCustom> widgetCustomFilterValues, FilterValueCustom cvf,
                                            String name) {
    String valueEt;

    if (cvf.getSelectionValue1().startsWith(FILTER_VALUE_CUSTOM_PREFIX)) {
      valueEt = getCustomWidget(dimensions, widgetCustomFilterValues, cvf.getSelectionValue1());
    } else {
      valueEt = getDimensionValueEt(dimensions, cvf.getDimension1NameEt(), cvf.getSelectionValue1());
    }

    String valueEt2 = getValueEt2(dimensions, widgetCustomFilterValues, cvf);

    return name + " (" + cvf.getOperation() + ": " + valueEt + "; " + valueEt2 + ")";

  }

  private static @Nullable String getValueEt2(List<XmlDimension> dimensions, List<FilterValueCustom> widgetCustomFilterValues, FilterValueCustom cvf) {
    if (cvf.getDimension2NameEt() == null) {
      return getDimensionValueEt(dimensions, cvf.getDimension1NameEt(), cvf.getSelectionValue2());
    } else if (cvf.getSelectionValue2().startsWith(FILTER_VALUE_CUSTOM_PREFIX)) {
      return getCustomWidget(dimensions, widgetCustomFilterValues, cvf.getSelectionValue2());
    }

    return getDimensionValueEt(dimensions, cvf.getDimension2NameEt(), cvf.getSelectionValue2());
  }

  private static String getCustomWidget(List<XmlDimension> dimensions, List<FilterValueCustom> widgetCustomFilterValues, String customSectionValue) {
    String name = "";
    var filterValueCustomOptional =
        widgetCustomFilterValues.stream().filter(d -> parseLong(customSectionValue.replace(FILTER_VALUE_CUSTOM_PREFIX, "")) == d.getId()).findFirst();

    if (filterValueCustomOptional.isPresent()) {
      var filterValueCustom = filterValueCustomOptional.get();

      name = filterValueCustom.getDisplayNameEt();

      name = getCustomFilterName(dimensions, widgetCustomFilterValues, filterValueCustom, name);
    }

    return name;
  }

  private static String getDimensionValueEt(List<XmlDimension> dimensions, String compareName, String selectionValue) {
    var dimensionOptional = dimensions.stream().filter(d -> d.getNameEt().equals(compareName)).findFirst();
    if (dimensionOptional.isPresent()) {
      var xmlValue = dimensionOptional.get().getValues().stream().filter(d -> d.getStatId().equals(selectionValue)).findFirst();
      if (xmlValue.isPresent()) {
        return xmlValue.get().getValueEt();
      }
    }
    return null;
  }

}
