package ee.stat.dashboard.service.statdata.publishedat;

import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.service.client.StatDataApiClient;
import ee.stat.dashboard.util.StatDateUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ee.stat.dashboard.model.widget.back.StatDataType.DATA_API;
import static java.nio.charset.StandardCharsets.UTF_8;

@Slf4j
@Service
@RequiredArgsConstructor
public class StatDataApiCubeCache implements StatPublishedAtInterface {

    private static final DateTimeFormatter STAT_API_PUBLISHED_PATTERN = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
    private static final ZoneId EUROPE_TALLINN = ZoneId.of("Europe/Tallinn");
    private static final ZoneId UTC = ZoneId.of("UTC");

    private final StatDataApiClient statDataApiClient;
    private Map<String, LocalDateTime> cache = new HashMap<>();

    @Override
    public StatDataType statDataType() {
        return DATA_API;
    }

    @Override
    public LocalDateTime publishedAt(String cube) {
        LocalDateTime published = cache.get(cube);
        if (published == null) {
            log.info("Cube not found in published cache {}", cube);
        }
        return published;
    }

    public void updateCache() {
        LocalDateTime start = LocalDateTime.now();
        try {
            String response = statDataApiClient.getCubesData();

            SAXReader reader = new SAXReader();
            Document body = readBody(response, reader);

            Node eeMenu = body.selectSingleNode("/Menu/Language[@lang='et']");
            List<Node> activeLinks = eeMenu.selectNodes(".//Link[not(contains(@selection, 'Lepetatud_tabelid'))]");

            Map<String, LocalDateTime> map = activeLinks.stream()
                    .collect(HashMap::new,
                            (m, node) -> m.put(
                                    node.selectSingleNode("./TextShort").getStringValue(),
                                    parse(node.selectSingleNode("./Published").getStringValue())),
                            HashMap::putAll);

            log.info("Successfully updated cache: {} nodes, {} cubes ", activeLinks.size(), map.size());

            cache = map;
        } catch (Exception e) {
            log.error("Unexpected error on cache update", e);
        } finally {
            log.info("Cache update took {}", StatDateUtil.toDurationStr(start));
        }
    }

    public static LocalDateTime parse(String value) {
        try {
            return LocalDateTime.parse(value, STAT_API_PUBLISHED_PATTERN)
                    .atZone(EUROPE_TALLINN)
                    .withZoneSameInstant(UTC)
                    .toLocalDateTime();
        } catch (Exception e) {
            log.error("Unexpected error while parsing date {}", value, e);
            return null;
        }
    }

    private Document readBody(String response, SAXReader reader) throws IOException, DocumentException {
        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(response.getBytes(UTF_8))) {
            return reader.read(inputStream);
        }
    }
}
