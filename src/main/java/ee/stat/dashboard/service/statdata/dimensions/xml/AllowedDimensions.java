package ee.stat.dashboard.service.statdata.dimensions.xml;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AllowedDimensions {

    private List<String> allDimensions;
    private List<String> timeDimensions;
}
