package ee.stat.dashboard.service.statdata.dimensions;

import ee.stat.dashboard.service.statdata.dimensions.api.StatMetaResponse;
import ee.stat.dashboard.service.statdata.dimensions.api.Variable;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlDimension;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlValue;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import static ee.stat.dashboard.util.StatStringUtil.cleanUp;
import static java.util.stream.Collectors.toList;
import static lombok.AccessLevel.PRIVATE;
import static org.apache.commons.lang3.BooleanUtils.isTrue;

@NoArgsConstructor(access = PRIVATE)
public class StatDataSharedResponseConverter {

    private static final List<String> TIME_ET_NAMES = List.of("Vaatlusperiood", "Aasta");

    public static List<XmlDimension> toXmlDimensions(StatMetaResponse eeResponse, StatMetaResponse enResponse) {
        return eeResponse.getVariableMap().entrySet().stream()
                .map(entry -> convert(entry, enResponse.getVariableMap()))
                .collect(toList());
    }

    private static XmlDimension convert(Map.Entry<String, Variable> etVariableEntry, Map<String, Variable> enVariableMap) {
        XmlDimension dimension = new XmlDimension();
        dimension.setStatId(etVariableEntry.getKey());
        Variable etVariable = etVariableEntry.getValue();
        if (isTrue(etVariable.getTime()) || TIME_ET_NAMES.contains(etVariable.getCode())) {
            dimension.setTime(true);
        }
        dimension.setNameEt(etVariable.getText());
        Variable enVariable = enVariableMap.get(etVariableEntry.getKey());
        dimension.setNameEn(enVariable.getText());
        List<XmlValue> values = IntStream.range(0, etVariable.getValues().size())
                .mapToObj(i -> map(etVariable, enVariable, i))
                .collect(toList());
        dimension.setValues(values);
        return dimension;
    }

    private static XmlValue map(Variable value, Variable enVariable, int i) {
        XmlValue xmlValue = new XmlValue();
        xmlValue.setStatId(value.getValues().get(i));
        xmlValue.setValueEt(cleanUp(value.getValueTexts().get(i)));
        xmlValue.setValueEn(cleanUp(enVariable.getValueTexts().get(i)));
        return xmlValue;
    }
}
