package ee.stat.dashboard.service.statdata.dimensions;

import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.service.client.StatDataApiClient;
import ee.stat.dashboard.service.statdata.dimensions.api.StatMetaResponse;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlDimension;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static ee.stat.dashboard.model.widget.back.StatDataType.DATA_API;
import static ee.stat.dashboard.model.widget.back.enums.Language.EN;
import static ee.stat.dashboard.model.widget.back.enums.Language.ET;
import static ee.stat.dashboard.service.statdata.dimensions.StatDataSharedResponseConverter.toXmlDimensions;

@Slf4j
@Service
@AllArgsConstructor
class StatDataApiDimensionsService implements StatDimensionsInterface {

    private final StatDataApiClient statDataApiClient;

    @Override
    public StatDataType statDataType() {
        return DATA_API;
    }

    @Override
    public List<XmlDimension> getDimensions(String cube) {
        StatMetaResponse eeResponse = statDataApiClient.getMeta(ET, cube);
        StatMetaResponse enResponse = statDataApiClient.getMeta(EN, cube);
        return toXmlDimensions(eeResponse, enResponse);
    }
}
