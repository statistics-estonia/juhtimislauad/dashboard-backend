package ee.stat.dashboard.service.statdata.dimensions.api;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Map;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

@Getter
@Setter
@NoArgsConstructor
public class StatMetaResponse {

    private String title;
    private List<Variable> variables;

    public Map<String, Variable> getVariableMap(){
        return variables.stream().collect(toMap(Variable::getCode, identity()));
    }
}
