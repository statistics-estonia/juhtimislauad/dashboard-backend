package ee.stat.dashboard.service.statdata.dimensions;

import ee.stat.dashboard.service.StatApiType;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlDimension;

import java.util.List;

public interface StatDimensionsInterface extends StatApiType {

    List<XmlDimension> getDimensions(String code);

}
