package ee.stat.dashboard.service.statdata.request.dto;

import ee.stat.dashboard.model.widget.back.Filter;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlDimension;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

import static ee.stat.dashboard.service.widget.widget.FilterUtil.getTimePart;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.BooleanUtils.isNotTrue;
import static org.apache.commons.lang3.BooleanUtils.isTrue;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StatRequestConfig {

    private Integer periods;
    private LocalDate startDate;
    private LocalDate endDate;
    private TimePeriod timePeriod;
    private String cube;
    private List<Filter> filters;
    private Filter timePartFilter;
    private List<XmlDimension> allDimensions;
    private List<XmlDimension> dataDimensions;
    private List<XmlDimension> timeDimensions;

    public static StatRequestConfig requestConfig(Widget widget, String cube, List<Filter> filters, List<XmlDimension> dimensions) {
        StatRequestConfig config = new StatRequestConfig();
        config.setPeriods(widget.getPeriods());
        config.setEndDate(widget.getEndDate());
        config.setStartDate(widget.getStartDate());
        config.setTimePeriod(widget.getTimePeriod());
        config.setCube(cube);
        config.setFilters(filters);
        config.setTimePartFilter(getTimePart(filters).orElse(null));
        config.setAllDimensions(dimensions);
        config.setDataDimensions(dimensions.stream().filter(x -> isNotTrue(x.getTime())).collect(toList()));
        config.setTimeDimensions(dimensions.stream().filter(x -> isTrue(x.getTime())).collect(toList()));
        return config;
    }

    public static StatRequestConfig requestConfig(WidgetAdminResponse widget, String cube, List<Filter> filters, List<XmlDimension> dimensions) {
        StatRequestConfig config = new StatRequestConfig();
        config.setPeriods(widget.getPeriods());
        config.setEndDate(widget.getEndDate());
        config.setStartDate(widget.getStartDate());
        config.setTimePeriod(widget.getTimePeriod());
        config.setCube(cube);
        config.setFilters(filters);
        config.setTimePartFilter(getTimePart(filters).orElse(null));
        config.setAllDimensions(dimensions);
        config.setDataDimensions(dimensions.stream().filter(x -> isNotTrue(x.getTime())).collect(toList()));
        config.setTimeDimensions(dimensions.stream().filter(x -> isTrue(x.getTime())).collect(toList()));
        return config;
    }

    public static class StatRequestConfigBuilder {

        public StatRequestConfigBuilder allDimensions(List<XmlDimension> allDimensions) {
            this.allDimensions = allDimensions;
            this.dataDimensions = allDimensions.stream().filter(x -> isNotTrue(x.getTime())).collect(toList());
            this.timeDimensions = allDimensions.stream().filter(x -> isTrue(x.getTime())).collect(toList());
            return this;
        }

        public StatRequestConfigBuilder filters(List<Filter> filters) {
            this.filters = filters;
            this.timePartFilter = getTimePart(filters).orElse(null);
            return this;
        }
    }

    public Integer getYearlyPeriod() {
        if (timePeriod.isWeek()) {
            return periods == null ? null : periods / 52 + 1;
        } else if (timePeriod.isMonth()) {
            return periods == null ? null : periods / 12 + 1;
        } else if (timePeriod.isQuarter()) {
            return periods == null ? null : periods / 4 + 1;
        }
        return periods;
    }

    public Integer getPeriods() {
        return periods;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }
}
