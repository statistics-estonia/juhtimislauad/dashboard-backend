package ee.stat.dashboard.service.statdata;

import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.service.client.query.StatApiQuery;
import ee.stat.dashboard.service.statdata.request.StatRequestBuilderInterface;
import ee.stat.dashboard.service.statdata.request.dto.StatRequestConfig;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class StatRequestBuilderManager {

    private final List<StatRequestBuilderInterface> statRequestBuilderService;

    public StatApiQuery buildBody(StatRequestConfig config, StatDataType type, Long widgetId) throws StatMetaFetchException {
        return getStatRequestBuilderService(type).buildBody(config, widgetId);
    }

    private StatRequestBuilderInterface getStatRequestBuilderService(StatDataType type) throws StatMetaFetchException {
        return statRequestBuilderService.stream()
                .filter(i -> i.matches(type))
                .findAny().orElseThrow(() -> new StatMetaFetchException("unknown type: " + type.name()));
    }
}
