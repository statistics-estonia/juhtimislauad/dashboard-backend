package ee.stat.dashboard.service.statdata;

import ee.stat.dashboard.model.widget.back.FilterValueCustom;
import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.repository.FilterValueCustomRepository;
import ee.stat.dashboard.service.statdata.dimensions.StatDimensionsInterface;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlDimension;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlValue;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static ee.stat.dashboard.model.widget.back.enums.Language.ET;
import static ee.stat.dashboard.service.widget.widget.WidgetConstants.FILTER_VALUE_CUSTOM_PREFIX;
import static java.lang.String.format;

@Slf4j
@Service
@AllArgsConstructor
public class StatDimensionsManager {

    private final List<StatDimensionsInterface> statDimensionsService;
    private final FilterValueCustomRepository filterValueCustomRepository;

    public List<XmlDimension> getDimensions(String code, StatDataType type, Long widgetId) throws StatMetaFetchException {
        try {
            List<FilterValueCustom> widgetCustomFilterValues = filterValueCustomRepository.findAllByWidgetOrderByIdAsc(widgetId);
            List<XmlDimension> dimensions = getDimensionService(type).getDimensions(code);
            for (XmlDimension dimension : dimensions) {
                combineValuesForDimension(dimensions, dimension, widgetCustomFilterValues);
            }
            return dimensions;
        } catch (Exception e) {
            log.error("Getting dimensions failed cube: {}, {}", code, e.getMessage(), e);
            throw new StatMetaFetchException(format("Getting dimensions failed cube: %s, %s", code, e.getMessage()), e);
        }
    }

    public List<XmlDimension> getDimensions(String code, StatDataType type) throws StatMetaFetchException {
        try {
            return getDimensionService(type).getDimensions(code);
        } catch (Exception e) {
            log.error("Getting dimensions failed cube: {}, {}", code, e.getMessage(), e);
            throw new StatMetaFetchException(format("Getting dimensions failed cube: %s, %s", code, e.getMessage()), e);
        }
    }

    private StatDimensionsInterface getDimensionService(StatDataType type) throws StatMetaFetchException {
        return statDimensionsService.stream()
                .filter(i -> i.matches(type))
                .findAny().orElseThrow(() -> new StatMetaFetchException("unknown type: " + type.name()));
    }

    private static void combineValuesForDimension(List<XmlDimension> dimensions, XmlDimension dimension, List<FilterValueCustom> widgetCustomFilterValues) {
        List<XmlValue> dimensionCustomValues = widgetCustomFilterValues.stream()
                .filter(cvf -> cvf.getFirstDimensionName(ET).equals(dimension.getNameEt()))
                .map(cvf -> {
                  String hint = StatDimensionsExplained.getCustomExplanation(dimensions, widgetCustomFilterValues, cvf);

                    XmlValue value = new XmlValue();
                    value.setStatId(FILTER_VALUE_CUSTOM_PREFIX + cvf.getId());
                    value.setValueEt(cvf.getDisplayNameEt());
                    value.setValueEn(cvf.getDisplayNameEn());
                    value.setCustom(true);
                    value.setHint(hint);
                    return value;
                }).toList();
        Set<XmlValue> uniqueValues = new LinkedHashSet<>(dimension.getValues());
        uniqueValues.addAll(dimensionCustomValues);
        dimension.setValues(new ArrayList<>(uniqueValues));
    }
}
