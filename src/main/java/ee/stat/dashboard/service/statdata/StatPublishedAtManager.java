package ee.stat.dashboard.service.statdata;

import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.service.statdata.publishedat.StatPublishedAtInterface;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@AllArgsConstructor
public class StatPublishedAtManager {

    private final List<StatPublishedAtInterface> statPublishedAtInterfaces;

    public LocalDateTime publishedAt(String code, StatDataType type) throws StatMetaFetchException {
        return getStatPublishedAtInterfaces(type).publishedAt(code);
    }

    private StatPublishedAtInterface getStatPublishedAtInterfaces(StatDataType type) throws StatMetaFetchException {
        return statPublishedAtInterfaces.stream()
                .filter(i -> i.matches(type))
                .findAny().orElseThrow(() -> new StatMetaFetchException("unknown type: " + type.name()));
    }
}
