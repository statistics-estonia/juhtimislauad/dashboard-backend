package ee.stat.dashboard.service.statdata.dimensions.xml;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ee.stat.dashboard.model.widget.back.enums.Language;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Objects;

import static java.util.Objects.hash;

@Getter
@Setter
@NoArgsConstructor
public class XmlValue {

    private String statId;
    private String valueEt;
    private String valueEn;
    private Boolean custom = false;
    private String hint;

    @JsonIgnore
    private LocalDate date;

    @JsonIgnore
    public String getValue(Language lang) {
        return lang.isEt() ? this.valueEt : this.valueEn;
    }

    @JsonIgnore
    public LocalDate getDate() {
        return date;
    }

    @JsonIgnore
    public void setDate(LocalDate value) {
        this.date = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        XmlValue xmlValue = (XmlValue) obj;
        return Objects.equals(statId, xmlValue.statId);
    }

    @Override
    public int hashCode() {
        return hash(statId);
    }
}
