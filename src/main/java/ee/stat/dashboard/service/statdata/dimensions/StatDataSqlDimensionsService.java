package ee.stat.dashboard.service.statdata.dimensions;

import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.service.client.StatDataSqlClient;
import ee.stat.dashboard.service.statdata.dimensions.api.StatMetaResponse;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlDimension;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static ee.stat.dashboard.model.widget.back.StatDataType.DATA_SQL;
import static ee.stat.dashboard.model.widget.back.enums.Language.EN;
import static ee.stat.dashboard.model.widget.back.enums.Language.ET;
import static ee.stat.dashboard.service.statdata.dimensions.StatDataSharedResponseConverter.toXmlDimensions;

@Slf4j
@Service
@AllArgsConstructor
class StatDataSqlDimensionsService implements StatDimensionsInterface {

    private final StatDataSqlClient statDataSqlClient;

    @Override
    public StatDataType statDataType() {
        return DATA_SQL;
    }

    @Override
    public List<XmlDimension> getDimensions(String cube) {
        StatMetaResponse eeResponse = statDataSqlClient.getMeta(ET, cube);
        StatMetaResponse enResponse = statDataSqlClient.getMeta(EN, cube);
        return toXmlDimensions(eeResponse, enResponse);
    }

}
