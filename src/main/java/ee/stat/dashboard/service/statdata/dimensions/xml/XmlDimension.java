package ee.stat.dashboard.service.statdata.dimensions.xml;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ee.stat.dashboard.model.widget.back.enums.Language;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class XmlDimension {

    private String statId;
    private Boolean time;
    private String nameEt;
    private String nameEn;
    private List<XmlValue> values = new ArrayList<>();
    @JsonIgnore
    private List<Year> years;
    @JsonIgnore
    private List<LocalDate> months;
    @JsonIgnore
    private List<LocalDate> quarters;

    @JsonIgnore
    public void setName(String name, Language lang) {
        if (lang.isEt()) {
            this.nameEt = name;
        } else {
            this.nameEn = name;
        }
    }

    @JsonIgnore
    public String getName(Language lang) {
        return lang.isEt() ? this.nameEt : this.nameEn;
    }

}
