package ee.stat.dashboard.service.dashboard;

import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.repository.WidgetRepository;
import ee.stat.dashboard.service.widget.widget.WidgetConverter;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class DashboardWidgetService {

    private final WidgetRepository widgetRepository;
    private final WidgetConverter widgetConverter;

    public List<WidgetResponse> getWidgets(Dashboard dashboard, Language lang) {
        List<Widget> widgets = widgetRepository.findWidgetByMyDashboard(dashboard.getId());
        return map(widgets, lang);
    }

    public List<WidgetResponse> map(List<Widget> widgets, Language lang) {
        return widgets.stream()
                .filter(w -> w.getStatus().isVisible())
                .map(w -> widgetConverter.mapMinimal(w, lang))
                .filter(Objects::nonNull)
                .collect(toList());
    }
}
