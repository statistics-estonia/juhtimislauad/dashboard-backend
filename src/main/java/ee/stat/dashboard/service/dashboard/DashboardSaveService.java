package ee.stat.dashboard.service.dashboard;

import ee.stat.dashboard.config.security.StatUser;
import ee.stat.dashboard.model.classifier.Element;
import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.dashboard.DashboardStatus;
import ee.stat.dashboard.model.dashboard.DashboardType;
import ee.stat.dashboard.model.dashboard.DashboardUserType;
import ee.stat.dashboard.model.dashboard.DashboardWidget;
import ee.stat.dashboard.model.user.UserDashboard;
import ee.stat.dashboard.model.user.widget.UserWidget;
import ee.stat.dashboard.model.widget.back.WidgetDomain;
import ee.stat.dashboard.repository.DashboardRepository;
import ee.stat.dashboard.repository.DashboardWidgetRepository;
import ee.stat.dashboard.repository.UserDashboardRepository;
import ee.stat.dashboard.repository.UserFilterRepository;
import ee.stat.dashboard.repository.UserFilterValueRepository;
import ee.stat.dashboard.repository.UserWidgetRepository;
import ee.stat.dashboard.repository.WidgetDomainRepository;
import ee.stat.dashboard.service.dashboard.dto.UserDashboardDto;
import ee.stat.dashboard.service.dashboard.dto.UserWidgetsDto;
import ee.stat.dashboard.service.element.ElementService;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static ee.stat.dashboard.util.ErrorCode.MISSING_REGION;
import static ee.stat.dashboard.util.StatListUtil.first;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Slf4j
@Service
@Transactional
@AllArgsConstructor
public class DashboardSaveService {

    private final WidgetDomainRepository widgetDomainRepository;
    private final UserWidgetRepository userWidgetRepository;
    private final UserDashboardRepository userDashboardRepository;
    private final UserFilterValueRepository userFilterValueRepository;
    private final UserFilterRepository userFilterRepository;
    private final DashboardRepository dashboardRepository;
    private final ElementService elementService;
    private final DashboardFinder dashboardFinder;
    private final DashboardWidgetRepository widgetRepository;

    public Long saveWidgetsAndSetVisited(Long dashboardId, UserWidgetsDto dto, StatUser user) {
        Dashboard dashboard = dashboardFinder.dashboardForSaveWidgets(dashboardId, user);
        setVisited(dashboard, user);
        return saveWidgets(dashboard, user, dto);
    }

    public Long saveRegion(Long id, UserDashboardDto dto, StatUser user) {
        if (dto.getRegion() == null) throw new StatBadRequestException(MISSING_REGION);
        Dashboard dashboard = dashboardFinder.findValidDashboardForSaveRegion(id);
        Element region = elementService.findById(dto.getRegion()).orElseThrow(() -> new StatBadRequestException(MISSING_REGION));

        List<UserDashboard> userDashboards = userDashboardRepository.findAllByDashboardAndAndAppUser(dashboard.getId(), user.getId());
        UserDashboard userDashboard = userDashboards.stream().findFirst().orElse(new UserDashboard(user.getId(), dashboard.getId()));
        boolean updatePreferences = regionsHaveChanged(region, userDashboard); //check before setting region
        userDashboard.setRegion(region.getId());
        userDashboardRepository.save(userDashboard);

        if (updatePreferences) {
            userFilterValueRepository.deleteRegionFilterValuesByDashboard(dashboard.getId(), user.getId());
            userFilterRepository.deleteRegionFiltersByDashboard(dashboard.getId(), user.getId());
        }
        return id;
    }

    public Long saveWidgets(Dashboard dashboard, StatUser user, UserWidgetsDto dto) {
        if (dashboard.getUserType().isAdmin()) {
            List<WidgetDomain> widgetDomains = widgetDomainRepository.findAllByDashboard(dashboard.getId());
            if (isNotEmpty(widgetDomains)) {
                List<Long> allowedWidgetIds = getWidgetIds(widgetDomains);

                dto.getSelectedWidgets().removeIf(e -> !allowedWidgetIds.contains(e));

                List<UserWidget> userWidgets = widgetDomains.stream()
                        .map(widgetDomain -> mapFromWidgetDomain(widgetDomain, user, dashboard, dto.getSelectedWidgets()))
                        .collect(toList());
                userWidgetRepository.saveAll(userWidgets);
            }
        } else {
            List<DashboardWidget> dashboardWidgets = widgetRepository.findAllByDashboard(dashboard.getId());
            if (isNotEmpty(dashboardWidgets)) {
                List<UserWidget> userWidgets = dashboardWidgets.stream()
                        .filter(dw -> dto.getSelectedWidgets().contains(dw.getWidget()))
                        .map(dashboardWidget -> mapFromDashboardWidget(dashboardWidget, user, dashboard, dto.getSelectedWidgets()))
                        .collect(toList());
                userWidgetRepository.saveAll(userWidgets);
            }
        }
        return dashboard.getId();
    }

    public void setVisited(Dashboard dashboard, StatUser user) {
        List<UserDashboard> userDashboards = userDashboardRepository.findAllByDashboardAndAndAppUser(dashboard.getId(), user.getId());
        UserDashboard userDashboard = userDashboards.stream().findFirst().orElse(new UserDashboard(user.getId(), dashboard.getId()));
        if (!userDashboard.isVisited()) {
            userDashboard.setVisited(true);
            userDashboardRepository.save(userDashboard);
        }
    }

    public Dashboard findExistingOrCreateNew(StatUser user) {
        List<Dashboard> myDashboards = dashboardRepository.findByAppUser(user.getId());
        if (isNotEmpty(myDashboards)) {
            return first(myDashboards);
        }
        return saveNewUserDashboard(user.getId());
    }

    private List<Long> getWidgetIds(List<WidgetDomain> widgetDomains) {
        return widgetDomains.stream().map(WidgetDomain::getWidget).collect(toList());
    }

    private UserWidget mapFromWidgetDomain(WidgetDomain widgetDomain, StatUser user, Dashboard dashboard, List<Long> selectedWidgets) {
        UserWidget userWidget = existingOrNewWidgetDomain(widgetDomain, user, dashboard);
        userWidget.setSelected(selectedWidgets.contains(widgetDomain.getWidget()));
        int indexOf = selectedWidgets.indexOf(widgetDomain.getWidget());
        userWidget.setOrderNr(indexOf == -1 ? null : indexOf);
        return userWidget;
    }

    private UserWidget mapFromDashboardWidget(DashboardWidget dashboardWidget, StatUser user, Dashboard dashboard, List<Long> selectedWidgets) {
        UserWidget userWidget = existingOrNewDashboardWidget(dashboardWidget, user, dashboard);
        userWidget.setSelected(true);
        int indexOf = selectedWidgets.indexOf(dashboardWidget.getWidget());
        userWidget.setOrderNr(indexOf == -1 ? null : indexOf);
        return userWidget;
    }

    private UserWidget existingOrNewDashboardWidget(DashboardWidget dashboardWidget, StatUser user, Dashboard dashboard) {
        List<UserWidget> userWidgets = userWidgetRepository.findByAppUserAndDashboardAndWidget(user.getId(), dashboard.getId(), dashboardWidget.getWidget());
        return isNotEmpty(userWidgets) ? first(userWidgets) : UserWidget.fromDashboardWidget(user.getId(), dashboardWidget.getWidget(), dashboard.getId(), dashboardWidget.getId());
    }

    private UserWidget existingOrNewWidgetDomain(WidgetDomain widgetDomain, StatUser user, Dashboard dashboard) {
        List<UserWidget> userWidgets = userWidgetRepository.findByAppUserAndDashboardAndWidget(user.getId(), dashboard.getId(), widgetDomain.getWidget());
        return isNotEmpty(userWidgets) ? first(userWidgets) : UserWidget.fromWidgetDomain(user.getId(), widgetDomain.getWidget(), dashboard.getId(), widgetDomain.getId());
    }

    private boolean regionsHaveChanged(Element region, UserDashboard userDashboard) {
        return !regionsHaveNotChanged(region, userDashboard);
    }

    private boolean regionsHaveNotChanged(Element region, UserDashboard userDashboard) {
        return regionsEqual(userDashboard, region) || wholeEstoniaEquals(userDashboard, region);
    }

    public Dashboard saveNewUserDashboard(Long user) {
        Dashboard dashboard = new Dashboard();
        dashboard.setAppUser(user);
        dashboard.setStatus(DashboardStatus.VISIBLE);
        dashboard.setUserType(DashboardUserType.USER);
        dashboard.setType(DashboardType.GLOBAL);
        dashboard.setCreatedAt(LocalDateTime.now());
        return dashboardRepository.save(dashboard);
    }

    private boolean regionsEqual(UserDashboard userDashboard, Element region) {
        return region.getId().equals(userDashboard.getRegion());
    }

    private boolean wholeEstoniaEquals(UserDashboard userDashboard, Element region) {
        return userDashboard.getRegion() == null && region.getId().equals(elementService.getWholeEstonia().getId());
    }
}
