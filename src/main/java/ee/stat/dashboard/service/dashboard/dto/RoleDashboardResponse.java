package ee.stat.dashboard.service.dashboard.dto;

import ee.stat.dashboard.service.element.dto.ClassifierResponse;
import ee.stat.dashboard.service.element.dto.ElementResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RoleDashboardResponse {

    private ClassifierResponse mainRole;
    private List<ElementResponse> subRoles;
    private DashboardResponse dashboard;

    public RoleDashboardResponse(ClassifierResponse mainRole, List<ElementResponse> subRoles) {
        this.mainRole = mainRole;
        this.subRoles = subRoles;
    }
}
