package ee.stat.dashboard.service.dashboard;

import ee.stat.dashboard.config.security.StatUser;
import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.user.UserDashboard;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.repository.UserDashboardRepository;
import ee.stat.dashboard.service.dashboard.dto.DashboardResponse;
import ee.stat.dashboard.service.element.ElementService;
import ee.stat.dashboard.service.element.dto.ElementResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
@AllArgsConstructor
public class DashboardConverter {

    private final ElementService elementService;
    private final UserDashboardRepository userDashboardRepository;

    public List<DashboardResponse> mapSimpleWRegion(List<Dashboard> dashboard, Language lang, StatUser user) {
        return dashboard.stream().map(a -> mapSimpleWRegion(a, lang, user)).collect(toList());
    }

    public List<DashboardResponse> mapSimple(List<Dashboard> dashboard, Language lang) {
        return dashboard.stream().map(a -> mapSimple(a, lang)).collect(toList());
    }

    public DashboardResponse mapSimple(Dashboard dashboard, Language lang) {
        return mapSimpleInner(dashboard, lang);
    }

    public DashboardResponse mapSimpleWRegion(Dashboard dashboard, Language lang, StatUser user) {
        DashboardResponse dto = mapSimpleInner(dashboard, lang);
        mapRegion(dashboard, lang, user, dto);
        return dto;
    }

    private void mapRegion(Dashboard dashboard, Language lang, StatUser user, DashboardResponse dto) {
        if (dashboard.getType().isRegional()) {
            List<ElementResponse> regions = elementService.findAllDashboardRegions(dashboard.getId(), lang);
            if (user != null) {
                setSelectedRegion(dashboard, user, regions);
            } else {
                regions.stream().limit(1).forEach(r -> r.setSelected(true));
            }
            dto.setRegions(regions);
        }
    }

    private DashboardResponse mapSimpleInner(Dashboard dashboard, Language lang) {
        DashboardResponse dto = new DashboardResponse();
        dto.setId(dashboard.getId());
        dto.setLang(lang);
        dto.setCode(dashboard.getCode());
        dto.setName(dashboard.getName(lang));
        dto.setType(dashboard.getType());
        return dto;
    }

    private void setSelectedRegion(Dashboard dashboard, StatUser user, List<ElementResponse> regions) {
        List<UserDashboard> userDashboards = userDashboardRepository.findAllByDashboardAndAndAppUser(dashboard.getId(), user.getId());
        if (isNotEmpty(userDashboards)) {
            regions.stream()
                    .filter(r -> r.getId().equals(userDashboards.get(0).getRegion()))
                    .forEach(r -> r.setSelected(true));
        }
    }
}
