package ee.stat.dashboard.service.dashboard.dto;

import ee.stat.dashboard.model.dashboard.DashboardStatus;
import ee.stat.dashboard.model.dashboard.DashboardType;
import ee.stat.dashboard.model.dashboard.DashboardUserType;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.element.dto.DomainResponse;
import ee.stat.dashboard.service.element.dto.ElementResponse;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class DashboardResponse {

    private Long id;
    private String code;
    private Language lang;
    private String name;
    private DashboardType type;
    private DashboardStatus status;
    private DashboardUserType userType;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    private List<DomainResponse> elements;
    private List<Long> selectedWidgets;
    private List<ElementResponse> regions;
    private List<WidgetResponse> widgets;
    private String mainRoleName;
    private String mainRoleCode;

    private Boolean firstVisit;
}
