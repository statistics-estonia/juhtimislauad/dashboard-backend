package ee.stat.dashboard.service.dashboard;

import ee.stat.dashboard.config.security.StatUser;
import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.service.widget.widget.dto.PinningStrategy;
import ee.stat.dashboard.util.StatBadRequestException;
import ee.stat.dashboard.util.StatNotFoundRequestException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Objects;

import static ee.stat.dashboard.util.ErrorCode.DASHBOARD_IS_HIDDEN;
import static ee.stat.dashboard.util.ErrorCode.DASHBOARD_IS_NOT_EXISTING;
import static ee.stat.dashboard.util.ErrorCode.DASHBOARD_MUST_BE_OF_ADMIN_TYPE;
import static ee.stat.dashboard.util.ErrorCode.DASHBOARD_MUST_BE_REGIONAL;
import static ee.stat.dashboard.util.ErrorCode.MY_DASHBOARD_CANNOT_BE_PINNED;
import static ee.stat.dashboard.util.ErrorCode.MY_DASHBOARD_DOES_NOT_EXIST;

@Service
@AllArgsConstructor
public class DashboardFinder {

    private final DashboardServiceCache dashboardServiceCache;

    public Dashboard findValidDashboardForViewing(Long id) {
        Dashboard dashboard = findById(id);
        mustBeVisible(dashboard);
        return dashboard;
    }

    public Dashboard dashboardForSaveWidgets(Long id, StatUser user) {
        Dashboard dashboard = findById(id);
        mustBeVisible(dashboard);
        ifTypeIsUserUsersMustMatch(user, dashboard);
        return dashboard;
    }

    public Dashboard findValidDashboardForPinning(Long id, PinningStrategy pinning) {
        Dashboard dashboard = findById(id);
        mustBeVisible(dashboard);
        ifTypeIsUserMustBeUnpinOnly(dashboard, pinning);
        return dashboard;
    }

    public Dashboard findValidDashboardForSaveRegion(Long id) {
        Dashboard dashboard = findById(id);
        mustBeVisible(dashboard);
        mustBeRegional(dashboard);
        mustBeAdminType(dashboard);
        return dashboard;
    }

    public Dashboard findById(Long id) {
        return dashboardServiceCache.findById(id)
                .orElseThrow(() -> new StatBadRequestException(DASHBOARD_IS_NOT_EXISTING, "Dashboard id" + id));
    }

    private void ifTypeIsUserMustBeUnpinOnly(Dashboard dashboard, PinningStrategy pinning) {
        if (dashboard.getUserType().isUser() && !pinning.unpin()) {
            throw new StatBadRequestException(MY_DASHBOARD_CANNOT_BE_PINNED, "Dashboard id" + dashboard.getId());
        }
    }

    private void ifTypeIsUserUsersMustMatch(StatUser user, Dashboard dashboard) {
        if (dashboard.getUserType().isUser()) {
            if (user == null || !Objects.equals(user.getId(), dashboard.getAppUser())) {
                throw new StatNotFoundRequestException(MY_DASHBOARD_DOES_NOT_EXIST);
            }
        }
    }

    private void mustBeAdminType(Dashboard dashboard) {
        if (dashboard.getUserType().isUser()) {
            throw new StatBadRequestException(DASHBOARD_MUST_BE_OF_ADMIN_TYPE, "Dashboard id" + dashboard.getId());
        }
    }

    private void mustBeRegional(Dashboard dashboard) {
        if (!dashboard.getType().isRegional()) {
            throw new StatBadRequestException(DASHBOARD_MUST_BE_REGIONAL, "Dashboard id" + dashboard.getId());
        }
    }

    private void mustBeVisible(Dashboard dashboard) {
        if (dashboard.getStatus().isHidden()) {
            throw new StatBadRequestException(DASHBOARD_IS_HIDDEN, "Dashboard id " + dashboard.getId());
        }
    }
}
