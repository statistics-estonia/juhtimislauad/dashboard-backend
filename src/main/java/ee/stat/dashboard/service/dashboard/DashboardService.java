package ee.stat.dashboard.service.dashboard;


import ee.stat.dashboard.config.security.StatUser;
import ee.stat.dashboard.model.classifier.ClassifierCode;
import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.dashboard.dto.DashboardResponse;
import ee.stat.dashboard.service.dashboard.dto.RoleDashboardResponse;
import ee.stat.dashboard.service.element.DomainService;
import ee.stat.dashboard.service.user.UserDashboardService;
import ee.stat.dashboard.service.user.UserWidgetService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(readOnly = true)
public class DashboardService {

    private final DomainService domainService;
    private final UserWidgetService userWidgetService;
    private final DashboardConverter dashboardConverter;
    private final DashboardServiceCache dashboardServiceCache;
    private final UserDashboardService userDashboardService;
    private final DashboardWidgetService dashboardWidgetService;
    private final DashboardFinder dashboardFinder;
    private final RoleDashboardService roleDashboardService;

    public List<DashboardResponse> findAll(Language lang, StatUser user) {
        return dashboardConverter.mapSimpleWRegion(dashboardServiceCache.findAllByLevel(), lang, user);
    }

    public DashboardResponse findById(Long id, Language lang, StatUser user) {
        Dashboard dashboard = dashboardFinder.findValidDashboardForViewing(id);
        return map(dashboard, lang, user);
    }

    public DashboardResponse findByIdWithMainRole(Long id, Language lang, StatUser user) {
        DashboardResponse dashboardResponse = map(dashboardFinder.findValidDashboardForViewing(id), lang, user);
        List<RoleDashboardResponse> roles = roleDashboardService.findAll(Language.getValue(lang.toString()));

        return updateDashboardWithRoles(dashboardResponse, id, roles);
    }

    public DashboardResponse updateDashboardWithRoles(DashboardResponse dashboardResponse, Long id, List<RoleDashboardResponse> roles) {
        for (RoleDashboardResponse role : roles) {
            if (role.getMainRole() != null) {
                if (ClassifierCode.EHAK.equals(role.getMainRole().getCode())) {
                    dashboardResponse.setMainRoleCode(role.getMainRole().getCode().toString());
                    dashboardResponse.setMainRoleName(role.getMainRole().getName());
                }

                if (role.getSubRoles() != null) {
                    for (var subRole : role.getSubRoles()) {
                        if (subRole.getDashboards() != null && subRole.getDashboards().stream().anyMatch(dashboard -> id.equals(dashboard.getId()))) {
                            dashboardResponse.setMainRoleCode(role.getMainRole().getCode().toString());
                            dashboardResponse.setMainRoleName(role.getMainRole().getName());
                            return dashboardResponse;
                        }
                    }
                }
            }
        }
        return dashboardResponse;
    }

    public DashboardResponse map(Dashboard dashboard, Language lang, StatUser user) {
        DashboardResponse dto = dashboardConverter.mapSimpleWRegion(dashboard, lang, user);
        if (dashboard.getUserType().isAdmin()) {
            dto.setElements(domainService.loadDomainsAndWidgets(dashboard.getId(), lang));
        } else {
            dto.setWidgets(dashboardWidgetService.getWidgets(dashboard, lang));
        }
        if (user != null) {
            dto.setSelectedWidgets(userWidgetService.getSelectedWidgets(dashboard, user));
            if (userDashboardService.getFirstVisit(dashboard, user)) {
                dto.setFirstVisit(true);
            }
        }
        return dto;
    }
}
