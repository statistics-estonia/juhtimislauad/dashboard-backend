package ee.stat.dashboard.service.dashboard;

import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.dashboard.DashboardStatus;
import ee.stat.dashboard.model.dashboard.DashboardUserType;
import ee.stat.dashboard.repository.DashboardRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class DashboardServiceCache {

    private final DashboardRepository dashboardRepository;

    @Cacheable(value = "DashboardServiceCache_findById", key = "#dashboardId")
    public Optional<Dashboard> findById(Long dashboardId) {
        return dashboardRepository.findById(dashboardId);
    }

    @Cacheable("DashboardServiceCache_findAllByLevel")
    public List<Dashboard> findAllByLevel() {
        return dashboardRepository.findAllByStatusAndUserType(DashboardStatus.VISIBLE, DashboardUserType.ADMIN);
    }
}
