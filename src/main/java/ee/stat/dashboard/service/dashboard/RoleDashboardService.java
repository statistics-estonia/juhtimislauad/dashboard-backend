package ee.stat.dashboard.service.dashboard;

import ee.stat.dashboard.controller.element.dto.ElementStrategy;
import ee.stat.dashboard.controller.element.dto.ParentStrategy;
import ee.stat.dashboard.model.classifier.Classifier;
import ee.stat.dashboard.model.classifier.ClassifierCode;
import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.repository.DashboardRepository;
import ee.stat.dashboard.service.dashboard.dto.DashboardResponse;
import ee.stat.dashboard.service.dashboard.dto.RoleDashboardResponse;
import ee.stat.dashboard.service.element.ClassifierService;
import ee.stat.dashboard.service.element.ClassifierServiceCache;
import ee.stat.dashboard.service.element.ElementService;
import ee.stat.dashboard.service.element.dto.ClassifierResponse;
import ee.stat.dashboard.service.element.dto.ElementResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static ee.stat.dashboard.service.element.EhakLevels.COUNTRY_AND_COUNTY_LIST;
import static ee.stat.dashboard.service.element.dto.RoleStrategy.HAS_DASHBOARDS;
import static ee.stat.dashboard.util.StatListUtil.first;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;

@Service
@AllArgsConstructor
@Transactional(readOnly = true)
public class RoleDashboardService {

    private final ElementService elementService;
    private final DashboardRepository dashboardRepository;
    private final ClassifierServiceCache classifierServiceCache;
    private final DashboardConverter dashboardConverter;
    private final ClassifierService classifierService;

    public List<RoleDashboardResponse> findAll(Language lang) {
        return classifierServiceCache.findByCodeIn(ClassifierCode.getRoleClassifiers()).stream()
                .map(classifier -> mapToResponse(classifier, lang))
                .collect(toList());
    }

    private RoleDashboardResponse mapToResponse(Classifier classifier, Language lang) {
        if (classifier.getCode() == ClassifierCode.EHAK) {
            return kovResponse(classifier, lang);
        }
        return subroleResponse(classifier, lang);
    }

    private RoleDashboardResponse kovResponse(Classifier clf, Language lang) {
        ClassifierResponse classifier = classifierService.mapSimple(clf, lang);
        List<ElementResponse> elements = elementService.findAll(clf.getCode(), lang, COUNTRY_AND_COUNTY_LIST, null, null, ElementStrategy.FLAT, ParentStrategy.EXCLUDE);
        DashboardResponse dashboard = getDashboard(clf, lang);
        return new RoleDashboardResponse(classifier, elements, dashboard);
    }

    private RoleDashboardResponse subroleResponse(Classifier clf, Language lang) {
        ClassifierResponse classifier = classifierService.mapSimple(clf, lang);
        List<ElementResponse> elements = elements(clf.getCode(), lang);
        return new RoleDashboardResponse(classifier, elements);
    }

    private DashboardResponse getDashboard(Classifier clf, Language lang) {
        List<Dashboard> dashboards = dashboardRepository.findByRoleClassifier(clf.getId(), lang);
        if (isEmpty(dashboards)) {
            return null;
        }
        return dashboardConverter.mapSimple(first(dashboards), lang);
    }

    private List<ElementResponse> elements(ClassifierCode code, Language lang) {
        List<ElementResponse> elementsWithDash = elementService.findAll(code, HAS_DASHBOARDS, lang);
        for (ElementResponse element : elementsWithDash) {
            List<Dashboard> dashboards = dashboardRepository.findByRoleElement(element.getId(), lang);
            element.setDashboards(dashboardConverter.mapSimple(dashboards, lang));
        }
        List<ElementResponse> returnedElements = new ArrayList<>(elementsWithDash);

        List<ElementResponse> withoutDashboards = new ArrayList<>();
        for (ElementResponse element : elementService.findAll(code, lang)) {
            if (elementsWithDash.stream().noneMatch(e-> e.getId().equals(element.getId()))){
                withoutDashboards.add(element);
            }
        }
        returnedElements.addAll(withoutDashboards.stream()
                .sorted(Comparator.comparing(ElementResponse::getName))
                .collect(toList()));
        return returnedElements;
    }
}
