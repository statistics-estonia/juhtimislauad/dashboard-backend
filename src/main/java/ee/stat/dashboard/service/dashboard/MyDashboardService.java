package ee.stat.dashboard.service.dashboard;

import ee.stat.dashboard.config.security.StatUser;
import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.dashboard.dto.DashboardResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional
@AllArgsConstructor
public class MyDashboardService {

    private final DashboardService dashboardService;
    private final DashboardSaveService dashboardSaveService;

    public DashboardResponse findMyDashboard(Language lang, StatUser user) {
        Dashboard dashboard = dashboardSaveService.findExistingOrCreateNew(user);
        return dashboardService.map(dashboard, lang, user);
    }
}
