package ee.stat.dashboard.service.dashboard.dto;

public enum DashboardLoadStrategy {
    LOAD_CHILDREN, SKIP_CHILDREN;

    public boolean loadChildren(){
        return this == LOAD_CHILDREN;
    }
}
