package ee.stat.dashboard.service.dashboard.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class UserWidgetsDto {

    private String language;
    private List<Long> selectedWidgets;

    public UserWidgetsDto(List<Long> selectedWidgets) {
        this.selectedWidgets = selectedWidgets;
    }
}
