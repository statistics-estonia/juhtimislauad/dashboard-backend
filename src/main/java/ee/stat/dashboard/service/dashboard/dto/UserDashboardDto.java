package ee.stat.dashboard.service.dashboard.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserDashboardDto {

    private String language;
    private Long region;

    public UserDashboardDto(Long region) {
        this.region = region;
    }
}
