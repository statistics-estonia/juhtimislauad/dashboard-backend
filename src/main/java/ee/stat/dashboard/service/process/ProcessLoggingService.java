package ee.stat.dashboard.service.process;

import ee.stat.dashboard.model.process.ProcessLog;
import ee.stat.dashboard.model.process.StatLog;
import ee.stat.dashboard.model.process.enums.LogStatus;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.repository.StatLogRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static ee.stat.dashboard.util.StatStringUtil.cutTo;
import static java.time.LocalDateTime.now;
import static org.springframework.transaction.annotation.Propagation.REQUIRES_NEW;

@Slf4j
@Service
@AllArgsConstructor
@Transactional(propagation = REQUIRES_NEW)
public class ProcessLoggingService {

    private final StatLogRepository statLogRepository;

    public void log(ProcessLog processLog, Widget widget, LogStatus status, String message, String cube) {
        logInner(processLog, widget, status, message, cube, null);
    }

    public void log(ProcessLog processLog, Widget widget, LogStatus status, String message, String cube, Throwable e) {
        logInner(processLog, widget, status, message, cube, e);
    }

    private void logInner(ProcessLog processLog, Widget widget, LogStatus status, String message, String cube, Throwable e) {
        StatLog statLog = new StatLog();
        statLog.setProcessLog(processLog.getId());
        statLog.setMessage(cutTo(message, 400));
        statLog.setStatus(status);
        statLog.setTime(now());
        statLog.setWidget(widget != null ? widget.getId() : null);
        statLog.setCube(cube);
        if (e != null) {
            statLog.setError(e.getMessage());
        }
        if (status.isInfo()) {
            log.info(message);
        } else {
            log.error(message, e);
        }
        statLogRepository.save(statLog);
    }
}
