package ee.stat.dashboard.service.process;

import ee.stat.dashboard.model.process.ProcessLog;
import ee.stat.dashboard.model.widget.back.GraphType;
import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.model.widget.back.Widget;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import static ee.stat.dashboard.model.process.enums.LogStatus.ERROR;
import static ee.stat.dashboard.model.process.enums.LogStatus.INFO;
import static java.lang.String.format;

@Service
@AllArgsConstructor
public class ProcessLoggingHelper {

    private final ProcessLoggingService log;

    public void analysingWidget(ProcessLog processLog, Widget widget, String cube, StatDataType dataApi) {
        log.log(processLog, widget, INFO, format("Analysing widget id: %s, name: %s, cube: %s, source: %s",
                widget.getId(), widget.getNameEt(), cube, dataApi.name()), cube);
    }

    public void analysingGraphType(ProcessLog processLog, Widget widget, GraphType graphType, String cube) {
        log.log(processLog, widget, INFO, format("Analysing widget id: %s, graph data for %s",
                widget.getId(), graphType.getType().name()), cube);
    }

    public void responsesAreDifferent(ProcessLog processLog, Widget widget, String cube) {
        log.log(processLog, widget, INFO, "Stat data is different, updating", cube);
    }

    public void responsesAreSame(ProcessLog processLog, Widget widget, String cube) {
        log.log(processLog, widget, INFO, "Stat data is the same, nothing to update", cube);
    }

    public void requestChanged(ProcessLog processLog, Widget widget, String newUrl, String existingUrl, String cube) {
        log.log(processLog, widget, INFO, format("Request has changed, fetching new data. %nBefore: %s %nAfter: %s",
                existingUrl, newUrl), cube);
    }

    public void widgetHasChanged(ProcessLog processLog, Widget widget, String cube) {
        log.log(processLog, widget, INFO, format("Widget name: %s, id: %s has been marked for recalc",
                widget.getNameEt(), widget.getId()), cube);
    }

    public void publishedAtHasChanged(ProcessLog processLog, Widget widget, String cube) {
        log.log(processLog, widget, INFO, format("Cube %s publishedAt has changed", cube), cube);
    }

    public void publishedAtHasNotChanged(ProcessLog processLog, Widget widget, String cube) {
        log.log(processLog, widget, INFO, format("Cube %s publishedAt has not changed", cube), cube);
    }

    public void publishedAtIsMissing(ProcessLog processLog, Widget widget, String cube) {
        log.log(processLog, widget, ERROR, format("Cube %s publishedAt is missing", cube), cube);
    }

    public void forceRebuild(ProcessLog processLog, Widget widget, String cube) {
        log.log(processLog, widget, ERROR, format("Cube %s forceRebuild is on, did diagram creation fail last time ?", cube), cube);
    }
}
