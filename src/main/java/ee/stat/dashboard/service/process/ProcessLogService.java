package ee.stat.dashboard.service.process;

import ee.stat.dashboard.model.process.Process;
import ee.stat.dashboard.model.process.ProcessLog;
import ee.stat.dashboard.repository.ProcessLogRepository;
import ee.stat.dashboard.service.statsync.shared.dto.ProcessResult;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

import static ee.stat.dashboard.model.process.enums.ProcessLogStatus.ERROR;
import static ee.stat.dashboard.model.process.enums.ProcessLogStatus.FINISHED;
import static ee.stat.dashboard.model.process.enums.ProcessLogStatus.STARTED;
import static org.springframework.transaction.annotation.Propagation.REQUIRES_NEW;

@Service
@AllArgsConstructor
@Transactional(propagation = REQUIRES_NEW)
public class ProcessLogService {

    private final ProcessLogRepository processLogRepository;

    public ProcessLog start(Process process) {
        ProcessLog processLog = new ProcessLog();
        processLog.setProcess(process);
        processLog.setStartTime(LocalDateTime.now());
        processLog.setStatus(STARTED);
        processLogRepository.save(processLog);
        return processLog;
    }

    public void finished(ProcessLog processLog, ProcessResult result) {
        processLog.setEndTime(LocalDateTime.now());
        processLog.setStatus(FINISHED);
        processLog.setAnalysed(result.getAnalysed());
        processLog.setUnchanged(result.getUnchanged());
        processLog.setChanged(result.getChanged());
        processLog.setFailed(result.getFailed());
        processLogRepository.save(processLog);
    }

    public void fail(ProcessLog processLog) {
        processLog.setEndTime(LocalDateTime.now());
        processLog.setStatus(ERROR);
        processLogRepository.save(processLog);
    }
}
