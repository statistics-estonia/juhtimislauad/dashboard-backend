package ee.stat.dashboard.service.widget.coordinates;

import ee.stat.dashboard.model.json.Source;
import ee.stat.dashboard.model.widget.back.Filter;
import ee.stat.dashboard.model.widget.back.FilterValue;
import ee.stat.dashboard.model.widget.back.FilterValueCustom;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.repository.StatDataApiRepository;
import ee.stat.dashboard.repository.StatDataSqlRepository;
import ee.stat.dashboard.service.statdata.StatDimensionsManager;
import ee.stat.dashboard.service.statdata.StatMetaFetchException;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlValue;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.RequiredArgsConstructor;
import no.ssb.jsonstat.v2.Dimension;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static ee.stat.dashboard.model.json.Source.STAT_DATA_API;
import static ee.stat.dashboard.model.widget.back.StatDataType.convertFromSource;
import static ee.stat.dashboard.service.widget.coordinates.CoordinateUtil.findDimensionIndex;
import static ee.stat.dashboard.service.widget.widget.WidgetConstants.FILTER_VALUE_CUSTOM_PREFIX;
import static ee.stat.dashboard.util.ErrorCode.FILTER_ADMIN_NO_VALUE_FOR_CONSTRAINT;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
@RequiredArgsConstructor
public class CoordinateConstraintCleaner {

    private final StatDimensionsManager statDimensionsManager;
    private final StatDataApiRepository statDataApiRepository;
    private final StatDataSqlRepository statDataSqlRepository;

    private String cubeCode;

    // constraints must only have one value and since constraint filter values are ignored during datapoint mapping,
    // it is necessary to delete all other datapoints from each constraint dimension that were only used for calculations
    public void removeExcessConstraintDatapoints(List<Filter> filters, List<Dimension> dimensions, Language lang, List<FilterValueCustom> selectedCustomFilterValues, Source source, Map<List<String>, Number> values, Long graphTypeId) throws StatMetaFetchException {
        List<Filter> constraints = filters.stream()
                .filter(f -> f.getType().isConstraint())
                .collect(toList());

        if (isNotEmpty(constraints)) {
            this.cubeCode = STAT_DATA_API.equals(source)
                    ? statDataApiRepository.findByGraphTypeId(graphTypeId).getCube()
                    : statDataSqlRepository.findByGraphTypeId(graphTypeId).getCube();
        }

        for (Filter filter : constraints) {
            findAndRemoveDatapoints(dimensions, lang, selectedCustomFilterValues, source, values, filter);
        }
    }

    private void findAndRemoveDatapoints(List<Dimension> dimensions, Language lang, List<FilterValueCustom> selectedCustomFilterValues, Source source, Map<List<String>, Number> values, Filter filter) throws StatMetaFetchException {
        int dimIndex = findDimensionIndex(dimensions, filter.getName(lang));
        if (dimIndex > 0) {
            String valueToKeep;
            if (filter.getValues().isEmpty()) { // no custom filter values
                valueToKeep = keepCustomConstraintValue(selectedCustomFilterValues, filter);
            } else {
                valueToKeep = keepNativeConstraintValue(source, filter);
            }
            removeDatapoints(values, dimIndex, valueToKeep);
        }
    }

    private static String keepCustomConstraintValue(List<FilterValueCustom> selectedCustomFilterValues, Filter filter) {
        FilterValueCustom filterValueCustom = selectedCustomFilterValues.stream()
                .filter(f -> f.getDimension1NameEt().equals(filter.getNameEt())).findFirst()
                .orElseThrow(() -> new StatBadRequestException(FILTER_ADMIN_NO_VALUE_FOR_CONSTRAINT));
        return FILTER_VALUE_CUSTOM_PREFIX + filterValueCustom.getId();
    }

    private String keepNativeConstraintValue(Source source, Filter filter) throws StatMetaFetchException {
        String selectedValue = filter.getValues().stream().map(FilterValue::getValueEt).findFirst()
                .orElseThrow(() -> new StatBadRequestException(FILTER_ADMIN_NO_VALUE_FOR_CONSTRAINT));
        return getSelectedValueStatId(source, filter, selectedValue);
    }

    private String getSelectedValueStatId(Source source, Filter filter, String selectedValue) throws StatMetaFetchException {
        return statDimensionsManager.getDimensions(this.cubeCode, convertFromSource(source)).stream()
                .filter(d -> d.getNameEt().equals(filter.getNameEt()))
                .findFirst()
                .map(d -> d.getValues().stream()
                        .filter(v -> v.getValueEt().equals(selectedValue))
                        .findFirst()
                        .map(XmlValue::getStatId)
                        .orElseThrow(() -> new StatBadRequestException(FILTER_ADMIN_NO_VALUE_FOR_CONSTRAINT)))
                .orElseThrow(() -> new StatBadRequestException(FILTER_ADMIN_NO_VALUE_FOR_CONSTRAINT));
    }

    private static void removeDatapoints(Map<List<String>, Number> values, int dimIndex, String valueToKeep) {
        Iterator<Map.Entry<List<String>, Number>> iterator = values.entrySet().iterator();
        while (iterator.hasNext()) {
            String dimensionValue = iterator.next().getKey().get(dimIndex);
            if (!valueToKeep.equals(dimensionValue)) {
                iterator.remove();
            }
        }
    }
}
