package ee.stat.dashboard.service.widget.coordinates;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ee.stat.dashboard.model.classifier.ClassifierCode;
import ee.stat.dashboard.model.json.CoordinatedResponse;
import ee.stat.dashboard.model.json.DataPoint;
import ee.stat.dashboard.model.json.Source;
import ee.stat.dashboard.model.widget.back.Filter;
import ee.stat.dashboard.model.widget.back.FilterValue;
import ee.stat.dashboard.model.widget.back.FilterValueCustom;
import ee.stat.dashboard.model.widget.back.FilterValueCustomForFilter;
import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.repository.FilterValueCustomForFilterRepository;
import ee.stat.dashboard.repository.FilterValueCustomRepository;
import ee.stat.dashboard.service.StatApiType;
import ee.stat.dashboard.service.element.ElementServiceCache;
import ee.stat.dashboard.service.statdata.StatMetaFetchException;
import ee.stat.dashboard.service.widget.coordinates.dto.FiltersConfig;
import ee.stat.dashboard.service.widget.coordinates.exception.IncorrectLabelException;
import ee.stat.dashboard.service.widget.coordinates.exception.IncorrectValueException;
import ee.stat.dashboard.service.widget.coordinates.exception.NullValueException;
import ee.stat.dashboard.service.widget.coordinates.exception.RegionFilterException;
import ee.stat.dashboard.service.widget.coordinates.exception.TimeFilterException;
import ee.stat.dashboard.service.widget.coordinates.exception.TimePartCalculationException;
import ee.stat.dashboard.service.widget.coordinates.exception.TimePartMappingException;
import ee.stat.dashboard.service.widget.coordinates.exception.UnknownFilterException;
import ee.stat.dashboard.service.widget.coordinates.exception.UnknownFilterValueException;
import ee.stat.dashboard.service.widget.coordinates.time.TimeService;
import ee.stat.dashboard.service.widget.coordinates.timepart.TimePartLogic;
import ee.stat.dashboard.service.widget.coordinates.timepart.TimePartService;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import no.ssb.jsonstat.v2.Dataset;
import no.ssb.jsonstat.v2.DatasetBuildable;
import no.ssb.jsonstat.v2.Dimension;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static ee.stat.dashboard.model.widget.back.StatDataType.DATA_SHARED;
import static ee.stat.dashboard.service.widget.coordinates.CoordinateUtil.setDimensions;
import static ee.stat.dashboard.service.widget.coordinates.StatJsonDatasetCustomValueCalculator.calculateDatapointsForCustomFilterValues;
import static ee.stat.dashboard.service.widget.widget.FilterUtil.getConstraintNameEt;
import static ee.stat.dashboard.service.widget.widget.FilterUtil.getRegion;
import static ee.stat.dashboard.service.widget.widget.FilterUtil.getRegularFilter;
import static ee.stat.dashboard.service.widget.widget.FilterUtil.getTimePart;
import static ee.stat.dashboard.service.widget.widget.FilterUtil.timeFilterOrThrow;
import static ee.stat.dashboard.service.widget.widget.WidgetConstants.FILTER_VALUE_CUSTOM_PREFIX;
import static ee.stat.dashboard.util.ErrorCode.WIDGET_ADMIN_FILTER_VALUE_MISSING;
import static ee.stat.dashboard.util.StatStringUtil.cleanUp;
import static java.lang.Long.parseLong;
import static java.lang.String.format;
import static java.lang.String.valueOf;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service
@AllArgsConstructor
public class StatJson2ToCoordinates implements StatApiType {

    private final ObjectMapper objectMapper;
    private final ElementServiceCache elementServiceCache;
    private final TimePartService timePartService;
    private final TimeService timeService;
    private final CoordinateConstraintCleaner coordinateConstraintCleaner;
    private final FilterValueCustomRepository filterValueCustomRepository;
    private final FilterValueCustomForFilterRepository filterValueCustomForFilterRepository;

    @Override
    public StatDataType statDataType() {
        return DATA_SHARED;
    }

    public CoordinatedResponse convert(String response, Long graphTypeId, List<Filter> filters, TimePeriod timePeriod, Language lang, Source source) throws StatMetaFetchException {
        Dataset dataset = getDataset(response).build();
        List<DataPoint> dataPoints = new ArrayList<>();

        FiltersConfig filtersConfig = filtersConfig(filters, timePeriod, lang);

        List<Dimension> dimensions = new ArrayList<>(dataset.getDimension().values());
        Map<List<String>, Number> values = new HashMap<>(dataset.asMap());

        List<Long> filterIds = filters.stream().map(Filter::getId).collect(toList());
        List<FilterValueCustom> selectedCustomFilterValues = filterValueCustomForFilterRepository.findAllByFilterIdInOrderByFilterValueCustomId(filterIds).stream()
                .map(FilterValueCustomForFilter::getFilterValueCustom)
                .collect(toList());
        List<FilterValueCustom> customFilterValues = filterValueCustomRepository.findAllByWidgetOrderByIdAsc(filters.getFirst().getWidget());

        calculateDatapointsForCustomFilterValues(customFilterValues, dimensions, lang, values);
        coordinateConstraintCleaner.removeExcessConstraintDatapoints(filters, dimensions, lang, selectedCustomFilterValues, source, values, graphTypeId);

        for (Map.Entry<List<String>, Number> keyValue : values.entrySet()) {
            try {
                dataPoints.add(mapToPoint(keyValue, dimensions, filtersConfig, lang, source, customFilterValues));
            } catch (TimeFilterException | TimePartMappingException | TimePartCalculationException |
                     RegionFilterException | IncorrectLabelException | IncorrectValueException |
                     UnknownFilterException | UnknownFilterValueException | NullValueException e) {
                log.info("Observation is ignored. {}: {}. Observation {} with value {}.", rule(e), e.getMessage(),
                        keyValue.getKey(), keyValue.getValue());
            }
        }
        if (dataPoints.isEmpty()) {
            List<String> statFilters = dimensions.stream().map(Dimension::getLabel).filter(Optional::isPresent).map(Optional::get).collect(toList());
            List<String> collectEt = filters.stream().map(Filter::getNameEt).collect(toList());
            List<String> collectEn = filters.stream().map(Filter::getNameEn).collect(toList());
            List<String> dashValuesEt = filters.stream().map(Filter::getValues).flatMap(Collection::stream).map(FilterValue::getValueEt).collect(toList());
            List<String> dashValuesEn = filters.stream().map(Filter::getValues).flatMap(Collection::stream).map(FilterValue::getValueEn).collect(toList());
            List<String> ehak = filtersConfig.getEhakNames();
            Map.Entry<List<String>, Number> entry = values.entrySet().iterator().next();
            log.error("Datapoints are empty \n" +
                            "[api] {} \n" +
                            "[nr-of-values] {} \n" +
                            "[ee] {} \n" +
                            "[en] {} \n" +
                            "[ee-val] {} \n" +
                            "[en-val] {} \n" +
                            "[single-entry] {} {} \n" +
                            "[ehak] {} \n",
                    statFilters, values.size(),
                    collectEt, collectEn,
                    dashValuesEt, dashValuesEn,
                    entry != null ? entry.getKey() : null,
                    entry != null ? entry.getValue() : null,
                    ehak);
        }

        return new CoordinatedResponse(dataPoints, source);
    }

    private DatasetBuildable getDataset(String response) {
        try {
            return objectMapper.readValue(response, DatasetBuildable.class);
        } catch (JsonProcessingException e) {
            log.error("Json error ", e);
            throw new RuntimeException("Dataset reading failed");
        }
    }

    private DataPoint mapToPoint(Map.Entry<List<String>, Number> keyValue, List<Dimension> observations, FiltersConfig filtersConfig, Language lang, Source source, List<FilterValueCustom> customFilterValues) throws TimePartMappingException, TimePartCalculationException, TimeFilterException, RegionFilterException, IncorrectLabelException, IncorrectValueException, UnknownFilterException, NullValueException, UnknownFilterValueException {
        List<String> code = keyValue.getKey();
        DataPoint dataPoint = new DataPoint();
        dataPoint.setSource(source);
        BigDecimal value = toValue(keyValue.getValue());
        if (value == null) {
            // stat json returns empty values as nulls, we will ignore these here and if necessary build them up later
            throw new NullValueException("there is no value");
        }
        dataPoint.setValue(value);
        int countOfUsedDimensions = 0;
        for (int i = 0; i < code.size(); i++) {
            String valueDimCode = code.get(i);
            Dimension dimension = observations.get(i);

            String dimensionName = getDimensionName(dimension);

            if (filtersConfig.getConstraintFilterNames().get(lang).contains(dimensionName)) { // constraint
                // constraint filter values are ignored
            } else if (dimensionName.equals(filtersConfig.getTimeFilter().getName(lang))) { // time
                timeService.setTime(dataPoint, filtersConfig, valueDimCode);
            } else if (filtersConfig.hasTimePart() && dimensionName.equals(filtersConfig.getTimePart().getName(lang))) { // time part
                timePartService.setTimePart(dataPoint, dimension, filtersConfig.getTimePart(), valueDimCode, lang);
            } else if (filtersConfig.hasRegion() && dimensionName.equals(filtersConfig.getRegionFilter().getName(lang))) { // region
                Filter filter = filtersConfig.getRegionFilter();
                String valueDimText = cleanUp(dimension.getCategory().getLabel().get(valueDimCode));
                validateEhak(filtersConfig, valueDimText);
                setDimensions(dataPoint, countOfUsedDimensions, filter.getFrontName(lang), i, valueDimText);
                countOfUsedDimensions++;
            } else { // regular filter
                Filter filter = filtersConfig.getRegularFilters().get(lang).get(dimensionName);
                if (filter == null) {
                    throw new UnknownFilterException("dimension name is unknown " + dimensionName);
                }
                FilterValueCustom filterValueCustom = getFilterValueCustom(valueDimCode, customFilterValues, lang, dimensionName);
                String valueDimText = getValueDimText(dimension, valueDimCode, filterValueCustom, lang);
                FilterValue filterValue = filter.getValueMap().get(lang).get(valueDimText);
                if (isNull(filterValue) && isNull(filterValueCustom)) {
                    throw new UnknownFilterValueException("dimension value text is unknown " + valueDimText);
                }
                String displayValue = isNull(filterValue)
                        ? filterValueCustom.getDisplayName(lang)
                        : filterValue.getFrontValue(lang);
                setDimensions(dataPoint, countOfUsedDimensions, filter.getFrontName(lang), i, displayValue);
                countOfUsedDimensions++;
            }
        }
        if (filtersConfig.hasTimePart()) {
            dataPoint.setDate(calcDate(dataPoint));
        }
        return dataPoint;
    }

    private FilterValueCustom getFilterValueCustom(String valueDimCode, List<FilterValueCustom> customFilterValues, Language lang, String dimensionName) {
        try {
            Long valueDimCodeForCustom = parseLong(valueDimCode.replace(FILTER_VALUE_CUSTOM_PREFIX, ""));
            return customFilterValues.stream()
                    .filter(fvc -> fvc.getFirstDimensionName(lang) != null && fvc.getFirstDimensionName(lang).equals(dimensionName)
                            && fvc.getId().equals(valueDimCodeForCustom))
                    .findFirst().orElse(null);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private String getValueDimText(Dimension dimension, String valueDimCode, FilterValueCustom filterValueCustom, Language lang) {
        String nativeValue = dimension.getCategory().getLabel().get(valueDimCode);
        if (isNull(nativeValue) && isNull(filterValueCustom)) {
            throw new StatBadRequestException(WIDGET_ADMIN_FILTER_VALUE_MISSING);
        }
        return nonNull(nativeValue)
                ? cleanUp(nativeValue)
                : filterValueCustom.getDisplayName(lang);
    }

    private BigDecimal toValue(Number value) throws IncorrectValueException {
        try {
            return value != null ? new BigDecimal(valueOf(value)) : null;
        } catch (Exception e) {
            throw new IncorrectValueException(format("Failed to get value from: %s", value));
        }
    }

    private String getDimensionName(Dimension dimension) throws IncorrectLabelException {
        return dimension.getLabelOriginal().orElseThrow(() -> new IncorrectLabelException("Label is required to find Filter name"));
    }

    private FiltersConfig filtersConfig(List<Filter> filters, TimePeriod timePeriod, Language lang) {
        FiltersConfig filtersConfig = new FiltersConfig();
        filtersConfig.setTimePeriod(timePeriod);
        filtersConfig.setTimeFilter(timeFilterOrThrow(filters));
        filtersConfig.setTimePart(getTimePart(filters).orElse(null));

        Optional<Filter> regionFilter = getRegion(filters);
        if (regionFilter.isPresent()) {
            filtersConfig.setRegionFilter(regionFilter.get());
            filtersConfig.setEhakNames(ehakNamesLowerCase(lang));
        }

        filtersConfig.setConstraintFilterNames(getConstraintNameEt(filters));
        filtersConfig.setRegularFilters(getRegularFilter(filters));
        return filtersConfig;
    }

    private List<String> ehakNamesLowerCase(Language lang) {
        return elementServiceCache.findAllLowerNamesByClfCode(ClassifierCode.EHAK, lang, null);
    }

    private void validateEhak(FiltersConfig filtersConfig, String value) throws RegionFilterException {
        if (!filtersConfig.getEhakNames().contains(value.toLowerCase())) {
            throw new RegionFilterException(format("%s is not a valid ehak region", value));
        }
    }

    private LocalDate calcDate(DataPoint dp) throws TimePartCalculationException, TimePartMappingException {
        if (dp.getYear() == null) {
            return null;
        }
        TimePartLogic logic = timePartService.timePartLogic(dp.getTimePart());
        return logic.calcDate(dp.getYear(), logic.getUnit(dp));
    }

    private String rule(Exception e) {
        String name = e.getClass().getSimpleName();
        return name.replace("Exception", "");
    }
}
