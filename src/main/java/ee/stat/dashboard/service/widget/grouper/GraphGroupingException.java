package ee.stat.dashboard.service.widget.grouper;

import ee.stat.dashboard.service.widget.diagram.CalculationException;

public class GraphGroupingException extends CalculationException {

    public GraphGroupingException(String message) {
        super(message);
    }

    public GraphGroupingException(String message, Throwable cause) {
        super(message, cause);
    }
}
