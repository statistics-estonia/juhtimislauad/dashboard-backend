package ee.stat.dashboard.service.widget.coordinates.exception;

public class NullValueException extends Exception{

    public NullValueException(String message) {
        super(message);
    }
}
