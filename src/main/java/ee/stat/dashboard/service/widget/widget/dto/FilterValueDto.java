package ee.stat.dashboard.service.widget.widget.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FilterValueDto {

    private Long id;
    private String option;
    private Boolean selected;
}
