package ee.stat.dashboard.service.widget.widget;

import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.repository.WidgetRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@AllArgsConstructor
public class WidgetServiceCache {

    private final WidgetRepository widgetRepository;

    @Cacheable(value = "WidgetServiceCache_findById", key = "#widgetId")
    public Optional<Widget> findById(Long widgetId) {
        return widgetRepository.findById(widgetId);
    }
}
