package ee.stat.dashboard.service.widget.diagram;

import ee.stat.dashboard.model.widget.front.Diagram;
import ee.stat.dashboard.model.widget.front.DiagramData;
import ee.stat.dashboard.model.widget.front.enums.DiagramStatus;
import ee.stat.dashboard.repository.DiagramDataRepository;
import ee.stat.dashboard.repository.DiagramRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static ee.stat.dashboard.util.StatListUtil.firstOrNull;

@Service
@AllArgsConstructor
public class DiagramServiceCache {

    private final DiagramRepository diagramRepository;
    private final DiagramDataRepository diagramDataRepository;

    @Cacheable(value = "DiagramServiceCache_findDiagram", key = "#widgetId")
    public Optional<Diagram> findDiagram(Long widgetId) {
        return Optional.ofNullable(firstOrNull(diagramRepository.findByWidgetAndStatusOrderByCreatedAtDesc(widgetId, DiagramStatus.APPROVED)));
    }

    @Cacheable(value = "DiagramServiceCache_findDiagramGraph", key = "#graphTypeId")
    public Optional<DiagramData> findDiagramGraph(Long graphTypeId, Long diagramId) {
        return diagramDataRepository.findByGraphTypeAndDiagram(graphTypeId, diagramId);
    }
}
