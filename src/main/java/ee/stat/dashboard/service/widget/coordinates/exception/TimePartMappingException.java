package ee.stat.dashboard.service.widget.coordinates.exception;

import lombok.Getter;

@Getter
public class TimePartMappingException extends Exception{

    public TimePartMappingException(String message) {
        super(message);
    }
}
