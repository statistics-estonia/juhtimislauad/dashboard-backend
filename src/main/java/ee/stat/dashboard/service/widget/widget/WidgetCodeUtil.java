package ee.stat.dashboard.service.widget.widget;

import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class WidgetCodeUtil {

    public static String toCode(String value) {
        return value.toLowerCase()
                .replace(" ", "_")
                .replace("õ", "o")
                .replace("ä", "a")
                .replace("ö", "o")
                .replace("ü", "u")
                .replace(",", "");
    }
}
