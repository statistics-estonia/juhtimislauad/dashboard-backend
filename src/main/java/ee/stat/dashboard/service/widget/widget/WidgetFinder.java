package ee.stat.dashboard.service.widget.widget;

import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.repository.DashboardWidgetRepository;
import ee.stat.dashboard.repository.WidgetRepository;
import ee.stat.dashboard.service.dashboard.DashboardFinder;
import ee.stat.dashboard.service.widget.widget.dto.WidgetDashboardContainer;
import ee.stat.dashboard.util.StatBadRequestException;
import ee.stat.dashboard.util.StatNotFoundRequestException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import static ee.stat.dashboard.util.ErrorCode.MY_DASHBOARD_DOES_NOT_EXIST;
import static ee.stat.dashboard.util.ErrorCode.WIDGET_IS_HIDDEN;
import static ee.stat.dashboard.util.ErrorCode.WIDGET_IS_NOT_EXISTING;
import static ee.stat.dashboard.util.ErrorCode.WIDGET_IS_NOT_EXISTING_OR_ADDED_TO_DASHBOARD;

@Service
@AllArgsConstructor
public class WidgetFinder {

    private final WidgetServiceCache widgetServiceCache;
    private final DashboardFinder dashboardFinder;
    private final WidgetRepository widgetRepository;
    private final DashboardWidgetRepository dashboardWidgetRepository;

    public Widget findWidgetForPinning(Long id){
        Widget widget = findWidgetByIdFromCache(id);
        mustBeVisible(widget);
        return widget;
    }

    public WidgetDashboardContainer findWidgetForAdmin(Long id) {
        Widget widget = widgetRepository.findById(id).orElseThrow(() -> new StatBadRequestException(WIDGET_IS_NOT_EXISTING));
        return new WidgetDashboardContainer(widget);
    }

    public WidgetDashboardContainer findWidgetForViewing(Long id, Long dashboardId) {
        Dashboard dashboard = dashboardFinder.findById(dashboardId);
        if (dashboard.getUserType().isAdmin()) {
            existsAdminWidget(id, dashboardId);
        } else {
            existsUserWidget(id, dashboardId);
        }
        Widget widget = findWidgetByIdFromCache(id);
        mustBeVisible(widget);
        return new WidgetDashboardContainer(widget, dashboard);
    }

    private Widget findWidgetByIdFromCache(Long id) {
        return widgetServiceCache.findById(id).orElseThrow(() -> new StatBadRequestException(WIDGET_IS_NOT_EXISTING));
    }

    private void existsUserWidget(Long widgetId, Long dashboardId) {
        if (!dashboardWidgetRepository.existsByDashboardAndWidget(dashboardId, widgetId)) {
            throw new StatNotFoundRequestException(MY_DASHBOARD_DOES_NOT_EXIST);
        }
    }

    private void mustBeVisible(Widget widget) {
        if (widget.getStatus().isHidden()) {
            throw new StatBadRequestException(WIDGET_IS_HIDDEN);
        }
    }

    private void existsAdminWidget(Long id, Long dashboardId) {
        if (!widgetRepository.existsWidget(id, dashboardId)) {
            throw new StatBadRequestException(WIDGET_IS_NOT_EXISTING_OR_ADDED_TO_DASHBOARD);
        }
    }
}
