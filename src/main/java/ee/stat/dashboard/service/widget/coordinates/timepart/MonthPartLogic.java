package ee.stat.dashboard.service.widget.coordinates.timepart;

import ee.stat.dashboard.model.dashboard.StatMonth;
import ee.stat.dashboard.model.dashboard.Unit;
import ee.stat.dashboard.model.json.DataPoint;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.enums.TimePart;
import ee.stat.dashboard.service.widget.coordinates.converters.MonthConverter;
import ee.stat.dashboard.service.widget.coordinates.exception.TimePartMappingException;
import ee.stat.dashboard.util.StatDateUtil;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;

import static ee.stat.dashboard.util.StatStringUtil.cleanUp;

@Service
class MonthPartLogic implements TimePartLogic {

    @Override
    public TimePart timePart() {
        return TimePart.MONTH;
    }

    @Override
    public Class<? extends Unit> unit() {
        return StatMonth.class;
    }

    @Override
    public StatMonth convert(Language lang, String value) throws TimePartMappingException {
        return StatMonth.valueFrom(MonthConverter.convert(lang, value));
    }

    @Override
    public LocalDate calcDate(Year year, Unit unit) throws TimePartMappingException {
        if (!(unit instanceof StatMonth)) {
            throw new TimePartMappingException("Wrong unit passed: " + unit.getClass());
        }
        return StatDateUtil.buildMonthDate(year, ((StatMonth) unit).getMonth());
    }

    @Override
    public Unit getUnit(DataPoint dp) {
        Month month = dp.getMonth();
        return month != null ? StatMonth.valueFrom(month) : null;
    }

    @Override
    public void setUnit(DataPoint dp, Unit unit) throws TimePartMappingException {
        if (!(unit instanceof StatMonth)) {
            throw new TimePartMappingException("Wrong unit passed: " + unit.getClass());
        }
        dp.setMonth(((StatMonth) unit).getMonth());
    }

    @Override
    public String getValue(no.ssb.jsonstat.v2.Dimension dimension, String valueCode) {
        return cleanUp(dimension.getCategory().getLabel().get(valueCode));
    }
}
