package ee.stat.dashboard.service.widget.excelsplitter;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class CommonExcelMapper {

    public static boolean isDouble(String column) {
        try {
            Double.valueOf(column);
            return true;
        } catch (Exception ignored) {
            return false;
        }
    }

    public Workbook tryToCreateWorkbook(File file) {
        try {
            return WorkbookFactory.create(file);
        } catch (IOException e) {
            log.error("failed to load file: ", e);
            return null;
        }
    }

    public List<List<String>> excelToStrings(Sheet sheet) {
        List<List<String>> rows = new ArrayList<>();
        for (Row row : sheet) {
            rows.add(mapRow(row));
        }
        return rows;
    }

    private List<String> mapRow(Row row) {
        List<String> columns = new ArrayList<>();
        for (Cell cell : row) {
            String string = printCellValue(cell);
            if (string.isEmpty()) {
                columns.add(string);
            } else {
                columns.add(string
                        .replaceAll("\\p{Z}", " ")); // wierd whitespace
            }
        }
        return columns;
    }

    private static String printCellValue(Cell cell) {
        switch (cell.getCellType()) {
            case BOOLEAN:
                return cell.getBooleanCellValue() + "";
            case STRING:
                return cell.getRichStringCellValue().getString();
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    return cell.getDateCellValue().toString();
                }
                return cell.getNumericCellValue() + "";
            case FORMULA:
                return cell.getCellFormula();
            case BLANK:
                return "";
            default:
                return "";
        }
    }
}
