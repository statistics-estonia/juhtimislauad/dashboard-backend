package ee.stat.dashboard.service.widget.widget;


import ee.stat.dashboard.model.user.widget.UserFilter;
import ee.stat.dashboard.model.user.widget.UserFilterValue;
import ee.stat.dashboard.model.user.widget.UserGraphType;
import ee.stat.dashboard.model.user.widget.UserLegend;
import ee.stat.dashboard.model.user.widget.UserLegendValue;
import ee.stat.dashboard.model.user.widget.UserWidget;
import ee.stat.dashboard.model.widget.back.GraphType;
import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.repository.GraphTypeRepository;
import ee.stat.dashboard.repository.UserFilterRepository;
import ee.stat.dashboard.repository.UserFilterValueRepository;
import ee.stat.dashboard.repository.UserGraphTypeRepository;
import ee.stat.dashboard.repository.UserLegendRepository;
import ee.stat.dashboard.repository.UserLegendValueRepository;
import ee.stat.dashboard.service.admin.widget.UserWidgetDeleteService;
import ee.stat.dashboard.service.widget.widget.dto.UserFilterDto;
import ee.stat.dashboard.service.widget.widget.dto.UserGraphTypeDto;
import ee.stat.dashboard.service.widget.widget.dto.UserLegendDto;
import ee.stat.dashboard.service.widget.widget.dto.UserLegendValueDto;
import ee.stat.dashboard.service.widget.widget.dto.UserWidgetDto;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static ee.stat.dashboard.util.ErrorCode.ILLEGAL_GRAPH_TYPE;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
@AllArgsConstructor
public class UserWidgetPreferencesSaveService {

    private final UserGraphTypeRepository userGraphTypeRepository;
    private final GraphTypeRepository graphTypeRepository;
    private final UserFilterRepository userFilterRepository;
    private final UserFilterValueRepository userFilterValueRepository;
    private final UserLegendRepository userLegendRepository;
    private final UserLegendValueRepository userLegendValueRepository;
    private final UserWidgetDeleteService userWidgetDeleteService;

    public void savePreferences(UserWidget userWidget, UserWidgetDto preferences) {
        userWidgetDeleteService.deleteUserData(userWidget);

        List<UserGraphTypeDto> graphTypes = listOfCharts(preferences);
        if (isNotEmpty(graphTypes)) {
            for (UserGraphTypeDto graphTypeDto : graphTypes) {
                GraphTypeEnum type = graphTypeDto.getGraphType();
                Optional<GraphType> graphType = graphTypeRepository.findByWidgetAndType(userWidget.getWidget(), type);
                if (graphType.isEmpty()) {
                    throw new StatBadRequestException(ILLEGAL_GRAPH_TYPE);
                }

                UserGraphType savedUserGraphType = saveUserGraphType(userWidget, graphType.get(), type, preferences.getDefaultGraphType());
                if (isNotEmpty(graphTypeDto.getFilters())) {
                    saveUserFilters(graphTypeDto, savedUserGraphType);
                }
                if (isNotEmpty(graphTypeDto.getLegends())){
                    saveUserLegends(graphTypeDto, savedUserGraphType);
                }
            }
        }
    }

    private void saveUserFilters(UserGraphTypeDto graphTypeDto, UserGraphType savedUserGraphType) {
        for (UserFilterDto filter : graphTypeDto.getFilters()) {
            UserFilter savedUserFilter = saveUserFilter(savedUserGraphType, filter);
            if (isNotEmpty(filter.getValues())) {
                for (Long value : filter.getValues()) {
                    saveUserFilterValue(savedUserFilter, value);
                }
            }
        }
    }

    private void saveUserLegends(UserGraphTypeDto graphTypeDto, UserGraphType savedUserGraphType) {
        for (UserLegendDto legend : graphTypeDto.getLegends()) {
            UserLegend userLegend = saveUserLegend(savedUserGraphType, legend);
            if (isNotEmpty(legend.getValues())){
                for (UserLegendValueDto value : legend.getValues()) {
                    saveUserLegendValue(userLegend, value);
                }
            }
        }
    }

    private List<UserGraphTypeDto> listOfCharts(UserWidgetDto preferences) {
        List<UserGraphTypeDto> charts = preferences.getGraphs();
        List<UserGraphTypeDto> graphTypes = charts != null ? new ArrayList<>(charts) : new ArrayList<>();
        if (preferences.getDefaultGraphType() != null && graphTypes.stream().noneMatch(e -> e.getGraphType().equals(preferences.getDefaultGraphType()))) {
            graphTypes.add(new UserGraphTypeDto(preferences.getDefaultGraphType()));
        }
        return graphTypes;
    }

    private UserGraphType saveUserGraphType(UserWidget userWidget, GraphType graphType, GraphTypeEnum type, GraphTypeEnum defaultType) {
        UserGraphType userGraphType = new UserGraphType();
        userGraphType.setType(type);
        userGraphType.setGraphType(graphType.getId());
        userGraphType.setUserWidget(userWidget.getId());
        userGraphType.setSelected(type.equals(defaultType));
        return userGraphTypeRepository.save(userGraphType);
    }

    private UserFilter saveUserFilter(UserGraphType userGraphType, UserFilterDto filter) {
        UserFilter userFilter = new UserFilter();
        userFilter.setFilter(filter.getFilter());
        userFilter.setUserGraphType(userGraphType.getId());
        return userFilterRepository.save(userFilter);
    }

    private void saveUserFilterValue(UserFilter savedUserFilter, Long value) {
        UserFilterValue userFilterValue = new UserFilterValue();
        userFilterValue.setUserFilter(savedUserFilter.getId());
        userFilterValue.setFilterValue(value);
        userFilterValueRepository.save(userFilterValue);
    }

    private UserLegend saveUserLegend(UserGraphType userGraphType, UserLegendDto legend) {
        UserLegend userLegend = new UserLegend();
        userLegend.setTarget(BooleanUtils.isTrue(legend.getTarget()));
        userLegend.setUserGraphType(userGraphType.getId());
        return userLegendRepository.save(userLegend);
    }

    private void saveUserLegendValue(UserLegend savedUserLegend, UserLegendValueDto value) {
        UserLegendValue userLegendValue = new UserLegendValue();
        userLegendValue.setUserLegend(savedUserLegend.getId());
        userLegendValue.setFilter(value.getFilter());
        userLegendValue.setFilterValue(value.getValue());
        userLegendValueRepository.save(userLegendValue);
    }
}
