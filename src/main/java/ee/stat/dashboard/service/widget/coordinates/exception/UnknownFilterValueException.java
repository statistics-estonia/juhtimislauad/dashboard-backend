package ee.stat.dashboard.service.widget.coordinates.exception;

public class UnknownFilterValueException extends Exception {

    public UnknownFilterValueException(String message) {
        super(message);
    }
}
