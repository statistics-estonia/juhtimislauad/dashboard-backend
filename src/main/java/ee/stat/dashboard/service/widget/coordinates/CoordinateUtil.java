package ee.stat.dashboard.service.widget.coordinates;

import ee.stat.dashboard.model.json.DataPoint;
import lombok.NoArgsConstructor;
import no.ssb.jsonstat.v2.Dimension;

import java.util.List;
import java.util.Optional;

import static java.util.stream.IntStream.range;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class CoordinateUtil {

    public static void setDimensions(DataPoint dataPoint, int dimensionOrder, String dimension, int id, String value) {
        if (dimensionOrder == 0) {
            dataPoint.setDim1(dimension);
            dataPoint.setOrder1(id);
            dataPoint.setVal1(value);
        } else if (dimensionOrder == 1) {
            dataPoint.setDim2(dimension);
            dataPoint.setOrder2(id);
            dataPoint.setVal2(value);
        } else if (dimensionOrder == 2) {
            dataPoint.setDim3(dimension);
            dataPoint.setOrder3(id);
            dataPoint.setVal3(value);
        } else if (dimensionOrder == 3) {
            dataPoint.setDim4(dimension);
            dataPoint.setOrder4(id);
            dataPoint.setVal4(value);
        } else if (dimensionOrder == 4) {
            dataPoint.setDim5(dimension);
            dataPoint.setOrder5(id);
            dataPoint.setVal5(value);
        } else {
            throw new UnsupportedOperationException("too many dimensions");
        }
    }

    public static int findDimensionIndex(List<Dimension> dimensions, String dimensionLabel) {
        return range(0, dimensions.size())
                .filter(i -> {
                    Optional<String> label = dimensions.get(i).getLabel();
                    return label.isPresent() && label.get().equalsIgnoreCase(dimensionLabel);
                })
                .findFirst()
                .orElse(-1);
    }


}
