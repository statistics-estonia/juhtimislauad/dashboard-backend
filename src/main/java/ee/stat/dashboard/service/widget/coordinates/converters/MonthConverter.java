package ee.stat.dashboard.service.widget.coordinates.converters;

import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.widget.coordinates.exception.TimePartMappingException;
import lombok.NoArgsConstructor;

import java.time.Month;

import static java.lang.String.format;
import static java.time.Month.APRIL;
import static java.time.Month.AUGUST;
import static java.time.Month.DECEMBER;
import static java.time.Month.FEBRUARY;
import static java.time.Month.JANUARY;
import static java.time.Month.JULY;
import static java.time.Month.JUNE;
import static java.time.Month.MARCH;
import static java.time.Month.MAY;
import static java.time.Month.NOVEMBER;
import static java.time.Month.OCTOBER;
import static java.time.Month.SEPTEMBER;
import static java.time.Month.valueOf;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class MonthConverter {

    public static Month convert(Language lang, String value) throws TimePartMappingException {
        return lang.isEt() ? convertEstMonth(value) : convertEngMonth(value);
    }

    private static Month convertEngMonth(String value) throws TimePartMappingException {
        try {
            return valueOf(value.toUpperCase());
        } catch (Exception e) {
            throw new TimePartMappingException(format("%s is not a valid EN month", value));
        }
    }

    private static Month convertEstMonth(String value) throws TimePartMappingException {
        switch (value.toLowerCase()) {
            case "jaanuar":
                return JANUARY;
            case "veebruar":
                return FEBRUARY;
            case "märts":
                return MARCH;
            case "aprill":
                return APRIL;
            case "mai":
                return MAY;
            case "juuni":
                return JUNE;
            case "juuli":
                return JULY;
            case "august":
                return AUGUST;
            case "september":
                return SEPTEMBER;
            case "oktoober":
                return OCTOBER;
            case "november":
                return NOVEMBER;
            case "detsember":
                return DECEMBER;
            default:
                throw new TimePartMappingException(format("%s is not a valid ET month", value));
        }
    }
}
