package ee.stat.dashboard.service.widget.widget.dto;

import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.widget.back.Widget;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class WidgetDashboardContainer {

    private Widget widget;
    private Dashboard dashboard;

    public WidgetDashboardContainer(Widget widget) {
        this.widget = widget;
    }
}
