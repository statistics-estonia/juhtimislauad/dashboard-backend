package ee.stat.dashboard.service.widget.diagram.dto;

import ee.stat.dashboard.model.widget.back.Filter;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.model.widget.back.excel.Excel;
import ee.stat.dashboard.model.widget.back.statdataapi.StatDataApiData;
import ee.stat.dashboard.model.widget.back.statdatasql.StatDataSqlData;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
@AllArgsConstructor
public class RawDataConfig {
    private StatDataApiData statDataApiData;
    private StatDataSqlData statDataSqlData;
    private Excel excel;
    private List<Filter> filters;
    private TimePeriod timePeriod;
    private Language lang;
}
