package ee.stat.dashboard.service.widget.excelsplitter.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class WidgetExcelPartition {

    private List<List<String>> location;
    private List<List<String>> main;
    private List<List<String>> filter;
    private List<List<List<String>>> data = new ArrayList<>();

}
