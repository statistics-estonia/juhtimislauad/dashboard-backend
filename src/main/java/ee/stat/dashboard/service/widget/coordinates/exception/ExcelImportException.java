package ee.stat.dashboard.service.widget.coordinates.exception;

import ee.stat.dashboard.util.ErrorCode;
import lombok.Getter;

@Getter
public class ExcelImportException extends Exception{

    private ErrorCode code;

    public ExcelImportException(String message) {
        super(message);
    }

    public ExcelImportException(ErrorCode code, String message) {
        super(message);
        this.code = code;
    }
}
