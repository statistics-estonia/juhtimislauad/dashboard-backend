package ee.stat.dashboard.service.widget.diagram;

import ee.stat.dashboard.model.widget.back.Filter;
import ee.stat.dashboard.model.widget.back.FilterValue;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.repository.FilterValueRepository;
import ee.stat.dashboard.service.admin.widget.WidgetDeleteService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static java.util.Comparator.comparing;
import static java.util.Comparator.reverseOrder;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
@AllArgsConstructor
public class TimeFilterManagement {

    private final WidgetDeleteService widgetDeleteService;
    private final FilterValueRepository filterValueRepository;

    public void buildTimeFilter(Language lang, Filter timeFilter, List<LocalDate> correctDates) {
        List<String> correctDateStrings = correctDates.stream()
                .sorted(reverseOrder())
                .map(LocalDate::toString)
                .collect(toList());

        List<FilterValue> valuesToRemove = new ArrayList<>();
        for (FilterValue value : timeFilter.getValues()) {
            if (!correctDateStrings.contains(value.getValue(lang))) {
                valuesToRemove.add(value);
            }
        }
        List<FilterValue> valuesToInsert = new ArrayList<>();
        for (String dateString : correctDateStrings) {
            if (timeFilter.getValues().stream().noneMatch(v -> v.getValueEt().equals(dateString))) {
                FilterValue value = new FilterValue();
                value.setFilter(timeFilter.getId());
                value.setValueEt(dateString);
                value.setValueEn(dateString);
                valuesToInsert.add(value);
            }
        }

        if (isNotEmpty(valuesToInsert) || isNotEmpty(valuesToRemove) || hasNoOrderNr(timeFilter.getValues())) {
            List<FilterValue> existingTimeFilterValues = timeFilter.getValues();
            existingTimeFilterValues.removeAll(valuesToRemove);
            existingTimeFilterValues.addAll(0, valuesToInsert);
            sortDesc(existingTimeFilterValues);
            for (int i = 0; i < existingTimeFilterValues.size(); i++) {
                FilterValue value = existingTimeFilterValues.get(i);
                value.setOrderNr(i);
                value.setSelected(i == 0);
            }
            widgetDeleteService.deleteFilterValues(valuesToRemove);
            filterValueRepository.saveAll(existingTimeFilterValues);
            filterValueRepository.flush();

            timeFilter.setValues(filterValueRepository.findAllByFilterOrderByOrderNrAscIdAsc(timeFilter.getId()));
        }
    }

    static void sortDesc(List<FilterValue> existingTimeFilterValues) {
        existingTimeFilterValues.sort(comparing(FilterValue::getValueEt).reversed());
    }

    boolean hasNoOrderNr(List<FilterValue> filterValues){
        return isNotEmpty(filterValues) && filterValues.stream().anyMatch(fv -> fv.getOrderNr() == null);
    }
}
