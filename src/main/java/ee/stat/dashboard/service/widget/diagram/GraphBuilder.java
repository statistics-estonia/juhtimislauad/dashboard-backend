package ee.stat.dashboard.service.widget.diagram;

import ee.stat.dashboard.model.json.CoordinatedResponse;
import ee.stat.dashboard.model.widget.back.Filter;
import ee.stat.dashboard.model.widget.back.GraphType;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.excel.Excel;
import ee.stat.dashboard.model.widget.back.statdataapi.StatDataApiData;
import ee.stat.dashboard.model.widget.back.statdatasql.StatDataSqlData;
import ee.stat.dashboard.model.widget.front.DiagramData;
import ee.stat.dashboard.model.widget.front.FilterSerie;
import ee.stat.dashboard.model.widget.front.Graph;
import ee.stat.dashboard.model.widget.front.Period;
import ee.stat.dashboard.repository.ExcelRepository;
import ee.stat.dashboard.repository.GraphTypeRepository;
import ee.stat.dashboard.repository.StatDataApiDataRepository;
import ee.stat.dashboard.repository.StatDataSqlDataRepository;
import ee.stat.dashboard.service.widget.coordinates.RawDataTransformer;
import ee.stat.dashboard.service.widget.coordinates.dto.MergeResponse;
import ee.stat.dashboard.service.widget.diagram.dto.RawDataConfig;
import ee.stat.dashboard.service.widget.grouper.DatapointGrouper;
import ee.stat.dashboard.service.widget.grouper.GraphGroupingException;
import ee.stat.dashboard.service.widget.widget.FilterService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static ee.stat.dashboard.model.widget.back.enums.ExcelType.DATA;
import static ee.stat.dashboard.model.widget.back.enums.ExcelType.WIDGET_META_AND_DATA;
import static ee.stat.dashboard.service.widget.diagram.DiagramFixer.addMissingPoints;
import static ee.stat.dashboard.service.widget.diagram.DiagramFixer.removeOutsidePoints;
import static ee.stat.dashboard.service.widget.diagram.DiagramMath.divideValues;
import static ee.stat.dashboard.service.widget.diagram.DiagramMath.round;
import static ee.stat.dashboard.service.widget.widget.FilterUtil.axisFilter;
import static ee.stat.dashboard.service.widget.widget.FilterUtil.timeFilter;
import static ee.stat.dashboard.service.widget.widget.FilterUtil.values;
import static ee.stat.dashboard.util.StatListUtil.firstOrNull;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;

@Slf4j
@Service
@AllArgsConstructor
public class GraphBuilder {

    private final DatapointGrouper datapointGrouper;
    private final RawDataTransformer rawDataTransformer;
    private final FilterService filterService;
    private final TimeFilterManagement timeFilterManagement;
    private final DiagramDateUtil diagramDateUtil;
    private final GraphTypeRepository graphTypeRepository;
    private final StatDataApiDataRepository statDataApiDataRepository;
    private final StatDataSqlDataRepository statDataSqlDataRepository;
    private final ExcelRepository excelRepository;

    public List<DiagramData> buildDiagramGraphs(Widget widget) throws CalculationException {
        Excel excel = firstOrNull(excelRepository.findByWidgetAndExcelTypeIn(widget.getId(), List.of(WIDGET_META_AND_DATA, DATA)));

        List<GraphType> graphTypes = graphTypeRepository.findAllByWidgetOrderByIdAsc(widget.getId());
        if (isEmpty(graphTypes)) {
            log.info("No diagrams will be generated for widget {} as it has no graph types", widget.getId());
            return List.of();
        }
        List<DiagramData> data = new ArrayList<>();
        for (GraphType graphType : graphTypes) {
            data.add(mapDiagramGraph(widget, excel, graphType));
        }
        return data;
    }

    private DiagramData mapDiagramGraph(Widget widget, Excel excel, GraphType graphType) throws CalculationException {
        StatDataApiData statDataApiData = firstOrNull(statDataApiDataRepository.findByGraphType(graphType.getId()));
        StatDataSqlData statDataSqlData = statDataApiData == null ? firstOrNull(statDataSqlDataRepository.findByGraphType(graphType.getId())) : null;
        DiagramData diagramData = new DiagramData();
        diagramData.setWidget(widget.getId());
        diagramData.setGraphType(graphType.getId());
        diagramData.setType(graphType.getType());

        List<Filter> filters = filterService.getDbFiltersNoCache(graphType.getId());
        for (Language lang : Language.values()) {
            RawDataConfig config = RawDataConfig.builder()
                    .statDataApiData(statDataApiData)
                    .statDataSqlData(statDataSqlData)
                    .excel(excel)
                    .filters(filters)
                    .timePeriod(widget.getTimePeriod())
                    .lang(lang).build();
            MergeResponse response = rawDataTransformer.transformRowData(config);
            validResponse(widget, response);
            diagramData.setGraph(buildGraphData(lang, widget, filters, response, graphType.getType()), lang);
        }
        return diagramData;
    }

    private Graph buildGraphData(Language lang, Widget widget, List<Filter> dbFilters,
                                 MergeResponse mergeResponse, GraphTypeEnum graphType) throws GraphBuilderException, GraphGroupingException {
        CoordinatedResponse response = mergeResponse.getResponse();

        if (widget.getDivisor() != null && widget.getDivisor().signum() != 0) {
            divideValues(response, widget.getDivisor());
        }
        if (widget.getPrecisionScale() != null) {
            round(response, widget.getPrecisionScale());
        }

        Period period = diagramDateUtil.buildPeriod(widget, mergeResponse);
        if (period.getFrom() == null || period.getTo() == null) {
            throw new GraphBuilderException("Period has a null date " + period);
        }
        List<LocalDate> correctDates = diagramDateUtil.generateDates(period.getFrom(), period.getTo(), widget.getTimePeriod());

        Filter axisFilter = axisFilter(dbFilters);
        Filter timeFilter = timeFilter(dbFilters);

        if (axisFilter.equals(timeFilter)) {
            if (graphType.isLineOrBarOrStackedOrArea()) {
                Graph graph = datapointGrouper.createDiagram(response.getDataPoints());
                graph.setPeriod(period);

                for (FilterSerie filterSerie : graph.getSeries()) {
                    removeOutsidePoints(filterSerie, period);
                    addMissingPoints(filterSerie, correctDates, mergeResponse.getMaxRealDataDate());
                }
                return graph;
            } else if (graphType.isMapOrVerticalOrPieOrTreemapOrRadarOrPyramid()) {
                throw illegalState(lang, widget, graphType, axisFilter, timeFilter);
            } else {
                throw illegalState(lang, widget, graphType, axisFilter, timeFilter);
            }
        } else {
            if (graphType.isLineOrBarOrStackedOrArea()) {
                throw illegalState(lang, widget, graphType, axisFilter, timeFilter);
            } else if (graphType.isMapOrVerticalOrPieOrTreemapOrRadarOrPyramid()) {
                Graph graph = datapointGrouper.createDiagram(response.getDataPoints(),
                        axisFilter.getFrontName(lang), timeFilter.getFrontName(lang),
                        axisFilter.getAxisOrder(), values(axisFilter, lang));
                timeFilterManagement.buildTimeFilter(lang, timeFilter, correctDates);
                return graph;
            } else {
                throw illegalState(lang, widget, graphType, axisFilter, timeFilter);
            }
        }
    }

    private GraphBuilderException illegalState(Language lang, Widget widget, GraphTypeEnum graphType, Filter axisFilter, Filter timeFilter) {
        return new GraphBuilderException(String.format(
                "Unexpected state. %n" +
                        "Check if GraphType allows following axis or whether Axis is missing. %n" +
                        "Widget %s, %n" +
                        "GraphType %s, %n" +
                        "AxisFilter %s, %n" +
                        "TimeFilter %s %n",
                widget.getId(), graphType, axisFilter.getFrontName(lang), timeFilter.getFrontName(lang)));
    }

    private void validResponse(Widget widget, MergeResponse response) throws GraphBuilderException {
        if (response == null) {
            throw new GraphBuilderException("Stat raw data transformation is missing. Widget id: " + widget.getId());
        }
    }
}
