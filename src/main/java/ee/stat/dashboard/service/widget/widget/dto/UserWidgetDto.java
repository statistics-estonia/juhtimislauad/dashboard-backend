package ee.stat.dashboard.service.widget.widget.dto;

import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UserWidgetDto {

    private Boolean pinned;
    private GraphTypeEnum defaultGraphType;
    private List<UserGraphTypeDto> graphs;
}
