package ee.stat.dashboard.service.widget.widget;

import ee.stat.dashboard.model.widget.back.Filter;
import ee.stat.dashboard.model.widget.back.FilterValue;
import ee.stat.dashboard.model.widget.back.FilterValueCustom;
import ee.stat.dashboard.model.widget.back.FilterValueCustomForFilter;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.repository.FilterRepository;
import ee.stat.dashboard.repository.FilterValueCustomForFilterRepository;
import ee.stat.dashboard.repository.FilterValueRepository;
import ee.stat.dashboard.service.widget.widget.dto.FilterDto;
import ee.stat.dashboard.service.widget.widget.dto.FilterValueDto;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.concat;

@Service
@AllArgsConstructor
public class FilterService {

    private FilterRepository filterRepository;
    private FilterValueRepository filterValueRepository;
    private FilterValueCustomForFilterRepository filterValueCustomForFilterRepository;

    @Cacheable(value = "FilterService_getDbFilters", key = "#graphTypeId")
    public List<Filter> getDbFilters(Long graphTypeId) {
        return getDbFiltersNoCache(graphTypeId);
    }

    public List<Filter> getDbFiltersNoCache(Long graphTypeId) {
        List<Filter> dbfilters = filterRepository.findAllByGraphTypeOrderByOrderNrAscIdAsc(graphTypeId);
        dbfilters.forEach(f -> f.setValues(filterValueRepository.findAllByFilterOrderByOrderNrAscIdAsc(f.getId())));
        return dbfilters;
    }

    public List<FilterDto> mapFiltersWithValues(List<Filter> filters, Language lang) {
        return filters.stream()
                .filter(filter -> filter.getType().logicFilter())
                .map(filter -> mapWithValues(filter, lang, filter.getValues()))
                .collect(toList());
    }

    public FilterDto mapFilter(Filter filter, Language lang) {
        FilterDto filterDto = new FilterDto();
        filterDto.setId(filter.getId());
        filterDto.setOrder(filter.getOrderNr());
        filterDto.setType(filter.getType());
        if (filter.isRegion()) {
            filterDto.setRegion(true);
        }
        if (filter.isTime()) {
            filterDto.setTime(true);
        }
        filterDto.setName(filter.getFrontName(lang));
        filterDto.setNumberOfOptions(filter.getNumberOfValues());
        return filterDto;
    }

    public FilterDto mapWithValues(Filter filter, Language lang, List<FilterValue> valueObj) {
        List<FilterValueCustom> customFilterValues = filterValueCustomForFilterRepository.findAllByFilterId(filter.getId())
                .stream().map(FilterValueCustomForFilter::getFilterValueCustom).collect(toList());
        FilterDto filterDto = mapFilter(filter, lang);
        filterDto.setValues(values(lang, valueObj, customFilterValues));
        filterDto.recalcOptions();
        filterDto.recalcDefaultOptions();
        return filterDto;
    }

    private List<FilterValueDto> values(Language lang, List<FilterValue> valueObj, List<FilterValueCustom> customValueObj) {
        return concat(
                valueObj.stream().map(e -> mapValue(e, lang)),
                customValueObj.stream().map(e -> mapCustomValue(e, lang))
        ).collect(toList());
    }

    private FilterValueDto mapValue(FilterValue value, Language lang) {
        FilterValueDto dto = new FilterValueDto();
        dto.setId(value.getId());
        dto.setOption(value.getFrontValue(lang));
        if (value.isSelected()) {
            dto.setSelected(true);
        }
        return dto;
    }

    private FilterValueDto mapCustomValue(FilterValueCustom value, Language lang) {
        FilterValueDto dto = new FilterValueDto();
        dto.setId(value.getId());
        dto.setOption(value.getDisplayName(lang));
        if (value.isSelected()) {
            dto.setSelected(true);
        }
        return dto;
    }
}
