package ee.stat.dashboard.service.widget.grouper.dto;

import ee.stat.dashboard.model.json.DataPoint;
import ee.stat.dashboard.model.widget.back.enums.AxisOrder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.util.List;
import java.util.function.Function;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Slf4j
@Getter
public class Grouping {

    private int dimensions;
    private Function<DataPoint, String> dim1;
    private Function<DataPoint, String> group1;
    private Function<DataPoint, String> dim2;
    private Function<DataPoint, String> group2;
    private Function<DataPoint, String> dim3;
    private Function<DataPoint, String> group3;
    private Function<DataPoint, LocalDate> map1;
    private Function<DataPoint, String> map2;
    private AxisOrder order;
    private List<String> options;

    public Grouping(DataPoint anyPoint, String abscissa, String timeName, AxisOrder order, List<String> options) {
        dimensions = anyPoint.countOfDimensions();
        if (abscissa == null) {
            this.dim1 = DataPoint::getDim1;
            this.group1 = DataPoint::getVal1;
            this.dim2 = DataPoint::getDim2;
            this.group2 = DataPoint::getVal2;
            this.dim3 = DataPoint::getDim3;
            this.group3 = DataPoint::getVal3;
            this.map1 = DataPoint::getDate;
            this.order = order;
            this.options = options;
        } else {
            this.order = order;
            this.options = options;
            if (equals(anyPoint.getDim1(), abscissa)) {
                this.dim1 = x -> timeName;
                this.group1 = DataPoint::getDateString;
                this.map2 = DataPoint::getVal1;
            } else {
                this.dim1 = DataPoint::getDim1;
                this.group1 = DataPoint::getVal1;
            }
            if (equals(anyPoint.getDim2(), abscissa)) {
                this.dim2 = x -> timeName;
                this.group2 = DataPoint::getDateString;
                this.map2 = DataPoint::getVal2;
            } else {
                this.dim2 = DataPoint::getDim2;
                this.group2 = DataPoint::getVal2;
            }
            if (equals(anyPoint.getDim3(), abscissa)) {
                this.dim3 = x -> timeName;
                this.group3 = DataPoint::getDateString;
                this.map2 = DataPoint::getVal3;
            } else {
                this.dim3 = DataPoint::getDim3;
                this.group3 = DataPoint::getVal3;
            }
            if (map2 == null) {
                log.error("filter not found {} {} {} {} {}",  anyPoint, abscissa, timeName, order.name(), options);
                throw new IllegalStateException("filter not found ");
            }
        }

    }

    private boolean equals(String dim, String abscissa) {
        return isNotBlank(dim) && dim.equals(abscissa);
    }

}
