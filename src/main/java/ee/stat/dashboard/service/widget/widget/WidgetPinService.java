package ee.stat.dashboard.service.widget.widget;

import ee.stat.dashboard.config.security.StatUser;
import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.dashboard.DashboardWidget;
import ee.stat.dashboard.model.user.widget.UserWidget;
import ee.stat.dashboard.repository.DashboardWidgetRepository;
import ee.stat.dashboard.repository.UserWidgetRepository;
import ee.stat.dashboard.service.admin.widget.UserWidgetDeleteService;
import ee.stat.dashboard.service.dashboard.DashboardSaveService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import static ee.stat.dashboard.util.StatListUtil.first;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
@AllArgsConstructor
public class WidgetPinService {

    private final UserWidgetRepository userWidgetRepository;
    private final DashboardWidgetRepository dashboardWidgetRepository;
    private final DashboardSaveService dashboardSaveService;
    private final UserWidgetDeleteService userWidgetDeleteService;
    private final UserWidgetCopyService userWidgetCopyService;

    public void unpinFlow(StatUser user, Long widgetId) {
        Dashboard myDashboard = dashboardSaveService.findExistingOrCreateNew(user);
        if (!dashboardWidgetRepository.existsByDashboardAndWidget(myDashboard.getId(), widgetId)) {
            return;
        }
        List<UserWidget> userWidgets = userWidgetRepository.findByAppUserAndDashboardAndWidget(user.getId(), myDashboard.getId(), widgetId);
        if (isNotEmpty(userWidgets)) {
            userWidgets.forEach(userWidget -> {
                userWidgetDeleteService.deleteUserData(userWidget);
                userWidgetRepository.deleteById(userWidget.getId());
            });
        }
        dashboardWidgetRepository.deleteByDashboardAndWidget(myDashboard.getId(), widgetId);
    }

    public void pinFlow(StatUser user, Long dashboardId, Long widgetId) {
        Dashboard myDashboard = dashboardSaveService.findExistingOrCreateNew(user);
        if (dashboardWidgetRepository.existsByDashboardAndWidget(myDashboard.getId(), widgetId)) {
            return;
        }
        DashboardWidget dashboardWidget = saveDashboardWidget(myDashboard, widgetId);
        List<UserWidget> myDashboardUserWidgets = userWidgetRepository.findByAppUserAndDashboardAndWidget(user.getId(), myDashboard.getId(), widgetId);
        if (isEmpty(myDashboardUserWidgets)) {
            List<UserWidget> userWidgetsOfPrevDashboard = userWidgetRepository.findByAppUserAndDashboardAndWidget(user.getId(), dashboardId, widgetId);
            if (isNotEmpty(userWidgetsOfPrevDashboard)) {
                List<UserWidget> currentNrOfWidgets = userWidgetRepository.findByAppUserAndDashboard(user.getId(), myDashboard.getId());
                userWidgetCopyService.saveAsCopy(first(userWidgetsOfPrevDashboard), myDashboard, dashboardWidget, currentNrOfWidgets.size() * -1);
            }
        }
    }

    private DashboardWidget saveDashboardWidget(Dashboard myDashboard, Long widgetId) {
        DashboardWidget dashboardWidget = new DashboardWidget(myDashboard.getId(), widgetId);
        dashboardWidgetRepository.save(dashboardWidget);
        return dashboardWidget;
    }
}
