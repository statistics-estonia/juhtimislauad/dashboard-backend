package ee.stat.dashboard.service.widget.grouper;

import ee.stat.dashboard.model.json.DataPoint;
import ee.stat.dashboard.model.widget.back.enums.AxisOrder;
import ee.stat.dashboard.model.widget.front.FilterSerie;
import ee.stat.dashboard.model.widget.front.Graph;
import ee.stat.dashboard.model.widget.front.Serie;
import ee.stat.dashboard.service.widget.grouper.dto.Grouping;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static ee.stat.dashboard.service.widget.grouper.GrouperUtil.buildCollector;
import static ee.stat.dashboard.service.widget.grouper.GrouperUtil.dimensionFilter;
import static ee.stat.dashboard.service.widget.grouper.GrouperUtil.oneDimensionGroup;
import static ee.stat.dashboard.service.widget.grouper.GrouperUtil.threeDimensionGroup;
import static ee.stat.dashboard.service.widget.grouper.GrouperUtil.twoDimensionGroup;
import static java.lang.String.format;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;

@Service
public class DatapointGrouper {

    public Graph createDiagram(List<DataPoint> dataPoints) throws GraphGroupingException {
        return createDiagram(dataPoints, null, null, null, null);
    }

    public Graph createDiagram(List<DataPoint> dataPoints, String abscissa, String timeName, AxisOrder order, List<String> options) throws GraphGroupingException {
        if (isEmpty(dataPoints)) {
            return Graph.builder().build();
        }

        try {
            return makeGraph(dataPoints, abscissa, timeName, order, options);
        } catch (Exception e) {
            throw new GraphGroupingException(format("Problem grouping datapoints: %s", e.getMessage()), e);
        }
    }

    private Graph makeGraph(List<DataPoint> dataPoints, String abscissa, String timeName, AxisOrder order, List<String> options) {
        DataPoint anyPoint = dataPoints.get(0);
        Grouping grouping = new Grouping(anyPoint, abscissa, timeName, order, options);
        if (grouping.getDimensions() == 0) {
            List<Serie> grouped = dataPoints.stream().collect(buildCollector(grouping));
            return graph(List.of(new FilterSerie(grouped)));
        } else if (grouping.getDimensions() == 1) {
            Map<String, List<Serie>> grouped =
                    oneDimensionGroup(dataPoints, grouping.getGroup1(), buildCollector(grouping));
            return graph(flatten1Lvl(anyPoint, grouping, grouped));
        } else if (grouping.getDimensions() == 2) {
            Map<String, Map<String, List<Serie>>> grouped =
                    twoDimensionGroup(dataPoints, grouping.getGroup1(), grouping.getGroup2(), buildCollector(grouping));
            return graph(flatten2Lvl(anyPoint, grouping, grouped));
        } else {
            Map<String, Map<String, Map<String, List<Serie>>>> grouped =
                    threeDimensionGroup(dataPoints, grouping.getGroup1(), grouping.getGroup2(), grouping.getGroup3(), buildCollector(grouping));
            return graph(flatten3Lvl(anyPoint, grouping, grouped));
        }
    }

    private List<FilterSerie> flatten1Lvl(DataPoint anyPoint, Grouping grouping, Map<String, List<Serie>> grouped) {
        List<FilterSerie> list = new ArrayList<>();
        for (Map.Entry<String, List<Serie>> entry : grouped.entrySet()) {
            FilterSerie filterSerie = new FilterSerie();
            LinkedHashMap<String, String> filters = dimensionFilter(grouping, anyPoint, entry);
            filterSerie.setFilters(filters);
            filterSerie.setOrder(new ArrayList<>(filters.keySet()));
            filterSerie.setSeries(entry.getValue());
            list.add(filterSerie);
        }
        return list;
    }

    private List<FilterSerie> flatten2Lvl(DataPoint anyPoint, Grouping grouping, Map<String, Map<String, List<Serie>>> grouped) {
        List<FilterSerie> list = new ArrayList<>();
        for (Map.Entry<String, Map<String, List<Serie>>> entry1 : grouped.entrySet()) {
            for (Map.Entry<String, List<Serie>> entry2 : entry1.getValue().entrySet()) {
                FilterSerie filterSerie = new FilterSerie();
                LinkedHashMap<String, String> filters = dimensionFilter(grouping, anyPoint, entry1, entry2);
                filterSerie.setFilters(filters);
                filterSerie.setOrder(new ArrayList<>(filters.keySet()));
                filterSerie.setSeries(entry2.getValue());
                list.add(filterSerie);
            }
        }
        return list;
    }

    private List<FilterSerie> flatten3Lvl(DataPoint anyPoint, Grouping grouping, Map<String, Map<String, Map<String, List<Serie>>>> grouped) {
        List<FilterSerie> list = new ArrayList<>();
        for (Map.Entry<String, Map<String, Map<String, List<Serie>>>> entry1 : grouped.entrySet()) {
            for (Map.Entry<String, Map<String, List<Serie>>> entry2 : entry1.getValue().entrySet()) {
                for (Map.Entry<String, List<Serie>> entry3 : entry2.getValue().entrySet()) {
                    FilterSerie filterSerie = new FilterSerie();
                    LinkedHashMap<String, String> filters = dimensionFilter(grouping, anyPoint, entry1, entry2, entry3);
                    filterSerie.setFilters(filters);
                    filterSerie.setOrder(new ArrayList<>(filters.keySet()));
                    filterSerie.setSeries(entry3.getValue());
                    list.add(filterSerie);
                }
            }
        }
        return list;
    }

    private Graph graph(List<FilterSerie> list) {
        return Graph.builder()
                .series(list)
                .build();
    }
}
