package ee.stat.dashboard.service.widget.coordinates.exception;

import ee.stat.dashboard.service.widget.diagram.CalculationException;

public class RawDataException extends CalculationException {

    public RawDataException(String message) {
        super(message);
    }

    public RawDataException(String message, Throwable cause) {
        super(message, cause);
    }
}
