package ee.stat.dashboard.service.widget.widget;

import lombok.NoArgsConstructor;

import java.util.List;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class WidgetConstants {
    public static final String MEASURE_ET = "Meede";
    public static final String MEASURE_EN = "Measure";
    public static final String PROGRAM_ET = "Programm";
    public static final String PROGRAM_EN = "Programme";
    public static final String DOMAIN_ET = "Valdkond";
    public static final String DOMAIN_EN = "Domain";
    public static final String RESULT_DOMAIN_ET = "Tulemusvaldkond";
    public static final List<String> domainsEt = List.of(DOMAIN_ET, RESULT_DOMAIN_ET);
    public static final String RESULT_DOMAIN_EN = "Performance area";
    public static final String FILTER_VALUE_CUSTOM_PREFIX = "CUSTOM-";
}
