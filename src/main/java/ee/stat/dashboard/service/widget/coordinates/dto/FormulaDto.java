package ee.stat.dashboard.service.widget.coordinates.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FormulaDto {

    private Long id;
    private Long widget;
    private String dimension;
    private String fromDim;
    private String toDim;
    private String fromVal;
    private String toVal;
}
