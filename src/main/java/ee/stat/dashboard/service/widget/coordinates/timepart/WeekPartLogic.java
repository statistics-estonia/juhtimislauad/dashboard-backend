package ee.stat.dashboard.service.widget.coordinates.timepart;

import ee.stat.dashboard.model.dashboard.Unit;
import ee.stat.dashboard.model.dashboard.Week;
import ee.stat.dashboard.model.json.DataPoint;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.enums.TimePart;
import ee.stat.dashboard.service.widget.coordinates.converters.WeekConverter;
import ee.stat.dashboard.service.widget.coordinates.exception.TimePartCalculationException;
import ee.stat.dashboard.service.widget.coordinates.exception.TimePartMappingException;
import ee.stat.dashboard.util.StatDateUtil;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Year;

@Service
class WeekPartLogic implements TimePartLogic {

    @Override
    public TimePart timePart() {
        return TimePart.WEEK;
    }

    @Override
    public Class<? extends Unit> unit() {
        return Week.class;
    }

    @Override
    public Week convert(Language lang, String value) throws TimePartMappingException {
        return WeekConverter.convert(value);
    }

    @Override
    public LocalDate calcDate(Year year, Unit unit) throws TimePartCalculationException, TimePartMappingException {
        if (!(unit instanceof Week)) {
            throw new TimePartMappingException("Wrong unit passed: " + unit.getClass());
        }
        return StatDateUtil.buildWeekDate(year, (Week) unit);
    }

    @Override
    public Unit getUnit(DataPoint dp) {
        return dp.getWeek();
    }

    @Override
    public void setUnit(DataPoint dp, Unit unit) throws TimePartMappingException {
        if (!(unit instanceof Week)) {
            throw new TimePartMappingException("Wrong unit passed: " + unit.getClass());
        }
        dp.setWeek((Week) unit);
    }

    @Override
    public String getValue(no.ssb.jsonstat.v2.Dimension dimension, String valueCode) throws TimePartMappingException {
        return valueCode;
    }
}
