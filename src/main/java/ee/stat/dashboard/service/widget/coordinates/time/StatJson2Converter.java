package ee.stat.dashboard.service.widget.coordinates.time;

import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.Year;
import java.time.temporal.IsoFields;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class StatJson2Converter {

    /**
     * 2020
     */
    public static Year fromYear(String value) {
        return Year.of(Integer.parseInt(value));
    }

    /**
     * 2020
     */
    public static LocalDate fromYearDate(String value) {
        return fromYear(value).atDay(1);
    }

    /**
     * 2020Q1,2020Q4
     */
    public static LocalDate fromQuarterDate(String value) {
        String[] split = value.split("Q");
        return LocalDate.of(Integer.parseInt(split[0]), 1, 1).with(IsoFields.QUARTER_OF_YEAR, Integer.parseInt(split[1]));
    }

    /**
     * 2020M1,2020M12
     */
    public static LocalDate fromMonthDate(String value) {
        String[] split = value.split("M");
        return LocalDate.of(Integer.parseInt(split[0]), Integer.parseInt(split[1]), 1);
    }
}
