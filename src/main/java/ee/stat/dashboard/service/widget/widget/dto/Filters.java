package ee.stat.dashboard.service.widget.widget.dto;

import ee.stat.dashboard.model.classifier.Element;
import ee.stat.dashboard.model.widget.back.GraphType;
import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.enums.MapRule;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;

@Getter
@Setter
@Builder
public class Filters {

    private Element ehak;
    private Language lang;
    private GraphType graphType;
    private Map<String, String> params;
    private List<UserFilterDto> userFilters;

    public MapRule getMapRule() {
        return this.getGraphType().getMapRule();
    }

    public GraphTypeEnum getType() {
        return this.getGraphType().getType();
    }

    public List<String> getEhakNamesUpper() {
        if (isEmpty(ehak.getAlternatives())){
            return singletonList(ehak.getName(lang).toUpperCase());
        }
        List<String> values = ehak.getAlternatives().stream()
                .map(ea -> ea.getName(lang).toUpperCase())
                .collect(toList());
        values.add(ehak.getName(lang).toUpperCase());
        return values;
    }

    public Integer getEhakLevel(){
        return ehak.getLevel();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Filters filters = (Filters) o;
        return lang == filters.lang &&
                Objects.equals(params, filters.params) &&
                Objects.equals(ehak, filters.ehak) &&
                Objects.equals(graphType, filters.graphType) &&
                Objects.equals(userFilters, filters.userFilters);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lang, params, ehak, graphType, userFilters);
    }
}
