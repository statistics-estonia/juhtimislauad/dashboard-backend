package ee.stat.dashboard.service.widget.widget.timeperiod;

import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.util.StatDateUtil;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import static ee.stat.dashboard.model.widget.back.enums.TimePeriod.QUARTER;
import static java.time.Month.APRIL;
import static java.time.Month.JANUARY;
import static java.time.Month.JULY;
import static java.time.Month.OCTOBER;

@Service
class QuarterPeriodLogic implements TimePeriodLogic {

    private static final List<Month> QUARTER_MONTHS = List.of(JANUARY, APRIL, JULY, OCTOBER);

    @Override
    public TimePeriod timePeriod() {
        return QUARTER;
    }

    @Override
    public LocalDate generateStartDate(LocalDate to, Integer periods) {
        return StatDateUtil.beforeDateQuarter(to, periods);
    }

    @Override
    public List<LocalDate> generateDates(LocalDate from, LocalDate to) {
        return StatDateUtil.generateDatesQuarter(from, to);
    }

    @Override
    public boolean valid(LocalDate date) {
        return date == null || (QUARTER_MONTHS.contains(date.getMonth()) && date.getDayOfMonth() == 1);
    }
}
