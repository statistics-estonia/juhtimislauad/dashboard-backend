package ee.stat.dashboard.service.widget.widget.dto;

import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.service.dashboard.dto.DashboardResponse;
import ee.stat.dashboard.service.element.dto.DomainResponse;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class WidgetResponse {

    private Long id;
    private String code;
    private Language lang;
    private TimePeriod timePeriod;
    private Integer precisionScale;
    private String shortname;
    private String name;
    private String description;
    private String note;
    private String unit;
    private String source;
    private List<DomainResponse> elements;
    private List<DashboardResponse> dashboards;
    private LocalDateTime dataUpdatedAt;
    private List<StatDataDto> statCubes;
    private List<String> statData;
    private List<String> methodsLinks;
    private List<String> statisticianJobLinks;
    private List<String> excels;
    private GraphTypeDto graphTypes;
    private GraphTypeEnum graphType;

    private UserWidgetDto preferences;

    private GraphDto diagram;
}
