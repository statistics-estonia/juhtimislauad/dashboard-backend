package ee.stat.dashboard.service.widget.widget;


import ee.stat.dashboard.model.user.widget.UserFilter;
import ee.stat.dashboard.model.user.widget.UserFilterValue;
import ee.stat.dashboard.model.user.widget.UserGraphType;
import ee.stat.dashboard.model.user.widget.UserLegend;
import ee.stat.dashboard.model.user.widget.UserLegendValue;
import ee.stat.dashboard.repository.UserFilterRepository;
import ee.stat.dashboard.repository.UserFilterValueRepository;
import ee.stat.dashboard.repository.UserLegendRepository;
import ee.stat.dashboard.repository.UserLegendValueRepository;
import ee.stat.dashboard.service.widget.widget.dto.GraphTypeDto;
import ee.stat.dashboard.service.widget.widget.dto.UserFilterDto;
import ee.stat.dashboard.service.widget.widget.dto.UserGraphTypeDto;
import ee.stat.dashboard.service.widget.widget.dto.UserLegendDto;
import ee.stat.dashboard.service.widget.widget.dto.UserLegendValueDto;
import ee.stat.dashboard.service.widget.widget.dto.UserWidgetDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
@AllArgsConstructor
public class UserWidgetPreferencesService {

    private final UserFilterRepository userFilterRepository;
    private final UserLegendRepository userLegendRepository;
    private final UserFilterValueRepository userFilterValueRepository;
    private final UserLegendValueRepository userLegendValueRepository;

    public UserWidgetDto convert(GraphTypeDto graphTypes) {
        UserWidgetDto preferences = new UserWidgetDto();
        preferences.setDefaultGraphType(graphTypes.getDefaultOption());
        if (isNotEmpty(graphTypes.getUserGraphTypes())) {
            preferences.setGraphs(mapCharts(graphTypes.getUserGraphTypes()));
        }
        return preferences;
    }

    private List<UserGraphTypeDto> mapCharts(List<UserGraphType> userGraphTypes) {
        return userGraphTypes.stream()
                .map(this::mapChart)
                .collect(toList());
    }

    private UserGraphTypeDto mapChart(UserGraphType type) {
        UserGraphTypeDto userGraphTypeDto = new UserGraphTypeDto();
        userGraphTypeDto.setGraphType(type.getType());
        userGraphTypeDto.setFilters(mapFilters(type));
        userGraphTypeDto.setLegends(mapLegends(type));
        return userGraphTypeDto;
    }

    private List<UserFilterDto> mapFilters(UserGraphType type) {
        return userFilterRepository.findAllByUserGraphType(type.getId()).stream()
                .map(this::mapUserFilters)
                .collect(toList());
    }

    private List<UserLegendDto> mapLegends(UserGraphType type) {
        return userLegendRepository.findAllByUserGraphType(type.getId()).stream()
                .map(this::mapUserLegends)
                .collect(toList());
    }

    private UserFilterDto mapUserFilters(UserFilter userFilter) {
        UserFilterDto userFilterDto = new UserFilterDto();
        userFilterDto.setFilter(userFilter.getFilter());
        userFilterDto.setValues(mapToValueIds(userFilter));
        return userFilterDto;
    }

    private UserLegendDto mapUserLegends(UserLegend userLegend) {
        UserLegendDto userFilterDto = new UserLegendDto();
        if (userLegend.isTarget()) {
            userFilterDto.setTarget(true);
        }
        userFilterDto.setValues(mapLegendValues(userLegend));
        return userFilterDto;
    }

    private List<Long> filterValueIds(List<UserFilterValue> filterValues) {
        return filterValues.stream().map(UserFilterValue::getFilterValue).collect(toList());
    }

    private List<Long> mapToValueIds(UserFilter userFilter) {
        return filterValueIds(userFilterValueRepository.findAllByUserFilter(userFilter.getId()));
    }

    private List<UserLegendValueDto> mapLegendValues(UserLegend userLegend) {
        return userLegendValueRepository.findAllByUserLegend(userLegend.getId()).stream()
                .map(this::convert)
                .collect(toList());
    }

    private UserLegendValueDto convert(UserLegendValue e) {
        UserLegendValueDto dto = new UserLegendValueDto();
        dto.setFilter(e.getFilter());
        dto.setValue(e.getFilterValue());
        return dto;
    }
}
