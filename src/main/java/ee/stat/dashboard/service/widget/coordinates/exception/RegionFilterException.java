package ee.stat.dashboard.service.widget.coordinates.exception;

public class RegionFilterException extends Exception {

    public RegionFilterException(String message) {
        super(message);
    }
}
