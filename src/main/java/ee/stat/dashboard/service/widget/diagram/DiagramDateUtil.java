package ee.stat.dashboard.service.widget.diagram;

import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.model.widget.front.Period;
import ee.stat.dashboard.service.widget.coordinates.dto.MergeResponse;
import ee.stat.dashboard.service.widget.widget.timeperiod.TimePeriodService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

import static ee.stat.dashboard.util.StatDateUtil.afterOrEquals;
import static ee.stat.dashboard.util.StatDateUtil.beforeOrEquals;
import static ee.stat.dashboard.util.StatDateUtil.getEarliest;
import static ee.stat.dashboard.util.StatDateUtil.getLatest;

@Service
@AllArgsConstructor
public class DiagramDateUtil {

    private final TimePeriodService timePeriodService;

    public Period buildPeriod(Widget widget, MergeResponse data) {
        Period period = new Period();
        period.setTo(widget.getEndDate() != null ? widget.getEndDate() : data.getMaxDate());
        period.setFrom(calcFromDate(widget, data));
        return period;
    }

    private LocalDate calcFromDate(Widget widget, MergeResponse data) {
        LocalDate calculatedStartDate;
        if (widget.getPeriods() == null) {
            calculatedStartDate = nullPeriodDate(widget, data);
        } else {
            LocalDate endDate = getEarliest(widget.getEndDate(), data.getMaxDate());
            calculatedStartDate = generateStartDateFromPeriod(endDate, widget.getPeriods(), widget.getTimePeriod());
        }

        if (widget.getStartDate() == null) {
            return calculatedStartDate;
        } else if (afterOrEquals(calculatedStartDate, data.getMinDate())) {
            return calculatedStartDate;
        } else if (beforeOrEquals(data.getMinDate(), widget.getStartDate())) {
            return data.getMinDate();
        } else {
            return getLatest(widget.getStartDate(), calculatedStartDate);
        }
    }

    LocalDate generateStartDateFromPeriod(LocalDate to, Integer periods, TimePeriod timePeriod) {
        return timePeriodService.getLogic(timePeriod).generateStartDate(to, periods - 1);
    }

    private LocalDate nullPeriodDate(Widget widget, MergeResponse data) {
        if (widget.getEndDate() != null && widget.getStartDate() != null) {
            return widget.getStartDate();
        }
        return getEarliest(data.getMinDate(), widget.getEndDate());
    }

    public List<LocalDate> generateDates(LocalDate from, LocalDate to, TimePeriod timePeriod) {
        return timePeriodService.getLogic(timePeriod).generateDates(from, to);
    }
}
