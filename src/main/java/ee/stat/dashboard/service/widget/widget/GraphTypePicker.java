package ee.stat.dashboard.service.widget.widget;

import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.service.widget.widget.dto.GraphTypeDto;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.AllArgsConstructor;

import static ee.stat.dashboard.util.ErrorCode.ILLEGAL_GRAPH_TYPE;
import static ee.stat.dashboard.util.ErrorCode.MISSING_GRAPH_TYPE;
import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor(access = PRIVATE)
public class GraphTypePicker {

    public static GraphTypeEnum getGraphType(String graphTypeStr, GraphTypeDto graphTypes) {
        if (graphTypeStr == null) {
            return graphTypes.getDefaultOption();
        }

        GraphTypeEnum graphType = getGraphType(graphTypeStr);
        if (graphTypes.getOptions().contains(graphType)) {
            return graphType;
        }
        throw new StatBadRequestException(ILLEGAL_GRAPH_TYPE);
    }

    private static GraphTypeEnum getGraphType(String graphTypeStr) {
        try {
            return GraphTypeEnum.valueOf(graphTypeStr);
        } catch (Exception e) {
            throw new StatBadRequestException(MISSING_GRAPH_TYPE);
        }
    }
}
