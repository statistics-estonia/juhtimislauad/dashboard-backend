package ee.stat.dashboard.service.widget.excelsplitter;

import ee.stat.dashboard.service.widget.excelsplitter.dto.ExcelDataPartition;
import ee.stat.dashboard.service.widget.excelsplitter.dto.Partition;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;


@Slf4j
@Service
@AllArgsConstructor
public class ExcelSplitter {

    public ExcelDataPartition toExcelDataPartition(List<List<String>> rows) {
        ExcelDataPartition excelData = new ExcelDataPartition();
        addData(rows, excelData.getData());
        return excelData;
    }

    private void addData(List<List<String>> rest, List<List<List<String>>> data) {
        Partition excel2 = split(rest, s -> s.equals("Andmetüüp"));
        if (isNotEmpty(excel2.getFirst())) {
            data.add(excel2.getFirst());
        }
        if (isNotEmpty(excel2.getRest())) {
            Partition excel3 = split(excel2.getRest(), s -> s.equals("Andmetüüp"));
            data.add(excel3.getFirst());
        }
    }

    private Partition split(List<List<String>> rows, Predicate<String> condition) {
        List<List<String>> first = new ArrayList<>();
        List<List<String>> second = new ArrayList<>();
        boolean isMeta = true;
        boolean ignoreRule = true;
        for (List<String> row : rows) {
            if (row.isEmpty()) continue;
            if (condition.test(row.get(0))) {
                if (ignoreRule) {
                    ignoreRule = false;
                } else {
                    isMeta = false;
                }
            }
            if (isMeta) {
                first.add(row);
            } else {
                second.add(row);
            }
        }
        return new Partition(first, second);
    }
}
