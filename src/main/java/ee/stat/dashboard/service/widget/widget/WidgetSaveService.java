package ee.stat.dashboard.service.widget.widget;

import ee.stat.dashboard.config.security.StatUser;
import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.user.widget.UserWidget;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.repository.UserWidgetRepository;
import ee.stat.dashboard.service.dashboard.DashboardFinder;
import ee.stat.dashboard.service.widget.widget.dto.PinningStrategy;
import ee.stat.dashboard.service.widget.widget.dto.UserWidgetDto;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static ee.stat.dashboard.util.ErrorCode.USER_WIDGET_IS_NOT_EXISTING;
import static ee.stat.dashboard.util.StatListUtil.first;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;

@Slf4j
@Service
@AllArgsConstructor
@Transactional
public class WidgetSaveService {

    private final UserWidgetRepository userWidgetRepository;
    private final UserWidgetPreferencesSaveService userWidgetPreferencesSaveService;
    private final DashboardFinder dashboardFinder;
    private final WidgetFinder widgetFinder;
    private final WidgetPinService widgetPinService;

    public void savePreferences(Long widgetId, Long dashboardId, StatUser user, UserWidgetDto preferences) {
        List<UserWidget> userWidgets = userWidgetRepository.findByAppUserAndDashboardAndWidget(user.getId(), dashboardId, widgetId);
        if (isEmpty(userWidgets)){
            throw new StatBadRequestException(USER_WIDGET_IS_NOT_EXISTING);
        }
        userWidgetPreferencesSaveService.savePreferences(first(userWidgets), preferences);
    }

    public void savePin(Long requestWidgetId, Long requestDashboardId, StatUser user, UserWidgetDto dto) {
        PinningStrategy pinningStrategy = PinningStrategy.from(dto.getPinned());
        Dashboard dashboard = dashboardFinder.findValidDashboardForPinning(requestDashboardId, pinningStrategy);
        Widget widget = widgetFinder.findWidgetForPinning(requestWidgetId);
        if (pinningStrategy.pin()) {
            widgetPinService.pinFlow(user, dashboard.getId(), widget.getId());
        } else {
            widgetPinService.unpinFlow(user, widget.getId());
        }
    }
}
