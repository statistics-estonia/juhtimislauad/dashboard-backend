package ee.stat.dashboard.service.widget.coordinates.timepart;

import ee.stat.dashboard.model.dashboard.Unit;
import ee.stat.dashboard.model.json.DataPoint;
import ee.stat.dashboard.model.widget.back.Filter;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.enums.TimePart;
import ee.stat.dashboard.service.widget.coordinates.exception.TimePartMappingException;
import lombok.AllArgsConstructor;
import no.ssb.jsonstat.v2.Dimension;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class TimePartService {

    private final List<TimePartLogic> logics;

    public TimePartLogic timePartLogic(TimePart timePart) throws TimePartMappingException {
        return logics.stream()
                .filter(l -> l.timePart() == timePart)
                .findAny().orElseThrow(() -> new TimePartMappingException("unknown time period : " + timePart));
    }

    public void setTimePart(DataPoint dataPoint, Dimension dimension, Filter timePartFilter, String valueCode, Language lang) throws TimePartMappingException {
        TimePartLogic logic = timePartLogic(timePartFilter.getTimePart());
        String value = logic.getValue(dimension, valueCode);
        Unit unit = logic.convert(lang, value);
        logic.setUnit(dataPoint, unit);
        dataPoint.setTimePart(logic.timePart());
    }
}
