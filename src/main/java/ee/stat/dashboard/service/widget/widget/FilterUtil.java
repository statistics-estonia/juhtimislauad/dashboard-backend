package ee.stat.dashboard.service.widget.widget;

import ee.stat.dashboard.model.widget.back.Filter;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.widget.diagram.GraphBuilderException;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class FilterUtil {

    public static List<Filter> filtersWithValues(List<Filter> dbFilters) {
        return dbFilters.stream()
                .filter(f -> f.getValues().size() > 1 || f.isTime() && f.getValues().size() == 1)
                .collect(toList());
    }

    public static Optional<Filter> getRegion(List<Filter> filters) {
        return filters.stream().filter(Filter::isRegion).findAny();
    }

    public static Map<Language, List<String>> getConstraintNameEt(List<Filter> filters) {
        return Arrays.stream(Language.values())
                .collect(toMap(lang -> lang,
                        lang -> filters.stream().filter(f -> f.getType().isConstraint()).map(f -> f.getName(lang)).collect(toList())));
    }

    public static Map<Language, Map<String, Filter>> getRegularFilter(List<Filter> filters) {
        for (Filter filter : filters) {
            filter.setValueMap(Arrays.stream(Language.values())
                    .collect(toMap(lang -> lang,
                            lang -> filter.getValues().stream()
                                    .collect(toMap(fv -> fv.getValue(lang), fv -> fv)))));
        }
        return Arrays.stream(Language.values())
                .collect(toMap(lang -> lang,
                        lang -> filters.stream()
                                .filter(f -> f.getType().logicFilter())
                                .filter(f -> !f.isTime())
                                .filter(f -> !f.isRegion())
                                .collect(toMap(f -> f.getName(lang), f -> f))));
    }

    public static Filter timeFilterOrThrow(List<Filter> filters) {
        return filters.stream().filter(Filter::isTime).findAny().orElseThrow(() -> new RuntimeException("Time dimension is missing"));
    }

    public static Filter timeFilter(List<Filter> filters) throws GraphBuilderException {
        return filters.stream().filter(Filter::isTime).findAny().orElseThrow(() -> new GraphBuilderException("time filter is missing"));
    }

    public static Optional<Filter> getTimePart(List<Filter> filters) {
        return filters.stream().filter(f -> f.getType() != null && f.getType().isTimePart()).findAny();
    }

    public static Filter axisFilter(List<Filter> filters) throws GraphBuilderException {
        return filters.stream().filter(f -> f.getType().isAxis()).findAny().orElseThrow(() -> new GraphBuilderException("axis filter is missing"));
    }

    public static List<String> values(Filter filter, Language lang) {
        return filter.getValues().stream().map(v -> v.getFrontValue(lang)).collect(toList());
    }
}
