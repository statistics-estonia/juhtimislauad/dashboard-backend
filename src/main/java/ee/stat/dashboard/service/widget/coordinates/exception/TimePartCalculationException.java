package ee.stat.dashboard.service.widget.coordinates.exception;

import lombok.Getter;

@Getter
public class TimePartCalculationException extends Exception {

    public TimePartCalculationException(String message) {
        super(message);
    }
}
