package ee.stat.dashboard.service.widget.widget;

import ee.stat.dashboard.model.widget.back.statdataapi.StatDataApi;
import ee.stat.dashboard.repository.StatDataApiRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class StatDataApiCache {

    private final StatDataApiRepository statDataApiRepository;

    @Cacheable(value = "StatDataApiCache_findByWidget", key = "#widget")
    public List<StatDataApi> findByWidget(Long widget){
        return statDataApiRepository.findByWidget(widget);
    }
}
