package ee.stat.dashboard.service.widget.diagram;

import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.front.Diagram;
import ee.stat.dashboard.repository.DiagramDataRepository;
import ee.stat.dashboard.repository.DiagramRepository;
import ee.stat.dashboard.repository.WidgetRepository;
import ee.stat.dashboard.service.admin.widget.WidgetDeleteService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static ee.stat.dashboard.model.widget.front.enums.DiagramStatus.APPROVED;
import static java.time.LocalDateTime.now;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.springframework.transaction.annotation.Propagation.REQUIRES_NEW;

@Slf4j
@Service
@AllArgsConstructor
public class DiagramBuilder {

    private final WidgetRepository widgetRepository;
    private final DiagramDataRepository diagramDataRepository;
    private final DiagramRepository diagramRepository;
    private final GraphBuilder graphBuilder;
    private final WidgetDeleteService widgetDeleteService;

    @Transactional(propagation = REQUIRES_NEW)
    public void createNewDiagramForUpdater(Long widgetId) throws CalculationException {
        Widget widget = widgetRepository.findById(widgetId).orElseThrow(RuntimeException::new);

        Diagram diagram = buildDiagramInner(widget);
        saveDiagram(diagram);
    }

    public void createNewDiagramForUpdaterWithoutTrans(Long widgetId) throws CalculationException {
        Widget widget = widgetRepository.findById(widgetId).orElseThrow(RuntimeException::new);

        Diagram diagram = buildDiagramInner(widget);
        saveDiagram(diagram);
    }

    private void saveDiagram(Diagram diagram) {
        if (diagram != null && isNotEmpty(diagram.getDiagramData())) {
            widgetDeleteService.deleteDiagrams(diagram.getWidget());

            Diagram savedDiagram = diagramRepository.save(diagram);
            diagram.getDiagramData().forEach(e -> e.setDiagram(savedDiagram.getId()));
            diagramDataRepository.saveAll(diagram.getDiagramData());
        }
    }

    private Diagram buildDiagramInner(Widget widget) throws CalculationException {
        Diagram diagram = new Diagram();
        Long widgetId = widget.getId();

        diagram.setWidget(widgetId);
        diagram.setCreatedAt(now());
        diagram.setStatus(APPROVED);
        diagram.setDiagramData(graphBuilder.buildDiagramGraphs(widget));
        return diagram;
    }
}
