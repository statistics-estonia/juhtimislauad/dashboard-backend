package ee.stat.dashboard.service.widget.diagram;

import ee.stat.dashboard.model.json.CoordinatedResponse;
import ee.stat.dashboard.model.json.DataPoint;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

import static java.math.RoundingMode.HALF_UP;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class DiagramMath {

    public static void round(CoordinatedResponse modifiedResponse, Integer decimalPoints) {
        for (DataPoint dataPoint : modifiedResponse.getDataPoints()) {
            BigDecimal value = dataPoint.getValue();
            if (value != null) {
                dataPoint.setValue(value.setScale(decimalPoints, HALF_UP));
            }
        }
    }

    public static void divideValues(CoordinatedResponse modifiedResponse, BigDecimal divisor) {
        for (DataPoint dataPoint : modifiedResponse.getDataPoints()) {
            BigDecimal value = dataPoint.getValue();
            if (value != null) {
                dataPoint.setValue(value.divide(divisor, HALF_UP));
            }
        }
    }
}
