package ee.stat.dashboard.service.widget.widget.dto;

import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.MapType;
import ee.stat.dashboard.model.widget.front.FilterSerie;
import ee.stat.dashboard.model.widget.front.Period;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class GraphDto {

    private Long diagramId;
    private Long diagramDataId;
    private LocalDateTime diagramCreatedAt;
    private Boolean empty;
    private MapType mapType;
    private Period period;
    private GraphTypeEnum graphType;
    private List<FilterDto> filters;
    private List<FilterSerie> series;

    public static GraphDto empty(){
        GraphDto graphDto = new GraphDto();
        graphDto.setEmpty(true);
        graphDto.setFilters(new ArrayList<>());
        graphDto.setSeries(new ArrayList<>());
        return graphDto;
    }
}
