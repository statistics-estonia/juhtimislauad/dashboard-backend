package ee.stat.dashboard.service.widget.widget.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
public class UserLegendValueDto {

    private Long filter;
    private Long value;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserLegendValueDto that = (UserLegendValueDto) o;
        return Objects.equals(filter, that.filter) &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(filter, value);
    }
}
