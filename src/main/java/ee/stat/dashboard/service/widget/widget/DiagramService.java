package ee.stat.dashboard.service.widget.widget;

import ee.stat.dashboard.model.widget.back.GraphType;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.front.Diagram;
import ee.stat.dashboard.model.widget.front.DiagramData;
import ee.stat.dashboard.service.widget.diagram.DiagramServiceCache;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@AllArgsConstructor
public class DiagramService {

    private final DiagramServiceCache diagramServiceCache;

    public Diagram findDiagram(Widget widget) {
        Optional<Diagram> diagramOp = diagramServiceCache.findDiagram(widget.getId());
        if (diagramOp.isPresent()) {
            return diagramOp.get();
        } else {
            log.error("Diagram is missing, should not happen for widget {}", widget.getId());
            return null;
        }
    }

    public DiagramData findDiagramData(Long widgetId, Long diagramId, GraphType graphType) {
        Optional<DiagramData> graphOp = diagramServiceCache.findDiagramGraph(graphType.getId(), diagramId);
        if (graphOp.isPresent()) {
            return graphOp.get();
        } else {
            log.error("Diagram is missing, should not happen widget {} diagram {} type {} {}",
                    widgetId, diagramId, graphType, graphType.getType());
            return null;
        }
    }
}
