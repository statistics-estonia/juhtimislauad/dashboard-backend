package ee.stat.dashboard.service.widget.widget;

import ee.stat.dashboard.model.widget.back.FilterValueCustom;
import ee.stat.dashboard.service.admin.widget.dto.FilterValueCustomAdminResponse;
import lombok.NoArgsConstructor;

import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class FilterValueCustomConverter {

    public static FilterValueCustom mapToEntity(Long widgetId, FilterValueCustomAdminResponse adminResponse) {
        FilterValueCustom entity = FilterValueCustom.builder()
                .widget(widgetId)
                .displayNameEt(adminResponse.getDisplayNameEt())
                .displayNameEn(adminResponse.getDisplayNameEn())
                .dimension1NameEt(adminResponse.getDimension1NameEt())
                .dimension1NameEn(adminResponse.getDimension1NameEn())
                .selectionValue1(adminResponse.getSelectionValue1())
                .selectionValue2(adminResponse.getSelectionValue2())
                .operation(adminResponse.getOperation())
                .build();

        if (nonNull(adminResponse.getDimension2NameEt()))
            entity.setDimension2NameEt(adminResponse.getDimension2NameEt());
        if (nonNull(adminResponse.getDimension2NameEn()))
            entity.setDimension2NameEn(adminResponse.getDimension2NameEn());

        return entity;
    }

    public static FilterValueCustomAdminResponse mapToDto(FilterValueCustom filterValueCustom) {
        return FilterValueCustomAdminResponse.builder()
                .displayNameEt(filterValueCustom.getDisplayNameEt())
                .displayNameEn(filterValueCustom.getDisplayNameEn())
                .dimension1NameEt(filterValueCustom.getDimension1NameEt())
                .dimension1NameEn(filterValueCustom.getDimension1NameEn())
                .dimension2NameEt(filterValueCustom.getDimension2NameEt())
                .dimension2NameEn(filterValueCustom.getDimension2NameEn())
                .selectionValue1(filterValueCustom.getSelectionValue1())
                .selectionValue2(filterValueCustom.getSelectionValue2())
                .operation(filterValueCustom.getOperation())
                .build();
    }
}
