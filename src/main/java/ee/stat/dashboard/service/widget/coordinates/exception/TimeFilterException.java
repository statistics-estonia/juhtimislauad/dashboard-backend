package ee.stat.dashboard.service.widget.coordinates.exception;

public class TimeFilterException extends Exception {

    public TimeFilterException(String message) {
        super(message);
    }
}
