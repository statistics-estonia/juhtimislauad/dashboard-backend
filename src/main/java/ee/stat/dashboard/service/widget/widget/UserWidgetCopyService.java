package ee.stat.dashboard.service.widget.widget;

import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.dashboard.DashboardWidget;
import ee.stat.dashboard.model.user.widget.UserFilter;
import ee.stat.dashboard.model.user.widget.UserFilterValue;
import ee.stat.dashboard.model.user.widget.UserGraphType;
import ee.stat.dashboard.model.user.widget.UserLegend;
import ee.stat.dashboard.model.user.widget.UserLegendValue;
import ee.stat.dashboard.model.user.widget.UserWidget;
import ee.stat.dashboard.repository.UserFilterRepository;
import ee.stat.dashboard.repository.UserFilterValueRepository;
import ee.stat.dashboard.repository.UserGraphTypeRepository;
import ee.stat.dashboard.repository.UserLegendRepository;
import ee.stat.dashboard.repository.UserLegendValueRepository;
import ee.stat.dashboard.repository.UserWidgetRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class UserWidgetCopyService {

    private final UserWidgetRepository userWidgetRepository;
    private final UserGraphTypeRepository userGraphTypeRepository;
    private final UserFilterRepository userFilterRepository;
    private final UserFilterValueRepository userFilterValueRepository;
    private final UserLegendRepository userLegendRepository;
    private final UserLegendValueRepository userLegendValueRepository;

    public void saveAsCopy(UserWidget userWidget, Dashboard myDashboard, DashboardWidget dashboardWidget, int order) {
        UserWidget newUserWidget = copy(userWidget, myDashboard, dashboardWidget, order);
        List<UserGraphType> userGraphTypes = userGraphTypeRepository.findAllByUserWidget(userWidget.getId());
        for (UserGraphType userGraphType : userGraphTypes) {
            UserGraphType newUserGraph = copy(userGraphType, newUserWidget);
            for (UserFilter userFilter : userFilterRepository.findAllByUserGraphType(userGraphType.getId())) {
                UserFilter newUserFilter = copy(userFilter, newUserGraph);
                for (UserFilterValue userValue : userFilterValueRepository.findAllByUserFilter(userFilter.getId())) {
                    copy(userValue, newUserFilter);
                }
            }
            for (UserLegend userLegend : userLegendRepository.findAllByUserGraphType(userGraphType.getId())) {
                UserLegend newUserLegend = copy(userLegend, newUserGraph);
                for (UserLegendValue userLegendValue : userLegendValueRepository.findAllByUserLegend(userLegend.getId())) {
                    copy(userLegendValue, newUserLegend);
                }
            }
        }
    }

    private UserWidget copy(UserWidget existing, Dashboard reference, DashboardWidget dashboardWidget, int order) {
        UserWidget newItem = new UserWidget();
        BeanUtils.copyProperties(existing, newItem);
        newItem.setId(null);
        newItem.setDashboard(reference.getId());
        newItem.setWidgetDomain(null);
        newItem.setDashboardWidget(dashboardWidget.getId());
        newItem.setOrderNr(order);
        return userWidgetRepository.save(newItem);
    }

    private UserGraphType copy(UserGraphType existing, UserWidget reference) {
        UserGraphType newItem = new UserGraphType();
        BeanUtils.copyProperties(existing, newItem);
        newItem.setId(null);
        newItem.setUserWidget(reference.getId());
        return userGraphTypeRepository.save(newItem);
    }

    private UserFilter copy(UserFilter existing, UserGraphType reference) {
        UserFilter newItem = new UserFilter();
        BeanUtils.copyProperties(existing, newItem);
        newItem.setId(null);
        newItem.setUserGraphType(reference.getId());
        return userFilterRepository.save(newItem);
    }

    private UserFilterValue copy(UserFilterValue existing, UserFilter reference) {
        UserFilterValue newItem = new UserFilterValue();
        BeanUtils.copyProperties(existing, newItem);
        newItem.setId(null);
        newItem.setUserFilter(reference.getId());
        return userFilterValueRepository.save(newItem);
    }

    private UserLegend copy(UserLegend existing, UserGraphType reference) {
        UserLegend newItem = new UserLegend();
        BeanUtils.copyProperties(existing, newItem);
        newItem.setId(null);
        newItem.setUserGraphType(reference.getId());
        return userLegendRepository.save(newItem);
    }

    private UserLegendValue copy(UserLegendValue existing, UserLegend reference) {
        UserLegendValue newItem = new UserLegendValue();
        BeanUtils.copyProperties(existing, newItem);
        newItem.setId(null);
        newItem.setUserLegend(reference.getId());
        return userLegendValueRepository.save(newItem);
    }
}
