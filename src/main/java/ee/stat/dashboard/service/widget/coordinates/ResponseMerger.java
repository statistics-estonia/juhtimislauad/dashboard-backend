package ee.stat.dashboard.service.widget.coordinates;

import ee.stat.dashboard.model.json.CoordinatedResponse;
import ee.stat.dashboard.model.json.DataPoint;
import ee.stat.dashboard.service.widget.coordinates.dto.MergeResponse;
import ee.stat.dashboard.service.widget.coordinates.exception.RawDataException;
import ee.stat.dashboard.util.StatListUtil;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;

import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;
import static lombok.AccessLevel.PRIVATE;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;

/**
 * Data points are filtered so that each date has only one point
 * Priority is based on source
 */
@NoArgsConstructor(access = PRIVATE)
public class ResponseMerger {

    public static MergeResponse mergeToSingleResponse(List<CoordinatedResponse> responses) throws RawDataException {
        if (isEmpty(responses)) {
            return null;
        }
        validateDatapoints(responses);
        if (responses.size() == 1) {
            CoordinatedResponse response = StatListUtil.first(responses);
            TreeSet<LocalDate> uniqueDates = response.getDataPoints().stream().map(DataPoint::getDate).collect(toCollection(TreeSet::new));
            return new MergeResponse(response, 1, null, uniqueDates.last(), uniqueDates.first());
        }
        return mergePoints(responses);
    }

    private static void validateDatapoints(List<CoordinatedResponse> responses) throws RawDataException {
        for (CoordinatedResponse coordinatedResponse : responses) {
            if (isEmpty(coordinatedResponse.getDataPoints())) {
                throw new RawDataException("datapoints are empty for response with source: " + coordinatedResponse.getSource());
            }
            for (DataPoint dataPoint : coordinatedResponse.getDataPoints()) {
                if (dataPoint.getDate() == null) {
                    throw new RawDataException("invalid datapoint, date is null: " + dataPoint);
                }
            }
        }
    }

    private static MergeResponse mergePoints(List<CoordinatedResponse> responses) {
        //todo2 set dim1's to be the same type

        boolean hasExpecations = responses.stream().anyMatch(r -> r.getSource().isExpectation());
        Optional<LocalDate> maxRealDateOp = hasExpecations ? maxRealDate(responses) : Optional.empty();
        List<DataPoint> allDataPoints = responses.stream()
                .map(CoordinatedResponse::getDataPoints)
                .flatMap(Collection::stream)
                .sorted(Comparator.comparing(DataPoint::getSource))
                .collect(toList());
        List<DataPoint> uniqueDataPoints = new ArrayList<>();
        for (DataPoint dataPoint : allDataPoints) {
            if (ifExpectationAndIsBeforeExpectationStart(hasExpecations, maxRealDateOp, dataPoint)) {
                continue;
            }
            if (uniqueDataPoints.stream().noneMatch(d -> d.equalTo(dataPoint))) {
                uniqueDataPoints.add(dataPoint);
            }
        }
        CoordinatedResponse response = new CoordinatedResponse(new ArrayList<>(uniqueDataPoints));
        TreeSet<LocalDate> uniqueDates = uniqueDataPoints.stream().map(DataPoint::getDate).collect(toCollection(TreeSet::new));
        return new MergeResponse(response, responses.size(), maxRealDateOp.orElse(null), uniqueDates.last(), uniqueDates.first());
    }

    private static boolean ifExpectationAndIsBeforeExpectationStart(boolean hasExpecations, Optional<LocalDate> maxRealDateOp, DataPoint dataPoint) {
        return hasExpecations &&
                maxRealDateOp.isPresent() &&
                dataPoint.getSource().isExpectation() &&
                !dataPoint.getDate().isAfter(maxRealDateOp.get());
    }

    private static Optional<LocalDate> maxRealDate(List<CoordinatedResponse> responses) {
        return responses.stream()
                .map(CoordinatedResponse::getDataPoints)
                .flatMap(Collection::stream)
                .filter(dataPoint -> dataPoint.getSource().isRealData())
                .map(DataPoint::getDate)
                .max(LocalDate::compareTo);
    }
}
