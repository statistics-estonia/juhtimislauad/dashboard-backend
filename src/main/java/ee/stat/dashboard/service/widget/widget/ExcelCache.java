package ee.stat.dashboard.service.widget.widget;

import ee.stat.dashboard.model.widget.back.excel.Excel;
import ee.stat.dashboard.repository.ExcelRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ExcelCache {

    private final ExcelRepository excelRepository;

    @Cacheable(value = "ExcelCache_findByWidget", key = "#widget")
    public List<Excel> findByWidget(Long widget){
        return excelRepository.findByWidget(widget);
    }
}
