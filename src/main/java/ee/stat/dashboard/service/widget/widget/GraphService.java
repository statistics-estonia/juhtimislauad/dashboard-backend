package ee.stat.dashboard.service.widget.widget;

import ee.stat.dashboard.model.widget.back.Filter;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.enums.MapType;
import ee.stat.dashboard.model.widget.front.DiagramData;
import ee.stat.dashboard.model.widget.front.FilterSerie;
import ee.stat.dashboard.model.widget.front.Graph;
import ee.stat.dashboard.service.widget.diagram.GraphFiltering;
import ee.stat.dashboard.service.widget.widget.dto.FilterDto;
import ee.stat.dashboard.service.widget.widget.dto.Filters;
import ee.stat.dashboard.service.widget.widget.dto.GraphDto;
import ee.stat.dashboard.service.widget.widget.dto.UserFilterDto;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;

import static ee.stat.dashboard.service.element.EhakLevels.ehakToMapType;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
@AllArgsConstructor
public class GraphService {

    private final GraphFiltering graphFiltering;
    private final FilterService filterService;

    //todo2 this cache had issues in the past and has been disabled
    //    @Cacheable(value = "GraphService_filterData", key = "\"\".concat(#data.hashCode()).concat(\"-\").concat(#config.hashCode())")
    public GraphDto filterData(DiagramData data, Filters config) {
        Language lang = config.getLang();
        List<FilterDto> filterDtos = getFilters(lang, data);
        updateRegionFiltersByEhak(filterDtos, config);
        updateFiltersByUserPreferences(filterDtos, config);

        Graph filteredGraph = graphFiltering.filterGraph(data.getGraph(lang), filterDtos, config);

        GraphDto graphDto = convert(filteredGraph);
        graphDto.setFilters(frontEndFilters(filterDtos));
        graphDto.setMapType(getMapType(config));
        graphDto.setDiagramId(data.getDiagram());
        graphDto.setDiagramDataId(data.getId());
        graphDto.setDiagramCreatedAt(data.getDiagramObj().getCreatedAt());
        return graphDto;
    }

    @Cacheable("GraphService_fullData")
    public GraphDto fullData(DiagramData diagramData, Filters config) {
        Language lang = config.getLang();
        List<FilterDto> filterDtos = getFilters(lang, diagramData);
        updateRegionFiltersByEhak(filterDtos, config);

        GraphDto graphDto = convertFull(diagramData.getGraph(lang));
        graphDto.setFilters(frontEndFilters(filterDtos));
        graphDto.setMapType(getMapType(config));
        graphDto.setDiagramDataId(diagramData.getId());
        graphDto.setDiagramCreatedAt(diagramData.getDiagramObj().getCreatedAt());
        return graphDto;
    }

    private void updateFiltersByUserPreferences(List<FilterDto> filterDtos, Filters config) {
        if (isNotEmpty(config.getUserFilters())) {
            for (FilterDto filter : filterDtos) {
                Optional<UserFilterDto> userFilterOp = config.getUserFilters().stream().filter(v -> v.getFilter().equals(filter.getId())).findAny();
                if (userFilterOp.isPresent()) {
                    UserFilterDto userFilter = userFilterOp.get();
                    filter.getValues().forEach(v -> v.setSelected(userFilter.getValues().contains(v.getId())));
                    filter.recalcDefaultOptions();
                }
            }
        }
    }

    private void updateRegionFiltersByEhak(List<FilterDto> filterDtos, Filters config) {
        for (FilterDto filter : filterDtos) {
            if (BooleanUtils.isTrue(filter.getRegion())) {
                filter.getValues().forEach(v -> v.setSelected(config.getEhakNamesUpper().contains(v.getOption().toUpperCase()) ? true : null));
                filter.recalcDefaultOptions();
            }
        }
    }

    private MapType getMapType(Filters config) {
        if (!config.getType().isMap()) {
            return null;
        }
        return config.getMapRule().isDynamic() ?
                ehakToMapType(config.getEhakLevel()) :
                config.getMapRule().toMapType();
    }

    private GraphDto convert(Graph graph) {
        GraphDto dto = new GraphDto();
        dto.setPeriod(graph.getPeriod());
        List<FilterSerie> series = graph.getSeries();
        if (isNotEmpty(series)) {
            for (FilterSerie filterSerie : series) {
                if (filterSerie.getFilters() != null && filterSerie.getFilters().size() > 1) {
                    LinkedHashMap<String, String> newFilters = new LinkedHashMap<>();
                    for (String key : filterSerie.getOrder()) {
                        newFilters.put(key, filterSerie.getFilters().get(key));
                    }
                    filterSerie.setFilters(newFilters);
                }
            }
        }
        dto.setSeries(series);
        return dto;
    }

    private GraphDto convertFull(Graph graph) {
        GraphDto dto = new GraphDto();
        dto.setPeriod(graph.getPeriod());
        dto.setSeries(graph.getSeries());
        return dto;
    }

    private List<FilterDto> frontEndFilters(List<FilterDto> filterDtos) {
        return filterDtos.stream()
                .filter(f -> f.getType().frontEndFilters())
                .collect(toList());
    }

    private List<FilterDto> getFilters(Language lang, DiagramData diagramData) {
        List<Filter> filters = filterService.getDbFilters(diagramData.getGraphType());
        return filterService.mapFiltersWithValues(filters, lang);
    }
}
