package ee.stat.dashboard.service.widget.diagram;

public class CalculationException extends Exception {

    public CalculationException(String message) {
        super(message);
    }

    public CalculationException(String message, Throwable cause) {
        super(message, cause);
    }
}
