package ee.stat.dashboard.service.widget.coordinates.converters;

import ee.stat.dashboard.model.dashboard.Quarter;
import ee.stat.dashboard.service.widget.coordinates.exception.TimePartMappingException;
import lombok.NoArgsConstructor;

import static ee.stat.dashboard.model.dashboard.Quarter.I;
import static ee.stat.dashboard.model.dashboard.Quarter.II;
import static ee.stat.dashboard.model.dashboard.Quarter.III;
import static ee.stat.dashboard.model.dashboard.Quarter.IV;
import static java.lang.String.format;
import static lombok.AccessLevel.PRIVATE;
import static org.apache.commons.lang3.StringUtils.countMatches;

@NoArgsConstructor(access = PRIVATE)
public class QuarterConverter {

    public static Quarter convert(String value) throws TimePartMappingException {
        if (q1AndQ4(value)) {
            throw new TimePartMappingException(format("%s is not a valid quarter", value));
        } else if (quarter4(value)) {
            return IV;
        } else if (quarter3(value)) {
            return III;
        } else if (quarter2(value)) {
            return II;
        } else if (quarter1(value)) {
            return I;
        } else {
            throw new TimePartMappingException(format("%s is not a valid quarter", value));
        }
    }

    private static boolean quarter1(String value) {
        return value.contains("1") || countMatches(value, "I") == 1 && !value.contains("IV");
    }

    private static boolean quarter2(String value) {
        return value.contains("2") || countMatches(value, "II") == 1;
    }

    private static boolean quarter3(String value) {
        return value.contains("3") || countMatches(value, "III") == 1;
    }

    private static boolean quarter4(String value) {
        return value.contains("4") || value.contains("IV");
    }

    private static boolean q1AndQ4(String value) {
        if (value.contains("IV")) {
            return value.replace("IV", "").contains("I");
        }
        if (value.contains("4")) {
            return value.replace("4", "").contains("1");
        }
        return false;
    }
}
