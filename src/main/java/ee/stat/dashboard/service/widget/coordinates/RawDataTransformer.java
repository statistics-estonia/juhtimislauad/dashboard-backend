package ee.stat.dashboard.service.widget.coordinates;

import ee.stat.dashboard.model.json.CoordinatedResponse;
import ee.stat.dashboard.model.json.CoordinatedResponses;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.excel.Excel;
import ee.stat.dashboard.service.statdata.StatMetaFetchException;
import ee.stat.dashboard.service.widget.coordinates.dto.MergeResponse;
import ee.stat.dashboard.service.widget.coordinates.exception.RawDataException;
import ee.stat.dashboard.service.widget.diagram.dto.RawDataConfig;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import static ee.stat.dashboard.model.json.Source.STAT_DATA_API;
import static ee.stat.dashboard.model.json.Source.STAT_DATA_SQL;
import static ee.stat.dashboard.service.widget.coordinates.ResponseMerger.mergeToSingleResponse;
import static ee.stat.dashboard.service.widget.widget.FilterUtil.filtersWithValues;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service
@AllArgsConstructor
public class RawDataTransformer {

    private final FormulaApplier formulaApplier;
    private final StatJson2ToCoordinates statJson2ToCoordinates;

    public MergeResponse transformRowData(RawDataConfig config) throws RawDataException {
        try {
            List<CoordinatedResponse> responses = rawDataTransformer(config);

            //todo2 MUST set dim1 and dim2's to be of the same type
            return mergeToSingleResponse(responses);
        } catch (Exception e) {
            throw new RawDataException(format("Raw data transformer problem: %s", e.getMessage()), e);
        }
    }

    private List<CoordinatedResponse> rawDataTransformer(RawDataConfig config) throws StatMetaFetchException {
        List<CoordinatedResponse> responses = new ArrayList<>();
        if (config.getStatDataApiData() != null) {
            responses.add(statJson2ToCoordinates.convert(
                    config.getStatDataApiData().getResponse(config.getLang()),
                    config.getStatDataApiData().getGraphType(),
                    config.getFilters(),
                    config.getTimePeriod(),
                    config.getLang(),
                    STAT_DATA_API));
        }
        if (config.getStatDataSqlData() != null) {
            responses.add(statJson2ToCoordinates.convert(
                    config.getStatDataSqlData().getResponse(config.getLang()),
                    config.getStatDataSqlData().getGraphType(),
                    config.getFilters(),
                    config.getTimePeriod(),
                    config.getLang(),
                    STAT_DATA_SQL));
        }
        if (config.getExcel() != null) {
            List<CoordinatedResponse> initialExcelResponses = transformExcels(config.getExcel(), config.getLang());
            responses.addAll(formulaApplier.apply(
                    initialExcelResponses,
                    filtersWithValues(config.getFilters()),
                    config.getLang()));
        }
        return responses;
    }

    private List<CoordinatedResponse> transformExcels(Excel excel, Language lang) {
        return excel.getExcelData().stream()
                .map(ed -> ed.getResponse(lang))
                .filter(Objects::nonNull)
                .map(CoordinatedResponses::getResponses)
                .flatMap(Collection::stream)
                .collect(toList());
    }
}
