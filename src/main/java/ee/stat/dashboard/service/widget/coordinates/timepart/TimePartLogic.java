package ee.stat.dashboard.service.widget.coordinates.timepart;

import ee.stat.dashboard.model.dashboard.Unit;
import ee.stat.dashboard.model.json.DataPoint;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.enums.TimePart;
import ee.stat.dashboard.service.widget.coordinates.exception.TimePartCalculationException;
import ee.stat.dashboard.service.widget.coordinates.exception.TimePartMappingException;

import java.time.LocalDate;
import java.time.Year;

public interface TimePartLogic {

    /**
     * What time part does this logic express
     */
    TimePart timePart();

    /**
     * What time unit is used for the logic
     */
    Class<? extends Unit> unit();

    /**
     * Converts stat-json2 value to unit
     */
    Unit convert(Language lang, String value) throws TimePartMappingException;

    /**
     * How datapoint date is calculated using year and time part unit
     */
    LocalDate calcDate(Year year, Unit unit) throws TimePartCalculationException, TimePartMappingException;

    /**
     * gets unit from DataPoint
     */
    Unit getUnit(DataPoint dp);

    /**
     * sets unit to DataPoint
     */
    void setUnit(DataPoint dp, Unit unit) throws TimePartMappingException;

    /**
     * Get value from stat-json2
     */
    String getValue(no.ssb.jsonstat.v2.Dimension dimension, String valueCode) throws TimePartMappingException;
}
