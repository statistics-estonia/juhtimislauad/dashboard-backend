package ee.stat.dashboard.service.widget.coordinates.dto;

import ee.stat.dashboard.model.json.CoordinatedResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MergeResponse {

    private CoordinatedResponse response;
    private int mergedFrom;
    private LocalDate maxRealDataDate;
    private LocalDate maxDate;
    private LocalDate minDate;

}
