package ee.stat.dashboard.service.widget.coordinates.converters;

import lombok.NoArgsConstructor;

import java.math.BigDecimal;

import static java.math.BigDecimal.valueOf;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class ValueConverter {

    public static BigDecimal getValue(Object value) {
        if (value == null) {
            return null;
        } else if (value instanceof Integer) {
            return valueOf((Integer) value);
        } else if (value instanceof Double) {
            return valueOf((Double) value);
        } else {
            throw new IllegalStateException("unknown value type: " + value + " " + value.getClass());
        }
    }
}
