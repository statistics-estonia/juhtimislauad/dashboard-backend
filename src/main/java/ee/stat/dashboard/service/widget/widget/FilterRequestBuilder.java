package ee.stat.dashboard.service.widget.widget;

import ee.stat.dashboard.model.classifier.ClassifierCode;
import ee.stat.dashboard.model.widget.back.GraphType;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.front.FilterSerie;
import ee.stat.dashboard.service.element.EhakLevels;
import ee.stat.dashboard.service.element.ElementServiceCache;
import ee.stat.dashboard.service.widget.widget.dto.FilterDto;
import ee.stat.dashboard.service.widget.widget.dto.FilterValueDto;
import ee.stat.dashboard.service.widget.widget.dto.Filters;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static ee.stat.dashboard.util.StatMapUtil.mapOf;
import static java.util.Collections.emptyMap;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.MapUtils.isEmpty;
import static org.apache.commons.lang3.BooleanUtils.isTrue;
import static org.apache.commons.lang3.StringUtils.isBlank;

@Slf4j
@Service
@AllArgsConstructor
public class FilterRequestBuilder {

    private final ElementServiceCache elementServiceCache;

    public Map<String, List<String>> buildFilterRequest(List<FilterDto> filters, Map<String, String> params) {
        Map<String, List<String>> filterRequest = new HashMap<>();
        for (FilterDto filter : filters) {
            if (filter.getType().isAxis()) {
                //you cannot filter the axis
                continue;
            }
            if (filter.getType().isLegend()) {
                //all legend values are always returned
                putAllOptions(filterRequest, filter);
                continue;
            }
            if (isEmpty(params) || !params.containsKey(filter.getId().toString())) {
                //when there are no
                putDefaultOptions(filterRequest, filter);
                continue;
            }
            String paramValue = params.get(filter.getId().toString());
            if (isBlank(paramValue)) {
                putNoOptions(filterRequest, filter);
                continue;
            }
            try {
                putSelectedOptions(filterRequest, filter, paramValue);
            } catch (Exception ignored) {
                log.error("Error with filters name: " + filter.getId() + " value: " + paramValue);
                putNoOptions(filterRequest, filter);
            }
        }
        return filterRequest;
    }

    public Map<String, List<String>> axisFilterRequest(Filters config, Optional<FilterDto> axisOp) {
        if (verticalAllRule(config.getGraphType())) {
            return emptyMap();
        }
        if (axisOp.isEmpty()) {
            return emptyMap();
        }
        FilterDto axis = axisOp.get();
        if (isTrue(axis.getTime())) {
            return emptyMap();
        }
        if (isTrue(axis.getRegion())) {
            List<Integer> levels = EhakLevels.ehakLevelToRequestLevels(config.getEhakLevel(), config.getGraphType());
            return mapOf(axis.getName(), regionValues(config.getLang(), levels));
        }
        return mapOf(axis.getName(), axis.getUpperOptions());
    }

    private List<String> getOption(FilterDto filter, String paramValue) {
        List<Long> values = getValues(paramValue);
        return filter.getValues().stream()
                .filter(v -> values.contains(v.getId()))
                .map(FilterValueDto::getOption)
                .map(String::toUpperCase)
                .collect(toList());
    }

    private List<Long> getValues(String value) {
        return Arrays.stream(value.split(","))
                .map(Long::parseLong)
                .collect(toList());
    }

    public static boolean hasFilter(FilterSerie filterSerie, Map<String, List<String>> search) {
        Map<String, String> filters = filterSerie.getFilters();
        if (filters == null) return true;

        if (filters.size() != search.size()) {
            return false;
        }
        boolean hasAllFilters = true;
        for (Map.Entry<String, List<String>> searchEntry : search.entrySet()) {
            if (hasAllFilters) {
                String value = filters.get(searchEntry.getKey());
                if (value == null || !hasValue(searchEntry, value)) {
                    hasAllFilters = false;
                }
            }

        }
        return hasAllFilters;
    }

    private static boolean hasValue(Map.Entry<String, List<String>> searchEntry, String value) {
        return searchEntry.getValue().contains(value.toUpperCase());
    }

    private List<String> regionValues(Language lang, List<Integer> levels) {
        return elementServiceCache.findAllLowerNamesByClfCode(ClassifierCode.EHAK, lang, levels).stream()
                .map(String::toUpperCase)
                .collect(toList());
    }

    private boolean verticalAllRule(GraphType graphType) {
        return graphType.getType().isVertical() && graphType.getVerticalRule().isAll();
    }

    private void putSelectedOptions(Map<String, List<String>> filterRequest, FilterDto filter, String paramValue) {
        filterRequest.put(filter.getName(), getOption(filter, paramValue));
    }

    private void putNoOptions(Map<String, List<String>> filterRequest, FilterDto filter) {
        filterRequest.put(filter.getName(), List.of());
    }

    private void putDefaultOptions(Map<String, List<String>> filterRequest, FilterDto filter) {
        filterRequest.put(filter.getName(), filter.getUpperDefaultOptions());
    }

    private void putAllOptions(Map<String, List<String>> filterRequest, FilterDto filter) {
        filterRequest.put(filter.getName(), filter.getUpperOptions());
    }
}
