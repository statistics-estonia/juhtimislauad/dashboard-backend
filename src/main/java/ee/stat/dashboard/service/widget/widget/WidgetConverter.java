package ee.stat.dashboard.service.widget.widget;

import com.google.common.collect.Lists;
import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.repository.DashboardRepository;
import ee.stat.dashboard.service.dashboard.DashboardConverter;
import ee.stat.dashboard.service.dashboard.dto.DashboardResponse;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class WidgetConverter {

    private final WidgetDomainService widgetDomainService;
    private final DashboardRepository dashboardRepository;
    private final DashboardConverter dashboardConverter;

    public WidgetResponse mapMinimal(Widget widget, Language lang) {
        WidgetResponse dto = new WidgetResponse();
        dto.setId(widget.getId());
        dto.setLang(lang);
        dto.setCode(widget.getCode());
        dto.setShortname(widget.getShortname(lang));
        dto.setName(widget.getName(lang));
        return dto;
    }

    public void mapWidgetData(Widget widget, Dashboard dashboard, Language lang, WidgetResponse dto) {
        dto.setTimePeriod(widget.getTimePeriod());
        dto.setPrecisionScale(widget.getPrecisionScale());
        dto.setNote(widget.getNote(lang));
        dto.setUnit(widget.getUnit(lang));
        dto.setSource(widget.getSource(lang));
        dto.setDescription(widget.getDescription(lang));
        if (dashboard != null) {
            if (dashboard.getUserType().isAdmin()) {
                dto.setElements(widgetDomainService.getDomains(widget.getId(), dashboard.getId(), lang));
            } else {
                dto.setDashboards(findAllByWidget(widget.getId(), lang));
            }
        }
        if (widget.getMethodsLink(lang) != null) {
            dto.setMethodsLinks(Lists.newArrayList(widget.getMethodsLink(lang)));
        }
        if (widget.getStatisticianJobLink(lang) != null) {
            dto.setStatisticianJobLinks(Lists.newArrayList(widget.getStatisticianJobLink(lang)));
        }
    }

    public List<DashboardResponse> findAllByWidget(Long widgetId, Language lang) {
        List<Dashboard> dashboards = dashboardRepository.findAllByWidget(widgetId);
        return dashboardConverter.mapSimple(dashboards, lang);
    }
}
