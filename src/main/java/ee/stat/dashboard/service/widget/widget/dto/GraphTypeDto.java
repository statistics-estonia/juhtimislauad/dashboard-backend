package ee.stat.dashboard.service.widget.widget.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ee.stat.dashboard.model.user.widget.UserGraphType;
import ee.stat.dashboard.model.widget.back.GraphType;
import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class GraphTypeDto {

    @JsonIgnore
    private List<GraphType> graphTypes;
    @JsonIgnore
    private List<UserGraphType> userGraphTypes;
    private List<GraphTypeEnum> options;
    private GraphTypeEnum defaultOption;

    public GraphTypeDto(List<GraphType> graphTypes, List<UserGraphType> userGraphTypes, List<GraphTypeEnum> options, GraphTypeEnum defaultOption) {
        this.graphTypes = graphTypes;
        this.userGraphTypes = userGraphTypes;
        this.options = options;
        this.defaultOption = defaultOption;
    }

    public static GraphTypeDto empty() {
        GraphTypeDto empty = new GraphTypeDto();
        empty.setOptions(new ArrayList<>());
        return empty;
    }
}
