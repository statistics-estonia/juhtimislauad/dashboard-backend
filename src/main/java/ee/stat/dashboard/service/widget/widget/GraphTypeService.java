package ee.stat.dashboard.service.widget.widget;

import ee.stat.dashboard.config.security.StatUser;
import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.user.widget.UserGraphType;
import ee.stat.dashboard.model.user.widget.UserWidget;
import ee.stat.dashboard.model.widget.back.GraphType;
import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.repository.UserGraphTypeRepository;
import ee.stat.dashboard.repository.UserWidgetRepository;
import ee.stat.dashboard.service.widget.widget.dto.GraphTypeDto;
import ee.stat.dashboard.util.StatListUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
@AllArgsConstructor
public class GraphTypeService {

    private UserWidgetRepository userWidgetRepository;
    private GraphTypeServiceCache graphTypeServiceCache;
    private UserGraphTypeRepository userGraphTypeRepository;

    public GraphTypeDto mapToGraphType(Dashboard dashboard, Long widgetId, StatUser user) {
        List<GraphType> graphTypes = graphTypeServiceCache.findAllByWidget(widgetId);
        if (isEmpty(graphTypes)) {
            return null;
        }
        List<UserGraphType> userGraphTypes = userGraphTypes(dashboard, widgetId, user);
        return mapToGraphType(graphTypes, userGraphTypes);
    }

    private List<UserGraphType> userGraphTypes(Dashboard dashboard, Long widgetId, StatUser user) {
        if (user == null || dashboard == null) {
            return null;
        }
        List<UserWidget> userWidgets = userWidgetRepository.findByAppUserAndDashboardAndWidget(user.getId(), dashboard.getId(), widgetId);
        if (isEmpty(userWidgets)) {
            return null;
        }
        return userGraphTypeRepository.findAllByUserWidget(StatListUtil.first(userWidgets).getId());
    }

    private GraphTypeDto mapToGraphType(List<GraphType> graphTypes, List<UserGraphType> userGraphTypes) {
        List<GraphTypeEnum> types = graphTypes.stream().map(GraphType::getType).sorted().collect(toList());
        GraphTypeEnum defaultOption = defaultType(graphTypes, userGraphTypes, types);
        return new GraphTypeDto(graphTypes, userGraphTypes, types, defaultOption);
    }

    private GraphTypeEnum defaultType(List<GraphType> graphTypes, List<UserGraphType> userGraphTypes, List<GraphTypeEnum> types) {
        if (isNotEmpty(userGraphTypes)) {
            Optional<GraphTypeEnum> userDefaultType = userGraphTypes.stream().filter(UserGraphType::isSelected).map(UserGraphType::getType).findAny();
            if (userDefaultType.isPresent()) {
                return userDefaultType.get();
            }
        }
        return graphTypes.stream().filter(GraphType::isDefaultType).map(GraphType::getType).findAny().orElse(types.get(0));
    }
}
