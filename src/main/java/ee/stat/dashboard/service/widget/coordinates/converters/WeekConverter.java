package ee.stat.dashboard.service.widget.coordinates.converters;

import ee.stat.dashboard.model.dashboard.Week;
import ee.stat.dashboard.service.widget.coordinates.exception.TimePartMappingException;
import lombok.NoArgsConstructor;

import static ee.stat.dashboard.model.dashboard.Week.valueFrom;
import static java.lang.Integer.parseInt;
import static java.lang.String.format;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class WeekConverter {

    public static Week convert(String key) throws TimePartMappingException {
        try {
            return valueFrom(parseInt(key));
        } catch (IllegalArgumentException ignored ) {
            throw new TimePartMappingException(format("%s is not a valid week", key));
        }
    }

}
