package ee.stat.dashboard.service.widget.widget.timeperiod;

import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.util.StatDateUtil;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

import static ee.stat.dashboard.model.widget.back.enums.TimePeriod.WEEK;
import static java.time.DayOfWeek.MONDAY;

@Service
class WeekPeriodLogic implements TimePeriodLogic {

    @Override
    public TimePeriod timePeriod() {
        return WEEK;
    }

    @Override
    public LocalDate generateStartDate(LocalDate to, Integer periods) {
        return StatDateUtil.beforeDateWeek(to, periods);
    }

    @Override
    public List<LocalDate> generateDates(LocalDate from, LocalDate to) {
        return StatDateUtil.generateDatesWeek(from, to);
    }

    @Override
    public boolean valid(LocalDate date) {
        return date == null || date.getDayOfWeek() == MONDAY;
    }
}
