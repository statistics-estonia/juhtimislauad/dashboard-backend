package ee.stat.dashboard.service.widget.coordinates.exception;

import lombok.Getter;

@Getter
public class IncorrectValueException extends Exception{

    public IncorrectValueException(String message) {
        super(message);
    }
}
