package ee.stat.dashboard.service.widget.widget;

import ee.stat.dashboard.model.widget.back.GraphType;
import ee.stat.dashboard.repository.GraphTypeRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class GraphTypeServiceCache {

    private final GraphTypeRepository graphTypeRepository;

    @Cacheable(value = "GraphTypeServiceCache_findAllByWidget", key = "#widgetId")
    public List<GraphType> findAllByWidget(Long widgetId) {
        return graphTypeRepository.findAllByWidgetOrderByIdAsc(widgetId);
    }
}
