package ee.stat.dashboard.service.widget.widget;

import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.element.DomainConverter;
import ee.stat.dashboard.service.element.DomainServiceCache;
import ee.stat.dashboard.service.element.dto.DomainResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class WidgetDomainService {

    private final DomainServiceCache domainServiceCache;
    private final DomainConverter domainConverter;

    public List<DomainResponse> getDomains(Long widgetId, Long dashboardId, Language lang) {
        return domainServiceCache.findAllByWidget(widgetId).stream()
                .filter(d -> d.getDashboard().equals(dashboardId))
                .map(o -> domainConverter.mapSimpleWithParent(o, lang))
                .collect(toList());
    }
}
