package ee.stat.dashboard.service.widget.grouper;

import ee.stat.dashboard.model.json.DataPoint;
import ee.stat.dashboard.model.widget.front.Serie;
import ee.stat.dashboard.service.widget.grouper.dto.Grouping;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collector;

import static java.util.Comparator.comparing;
import static java.util.Comparator.naturalOrder;
import static java.util.Comparator.nullsFirst;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;
import static lombok.AccessLevel.PRIVATE;

@Slf4j
@NoArgsConstructor(access = PRIVATE)
public class GrouperUtil {

    public static LinkedHashMap<String, String> dimensionFilter(Grouping group, DataPoint anypoint,
                                                                Map.Entry<String, ?> entry1) {
        return dimensionFilter(group, anypoint, entry1, null, null);
    }

    public static LinkedHashMap<String, String> dimensionFilter(Grouping group, DataPoint anypoint,
                                                                Map.Entry<String, ?> entry1,
                                                                Map.Entry<String, ?> entry2) {
        return dimensionFilter(group, anypoint, entry1, entry2, null);
    }

    public static LinkedHashMap<String, String> dimensionFilter(Grouping group, DataPoint anypoint,
                                                                Map.Entry<String, ?> entry1,
                                                                Map.Entry<String, ?> entry2,
                                                                Map.Entry<String, ?> entry3) {
        LinkedHashMap<String, String> map = new LinkedHashMap<>();
        if (group.getDim1() != null) {
            String apply = group.getDim1().apply(anypoint);
            if (apply != null) {
                map.put(apply, entry1.getKey());
            }
        }
        if (group.getDim2() != null) {
            String apply = group.getDim2().apply(anypoint);
            if (apply != null) {
                map.put(apply, entry2.getKey());
            }
        }
        if (group.getDim3() != null) {
            String apply = group.getDim3().apply(anypoint);
            if (apply != null) {
                map.put(apply, entry3.getKey());
            }
        }
        return map;
    }

    public static Map<String, List<Serie>> oneDimensionGroup(List<DataPoint> dataPoints, Function<DataPoint, String> group1, Collector<DataPoint, ?, List<Serie>> collector) {
        return dataPoints.stream()
                .collect(groupingBy(group1, collector));
    }

    public static Map<String, Map<String, List<Serie>>> twoDimensionGroup(List<DataPoint> dataPoints, Function<DataPoint, String> group1, Function<DataPoint, String> group2, Collector<DataPoint, ?, List<Serie>> collector) {
        return dataPoints.stream()
                .collect(groupingBy(group1,
                        groupingBy(group2, collector)));
    }

    public static Map<String, Map<String, Map<String, List<Serie>>>> threeDimensionGroup(List<DataPoint> dataPoints, Function<DataPoint, String> group1, Function<DataPoint, String> group2, Function<DataPoint, String> group3, Collector<DataPoint, ?, List<Serie>> collector) {
        return dataPoints.stream()
                .collect(groupingBy(group1,
                        groupingBy(group2,
                                groupingBy(group3, collector))));
    }

    public static Collector<DataPoint, ?, List<Serie>> buildCollector(Grouping group) {
        if (group.getMap1() != null) {
            return mapToSortedTimeSeries();
        }
        if (group.getOrder().valueDesc()) {
            return mapToSortedValueSeriesDesc(group.getMap2());
        } else if (group.getOrder().valueAsc()) {
            return mapToSortedValueSeriesAsc(group.getMap2());
        } else if (group.getOrder().axisOrder()) {
            return mapToSortedAxisSeriesAsc(group.getMap2(), group.getOptions());
        } else
            throw new IllegalStateException("unknown order");

    }

    public static Collector<DataPoint, ?, List<Serie>> mapToSortedTimeSeries() {
        return mapping(GrouperUtil::mapToTimeSerie,
                collectingAndThen(toList(), (l -> l.stream()
                        .sorted(comparing(Serie::getDate))
                        .collect(toList()))));
    }

    public static Collector<DataPoint, ?, List<Serie>> mapToSortedValueSeriesDesc(Function<DataPoint, String> map2) {
        return mapping(observation -> mapToAbsissSerie(observation, map2),
                collectingAndThen(toList(), l -> l.stream()
                        .sorted(comparing(Serie::getValue, nullsFirst(naturalOrder())).reversed())
                        .collect(toList())));
    }

    public static Collector<DataPoint, ?, List<Serie>> mapToSortedValueSeriesAsc(Function<DataPoint, String> map2) {
        return mapping(observation -> mapToAbsissSerie(observation, map2),
                collectingAndThen(toList(), (l -> l.stream()
                        .sorted(comparing(Serie::getValue, nullsFirst(naturalOrder())))
                        .collect(toList()))));
    }

    public static Collector<DataPoint, ?, List<Serie>> mapToSortedAxisSeriesAsc(Function<DataPoint, String> map2, List<String> options) {
        return mapping(observation -> mapToAbsissSerie(observation, map2),
                collectingAndThen(toList(), (l -> l.stream()
                        .sorted(comparing(v -> options.indexOf(v.getAbscissa())))
                        .collect(toList()))));
    }

    public static Serie mapToTimeSerie(DataPoint observation) {
        return new Serie(observation.getDate(), observation.getValue(), observation.getSource());
    }

    public static Serie mapToAbsissSerie(DataPoint observation, Function<DataPoint, String> valueFunction) {
        return new Serie(valueFunction.apply(observation), observation.getValue(), observation.getSource());
    }
}
