package ee.stat.dashboard.service.widget.excelsplitter.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class Partition {

    private List<List<String>> first;
    private List<List<String>> rest;
}
