package ee.stat.dashboard.service.widget.coordinates.exception;

public class UnknownFilterException extends Exception {

    public UnknownFilterException(String message) {
        super(message);
    }
}
