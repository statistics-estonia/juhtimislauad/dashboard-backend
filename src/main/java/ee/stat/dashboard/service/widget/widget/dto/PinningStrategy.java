package ee.stat.dashboard.service.widget.widget.dto;

import org.apache.commons.lang3.BooleanUtils;

public enum PinningStrategy {

    PIN, UNPIN;

    public boolean pin(){
        return this == PIN;
    }

    public boolean unpin(){
        return this == UNPIN;
    }

    public static PinningStrategy from(Boolean value){
        return BooleanUtils.isTrue(value) ? PIN : UNPIN;
    }
}
