package ee.stat.dashboard.service.widget.widget.timeperiod;

import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.util.StatDateUtil;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

import static java.time.Month.JANUARY;

@Service
class YearPeriodLogic implements TimePeriodLogic {

    @Override
    public TimePeriod timePeriod() {
        return TimePeriod.YEAR;
    }

    @Override
    public LocalDate generateStartDate(LocalDate to, Integer periods) {
        return StatDateUtil.beforeDateYear(to, periods);
    }

    @Override
    public List<LocalDate> generateDates(LocalDate from, LocalDate to) {
        return StatDateUtil.generateDatesYear(from, to);
    }

    @Override
    public boolean valid(LocalDate date) {
        return date == null || (date.getMonth() == JANUARY && date.getDayOfMonth() == 1);
    }
}
