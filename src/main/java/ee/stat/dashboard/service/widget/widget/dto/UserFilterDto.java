package ee.stat.dashboard.service.widget.widget.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Objects;

@Getter
@Setter
public class UserFilterDto {

    private Long filter;
    private List<Long> values;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserFilterDto that = (UserFilterDto) o;
        return Objects.equals(filter, that.filter) &&
                Objects.equals(values, that.values);
    }

    @Override
    public int hashCode() {
        return Objects.hash(filter, values);
    }
}
