package ee.stat.dashboard.service.widget.widget.timeperiod;

import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class TimePeriodService {

    private List<TimePeriodLogic> logics;

    public TimePeriodLogic getLogic(TimePeriod timePeriod) {
        return logics.stream()
                .filter(l -> l.timePeriod() == timePeriod)
                .findAny().orElseThrow(() -> new IllegalStateException("unknown time period : " + timePeriod));
    }
}
