package ee.stat.dashboard.service.widget.excelsplitter.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ExcelDataPartition {

    private List<List<List<String>>> data = new ArrayList<>();
}
