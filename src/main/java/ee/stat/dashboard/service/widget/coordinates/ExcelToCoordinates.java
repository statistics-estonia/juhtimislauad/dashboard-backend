package ee.stat.dashboard.service.widget.coordinates;

import com.google.common.collect.Lists;
import ee.stat.dashboard.config.props.ExcelConfig;
import ee.stat.dashboard.model.json.CoordinatedResponse;
import ee.stat.dashboard.model.json.CoordinatedResponses;
import ee.stat.dashboard.model.json.DataPoint;
import ee.stat.dashboard.model.json.Source;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.excel.Excel;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlDimension;
import ee.stat.dashboard.service.widget.coordinates.exception.ExcelImportException;
import ee.stat.dashboard.service.widget.excelsplitter.CommonExcelMapper;
import ee.stat.dashboard.service.widget.excelsplitter.ExcelSplitter;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static ee.stat.dashboard.service.widget.coordinates.CoordinateUtil.setDimensions;
import static ee.stat.dashboard.service.widget.excelsplitter.CommonExcelMapper.isDouble;
import static ee.stat.dashboard.util.ErrorCode.EXCEL_FILTERS_DO_NOT_MATCH;
import static ee.stat.dashboard.util.ErrorCode.EXCEL_FILTER_VALUES_DO_NOT_MATCH;
import static java.lang.String.format;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;

@Service
@AllArgsConstructor
@Slf4j
public class ExcelToCoordinates {

    private final ExcelConfig excelConfig;
    private final CommonExcelMapper commonExcelMapper;
    private final ExcelSplitter excelSplitter;

    public CoordinatedResponses convert(Excel excel, Language lang, List<XmlDimension> allowedDimensions) throws ExcelImportException {
        Workbook workbook = getSheets(excel, lang);
        if (workbook == null) return null;
        List<List<String>> rows = commonExcelMapper.excelToStrings(workbook.getSheetAt(0));
        return convertExcelRows(lang, rows, allowedDimensions);
    }

    private CoordinatedResponses convertExcelRows(Language lang, List<List<String>> rows, List<XmlDimension> allowedDimensions) throws ExcelImportException {
        List<CoordinatedResponse> excelResponses = new ArrayList<>();
        for (List<List<String>> rows1 : partition(rows)) {
            excelResponses.add(buildResponse(rows1, lang, allowedDimensions));
        }
        return new CoordinatedResponses(excelResponses);
    }

    private List<List<List<String>>> partition(List<List<String>> rows) {
        return excelSplitter.toExcelDataPartition(rows).getData();
    }

    private CoordinatedResponse buildResponse(List<List<String>> rows, Language lang, List<XmlDimension> allowedDimensions) throws ExcelImportException {
        CoordinatedResponse response = new CoordinatedResponse();
        response.setDataPoints(buildDataPoints(rows, lang, response, allowedDimensions));
        return response;
    }

    private List<DataPoint> buildDataPoints(List<List<String>> rows, Language lang, CoordinatedResponse response, List<XmlDimension> allowedDimensions) throws ExcelImportException {
        sourceFromFirstRow(rows, response);

        List<String> dataHeader = rows.get(1);
        int dataColumnIndex = dataStartColumn(dataHeader);
        List<String> headersToSkip = checkForHeadersToSkip(rows, lang, dataHeader, dataColumnIndex);

        List<DataPoint> dataPoints = new ArrayList<>();
        for (int i = 2; i < rows.size(); i++) {
            List<String> row = rows.get(i);
            if (row.size() < dataColumnIndex) {
                continue; //invalid data
            }
            for (int j = dataColumnIndex; j < row.size(); j++) {
                if (isNum(row, j)) {
                    DataPoint dataPoint = new DataPoint();
                    int dimNr = 0;
                    for (int k = 0; k < dataColumnIndex; k++) {
                        String columnName = dataHeader.get(k);
                        String dimensionName = splitValue(columnName, lang);
                        if (dimensionName.isEmpty()) {
                            continue;
                        }
                        if (headersToSkip.contains(dimensionName)) {
                            continue;
                        }
                        String value = splitValue(row.get(k), lang);
                        if (allowedDimensions != null) {
                            Optional<XmlDimension> statDimension = allowedDimensions.stream().filter(d -> d.getName(lang).equals(dimensionName)).findAny();
                            if (statDimension.isEmpty()) {
                                throw new ExcelImportException(EXCEL_FILTERS_DO_NOT_MATCH, format("excel and api dimensions do not match for %s for lang %s", dimensionName, lang.name()));
                            }
                            if (statDimension.get().getValues().stream().noneMatch(v -> v.getValue(lang).equals(value))) {
                                throw new ExcelImportException(EXCEL_FILTER_VALUES_DO_NOT_MATCH, format("excel and api values do not match for %s for lang %s", value, lang.name()));
                            }
                        }
                        setDimensions(dataPoint, dimNr++, dimensionName, -1, value);
                    }
                    String dateColumn = dataHeader.get(j);
                    dataPoint.setDate(pickDate(dateColumn));
                    dataPoint.setValue(BigDecimal.valueOf(Double.valueOf(row.get(j))));
                    dataPoint.setSource(response.getSource());
                    dataPoints.add(dataPoint);
                }
            }
        }
        return dataPoints;
    }

    private void sourceFromFirstRow(List<List<String>> rows, CoordinatedResponse response) {
        for (List<String> row : rows) {
            if (row.get(0).equals(excelConfig.getType())) {
                response.setSource(Source.getFromFile(row.get(1), excelConfig.getRealLevels()));
                break;
            }
        }
        if (response.getSource() == null) {
            response.setSource(Source.EXCEL_TARGET);
        }
    }

    private List<String> checkForHeadersToSkip(List<List<String>> rows, Language lang, List<String> dataHeader, int dataColumnIndex) {
        List<String> dataHeadersToSkip = new ArrayList<>();
        Map<String, List<String>> dimensions = new HashMap<>();
        for (int i = 2; i < rows.size(); i++) {
            List<String> row = rows.get(i);

            if (row.size() < dataColumnIndex) {
                continue; //invalid data
            }
            for (int k = 0; k < dataColumnIndex; k++) {
                String columnName = dataHeader.get(k);
                String dimension = splitValue(columnName, lang);
                String value = splitValue(row.get(k), lang);
                List<String> values = dimensions.get(dimension);
                if (values == null) {
                    dimensions.put(dimension, Lists.newArrayList(value));
                } else {
                    if (!values.contains(value)) values.add(value);
                }
            }
        }
        for (Map.Entry<String, List<String>> dimension : dimensions.entrySet()) {
            if (dimension.getValue().stream().filter(StringUtils::isNotBlank).count() == 1) {
                dataHeadersToSkip.add(dimension.getKey());
            }
        }
        return dataHeadersToSkip;
    }

    private boolean isNum(List<String> row, int j) {
        return isDouble(row.get(j));
    }

    private int dataStartColumn(List<String> dataHeader) throws ExcelImportException {
        int dataColumnIndex = 0;
        for (int i = 0; i < dataHeader.size(); i++) {
            if (isNum(dataHeader, i)) {
                dataColumnIndex = i;
                break;
            }
        }
        if (isEmpty(dataHeader) || dataColumnIndex == 0 && !isNum(dataHeader, 0)) {
            throw new ExcelImportException("data not found");
        }
        return dataColumnIndex;
    }

    private LocalDate pickDate(String dateColumn) throws ExcelImportException {
        if (dateColumn.length() != 6) {
            throw new ExcelImportException("unknown date column length: " + dateColumn);
        }
        return LocalDate.of(Double.valueOf(dateColumn).intValue(), 1, 1);
    }

    private String splitValue(String columnName, Language lang) {
        String[] split = columnName.split(excelConfig.getSeparator());
        int languagePos = lang.isEt() ? 0 : split.length - 1;
        return split[languagePos].trim();
    }

    private Workbook getSheets(Excel excel, Language lang) throws ExcelImportException {
        if (excel.getLocation() == null) {
            throw new ExcelImportException("Excel has no location, filename: " + excel.getFilename());
        }
        log.info("looking for excel: {} {}", lang.name(), excel.getLocation());
        return commonExcelMapper.tryToCreateWorkbook(new File(excel.getLocation()));
    }
}
