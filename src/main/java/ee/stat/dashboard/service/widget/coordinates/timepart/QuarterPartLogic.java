package ee.stat.dashboard.service.widget.coordinates.timepart;

import ee.stat.dashboard.model.dashboard.Quarter;
import ee.stat.dashboard.model.dashboard.Unit;
import ee.stat.dashboard.model.json.DataPoint;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.enums.TimePart;
import ee.stat.dashboard.service.widget.coordinates.converters.QuarterConverter;
import ee.stat.dashboard.service.widget.coordinates.exception.TimePartMappingException;
import ee.stat.dashboard.util.StatDateUtil;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Year;

import static ee.stat.dashboard.util.StatStringUtil.cleanUp;

@Service
class QuarterPartLogic implements TimePartLogic {

    @Override
    public TimePart timePart() {
        return TimePart.QUARTER;
    }

    @Override
    public Class<Quarter> unit() {
        return Quarter.class;
    }

    @Override
    public Quarter convert(Language lang, String value) throws TimePartMappingException {
        return QuarterConverter.convert(value);
    }

    @Override
    public LocalDate calcDate(Year year, Unit unit) throws TimePartMappingException {
        if (!(unit instanceof Quarter)) {
            throw new TimePartMappingException("Wrong unit passed: " + unit.getClass());
        }
        return StatDateUtil.buildQuarterDate(year, (Quarter) unit);
    }

    @Override
    public Unit getUnit(DataPoint dp) {
        return dp.getQuarter();
    }

    @Override
    public void setUnit(DataPoint dp, Unit unit) throws TimePartMappingException {
        if (!(unit instanceof Quarter)) {
            throw new TimePartMappingException("Wrong unit passed: " + unit.getClass());
        }
        dp.setQuarter((Quarter) unit);
    }

    @Override
    public String getValue(no.ssb.jsonstat.v2.Dimension dimension, String valueCode) {
        return cleanUp(dimension.getCategory().getLabel().get(valueCode));
    }
}
