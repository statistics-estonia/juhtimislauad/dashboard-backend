package ee.stat.dashboard.service.widget.coordinates.time;

import ee.stat.dashboard.model.json.DataPoint;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.service.widget.coordinates.dto.FiltersConfig;
import ee.stat.dashboard.service.widget.coordinates.exception.TimeFilterException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.function.Function;

import static java.lang.String.format;

@Service
@AllArgsConstructor
public class TimeService {

    public void setTime(DataPoint dataPoint, FiltersConfig filtersConfig, String valueDimCode) throws TimeFilterException {
        if (filtersConfig.hasTimePart()) { //there is year + time part setup
            dataPoint.setYear(conversion(valueDimCode, StatJson2Converter::fromYear, filtersConfig.getTimePeriod()));
        } else {
            dataPoint.setDate(getDate(filtersConfig.getTimePeriod(), valueDimCode));
        }
    }

    public LocalDate safeGetDate(TimePeriod timePeriod, String valueDimCode) {
        try {
            return getDate(timePeriod, valueDimCode);
        } catch (Exception e) {
            return null;
        }
    }

    private LocalDate getDate(TimePeriod timePeriod, String valueDimCode) throws TimeFilterException {
        if (timePeriod.isYear()) {
            return conversion(valueDimCode, StatJson2Converter::fromYearDate, timePeriod);
        } else if (timePeriod.isQuarter()) {
            return conversion(valueDimCode, StatJson2Converter::fromQuarterDate, timePeriod);
        } else if (timePeriod.isMonth()) {
            return conversion(valueDimCode, StatJson2Converter::fromMonthDate, timePeriod);
        } else if (timePeriod.isWeek()) {
            throw new TimeFilterException(format("stat-json2 %s is not supported for %s", timePeriod, valueDimCode));
        } else {
            throw new TimeFilterException(format("stat-json2 %s is not supported for %s", timePeriod, valueDimCode));
        }
    }

    private <T> T conversion(String valueDimText, Function<String, T> converter, TimePeriod timePeriod) throws TimeFilterException {
        try {
            return converter.apply(valueDimText);
        } catch (Exception e) {
            throw new TimeFilterException(format("Unsuccessful conversion from %s to %s", valueDimText, timePeriod.name()));
        }
    }
}
