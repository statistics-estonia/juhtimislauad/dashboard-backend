package ee.stat.dashboard.service.widget.widget.dto;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserFilterValueDto {

    private Long id;
    private Boolean target;
}
