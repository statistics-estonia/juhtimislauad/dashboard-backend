package ee.stat.dashboard.service.widget.widget;

import ee.stat.dashboard.model.widget.back.statdatasql.StatDataSql;
import ee.stat.dashboard.repository.StatDataSqlRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class StatDataSqlCache {

    private final StatDataSqlRepository statDataSqlRepository;

    @Cacheable(value = "StatDataSqlCache_findByWidget", key = "#widget")
    public List<StatDataSql> findByWidget(Long widget){
        return statDataSqlRepository.findByWidget(widget);
    }
}
