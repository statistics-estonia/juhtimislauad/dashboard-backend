package ee.stat.dashboard.service.widget.coordinates;

import ee.stat.dashboard.model.json.CoordinatedResponse;
import ee.stat.dashboard.model.json.DataPoint;
import ee.stat.dashboard.model.widget.back.Filter;
import ee.stat.dashboard.model.widget.back.FilterValue;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.widget.coordinates.dto.FormulaDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Service
public class FormulaApplier {

    public List<CoordinatedResponse> apply(List<CoordinatedResponse> responses, List<Filter> filters, Language lang) {
        List<FormulaDto> formulas = new ArrayList<>();
        for (Filter filter : filters) {
            if (filter.getDisplayName(lang) != null) {
                formulas.add(formula(lang, filter));
            }
            for (FilterValue value : filter.getValues()) {
                if (value.getDisplayValue(lang) != null) {
                    formulas.add(formula(lang, value));
                }
            }
        }
        if (formulas.isEmpty()){
            return responses;
        }
        //todo2 in future to optimize remove formulas and check directly
        for (CoordinatedResponse response : responses) {
            for (DataPoint dataPoint : response.getDataPoints()) {
                for (FormulaDto formula : formulas) {
                    setField(formula, dataPoint::setDim1, dataPoint.getDim1());
                    setField(formula, dataPoint::setDim2, dataPoint.getDim2());
                    setField(formula, dataPoint::setDim3, dataPoint.getDim3());
                    setField(formula, dataPoint::setDim4, dataPoint.getDim4());
                    setField(formula, dataPoint::setDim5, dataPoint.getDim5());
                    setField(formula, dataPoint::setVal1, dataPoint.getVal1());
                    setField(formula, dataPoint::setVal2, dataPoint.getVal2());
                    setField(formula, dataPoint::setVal3, dataPoint.getVal3());
                    setField(formula, dataPoint::setVal4, dataPoint.getVal4());
                    setField(formula, dataPoint::setVal5, dataPoint.getVal5());
                }
            }
        }
        return responses;
    }

    private FormulaDto formula(Language lang, Filter filter) {
        FormulaDto formula = new FormulaDto();
        formula.setFromVal(filter.getName(lang));
        formula.setToVal(filter.getDisplayName(lang));
        return formula;
    }

    private FormulaDto formula(Language lang, FilterValue value) {
        FormulaDto formula = new FormulaDto();
        formula.setFromVal(value.getValue(lang));
        formula.setToVal(value.getDisplayValue(lang));
        return formula;
    }

    private void setField(FormulaDto formula, Consumer<String> consumer, String dim) {
        if (dim != null) {
            consumer.accept(dim.replaceAll(formula.getFromVal(), formula.getToVal()));
        }
    }
}
