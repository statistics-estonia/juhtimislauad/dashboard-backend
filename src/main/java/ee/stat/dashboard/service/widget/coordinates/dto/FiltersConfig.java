package ee.stat.dashboard.service.widget.coordinates.dto;

import ee.stat.dashboard.model.widget.back.Filter;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@ToString
public class FiltersConfig {

    private TimePeriod timePeriod;
    private Filter timeFilter;
    private Filter timePart;
    private Filter regionFilter;
    private List<String> ehakNames;
    private Map<Language, List<String>> constraintFilterNames;
    private Map<Language, Map<String, Filter>> regularFilters;

    public boolean hasTimePart() {
        return timePart != null;
    }

    public boolean hasRegion() {
        return regionFilter != null;
    }
}
