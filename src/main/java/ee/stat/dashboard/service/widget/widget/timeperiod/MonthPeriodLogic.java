package ee.stat.dashboard.service.widget.widget.timeperiod;

import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.util.StatDateUtil;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

import static ee.stat.dashboard.model.widget.back.enums.TimePeriod.MONTH;

@Service
class MonthPeriodLogic implements TimePeriodLogic {

    @Override
    public TimePeriod timePeriod() {
        return MONTH;
    }

    @Override
    public LocalDate generateStartDate(LocalDate to, Integer periods) {
        return StatDateUtil.beforeDateMonth(to, periods);
    }

    @Override
    public List<LocalDate> generateDates(LocalDate from, LocalDate to) {
        return StatDateUtil.generateDatesMonth(from, to);
    }

    @Override
    public boolean valid(LocalDate date) {
        return date == null || date.getDayOfMonth() == 1;
    }
}
