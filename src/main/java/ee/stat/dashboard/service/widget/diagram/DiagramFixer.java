package ee.stat.dashboard.service.widget.diagram;

import ee.stat.dashboard.model.json.Source;
import ee.stat.dashboard.model.widget.front.FilterSerie;
import ee.stat.dashboard.model.widget.front.Period;
import ee.stat.dashboard.model.widget.front.Serie;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static ee.stat.dashboard.model.json.Source.GENERATED_REAL;
import static ee.stat.dashboard.model.json.Source.GENERATED_TARGET;
import static java.util.Comparator.comparing;
import static lombok.AccessLevel.PRIVATE;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@NoArgsConstructor(access = PRIVATE)
public class DiagramFixer {

    public static void removeOutsidePoints(FilterSerie filterSerie, Period period) {
        filterSerie.getSeries().removeIf(
                s -> s.getDate().isBefore(period.getFrom())
                        || s.getDate().isAfter(period.getTo())
        );
    }

    public static void addMissingPoints(FilterSerie filterSerie, List<LocalDate> correctDates, LocalDate maxRealDataDate) {
        List<Serie> missingSeries = new ArrayList<>();
        for (LocalDate correctDate : correctDates) {
            Optional<Serie> any = filterSerie.getSeries().stream().filter(s -> s.getDate().equals(correctDate)).findAny();
            if (any.isEmpty()) {
                missingSeries.add(new Serie(correctDate, null, pickSource(correctDate, maxRealDataDate)));
            }
        }
        if (isNotEmpty(missingSeries)) {
            filterSerie.getSeries().addAll(missingSeries);
            filterSerie.getSeries().sort(comparing(Serie::getDate));
        }
    }

    static Source pickSource(LocalDate correctDate, LocalDate maxRealDataDate) {
        if (maxRealDataDate != null && correctDate.isAfter(maxRealDataDate)) {
            return GENERATED_TARGET;
        }
        return GENERATED_REAL;
    }
}
