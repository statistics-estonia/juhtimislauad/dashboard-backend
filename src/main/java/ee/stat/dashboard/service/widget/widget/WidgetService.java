package ee.stat.dashboard.service.widget.widget;

import ee.stat.dashboard.config.props.StatConfig;
import ee.stat.dashboard.config.security.StatUser;
import ee.stat.dashboard.controller.admin.FilterType;
import ee.stat.dashboard.model.classifier.Element;
import ee.stat.dashboard.model.classifier.ElementAlternative;
import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.widget.back.GraphType;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.excel.Excel;
import ee.stat.dashboard.model.widget.back.statdataapi.StatDataApi;
import ee.stat.dashboard.model.widget.back.statdatasql.StatDataSql;
import ee.stat.dashboard.model.widget.front.Diagram;
import ee.stat.dashboard.model.widget.front.DiagramData;
import ee.stat.dashboard.repository.DashboardWidgetRepository;
import ee.stat.dashboard.repository.ElementAlternativeRepository;
import ee.stat.dashboard.service.element.ElementService;
import ee.stat.dashboard.service.widget.widget.dto.Filters;
import ee.stat.dashboard.service.widget.widget.dto.GraphDto;
import ee.stat.dashboard.service.widget.widget.dto.GraphTypeDto;
import ee.stat.dashboard.service.widget.widget.dto.StatDataDto;
import ee.stat.dashboard.service.widget.widget.dto.UserGraphTypeDto;
import ee.stat.dashboard.service.widget.widget.dto.UserWidgetDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetDashboardContainer;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import ee.stat.dashboard.util.StatBadRequestException;
import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static ee.stat.dashboard.util.ErrorCode.DASHBOARD_WIDGET_UNKNOWN_EHAK;
import static ee.stat.dashboard.util.StatDateUtil.getLatest;
import static java.text.MessageFormat.format;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isBlank;

@Slf4j
@Service
@AllArgsConstructor
@Transactional(readOnly = true)
public class WidgetService {

    private final DiagramService diagramService;
    private final WidgetConverter widgetConverter;
    private final GraphService graphService;
    private final GraphTypeService graphTypeService;
    private final UserWidgetPreferencesService userWidgetPreferencesService;
    private final DashboardWidgetRepository dashboardWidgetRepository;
    private final ElementAlternativeRepository elementAlternativeRepository;
    private final WidgetFinder widgetFinder;
    private final ElementService elementService;
    private final StatDataApiCache statDataApiCache;
    private final StatDataSqlCache statDataSqlCache;
    private final ExcelCache excelCache;
    private final StatConfig statConfig;

    @PostConstruct
    private void checkUrls() {
        checkUrl(statConfig.getDataApiUrl());
        checkUrl(statConfig.getDataApiCubesUrl());
    }

    public WidgetResponse findById(Long id, Long dashboardId, Long ehakId, Language lang, String graphType,
                                   StatUser user, Map<String, String> params, FilterType filterType) {
        WidgetDashboardContainer widget = widgetFinder.findWidgetForViewing(id, dashboardId);
        return mapWidget(widget, ehakId, lang, graphType, user, params, filterType);
    }

    public WidgetResponse findByWidgetIdOnly(Long id, Language lang, String graphType,
                                             Map<String, String> params, FilterType filterType) {
        WidgetDashboardContainer widget = widgetFinder.findWidgetForAdmin(id);
        return mapWidget(widget, null, lang, graphType, null, params, filterType);
    }

    private WidgetResponse mapWidget(WidgetDashboardContainer widgetContainer, Long ehakId, Language lang, String graphTypeStr,
                                     StatUser user, Map<String, String> params, FilterType filterType) {
        Widget widget = widgetContainer.getWidget();
        Dashboard dashboard = widgetContainer.getDashboard();
        WidgetResponse dto = widgetConverter.mapMinimal(widget, lang);
        widgetConverter.mapWidgetData(widget, dashboard, lang, dto);

        List<StatDataApi> statDataApis = statDataApiCache.findByWidget(widget.getId());
        if (isNotEmpty(statDataApis)) {
            statDataApis.forEach(api -> addStatMeta(dto, api, lang));
        } else {
            List<StatDataSql> statDataSqls = statDataSqlCache.findByWidget(widget.getId());
            if (isNotEmpty(statDataSqls)) {
                statDataSqls.forEach(sql -> addStatMeta(dto, sql, lang));
            }
        }

        List<Excel> rawExcels = excelCache.findByWidget(widget.getId());
        rawExcels.forEach(excel -> addRawExcelMeta(dto, excel));
        rawExcels.stream()
                .filter(e -> !e.getExcelType().isWidgetMeta())
                .forEach(excel -> addExcelMeta(dto, excel));

        GraphTypeDto graphTypes = graphTypeService.mapToGraphType(dashboard, widget.getId(), user);
        if (graphTypes == null) {
            dto.setGraphTypes(GraphTypeDto.empty());
            dto.setDiagram(GraphDto.empty());
            return dto;
        }
        dto.setGraphTypes(graphTypes);
        GraphTypeEnum graphTypeEnum = GraphTypePicker.getGraphType(graphTypeStr, graphTypes);
        dto.setGraphType(graphTypeEnum);

        Diagram diagram = diagramService.findDiagram(widget);
        if (diagram == null) {
            dto.setDiagram(GraphDto.empty());
            return dto;
        }

        GraphType graphType = getGraphType(graphTypeEnum, graphTypes);

        DiagramData data = diagramService.findDiagramData(dto.getId(), diagram.getId(), graphType);
        if (data == null) {
            dto.setDiagram(GraphDto.empty());
            return dto;
        }


        Filters filters = Filters.builder()
                .lang(lang)
                .ehak(element(ehakId))
                .params(params)
                .graphType(graphType)
                .build();

        if (filterType.filterUserPreferences() && user != null) {
            UserWidgetDto preferences = getPreferences(dto, graphTypes);
            preferences.setPinned(dashboardWidgetRepository.existsByAppUserWidget(user.getId(), widget.getId()) ? true : null);
            dto.setPreferences(preferences);
            if (isNotEmpty(preferences.getGraphs())) {
                List<UserGraphTypeDto> charts = preferences.getGraphs();
                Optional<UserGraphTypeDto> any = charts.stream().filter(f -> f.getGraphType().equals(graphType.getType())).findAny();
                any.ifPresent(userGraphTypeDto -> filters.setUserFilters(userGraphTypeDto.getFilters()));
            }
        }

        if (filterType.filterGraphData()) {
            data.setDiagramObj(diagram);
            dto.setDiagram(graphService.filterData(data, filters));
        } else {
            data.setDiagramObj(diagram);
            dto.setDiagram(graphService.fullData(data, filters));
        }
        return dto;
    }

    private UserWidgetDto getPreferences(WidgetResponse dto, GraphTypeDto graphTypes) {
        return isNotEmpty(graphTypes.getUserGraphTypes()) ?
                userWidgetPreferencesService.convert(dto.getGraphTypes()) : new UserWidgetDto();
    }

    private GraphType getGraphType(GraphTypeEnum graphTypeEnum, GraphTypeDto graphTypes) {
        return graphTypes.getGraphTypes().stream()
                .filter(f -> f.getType().equals(graphTypeEnum))
                .findAny().orElseThrow(RuntimeException::new);
    }

    private void addStatMeta(WidgetResponse dto, StatDataApi api, Language lang) {
        if (dto.getStatCubes() == null) {
            dto.setStatCubes(new ArrayList<>());
        }
        dto.getStatCubes().add(
                new StatDataDto(
                        api.getCube(),
                        format(statConfig.getDataApiPortalUrl(), lang.toLocale(), api.getCube()))
        );

        dto.setDataUpdatedAt(getLatest(dto.getDataUpdatedAt(), api.getLatestDate()));
    }

    private void addStatMeta(WidgetResponse dto, StatDataSql sql, Language lang) {
        if (dto.getStatCubes() == null) {
            dto.setStatCubes(new ArrayList<>());
        }
        dto.getStatCubes().add(
                new StatDataDto(
                        sql.getCube(),
                        format(statConfig.getDataSqlPortalUrl(), lang.toLocale(), sql.getCube()))
        );

        dto.setDataUpdatedAt(getLatest(dto.getDataUpdatedAt(), sql.getLatestDate()));
    }

    private void addRawExcelMeta(WidgetResponse dto, Excel excel) {
        if (dto.getExcels() == null) {
            dto.setExcels(new ArrayList<>());
        }
        dto.getExcels().add(excel.getFilename());
    }

    private void addExcelMeta(WidgetResponse dto, Excel excel) {
        dto.setDataUpdatedAt(getLatest(dto.getDataUpdatedAt(), excel.getLatestDate()));
    }

    private Element element(Long ehakId) {
        Element element = getElement(ehakId);
        List<ElementAlternative> alternatives = elementAlternativeRepository.findByElement(element.getId());
        if (isNotEmpty(alternatives)) {
            element.setAlternatives(alternatives);
        }
        return element;
    }

    private Element getElement(Long ehakId) {
        if (ehakId == null) {
            return elementService.getWholeEstonia();
        }
        return elementService.findById(ehakId).orElseThrow(() -> new StatBadRequestException(DASHBOARD_WIDGET_UNKNOWN_EHAK));
    }

    private void checkUrl(String metaUrl) {
        if (isBlank(metaUrl)) {
            throw new IllegalStateException("Portal link is not configured");
        }
    }
}
