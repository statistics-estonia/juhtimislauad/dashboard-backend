package ee.stat.dashboard.service.widget.widget.dto;

import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class UserGraphTypeDto {

    private GraphTypeEnum graphType;
    private List<UserFilterDto> filters;
    private List<UserLegendDto> legends;

    public UserGraphTypeDto(GraphTypeEnum graphType) {
        this.graphType = graphType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserGraphTypeDto that = (UserGraphTypeDto) o;
        return graphType == that.graphType &&
                Objects.equals(filters, that.filters) &&
                Objects.equals(legends, that.legends);
    }

    @Override
    public int hashCode() {
        return Objects.hash(graphType, filters, legends);
    }
}
