package ee.stat.dashboard.service.widget.coordinates;

import ee.stat.dashboard.model.widget.back.DatasetCustomEntry;
import ee.stat.dashboard.model.widget.back.FilterValueCustom;
import ee.stat.dashboard.model.widget.back.enums.Language;
import lombok.NoArgsConstructor;
import no.ssb.jsonstat.v2.Dimension;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static ee.stat.dashboard.service.widget.coordinates.CoordinateUtil.findDimensionIndex;
import static ee.stat.dashboard.service.widget.widget.WidgetConstants.FILTER_VALUE_CUSTOM_PREFIX;
import static java.lang.String.format;
import static java.util.Objects.isNull;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class StatJsonDatasetCustomValueCalculator {

    public static void calculateDatapointsForCustomFilterValues(List<FilterValueCustom> customFilterValues, List<Dimension> dimensions, Language lang, Map<List<String>, Number> values) {
        for (FilterValueCustom customFilterValue : customFilterValues) {
            // does not use two dimensions for calculation, except when two dimensions are same type, so operation is either ADDITION or SUBTRACTION
            // else block executes when operation is either WEIGHT or PERCENTAGE (two dimensions present)
            if (isNull(customFilterValue.getDimension2NameEt())) {
                int dimIndex = findDimensionIndex(dimensions, customFilterValue.getFirstDimensionName(lang));
                List<DatasetCustomEntry> entries1 = new ArrayList<>();
                List<DatasetCustomEntry> entries2 = new ArrayList<>();

                collectSingleDimensionDatapointsForCalculation(values, customFilterValue, dimIndex, entries1, entries2);
                calculateNewDatapoints(values, customFilterValue, entries1, entries2, dimIndex);
            } else {
                int dimIndex1 = findDimensionIndex(dimensions, customFilterValue.getFirstDimensionName(lang));
                int dimIndex2 = findDimensionIndex(dimensions, customFilterValue.getSecondDimensionName(lang));
                List<DatasetCustomEntry> entries1 = new ArrayList<>();
                List<DatasetCustomEntry> entries2 = new ArrayList<>();

                collectDoubleDimensionDatapointsForCalculation(values, customFilterValue, dimIndex1, dimIndex2, entries1, entries2);
                calculateNewDatapoints(values, customFilterValue, entries1, entries2, dimIndex1);
            }
        }
    }

    private static void collectSingleDimensionDatapointsForCalculation(Map<List<String>, Number> values, FilterValueCustom customFilterValue, int dimIndex, List<DatasetCustomEntry> entries1, List<DatasetCustomEntry> entries2) {
        for (Map.Entry<List<String>, Number> keyValue : values.entrySet()) {
            String key = keyValue.getKey().get(dimIndex);
            if (key.equals(customFilterValue.getSelectionValue1())) {
                entries1.add(new DatasetCustomEntry(keyValue.getKey(), keyValue.getValue()));
            } else if (key.equals(customFilterValue.getSelectionValue2())) {
                entries2.add(new DatasetCustomEntry(keyValue.getKey(), keyValue.getValue()));
            }
        }
    }

    private static void collectDoubleDimensionDatapointsForCalculation(Map<List<String>, Number> values, FilterValueCustom customFilterValue, int dimIndex1, int dimIndex2, List<DatasetCustomEntry> entries1, List<DatasetCustomEntry> entries2) {
        for (Map.Entry<List<String>, Number> keyValue : values.entrySet()) {
            String key1 = keyValue.getKey().get(dimIndex1);
            String key2 = keyValue.getKey().get(dimIndex2);

            boolean isKey1Selection1 = key1.equals(customFilterValue.getSelectionValue1());
            boolean isKey2Selection2 = key2.equals(customFilterValue.getSelectionValue2());
            boolean keysEqual = key1.equals(key2);

            if (keysEqual) {
                if (isKey1Selection1) {
                    entries2.add(new DatasetCustomEntry(keyValue.getKey(), keyValue.getValue()));
                } else if (isKey2Selection2) {
                    entries1.add(new DatasetCustomEntry(keyValue.getKey(), keyValue.getValue()));
                }
            } else if (isKey1Selection1 && isKey2Selection2) {
                entries2.add(new DatasetCustomEntry(keyValue.getKey(), keyValue.getValue()));
            } else if (isKey1Selection1 || isKey2Selection2) {
                entries1.add(new DatasetCustomEntry(keyValue.getKey(), keyValue.getValue()));
            }
        }
    }

    private static void calculateNewDatapoints(Map<List<String>, Number> values, FilterValueCustom customFilterValue, List<DatasetCustomEntry> entries1, List<DatasetCustomEntry> entries2, int dimIndex) {
        for (DatasetCustomEntry entry1 : entries1) {
            DatasetCustomEntry entry2 = getMatchingEntry(entries2, entry1);
            if (isNull(entry2) || isNull(entry1.getValue()) || isNull(entry2.getValue())) {
                continue;
            }

            Number result = calculateSingleDatapoint(customFilterValue, entry1.getValue().doubleValue(), entry2.getValue().doubleValue());
            List<String> finalValues = new ArrayList<>(entry1.getValues());
            finalValues.set(dimIndex, FILTER_VALUE_CUSTOM_PREFIX + customFilterValue.getId());
            values.put(finalValues, result);
        }
    }

    private static DatasetCustomEntry getMatchingEntry(List<DatasetCustomEntry> entries2, DatasetCustomEntry entry1) {
        return entries2.stream()
                .filter(e -> e.valuesDifferByOne(entry1.getValues()))
                .findFirst()
                .orElse(null);
    }

    private static Number calculateSingleDatapoint(FilterValueCustom customFilterValue, double value1, double value2) {
        switch (customFilterValue.getOperation()) {
            case ADDITION:
                return value1 + value2;
            case SUBTRACTION:
                return value1 - value2;
            case WEIGHT:
                return value1 / value2;
            case PERCENTAGE:
                return value1 / value2 * 100;
            default:
                throw new IllegalArgumentException(format("Unknown operation type: %s", customFilterValue.getOperation()));
        }
    }
}
