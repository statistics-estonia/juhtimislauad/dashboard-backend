package ee.stat.dashboard.service.widget.coordinates.exception;

import lombok.Getter;

@Getter
public class IncorrectLabelException extends Exception{

    public IncorrectLabelException(String message) {
        super(message);
    }
}
