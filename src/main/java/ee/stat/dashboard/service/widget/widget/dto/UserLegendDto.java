package ee.stat.dashboard.service.widget.widget.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Objects;

@Getter
@Setter
public class UserLegendDto {

    private Boolean target;
    private List<UserLegendValueDto> values;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserLegendDto that = (UserLegendDto) o;
        return Objects.equals(target, that.target) &&
                Objects.equals(values, that.values);
    }

    @Override
    public int hashCode() {
        return Objects.hash(target, values);
    }
}
