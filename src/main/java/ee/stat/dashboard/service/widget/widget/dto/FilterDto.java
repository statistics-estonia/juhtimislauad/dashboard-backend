package ee.stat.dashboard.service.widget.widget.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ee.stat.dashboard.model.widget.front.enums.FilterDisplay;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.BooleanUtils;

import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.toList;

@Getter
@Setter
public class FilterDto {

    private Long id;
    private Integer order;
    private Integer numberOfOptions;
    private String name;
    private FilterDisplay type;
    private Boolean region;
    private Boolean time;
    @JsonIgnore
    private List<String> upperOptions;
    @JsonIgnore
    private List<String> upperDefaultOptions;
    private List<FilterValueDto> values;

    @JsonIgnore
    public void recalcDefaultOptions(){
        this.upperDefaultOptions = values.stream()
                .filter(v -> BooleanUtils.isTrue(v.getSelected()))
                .map(FilterValueDto::getOption)
                .map(String::toUpperCase)
                .collect(toList());
    }

    @JsonIgnore
    public void recalcOptions(){
        this.upperOptions = values.stream()
                .map(FilterValueDto::getOption)
                .map(String::toUpperCase)
                .collect(toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FilterDto filterDto = (FilterDto) o;
        return Objects.equals(id, filterDto.id) &&
                Objects.equals(order, filterDto.order) &&
                Objects.equals(numberOfOptions, filterDto.numberOfOptions) &&
                Objects.equals(name, filterDto.name) &&
                type == filterDto.type &&
                Objects.equals(region, filterDto.region) &&
                Objects.equals(time, filterDto.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, order, numberOfOptions, name, type, region, time);
    }
}
