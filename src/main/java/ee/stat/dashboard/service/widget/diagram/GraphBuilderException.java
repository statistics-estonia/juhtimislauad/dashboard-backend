package ee.stat.dashboard.service.widget.diagram;

public class GraphBuilderException extends CalculationException {

    public GraphBuilderException(String message) {
        super(message);
    }

}
