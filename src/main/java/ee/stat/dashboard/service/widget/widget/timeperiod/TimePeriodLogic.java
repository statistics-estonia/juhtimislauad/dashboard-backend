package ee.stat.dashboard.service.widget.widget.timeperiod;

import ee.stat.dashboard.model.widget.back.enums.TimePeriod;

import java.time.LocalDate;
import java.util.List;

/**
 * this class purpose is to show explicitly where time part filter is used
 */
public interface TimePeriodLogic {

    TimePeriod timePeriod();

    /**
     * generate a start date using to date and a number of periods
     */
    LocalDate generateStartDate(LocalDate to, Integer nrOfPeriods);

    /**
     * generate a set of valid dates using from, to and a time period
     */
    List<LocalDate> generateDates(LocalDate from, LocalDate to);

    /**
     * is the widget date (start or end) valid
     */
    boolean valid(LocalDate date);

    default boolean inValid(LocalDate date){
        return !valid(date);
    }
}
