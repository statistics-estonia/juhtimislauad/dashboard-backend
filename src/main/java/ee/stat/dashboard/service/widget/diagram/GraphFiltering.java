package ee.stat.dashboard.service.widget.diagram;

import ee.stat.dashboard.model.widget.front.FilterSerie;
import ee.stat.dashboard.model.widget.front.Graph;
import ee.stat.dashboard.model.widget.front.Serie;
import ee.stat.dashboard.service.widget.widget.FilterRequestBuilder;
import ee.stat.dashboard.service.widget.widget.dto.FilterDto;
import ee.stat.dashboard.service.widget.widget.dto.Filters;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.collections4.MapUtils.isEmpty;
import static org.apache.commons.lang3.BooleanUtils.isTrue;

@Slf4j
@Service
@AllArgsConstructor
public class GraphFiltering {

    private final FilterRequestBuilder filterRequestBuilder;

    public Graph filterGraph(Graph graph, List<FilterDto> filters, Filters config) {
        if (isEmpty(filters)) {
            return graph;
        }
        Map<String, List<String>> filterRequest = filterRequestBuilder.buildFilterRequest(filters, config.getParams());
        //    todo2 filtering log, disable if unnecessary
        for (Map.Entry<String, List<String>> entry : filterRequest.entrySet()) {
            log.info("filtering {} {}", entry.getKey(), entry.getValue());
        }
        if (isNotEmpty(graph.getSeries())) {
            FilterSerie firstSeries = graph.getSeries().get(0);
            if (isNotEmpty(firstSeries.getSeries())) {
                log.info("first serie {} {}", firstSeries.getFilters(), firstSeries.getSeries().get(0));
            } else {
                log.info("first serie {} {}", firstSeries.getFilters(), "no series");
            }
        }
        List<FilterSerie> filteredSeries = filterSeries(graph, filterRequest);

        Optional<FilterDto> axisFilter = getAxis(filters);
        Map<String, List<String>> axisFilterRequest = filterRequestBuilder.axisFilterRequest(config, axisFilter);

        List<FilterSerie> axisFiltered = filterAxis(filteredSeries, axisFilterRequest);

        if (isAxisRegional(axisFilter)) {
            setSerieSelected(config, axisFiltered);
        }
        return Graph.builder()
                .period(graph.getPeriod())
                .series(axisFiltered)
                .build();
    }

    private List<FilterSerie> filterSeries(Graph graph, Map<String, List<String>> filterRequest) {
        if (isEmpty(filterRequest)) {
            return graph.getSeries();
        }
        return graph.getSeries().stream()
                .filter(s -> FilterRequestBuilder.hasFilter(s, filterRequest))
                .collect(toList());
    }

    private List<FilterSerie> filterAxis(List<FilterSerie> series, Map<String, List<String>> axisFilterRequest) {
        if (isEmpty(axisFilterRequest)) {
            return series;
        }
        List<FilterSerie> filteredSeries = new ArrayList<>();
        for (Map.Entry<String, List<String>> axisFilter : axisFilterRequest.entrySet()) {
            for (FilterSerie serie : series) {
                filteredSeries.add(map(axisFilter, serie));
            }
        }
        return filteredSeries;
    }

    private boolean isAxisRegional(Optional<FilterDto> axisFilter) {
        return axisFilter.isPresent() && isTrue(axisFilter.get().getRegion());
    }

    private void setSerieSelected(Filters config, List<FilterSerie> newSeries) {
        for (FilterSerie newSerie : newSeries) {
            for (Serie serie : newSerie.getSeries()) {
                serie.setSelected(config.getEhakNamesUpper().contains(serie.getAbscissa().toUpperCase()) ? true : null);
            }
        }
    }

    private FilterSerie map(Map.Entry<String, List<String>> axisFilter, FilterSerie originalSerie) {
        return new FilterSerie(originalSerie.getFilters(),
                originalSerie.getOrder(),
                filteredSeries(originalSerie.getSeries(), axisFilter.getValue()));
    }

    private List<Serie> filteredSeries(List<Serie> series, List<String> values) {
        return series.stream()
                .filter(s -> values.contains(s.getAbscissa().toUpperCase()))
                .collect(toList());
    }

    private Optional<FilterDto> getAxis(List<FilterDto> filters) {
        return filters.stream()
                .filter(f -> f.getType().isAxis())
                .findAny();
    }
}
