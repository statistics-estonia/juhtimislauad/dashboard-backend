package ee.stat.dashboard.service.tara;

import ee.stat.dashboard.config.props.OAuth2Config;
import ee.stat.dashboard.controller.user.dto.TaraDataDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.WebUtils;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;

import static org.apache.commons.lang3.StringUtils.isBlank;

@Slf4j
@Service
@AllArgsConstructor
public class TaraService {

    private final OAuth2Config oAuth2Config;
    private final TaraValidationService taraValidationService;
    private final RestTemplate restTemplate;

    public String prepareUrl(String csrfToken) {
        try {
            return MessageFormat.format("{0}?client_id={1}&response_type=code&scope={2}&redirect_uri={3}&state={4}",
                    oAuth2Config.getTARAAuthorizationEndpoint(),
                    oAuth2Config.getClientId(),
                    URLEncoder.encode(oAuth2Config.getSimpleScope(), "UTF-8"),
                    URLEncoder.encode(oAuth2Config.getRedirectUri(), "UTF-8"),
                    csrfToken);
        } catch (UnsupportedEncodingException e) {
            log.error("Exception while preparing url for redirect ", e);
        }
        return null;
    }

    public TaraDataDto validateResponse(HttpServletRequest request, String authCode, String state, String error, String errorDescription) {
        if (!(StringUtils.isNotBlank(error) || StringUtils.isNotBlank(authCode) && StringUtils.isNotBlank(state))) {
            return returnError("No success or error");
        }
        if (StringUtils.isNotBlank(error)) {
            return returnError(error + " : " + errorDescription);
        }
        if (invalidCsrf(state, request)) {
            return returnError("Csrf token is wrong or expired");
        }
        IDTokenResponse idTokenResponse = requestIDToken(authCode);
        if (idTokenResponse == null) {
            return returnError("Http state is different");
        }
        String idToken = idTokenResponse.getIdToken();
        if (isBlank(idToken)) {
            return returnError("id response is empty");
        }
        TaraDataDto taraDataDto = taraValidationService.isValidIDToken(idToken);
        if (!taraDataDto.isValid()) {
            return returnError("Token is invalid");
        }
        log.info("Identification identified");
        return taraDataDto;
    }

    private TaraDataDto returnError(String s) {
        log.error(s);
        TaraDataDto dto = new TaraDataDto();
        dto.setError(s);
        return dto;
    }

    private boolean invalidCsrf(String state, HttpServletRequest request) {
        Cookie tara = WebUtils.getCookie(request, "TARA");
        if (tara == null){
            log.info("no cookies");
            return false;
        }
        String csrfToken = tara.getValue();
        boolean result = csrfToken == null || !csrfToken.equals(state);
        log.error("session csrfToken: {}", csrfToken);
        log.error("result csrfToken: {}", state);
        return result;
    }

    private IDTokenResponse requestIDToken(String authCode) {
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(body(authCode), headers());
        try {
            ResponseEntity<IDTokenResponse> response = restTemplate.postForEntity(oAuth2Config.getTARATokenEndpoint(), request, IDTokenResponse.class);

            log.info("Response Code : " + response.getStatusCodeValue());
            if (response.getStatusCodeValue() != 200) {
                log.info("Error asking for request ID token");
                return null;
            }

            log.debug("Identification result {}", response.getBody() != null ? response.getBody().toString() : "null body");
            return response.getBody();
        } catch (Exception e){
            log.error("Error while requesting id token: {}" ,e.getMessage(), e);
            return null;
        }
    }

    private HttpHeaders headers() {
        HttpHeaders headers = new HttpHeaders();
        String base64ClientIdSec = Base64.encodeBase64String((oAuth2Config.getClientId() + ":" + oAuth2Config.getClientSecret()).getBytes(StandardCharsets.UTF_8));
        headers.add("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
        headers.add("Authorization", "Basic " + base64ClientIdSec);
        headers.add("Accept", "application/json");
        return headers;
    }

    private MultiValueMap<String, String> body(String authCode) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("code", authCode);
        map.add("redirect_uri", oAuth2Config.getRedirectUri());
        map.add("grant_type", "authorization_code");
        return map;
    }
}
