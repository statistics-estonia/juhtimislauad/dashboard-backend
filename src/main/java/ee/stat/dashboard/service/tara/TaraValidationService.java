package ee.stat.dashboard.service.tara;

import ee.stat.dashboard.config.props.OAuth2Config;
import ee.stat.dashboard.controller.user.dto.TaraDataDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.RSAPublicKeySpec;
import java.time.Instant;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Identsustõendi kontrollija
 */
@Slf4j
@Service
@AllArgsConstructor
public class TaraValidationService {

    private final OAuth2Config oAuth2Config;
    private final RestTemplate restTemplate;

    public TaraDataDto isValidIDToken(String idToken) {
        TaraDataDto taraDataDto = new TaraDataDto();
        try {
            String[] idTokenParts = idToken.split("\\.");

            if (idTokenParts.length < 3) {
                log.debug("invalid idTokenParts length");
                return taraDataDto;
            }

            String idTokenPayload = base64UrlDecode(idTokenParts[1]);

            JSONObject idTokenHeaderPayload = new JSONObject(idTokenPayload);

            taraDataDto.setIdCode(idTokenHeaderPayload.getString("sub"));

            JSONObject profileAttributes = idTokenHeaderPayload.getJSONObject("profile_attributes");
            taraDataDto.setFirstName(profileAttributes.getString("given_name"));
            taraDataDto.setLastName(profileAttributes.getString("family_name"));
            taraDataDto.setFrom(idTokenHeaderPayload.getJSONArray("amr").getString(0));

            String issuer = idTokenHeaderPayload.getString("iss");
            if (!issuer.equalsIgnoreCase(oAuth2Config.getIssuer())) {
                log.error("Issuer is invalid: {}", issuer);
                return taraDataDto;
            }

            String audit = idTokenHeaderPayload.getString("aud");
            if (!audit.equalsIgnoreCase(oAuth2Config.getClientId())) {
                log.error("Audit is invalid: {}", audit);
                return taraDataDto;
            }

            if (hasExpired(idTokenHeaderPayload)) {
                log.error("Id card has expired");
                return taraDataDto;
            }

            JSONObject taraSignKey = getTaraSignKey();
            if (taraSignKey == null) {
                return taraDataDto;
            }

            return verifyTaraPublicKey(taraDataDto, idTokenParts, taraSignKey);
        } catch (JSONException e) {
            log.error("JSON error on validation ", e);
            return taraDataDto;
        }
    }

    private TaraDataDto verifyTaraPublicKey(TaraDataDto taraDataDto, String[] idTokenParts, JSONObject taraSignKey) {
        try {
            byte[] data = (idTokenParts[0] + "." + idTokenParts[1]).getBytes(UTF_8);
            byte[] idTokenSignature = base64UrlDecodeToBytes(idTokenParts[2]);
            boolean isSignatureValid = verifyUsingPublicKey(data, idTokenSignature, getPublicKey(taraSignKey));
            log.info("Signature is valid " + isSignatureValid);
            taraDataDto.setValid(isSignatureValid);
            return taraDataDto;
        } catch (GeneralSecurityException e) {
            log.error("Error checking signature ", e);
            return taraDataDto;
        }
    }

    private PublicKey getPublicKey(JSONObject taraKey) {
        try {
            RSAPublicKeySpec rsaPublicKeySpec = new RSAPublicKeySpec(getInt(taraKey, "n"), getInt(taraKey, "e"));
            return KeyFactory.getInstance("RSA").generatePublic(rsaPublicKeySpec);
        } catch (Exception ex) {
            log.error("Exception while getting public key ", ex);
            throw new RuntimeException("Cant create public key", ex);
        }
    }

    private BigInteger getInt(JSONObject taraKey, String key) throws JSONException {
        String modulo = taraKey.getString(key);
        byte[] nb = base64UrlDecodeToBytes(modulo);
        return new BigInteger(1, nb);
    }

    private boolean hasExpired(JSONObject idTokenHeaderPayload) throws JSONException {
        return (idTokenHeaderPayload.getLong("exp") - Instant.now().getEpochSecond()) <= 0;
    }

    private String base64UrlDecode(String input) {
        byte[] decodedBytes = base64UrlDecodeToBytes(input);
        return new String(decodedBytes, UTF_8);
    }

    private byte[] base64UrlDecodeToBytes(String input) {
        Base64 decoder = new Base64(-1, null, true);
        return decoder.decode(input);
    }

    private JSONObject getTaraSignKey() {
        try {
            ResponseEntity<String> exchange = restTemplate.getForEntity(oAuth2Config.getTARAKeyEndpoint(), String.class);
            log.info("TARA signing key result " + exchange.getBody());
            return new JSONObject(exchange.getBody()).getJSONArray("keys").getJSONObject(0);
        } catch (Exception e) {
            log.error("Error getting TARA signing key: ", e);
            return null;
        }
    }

    private boolean verifyUsingPublicKey(byte[] data, byte[] signature, PublicKey pubKey) throws GeneralSecurityException {
        Signature sig = Signature.getInstance("SHA256withRSA");
        sig.initVerify(pubKey);
        sig.update(data);
        return sig.verify(signature);
    }
}
