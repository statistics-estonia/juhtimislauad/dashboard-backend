package ee.stat.dashboard.service;

import ee.stat.dashboard.model.widget.back.StatDataType;

public interface StatApiType {

    default boolean matches(StatDataType statDataType) {
        StatDataType dataType = statDataType();
        if (dataType.isDataShared()) {
            return statDataType.sharedMatch();
        }
        return dataType == statDataType;
    }

    StatDataType statDataType();
}
