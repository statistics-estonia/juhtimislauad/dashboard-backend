package ee.stat.dashboard;

import ee.stat.dashboard.demo.DemoData;
import ee.stat.dashboard.service.statdata.publishedat.StatDataApiCubeCache;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@AllArgsConstructor
public class ApplicationInit implements ApplicationRunner {

    private final DemoData demoData;
    private final Environment environment;
    private final StatDataApiCubeCache statDataApiCubeCache;

    @Override
    public void run(ApplicationArguments args) {
        log.info("Starting postinit import");
        statDataApiCubeCache.updateCache();

        if (environment.acceptsProfiles(Profiles.of("test"))) {
            log.info("Demo data builder is enabled for test profile");
            demoData.build();
        }
    }
}
