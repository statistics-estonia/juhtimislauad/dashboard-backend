package ee.stat.dashboard.controller;

import lombok.Getter;
import lombok.Setter;

import java.time.OffsetDateTime;

@Getter
@Setter
public class ExceptionResponse {

    private String message;
    private String localizedMessage;
    private String code;
    private int httpStatus;
    private OffsetDateTime timestamp;
}
