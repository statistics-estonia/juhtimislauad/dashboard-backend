package ee.stat.dashboard.controller;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Response<T> {

    private T data;

    public Response(T data) {
        this.data = data;
    }

    public static <T> Response<T> of (T data){
        return new Response<>(data);
    }
}
