package ee.stat.dashboard.controller.admin;

public enum DashboardStrategy {
    DASHBOARDS_ONLY, WITH_ELEMENTS;

    public boolean dashboardsOnly(){
        return this == DASHBOARDS_ONLY;
    }

    public boolean withElements(){
        return this == WITH_ELEMENTS;
    }
}
