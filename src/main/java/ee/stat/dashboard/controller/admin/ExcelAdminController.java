package ee.stat.dashboard.controller.admin;

import ee.stat.dashboard.config.security.RoleString;
import ee.stat.dashboard.model.widget.back.enums.ExcelType;
import ee.stat.dashboard.service.admin.widget.ExcelFileStorageService;
import ee.stat.dashboard.service.admin.widget.dto.UploadFileResponse;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import jakarta.servlet.http.HttpServletRequest;
import java.io.IOException;

import static ee.stat.dashboard.util.ErrorCode.UPLOADED_FILE_HAS_NO_EXTENSION;
import static ee.stat.dashboard.util.ErrorCode.UPLOADED_FILE_HAS_NO_NAME;
import static ee.stat.dashboard.util.ErrorCode.UPLOADED_FILE_IS_NOT_EXCEL;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;


@RestController
@RequestMapping("/admin/excel")
@Secured(RoleString.ADMIN)
@AllArgsConstructor
public class ExcelAdminController {

    private static final String APPLICATION_OCTET_STREAM = "application/octet-stream";
    private final ExcelFileStorageService excelFileStorageService;

    @PostMapping("/upload")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file) {
        validateToBeExcel(file);
        String fileName = excelFileStorageService.storeFile(file);

        return new UploadFileResponse(fileName, file.getContentType(), file.getSize());
    }

    @GetMapping("/download/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName,
                                                 @RequestParam(defaultValue = "DATA") ExcelType excelType,
                                                 HttpServletRequest request) {
        Resource resource = excelFileStorageService.loadFileAsResource(fileName, excelType);
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType(request, resource)))
                .header(CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    private void validateToBeExcel(@RequestParam("file") MultipartFile file) {
        if (file.getOriginalFilename() == null) {
            throw new StatBadRequestException(UPLOADED_FILE_HAS_NO_NAME, "file doesn't have a name");
        }
        int beginIndex = file.getOriginalFilename().lastIndexOf('.');
        if (beginIndex < 0) {
            throw new StatBadRequestException(UPLOADED_FILE_HAS_NO_EXTENSION, "file doesn't have extension");
        }
        String extention = file.getOriginalFilename().substring(beginIndex);
        if (!(extention.equals(".xls") || extention.equals(".xlsx"))) {
            throw new StatBadRequestException(UPLOADED_FILE_IS_NOT_EXCEL, "only .xls or .xlsx files are allowed");
        }
    }

    private String contentType(HttpServletRequest request, Resource resource) {
        try {
            String mimeType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
            return mimeType != null ? mimeType : APPLICATION_OCTET_STREAM;
        } catch (IOException ignored) {
            return APPLICATION_OCTET_STREAM;
        }
    }
}
