package ee.stat.dashboard.controller.admin;

public enum FilterType {
    FILTER_USER_AND_DATA, FILTER_DATA_ONLY, DO_NOT_FILTER;

    public boolean filterUserPreferences(){
        return this == FILTER_USER_AND_DATA;
    }

    public boolean filterGraphData() {
        return this == FILTER_USER_AND_DATA || this == FILTER_DATA_ONLY;
    }

}
