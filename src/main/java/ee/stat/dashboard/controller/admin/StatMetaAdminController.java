package ee.stat.dashboard.controller.admin;

import ee.stat.dashboard.controller.Response;
import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.service.statdata.StatDimensionsManager;
import ee.stat.dashboard.service.statdata.dimensions.xml.XmlDimension;
import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static ee.stat.dashboard.config.security.RoleString.ADMIN;
import static ee.stat.dashboard.controller.Response.of;
import static java.lang.String.format;
import static java.util.Objects.nonNull;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestController
@RequestMapping("/admin/stat")
@AllArgsConstructor
public class StatMetaAdminController {

    private final StatDimensionsManager statDimensionsManager;

    @GetMapping("/{code}/dimensions")
    @Secured(ADMIN)
    public Response<List<XmlDimension>> getJsonDimensions(
            @PathVariable("code") String code,
            @RequestParam(defaultValue = "DATA_API") StatDataType type,
            @RequestParam(required = false) Long widgetId) {
        try {
            return nonNull(widgetId)
                    ? of(statDimensionsManager.getDimensions(code, type, widgetId))
                    : of(statDimensionsManager.getDimensions(code, type));
        } catch (Exception e) {
            throw new ResponseStatusException(BAD_REQUEST, format("problem with the cube %s", code));
        }
    }
}
