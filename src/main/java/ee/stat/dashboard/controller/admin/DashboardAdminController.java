package ee.stat.dashboard.controller.admin;


import ee.stat.dashboard.config.security.RoleString;
import ee.stat.dashboard.controller.Response;
import ee.stat.dashboard.service.admin.DashboardAdminSaveService;
import ee.stat.dashboard.service.admin.DashboardAdminService;
import ee.stat.dashboard.service.admin.dashboard.dto.DashboardAdminResponse;
import ee.stat.dashboard.service.admin.dto.StatPage;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static ee.stat.dashboard.controller.Response.of;
import static ee.stat.dashboard.controller.admin.DashboardStrategy.DASHBOARDS_ONLY;
import static ee.stat.dashboard.controller.admin.DashboardStrategy.WITH_ELEMENTS;
import static ee.stat.dashboard.util.ErrorCode.DASHBOARD_ADMIN_SEARCH_TOO_SHORT;
import static org.apache.commons.lang3.BooleanUtils.isTrue;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@RestController
@RequestMapping("/admin/dashboard")
@AllArgsConstructor
public class DashboardAdminController {

    private final DashboardAdminService dashboardAdminService;
    private final DashboardAdminSaveService dashboardAdminSaveService;

    @GetMapping
    @Secured(RoleString.ADMIN)
    public Response<StatPage<DashboardAdminResponse>> findAll(
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "1000") Integer size,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) Boolean elements) {
        if (isNotBlank(name) && name.trim().length() < 3) {
            throw new StatBadRequestException(DASHBOARD_ADMIN_SEARCH_TOO_SHORT);
        }
        DashboardStrategy dashboardStrategy = isTrue(elements) ? WITH_ELEMENTS : DASHBOARDS_ONLY;
        return of(dashboardAdminService.findAll(page, size, name, dashboardStrategy));
    }

    @GetMapping("/{dashboardId}")
    @Secured(RoleString.ADMIN)
    public Response<DashboardAdminResponse> findById(@PathVariable Long dashboardId) {
        return of(dashboardAdminService.findById(dashboardId));
    }

    @PostMapping
    @Secured(RoleString.ADMIN)
    public Response<DashboardAdminResponse> save(@RequestBody DashboardAdminResponse dashboard) {
        Long save = dashboardAdminSaveService.save(dashboard);
        return of(dashboardAdminService.findById(save));
    }

    @PutMapping("/{dashboardId}")
    @Secured(RoleString.ADMIN)
    public Response<DashboardAdminResponse> update(@PathVariable Long dashboardId, @RequestBody DashboardAdminResponse dashboard) {
        Long update = dashboardAdminSaveService.update(dashboardId, dashboard);
        return of(dashboardAdminService.findById(update));
    }

    @DeleteMapping("/{dashboardId}")
    @Secured(RoleString.ADMIN)
    public void delete(@PathVariable Long dashboardId) {
        dashboardAdminSaveService.delete(dashboardId);
    }
}
