package ee.stat.dashboard.controller.admin;

import ee.stat.dashboard.controller.Response;
import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.admin.WidgetAdminSaveService;
import ee.stat.dashboard.service.admin.WidgetAdminService;
import ee.stat.dashboard.service.admin.dto.StatPage;
import ee.stat.dashboard.service.admin.dto.WidgetAdminRequestDto;
import ee.stat.dashboard.service.admin.widget.FilterValueCustomService;
import ee.stat.dashboard.service.admin.widget.dto.FilterValueCustomAdminResponse;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import ee.stat.dashboard.service.widget.widget.WidgetService;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static ee.stat.dashboard.config.security.RoleString.ADMIN;
import static ee.stat.dashboard.controller.admin.FilterType.DO_NOT_FILTER;
import static ee.stat.dashboard.controller.admin.FilterType.FILTER_DATA_ONLY;
import static ee.stat.dashboard.controller.publics.DashboardWidgetController.removeParams;
import static ee.stat.dashboard.util.ErrorCode.WIDGET_ADMIN_QUERY_TOO_SHORT;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/admin/widget")
@AllArgsConstructor
public class WidgetAdminController {

    private final WidgetService widgetService;
    private final WidgetAdminService widgetAdminService;
    private final WidgetAdminSaveService widgetAdminSaveService;
    private final FilterValueCustomService filterValueCustomService;

    @GetMapping
    @Secured(ADMIN)
    public Response<StatPage<WidgetAdminResponse>> findAll(WidgetAdminRequestDto requestDto) {
        validateQuery(requestDto.getQueryForCube());
        validateQuery(requestDto.getQueryForWidget());
        validateQuery(requestDto.getQueryForDashboard());
        return Response.of(widgetAdminService.findAll(requestDto));
    }

    @GetMapping("/{widgetId}")
    @Secured(ADMIN)
    public Response<WidgetAdminResponse> findById(@PathVariable Long widgetId) {
        return Response.of(widgetAdminService.findById(widgetId));
    }

    @GetMapping("/{widgetId}/preview")
    @Secured(ADMIN)
    public Response<WidgetResponse> preview(
            @PathVariable Long widgetId,
            @RequestParam(defaultValue = "et") String language,
            @RequestParam(required = false) String graphType,
            @RequestParam(required = false) Map<String, String> params
    ) {
        removeParams(params);
        WidgetResponse response = widgetService.findByWidgetIdOnly(widgetId, Language.getValue(language), graphType,
                params, FILTER_DATA_ONLY);
        return Response.of(response);
    }

    @GetMapping("/{widgetId}/data")
    @Secured(ADMIN)
    public Response<WidgetResponse> fullData(
            @PathVariable Long widgetId,
            @RequestParam(defaultValue = "et") String language,
            @RequestParam(required = false) String graphType,
            @RequestParam(required = false) Map<String, String> params
    ) {
        removeParams(params);
        WidgetResponse response = widgetService.findByWidgetIdOnly(widgetId, Language.getValue(language), graphType,
                params, DO_NOT_FILTER);
        return Response.of(response);
    }

    @PostMapping
    @Secured(ADMIN)
    public Response<WidgetAdminResponse> save(@RequestBody WidgetAdminResponse widget) {
        Long save = widgetAdminSaveService.save(widget);
        String calculationMessage = widgetAdminSaveService.buildNewDiagram(widget);
        WidgetAdminResponse byId = widgetAdminService.findById(save);
        if (isNotEmpty(calculationMessage)) {
            byId.setCalculationProblem(true);
            byId.setCalculationProblemMessage(calculationMessage);
        }
        return Response.of(byId);
    }

    @PutMapping("/{widgetId}")
    @Secured(ADMIN)
    public Response<WidgetAdminResponse> update(@PathVariable Long widgetId, @RequestBody WidgetAdminResponse widget) {
        Long update = widgetAdminSaveService.update(widgetId, widget);
        String calculationMessage = widgetAdminSaveService.buildNewDiagram(widget);
        WidgetAdminResponse byId = widgetAdminService.findById(update);
        if (isNotEmpty(calculationMessage)) {
            byId.setCalculationProblem(true);
            byId.setCalculationProblemMessage(calculationMessage);
        }
        return Response.of(byId);
    }

    @DeleteMapping("/{widgetId}")
    @Secured(ADMIN)
    public void delete(@PathVariable Long widgetId) {
        widgetAdminSaveService.delete(widgetId);
    }

    @GetMapping("graphTypes")
    @Secured(ADMIN)
    public Response<GraphTypeEnum[]> graphTypes() {
        return Response.of(GraphTypeEnum.values());
    }

    @PostMapping("/{widgetId}/filter-value-custom")
    @ResponseStatus(CREATED)
    @Secured(ADMIN)
    public Response<FilterValueCustomAdminResponse> saveFilterValueCustom(@PathVariable Long widgetId, @RequestBody FilterValueCustomAdminResponse adminResponse) {
        return Response.of(filterValueCustomService.saveFilterValueCustom(widgetId, adminResponse));
    }

    @DeleteMapping("/{widgetId}/filter-value-custom")
    @Secured(ADMIN)
    public void deleteCustomFilterValues(@PathVariable Long widgetId) {
        filterValueCustomService.deleteCustomFilterValues(widgetId);
    }

    private void validateQuery(String cubeQuery) {
        if (isNotBlank(cubeQuery) && cubeQuery.length() < 2) {
            throw new StatBadRequestException(WIDGET_ADMIN_QUERY_TOO_SHORT);
        }
    }
}
