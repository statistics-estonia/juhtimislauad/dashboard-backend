package ee.stat.dashboard.controller.admin;

import ee.stat.dashboard.config.security.RoleString;
import ee.stat.dashboard.controller.Response;
import ee.stat.dashboard.model.process.enums.LogStatus;
import ee.stat.dashboard.service.admin.dto.StatPage;
import ee.stat.dashboard.service.admin.process.ProcessLogQueryService;
import ee.stat.dashboard.service.admin.process.StatLogQueryService;
import ee.stat.dashboard.service.admin.process.dto.ProcessLogDto;
import ee.stat.dashboard.service.admin.process.dto.StatLogDto;
import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static ee.stat.dashboard.controller.Response.of;

@RestController
@RequestMapping("/admin/processlog")
@AllArgsConstructor
public class ProcessLogAdminController {

    private final ProcessLogQueryService processLogQueryService;
    private final StatLogQueryService statLogQueryService;

    @GetMapping
    @Secured(RoleString.ADMIN)
    public Response<StatPage<ProcessLogDto>> findAll(@RequestParam(defaultValue = "0") Integer page,
                                                     @RequestParam(defaultValue = "1000") Integer size) {
        return of(processLogQueryService.findAll(page, size));
    }

    @GetMapping(value = "{processLogId}")
    @Secured(RoleString.ADMIN)
    public Response<ProcessLogDto> findOne(@PathVariable Long processLogId) {
        return of(processLogQueryService.findById(processLogId));
    }

    @GetMapping(value = "{processLogId}/logs")
    @Secured(RoleString.ADMIN)
    public Response<StatPage<StatLogDto>> findAll(@RequestParam(defaultValue = "0") Integer page,
                                                  @RequestParam(defaultValue = "1000") Integer size,
                                                  @PathVariable Long processLogId,
                                                  @RequestParam(required = false) LogStatus status) {
        return of(statLogQueryService.findAll(page, size, processLogId, status));
    }
}
