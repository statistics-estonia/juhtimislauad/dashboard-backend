package ee.stat.dashboard.controller.user.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TaraDataDto {

    private String idCode;
    private String firstName;
    private String lastName;
    private String from;
    private String error;
    private boolean valid;
    private String ip;
}
