package ee.stat.dashboard.controller.user;

import ee.stat.dashboard.config.security.StatUser;
import ee.stat.dashboard.service.user.UserSessionService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@Controller
@AllArgsConstructor
@RequestMapping("/session")
public class SessionController {

    private final UserSessionService sessionService;

    @PostMapping("prolong")
    public void prolong() {
        StatUser loggedInUser = UserSessionHolder.getLoggedInUser();
        if (loggedInUser != null) {
            sessionService.prolong(loggedInUser);
        }
    }

    @PostMapping("logout")
    public void logout() {
        StatUser loggedInUser = UserSessionHolder.getLoggedInUser();
        if (loggedInUser != null) {
            sessionService.logout(loggedInUser);
        }
    }
}
