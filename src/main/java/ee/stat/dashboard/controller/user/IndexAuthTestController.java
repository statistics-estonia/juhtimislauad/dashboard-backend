package ee.stat.dashboard.controller.user;

import ee.stat.dashboard.config.security.RoleString;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class IndexAuthTestController {

    @GetMapping("/auth")
    @Secured(RoleString.USER)
    public String authenticated() {
        return "User is authenticated";
    }

    @GetMapping("/admin")
    @Secured(RoleString.ADMIN)
    public String admin() {
        return "User is admin";
    }
}
