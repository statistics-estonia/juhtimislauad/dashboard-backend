package ee.stat.dashboard.controller.user;

import ee.stat.dashboard.controller.Response;
import ee.stat.dashboard.model.user.user.AppUser;
import ee.stat.dashboard.service.user.UserService;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Profile("dev")
@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping
    public Response<List<AppUser>> findAll() {
        return Response.of(userService.findAll());
    }
}
