package ee.stat.dashboard.controller.user;

import ee.stat.dashboard.config.props.OAuth2Config;
import ee.stat.dashboard.controller.user.dto.TaraDataDto;
import ee.stat.dashboard.model.user.user.AppUser;
import ee.stat.dashboard.service.tara.TaraService;
import ee.stat.dashboard.service.user.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.UUID;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.text.MessageFormat.format;

@Slf4j
@Controller
@AllArgsConstructor
@RequestMapping("/login/tara")
public class LoginController {

    private final TaraService taraService;
    private final OAuth2Config oAuth2Config;
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    @GetMapping("authenticate")
    public void sendAuthRequest(HttpServletResponse response) throws IOException {
        log.info("Sending authentication request");
        String csrfToken = UUID.randomUUID().toString();
        String encoded = passwordEncoder.encode(csrfToken);
        response.addCookie(taraCsrf(encoded, 1500));
        response.sendRedirect(taraService.prepareUrl(encoded));
    }

    @GetMapping("/callback")
    public void callBackFromOAuth(
            @RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "error_description", required = false) String errorDescription,
            @RequestParam(value = "code", required = false) String authCode,
            @RequestParam(value = "state", required = false) String state,
            HttpServletRequest request, HttpServletResponse response) throws IOException {
        TaraDataDto dto = taraService.validateResponse(request, authCode, state, error, errorDescription);
        if (dto.isValid()) {
            log.info("dto is valid");
            dto.setIp(realIp(request));
            AppUser user = userService.login(dto);
            response.addCookie(taraCsrf(null, 0));
            response.sendRedirect(format("{0}?username={1}&idCode={2}&token={3}",
                    oAuth2Config.getLocalSuccessUrl(),
                    URLEncoder.encode(user.getFullName(), UTF_8),
                    URLEncoder.encode(user.getIdCode(), UTF_8),
                    user.getSession().getToken()));
        } else {
            log.info("dto is invalid");
            response.addCookie(taraCsrf(null, 0));
            response.sendRedirect(format("{0}?error={1}", oAuth2Config.getLocalFailureUrl(), dto.getError()));
        }
    }

    @GetMapping("cancel")
    public View cancel() {
        return new RedirectView(oAuth2Config.getLocalCancelUrl());
    }

    private Cookie taraCsrf(String csrfToken, int time) {
        Cookie cookie = new Cookie("TARA", csrfToken);
        cookie.setHttpOnly(true);
        cookie.setPath("/api");
        cookie.setMaxAge(time);
        return cookie;
    }

    private String realIp(HttpServletRequest request) {
        String realIp = request.getHeader("x-real-ip");
        if (StringUtils.isNotBlank(realIp)) {
            return realIp;
        }
        log.error("x-real-ip header is blank: {} using remote_addr {}", realIp, request.getRemoteAddr());
        return request.getRemoteAddr();
    }
}
