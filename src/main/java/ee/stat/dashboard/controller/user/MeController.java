package ee.stat.dashboard.controller.user;

import ee.stat.dashboard.config.security.RoleString;
import ee.stat.dashboard.config.security.StatUser;
import ee.stat.dashboard.controller.Response;
import ee.stat.dashboard.controller.user.dto.UserData;
import ee.stat.dashboard.service.user.UserService;
import ee.stat.dashboard.service.user.dto.UserNewsDto;
import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;

import static ee.stat.dashboard.controller.Response.of;
import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/me")
@AllArgsConstructor
public class MeController {

    private final UserService userService;

    @GetMapping
    @Secured(RoleString.USER)
    public Response<UserData> userData() {
        StatUser user = UserSessionHolder.getLoggedInUser();
        return user == null ? null : of(new UserData(user.getFullName(), getRoles(user.getAuthorities()), user.getSeenTheNews() ? null : true));
    }

    @PatchMapping("/news")
    @Secured(RoleString.USER)
    public Response<UserNewsDto> userData(@RequestBody UserNewsDto userNewsDto) {
        StatUser user = UserSessionHolder.getLoggedInUserOrThrow();
        userNewsDto.setId(user.getId());
        userService.seenTheNews(userNewsDto);
        return of(userNewsDto);
    }

    private List<String> getRoles(Collection<GrantedAuthority> authorities) {
        return authorities.stream().map(GrantedAuthority::getAuthority).collect(toList());
    }
}
