package ee.stat.dashboard.controller.user;

import ee.stat.dashboard.config.security.StatUser;
import ee.stat.dashboard.util.ErrorCode;
import ee.stat.dashboard.util.StatBadRequestException;
import lombok.NoArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class UserSessionHolder {

    public static StatUser getLoggedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) return null;
        Object principal = authentication.getPrincipal();
        if (principal == null) return null;
        return principal instanceof StatUser ? (StatUser) principal : null;
    }

    public static StatUser getLoggedInUserOrThrow() {
        StatUser user = getLoggedInUser();
        if (user != null) {
            return user;
        }
        throw new StatBadRequestException(ErrorCode.USER_MISSING);
    }
}
