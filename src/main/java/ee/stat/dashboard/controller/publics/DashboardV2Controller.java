package ee.stat.dashboard.controller.publics;

import ee.stat.dashboard.config.security.RoleString;
import ee.stat.dashboard.config.security.StatUser;
import ee.stat.dashboard.controller.Response;
import ee.stat.dashboard.controller.user.UserSessionHolder;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.dashboard.DashboardSaveService;
import ee.stat.dashboard.service.dashboard.DashboardService;
import ee.stat.dashboard.service.dashboard.MyDashboardService;
import ee.stat.dashboard.service.dashboard.RoleDashboardService;
import ee.stat.dashboard.service.dashboard.dto.DashboardResponse;
import ee.stat.dashboard.service.dashboard.dto.RoleDashboardResponse;
import ee.stat.dashboard.service.dashboard.dto.UserDashboardDto;
import ee.stat.dashboard.service.dashboard.dto.UserWidgetsDto;
import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static ee.stat.dashboard.controller.Response.of;

@RestController
@RequestMapping("/v2/dashboard")
@AllArgsConstructor
public class DashboardV2Controller {

    private final RoleDashboardService roleDashboardService;
    private final DashboardService dashboardService;
    private final MyDashboardService myDashboardService;
    private final DashboardSaveService dashboardSaveService;

    @GetMapping
    public Response<List<RoleDashboardResponse>> findAll(@RequestParam(defaultValue = "et") String language) {
        return of(roleDashboardService.findAll(Language.getValue(language)));
    }

    @GetMapping("/{dashboardId}")
    public Response<DashboardResponse> findById(@PathVariable Long dashboardId, @RequestParam(defaultValue = "et") String language) {
        StatUser user = UserSessionHolder.getLoggedInUser();
        return of(dashboardService.findByIdWithMainRole(dashboardId, Language.getValue(language), user));
    }

    @GetMapping("/me")
    @Secured(RoleString.USER)
    public Response<DashboardResponse> myDashboard(@RequestParam(defaultValue = "et") String language) {
        StatUser user = UserSessionHolder.getLoggedInUserOrThrow();
        return of(myDashboardService.findMyDashboard(Language.getValue(language), user));
    }

    @PatchMapping("/{dashboardId}/region")
    @Secured(RoleString.USER)
    public Response<DashboardResponse> saveSelectedRegion(@PathVariable Long dashboardId,
                                                          @RequestBody UserDashboardDto preferences) {
        StatUser user = UserSessionHolder.getLoggedInUserOrThrow();
        Long savedDashboard = dashboardSaveService.saveRegion(dashboardId, preferences, user);
        return of(dashboardService.findById(savedDashboard, Language.getValue(preferences.getLanguage()), user));
    }

    @PatchMapping("/{dashboardId}/widgets")
    @Secured(RoleString.USER)
    public Response<DashboardResponse> saveSelectedWidgets(@PathVariable Long dashboardId,
                                                           @RequestBody UserWidgetsDto preferences) {
        StatUser user = UserSessionHolder.getLoggedInUserOrThrow();
        Long savedDashboardId = dashboardSaveService.saveWidgetsAndSetVisited(dashboardId, preferences, user);

        DashboardResponse dashboardResponse = dashboardService.findById(savedDashboardId,
            Language.getValue(preferences.getLanguage()), user);

        List<RoleDashboardResponse> roles = roleDashboardService.findAll(Language.getValue(preferences.getLanguage()));
        dashboardService.updateDashboardWithRoles(dashboardResponse, savedDashboardId, roles);

        return of(dashboardResponse);
    }

}