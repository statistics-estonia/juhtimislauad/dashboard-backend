package ee.stat.dashboard.controller.publics;

import ee.stat.dashboard.config.security.RoleString;
import ee.stat.dashboard.config.security.StatUser;
import ee.stat.dashboard.controller.Response;
import ee.stat.dashboard.controller.admin.FilterType;
import ee.stat.dashboard.controller.user.UserSessionHolder;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.widget.widget.WidgetSaveService;
import ee.stat.dashboard.service.widget.widget.WidgetService;
import ee.stat.dashboard.service.widget.widget.dto.UserWidgetDto;
import ee.stat.dashboard.service.widget.widget.dto.WidgetResponse;
import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static ee.stat.dashboard.controller.Response.of;

@RestController
@RequestMapping({"/dashboard/{dashboardId}/widget/{widgetId}"})
@AllArgsConstructor
public class DashboardWidgetController {

    private final WidgetService widgetService;
    private final WidgetSaveService widgetSaveService;

    @GetMapping
    public Response<WidgetResponse> findById(@PathVariable Long dashboardId,
                                             @PathVariable Long widgetId,
                                             @RequestParam(required = false) Long ehakId,
                                             @RequestParam(defaultValue = "et") String language,
                                             @RequestParam(required = false) String graphType,
                                             @RequestParam(required = false) Map<String, String> params
    ) {
        removeParams(params);
        StatUser user = UserSessionHolder.getLoggedInUser();
        WidgetResponse response = widgetService.findById(widgetId, dashboardId,
                ehakId, Language.getValue(language), graphType, user, params, FilterType.FILTER_USER_AND_DATA);
        return of(response);
    }

    @PatchMapping("/preferences")
    @Secured(RoleString.USER)
    public Response<UserWidgetDto> savePreferences(@PathVariable Long dashboardId,
                                                   @PathVariable Long widgetId,
                                                   @RequestBody UserWidgetDto dto
    ) {
        StatUser user = UserSessionHolder.getLoggedInUserOrThrow();
        widgetSaveService.savePreferences(widgetId, dashboardId, user, dto);
        return of(dto);
    }


    @PatchMapping("/pin")
    @Secured(RoleString.USER)
    public Response<UserWidgetDto> pin(@PathVariable Long dashboardId,
                                       @PathVariable Long widgetId,
                                       @RequestBody UserWidgetDto dto
    ) {
        StatUser user = UserSessionHolder.getLoggedInUserOrThrow();
        widgetSaveService.savePin(widgetId, dashboardId, user, dto);
        return of(dto);
    }

    public static void removeParams(@RequestParam(required = false) Map<String, String> params) {
        params.remove("ehakId");
        params.remove("language");
        params.remove("graphType");
    }
}
