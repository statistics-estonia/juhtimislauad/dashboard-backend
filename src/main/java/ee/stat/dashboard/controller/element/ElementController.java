package ee.stat.dashboard.controller.element;


import ee.stat.dashboard.controller.Response;
import ee.stat.dashboard.controller.element.dto.ElementStrategy;
import ee.stat.dashboard.controller.element.dto.ParentStrategy;
import ee.stat.dashboard.model.classifier.ClassifierCode;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.element.ElementService;
import ee.stat.dashboard.service.element.dto.ElementResponse;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static ee.stat.dashboard.controller.Response.of;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@RestController
@RequestMapping("/element")
@AllArgsConstructor
public class ElementController {

    private final ElementService elementService;

    @GetMapping
    public Response<List<ElementResponse>> findAll(@RequestParam ClassifierCode clfCode,
                                                   @RequestParam(defaultValue = "et") String language,
                                                   @RequestParam(required = false) List<Integer> level,
                                                   @RequestParam(required = false) Long parentId,
                                                   @RequestParam(required = false) String elementCode,
                                                   @RequestParam(defaultValue = "FLAT") ElementStrategy elementStrategy,
                                                   @RequestParam(defaultValue = "EXCLUDE") ParentStrategy parentStrategy
    ) {
        if (isNotEmpty(level) || parentId != null || elementCode != null) {
            return of(elementService.findAll(clfCode, Language.getValue(language), level, parentId, elementCode, elementStrategy, parentStrategy));
        }
        return of(elementService.findAll(clfCode, Language.getValue(language)));
    }
}
