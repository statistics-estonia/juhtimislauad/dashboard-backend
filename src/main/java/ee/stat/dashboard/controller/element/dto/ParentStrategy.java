package ee.stat.dashboard.controller.element.dto;

public enum ParentStrategy {
    INCLUDE, EXCLUDE;

    public boolean isIncluded(){
        return this == INCLUDE;
    }
}
