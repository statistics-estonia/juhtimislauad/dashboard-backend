package ee.stat.dashboard.controller.element.dto;

public enum ElementStrategy {
    TREE, FLAT;

    public boolean isFlat(){
        return this == FLAT;
    }
}
