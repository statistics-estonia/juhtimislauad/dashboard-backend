package ee.stat.dashboard.controller.element;

import ee.stat.dashboard.controller.Response;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.element.ClassifierService;
import ee.stat.dashboard.service.element.dto.ClassifierResponse;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static ee.stat.dashboard.controller.Response.of;

@RestController
@RequestMapping("/classifier")
@AllArgsConstructor
public class ClassifierController {

    private final ClassifierService classifierService;

    @GetMapping("roles")
    public Response<List<ClassifierResponse>> findAll(@RequestParam(defaultValue = "et") String language) {
        return of(classifierService.getRoles(Language.getValue(language)));
    }
}
