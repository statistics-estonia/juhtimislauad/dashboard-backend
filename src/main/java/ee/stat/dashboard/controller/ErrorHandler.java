package ee.stat.dashboard.controller;

import ee.stat.dashboard.util.StatBadRequestException;
import ee.stat.dashboard.util.StatNotFoundRequestException;
import ee.stat.dashboard.util.StatRuntimeException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static java.time.OffsetDateTime.now;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(StatBadRequestException.class)
    @ResponseStatus(BAD_REQUEST)
    public ExceptionResponse handleStatDbBadRequest(StatBadRequestException ex) {
        return map(ex, BAD_REQUEST);
    }

    @ExceptionHandler(StatNotFoundRequestException.class)
    @ResponseStatus(NOT_FOUND)
    public ExceptionResponse handleStatDbBadRequest(StatNotFoundRequestException ex) {
        return map(ex, NOT_FOUND);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    public ExceptionResponse handleInternalError(RuntimeException ce) {
        ExceptionResponse response = new ExceptionResponse();
        response.setMessage("Error occured");
        response.setHttpStatus(INTERNAL_SERVER_ERROR.value());
        response.setTimestamp(now());
        return response;
    }

    private ExceptionResponse map(StatRuntimeException ex, HttpStatus status) {
        ExceptionResponse response = new ExceptionResponse();
        response.setCode(ex.getCode().getValue());
        response.setMessage(ex.getMessage());
        response.setHttpStatus(status.value());
        response.setTimestamp(now());
        return response;
    }
}
