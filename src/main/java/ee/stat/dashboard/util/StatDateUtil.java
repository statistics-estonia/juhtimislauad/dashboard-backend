package ee.stat.dashboard.util;


import ee.stat.dashboard.model.dashboard.Quarter;
import ee.stat.dashboard.model.dashboard.Week;
import ee.stat.dashboard.service.widget.coordinates.exception.TimePartCalculationException;
import lombok.NoArgsConstructor;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.Year;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.IsoFields;
import java.util.List;
import java.util.stream.IntStream;

import static java.lang.String.format;
import static java.time.temporal.ChronoUnit.MONTHS;
import static java.time.temporal.ChronoUnit.WEEKS;
import static java.time.temporal.ChronoUnit.YEARS;
import static java.util.stream.Collectors.toList;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class StatDateUtil {

    public static LocalDateTime getLatest(LocalDateTime date1, LocalDateTime date2) {
        if (date1 == null) return date2;
        if (date2 == null) return date1;
        return date1.isAfter(date2) ? date1 : date2;
    }

    public static LocalDate getLatest(LocalDate date1, LocalDate date2) {
        if (date1 == null) return date2;
        if (date2 == null) return date1;
        return date1.isAfter(date2) ? date1 : date2;
    }

    public static LocalDate getEarliest(LocalDate date1, LocalDate date2) {
        if (date1 == null) return date2;
        if (date2 == null) return date1;
        return date1.isBefore(date2) ? date1 : date2;
    }

    public static boolean beforeOrEquals(LocalDate date1, LocalDate date2) {
        return date1.isBefore(date2) || date1.equals(date2);
    }

    public static boolean afterOrEquals(LocalDate date1, LocalDate date2) {
        return date1.isAfter(date2) || date1.equals(date2);
    }

    public static List<LocalDate> generateDatesYear(LocalDate from, LocalDate to) {
        return generateDatesInner(from, to, YEARS, 1);
    }

    public static List<LocalDate> generateDatesMonth(LocalDate from, LocalDate to) {
        return generateDatesInner(from, to, MONTHS, 1);
    }

    public static List<LocalDate> generateDatesWeek(LocalDate from, LocalDate to) {
        return generateDatesInner(from, to, WEEKS, 1);
    }

    public static List<LocalDate> generateDatesQuarter(LocalDate from, LocalDate to) {
        return generateDatesInner(from, to, MONTHS, 3);
    }

    public static List<LocalDate> generateDatesSemester(LocalDate from, LocalDate to) {
        return generateDatesInner(from, to, MONTHS, 6);
    }

    /**
     * to is inclusive
     */
    private static List<LocalDate> generateDatesInner(LocalDate from, LocalDate to, ChronoUnit unit, Integer unitMultiplier) {
        long periodBetween = unit.between(from, to.plus(unitMultiplier, unit)) / unitMultiplier;
        return IntStream.iterate(0, i -> i + 1)
                .limit(periodBetween)
                .mapToObj(unitsToAdd -> from.plus((long) unitsToAdd * unitMultiplier, unit))
                .collect(toList());
    }

    public static LocalDate beforeDateWeek(LocalDate from, Integer periods) {
        return minus(from, periods, WEEKS, 1);
    }

    public static LocalDate beforeDateMonth(LocalDate from, Integer periods) {
        return minus(from, periods, MONTHS, 1);
    }

    public static LocalDate beforeDateYear(LocalDate from, Integer periods) {
        return minus(from, periods, YEARS, 1);
    }

    public static LocalDate beforeDateQuarter(LocalDate from, Integer periods) {
        return minus(from, periods, MONTHS, 3);
    }

    public static LocalDate beforeDateSemester(LocalDate from, Integer periods) {
        return minus(from, periods, MONTHS, 6);
    }

    private static LocalDate minus(LocalDate from, Integer periods, ChronoUnit months, Integer unitMultiplier) {
        return from.minus((long) periods * unitMultiplier, months);
    }

    public static LocalDate buildQuarterDate(Year year, Quarter unit) {
        if (year == null || unit == null) return null;
        return LocalDate.of(year.getValue(), 1, 1).with(IsoFields.QUARTER_OF_YEAR, unit.getValue());
    }

    public static LocalDate buildMonthDate(Year year, Month unit) {
        if (year == null || unit == null) return null;
        return LocalDate.of(year.getValue(), unit, 1);
    }

    public static LocalDate buildWeekDate(Year year, Week unit) throws TimePartCalculationException {
        if (year == null || unit == null) return null;
        long maximum = IsoFields.WEEK_OF_WEEK_BASED_YEAR.rangeRefinedBy(LocalDate.of(year.getValue(), Month.JUNE, 1)).getMaximum();
        if (unit.getValue() > maximum) {
            throw new TimePartCalculationException(format("Year %s does not have week %s", year.getValue(), unit.getValue()));
        }
        return LocalDate.now().
                withYear(year.getValue())
                .with(IsoFields.WEEK_OF_WEEK_BASED_YEAR, unit.getValue())
                .with(ChronoField.DAY_OF_WEEK, 1);
    }

    public static LocalDate firstWeekByYear(int year) {
        return LocalDate.now().withYear(year).with(IsoFields.WEEK_OF_WEEK_BASED_YEAR, 1).with(ChronoField.DAY_OF_WEEK, 1);
    }

    public static String toDurationStr(LocalDateTime startOfValidation){
        return toHumanString(duration(startOfValidation));
    }

    public static Duration duration(LocalDateTime startOfValidation) {
        return Duration.between(startOfValidation, LocalDateTime.now()).withNanos(0);
    }

    public static String toHumanString(Duration duration) {
        return duration.toString()
                .substring(2)
                .replaceAll("(\\d[HMS])(?!$)", "$1 ")
                .toLowerCase();
    }
}
