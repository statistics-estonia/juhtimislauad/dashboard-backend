package ee.stat.dashboard.util;

import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class StatStringUtil {

    public static final String CUT_NOTICE = " [cut string]";

    public static String cleanUp(String string) {
        return string.trim().replaceAll("^(\\.*)", "").trim();
    }

    public static String cutTo(String string, int val) {
        if (string.length() > (val - CUT_NOTICE.length())) {
            return string.substring(0, val - CUT_NOTICE.length() ) + CUT_NOTICE;
        }
        return string;
    }


}
