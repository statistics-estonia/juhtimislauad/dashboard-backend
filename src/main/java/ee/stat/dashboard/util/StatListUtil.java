package ee.stat.dashboard.util;

import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

import static lombok.AccessLevel.PRIVATE;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@NoArgsConstructor(access = PRIVATE)
public class StatListUtil {

    public static <T> List<T> union(List<T> list1, List<T> list2) {
        List<T> union = new ArrayList<>();
        union.addAll(list1);
        union.addAll(list2);
        return union;
    }

    public static <T> T firstOrNull(List<T> list) {
        return isNotEmpty(list) ? list.get(0) : null;
    }

    public static <T> T first(List<T> list) {
        return list.get(0);
    }

    public static <T> List<T> listOrNull(List<T> list) {
        return isNotEmpty(list) ? list : null;
    }
}
