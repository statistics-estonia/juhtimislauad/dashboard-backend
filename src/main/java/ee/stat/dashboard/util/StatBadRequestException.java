package ee.stat.dashboard.util;

public class StatBadRequestException extends StatRuntimeException {

    public StatBadRequestException(ErrorCode code) {
        super(code);
    }

    public StatBadRequestException(ErrorCode code, String message) {
        super(code, message);
    }
}
