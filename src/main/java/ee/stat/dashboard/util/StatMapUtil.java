package ee.stat.dashboard.util;

import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class StatMapUtil {

    public static <T> Map<String, List<T>> mapOf(String key, List<T> filter) {
        HashMap<String, List<T>> map = new HashMap<>();
        map.put(key, filter);
        return map;
    }
}
