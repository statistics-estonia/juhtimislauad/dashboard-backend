package ee.stat.dashboard.util;

public class StatNotFoundRequestException extends StatRuntimeException {

    public StatNotFoundRequestException(ErrorCode code) {
        super(code);
    }

    public StatNotFoundRequestException(ErrorCode code, String message) {
        super(code, message);
    }
}
