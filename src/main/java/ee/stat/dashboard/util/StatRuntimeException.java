package ee.stat.dashboard.util;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatRuntimeException extends RuntimeException {

    private final ErrorCode code;

    public StatRuntimeException(ErrorCode code) {
        this.code = code;
    }

    public StatRuntimeException(ErrorCode code, String message) {
        super(message);
        this.code = code;
    }
}
