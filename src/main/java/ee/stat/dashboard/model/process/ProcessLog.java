package ee.stat.dashboard.model.process;

import ee.stat.dashboard.model.process.enums.ProcessLogStatus;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

import static jakarta.persistence.EnumType.STRING;
import static jakarta.persistence.FetchType.EAGER;
import static jakarta.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
public class ProcessLog {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    @ManyToOne(fetch = EAGER)
    @JoinColumn(name = "process")
    private Process process;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    @Enumerated(STRING)
    private ProcessLogStatus status;
    private Integer analysed;
    private Integer unchanged;
    private Integer changed;
    private Integer failed;
}
