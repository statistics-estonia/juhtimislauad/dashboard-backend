package ee.stat.dashboard.model.process.enums;

public enum ProcessLogStatus {
    STARTED, ERROR, FINISHED
}
