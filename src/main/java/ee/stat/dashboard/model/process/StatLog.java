package ee.stat.dashboard.model.process;

import ee.stat.dashboard.model.process.enums.LogStatus;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

import static jakarta.persistence.EnumType.STRING;
import static jakarta.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
public class StatLog {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long processLog;
    @Enumerated(STRING)
    private LogStatus status;
    private LocalDateTime time;
    private String message;
    private String error;
    private Long widget;
    private String cube;
}
