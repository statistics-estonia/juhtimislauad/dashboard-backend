package ee.stat.dashboard.model.process.enums;

public enum LogStatus {
    INFO, ERROR;

    public boolean isInfo(){
        return this == INFO;
    }

    public boolean isError(){
        return this == ERROR;
    }
}
