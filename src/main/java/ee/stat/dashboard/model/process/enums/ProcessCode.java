package ee.stat.dashboard.model.process.enums;

public enum ProcessCode {
    STAT_DATA_API_UPDATE_AND_WIDGET_RECALC,
    STAT_DATA_SQL_UPDATE_AND_WIDGET_RECALC,
    PROCESS_LOG_CLEANUP
}
