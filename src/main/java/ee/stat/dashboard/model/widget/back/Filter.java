package ee.stat.dashboard.model.widget.back;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ee.stat.dashboard.model.widget.back.enums.AxisOrder;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.enums.TimePart;
import ee.stat.dashboard.model.widget.front.enums.FilterDisplay;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Transient;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static jakarta.persistence.EnumType.STRING;
import static jakarta.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
public class Filter {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long widget;
    private Long graphType;
    @Enumerated(STRING)
    private FilterDisplay type;
    private String nameEt;
    private String nameEn;
    private String displayNameEt;
    private String displayNameEn;
    private Integer orderNr;
    private Integer numberOfValues;
    private boolean time;
    private boolean region;
    @Enumerated(STRING)
    private TimePart timePart;
    @Enumerated(STRING)
    private AxisOrder axisOrder;

    @Transient
    private boolean special;
    @Transient
    private boolean dimAxis;
    @Transient
    private Map<Language, Map<String, FilterValue>> valueMap = new EnumMap<>(Language.class);
    @Transient
    private List<FilterValue> values = new ArrayList<>();

    @JsonIgnore
    public String getName(Language language) {
        return language.isEt() ? nameEt : nameEn;
    }

    @JsonIgnore
    public String getFrontName(Language language) {
        return language.isEt() ? getFrontNameEt() : getFrontNameEn();
    }

    @JsonIgnore
    public String getFrontNameEt() {
        return displayNameEt != null ? displayNameEt : nameEt;
    }

    @JsonIgnore
    public String getFrontNameEn() {
        return displayNameEn != null ? displayNameEn : nameEn;
    }

    @JsonIgnore
    public String getDisplayName(Language language) {
        return language.isEt() ? displayNameEt : displayNameEn;
    }

    @Override
    public String toString() {
        return "Filter{" +
                "id=" + id +
                ", widget=" + widget +
                ", displayType=" + type +
                ", nameEt='" + nameEt + '\'' +
                ", nameEn='" + nameEn + '\'' +
                ", orderNr=" + orderNr +
                ", values=" + values +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Filter filter = (Filter) o;
        return Objects.equals(id, filter.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
