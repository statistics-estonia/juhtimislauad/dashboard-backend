package ee.stat.dashboard.model.widget.back.enums;

public enum AxisOrder {

    VALUE_ASC, VALUE_DESC, KEEP_AXIS;

    public boolean valueAsc() {
        return this == VALUE_ASC;
    }

    public boolean valueDesc() {
        return this == VALUE_DESC;
    }

    public boolean axisOrder() {
        return this == KEEP_AXIS;
    }
}
