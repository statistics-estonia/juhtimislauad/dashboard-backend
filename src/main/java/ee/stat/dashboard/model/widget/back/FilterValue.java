package ee.stat.dashboard.model.widget.back;


import com.fasterxml.jackson.annotation.JsonIgnore;
import ee.stat.dashboard.model.widget.back.enums.Language;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Transient;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

import static jakarta.persistence.GenerationType.IDENTITY;
import static java.util.Objects.hash;

@Getter
@Setter
@Entity
public class FilterValue {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long filter;
    private Integer orderNr;
    private String valueEt;
    private String valueEn;
    private String displayValueEt;
    private String displayValueEn;
    private boolean selected;

    @Transient
    private boolean custom;

    @JsonIgnore
    public String getValue(Language language) {
        return language.isEt() ? valueEt : valueEn;
    }

    @JsonIgnore
    public String getFrontValue(Language language) {
        if (language.isEt()) {
            return displayValueEt != null ? displayValueEt : valueEt;
        } else {
            return displayValueEn != null ? displayValueEn : valueEn;
        }
    }

    @JsonIgnore
    public String getDisplayValue(Language language) {
        return language.isEt() ? displayValueEt : displayValueEn;
    }

    @Override
    public String toString() {
        return "FilterValue{" +
                "id=" + id +
                ", filter=" + filter +
                ", valueEt='" + valueEt + '\'' +
                ", valueEn='" + valueEn + '\'' +
                ", defaultValue=" + selected +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FilterValue that = (FilterValue) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return hash(id);
    }
}
