package ee.stat.dashboard.model.widget.back.enums;

public enum FilterValueCustomType {
    ONE_DIMENSIONAL,
    TWO_DIMENSIONAL_FIRST_MATCH,
    TWO_DIMENSIONAL_SECOND_MATCH,
    TWO_DIMENSIONAL_SAME_TYPE,
    OTHER,
}
