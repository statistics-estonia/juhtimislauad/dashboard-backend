package ee.stat.dashboard.model.widget.front.enums;

public enum DiagramStatus {
    APPROVED, ANTIQUATED
}
