package ee.stat.dashboard.model.widget.back.excel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ee.stat.dashboard.model.widget.back.enums.ExcelType;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

import static jakarta.persistence.EnumType.STRING;
import static jakarta.persistence.FetchType.LAZY;
import static jakarta.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class Excel {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long widget;
    private String filename;
    private String location;
    //one excel one or no data
    @OneToMany(mappedBy = "excel", fetch = LAZY)
    private List<ExcelData> excelData;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    @Enumerated(STRING)
    private ExcelType excelType;

    public Excel(String location, ExcelType excelType) {
        this.location = location;
        this.excelType = excelType;
    }

    @JsonIgnore
    public LocalDateTime getLatestDate(){
        return updatedAt != null ? updatedAt : createdAt;
    }
}
