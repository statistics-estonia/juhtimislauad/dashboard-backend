package ee.stat.dashboard.model.widget.back;

import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.MapRule;
import ee.stat.dashboard.model.widget.back.enums.VerticalRule;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Transient;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static jakarta.persistence.EnumType.STRING;
import static jakarta.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
public class GraphType {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long widget;
    @Enumerated(STRING)
    private GraphTypeEnum type;
    private boolean defaultType;
    @Enumerated(STRING)
    private MapRule mapRule;
    @Enumerated(STRING)
    private VerticalRule verticalRule;

    @Transient
    private List<Filter> filters = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GraphType graphType = (GraphType) o;
        return Objects.equals(id, graphType.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
