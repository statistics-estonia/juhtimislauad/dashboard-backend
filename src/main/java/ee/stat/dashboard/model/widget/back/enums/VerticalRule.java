package ee.stat.dashboard.model.widget.back.enums;

public enum VerticalRule {
    /**
     * vertical depends on the ehak
     */
    DYNAMIC,
    /**
     * all is always returned
     */
    ALL;

    public boolean isAll() {
        return this == ALL;
    }

    public boolean isDynamic() {
        return this == DYNAMIC;
    }
}
