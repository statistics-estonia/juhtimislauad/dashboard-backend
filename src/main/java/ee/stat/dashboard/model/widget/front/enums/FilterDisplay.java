package ee.stat.dashboard.model.widget.front.enums;

public enum FilterDisplay {
    LEGEND,
    MENU,
    AXIS,
    CONSTRAINT,
    TIME_PART;

    public boolean isMenu(){
        return this == MENU;
    }

    public boolean isConstraint(){
        return this == CONSTRAINT;
    }

    public boolean isLegend(){
        return this == LEGEND;
    }

    public boolean isAxis(){
        return this == AXIS;
    }

    public boolean isTimePart(){
        return this == TIME_PART;
    }

    public boolean logicFilter(){
        return this == MENU || this == LEGEND || this == AXIS;
    }

    public boolean frontEndFilters(){
        return this == MENU || this == LEGEND;
    }

}
