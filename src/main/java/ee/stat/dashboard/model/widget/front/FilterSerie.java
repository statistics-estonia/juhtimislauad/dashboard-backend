package ee.stat.dashboard.model.widget.front;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.LinkedHashMap;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class FilterSerie {

    private LinkedHashMap<String, String> filters;
    private List<String> order;
    private List<Serie> series;

    public FilterSerie(List<Serie> series) {
        this.series = series;
    }

    public FilterSerie(LinkedHashMap<String, String> filters, List<String> order, List<Serie> series) {
        this.filters = filters;
        this.order = order;
        this.series = series;
    }
}
