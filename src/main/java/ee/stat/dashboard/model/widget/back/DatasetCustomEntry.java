package ee.stat.dashboard.model.widget.back;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

import static java.util.stream.IntStream.range;

@Getter
@Setter
@AllArgsConstructor
public class DatasetCustomEntry {

    private List<String> values;
    private Number value;

    public boolean valuesDifferByOne(List<String> list) {
        long differingElementCount = range(0, values.size())
                .filter(i -> !list.get(i).equals(values.get(i)))
                .count();

        return differingElementCount == 1;
    }
}
