package ee.stat.dashboard.model.widget.back;

import ee.stat.dashboard.model.json.Source;

public enum StatDataType {

    DATA_API,
    DATA_SQL,
    /**
     * pseudo-source to describe common parts for DATA_API and DATA_SQL
     */
    DATA_SHARED
    ;

    public boolean isDataApi() {
        return this == DATA_API;
    }

    public boolean isDataSql() {
        return this == DATA_SQL;
    }

    public boolean isDataShared() {
        return this == DATA_SHARED;
    }

    public boolean sharedMatch(){
        return isDataApi() || isDataSql();
    }

    public String suffix() {
        return this == DATA_API ? " api" : " sql";
    }

    public static StatDataType convertFromSource(Source source) {
        switch (source) {
            case STAT_DATA_API:
                return DATA_API;
            case STAT_DATA_SQL:
                return DATA_SQL;
            default:
                throw new IllegalArgumentException("Unsupported source value: " + source);
        }
    }
}
