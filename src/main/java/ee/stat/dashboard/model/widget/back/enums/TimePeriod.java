package ee.stat.dashboard.model.widget.back.enums;

public enum TimePeriod {
    YEAR, MONTH, QUARTER, WEEK;

    public boolean isWeek() {
        return this == WEEK;
    }

    public boolean isMonth() {
        return this == MONTH;
    }

    public boolean isQuarter() {
        return this == QUARTER;
    }

    public boolean isYear() {
        return this == YEAR;
    }

}
