package ee.stat.dashboard.model.widget.back.enums;

public enum OperationType {
    ADDITION,
    SUBTRACTION,
    WEIGHT,
    PERCENTAGE
}
