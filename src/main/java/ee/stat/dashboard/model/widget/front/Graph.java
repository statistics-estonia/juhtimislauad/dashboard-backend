package ee.stat.dashboard.model.widget.front;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Graph {

    private Period period;
    private List<FilterSerie> series;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Graph graph = (Graph) o;
        return Objects.equals(period, graph.period) &&
                Objects.equals(series, graph.series);
    }

    @Override
    public int hashCode() {
        return Objects.hash(period, series);
    }
}
