package ee.stat.dashboard.model.widget.back.enums;

public enum ExcelType {
    DATA,
    /**
     * Used for initial import only, however still present in production database.
     */
    @Deprecated(forRemoval = true)
    WIDGET_META_AND_DATA,
    /**
     * Used for initial import only, however still present in production database.
     */
    @Deprecated(forRemoval = true)
    WIDGET_META;

    public boolean isDataOnly() {
        return this == DATA;
    }

    public boolean isWidgetMeta() {
        return this == WIDGET_META;
    }
}
