package ee.stat.dashboard.model.widget.front;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.Language;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Transient;
import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.JdbcTypeCode;

import java.util.Objects;
import org.hibernate.type.SqlTypes;

import static jakarta.persistence.EnumType.STRING;
import static jakarta.persistence.GenerationType.IDENTITY;


@Getter
@Setter
@Entity
@Convert(
    attributeName = "jsonb",
    converter = JsonBinaryType.class
)
public class DiagramData {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long widget;
    private Long graphType;
    private Long diagram;
    @Enumerated(STRING)
    private GraphTypeEnum type;
    @JdbcTypeCode(SqlTypes.JSON)
    @Column(columnDefinition = "jsonb")
    private Graph graphEt;
    @JdbcTypeCode(SqlTypes.JSON)
    @Column(columnDefinition = "jsonb")
    private Graph graphEn;

    @Transient
    private Diagram diagramObj;

    public Graph getGraph(Language lang) {
        return lang.isEt() ? graphEt : graphEn;
    }

    public void setGraph(Graph graph, Language lang) {
        if (lang.isEt()) {
            graphEt = graph;
        } else {
            graphEn = graph;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DiagramData that = (DiagramData) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
