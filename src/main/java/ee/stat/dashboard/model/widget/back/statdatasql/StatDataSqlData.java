package ee.stat.dashboard.model.widget.back.statdatasql;

import ee.stat.dashboard.model.widget.back.enums.Language;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

import static jakarta.persistence.GenerationType.IDENTITY;


@Getter
@Setter
@Entity
public class StatDataSqlData {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long statDataSql;
    private Long graphType;
    private String request;
    private String responseEt;
    private String responseEn;

    public String getResponse(Language lang) {
        return lang.isEt() ? responseEt : responseEn;
    }

    public void setResponse(String response, Language lang) {
        if (lang.isEt()) {
            this.setResponseEt(response);
        } else {
            this.setResponseEn(response);
        }
    }
}
