package ee.stat.dashboard.model.widget.back;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.enums.OperationType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static jakarta.persistence.EnumType.STRING;
import static jakarta.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FilterValueCustom {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long widget;
    private String displayNameEt;
    private String displayNameEn;
    @Column(name = "dimension_1_name_et")
    private String dimension1NameEt;
    @Column(name = "dimension_1_name_en")
    private String dimension1NameEn;
    @Column(name = "dimension_2_name_et")
    private String dimension2NameEt;
    @Column(name = "dimension_2_name_en")
    private String dimension2NameEn;
    @Column(name = "selection_value_1")
    private String selectionValue1;
    @Column(name = "selection_value_2")
    private String selectionValue2;
    @Enumerated(STRING)
    private OperationType operation;
    private boolean selected;

    @JsonIgnore
    public String getDisplayName(Language language) {
        return language.isEt()
                ? displayNameEt
                : displayNameEn;
    }

    @JsonIgnore
    public String getFirstDimensionName(Language language) {
        return language.isEt()
                ? dimension1NameEt
                : dimension1NameEn;
    }

    @JsonIgnore
    public String getSecondDimensionName(Language language) {
        return language.isEt()
                ? dimension2NameEt
                : dimension2NameEn;
    }

}
