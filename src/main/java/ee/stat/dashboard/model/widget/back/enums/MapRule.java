package ee.stat.dashboard.model.widget.back.enums;

public enum MapRule {
    /**
     * map depends on the ehak
     */
    DYNAMIC,
    /**
     * always return KOV map
     */
    KOV,
    /**
     * always return MK map
     */
    MK;

    public boolean isDynamic() {
        return this == DYNAMIC;
    }

    public boolean isKOV() {
        return this == KOV;
    }

    public boolean isMK() {
        return this == MK;
    }

    public boolean isNotDynamic() {
        return !isDynamic();
    }

    public MapType toMapType() {
        return this == KOV ? MapType.KOV : MapType.MK;
    }
}
