package ee.stat.dashboard.model.widget.back.statdataapi;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static jakarta.persistence.FetchType.LAZY;
import static jakarta.persistence.GenerationType.IDENTITY;


@Getter
@Setter
@Entity
public class StatDataApi {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long widget;
    private String cube;
    private boolean forceRebuild;
    @OneToMany(mappedBy = "statDataApi", fetch = LAZY)
    private List<StatDataApiData> statDbData = new ArrayList<>();
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private LocalDateTime publishedAt;

    @JsonIgnore
    public LocalDateTime getLatestDate(){
        return updatedAt != null ? updatedAt : createdAt;
    }
}
