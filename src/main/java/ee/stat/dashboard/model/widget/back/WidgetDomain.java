package ee.stat.dashboard.model.widget.back;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

import static jakarta.persistence.GenerationType.IDENTITY;


@Getter
@Setter
@Entity
@NoArgsConstructor
public class WidgetDomain {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long widget;
    private Long domain;
    private Long dashboard;

    public WidgetDomain(Long widget, Long domain, Long dashboard) {
        this.widget = widget;
        this.domain = domain;
        this.dashboard = dashboard;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WidgetDomain that = (WidgetDomain) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
