package ee.stat.dashboard.model.widget.back.enums;

public enum GraphTypeEnum {
    line,
    bar,
    map,
    vertical,
    stacked,
    pie,
    area,
    radar,
    treemap,
    pyramid;

    public boolean isMap() {
        return this == map;
    }

    public boolean isVertical() {
        return this == vertical;
    }

    public boolean isVerticalOrTreemap() {
        return this == vertical || this == treemap;
    }

    public boolean isVerticalOrRadar() {
        return this == vertical || this == radar;
    }

    public boolean isMapOrVertical() {
        return this == map || this == vertical;
    }

    public boolean isMapOrVerticalOrPieOrTreemap() {
        return this == map || this == vertical || this == pie || this == treemap;
    }

    /**
     * not timeline graph types
     */
    public boolean isMapOrVerticalOrPieOrTreemapOrRadarOrPyramid() {
        return this == map || this == vertical || this == pie || this == treemap || this == radar || this == pyramid;
    }

    public boolean isMapOrPieOrTreemap() {
        return this == map || this == pie || this == treemap;
    }

    public boolean isLineOrBar() {
        return this == line || this == bar;
    }

    public boolean isLine() {
        return this == line;
    }

    public boolean isPieOrVertical() {
        return this == pie || this == vertical;
    }

    public boolean isPie() {
        return this == pie;
    }

    public boolean isTreemap() {
        return this == treemap;
    }

    /**
     * timeline graph types
     */
    public boolean isLineOrBarOrStackedOrArea() {
        return this == line || this == bar || this == stacked || this == area;
    }

    public boolean isArea() {
        return this == area;
    }

    public boolean isRepresentedWithoutTotal() {
        return this == pie || this == stacked;
    }

}
