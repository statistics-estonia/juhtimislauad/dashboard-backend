package ee.stat.dashboard.model.widget.back.enums;

public enum WidgetStatus {
    VISIBLE, HIDDEN;

    public boolean isVisible() {
        return this == VISIBLE;
    }

    public boolean isHidden() {
        return this == HIDDEN;
    }
}
