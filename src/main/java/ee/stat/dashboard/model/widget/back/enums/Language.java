package ee.stat.dashboard.model.widget.back.enums;

public enum Language {
    ET, EN;

    public boolean isEt() {
        return this == ET;
    }

    public boolean isEn() {
        return this == EN;
    }

    public String toLocale() {
        return isEt() ? "et" : "en";
    }

    public static Language getValue(String language) {
        return "en".equalsIgnoreCase(language) ? EN : ET;
    }
}
