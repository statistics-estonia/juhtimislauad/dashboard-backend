package ee.stat.dashboard.model.widget.front;

import ee.stat.dashboard.model.json.FrontSource;
import ee.stat.dashboard.model.json.Source;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class Serie {

    private String abscissa;
    private LocalDate date;
    private BigDecimal value;
    private FrontSource source;
    private Boolean selected;

    public Serie(String abscissa, BigDecimal value, Source source) {
        this.abscissa = abscissa;
        this.value = value;
        this.source = source.toFront();
    }

    public Serie(LocalDate date, BigDecimal value, Source source) {
        this.date = date;
        this.value = value;
        this.source = source.toFront();
    }
}
