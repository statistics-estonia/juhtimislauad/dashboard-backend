package ee.stat.dashboard.model.widget.front;

import ee.stat.dashboard.model.widget.front.enums.DiagramStatus;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Transient;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static jakarta.persistence.EnumType.STRING;
import static jakarta.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
public class Diagram {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long widget;
    @Enumerated(STRING)
    private DiagramStatus status;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    @Transient
    private List<DiagramData> diagramData = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Diagram diagram = (Diagram) o;
        return Objects.equals(id, diagram.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
