package ee.stat.dashboard.model.widget.back;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.model.widget.back.enums.WidgetStatus;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;


import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static jakarta.persistence.EnumType.STRING;
import static jakarta.persistence.GenerationType.IDENTITY;


@Getter
@Setter
@Entity
public class Widget {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private String code;
    private String shortnameEt;
    private String shortnameEn;
    private String nameEt;
    private String nameEn;
    private String descriptionEt;
    private String descriptionEn;
    private String noteEt;
    private String noteEn;
    private String sourceEt;
    private String sourceEn;
    private String unitEt;
    private String unitEn;
    private BigDecimal divisor;
    private Integer precisionScale;
    private Integer periods;
    private String methodsLinkEt;
    private String methodsLinkEn;
    private String statisticianJobLinkEt;
    private String statisticianJobLinkEn;
    private LocalDate startDate;
    private LocalDate endDate;
    @Enumerated(STRING)
    private TimePeriod timePeriod;
    @Enumerated(STRING)
    private WidgetStatus status;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    @JsonIgnore
    public String getShortname(Language language) {
        return language.isEt() ? shortnameEt : shortnameEn;
    }

    @JsonIgnore
    public String getName(Language language) {
        return language.isEt() ? nameEt : nameEn;
    }

    @JsonIgnore
    public String getDescription(Language language) {
        return language.isEt() ? descriptionEt : descriptionEn;
    }

    @JsonIgnore
    public String getNote(Language language) {
        return language.isEt() ? noteEt : noteEn;
    }

    @JsonIgnore
    public String getSource(Language language) {
        return language.isEt() ? sourceEt : sourceEn;
    }

    @JsonIgnore
    public String getUnit(Language language) {
        return language.isEt() ? unitEt : unitEn;
    }

    @JsonIgnore
    public String getMethodsLink(Language lang) {
        return lang.isEt() ? methodsLinkEt : methodsLinkEn;
    }

    @JsonIgnore
    public String getStatisticianJobLink(Language lang) {
        return lang.isEt() ? statisticianJobLinkEt : statisticianJobLinkEn;
    }
}
