package ee.stat.dashboard.model.widget.back.excel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import ee.stat.dashboard.model.json.CoordinatedResponses;
import ee.stat.dashboard.model.widget.back.enums.Language;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import static jakarta.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
@Convert(
    attributeName = "jsonb",
    converter = JsonBinaryType.class
)
public class ExcelData {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long excel;
    @JdbcTypeCode(SqlTypes.JSON)
    @Column(columnDefinition = "jsonb")
    private CoordinatedResponses responseEt;
    @JdbcTypeCode(SqlTypes.JSON)
    @Column(columnDefinition = "jsonb")
    private CoordinatedResponses responseEn;

    @JsonIgnore
    public CoordinatedResponses getResponse(Language lang) {
        return lang.isEt() ? responseEt : responseEn;
    }

    @JsonIgnore
    public void setResponse(CoordinatedResponses response, Language lang) {
        if (lang.isEt()) {
            this.responseEt = response;
        } else
            this.responseEn = response;
    }
}
