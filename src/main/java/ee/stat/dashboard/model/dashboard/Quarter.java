package ee.stat.dashboard.model.dashboard;

import ee.stat.dashboard.model.widget.back.enums.TimePart;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Quarter implements Unit {
    I(1), II(2), III(3), IV(4);

    private final Integer value;

    @Override
    public TimePart getTimePart() {
        return TimePart.QUARTER;
    }
}
