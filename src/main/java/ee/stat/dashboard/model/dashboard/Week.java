package ee.stat.dashboard.model.dashboard;

import ee.stat.dashboard.model.widget.back.enums.TimePart;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

import static java.util.Arrays.stream;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

@Getter
@AllArgsConstructor
public enum Week implements Unit {

    _1(1),
    _2(2),
    _3(3),
    _4(4),
    _5(5),
    _6(6),
    _7(7),
    _8(8),
    _9(9),
    _10(10),
    _11(11),
    _12(12),
    _13(13),
    _14(14),
    _15(15),
    _16(16),
    _17(17),
    _18(18),
    _19(19),
    _20(20),
    _21(21),
    _22(22),
    _23(23),
    _24(24),
    _25(25),
    _26(26),
    _27(27),
    _28(28),
    _29(29),
    _30(30),
    _31(31),
    _32(32),
    _33(33),
    _34(34),
    _35(35),
    _36(36),
    _37(37),
    _38(38),
    _39(39),
    _40(40),
    _41(41),
    _42(42),
    _43(43),
    _44(44),
    _45(45),
    _46(46),
    _47(47),
    _48(48),
    _49(49),
    _50(50),
    _51(51),
    _52(52),
    _53(53);

    private final Integer value;

    private static final Map<Integer, Week> weeks = stream(Week.values())
            .collect(toMap(Week::getValue, identity()));

    public static Week valueFrom(Integer key) {
        Week week = weeks.get(key);
        if (week == null) {
            throw new IllegalArgumentException("unknown week " + key);
        }
        return week;
    }

    @Override
    public TimePart getTimePart() {
        return TimePart.WEEK;
    }
}
