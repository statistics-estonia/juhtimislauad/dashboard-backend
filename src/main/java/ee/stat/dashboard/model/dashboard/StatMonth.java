package ee.stat.dashboard.model.dashboard;

import ee.stat.dashboard.model.widget.back.enums.TimePart;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.Month;

@Getter
@AllArgsConstructor
public enum StatMonth implements Unit {
    JANUARY(Month.JANUARY),
    FEBRUARY(Month.FEBRUARY),
    MARCH(Month.MARCH),
    APRIL(Month.APRIL),
    MAY(Month.MAY),
    JUNE(Month.JUNE),
    JULY(Month.JULY),
    AUGUST(Month.AUGUST),
    SEPTEMBER(Month.SEPTEMBER),
    OCTOBER(Month.OCTOBER),
    NOVEMBER(Month.NOVEMBER),
    DECEMBER(Month.DECEMBER);

    private final Month month;

    public Integer getValue() {
        return month.getValue();
    }

    @Override
    public TimePart getTimePart() {
        return TimePart.MONTH;
    }

    public static StatMonth valueFrom(Month key) {
        StatMonth result;
        switch (key) {
            case JANUARY:
                result = StatMonth.JANUARY;
                break;
            case FEBRUARY:
                result = StatMonth.FEBRUARY;
                break;
            case MARCH:
                result = StatMonth.MARCH;
                break;
            case APRIL:
                result = StatMonth.APRIL;
                break;
            case MAY:
                result = StatMonth.MAY;
                break;
            case JUNE:
                result = StatMonth.JUNE;
                break;
            case JULY:
                result = StatMonth.JULY;
                break;
            case AUGUST:
                result = StatMonth.AUGUST;
                break;
            case SEPTEMBER:
                result = StatMonth.SEPTEMBER;
                break;
            case OCTOBER:
                result = StatMonth.OCTOBER;
                break;
            case NOVEMBER:
                result = StatMonth.NOVEMBER;
                break;
            case DECEMBER:
                result = StatMonth.DECEMBER;
                break;
            default:
                throw new IllegalArgumentException("unknown month");
        }
        return result;
    }
}
