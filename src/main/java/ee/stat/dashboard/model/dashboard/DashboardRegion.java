package ee.stat.dashboard.model.dashboard;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

import static jakarta.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
public class DashboardRegion {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long dashboard;
    private Long element;

    public DashboardRegion() {
    }

    public DashboardRegion(Long dashboard, Long element) {
        this.dashboard = dashboard;
        this.element = element;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DashboardRegion that = (DashboardRegion) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
