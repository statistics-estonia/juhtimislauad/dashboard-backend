package ee.stat.dashboard.model.dashboard;

public enum DashboardStatus {
    VISIBLE, HIDDEN;

    public boolean isVisible() {
        return this == VISIBLE;
    }

    public boolean isHidden() {
        return this == HIDDEN;
    }
}
