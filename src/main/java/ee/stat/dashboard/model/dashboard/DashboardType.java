package ee.stat.dashboard.model.dashboard;

public enum DashboardType {
    REGIONAL, GLOBAL;

    public boolean isRegional(){
        return this == REGIONAL;
    }

    public boolean isGlobal(){
        return this == GLOBAL;
    }

}
