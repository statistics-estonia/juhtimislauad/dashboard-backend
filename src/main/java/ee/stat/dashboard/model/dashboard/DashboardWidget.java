package ee.stat.dashboard.model.dashboard;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Objects;

import static jakarta.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class DashboardWidget {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long dashboard;
    private Long widget;
    private LocalDateTime createdAt;

    public DashboardWidget(Long dashboard, Long widget) {
        this.dashboard = dashboard;
        this.widget = widget;
        this.createdAt = LocalDateTime.now();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DashboardWidget that = (DashboardWidget) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(dashboard, that.dashboard) &&
                Objects.equals(widget, that.widget) &&
                Objects.equals(createdAt, that.createdAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dashboard, widget, createdAt);
    }
}
