package ee.stat.dashboard.model.dashboard;

import ee.stat.dashboard.model.widget.back.enums.Language;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

import static jakarta.persistence.EnumType.STRING;
import static jakarta.persistence.GenerationType.IDENTITY;


@Getter
@Setter
@Entity
public class Dashboard {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long appUser;
    private String code;
    private String nameEt;
    private String nameEn;
    @Enumerated(STRING)
    private DashboardStatus status;
    @Enumerated(STRING)
    private DashboardType type;
    @Enumerated(STRING)
    private DashboardUserType userType;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    public String getName(Language lang) {
        return lang.isEt() ? nameEt : nameEn;
    }

}
