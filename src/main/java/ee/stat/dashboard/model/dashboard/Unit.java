package ee.stat.dashboard.model.dashboard;

import ee.stat.dashboard.model.widget.back.enums.TimePart;

public interface Unit {

    Integer getValue();

    TimePart getTimePart();
}
