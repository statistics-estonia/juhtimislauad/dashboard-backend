package ee.stat.dashboard.model.dashboard;

public enum DashboardUserType {
    ADMIN, USER;

    public boolean isAdmin() {
        return this == ADMIN;
    }

    public boolean isUser() {
        return this == USER;
    }
}
