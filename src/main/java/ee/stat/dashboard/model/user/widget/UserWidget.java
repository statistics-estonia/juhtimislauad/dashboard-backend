package ee.stat.dashboard.model.user.widget;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static jakarta.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class UserWidget {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long appUser;
    private Long widget;
    private Long dashboard;
    private Long widgetDomain;
    private Long dashboardWidget;
    private boolean selected;
    private Integer orderNr;

    public static UserWidget fromWidgetDomain(Long appUser, Long widget, Long dashboard, Long widgetDomain){
        UserWidget uw = new UserWidget();
        uw.appUser = appUser;
        uw.widget = widget;
        uw.dashboard = dashboard;
        uw.widgetDomain = widgetDomain;
        return uw;
    }

    public static UserWidget fromDashboardWidget(Long appUser, Long widget, Long dashboard, Long dashboardWidget){
        UserWidget uw = new UserWidget();
        uw.appUser = appUser;
        uw.widget = widget;
        uw.dashboard = dashboard;
        uw.dashboardWidget = dashboardWidget;
        return uw;
    }
}
