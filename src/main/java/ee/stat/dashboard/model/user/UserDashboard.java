package ee.stat.dashboard.model.user;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static jakarta.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class UserDashboard {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long appUser;
    private Long dashboard;
    private Long region;
    private boolean visited;

    public UserDashboard(Long appUser, Long dashboard) {
        this.appUser = appUser;
        this.dashboard = dashboard;
    }
}
