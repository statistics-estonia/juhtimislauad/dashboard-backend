package ee.stat.dashboard.model.user.widget;

import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

import static jakarta.persistence.EnumType.STRING;
import static jakarta.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
public class UserGraphType {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long graphType;
    private Long userWidget;
    @Enumerated(STRING)
    private GraphTypeEnum type;
    private boolean selected;
}
