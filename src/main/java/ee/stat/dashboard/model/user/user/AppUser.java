package ee.stat.dashboard.model.user.user;

import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Transient;

import static jakarta.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
public class AppUser {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private String idCode;
    private String firstName;
    private String lastName;
    private boolean seenTheNews;

    @Transient
    private AppUserSession session;

    public String getFullName() {
        return firstName + " " + lastName;
    }
}
