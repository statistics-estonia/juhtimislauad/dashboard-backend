package ee.stat.dashboard.model.user.widget;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

import static jakarta.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
public class UserLegendValue {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long userLegend;
    private Long filter;
    private Long filterValue;
}
