package ee.stat.dashboard.model.user.user;

import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import java.time.LocalDateTime;

import static jakarta.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
public class AppUserSession {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long appUser;
    private String token;
    private String loginWith;
    private String ip;
    private LocalDateTime createdAt;
    private LocalDateTime finishedAt;
    private LocalDateTime sessionEndsAt;
}
