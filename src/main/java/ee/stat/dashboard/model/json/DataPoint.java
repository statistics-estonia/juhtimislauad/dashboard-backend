package ee.stat.dashboard.model.json;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ee.stat.dashboard.model.dashboard.Quarter;
import ee.stat.dashboard.model.dashboard.Week;
import ee.stat.dashboard.model.widget.back.enums.TimePart;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.Objects;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DataPoint {

    private Source source;
    private BigDecimal value;
    private String dim1;
    private int order1;
    private String val1;
    private String dim2;
    private int order2;
    private String val2;
    private String dim3;
    private int order3;
    private String val3;
    private String dim4;
    private int order4;
    private String val4;
    private String dim5;
    private int order5;
    private String val5;
    private TimePart timePart;
    private Week week;
    private Month month;
    private Quarter quarter;
    private Year year;
    private LocalDate date;

    @JsonIgnore
    public String getDateString() {
        return date.toString();
    }

    public boolean equalTo(DataPoint other) {
        return Objects.equals(dim1, other.dim1) &&
                Objects.equals(val1, other.val1) &&
                Objects.equals(dim2, other.dim2) &&
                Objects.equals(val2, other.val2) &&
                Objects.equals(dim3, other.dim3) &&
                Objects.equals(val3, other.val3) &&
                Objects.equals(dim4, other.dim4) &&
                Objects.equals(val4, other.val4) &&
                Objects.equals(dim5, other.dim5) &&
                Objects.equals(val5, other.val5) &&
                Objects.equals(date, other.date);
    }

    public int countOfDimensions() {
        int count = 0;
        if (dim1 != null) {
            count++;
        }
        if (dim2 != null) {
            count++;
        }
        if (dim3 != null) {
            count++;
        }
        if (dim4 != null) {
            count++;
        }
        if (dim5 != null) {
            count++;
        }
        return count;
    }

}
