package ee.stat.dashboard.model.json;

import java.util.List;

public enum Source {
    /**
     * order is important
     */
    STAT_DATA_API, STAT_DATA_SQL, EXCEL_REALITY, EXCEL_TARGET, GENERATED_REAL, GENERATED_TARGET;

    private static final List<Source> target = List.of(EXCEL_TARGET, GENERATED_TARGET);
    private static final List<Source> real = List.of(STAT_DATA_API, STAT_DATA_SQL, EXCEL_REALITY, GENERATED_REAL);

    public FrontSource toFront() {
        return real.contains(this) ? FrontSource.stat : FrontSource.excel;
    }

    public boolean isRealData() {
        return this == STAT_DATA_API || this == STAT_DATA_SQL || this == EXCEL_REALITY;
    }

    public boolean isExpectation() {
        return this == EXCEL_TARGET;
    }

    public static Source getFromFile(String code, String realLevels) {
        if (code.equalsIgnoreCase(realLevels)) {
            return EXCEL_REALITY;
        }
        return EXCEL_TARGET;
    }
}
