package ee.stat.dashboard.model.json;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CoordinatedResponse {

    private List<DataPoint> dataPoints;
    private Source source;

    public CoordinatedResponse(List<DataPoint> dataPoints) {
        this.dataPoints = dataPoints;
    }
}
