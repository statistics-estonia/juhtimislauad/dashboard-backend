package ee.stat.dashboard.model.classifier;

import ee.stat.dashboard.model.widget.back.enums.Language;
import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.*;

import static jakarta.persistence.EnumType.STRING;
import static jakarta.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
public class Classifier {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    @Enumerated(STRING)
    private ClassifierCode code;
    private String nameEt;
    private String nameEn;
    private String descriptionEt;
    private String descriptionEn;
    private Integer orderNr;

    public String getName(Language lang) {
        return lang.isEt() ? nameEt : nameEn;
    }

    public String getDescription(Language lang) {
        return lang.isEt() ? descriptionEt : descriptionEn;
    }
}
