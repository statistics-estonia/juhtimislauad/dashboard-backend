package ee.stat.dashboard.model.classifier;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

import static jakarta.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
public class ElementConnection {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long fromId;
    private Long toId;
}
