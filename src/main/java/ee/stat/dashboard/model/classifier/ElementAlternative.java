package ee.stat.dashboard.model.classifier;

import ee.stat.dashboard.model.widget.back.enums.Language;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

import static jakarta.persistence.GenerationType.IDENTITY;


@Getter
@Setter
@Entity
public class ElementAlternative {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long element;
    private String nameEt;
    private String nameEn;

    public String getName(Language lang) {
        return lang.isEt() ? nameEt : nameEn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ElementAlternative elementAlternative = (ElementAlternative) o;
        return Objects.equals(id, elementAlternative.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
