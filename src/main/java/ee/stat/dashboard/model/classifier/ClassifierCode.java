package ee.stat.dashboard.model.classifier;

import java.util.List;

public enum ClassifierCode {
    EHAK,
    GOV,
    BUSINESS;

    public boolean isEhak(){
        return this == EHAK;
    }

    public boolean isNotEhak(){
        return !isEhak();
    }

    public static List<ClassifierCode> getRoleClassifiers(){
        return List.of(GOV, EHAK, BUSINESS);
    }
}
