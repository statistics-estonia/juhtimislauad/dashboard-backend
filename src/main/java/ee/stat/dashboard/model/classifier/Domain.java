package ee.stat.dashboard.model.classifier;

import ee.stat.dashboard.model.widget.back.enums.Language;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

import static jakarta.persistence.GenerationType.IDENTITY;


@Setter
@Getter
@Entity
public class Domain {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long dashboard;
    private String code;
    private String nameEt;
    private String nameEn;
    private Integer level;
    private Integer orderNr;
    private String levelNameEt;
    private String levelNameEn;
    private boolean showLevelName;

    public String getName(Language lang) {
        return lang.isEt() ? nameEt : nameEn;
    }

    public String getLevelName(Language lang) {
        return lang.isEt() ? levelNameEt : levelNameEn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Domain domain = (Domain) o;
        return Objects.equals(id, domain.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
