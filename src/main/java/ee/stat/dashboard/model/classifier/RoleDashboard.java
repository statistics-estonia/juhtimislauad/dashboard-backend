package ee.stat.dashboard.model.classifier;

import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

import static jakarta.persistence.EnumType.STRING;
import static jakarta.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
public class RoleDashboard {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long dashboard;
    private Long roleElement;
    private Long roleClassifier;
    @Enumerated(STRING)
    private DashboardRule dashboardRule;
    private LocalDateTime createdAt;
}
