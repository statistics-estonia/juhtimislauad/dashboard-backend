package ee.stat.dashboard.model.classifier;

public enum DashboardRule {
    ONE_DASHBOARD, DASHBOARDS;

    public boolean isDashboards(){
        return this == DASHBOARDS;
    }

    public boolean isOneDashboards(){
        return this == ONE_DASHBOARD;
    }
}
