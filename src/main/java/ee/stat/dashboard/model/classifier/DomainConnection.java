package ee.stat.dashboard.model.classifier;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static jakarta.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class DomainConnection {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private Long fromId;
    private Long toId;

    public DomainConnection(Long from, Long to) {
        this.fromId = from;
        this.toId = to;
    }
}
