package ee.stat.dashboard.model.classifier;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ee.stat.dashboard.model.widget.back.enums.Language;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Transient;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Objects;

import static jakarta.persistence.FetchType.LAZY;
import static jakarta.persistence.GenerationType.IDENTITY;


@Getter
@Setter
@Entity
public class Element {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    @JsonIgnore
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "classifier")
    private Classifier classifier;
    private String code;
    private String nameEt;
    private String nameEn;
    private Integer level;

    @Transient
    private List<Element> subElements;

    @Transient
    private List<ElementAlternative> alternatives;

    public String getName(Language lang) {
        return lang.isEt() ? nameEt : nameEn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Element element = (Element) o;
        return Objects.equals(id, element.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
