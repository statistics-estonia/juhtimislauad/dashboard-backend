package ee.stat.dashboard;

import ee.stat.dashboard.config.props.CronConfig;
import ee.stat.dashboard.service.logcleanup.LogCleanupProcess;
import ee.stat.dashboard.service.statsync.datapi.StatDataApiUpdaterProcess;
import ee.stat.dashboard.service.statsync.datasql.StatDataSqlUpdaterProcess;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

import static ee.stat.dashboard.util.StatDateUtil.toDurationStr;

@Slf4j
@Component
@AllArgsConstructor
public class CronJobs {

    private final CronConfig cronConfig;
    private final StatDataApiUpdaterProcess statDataApiUpdaterProcess;
    private final StatDataSqlUpdaterProcess statDataSqlUpdaterProcess;
    private final LogCleanupProcess logCleanupProcess;

    @Scheduled(cron = "${app.cron.statDataSyncCron}", zone = "${app.cron.timezone}")
    public void statDataSyncCron() {
        LocalDateTime processStart = LocalDateTime.now();
        //todo modify w new source
        if (cronConfig.isStatDataApiEnabled()) {
            statDataApiUpdaterProcess.process();
            log.info("Stat data api sync took {}", toDurationStr(processStart));
        } else {
            log.info("Stat data api sync process is inactive");
        }

        LocalDateTime stepStart = LocalDateTime.now();
        if (cronConfig.isStatDataSqlEnabled()) {
            statDataSqlUpdaterProcess.process();
            log.info("Stat data sql sync took {}", toDurationStr(stepStart));
        } else {
            log.info("Stat data sql sync process is inactive");
        }
        log.info("Updating stat data (api & sql) update took {}", toDurationStr(processStart));
    }


    @Scheduled(cron = "${app.cron.logCleanup}", zone = "${app.cron.timezone}")
    public void logCleanup() {
        LocalDateTime processStart = LocalDateTime.now();
        if (cronConfig.isLogCleanupEnabled()) {
            logCleanupProcess.process();
            log.info("Stat log cleanup took {}", toDurationStr(processStart));
        } else {
            log.info("Stat log cleanup process is inactive");
        }
    }
}
