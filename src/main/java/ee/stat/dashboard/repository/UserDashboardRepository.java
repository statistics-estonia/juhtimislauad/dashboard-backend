package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.user.UserDashboard;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserDashboardRepository extends JpaRepository<UserDashboard, Long> {

    List<UserDashboard> findAllByDashboardAndAndAppUser(Long dashboard, Long appUser);

    void deleteByDashboard(Long dashboard);
}
