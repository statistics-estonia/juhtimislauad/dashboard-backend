package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.widget.back.FilterValue;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FilterValueRepository extends JpaRepository<FilterValue, Long> {

    List<FilterValue> findAllByFilterOrderByOrderNrAscIdAsc(Long filter);

    List<FilterValue> findAllByFilterIn(List<Long> filter);

    void deleteByIdIn(List<Long> filterValuesIds);
}
