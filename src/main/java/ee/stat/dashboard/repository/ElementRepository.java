package ee.stat.dashboard.repository;


import ee.stat.dashboard.model.classifier.Element;
import ee.stat.dashboard.repository.custom.ElementCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ElementRepository extends JpaRepository<Element, Long>, ElementCustomRepository {

}
