package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.widget.back.statdataapi.StatDataApi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StatDataApiRepository extends JpaRepository<StatDataApi, Long> {

    List<StatDataApi> findAllByOrderByWidgetAsc();

    List<StatDataApi> findByWidget(Long widget);

    boolean existsByWidget(Long widget);

    void deleteByWidget(Long widget);

    @Query(
            "SELECT sda FROM StatDataApi sda " +
                    "JOIN FETCH sda.statDbData sdad " +
                    "WHERE sdad.graphType = :graphTypeId"
    )
    StatDataApi findByGraphTypeId(Long graphTypeId);

}
