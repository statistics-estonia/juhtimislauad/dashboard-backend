package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.repository.custom.WidgetCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WidgetRepository extends JpaRepository<Widget, Long>, WidgetCustomRepository {

    List<Widget> findByNameEn(String name);

}
