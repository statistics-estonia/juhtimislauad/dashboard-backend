package ee.stat.dashboard.repository;


import ee.stat.dashboard.model.classifier.ElementConnection;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ElementConnectionRepository extends JpaRepository<ElementConnection, Long> {

}
