package ee.stat.dashboard.repository;


import ee.stat.dashboard.model.user.widget.UserLegend;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserLegendRepository extends JpaRepository<UserLegend, Long> {

    List<UserLegend> findAllByUserGraphType(Long userGraphType);

    List<UserLegend> findAllByUserGraphTypeIn(List<Long> userGraphType);

    void deleteByIdIn(List<Long> ids);

    void deleteByUserGraphTypeIn(List<Long> ids);
}
