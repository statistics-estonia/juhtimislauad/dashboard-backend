package ee.stat.dashboard.repository;


import ee.stat.dashboard.model.classifier.ElementAlternative;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ElementAlternativeRepository extends JpaRepository<ElementAlternative, Long> {

    List<ElementAlternative> findByElement(Long element);
}
