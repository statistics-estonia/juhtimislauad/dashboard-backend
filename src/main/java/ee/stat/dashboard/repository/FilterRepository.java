package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.widget.back.Filter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FilterRepository extends JpaRepository<Filter, Long> {

    List<Filter> findAllByWidget(Long widget);

    List<Filter> findAllByGraphTypeOrderByOrderNrAscIdAsc(Long widgetGraph);

    List<Filter> findAllByGraphTypeInOrderByIdAsc(List<Long> widgetGraph);

    void deleteByIdIn(List<Long> filterIds);
}
