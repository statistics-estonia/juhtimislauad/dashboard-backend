package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.widget.back.FilterValueCustomForFilter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FilterValueCustomForFilterRepository extends JpaRepository<FilterValueCustomForFilter, Long> {

    @Query(
            "SELECT fvcff FROM FilterValueCustomForFilter fvcff " +
                    "JOIN FETCH fvcff.filterValueCustom fvc " +
                    "WHERE fvcff.filter = :filterId"
    )
    List<FilterValueCustomForFilter> findAllByFilterId(Long filterId);

    @Query(
            "SELECT fvcff FROM FilterValueCustomForFilter fvcff " +
                    "JOIN FETCH fvcff.filterValueCustom fvc " +
                    "WHERE fvcff.filter in (:filterIds)" +
                    "ORDER BY fvcff.filterValueCustom.id"
    )
    List<FilterValueCustomForFilter> findAllByFilterIdInOrderByFilterValueCustomId(List<Long> filterIds);

    @Query(
            "SELECT fvcff FROM FilterValueCustomForFilter fvcff " +
                    "JOIN FETCH fvcff.filterValueCustom fvc " +
                    "WHERE fvc.widget = :widgetId"
    )
    List<FilterValueCustomForFilter> findAllByWidgetId(Long widgetId);

    void deleteByIdIn(List<Long> ids);

    void deleteByFilterIn(List<Long> filterIds);

    @Modifying
    @Query(
            "DELETE FROM FilterValueCustomForFilter fvcff " +
                    "WHERE fvcff.filterValueCustom IN " +
                    "(SELECT fvc FROM FilterValueCustom fvc WHERE fvc.widget = :widgetId)"
    )
    void deleteByWidgetId(Long widgetId);
}
