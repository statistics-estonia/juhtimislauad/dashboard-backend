package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.widget.back.FilterValueCustom;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FilterValueCustomRepository extends JpaRepository<FilterValueCustom, Long> {

    List<FilterValueCustom> findAllByWidgetOrderByIdAsc(Long widgetId);

    void deleteAllByWidget(Long widgetId);
}
