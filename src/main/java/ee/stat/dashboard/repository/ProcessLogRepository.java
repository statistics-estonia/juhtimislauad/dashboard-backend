package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.process.ProcessLog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface ProcessLogRepository extends JpaRepository<ProcessLog, Long> {

    void deleteByIdIn(List<Long> ids);

    Page<ProcessLog> findAllBy(Pageable pageable);

    List<ProcessLog> findAllByStartTimeBefore(LocalDateTime startTime);

}
