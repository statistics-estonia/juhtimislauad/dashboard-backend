package ee.stat.dashboard.repository;


import ee.stat.dashboard.model.user.widget.UserGraphType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserGraphTypeRepository extends JpaRepository<UserGraphType, Long> {

    List<UserGraphType> findAllByUserWidget(Long userWidget);

    List<UserGraphType> findAllByGraphTypeIn(List<Long> graphType);

    List<UserGraphType> findAllByUserWidgetIn(List<Long> userWidget);

    void deleteByIdIn(List<Long> ids);
}
