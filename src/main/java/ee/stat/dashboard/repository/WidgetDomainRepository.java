package ee.stat.dashboard.repository;


import ee.stat.dashboard.model.widget.back.WidgetDomain;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface WidgetDomainRepository extends JpaRepository<WidgetDomain, Long> {

    List<WidgetDomain> findAllByDashboard(Long dashboard);

    List<WidgetDomain> findAllByWidget(Long widget);

    Optional<WidgetDomain> findAllByDashboardAndWidget(Long dashboard, Long widget);

    void deleteByDashboard(Long dashboard);

    void deleteByWidget(Long widget);

    void deleteByIdIn(List<Long> id);
}
