package ee.stat.dashboard.repository;


import ee.stat.dashboard.model.user.widget.UserFilterValue;
import ee.stat.dashboard.repository.custom.UserFilterValueCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserFilterValueRepository extends JpaRepository<UserFilterValue, Long>, UserFilterValueCustomRepository {

    List<UserFilterValue> findAllByUserFilter(Long userFilter);

    List<UserFilterValue> findAllByUserFilterIn(List<Long> userFilter);

    void deleteByIdIn(List<Long> ids);

    void deleteByFilterValueIn(List<Long> filterValue);
}
