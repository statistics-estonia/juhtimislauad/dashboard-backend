package ee.stat.dashboard.repository;


import ee.stat.dashboard.model.classifier.Classifier;
import ee.stat.dashboard.model.classifier.ClassifierCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClassifierRepository extends JpaRepository<Classifier, Long> {

    Classifier findByCode(ClassifierCode code);

    List<Classifier> findByCodeInOrderByOrderNr(List<ClassifierCode> code);
}
