package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.process.StatLog;
import ee.stat.dashboard.model.process.enums.LogStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatLogRepository extends JpaRepository<StatLog, Long> {

    void deleteAllByProcessLog(Long processLog);

    Page<StatLog> findAllByProcessLog(Long processLog, Pageable pageable);

    Page<StatLog> findAllByProcessLogAndStatus(Long processLog, LogStatus status, Pageable pageable);
}
