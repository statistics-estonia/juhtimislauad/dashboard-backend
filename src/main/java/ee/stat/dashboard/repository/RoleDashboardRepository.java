package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.classifier.RoleDashboard;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RoleDashboardRepository extends JpaRepository<RoleDashboard, Long> {

    List<RoleDashboard> findByDashboard(Long dashboard);

    void deleteByDashboard(Long dashboard);
}
