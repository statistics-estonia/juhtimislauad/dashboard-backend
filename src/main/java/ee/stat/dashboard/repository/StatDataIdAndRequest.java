package ee.stat.dashboard.repository;

public interface StatDataIdAndRequest {

    Long getId();
    String getRequest();
}
