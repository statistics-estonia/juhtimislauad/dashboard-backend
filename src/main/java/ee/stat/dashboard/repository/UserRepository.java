package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.user.user.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<AppUser, Long> {

    AppUser findByIdCode(String idCode);
}
