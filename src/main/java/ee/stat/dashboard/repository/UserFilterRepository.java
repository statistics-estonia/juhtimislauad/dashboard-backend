package ee.stat.dashboard.repository;


import ee.stat.dashboard.model.user.widget.UserFilter;
import ee.stat.dashboard.repository.custom.UserFilterCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserFilterRepository extends JpaRepository<UserFilter, Long> , UserFilterCustomRepository {

    List<UserFilter> findAllByUserGraphType(Long userGraphType);

    List<UserFilter> findAllByUserGraphTypeIn(List<Long> userGraphType);

    void deleteByIdIn(List<Long> ids);

    void deleteByFilterIn(List<Long> filters);
}
