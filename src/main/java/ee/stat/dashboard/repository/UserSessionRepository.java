package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.user.user.AppUserSession;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface UserSessionRepository extends JpaRepository<AppUserSession, Long> {

    List<AppUserSession> findByAppUserAndSessionEndsAtAfterAndFinishedAtNullOrderBySessionEndsAtDesc(Long appUserId, LocalDateTime time);

}
