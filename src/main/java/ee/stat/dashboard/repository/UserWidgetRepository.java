package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.user.widget.UserWidget;
import ee.stat.dashboard.repository.custom.UserWidgetCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserWidgetRepository extends JpaRepository<UserWidget, Long>, UserWidgetCustomRepository {

    List<UserWidget> findAllByDashboard(Long dashboard);

    List<UserWidget> findAllByWidget(Long widget);

    List<UserWidget> findByAppUserAndDashboardAndWidget(Long user, Long dashboard, Long widget);

    List<UserWidget> findByAppUserAndDashboard(Long user, Long dashboard);

    List<UserWidget> findAllByWidgetDomainIn(List<Long> widgetDomain);

    void deleteByDashboard(Long dashboard);

    void deleteByWidget(Long widget);

    void deleteByWidgetDomainIn(List<Long> widgetDomain);
}
