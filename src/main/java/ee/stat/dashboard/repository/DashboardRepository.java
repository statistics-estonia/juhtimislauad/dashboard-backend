package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.dashboard.DashboardStatus;
import ee.stat.dashboard.model.dashboard.DashboardUserType;
import ee.stat.dashboard.repository.custom.DashboardCustomRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DashboardRepository extends JpaRepository<Dashboard, Long>, DashboardCustomRepository {

    Dashboard findByCode(String code);

    List<Dashboard> findByNameEt(String nameEt);

    List<Dashboard> findByNameEn(String nameEt);

    Page<Dashboard> findAllByUserType(DashboardUserType dashboardUserType, Pageable pageable);

    List<Dashboard> findAllByStatusAndUserType(DashboardStatus status, DashboardUserType dashboardUserType);

    List<Dashboard> findByAppUser(Long user);
}
