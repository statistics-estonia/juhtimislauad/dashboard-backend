package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.widget.back.statdataapi.StatDataApiData;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StatDataApiDataRepository extends JpaRepository<StatDataApiData, Long> {

    List<StatDataIdAndRequest> findRequestByGraphType(Long graphType);

    List<StatDataApiData> findByGraphType(Long graphType);

    void deleteByStatDataApiIn(List<Long> statDataApi);

    void deleteByStatDataApi(Long statDataApi);

    void deleteByGraphTypeIn(List<Long> graphType);
}
