package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.widget.back.enums.ExcelType;
import ee.stat.dashboard.model.widget.back.excel.Excel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ExcelRepository extends JpaRepository<Excel, Long> {

    boolean existsByWidget(Long widget);

    List<Excel> findByWidget(Long widget);

    List<Excel> findByWidgetAndExcelTypeIn(Long widget, List<ExcelType> excelTypes);

    void deleteByWidget(Long id);
}
