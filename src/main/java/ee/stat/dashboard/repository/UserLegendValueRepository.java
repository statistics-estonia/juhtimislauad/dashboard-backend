package ee.stat.dashboard.repository;


import ee.stat.dashboard.model.user.widget.UserLegendValue;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserLegendValueRepository extends JpaRepository<UserLegendValue, Long> {

    List<UserLegendValue> findAllByUserLegend(Long userLegend);

    List<UserLegendValue> findAllByUserLegendIn(List<Long> userLegend);

    void deleteByIdIn(List<Long> ids);

    void deleteByFilterValueIn(List<Long> values);

    void deleteByFilterIn(List<Long> filters);
}
