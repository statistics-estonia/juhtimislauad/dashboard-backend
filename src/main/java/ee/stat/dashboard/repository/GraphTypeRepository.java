package ee.stat.dashboard.repository;


import ee.stat.dashboard.model.widget.back.GraphType;
import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface GraphTypeRepository extends JpaRepository<GraphType, Long> {

    List<GraphType> findAllByWidgetOrderByIdAsc(Long widget);

    Optional<GraphType> findByWidgetAndType(Long widget, GraphTypeEnum type);

    void deleteByIdIn(List<Long> graphTypeIds);
}
