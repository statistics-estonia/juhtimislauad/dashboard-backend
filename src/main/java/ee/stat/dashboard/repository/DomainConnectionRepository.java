package ee.stat.dashboard.repository;


import ee.stat.dashboard.model.classifier.DomainConnection;
import ee.stat.dashboard.repository.custom.DomainConnectionCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DomainConnectionRepository extends JpaRepository<DomainConnection, Long>, DomainConnectionCustomRepository {

    void deleteByToIdIn(List<Long> domainId);
}
