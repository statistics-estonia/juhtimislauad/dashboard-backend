package ee.stat.dashboard.repository;


import ee.stat.dashboard.model.classifier.Domain;
import ee.stat.dashboard.repository.custom.DomainCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DomainRepository extends JpaRepository<Domain, Long>, DomainCustomRepository {

    void deleteByDashboard(Long dashboard);

    List<Domain> findByDashboard(Long dashboard);

    List<Domain> findByDashboardOrderByOrderNr(Long dashboard);

    List<Domain> findByDashboardAndLevelOrderByOrderNr(Long dashboard, Integer level);

    Domain findByCode(String code);

}
