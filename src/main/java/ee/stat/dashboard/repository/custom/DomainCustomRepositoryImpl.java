package ee.stat.dashboard.repository.custom;

import ee.stat.dashboard.model.classifier.Domain;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.List;

public class DomainCustomRepositoryImpl implements DomainCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Domain> findAllFromByTo(Long to) {
        return entityManager.createNativeQuery("" +
                "SELECT e.* from Domain e\n" +
                "join domain_connection ec on ec.from_id = e.id\n" +
                "where ec.to_id = :to\n" +
                "order by e.order_nr asc" +
                "", Domain.class)
                .setParameter("to", to)
                .getResultList();
    }

    @Override
    public List<Domain> findAllToByFrom(Long from) {
        return entityManager.createNativeQuery("" +
                "SELECT e.* from Domain e\n" +
                "join domain_connection ec on ec.to_id = e.id\n" +
                "where ec.from_id = :to \n " +
                "order by e.order_nr", Domain.class)
                .setParameter("to", from)
                .getResultList();
    }

    @Override
    public List<Domain> findAllByWidget(Long widget) {
        return entityManager.createNativeQuery("" +
                "SELECT d.* from Domain d\n" +
                "join widget_domain wd on wd.domain = d.id\n" +
                "where wd.widget = :widget", Domain.class)
                .setParameter("widget", widget)
                .getResultList();
    }

}
