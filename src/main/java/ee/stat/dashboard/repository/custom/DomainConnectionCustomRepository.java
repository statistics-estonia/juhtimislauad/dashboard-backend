package ee.stat.dashboard.repository.custom;

public interface DomainConnectionCustomRepository {

    void deleteByDashboardId(Long dashboard);
}
