package ee.stat.dashboard.repository.custom;

public interface DashboardWidgetCustomRepository {

    boolean existsByAppUserWidget(Long appUser, Long widget);

}
