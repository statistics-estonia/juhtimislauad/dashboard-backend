package ee.stat.dashboard.repository.custom;

import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.admin.dto.StatPage;
import ee.stat.dashboard.service.admin.dto.WidgetAdminRequestDto;

import java.util.List;

public interface WidgetCustomRepository {

    boolean existsWidget(Long id, Long dashboard);

    List<Widget> findWidgetByMyDashboard(Long dashboard);

    List<Widget> findWidgetsByDomain(Long domain, Language lang);

    StatPage<Widget> findAllByNameAndSources(WidgetAdminRequestDto requestDto);

}
