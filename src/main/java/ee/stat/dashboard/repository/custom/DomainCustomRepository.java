package ee.stat.dashboard.repository.custom;

import ee.stat.dashboard.model.classifier.Domain;

import java.util.List;

public interface DomainCustomRepository {

    /**
     * children
     */
    List<Domain> findAllToByFrom(Long from);

    /**
     * parents
     */
    List<Domain> findAllFromByTo(Long to);

    List<Domain> findAllByWidget(Long widget);
}
