package ee.stat.dashboard.repository.custom;

import ee.stat.dashboard.model.classifier.ClassifierCode;
import ee.stat.dashboard.model.classifier.Element;
import ee.stat.dashboard.model.dashboard.DashboardStatus;
import ee.stat.dashboard.model.dashboard.DashboardUserType;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.element.dto.RoleStrategy;

import java.util.List;

public interface ElementCustomRepository {

    /**
     * children
     */
    List<Element> findAllToByFrom(Long from);

    List<Element> findAllByClfCode(ClassifierCode code);

    List<String> findAllLowerNamesByClfCode(ClassifierCode code, Language lang, List<Integer> levels);

    List<Element> findAllByClfCodeAndRoleOrderByName(ClassifierCode code, RoleStrategy role, Language language, DashboardStatus status, DashboardUserType userType);

    List<Element> findAllByClfCodeAndElCode(ClassifierCode code, String elementCode);

    List<Element> findAllByClfCodeAndLevel(ClassifierCode code, Integer level);

    List<Element> findAllByClfCodeAndLevel(ClassifierCode code, List<Integer> level);

    List<Element> findAllDashboardRegions(Long dashboardId);

}
