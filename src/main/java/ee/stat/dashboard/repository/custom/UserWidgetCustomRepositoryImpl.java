package ee.stat.dashboard.repository.custom;

import ee.stat.dashboard.model.user.widget.UserWidget;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.List;

public class UserWidgetCustomRepositoryImpl implements UserWidgetCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<UserWidget> findByDashboardAndAppUserAndSelectedOrderByOrderNrAsc(Long dashboard, Long appUser, boolean selected) {
        return entityManager.createNativeQuery("" +
                "select uw2.*\n" +
                "from user_widget uw2\n" +
                "where id in (\n" +
                "    select min(id)\n" +
                "    from user_widget uw\n" +
                "    where dashboard = :dashboard\n" +
                "      and app_user = :appUser\n" +
                "      and uw.selected = :selected\n" +
                "    group by widget, dashboard, app_user)\n" +
                "order by uw2.order_nr", UserWidget.class)
                .setParameter("dashboard", dashboard)
                .setParameter("appUser", appUser)
                .setParameter("selected", selected)
                .getResultList();
    }
}
