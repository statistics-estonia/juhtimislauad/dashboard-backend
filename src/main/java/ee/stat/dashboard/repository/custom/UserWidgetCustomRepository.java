package ee.stat.dashboard.repository.custom;

import ee.stat.dashboard.model.user.widget.UserWidget;

import java.util.List;

public interface UserWidgetCustomRepository {

    List<UserWidget> findByDashboardAndAppUserAndSelectedOrderByOrderNrAsc(Long dashboard, Long user, boolean selected);
}
