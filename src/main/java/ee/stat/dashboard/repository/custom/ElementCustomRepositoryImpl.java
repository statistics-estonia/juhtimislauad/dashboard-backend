package ee.stat.dashboard.repository.custom;

import ee.stat.dashboard.model.classifier.ClassifierCode;
import ee.stat.dashboard.model.classifier.Element;
import ee.stat.dashboard.model.dashboard.DashboardStatus;
import ee.stat.dashboard.model.dashboard.DashboardUserType;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.element.dto.RoleStrategy;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import java.util.List;

import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

public class ElementCustomRepositoryImpl implements ElementCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Element> findAllToByFrom(Long from) {
        return entityManager.createNativeQuery("" +
                "SELECT e.* from Element e\n" +
                "join element_connection ec on ec.to_id = e.id\n" +
                "where ec.from_id = :to " +
                "order by e.id asc", Element.class)
                .setParameter("to", from)
                .getResultList();
    }

    @Override
    public List<Element> findAllByClfCode(ClassifierCode code) {
        return entityManager.createNativeQuery("" +
                "SELECT e.* from Element e\n" +
                "join Classifier c on c.id = e.classifier\n" +
                "where c.code = :code " +
                "order by e.id asc", Element.class)
                .setParameter("code", code.name())
                .getResultList();
    }

    @Override
    public List<String> findAllLowerNamesByClfCode(ClassifierCode code, Language language, List<Integer> levels) {
        String elementAlternativeName = language.isEt() ? "lower(ea.name_et)" : "lower(ea.name_en)";
        String elementName = language.isEt() ? "lower(e.name_et)" : "lower(e.name_en)";
        String levelCondition = isNotEmpty(levels) ? "  and e.level in (:level)\n" : "";

        Query query = entityManager.createNativeQuery("\n" +
                "select " + elementAlternativeName + "\n" +
                "from element e\n" +
                "         join element_alternative ea on e.id = ea.element\n" +
                "         join classifier c on c.id = e.classifier\n" +
                "\n" +
                "where c.code = :code\n" +
                levelCondition +
                "union all\n" +
                "select " + elementName + "\n" +
                "from element e\n" +
                "         join classifier c on c.id = e.classifier\n" +
                "where c.code = :code" +
                levelCondition);
        if (isNotEmpty(levels)) {
            query = query.setParameter("level", levels);
        }
        return query.setParameter("code", code.name())
                .getResultList();
    }

    @Override
    public List<Element> findAllByClfCodeAndRoleOrderByName(ClassifierCode code, RoleStrategy role, Language language, DashboardStatus status, DashboardUserType userType) {
        String roleExistsCondition = role.hasDashboard() ? " and d.id is not null \n" : " and d.id is null \n";
        String order = language.isEt() ? " order by e.name_et asc \n" : " order by e.name_en asc \n";
        String select = "SELECT DISTINCT e.*\n" +
                "from Element e\n" +
                "         join Classifier c on c.id = e.classifier\n" +
                "         left join role_dashboard rd on e.id = rd.role_element\n" +
                "         left join dashboard d on d.id = rd.dashboard and d.status = :status and d.user_type = :userType \n" +
                "where c.code = :code\n";
        return entityManager.createNativeQuery(select + roleExistsCondition + order, Element.class)
                .setParameter("code", code.name())
                .setParameter("status", status.name())
                .setParameter("userType", userType.name())
                .getResultList();
    }

    @Override
    public List<Element> findAllByClfCodeAndElCode(ClassifierCode code, String elementCode) {
        return entityManager.createNativeQuery("" +
                "SELECT e.* from Element e\n" +
                "join Classifier c on c.id = e.classifier\n" +
                "where c.code = :code" +
                " and e.code = :elCode " +
                "order by e.id asc", Element.class)
                .setParameter("code", code.name())
                .setParameter("elCode", elementCode)
                .getResultList();
    }

    @Override
    public List<Element> findAllByClfCodeAndLevel(ClassifierCode code, Integer level) {
        return entityManager.createNativeQuery("" +
                "SELECT e.* from Element e\n" +
                "join Classifier c on c.id = e.classifier\n" +
                "where c.code = :code " +
                "and e.level = :level " +
                "order by e.id asc", Element.class)
                .setParameter("code", code.name())
                .setParameter("level", level)
                .getResultList();
    }

    @Override
    public List<Element> findAllByClfCodeAndLevel(ClassifierCode code, List<Integer> level) {
        return entityManager.createNativeQuery("" +
                "SELECT e.* from Element e\n" +
                "join Classifier c on c.id = e.classifier\n" +
                "where c.code = :code " +
                "and e.level in (:level) " +
                "order by e.id asc", Element.class)
                .setParameter("code", code.name())
                .setParameter("level", level)
                .getResultList();
    }

    @Override
    public List<Element> findAllDashboardRegions(Long dashboardId) {
        return entityManager.createNativeQuery("" +
                "SELECT e.* from Element e\n" +
                "join Dashboard_Region dr on dr.element = e.id\n" +
                "where dr.dashboard = :dashboardId " +
                "order by e.id asc", Element.class)
                .setParameter("dashboardId", dashboardId)
                .getResultList();
    }
}
