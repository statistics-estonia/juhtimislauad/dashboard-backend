package ee.stat.dashboard.repository.custom;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

public class UserFilterValueCustomRepositoryImpl implements UserFilterValueCustomRepository{

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void deleteRegionFilterValuesByDashboard(Long dashboard, Long appUser) {
        entityManager.createNativeQuery("" +
                "delete\n" +
                "from user_filter_value\n" +
                "where id in (\n" +
                "    select ufv.id\n" +
                "    from user_filter_value ufv\n" +
                "             join user_filter uf on uf.id = ufv.user_filter\n" +
                "             join filter f on f.id = uf.filter\n" +
                "             join user_graph_type ugt on ugt.id = uf.user_graph_type\n" +
                "             join user_widget uw on uw.id = ugt.user_widget\n" +
                "    where uw.dashboard = :dashboard\n" +
                "      and uw.app_user = :appuser\n" +
                "      and f.region is true\n" +
                ")")
                .setParameter("dashboard", dashboard)
                .setParameter("appuser", appUser)
                .executeUpdate();
    }
}
