package ee.stat.dashboard.repository.custom;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

public class DashboardWidgetCustomRepositoryImpl implements DashboardWidgetCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public boolean existsByAppUserWidget(Long appUser, Long widget) {
        return (boolean) entityManager.createNativeQuery("" +
                "select case when count(1) > 0 then true else false end\n" +
                "from dashboard d\n" +
                "         join dashboard_widget dw on d.id = dw.dashboard\n" +
                "where d.app_user = :user\n" +
                "  and dw.widget = :widget")
                .setParameter("user", appUser)
                .setParameter("widget", widget)
                .getSingleResult();
    }
}
