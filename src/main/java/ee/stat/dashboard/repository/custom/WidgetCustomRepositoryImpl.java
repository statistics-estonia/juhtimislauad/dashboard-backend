package ee.stat.dashboard.repository.custom;

import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.back.enums.Language;
import ee.stat.dashboard.service.admin.dto.StatPage;
import ee.stat.dashboard.service.admin.dto.WidgetAdminRequestDto;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import java.math.BigInteger;
import java.util.List;

import static ee.stat.dashboard.model.dashboard.DashboardStatus.VISIBLE;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class WidgetCustomRepositoryImpl implements WidgetCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public boolean existsWidget(Long id, Long dashboard) {
        List list = entityManager.createNativeQuery("" +
                "select w.id from widget w\n" +
                "  join widget_domain wd on w.id = wd.widget " +
                "  join dashboard d on d.id = wd.dashboard " +
                "where wd.widget = :widget " +
                "and wd.dashboard = :dashboard " +
                "and d.status = :status")
                .setParameter("widget", id)
                .setParameter("dashboard", dashboard)
                .setParameter("status", VISIBLE.name())
                .getResultList();
        return !list.isEmpty();
    }

    @Override
    public List<Widget> findWidgetByMyDashboard(Long dashboard) {
        return entityManager.createNativeQuery("" +
                "SELECT distinct w.* from widget w\n" +
                "join dashboard_widget dw on w.id = dw.widget\n" +
                "where dw.dashboard = :dashboard \n", Widget.class)
                .setParameter("dashboard", dashboard)
                .getResultList();
    }

    @Override
    public List<Widget> findWidgetsByDomain(Long domain, Language lang) {
        return entityManager.createNativeQuery("" +
                "SELECT w.* from widget w\n" +
                "join widget_domain ws on w.id = ws.widget\n" +
                "where ws.domain = :domain\n " +
                (lang.isEt() ? "order by shortname_et ASC" : "order by shortname_en ASC"), Widget.class)
                .setParameter("domain", domain)
                .getResultList();
    }

    @Override
    public StatPage<Widget> findAllByNameAndSources(WidgetAdminRequestDto request) {
        String dashboardJoin = request.hasQueryForDashboard() ?
                "       left join widget_domain wd on w.id = wd.widget\n" +
                        "       left join dashboard d on d.id = wd.dashboard\n"
                : EMPTY;
        String excelJoin = request.hasNoneSource() || request.hasExcelSource() ?
                "left join excel e on w.id = e.widget\n" : EMPTY;
        String dataApiJoin = request.hasNoneSource() || request.hasDataApiSource() ?
                "left join stat_data_api sda on w.id = sda.widget\n" : EMPTY;
        String dataSqlJoin = request.hasNoneSource() || request.hasDataSqlSource() ?
                "left join stat_data_sql sds on w.id = sds.widget\n" : EMPTY;

        String where = where(request);
        Query query = entityManager.createNativeQuery("" +
                "select distinct w.*\n" +
                "from widget w\n" +
                dashboardJoin + excelJoin + dataApiJoin + dataSqlJoin +
                where +
                "order by w.shortname_et asc", Widget.class)
                .setFirstResult(request.getPage())
                .setMaxResults(request.getSize());
        List<Widget> content = addParams(query, request).getResultList();

        Query countQuery = entityManager.createNativeQuery("" +
                "select count(a.id)\n" +
                "from (\n" +
                "       select distinct w.*\n" +
                "       from widget w\n" +
                dashboardJoin + excelJoin + dataApiJoin + dataSqlJoin +
                where +
                "     ) a");

        Number result = (Number) addParams(countQuery, request).getSingleResult();
        BigInteger count = result instanceof BigInteger
                ? (BigInteger) result
                : BigInteger.valueOf(result.longValue());

        return toPage(request, content, count);
    }

    private String where(WidgetAdminRequestDto request) {
        String where = "where 1=1 \n";

        where += " and ( 1=2 \n";
        if (request.hasNoneSource()) {
            where += " or e.id is null and sda.id is null\n";
        }
        if (request.hasExcelSource()) {
            where += " or e.id is not null\n";
        }
        if (request.hasDataApiSource()) {
            where += " or sda.id is not null\n";
        }
        if (request.hasDataSqlSource()) {
            where += " or sds.id is not null\n";
        }
        where += ")\n";
        if (request.hasQuery()) {
            where += " and ( 1=2 \n";
            if (request.hasQueryForCube()) {
                if (request.hasDataApiSource()) {
                    where += " or upper(sda.cube) like :queryForCube\n";
                }
                if (request.hasDataSqlSource()) {
                    where += " or upper(sds.cube) like :queryForCube\n";
                }
            }
            if (request.hasQueryForDashboard()) {
                where += " or upper(d.name_et) like :queryForDashboard\n";
            }
            if (request.hasQueryForWidget()) {
                where += " or upper(w.name_et) like :queryForWidget\n" +
                        " or upper(w.shortname_et) like :queryForWidget\n";
            }
            where += ")\n";
        }
        return where;
    }

    private Query addParams(Query query, WidgetAdminRequestDto request) {
        if (request.hasStatSource() && isNotBlank(request.getQueryForCube())) {
            query = query.setParameter("queryForCube", "%" + request.getQueryForCube().toUpperCase() + "%");
        }
        if (isNotBlank(request.getQueryForDashboard())) {
            query = query.setParameter("queryForDashboard", "%" + request.getQueryForDashboard().toUpperCase() + "%");
        }
        if (isNotBlank(request.getQueryForWidget())) {
            query = query.setParameter("queryForWidget", "%" + request.getQueryForWidget().toUpperCase() + "%");
        }
        return query;
    }

    private StatPage<Widget> toPage(WidgetAdminRequestDto request, List<Widget> content, BigInteger count) {
        StatPage<Widget> page = new StatPage<>();
        page.setContent(content);
        int countInt = count.intValue();
        page.setTotalElements(countInt);
        page.setTotalPages(countInt == 0 ? 1 : countInt / request.getSize());
        return page;
    }
}
