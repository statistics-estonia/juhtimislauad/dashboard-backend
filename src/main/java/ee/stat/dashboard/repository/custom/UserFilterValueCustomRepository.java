package ee.stat.dashboard.repository.custom;

public interface UserFilterValueCustomRepository {

    void deleteRegionFilterValuesByDashboard(Long dashboard, Long appUser);
}
