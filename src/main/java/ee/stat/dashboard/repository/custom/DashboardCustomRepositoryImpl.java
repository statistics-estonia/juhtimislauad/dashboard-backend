package ee.stat.dashboard.repository.custom;

import ee.stat.dashboard.model.classifier.DashboardRule;
import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.dashboard.DashboardStatus;
import ee.stat.dashboard.model.dashboard.DashboardUserType;
import ee.stat.dashboard.model.widget.back.enums.Language;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.math.BigInteger;
import java.util.List;

public class DashboardCustomRepositoryImpl implements DashboardCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Dashboard> findAllByWidget(Long widget) {
        return entityManager.createNativeQuery("" +
                "SELECT DISTINCT e.* from Dashboard e\n" +
                "join widget_domain wd on wd.dashboard = e.id\n" +
                "where wd.widget = :widget", Dashboard.class)
                .setParameter("widget", widget)
                .getResultList();
    }

    @Override
    public Page<Dashboard> findAllByNameEtIsLikeAndLevel(String name, Integer level, Pageable pageable) {
        String searchName = "%" + name.toUpperCase() + "%";
        List<Dashboard> content = entityManager.createNativeQuery("" +
                "select distinct d0.* from dashboard d0\n" +
                "where upper(d0.name_et) like :name\n" +
                "and d0.user_type = :userType \n" +
                "", Dashboard.class)
                .setParameter("name", searchName)
                .setParameter("userType", DashboardUserType.ADMIN.name())
                .setFirstResult(pageable.getPageNumber())
                .setMaxResults(pageable.getPageSize())
                .getResultList();

        BigInteger count = (BigInteger) entityManager.createNativeQuery("" +
                "select count(a.id) from (\n" +
                "select distinct d0.* from dashboard d0\n" +
                "where upper(d0.name_et) like :name\n" +
                "                          ) a")
                .setParameter("name", searchName)
                .getSingleResult();
        return new PageImpl<>(content, pageable, count.longValue());
    }

    @Override
    public List<Dashboard> findByRoleElement(Long roleElement, Language lang) {
        String order = lang.isEt() ? "order by d.name_et" : "order by d.name_en";
        return entityManager.createNativeQuery("\n" +
                "select d.*\n" +
                "from dashboard d\n" +
                "         join role_dashboard rd on d.id = rd.dashboard\n" +
                "where rd.role_element = :roleElement\n" +
                " and rd.dashboard_rule = :dashboardRule\n" +
                " and d.user_type = :userType\n" +
                " and d.status = :status\n" +
                order, Dashboard.class)
                .setParameter("roleElement", roleElement)
                .setParameter("dashboardRule", DashboardRule.DASHBOARDS.name())
                .setParameter("status", DashboardStatus.VISIBLE.name())
                .setParameter("userType", DashboardUserType.ADMIN.name())
                .getResultList();
    }

    @Override
    public List<Dashboard> findByRoleClassifier(Long roleClassifier, Language lang) {
        String order = lang.isEt() ? "order by d.name_et" : "order by d.name_en";
        return entityManager.createNativeQuery("\n" +
                "select d.*\n" +
                "from dashboard d\n" +
                "         join role_dashboard rd on d.id = rd.dashboard\n" +
                "where rd.role_classifier = :roleClassifier\n" +
                " and rd.dashboard_rule = :dashboardRule\n" +
                " and d.user_type = :userType\n" +
                " and d.status = :status\n" +
                order, Dashboard.class)
                .setParameter("roleClassifier", roleClassifier)
                .setParameter("dashboardRule", DashboardRule.ONE_DASHBOARD.name())
                .setParameter("status", DashboardStatus.VISIBLE.name())
                .setParameter("userType", DashboardUserType.ADMIN.name())
                .getResultList();
    }
}
