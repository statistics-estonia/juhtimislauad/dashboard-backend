package ee.stat.dashboard.repository.custom;

public interface UserFilterCustomRepository {

    void deleteRegionFiltersByDashboard(Long dashboard, Long appUser);
}
