package ee.stat.dashboard.repository.custom;

import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.widget.back.enums.Language;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface DashboardCustomRepository {

    List<Dashboard> findAllByWidget(Long widget);

    Page<Dashboard> findAllByNameEtIsLikeAndLevel(String name, Integer level, Pageable pageable);

    List<Dashboard> findByRoleElement(Long roleElement, Language lang);

    List<Dashboard> findByRoleClassifier(Long roleClassifier, Language lang);

}
