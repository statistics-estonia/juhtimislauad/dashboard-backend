package ee.stat.dashboard.repository.custom;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

public class DomainConnectionCustomRepositoryImpl implements DomainConnectionCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void deleteByDashboardId(Long dashboard) {
        entityManager.createNativeQuery("" +
                "delete from domain_connection where id in (\n" +
                "    select dc.id from domain d\n" +
                "                          join domain_connection dc on dc.from_id = d.id\n" +
                "    where dashboard = :dashboard )")
                .setParameter("dashboard", dashboard)
                .executeUpdate();
    }

}
