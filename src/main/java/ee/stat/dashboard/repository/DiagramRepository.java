package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.widget.front.Diagram;
import ee.stat.dashboard.model.widget.front.enums.DiagramStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DiagramRepository extends JpaRepository<Diagram, Long> {

    List<Diagram> findByWidgetAndStatusOrderByCreatedAtDesc(Long widget, DiagramStatus status);

    List<Diagram> findByWidget(Long widget);

    void deleteByWidget(Long id);
}
