package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.widget.back.statdatasql.StatDataSqlData;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StatDataSqlDataRepository extends JpaRepository<StatDataSqlData, Long> {

    List<StatDataIdAndRequest> findRequestByGraphType(Long graphType);

    List<StatDataSqlData> findByGraphType(Long graphType);

    void deleteByStatDataSqlIn(List<Long> statDataApi);

    void deleteByStatDataSql(Long statDataApi);

    void deleteByGraphTypeIn(List<Long> graphType);
}
