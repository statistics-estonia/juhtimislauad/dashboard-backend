package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.widget.front.DiagramData;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DiagramDataRepository extends JpaRepository<DiagramData, Long> {

    Optional<DiagramData> findByGraphTypeAndDiagram(Long graphType, Long diagram);

    Optional<DiagramData> findByDiagramAndGraphType(Long diagram, Long graphType);

    void deleteByDiagramIn(List<Long> diagramIds);

    void deleteByGraphTypeIn(List<Long> graphTypeIds);
}
