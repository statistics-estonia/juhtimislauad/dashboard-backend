package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.widget.back.excel.ExcelData;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ExcelDataRepository extends JpaRepository<ExcelData, Long> {

    void deleteByExcelIn(List<Long> excelIds);

    void deleteByExcel(Long excel);
}
