package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.dashboard.DashboardWidget;
import ee.stat.dashboard.repository.custom.DashboardWidgetCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DashboardWidgetRepository extends JpaRepository<DashboardWidget, Long>, DashboardWidgetCustomRepository {

    List<DashboardWidget> findAllByDashboard(Long id);

    void deleteByWidget(Long id);

    void deleteByDashboard(Long id);

    boolean existsByDashboardAndWidget(Long dashboard, Long widget);

    void deleteByDashboardAndWidget(Long dashboard, Long widget);
}
