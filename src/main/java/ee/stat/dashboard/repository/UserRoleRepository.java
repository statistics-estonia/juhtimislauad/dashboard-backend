package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.user.user.AppUserRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRoleRepository extends JpaRepository<AppUserRole, Long> {

    List<AppUserRole> findByAppUser(Long appUserId);
}
