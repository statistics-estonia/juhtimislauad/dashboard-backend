package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.process.Process;
import ee.stat.dashboard.model.process.enums.ProcessCode;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProcessRepository extends JpaRepository<Process, Long> {

    Process findByCode(ProcessCode code);
}
