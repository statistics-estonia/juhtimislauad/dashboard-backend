package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.dashboard.DashboardRegion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DashboardRegionRepository extends JpaRepository<DashboardRegion, Long> {

    void deleteByDashboard(Long dashboard);

    List<DashboardRegion> findAllByDashboard(Long dashboard);
}
