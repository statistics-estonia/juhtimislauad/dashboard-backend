package ee.stat.dashboard.repository;

import ee.stat.dashboard.model.widget.back.statdatasql.StatDataSql;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StatDataSqlRepository extends JpaRepository<StatDataSql, Long> {

    List<StatDataSql> findAllByOrderByWidgetAsc();

    List<StatDataSql> findByWidget(Long widget);

    boolean existsByWidget(Long widget);

    void deleteByWidget(Long widget);

    @Query(
            "SELECT sds FROM StatDataSql sds " +
                    "JOIN FETCH sds.statDbData sdsd " +
                    "WHERE sdsd.graphType = :graphTypeId"
    )
    StatDataSql findByGraphTypeId(Long graphTypeId);

}
