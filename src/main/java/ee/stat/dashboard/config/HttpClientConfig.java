package ee.stat.dashboard.config;

import ee.stat.dashboard.config.logging.meta.RestTemplateInterceptor;
import ee.stat.dashboard.config.props.HttpClientPropsConfig;
import lombok.AllArgsConstructor;
import org.apache.hc.client5.http.config.ConnectionConfig;
import org.apache.hc.client5.http.config.RequestConfig;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManager;
import org.apache.hc.core5.http.io.SocketConfig;
import org.apache.hc.core5.util.Timeout;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
@AllArgsConstructor
public class HttpClientConfig {

    private final HttpClientPropsConfig httpClientPropsConfig;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        if (httpClientPropsConfig.invalidConfig()) throw new RuntimeException("invalid rest template config");

        ConnectionConfig connectionConfig = ConnectionConfig.custom()
            .setConnectTimeout(Timeout.ofSeconds(httpClientPropsConfig.getConnectionTimeout()))
            .build();

        SocketConfig socketConfig = SocketConfig.custom()
            .setSoTimeout(Timeout.ofSeconds(httpClientPropsConfig.getReadTimeout()))
            .build();

        RequestConfig requestConfig = RequestConfig.custom()
            .setConnectionRequestTimeout(Timeout.ofSeconds(httpClientPropsConfig.getConnectionRequestTimeout()))
            .build();

        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setDefaultSocketConfig(socketConfig);
        connectionManager.setDefaultConnectionConfig(connectionConfig);

        CloseableHttpClient httpClient = HttpClients.custom()
            .setConnectionManager(connectionManager)
            .setDefaultRequestConfig(requestConfig)
            .build();

        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
        BufferingClientHttpRequestFactory bufferingRequestFactory = new BufferingClientHttpRequestFactory(requestFactory);

        return builder
            .requestFactory(() -> bufferingRequestFactory)
            .interceptors(new RestTemplateInterceptor())
            .build();
    }
}
