package ee.stat.dashboard.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("test")
@OpenAPIDefinition(
        info = @Info(title = "Juhtimislaudade API"))
public class SwaggerConfig {

}
