package ee.stat.dashboard.config.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class StatUser extends User {

    private final Long id;
    private final String fullName;
    private final boolean seenTheNews;

    public StatUser(Long id, String fullName, boolean seenTheNews, String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
        this.id = id;
        this.fullName = fullName;
        this.seenTheNews = seenTheNews;
    }

    public StatUser(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        throw new UnsupportedOperationException("don't use this constructor");
    }

    public String getFullName() {
        return fullName;
    }

    public Long getId() {
        return id;
    }

    public boolean getSeenTheNews() {
        return seenTheNews;
    }
}
