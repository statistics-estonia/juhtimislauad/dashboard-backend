package ee.stat.dashboard.config.security;

public interface RoleString {
    String USER = "ROLE_USER";
    String ADMIN = "ROLE_ADMIN";
}
