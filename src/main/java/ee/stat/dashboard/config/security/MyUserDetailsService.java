package ee.stat.dashboard.config.security;

import ee.stat.dashboard.model.user.user.AppRoles;
import ee.stat.dashboard.model.user.user.AppUser;
import ee.stat.dashboard.model.user.user.AppUserRole;
import ee.stat.dashboard.model.user.user.AppUserSession;
import ee.stat.dashboard.repository.UserRepository;
import ee.stat.dashboard.repository.UserRoleRepository;
import ee.stat.dashboard.repository.UserSessionRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;

/**
 * Required to load up user data. As we have no password, temporary token is used as password instead.
 */
@Service
@Transactional(readOnly = true)
@AllArgsConstructor
public class MyUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;
    private final UserSessionRepository userSessionRepository;
    private final UserRoleRepository userRoleRepository;
    private final PasswordEncoder encoder;

    public UserDetails loadUserByUsername(String idCode) {
        AppUser user = userRepository.findByIdCode(idCode);
        if (user == null) {
            throw new UsernameNotFoundException("No user found with username: " + idCode);
        }

        List<AppUserSession> userLogin = userSessionRepository.findByAppUserAndSessionEndsAtAfterAndFinishedAtNullOrderBySessionEndsAtDesc(user.getId(), LocalDateTime.now());
        if (isEmpty(userLogin)) {
            throw new UsernameNotFoundException("No user login found with username: " + idCode);
        }
        String token = userLogin.get(0).getToken();

        List<AppUserRole> roles = userRoleRepository.findByAppUser(user.getId());
        List<AppRoles> roleStrings = roles.stream()
                .map(AppUserRole::getRole)
                .collect(toList());
        roleStrings.add(AppRoles.USER);
        return new StatUser(user.getId(), user.getFullName(), user.isSeenTheNews(),
                user.getIdCode(), encoder.encode(token), getAuthorities(roleStrings));
    }

    private static List<GrantedAuthority> getAuthorities(List<AppRoles> roles) {
        return roles.stream()
                .map(r -> "ROLE_" + r.name())
                .map(SimpleGrantedAuthority::new)
                .collect(toList());
    }
}
