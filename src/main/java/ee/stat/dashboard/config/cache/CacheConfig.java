package ee.stat.dashboard.config.cache;

import ee.stat.dashboard.config.props.CachePropsConfig;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static org.apache.commons.collections4.MapUtils.isNotEmpty;

@Slf4j
@Configuration
@EnableCaching
@Profile("default")
@AllArgsConstructor
public class CacheConfig {

    private CachePropsConfig cachePropsConfig;

    @Bean
    public CacheManager cacheManager() {
        CaffeineCacheManager cacheManager = new CaffeineCacheManager();
        cacheManager.setCacheNames(cachePropsConfig.getCacheNames());
        if (isNotEmpty(cachePropsConfig.getCaffeine())) {
            String spec = cachePropsConfig.getCaffeine().get("spec");
            log.info("Setting cache specification as {}", spec);
            cacheManager.setCacheSpecification(spec);
        }
        return cacheManager;
    }

}
