package ee.stat.dashboard.config.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import no.ssb.jsonstat.JsonStatModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Configuration
public class JacksonConfig {

    @Autowired
    public void configureJackson(ObjectMapper objectMapper) {
        objectMapper.setSerializationInclusion(NON_NULL);
        SimpleModule module = new SimpleModule();
        module.addSerializer(BigDecimal.class, new BigDecimalSerializer());
        objectMapper.registerModule(module);
        objectMapper.registerModule(new GuavaModule());
        objectMapper.registerModule(new JsonStatModule());
    }
}


