package ee.stat.dashboard.config.props;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "app.excel")
public class ExcelConfig {

    private String type;
    private String realLevels;
    private String separator;
    private String location;
    private Path locationPath;
    private String widgetLocation;
    private Path widgetLocationPath;

    public void setLocation(String location) {
        this.location = location;
        if (location != null)
            this.locationPath = Paths.get(location).toAbsolutePath().normalize();
    }

    public void setLocationPath(Path locationPath) {
        //ignored
    }

    public void setWidgetLocation(String widgetLocation) {
        this.widgetLocation = widgetLocation;
        if (widgetLocation != null)
            this.widgetLocationPath = Paths.get(widgetLocation).toAbsolutePath().normalize();
    }

    public void setWidgetLocationPath(Path widgetLocationPath) {
        //ignored
    }
}