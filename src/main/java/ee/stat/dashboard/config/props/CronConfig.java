package ee.stat.dashboard.config.props;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "app.cron")
public class CronConfig {

    private String statDataSyncCron;
    private String timezone;
    private boolean statDataApiEnabled;
    private boolean statDataSqlEnabled;
    private Integer statDataUpdateDelayMs;
    private boolean logCleanupEnabled;
    private Integer logCleanupDays;
}
