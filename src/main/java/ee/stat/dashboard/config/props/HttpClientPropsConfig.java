package ee.stat.dashboard.config.props;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "app.http")
public class HttpClientPropsConfig {

    private Integer readTimeout;
    private Integer connectionTimeout;
    private Integer connectionRequestTimeout;

    public boolean invalidConfig() {
        return readTimeout == null || connectionTimeout == null || connectionRequestTimeout == null;
    }
}
