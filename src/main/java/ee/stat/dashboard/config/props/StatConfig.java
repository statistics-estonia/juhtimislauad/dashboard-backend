package ee.stat.dashboard.config.props;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "app.stat")
public class StatConfig {

    private Integer urlLength;
    private String dataApiUrl;
    private String dataSqlUrl;
    private String dataApiCubesUrl;
    private String dataSqlPublishedAtUrl;
    private String dataApiPortalUrl;
    private String dataSqlPortalUrl;
}
