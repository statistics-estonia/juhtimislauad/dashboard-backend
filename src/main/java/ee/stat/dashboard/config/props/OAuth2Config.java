package ee.stat.dashboard.config.props;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "app.oauth")
public class OAuth2Config {

    private String clientId;
    private String redirectUri;
    private String cancelUri;
    private String simpleScope;
    private String TARATokenEndpoint;
    private String TARAAuthorizationEndpoint;
    private String issuer;
    private String TARAKeyEndpoint;
    private String clientSecret;
    private String localSuccessUrl;
    private String localFailureUrl;
    private String localCancelUrl;

}