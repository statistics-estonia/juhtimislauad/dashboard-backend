package ee.stat.dashboard.config.props;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "spring.cache")
public class CachePropsConfig {

    private List<String> cacheNames;
    private Map<String, String> caffeine;
}
