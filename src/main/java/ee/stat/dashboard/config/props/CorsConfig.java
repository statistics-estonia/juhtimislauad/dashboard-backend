package ee.stat.dashboard.config.props;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "management.endpoints.web.cors")
public class CorsConfig {

  String[] allowedOrigins;

  String[] allowedMethods;

  String[] allowedHeaders;

}
