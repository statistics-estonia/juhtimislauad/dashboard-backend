package ee.stat.dashboard.demo;

import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.service.admin.widget.dto.FilterAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import org.springframework.stereotype.Service;

import java.util.List;

import static ee.stat.dashboard.model.widget.back.StatDataType.DATA_API;
import static ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum.line;
import static ee.stat.dashboard.model.widget.back.enums.TimePart.WEEK;
import static ee.stat.dashboard.model.widget.back.enums.TimePeriod.YEAR;
import static ee.stat.dashboard.model.widget.front.enums.FilterDisplay.AXIS;
import static ee.stat.dashboard.model.widget.front.enums.FilterDisplay.CONSTRAINT;
import static ee.stat.dashboard.model.widget.front.enums.FilterDisplay.LEGEND;
import static ee.stat.dashboard.model.widget.front.enums.FilterDisplay.MENU;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.List.of;

@Service
public class StatLES01DataApiDemoWidget extends DemoWidget {

    @Override
    public StatDataType statDataType() {
        return DATA_API;
    }

    @Override
    public String cube() {
        return "LES01";
    }

    @Override
    public TimePeriod timePeriod() {
        return YEAR;
    }

    @Override
    public WidgetAdminResponse composeWidget() {
        WidgetAdminResponse widget = widget(null, null, 10, 1);

        widget.setGraphTypes(singletonList(graphTypeSelected(line, lineFilters())));
        return widget;
    }

    @Override
    public String nameEn() {
        return super.nameEn() + statDataType().suffix();
    }

    private List<FilterAdminDto> lineFilters() {
        return asList(
                filter(AXIS, "Vaatlusperiood", "Reference period"),

                filter(CONSTRAINT, "Näitaja", "Indicator",
                        of(filterValue("Absoluutse vaesuse määr, %", "Absolute poverty rate, %"))),

                filter(CONSTRAINT, "Väärtus/standardviga", "Value/standard error",
                        of(filterValue("Väärtus", "Value"))),

                filter(LEGEND, "Vanuserühm", "Age group",
                        asList(filterValue("0 kuni 17 aastat", "From 0 to 17 years"),
                                filterValue("18 kuni 64 aastat", "From 18 to 64 years"),
                                filterValue("Vähemalt 65 aastat", "65 years or over"))),

                filter(MENU, "Sugu", "Sex", 3, 1, false, false, WEEK,
                        null, asList(filterValueSelected("Kokku", "Total"),
                                filterValue("Mehed", "Males"),
                                filterValue("Naised", "Females")))
        );
    }
}
