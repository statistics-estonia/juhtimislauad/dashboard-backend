package ee.stat.dashboard.demo;

import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.model.widget.front.enums.FilterDisplay;
import ee.stat.dashboard.service.admin.widget.dto.FilterAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Arrays.asList;

@Service
public class StatVKT14DataSqlDemoWidget extends DemoWidget {

    @Override
    public StatDataType statDataType() {
        return StatDataType.DATA_SQL;
    }

    @Override
    public String cube() {
        return "VKT14";
    }

    @Override
    public TimePeriod timePeriod() {
        return TimePeriod.YEAR;
    }

    @Override
    public WidgetAdminResponse composeWidget() {
        WidgetAdminResponse widget = widget(null, null, 6, 1);

        widget.setGraphTypes(asList(graphTypeSelected(GraphTypeEnum.line, lineFilters())));
        return widget;
    }

    @Override
    public String nameEn() {
        return super.nameEn() + statDataType().suffix();
    }

    private List<FilterAdminDto> lineFilters() {
        return asList(
                filter(FilterDisplay.AXIS, "aasta", "year"),

                filter(FilterDisplay.CONSTRAINT, "näitaja", "indicator",
                        asList(filterValue("Teenuse väärtus, miljonit eurot", "Service value, million euros"))),

                filter(FilterDisplay.CONSTRAINT, "omaniku liik", "type of ownership",
                        asList(filterValue("Kokku", "Total"))),

                filter(FilterDisplay.MENU, "voog", "flow",
                        asList(
                                filterValueSelected("Bilanss", "Balance"),
                                filterValue("Import", "Imports"),
                                filterValue("Eksport", "Exports")
                        )),

                filter(FilterDisplay.LEGEND, "teenus (EBOPS)", "service (EBOPS)",
                        3, 0, null, null, null, null,
                        asList(
                                filterValue("Kokku", "Total"),
                                filterValue("Transport", "Transport"),
                                filterValue("Meretransport", "Sea Transport"),
                                filterValue("Õhutransport", "Air transport")
                        )
                )
        );
    }
}
