package ee.stat.dashboard.demo;

import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.TimePart;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.model.widget.front.enums.FilterDisplay;
import ee.stat.dashboard.service.admin.widget.dto.FilterAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

import static ee.stat.dashboard.model.widget.back.StatDataType.DATA_API;
import static ee.stat.dashboard.model.widget.front.enums.FilterDisplay.AXIS;
import static ee.stat.dashboard.model.widget.front.enums.FilterDisplay.CONSTRAINT;
import static ee.stat.dashboard.model.widget.front.enums.FilterDisplay.LEGEND;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

@Service
public class StatEKS21DataApiDemoWidget extends DemoWidget {

    @Override
    public String cube() {
        return "EKS21";
    }

    @Override
    public StatDataType statDataType() {
        return DATA_API;
    }

    @Override
    public TimePeriod timePeriod() {
        return TimePeriod.WEEK;
    }

    @Override
    public String nameEn() {
        return super.nameEn() + statDataType().suffix();
    }

    @Override
    public WidgetAdminResponse composeWidget() {
        WidgetAdminResponse widget = widget(LocalDate.of(2020, 3, 2), null, 10, 1);

        widget.setGraphTypes(singletonList(graphTypeSelected(GraphTypeEnum.line, lineFilters())));
        return widget;
    }

    private List<FilterAdminDto> lineFilters() {
        return asList(
                filter(AXIS, "Vaatlusperiood", "Reference period"),

                filter(FilterDisplay.TIME_PART, "Nädal", "Week", TimePart.WEEK),

                filter(CONSTRAINT, "Näitaja", "Indicator",
                        List.of(filterValue("Tarbimine, GWh", "Consumption, GWh"))),

                filter(LEGEND, "Kliendigrupp", "Customer group",
                        asList(filterValue("Suuräriklient", "Large business customer"), filterValue("Keskmine äriklient", "Medium business customer"), filterValue("Väikeäriklient", "Small business customer"), filterValue("Koduklient", "Residential customer")))
        );
    }
}
