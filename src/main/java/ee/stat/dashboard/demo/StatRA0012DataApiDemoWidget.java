package ee.stat.dashboard.demo;

import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.TimePart;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.model.widget.front.enums.FilterDisplay;
import ee.stat.dashboard.service.admin.widget.dto.FilterAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Arrays.asList;

@Service
public class StatRA0012DataApiDemoWidget extends DemoWidget {

    @Override
    public StatDataType statDataType() {
        return StatDataType.DATA_API;
    }

    @Override
    public String cube() {
        return "RAA0012";
    }

    @Override
    public TimePeriod timePeriod() {
        return TimePeriod.QUARTER;
    }

    @Override
    public WidgetAdminResponse composeWidget() {
        WidgetAdminResponse widget = widget(null, null, 40, 1);

        widget.setGraphTypes(asList(graphTypeSelected(GraphTypeEnum.bar, areaFilters())));
        return widget;
    }

    @Override
    public String nameEn() {
        return super.nameEn() + statDataType().suffix();
    }

    private List<FilterAdminDto> areaFilters() {
        return asList(
                filter(FilterDisplay.AXIS, "Aasta", "Year"),

                filter(FilterDisplay.TIME_PART, "Kvartal", "Quarter", TimePart.QUARTER),

                filter(FilterDisplay.CONSTRAINT, "Näitaja", "Indicator",
                        asList(
                                filterValue("SKP jooksevhindades, miljonit eurot",
                                        "GDP at current prices, million euros"))),

                filter(FilterDisplay.LEGEND, "Sesoonne korrigeerimine", "Adjustment",
                        asList(
                                filterValue("Sesoonselt ja tööpäevade arvuga korrigeerimata",
                                        "Seasonally and working day unadjusted"),
                                filterValue("Sesoonselt ja tööpäevade arvuga korrigeeritud",
                                        "Seasonally and working day adjusted")
                        )
                )
        );
    }
}
