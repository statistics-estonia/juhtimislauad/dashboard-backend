package ee.stat.dashboard.demo;

import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.model.widget.back.enums.AxisOrder;
import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.MapRule;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.model.widget.front.enums.FilterDisplay;
import ee.stat.dashboard.service.admin.widget.dto.FilterAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Arrays.asList;

@Service
public class StatTU122DataApiDemoWidget extends DemoWidget {

    @Override
    public StatDataType statDataType() {
        return StatDataType.DATA_API;
    }

    @Override
    public String cube() {
        return "TU122";
    }

    @Override
    public TimePeriod timePeriod() {
        return TimePeriod.MONTH;
    }

    @Override
    public WidgetAdminResponse composeWidget() {
        WidgetAdminResponse widget = widget(null, null, 60, 1);

        widget.setGraphTypes(asList(graphTypeSelected(GraphTypeEnum.map, mapFilters(), MapRule.KOV)));
        return widget;
    }

    @Override
    public String nameEn() {
        return super.nameEn() + statDataType().suffix();
    }

    private List<FilterAdminDto> mapFilters() {
        return asList(
                filter(FilterDisplay.MENU, "Vaatlusperiood", "Reference period"),

                filter(FilterDisplay.CONSTRAINT, "Näitaja", "Indicator",
                        asList(filterValue("Majutuskohtade arv", "Number of accommodation establishments"))),

                filter(FilterDisplay.AXIS, "Maakond", "County",
                        3, 0, false, true, null, AxisOrder.VALUE_DESC,
                        asList(
                                filterValue("Eesti", "Estonia"),
                                filterValue("Harju maakord", "Harju county"),
                                filterValue("Tallinn", "Tallinn")
                        )
                )
        );
    }
}
