package ee.stat.dashboard.demo;

import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.model.widget.back.enums.AxisOrder;
import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.model.widget.front.enums.FilterDisplay;
import ee.stat.dashboard.service.admin.widget.dto.FilterAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

import static java.util.Arrays.asList;

@Service
public class StatEH47UDataApiDemoWidget extends DemoWidget {

    @Override
    public StatDataType statDataType() {
        return StatDataType.DATA_API;
    }

    @Override
    public String cube() {
        return "EH47U";
    }

    @Override
    public TimePeriod timePeriod() {
        return TimePeriod.YEAR;
    }

    @Override
    public WidgetAdminResponse composeWidget() {
        WidgetAdminResponse widget = widget(LocalDate.of(2018, 1, 1), null, 60, 1);

        widget.setGraphTypes(asList(graphTypeSelected(GraphTypeEnum.treemap, treeFilters())));
        return widget;
    }

    @Override
    public String nameEn() {
        return super.nameEn() + statDataType().suffix();
    }

    private List<FilterAdminDto> treeFilters() {
        return asList(
                filter(FilterDisplay.MENU, "Vaatlusperiood", "Reference period"),

                filter(FilterDisplay.CONSTRAINT, "Näitaja", "Indicator",
                        asList(filterValue("Hoonete suletud netopind, m²", "Useful floor area of non-residential buildings, m²"))),

                filter(FilterDisplay.AXIS, "Hoone tüüp", "Type of non-residential building",
                        3, 0, false, false, null, AxisOrder.VALUE_ASC,
                        asList(
                                filterValue("Tööstushooned", "Industrial buildings"),
                                filterValue("Majutushooned", "Hotels and similar buildings"),
                                filterValue("Toitlustushooned", "Buildings for catering facilities")
                        )),

                filter(FilterDisplay.MENU, "Piirkond/haldusüksus", "Piirkond",
                        "Region/administrative unit", "Region", 1, 1, false, false, null, null,
                        asList(filterValueSelected("Eesti", "Estonia"))
                )
        );
    }
}
