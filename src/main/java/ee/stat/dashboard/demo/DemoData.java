package ee.stat.dashboard.demo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static ee.stat.dashboard.demo.DemoData.DemoWidgetRunMode.dwrm;
import static ee.stat.dashboard.demo.DemoRunMode.ONCE;

@Slf4j
@Service
@AllArgsConstructor
public class DemoData {

    private final StatEKS21DataApiDemoWidget statEKS21DataApiDemoWidget;
    private final StatEKS31DataApiDemoWidget statEKS31DataApiDemoWidget;
    private final StatLES01DataApiDemoWidget statLES01DataApiDemoWidget;
    private final StatPA001DataApiDemoWidget statPA001DataApiDemoWidget;
    private final StatPA006DataApiDemoWidget statPA006DataApiDemoWidget;
    private final StatRA0012DataApiDemoWidget statRA0012DataApiDemoWidget;
    private final StatTT065DataApiDemoWidget statTT065DataApiDemoWidget;
    private final StatEH47UDataApiDemoWidget statEH47UDataApiDemoWidget;
    private final StatTU51DataApiDemoWidget statTU51DataApiDemoWidget;
    private final StatTU122DataApiDemoWidget statTU122DataApiDemoWidget;
    private final StatVKT14DataSqlDemoWidget statVKT14DataSqlDemoWidget;

    /**
     * Add your demo widgets here
     */
    private List<DemoWidgetRunMode> demoWidgets() {
        List<DemoWidgetRunMode> widgets = new ArrayList<>();
        widgets.add(dwrm(statEKS21DataApiDemoWidget, ONCE));
        widgets.add(dwrm(statEKS31DataApiDemoWidget, ONCE));
        widgets.add(dwrm(statLES01DataApiDemoWidget, ONCE));
        widgets.add(dwrm(statTT065DataApiDemoWidget, ONCE));
        widgets.add(dwrm(statRA0012DataApiDemoWidget, ONCE));
        widgets.add(dwrm(statPA001DataApiDemoWidget, ONCE));
        widgets.add(dwrm(statPA006DataApiDemoWidget, ONCE));
        widgets.add(dwrm(statEH47UDataApiDemoWidget, ONCE));
        widgets.add(dwrm(statTU51DataApiDemoWidget, ONCE));
        widgets.add(dwrm(statTU122DataApiDemoWidget, ONCE));
        widgets.add(dwrm(statVKT14DataSqlDemoWidget, ONCE));
        return widgets;
    }

    public void build() {
        demoWidgets().forEach(this::tryToRun);
    }

    private void tryToRun(DemoWidgetRunMode widget) {
        try {
            widget.getDemoWidget().build(widget.getMode());
        } catch (Exception e) {
            log.error("{} failed {}", widget.getDemoWidget().nameEn(), e.getMessage(), e);
        }
    }

    @Getter
    @AllArgsConstructor
    static class DemoWidgetRunMode {
        private final DemoWidget demoWidget;
        private final DemoRunMode mode;

        public static DemoWidgetRunMode dwrm(DemoWidget demoWidget, DemoRunMode mode) {
            return new DemoWidgetRunMode(demoWidget, mode);
        }
    }
}
