package ee.stat.dashboard.demo;

import ee.stat.dashboard.config.props.ElementConfig;
import ee.stat.dashboard.model.classifier.Element;
import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.model.widget.back.enums.TimePart;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.model.widget.front.enums.FilterDisplay;
import ee.stat.dashboard.repository.ElementRepository;
import ee.stat.dashboard.service.admin.widget.dto.FilterAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.FilterValueAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

import static ee.stat.dashboard.model.classifier.ClassifierCode.EHAK;
import static ee.stat.dashboard.model.widget.back.StatDataType.DATA_API;
import static ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum.area;
import static ee.stat.dashboard.model.widget.front.enums.FilterDisplay.AXIS;
import static ee.stat.dashboard.model.widget.front.enums.FilterDisplay.LEGEND;
import static ee.stat.dashboard.model.widget.front.enums.FilterDisplay.MENU;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class StatTT065DataApiDemoWidget extends DemoWidget {

    @Override
    public StatDataType statDataType() {
        return DATA_API;
    }

    @Override
    public String cube() {
        return "TT065";
    }

    @Override
    public TimePeriod timePeriod() {
        return TimePeriod.MONTH;
    }

    private final ElementRepository elementRepository;
    private final ElementConfig elementConfig;

    @Override
    public WidgetAdminResponse composeWidget() {
        WidgetAdminResponse widget = widget(LocalDate.of(2018, 1, 1), null, 60, 1);

        widget.setGraphTypes(singletonList(graphTypeSelected(area, areaFilters())));
        return widget;
    }

    @Override
    public String nameEn() {
        return super.nameEn() + statDataType().suffix();
    }

    private List<FilterAdminDto> areaFilters() {
        return asList(
                filter(AXIS, "Aasta", "Year"),

                filter(FilterDisplay.TIME_PART, "Kuu", "Month", TimePart.MONTH),

                filter(MENU, "Piirkond/Haldusüksus", "Piirkond",
                        "Region/Administrative unit", "Region",
                        1, 1, false, true, null, null,
                        ehakValues()
                ),

                filter(MENU, "Sugu", "Sex",
                        asList(filterValueSelected("Mehed ja naised", "Males and females"),
                                filterValue("Mehed", "Males"),
                                filterValue("Naised", "Females"))),

                filter(LEGEND, "Vanuserühm", "Age group",
                        asList(filterValue("16-24"),
                                filterValue("25-54"),
                                filterValue("55 ja vanemad", "55 and older")))
        );
    }

    private List<FilterValueAdminDto> ehakValues() {
        return elementRepository.findAllByClfCode(EHAK).stream().map(this::toValue).collect(toList());
    }

    private FilterValueAdminDto toValue(Element element) {
        FilterValueAdminDto filterValue = new FilterValueAdminDto();
        filterValue.setValueEt(element.getNameEt());
        filterValue.setValueEn(element.getNameEn());
        filterValue.setSelected(element.getCode().equals(elementConfig.getWholeCode()));
        return filterValue;
    }
}
