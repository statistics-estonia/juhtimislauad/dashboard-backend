package ee.stat.dashboard.demo;

import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.TimePart;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.model.widget.front.enums.FilterDisplay;
import ee.stat.dashboard.service.admin.widget.dto.FilterAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Arrays.asList;

@Service
public class StatEKS31DataApiDemoWidget extends DemoWidget {

    @Override
    public StatDataType statDataType() {
        return StatDataType.DATA_API;
    }

    @Override
    public String cube() {
        return "EKS31";
    }

    @Override
    public TimePeriod timePeriod() {
        return TimePeriod.WEEK;
    }

    @Override
    public String nameEn() {
        return super.nameEn() + statDataType().suffix();
    }

    @Override
    public WidgetAdminResponse composeWidget() {
        WidgetAdminResponse widget = widget(null, null, 260, null);

        widget.setGraphTypes(asList(graphTypeSelected(GraphTypeEnum.line, lineFilters())));
        return widget;
    }

    private List<FilterAdminDto> lineFilters() {
        return asList(
                filter(FilterDisplay.AXIS, "Vaatlusperiood", "Reference period"),
                filter(FilterDisplay.TIME_PART, "Nädal", "Week", TimePart.WEEK),

                filter(FilterDisplay.CONSTRAINT, "Näitaja", "Indicator",
                        asList(filterValue("Surmade arv", "Number of deaths"))),

                filter(FilterDisplay.LEGEND, "Sugu", "Sex",
                        asList(
                                filterValue("Mehed ja naised", "Males and females"),
                                filterValue("Mehed", "Males"),
                                filterValue("Naised", "Females"))),

                filter(FilterDisplay.MENU, "Vanuserühm", "Age", 2, 1, false, false, TimePart.WEEK,
                        null, asList(
                                filterValueSelected("Vanuserühmad kokku", "Age groups total"),
                                filterValue("0-64"),
                                filterValue("65-79"),
                                filterValue("80 ja vanemad", "80 and over")))
        );
    }

}
