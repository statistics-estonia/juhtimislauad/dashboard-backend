package ee.stat.dashboard.demo;

import ee.stat.dashboard.model.classifier.Domain;
import ee.stat.dashboard.model.dashboard.Dashboard;
import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.model.widget.back.Widget;
import ee.stat.dashboard.model.widget.back.enums.AxisOrder;
import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.MapRule;
import ee.stat.dashboard.model.widget.back.enums.TimePart;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.model.widget.back.enums.WidgetStatus;
import ee.stat.dashboard.model.widget.front.enums.FilterDisplay;
import ee.stat.dashboard.repository.DashboardRepository;
import ee.stat.dashboard.repository.DomainRepository;
import ee.stat.dashboard.repository.WidgetRepository;
import ee.stat.dashboard.service.admin.WidgetAdminSaveService;
import ee.stat.dashboard.service.admin.dashboard.dto.DashboardAdminResponse;
import ee.stat.dashboard.service.admin.domain.dto.DomainAdminResponse;
import ee.stat.dashboard.service.admin.widget.dto.FilterAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.FilterValueAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.GraphTypeAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.StatDataAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import ee.stat.dashboard.service.statdata.StatDimensionsManager;
import ee.stat.dashboard.service.statdata.StatMetaFetchException;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


import java.time.LocalDate;
import java.util.List;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Slf4j
@Service
public abstract class DemoWidget {

    private static final String ACCOMODATION = "accomodation";
    @Resource
    protected StatDimensionsManager statDimensionsManager;
    @Resource
    protected WidgetAdminSaveService widgetAdminSaveService;
    @Resource
    protected WidgetRepository widgetRepository;
    @Resource
    protected DomainRepository domainRepository;
    @Resource
    protected DashboardRepository dashboardRepository;

    public abstract StatDataType statDataType();

    public abstract String cube();

    public abstract TimePeriod timePeriod();

    public abstract WidgetAdminResponse composeWidget();

    public String nameEn() {
        return field("NameEn");
    }

    public void build(DemoRunMode runMode) {
        if (runMode.disabled()) {
            log.info("Widget [{}] is disabled", nameEn());
            return;
        }
        List<Widget> existingWidget = widgetRepository.findByNameEn(nameEn());
        if (runMode.once() && isNotEmpty(existingWidget)) {
            log.info("Widget [{}] already exists and is skipped", nameEn());
            return;
        }
        if (runMode.rebuild() && isNotEmpty(existingWidget)) {
            existingWidget.stream().map(Widget::getId).forEach(widgetAdminSaveService::delete);
            log.info("Widget [{}] has been deleted", nameEn());
        }
        WidgetAdminResponse widget = composeWidget();

        Long widgetId = widgetAdminSaveService.save(widget);
        widgetAdminSaveService.buildNewDiagramWithId(widgetId);
        log.info("Widget [{}] has been created with id [{}]", nameEn(), widgetId);
    }

    protected WidgetAdminResponse widget(LocalDate startDate, LocalDate endDate, Integer periods, Integer precision) {
        WidgetAdminResponse widget = new WidgetAdminResponse();
        widget.setNameEt(field("NameEt"));
        widget.setNameEn(nameEn());
        widget.setShortnameEt(field("ShortnameEt"));
        widget.setShortnameEn(field("ShortnameEn"));
        widget.setDescriptionEt(field("DescriptionEt"));
        widget.setDescriptionEn(field("DescriptionEn"));
        widget.setNoteEt(field("NoteEt"));
        widget.setNoteEn(field("NoteEn"));
        widget.setSourceEt(field("SourceEt"));
        widget.setSourceEn(field("SourceEn"));
        widget.setUnitEt(field("UnitEt"));
        widget.setUnitEn(field("UnitEn"));
        widget.setMethodsLinkEt(field("MethodLinkEt"));
        widget.setMethodsLinkEn(field("MethodLinkEn"));
        widget.setStatisticianJobLinkEt(field("StatisticianJobLinkEt"));
        widget.setStatisticianJobLinkEn(field("StatisticianJobLinkEn"));
        widget.setNoteEt(field("NoteEt"));
        widget.setNoteEn(field("NoteEn"));
        widget.setPrecisionScale(precision);
        widget.setStatus(WidgetStatus.VISIBLE);
        widget.setTimePeriod(timePeriod());
        widget.setPeriods(periods);
        widget.setStartDate(startDate);
        widget.setEndDate(endDate);
        widget.setStatDb(statDb(cube()));
        widget.setElements(getDomainAdminResponses());
        try {
            widget.setDimensions(statDimensionsManager.getDimensions(cube(), statDataType()));
        } catch (StatMetaFetchException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        return widget;
    }

    private StatDataAdminDto statDb(String cube) {
        StatDataAdminDto statDb = new StatDataAdminDto();
        statDb.setCube(cube);
        statDb.setStatDataType(statDataType());
        return statDb;
    }

    protected GraphTypeAdminDto graphTypeSelected(GraphTypeEnum type, List<FilterAdminDto> filters) {
        return graphTypeSelected(type, filters, null);
    }

    protected GraphTypeAdminDto graphTypeSelected(GraphTypeEnum type, List<FilterAdminDto> filters, MapRule mapRule) {
        GraphTypeAdminDto graphType = new GraphTypeAdminDto();
        graphType.setType(type);
        graphType.setDefaultType(true);
        graphType.setFilters(filters);
        graphType.setMapRule(mapRule);
        return graphType;
    }

    public static FilterAdminDto filter(FilterDisplay filterDisplay, String nameEt, String nameEn, List<FilterValueAdminDto> values) {
        return filter(filterDisplay, nameEt, nameEn, null, null, null, false, null, null, values);
    }

    public static FilterAdminDto filter(FilterDisplay filterDisplay, String nameEt, String nameEn) {
        return filter(filterDisplay, nameEt, nameEn, null, null, true, false, null, null, null);
    }

    public static FilterAdminDto filter(FilterDisplay filterDisplay, String nameEt, String nameEn, TimePart timePart) {
        return filter(filterDisplay, nameEt, nameEn, null, null, null, false, timePart, null, null);
    }

    public static FilterAdminDto filter(FilterDisplay filterDisplay, String nameEt, String nameEn, Integer numberOfValues,
                                        Integer orderNr, Boolean time, Boolean region, TimePart timePart,
                                        AxisOrder axisOrder, List<FilterValueAdminDto> values) {
        return filter(filterDisplay, nameEt, nameEt, nameEn, nameEn, numberOfValues, orderNr, time, region, timePart, axisOrder, values);
    }

    public static FilterAdminDto filter(FilterDisplay filterDisplay, String nameEt, String displayNameEt,
                                        String nameEn, String displayNameEn, Integer numberOfValues,
                                        Integer orderNr, Boolean time, Boolean region, TimePart timePart,
                                        AxisOrder axisOrder, List<FilterValueAdminDto> values) {
        FilterAdminDto filter = new FilterAdminDto();
        filter.setType(filterDisplay);
        filter.setTimePart(timePart);
        filter.setNameEt(nameEt);
        filter.setNameEn(nameEn);
        filter.setNumberOfValues(numberOfValues);
        filter.setDisplayNameEt(displayNameEt);
        filter.setDisplayNameEn(displayNameEn);
        filter.setOrderNr(orderNr);
        filter.setTime(time);
        filter.setRegion(region);
        filter.setValues(values);
        filter.setAxisOrder(axisOrder);
        return filter;
    }

    public static FilterValueAdminDto filterValue(String value) {
        return filterValue(value, value);
    }

    public static FilterValueAdminDto filterValue(String valueEt, String valueEn) {
        FilterValueAdminDto filterValue = new FilterValueAdminDto();
        filterValue.setValueEt(valueEt);
        filterValue.setValueEn(valueEn);
        return filterValue;
    }

    public static FilterValueAdminDto filterValueSelected(String value) {
        return filterValueSelected(value, value);
    }

    public static FilterValueAdminDto filterValueSelected(String valueEt, String valueEn) {
        FilterValueAdminDto filterValue = new FilterValueAdminDto();
        filterValue.setValueEt(valueEt);
        filterValue.setValueEn(valueEn);
        filterValue.setSelected(true);
        return filterValue;
    }

    protected String field(String field) {
        return format("%s %s %s", timePeriod().name(), cube(), field);
    }

    private List<DomainAdminResponse> getDomainAdminResponses() {
        Domain domain = findOrCreate(ACCOMODATION);
        return asList(domain(domain.getId(), dashboard(domain.getDashboard())));
    }

    private Domain findOrCreate(String code) {
        Domain domain = domainRepository.findByCode(code);
        return domain != null ? domain : createDomain();
    }

    private Domain createDomain() {
        Domain newDomain = new Domain();
        newDomain.setLevel(0);
        newDomain.setLevelNameEt("Valdkond");
        newDomain.setLevelNameEn("Domain");
        newDomain.setShowLevelName(false);
        newDomain.setNameEt("Majutus");
        newDomain.setNameEn("Accomodation");
        newDomain.setCode(ACCOMODATION);
        newDomain.setOrderNr(1);
        Dashboard tourismDashboard = dashboardRepository.findByCode("Tourism dashboard");
        if (tourismDashboard == null) {
            tourismDashboard = dashboardRepository.findByCode("tourism");
        }
        newDomain.setDashboard(tourismDashboard.getId());
        return domainRepository.save(newDomain);
    }

    private DomainAdminResponse domain(Long id, DashboardAdminResponse dashboard) {
        DomainAdminResponse domain = new DomainAdminResponse();
        domain.setId(id);
        domain.setDashboard(dashboard);
        return domain;
    }

    private DashboardAdminResponse dashboard(Long id) {
        DashboardAdminResponse dashboard = new DashboardAdminResponse();
        dashboard.setId(id);
        return dashboard;
    }

}
