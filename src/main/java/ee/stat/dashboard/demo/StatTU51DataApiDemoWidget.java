package ee.stat.dashboard.demo;

import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.model.widget.back.enums.AxisOrder;
import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.model.widget.front.enums.FilterDisplay;
import ee.stat.dashboard.service.admin.widget.dto.FilterAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Arrays.asList;

@Service
public class StatTU51DataApiDemoWidget extends DemoWidget {

    @Override
    public StatDataType statDataType() {
        return StatDataType.DATA_API;
    }

    @Override
    public String cube() {
        return "TU51";
    }

    @Override
    public TimePeriod timePeriod() {
        return TimePeriod.YEAR;
    }

    @Override
    public WidgetAdminResponse composeWidget() {
        WidgetAdminResponse widget = widget(null, null, 10, 1);

        widget.setGraphTypes(asList(graphTypeSelected(GraphTypeEnum.pie, pieFilters())));
        return widget;
    }

    @Override
    public String nameEn() {
        return super.nameEn() + statDataType().suffix();
    }

    private List<FilterAdminDto> pieFilters() {
        return asList(
                filter(FilterDisplay.MENU, "Vaatlusperiood", "Reference period"),

                filter(FilterDisplay.CONSTRAINT, "Näitaja", "Indicator",
                        asList(filterValue("Ööbimisega sisereiside arv, tuhat", "Number of overnight domestic trips, thousands"))),

                filter(FilterDisplay.AXIS, "Reisi eesmärk", "Purpose of trip",
                        3, 0, false, false, null, AxisOrder.VALUE_DESC,
                        asList(
                                filterValue("Tööreis", "Professional, business"),
                                filterValue("Puhkusereis", "Holidays, leisure and recreation"),
                                filterValue("Tuttava või sugulase külastamine", "Visits to friends and relatives"),
                                filterValue("Muu isiklik reis", "Other")
                        ))
        );
    }
}
