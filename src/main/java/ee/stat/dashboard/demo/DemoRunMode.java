package ee.stat.dashboard.demo;

public enum DemoRunMode {

    /**
     * create new demo data on each run
     */
    CREATE_NEW,
    /**
     * create demo data only once
     */
    ONCE,
    /**
     * create demo data on each run deleting the existing one first
     */
    REBUILD,
    /**
     * disabled
     */
    DISABLED;

    public boolean createNew(){
        return this == CREATE_NEW;
    }

    public boolean once(){
        return this == ONCE;
    }

    public boolean rebuild(){
        return this == REBUILD;
    }

    public boolean disabled(){
        return this == DISABLED;
    }
}
