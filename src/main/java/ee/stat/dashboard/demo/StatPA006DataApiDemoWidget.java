package ee.stat.dashboard.demo;

import ee.stat.dashboard.model.widget.back.StatDataType;
import ee.stat.dashboard.model.widget.back.enums.GraphTypeEnum;
import ee.stat.dashboard.model.widget.back.enums.TimePeriod;
import ee.stat.dashboard.model.widget.front.enums.FilterDisplay;
import ee.stat.dashboard.service.admin.widget.dto.FilterAdminDto;
import ee.stat.dashboard.service.admin.widget.dto.WidgetAdminResponse;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Arrays.asList;

@Service
public class StatPA006DataApiDemoWidget extends DemoWidget {

    @Override
    public StatDataType statDataType() {
        return StatDataType.DATA_API;
    }

    @Override
    public String cube() {
        return "PA006";
    }

    @Override
    public TimePeriod timePeriod() {
        return TimePeriod.MONTH;
    }

    @Override
    public WidgetAdminResponse composeWidget() {
        WidgetAdminResponse widget = widget(null, null, 120, 1);

        widget.setGraphTypes(asList(graphTypeSelected(GraphTypeEnum.bar, areaFilters())));
        return widget;
    }

    @Override
    public String nameEn() {
        return super.nameEn() + statDataType().suffix();
    }

    private List<FilterAdminDto> areaFilters() {
        return asList(
                filter(FilterDisplay.AXIS, "Vaatlusperiood", "Reference period"),

                filter(FilterDisplay.CONSTRAINT, "Näitaja", "Indicator",
                        asList(
                                filterValue("Keskmine brutokuupalk, eurot",
                                        "Average monthly gross wages (salaries), euros"))),

                filter(FilterDisplay.LEGEND, "Tegevusala", "Economic activity",
                        asList(
                                filterValue("Põllumajandus, metsamajandus ja kalapüük",
                                        "Agriculture, forestry and fishing"),
                                filterValue("Mäetööstus",
                                        "Mining and quarrying"),
                                filterValue("Töötlev tööstus",
                                        "Manufacturing")
                        )
                )
        );
    }
}
