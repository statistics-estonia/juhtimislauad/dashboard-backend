#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE DATABASE dashboard;
    \connect dashboard
    CREATE SCHEMA dashboard;

    CREATE USER dashboard WITH ENCRYPTED PASSWORD 'dashboard';
    GRANT CONNECT ON DATABASE dashboard TO dashboard;
    GRANT ALL ON schema dashboard TO dashboard;
EOSQL