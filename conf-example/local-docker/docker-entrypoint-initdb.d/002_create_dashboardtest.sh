#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE DATABASE dashboardtest;
    \connect dashboardtest
    CREATE SCHEMA dashboardtest;

    CREATE USER dashboardtest WITH ENCRYPTED PASSWORD 'dashboardtest';
    GRANT CONNECT ON DATABASE dashboardtest TO dashboardtest;
    GRANT ALL ON schema dashboardtest TO dashboardtest;
EOSQL