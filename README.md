#Setup

Create new directory "conf" under the root folder. 
Copy all files from "conf-example" to "conf". Update all the scripts.

#### Database

###### run docker:

cd conf/local-docker

And use one of the scripts. Essentially scripts set up docker-compose:

docker-compose up

#### Application

###### run application:

mvn clean spring-boot:run -Dspring.config.location=conf/custom.yaml

###### swagger:

http://localhost:5080/api/swagger-ui.html

###### create jar:

mvn clean verify

###### running flyway separately from application (multiple backend nodes)

mvn compile flyway:migrate -Dflyway.configFile=conf/custom.yaml

###### install stat source
./mvnw install:install-file -DgroupId=no.ssb.jsonstat -DartifactId=json-stat-java -Dversion=0.2.7-SNAPSHOT -Dpackaging=jar -Dfile=lib/json-stat-java-0.2.7-SNAPSHOT.jar -DgeneratePom=true
